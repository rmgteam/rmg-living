<p>Please find below your login details for the CPM Living website. To access your account for:</p>
<p><? if($unit->unit['unit_address_1'] != ""){print "<b>Property:</b> ".$unit->unit['unit_address_1'];}?><br><?="<b>Management Company:</b> ".$rmc->rmc['rmc_name'];?></p>
<p> visit www.cpmliving.com and click the 'Login' button. Enter the following details as they appear in this letter:</p>
<table>
	<tr>
	<td nowrap class="letter">Lessee ID:</td>
	<td class="letter"><strong><?=$resident->resident['resident_num']?></strong></td>
	</tr>
	<tr>
	<td class="letter">Password:</td>
	<td class="letter"><strong><?=$resident->resident['password']?></strong></td>
	</tr>
</table>
<p>When you log in you will be asked to reset your  password. If you forget your password you will  need contact Technical Support  at support@cpmliving.com, or use the password reminder facility on the login page of the website. It is important that you do not disclose your password to anyone else.</p>
<p>We are continually looking for ways to expand the CPM Living website to include features that will benefit our customers. We welcome your feedback, either through letter or by email, about the site and any additions to the service that you would find useful.</p>