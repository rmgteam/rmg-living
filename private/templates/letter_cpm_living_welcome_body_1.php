<p><? if($unit->unit['unit_address_1'] != ""){print "<b>Property:</b> ".$unit->unit['unit_address_1'];}?><br><?="<b>Management Company:</b> ".$rmc->rmc['rmc_name'];?></p>
<p>CPM Living is part of our continued drive to improve service delivery to our customers. We were aware that many of our customers require an immediate service. From today, you are now able to view your statement and pay online through the secure CPM Living web site. To access your account visit  www.cpmliving.com and click on the 'Login' button. Enter the details below as they appear in this letter:</p>
<table>
	<tr>
	<td nowrap class="letter">Lessee ID:</td>
	<td class="letter"><strong><?=$resident->resident['resident_num']?></strong></td>
	</tr>
	<tr>
	<td class="letter">Password:</td>
	<td class="letter"><strong><?=$resident->resident['password']?></strong></td>
	</tr>
</table>
<p>The first time you login you will be asked to set your own password. If you forget your password you will need to contact Technical Support  at support@cpmliving.com, or use the password reminder facility on the login page of the website.</p>
<p>Over the coming months, the service will be expanded to allow the Directors of your Management Company to view service charge expenditure along with other information crucial to the management of your property. We shall be discussing with your Directors the full range of services we are able to provide through CPM Living over the coming weeks.</p>
<p>We believe we are one of the first agents to be able to offer this unique service to our customers. CPM Living is part of continued commitment to improve our customer service. We hope you will take the time to access your account and welcome your feedback. Any comments would be appreciated either through letter or by email. </p>		  