<p><b><?="Re: ".$rmc->rmc['rmc_name'];?></b></p>
<p>As you may be aware from recent communication detailing the improvements to our service and delivery, we are delighted to announce that phase two of CPM Living� is now available. </p>
<!--
<p>In order to access your area, you will need visit www.cpmliving.com and click the 'Login' button. Enter the details below, as they appear in this letter:</p>
<table>
	<tr>
	<td nowrap class="letter">Lessee ID:</td>
	<td class="letter"><strong><?=$resident->resident['resident_num']?></strong></td>
	</tr>
	<tr>
	<td class="letter">Password:</td>
	<td class="letter"><strong><?=$resident->resident['password']?></strong></td>
	</tr>
</table>
-->
<p>Over the forthcoming months, further information will be posted on to CPM Living� which will include notices of AGM and general letters of interest. However, financial information on the Company is available to access immediately, and these are as follows:</p>
<p><b>Aging Summary Report</b> � This is an arrears report on your particular development. It lists all residents who have an outstanding debt to the Management Company. The �Current Owed� column shows the amount owed by each debtor.</p>
<p><b>Account Ledger Report</b> � A report that provides a full breakdown of all expenditure (i.e. listing all contractors� names, what the work was for and the cost of this work). This report supplements your �Budget Comparison Cash Flow (Accrual) Report�.</p>
<p><b>Budget Comparison Cash Flow (Accrual) Report (BCCF)</b> � Your BCCF comprises 10 columns detailing your Management Company�s monthly expenditure, both actual and budgeted and any variances (if applicable); your yearly expenditure for the financial year, both actual and budgeted and any variances (if applicable).</p>
<p><b>Payable � Aging Detail Report</b> � This report provides you with a list of all unpaid invoices.</p>
<p><b>Balance Sheet</b> � This is a report effectively providing a snapshot of your company�s financial position at any given time.</p>
<p>The debtors figure will tally with what is shown in the �Aging Summary Report�, whilst the Service Charge account figure will tally with what is shown in the �Budget Comparison Cash Flow (Accrual)�. Finally, you will see that the Creditors Control Account figure is the same as shown in the unpaid invoice report, �Payable Aging Detail�.</p>
<p class="break"><!--&nbsp;--></p>
<p>At CPM, we are always looking to improve standards. The inspiration behind CPM Living� came from the desire to enhance communications with Resident Management Companies and ourselves, offering a tailored, web-based communication tool that gives real time interaction.</p>
<p>I believe that phase two of CPM Living� sets us apart from any other Residential Management Agents in the UK.  It is our stated aim to provide clear transparency in all our dealings with all Directors. By developing such facilities as CPM Living�, and being able to access your financial information online, CPM is taking significant and successful steps to achieve this objective.</p>
<p>As I expect you will appreciate, to maintain and develop CPM Living� does not come without expense for CPM, but we would like to offer this facility to you for the next three months free of charge.  At the end of this period, I suggest we review together how useful this service is to you as a Director and what if any, improvement can be made to the website. Whilst of course, we believe the website provides you with all the information you require, you may have a different view and from this exchange, we hope to be able to develop a tool that fits exactly with your requirements. </p>