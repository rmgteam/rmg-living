<?
//======================================================
// Class to build and manipulate HTML letter templates
//======================================================

class letter {

	var $signature_array = array();
	var $your_ref = "";
	var $recipient_name;
	var $recipient_address_1;
	var $recipient_address_2;
	var $recipient_address_3;
	var $recipient_address_city;
	var $recipient_address_county;
	var $recipient_address_postcode;
	//var $recipient_address_country;
	
	function set_recipient($id, $type = 1){
		if($type == 1){
			if($id != ""){
				$sql = "SELECT * FROM cpm_residents WHERE resident_num ='".$id."'";
				$result = mysql_query($sql);
				$this->recipient = mysql_fetch_array($result);
				$this->recipient_name = $this->recipient['resident_name'];
				$this->recipient_address_1 = $this->recipient['resident_address_1'];
				$this->recipient_address_2 = $this->recipient['resident_address_2'];
				$this->recipient_address_3 = $this->recipient['resident_address_3'];
				$this->recipient_address_city = $this->recipient['resident_address_city'];
				$this->recipient_address_county = $this->recipient['resident_address_county'];
				$this->recipient_address_postcode = $this->recipient['resident_address_postcode'];
				//$this->recipient_address_country = $this->recipient['resident_address_country'];
			}
			else{
				return false;
			}
		}
		
		$this->set_your_ref($id, $type);
		$this->set_opening($this->recipient_name, 3);
	}


	//==========================================================================================
	// Letter reference function
	//==========================================================================================
	
	// Sets the recipients reference for the letter 
	function set_your_ref($id, $type = 1){
		
		if($id != ""){
			
			// Type 1 = reference relating to resident
			if($type == 1){
				$sql = "SELECT rmc_num FROM cpm_residents WHERE resident_num ='".$id."'";
				$result = mysql_query($sql);
				$num_rows = mysql_num_rows($result);
				if($num_rows == 1){
					$row = mysql_fetch_row($result);
					$rmc_num = $row[0];
					$this->your_ref = "Your ref: $id-$rmc_num";
					return true;
				}
				else{
					return false;
				}
			}
			
		}
		else{
			return false;
		}
		
	}
	
	// Gets the recipients reference for the letter
	function get_your_ref(){
		return $this->your_ref;
	}
	
	
	//===================================================================================================
	// Recipient address function(s)
	//===================================================================================================
	
	function get_address($format = 1){
	
		switch ($format){
			case 1:		$start_enclosure = "";
						$finish_enclosure = "";
						$begin_enclosure = "";
						$end_enclosure = "<br>";
						break;
			case 2:		$start_enclosure = "<table>";
						$finish_enclosure = "</table>";
						$begin_enclosure = "<tr><td nowrap>";
						$end_enclosure = "</td></tr>";
						break;
		}
	

		if($this->recipient_name != ""){$formatted_address .= $begin_enclosure.$this->recipient_name.$end_enclosure;}
		if($this->recipient_address_1 != ""){$formatted_address .= $begin_enclosure.$this->recipient_address_1.$end_enclosure;}
		if($this->recipient_address_2 != ""){$formatted_address .= $begin_enclosure.$this->recipient_address_2.$end_enclosure;}
		if($this->recipient_address_3 != ""){$formatted_address .= $begin_enclosure.$this->recipient_address_3.$end_enclosure;}
		if($this->recipient_address_city != ""){$formatted_address .= $begin_enclosure.$this->recipient_address_city.$end_enclosure;}
		if($this->recipient_address_county != ""){$formatted_address .= $begin_enclosure.$this->recipient_address_county.$end_enclosure;}
		if($this->recipient_address_postcode != ""){$formatted_address .= $begin_enclosure.$this->recipient_address_postcode.$end_enclosure;}
		//if($this->recipient_address_country != ""){$formatted_address .= $begin_enclosure.$this->recipient_address_country.$end_enclosure;}
		$formatted_address = $start_enclosure.$formatted_address.$finish_enclosure;
		
		return $formatted_address;
		
	}
	
	
	//===========================================
	// 'Opening' functions
	//===========================================

	// Sets opening greeting
	function set_opening($name, $type = 1){
		if($type == 1){$this->opening = "Dear Sir/Madam,";}
		if($type == 2){$this->opening = "To whom it may concern,";}
		if($type == 3){$this->opening = "Dear $name,";}
	}
	
	function get_opening(){
		return $this->opening;
	}
	
	
	//==========================================================
	// Functions to set main body of letter (uses files in 'templates' folder)
	//==========================================================
	function set_body($template){
	
		global $UTILS_TEMPLATE_PATH;
	
		if($template != ""){
			$this->body = $UTILS_TEMPLATE_PATH.$template;
		}
		else{
			return false;
		}
	}

	function get_body(){
		return $this->body;
	}
	

	//===========================================
	// 'Closing' functions
	//===========================================

	// Sets up signature components based in username provided
	function set_signature($username = "cwoods"){
		$sql = "SELECT * FROM cpm_signatures WHERE signature_username = '".$username."'";
		$result = mysql_query($sql);
		$this->signature = mysql_fetch_array($result);	
	}
	
	// Sets closing (Yours sincerely, etc.)
	function set_closing($closing = "Yours sincerely,"){
		$this->signature['signature_closing'] = $closing;
	}
	
	// Returns signature
	function get_signature($sig_image=1, $img_per=100){
	
		if(is_array($this->signature)){
		
			global $UTILS_FILE_PATH;
			global $UTILS_WEBROOT;
		
			// Tests for image and returns new width and height
			unset($img_size);
			$img_size = $this->set_signature_image_size($UTILS_FILE_PATH.$this->signature['signature_image'], $img_per);
			
			// Sets closing if not already called
			if($this->signature['signature_closing'] == ""){$this->set_closing();}
			unset($this->signature_array);
			$this->signature_array[] = $this->signature['signature_closing'];
			
			// Sets up signature components into array
			if($sig_image && is_array($img_size)){
				$this->signature_array[] = "<img src=\"".$UTILS_WEBROOT.$this->signature['signature_image']."\" width=\"".$img_size[0]."\" height=\"".$img_size[1]."\">";
			}
			else{
				$this->signature_array[] = "<br>";
			}
			if($this->signature['signature_name'] != ""){$this->signature_array[] = $this->signature['signature_name'];}
			if($this->signature['signature_job_title'] != ""){$this->signature_array[] = $this->signature['signature_job_title'];}
			if($this->signature['signature_division'] != ""){$this->signature_array[] = $this->signature['signature_division'];}
			
			// Compiles signature components into signature
			for($s=0;$s<(count($this->signature_array)-1);$s++){
				$final_signature .= $this->signature_array[$s]."<br>";
			}
			$final_signature .= $this->signature_array[$s];
			
			return $final_signature;
		}
		else{
			return false;
		}
	}
	
	// Adjusts signature size (signatures are usually stored as high res.)
	function set_signature_image_size($src, $per=100){
		if($src != ""){
		
			// Sets image size to display
			$img_size = getimagesize($src);
			$img_size[0] = ceil($img_size[0] * ($per/100));
			$img_size[1] = ceil($img_size[1] * ($per/100));
			return $img_size;
		}
		else{
			return false;
		}
	}
	
	

	
	

	
	
	
	
	

}

?>