<?

//======================================================
// Class to extract and manipulate resident data
//======================================================

class rmc {

	function set_rmc($id){
		if($id != ""){
			$sql = "SELECT * FROM cpm_rmcs rmc, cpm_rmcs_extra rmcex WHERE rmc.rmc_num=rmcex.rmc_num AND rmc.rmc_num = '".$id."'";
			$result = mysql_query($sql);
			$this->rmc = mysql_fetch_array($result);
		}
		else{
			return false;
		}
	}

	function get_num_letters_of_interest(){

		// Letters of Interest
		$sql_letters = "SELECT * FROM cpm_letters  WHERE rmc_num = ".$this->rmc['rmc_num'];
		$result_letters = @mysql_query($sql_letters);
		$num_letters = @mysql_num_rows($result_letters);
		return $num_letters;
	}
	
	function get_num_newsletters(){
	
		// Newsletters
		$sql_newsletters = "SELECT * FROM cpm_newsletters WHERE rmc_num = ".$this->rmc['rmc_num'];
		$result_newsletters = @mysql_query($sql_newsletters);
		$num_newsletters = @mysql_num_rows($result_newsletters);
		return $num_newsletters;
	}
	
	function get_num_ssm(){
		
		// Site Specific Maintenance documents
		$sql_ssm = "SELECT * FROM cpm_ssm WHERE rmc_num = ".$this->rmc['rmc_num'];
		$result_ssm = @mysql_query($sql_ssm);
		$num_ssm = @mysql_num_rows($result_ssm);
		return $num_ssm;
	}
	
	function get_num_slas(){
		
		// SLA's
		$sql_sla = "SELECT * FROM cpm_sla WHERE rmc_num = ".$this->rmc['rmc_num'];
		$result_sla = @mysql_query($sql_sla);
		$num_slas = @mysql_num_rows($result_sla);
		return $num_slas;
	}
	
	function get_num_car_park_plans(){
				
		// Car park plan(s)
		$sql_car_park = "SELECT * FROM cpm_car_park WHERE rmc_num = ".$this->rmc['rmc_num'];
		$result_car_park = @mysql_query($sql_car_park);
		$num_car_park_plans = @mysql_num_rows($result_car_park);
		return $num_car_park_plans;
	}
	
	function get_num_budgets(){
				
		// Budget(s)
		$sql_budget = "SELECT * FROM cpm_budgets WHERE rmc_num = ".$this->rmc['rmc_num'];
		$result_budget = @mysql_query($sql_budget);
		$num_budgets = @mysql_num_rows($result_budget);
		return $num_budgets;
	}
	
	function get_num_meeting_notes(){
	
		// Meeting notes
		$sql_meeting_notes = "SELECT * FROM cpm_agm WHERE rmc_num = ".$this->rmc['rmc_num'];
		$result_meeting_notes = @mysql_query($sql_meeting_notes);
		$num_meeting_notes = @mysql_num_rows($result_meeting_notes);
		return $num_meeting_notes;
	}

}

?>