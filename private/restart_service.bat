@echo off

SET myvar=''

for /f "delims=" %%R in ('D:\Sites\rmgliving.co.uk\private\mysql_live.bat') do Set myvar=%%R

if NOT %myvar% == '' GOTO lEND

net stop "ssh_tunnel"
net start "ssh_tunnel"

:lEND

SET myvar=''

for /f "delims=" %%R in ('D:\Sites\rmgliving.co.uk\private\mysql_dev.bat') do Set myvar=%%R

if NOT %myvar% == '' GOTO dEND

net stop "ssh_tunnel_dev"
net start "ssh_tunnel_dev"

:dEND

SET myvar=''

for /f "delims=" %%R in ('D:\Sites\rmgliving.co.uk\private\mysql_stage.bat') do Set myvar=%%R

if NOT %myvar% == '' GOTO sEND

net stop "ssh_tunnel_stage"
net start "ssh_tunnel_stage"

:sEND