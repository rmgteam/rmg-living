<?
require("utils.php");
require($UTILS_CLASS_PATH."website.class.php");
$website = new website;

// Determine if allowed access into 'your community' section
$website->allow_community_access();

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><!-- InstanceBegin template="/Templates/main.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- InstanceBeginEditable name="doctitle" -->
<title>RMG Living - Agent Responsibilities</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->







<!-- InstanceEndEditable -->
<link href="styles.css" rel="stylesheet" type="text/css">
<!--[if lte IE 8]> 
<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if lte IE 7]> 
<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
<![endif]-->
</head>
<body>
<? if($_SESSION['is_demo_account'] == "Y" && $_SESSION['is_developer'] != "Y"){?>
<form method="post" action="/building_details.php" style="margin:0; padding:0;">
<input type="hidden" name="a" value="cl">
<input type="hidden" name="s" value="gkfcbk45cgtf5kngn7kns5cg7snk5g7nsv5ng">
<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" style="margin:0; padding:0;">
	<tr>
		<td style="padding:4px;">Change Account:&nbsp;<input type="text" name="change_lessee" value="<?=$_SESSION['resident_ref']?>" size="20"> <input type="submit" name="submit_button" value="Change"></td>
	</tr>
</table>
</form>
<? }?>
<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#E9EEF4" style="border-bottom:1px solid #c1d1e1;"><div align="center"><img src="images/templates/header_bar_grad2.jpg" alt=""></div></td>
  </tr>
  <tr>
    <td style="border-top:1px solid #ffffff;border-bottom:1px solid #ffffff;">
	<table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="180"><a href="<? print $UTILS_HTTPS_ADDRESS;if( isset($_SESSION['login']) && $_SESSION['login'] != ""){print "/building_details.php";}else{print "/index.php";}?>"><img src="/images/templates/rmg_living_logo.jpg" style="margin-top:10px;" width="180" alt="RMG Living" border="0"></a></td>
        <td width="580" align="right">
		<?
		// User logged in is a 'developer', show any developer specific logos, or default to normal ones...
		if($_SESSION['is_developer'] == "Y"){
			
			$sql = "SELECT developer_id FROM cpm_residents_extra WHERE resident_num = ".$_SESSION['resident_num'];
			$result = @mysql_query($sql);
			$row = @mysql_fetch_row($result);
			if($row[0] != "" && file_exists($UTILS_FILE_PATH.'/images/developers/'.$row[0].'/header_'.$row[0].'.jpg') ){
				
				print '<img src="/images/developers/'.$row[0].'/header_'.$row[0].'.jpg" alt="RMG Living" width="580" height="86" BORDER=0>';
			}
			else{
				
				print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
			}
		}
		else{
		
			// Check to see if the Man. Co. has a developer association, if so, set developer specific images, or default to normal images...
			if( isset($_SESSION['login']) && $_SESSION['login'] != "" && $_SESSION['rmc_num'] != ""){
			
				$sql = "SELECT developer_id FROM cpm_rmcs_extra WHERE rmc_num = ".$_SESSION['rmc_num'];
				$result = @mysql_query($sql);
				$row = @mysql_fetch_row($result);
				if( $row[0] != "" && file_exists($UTILS_FILE_PATH.'/images/developers/'.$row[0].'/header_'.$row[0].'.jpg') ){
					
					print '<img src="/images/developers/'.$row[0].'/header_'.$row[0].'.jpg" alt="RMG Living" width="580" height="86" BORDER=0>';
				}
				else{
					
					print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
				}
			}
			else{
				
				print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
			}
		}
		?> 
		
		<?
		/*
		if( isset($_SESSION['login']) && $_SESSION['login'] != "" && $_SESSION['subsidiary_id'] != ""){
			$sql = "SELECT subsidiary_header_image FROM cpm_subsidiary WHERE subsidiary_id = ".$_SESSION['subsidiary_id'];
			$result = @mysql_query($sql);
			$row = @mysql_fetch_row($result);
			if($row[0] != "" ){ 
				?>
				<img src="/images/templates/header_image_w_logo.jpg" width="580" height="86" border="0" alt="RMG Living">
				<img src="/images/logos/<?=$row[0]?>" style="position:absolute; left:50%; margin-left:245px; margin-top:23px; z-index:10;" border="0" >
				<?
			}
			else{
				?>
				<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">
				<?
			}
		}
		else{
			?>
			<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">
			<?
		}
		*/
		?>
		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
  <td>
  <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
  <tr>
    <td height="10" align="center" valign="bottom" bgcolor="#E9EEF4" style="border-bottom:1px solid #c1d1e1;border-top:1px solid #c1d1e1;"><img src="images/templates/header_bar_grad2.jpg" alt=""></td>
  </tr>
  <tr>
  <td height="8" align="center" valign="top" style="border-top:1px solid #ffffff;"><img src="images/templates/header_bar_grad3.jpg" alt=""></td>
  </tr>
  </table>
  </td>
  </tr>
 
  <tr>
    <td height="4"></td>
  </tr>
  <tr>
    <td><table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="28" valign="top">
	
	<table width="760" cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td>
	<!-- InstanceBeginEditable name="breadcrumbs" --><a href="/building_details.php" class="crumbs">Your Community</a>&nbsp;>&nbsp;<a href="general_info.php" class="crumbs">General Info</a>&nbsp;>&nbsp;Agent Responsibilities<!-- InstanceEndEditable -->
	</td>
	<td style="text-align:right;" nowrap>
	<? if(!empty($_SESSION['resident_session'])){?><a href="index.php?logoff=Y" class="crumbs">Log Off</a><? }?>
	</td>
	</tr>
	</table>
	
	</td>
  </tr>
</table>
</td>
  </tr>
  <tr>
    <td><!-- InstanceBeginEditable name="body" -->
      <table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td style="border-bottom:1px solid #003366;"><table width="760" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="15"><img src="images/dblue_box_top_left_corner.jpg" width="15" height="33"></td>
              <td width="730" bgcolor="#416CA0" style="border-top:1px solid #003366; vertical-align:middle"><img src="images/general_info/general_info_symbol.jpg" width="22" height="22" style="vertical-align:middle;">&nbsp;&nbsp;<span class="box_title">Agent Responsibilities (RMG) </span></td>
              <td width="15" align="right"><img src="images/dblue_box_top_right_corner.jpg" width="15" height="33"></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td><table width="758" border="0" cellspacing="0" cellpadding="15" style="border-left:1px solid #7ea0bd;border-right:1px solid #7ea0bd;border-top:1px solid #7ea0bd;border-bottom:1px solid #cccccc;">
            <tr>
              <td width="380" valign="top">
<table width="348" border="0" cellspacing="0" cellpadding="0">
                
<tr>
                  
<td width="260" height="1"></td>
<td width="88"></td>
</tr>
<tr valign="top">
                  
<td colspan="2"><p>As a Managing Agent, it's our job to manage the service providers that look after the upkeep of your development and assume the time-consuming administrative tasks involved in the day-to-day running of any development.</p>
  <p>In practice this means that RMG works on behalf of and at the direction of your Residents' Management Company (RMC), which has been entrusted with the task of overseeing the management of your living environment and setting the service charge budget.</p>
  <p>Leaseholders (you and your neighbours) are usually the 'owners' of the RMC who will vote for fellow leaseholders to be Directors who have the responsibility of that RMC. Your RMC Directors instruct RMG to collect service charges and allocate those funds accordingly; these funds are used to maintain the upkeep of the development and to pay for services and contractors, a service we have been providing for 20 years for hundreds of residential management companies.</p>
  <p>Services are divided into two main categories: Day-to-day Management and Administration (Financial and Legal).</p>
  <p><span class="text036"><strong>Day-to-day Management services include:</strong></span><br>
  	<ul>
    <li>Cleaning and lighting of all common areas (hallways, staircases, etc.)</li>
    <li>Gardening of the communal grounds</li>
    <li>Maintenance of lifts, security and intercom systems</li>
    <li>Arranging for external and internal re-decoration</li>
    <li>Appointing and managing the on-site concierge or caretakers</li>
    <li>Arranging your buildings insurance</li>
    <li>Various other repairs and renewals when needed</li>
	</ul>
	</p>
  	<p><span class="text036"><strong>Administration (Financial and Legal) services include:</strong></span><br>
	<ul>
	<li>Collecting money from you in the shape of your service charge (once the budget has been approved by directors of the residents' management company)</li>
	<li>Managing the residents' management company bank account</li>
	<li>Paying service providers on your behalf</li>
	<li>Administering and recording all transactions and relevant records</li>
	<li>Overseeing statutory accounting and returns</li>
	<li>Managing the property in line with relevant legislation including company law, terms of leases and good business practices</li>
	<li>Attending residents' meetings, preparing and circulating meeting notes</li>
	<li>Managing the tendering process for service and major works in accordance with statutory requirements</li>
	</ul>
            Our benchmark service is based on timely payment; the late or non-payment of service charges is in breach of your contractual agreement with your Management Company and will lead to non-attendance of service contractors on your development.  On these occasions RMC Directors may instruct RMG to seek debt recovery of any outstanding monies for them. </p></td>
</tr>
              </table>
			  
			  </td>
              <td valign="top" background="images/general_info/knowing_cpm.jpg" bgcolor="#F5F7FB" style="border-left:1px solid #eaeaea;background-repeat:no-repeat;"><p><img src="images/spacer.gif" alt="Knowing the responsibilities of RMG" width="348" height="310">
                </p>
              </td>
              </tr>
          </table></td>
        </tr>
		<tr><td height="5" bgcolor="#416CA0" style="border:1px solid #003366;"><img src="images/spacer.gif" alt=""></td>
		</tr>
      </table>
    <!-- InstanceEndEditable --></td>
  </tr>
  <tr>
    <td height="25"></td>
  </tr>
  <tr>
    <td><table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="445" height="14"><a href="about_us.php" class="link416CA0">About RMG Living</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="contact_us.php" class="link416CA0">Contact Us</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="privacy.php" class="link416CA0">Legal Information</a></td>
        <td width="315" align="right">Copyright <?=date("Y", time())?> &copy; <a href="http://www.rmgltd.co.uk">Residential Management Group Ltd</a></td>
      </tr>
	  <tr><td colspan="2">&nbsp;</td></tr>
	  <tr><td colspan="2" style="text-align:left;"><p>&nbsp;</p>
	    </td>
	  </tr>
    </table></td>
  </tr> 
  <tr><td>&nbsp;</td></tr>
</table>
</body>
<!-- InstanceEnd --></html>
