<?
require("utils.php");
require_once($UTILS_SERVER_ROOT."library/functions/get_file_size.php");
require($UTILS_CLASS_PATH."website.class.php");
require($UTILS_CLASS_PATH."rmc.class.php");
$website = new website;
$rmc = new rmc;

// Determine if allowed access into 'your community' section
$website->allow_community_access();

// Set RMC
$rmc->set_rmc($_SESSION['rmc_num']);

// Determine if Budgets available
$sql_budget = "SELECT * FROM cpm_budgets b, cpm_budget_types bt WHERE b.budget_type_id=bt.budget_type_id AND b.rmc_num = ".$_SESSION['rmc_num'];
$result_budget = @mysql_query($sql_budget);
$num_budgets = @mysql_num_rows($result_budget);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><!-- InstanceBegin template="/Templates/main.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- InstanceBeginEditable name="doctitle" -->
<title>RMG Living - Budgets</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->



<!-- InstanceEndEditable -->
<link href="styles.css" rel="stylesheet" type="text/css">
<!--[if lte IE 8]> 
<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if lte IE 7]> 
<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
<![endif]-->
</head>
<body>
<? if($_SESSION['is_demo_account'] == "Y" && $_SESSION['is_developer'] != "Y"){?>
<form method="post" action="/building_details.php" style="margin:0; padding:0;">
<input type="hidden" name="a" value="cl">
<input type="hidden" name="s" value="gkfcbk45cgtf5kngn7kns5cg7snk5g7nsv5ng">
<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" style="margin:0; padding:0;">
	<tr>
		<td style="padding:4px;">Change Account:&nbsp;<input type="text" name="change_lessee" value="<?=$_SESSION['resident_ref']?>" size="20"> <input type="submit" name="submit_button" value="Change"></td>
	</tr>
</table>
</form>
<? }?>
<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#E9EEF4" style="border-bottom:1px solid #c1d1e1;"><div align="center"><img src="images/templates/header_bar_grad2.jpg" alt=""></div></td>
  </tr>
  <tr>
    <td style="border-top:1px solid #ffffff;border-bottom:1px solid #ffffff;">
	<table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="180"><a href="<? print $UTILS_HTTPS_ADDRESS;if( isset($_SESSION['login']) && $_SESSION['login'] != ""){print "/building_details.php";}else{print "/index.php";}?>"><img src="/images/templates/rmg_living_logo.jpg" style="margin-top:10px;" width="180" alt="RMG Living" border="0"></a></td>
        <td width="580" align="right">
		<?
		// User logged in is a 'developer', show any developer specific logos, or default to normal ones...
		if($_SESSION['is_developer'] == "Y"){
			
			$sql = "SELECT developer_id FROM cpm_residents_extra WHERE resident_num = ".$_SESSION['resident_num'];
			$result = @mysql_query($sql);
			$row = @mysql_fetch_row($result);
			if($row[0] != "" && file_exists($UTILS_FILE_PATH.'/images/developers/'.$row[0].'/header_'.$row[0].'.jpg') ){
				
				print '<img src="/images/developers/'.$row[0].'/header_'.$row[0].'.jpg" alt="RMG Living" width="580" height="86" BORDER=0>';
			}
			else{
				
				print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
			}
		}
		else{
		
			// Check to see if the Man. Co. has a developer association, if so, set developer specific images, or default to normal images...
			if( isset($_SESSION['login']) && $_SESSION['login'] != "" && $_SESSION['rmc_num'] != ""){
			
				$sql = "SELECT developer_id FROM cpm_rmcs_extra WHERE rmc_num = ".$_SESSION['rmc_num'];
				$result = @mysql_query($sql);
				$row = @mysql_fetch_row($result);
				if( $row[0] != "" && file_exists($UTILS_FILE_PATH.'/images/developers/'.$row[0].'/header_'.$row[0].'.jpg') ){
					
					print '<img src="/images/developers/'.$row[0].'/header_'.$row[0].'.jpg" alt="RMG Living" width="580" height="86" BORDER=0>';
				}
				else{
					
					print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
				}
			}
			else{
				
				print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
			}
		}
		?> 
		
		<?
		/*
		if( isset($_SESSION['login']) && $_SESSION['login'] != "" && $_SESSION['subsidiary_id'] != ""){
			$sql = "SELECT subsidiary_header_image FROM cpm_subsidiary WHERE subsidiary_id = ".$_SESSION['subsidiary_id'];
			$result = @mysql_query($sql);
			$row = @mysql_fetch_row($result);
			if($row[0] != "" ){ 
				?>
				<img src="/images/templates/header_image_w_logo.jpg" width="580" height="86" border="0" alt="RMG Living">
				<img src="/images/logos/<?=$row[0]?>" style="position:absolute; left:50%; margin-left:245px; margin-top:23px; z-index:10;" border="0" >
				<?
			}
			else{
				?>
				<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">
				<?
			}
		}
		else{
			?>
			<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">
			<?
		}
		*/
		?>
		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
  <td>
  <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
  <tr>
    <td height="10" align="center" valign="bottom" bgcolor="#E9EEF4" style="border-bottom:1px solid #c1d1e1;border-top:1px solid #c1d1e1;"><img src="images/templates/header_bar_grad2.jpg" alt=""></td>
  </tr>
  <tr>
  <td height="8" align="center" valign="top" style="border-top:1px solid #ffffff;"><img src="images/templates/header_bar_grad3.jpg" alt=""></td>
  </tr>
  </table>
  </td>
  </tr>
 
  <tr>
    <td height="4"></td>
  </tr>
  <tr>
    <td><table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="28" valign="top">
	
	<table width="760" cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td>
	<!-- InstanceBeginEditable name="breadcrumbs" --><a href="/building_details.php" class="crumbs">Your Community</a>&nbsp;>&nbsp;Budgets<!-- InstanceEndEditable -->
	</td>
	<td style="text-align:right;" nowrap>
	<? if(!empty($_SESSION['resident_session'])){?><a href="index.php?logoff=Y" class="crumbs">Log Off</a><? }?>
	</td>
	</tr>
	</table>
	
	</td>
  </tr>
</table>
</td>
  </tr>
  <tr>
    <td><!-- InstanceBeginEditable name="body" -->
      <table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td style="border-bottom:1px solid #003366;"><table width="760" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="15"><img src="images/dblue_box_top_left_corner.jpg" width="15" height="33"></td>
              <td width="730" bgcolor="#416CA0" style="border-top:1px solid #003366; vertical-align:middle"><img src="images/budgets/budgets_symbol.jpg" width="22" height="22" style="vertical-align:middle;">&nbsp;&nbsp;<span class="box_title">Budgets</span></td>
              <td width="15" align="right"><img src="images/dblue_box_top_right_corner.jpg" width="15" height="33"></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td><table width="758" border="0" cellspacing="0" cellpadding="15" style="border-left:1px solid #7ea0bd;border-right:1px solid #7ea0bd;border-top:1px solid #7ea0bd;border-bottom:1px solid #cccccc;">
            <tr>
              <td width="380" valign="top"><table width="349" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td valign="top"><p><b><span class="subt036">Budget Types</span></b>
				          <br>
				          <br>
				      The following should help explain how a service charge budget is prepared and structured. The descriptions below do not relate specifically to your development or Management Company. Each development is unique and  we ensure that the budget created for the associated Management Company is specific to the development and in accordance with the  legal documentation (Lease/Transfer document).
                    </p>
                    </td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
				 <tr>
                  <td><b><span class="text036">Buildings Charge</span></b></td>
                </tr>
				 <tr>
                  <td>A Buildings Charge usually contains the costs associated with maintaining any apartment block(s) on a development, both internally and externally, any internal common parts and the facilities provided. This charge is payable only by those residents with a property within the block(s).  Examples of what a Buildings Charge may cover include: buildings insurance, cleaning of communal areas, cleaning of communal windows, maintenance of any mechanical and electrical equipment serving the block (e.g. door entry system, fire detection/alarm systems etc.), general administrative charges and a reserve fund for longer term maintenance, redecoration and repairs to the block.</td>
                </tr>
				 <tr>
				   <td>&nbsp;</td>
				   </tr>
				 <tr>
				   <td><b><span class="text036">Estate Charge</span></b></td>
				   </tr>
				 <tr>
				   <td>An Estate Charge usually contains the costs associated with maintaining any general 'Estate' areas on a development (i.e. the communal grounds), from which all units on the development derive a benefit. This can range from a private estate road to a large open space or play area. A contribution to this charge is usually (but not always) made by all units on the development or within the Management Company. By way of example, this charge would normally cover: gardening/sweeping costs, minor repairs, general administrative expenses, public liability insurance and a contribution to a reserve fund for longer term maintenance and repairs. Not all budgets will require a separate Estate Charge; a separate estate charge is usually (but not always) provided where there is a mix of different unit types, i.e. apartments and houses.</td>
				   </tr>
				 <tr>
				   <td>&nbsp;</td>
				   </tr>
				 <tr>
				   <td>&nbsp;</td>
				   </tr>
				 <tr>
				   <td><b><span class="subt036">Some Helpful notes about your contribution to the Budget for <?=$rmc->rmc['rmc_name']?></span></b></td>
				   </tr>
				 <tr>
				   <td>&nbsp;</td>
				   </tr>
				 <tr>
				   <td><b>Parking Charge</b></td>
				   </tr>
				 <tr>
				   <td>Where there are insufficient or limited  parking spaces on a development, there may be a separate parking charge payable by only those residents that  benefit from having a space. A parking charge would usually cover costs associated with maintaining the parking areas on the development and include:
					<ul>
					<li>An element of the sweeping costs</li>
					<li>Minor repairs</li>
					<li>Maintenance any entrance gates, barriers, shutters etc</li>
					<li>General administrative costs</li>
					<li>Contribution to a reserve fund for longer term maintenance/repairs in this area</li>
					</ul></td>
				   </tr>
				 <tr>
				   <td>&nbsp;</td>
				   </tr>
				 <tr>
				   <td><b>Areas / Heads of Charge</b></td>
				   </tr>
				 <tr>
				   <td>On certain developments, units (flats and houses etc.) may enjoy the benefit of a shared facility e.g. a communal garden. In this case the budget is structured by having various  'heads of charge' , with each unit paying a proportion of the charge associated with maintaining this facility.</td>
				   </tr>
				 <tr>
				   <td>&nbsp;</td>
				   </tr>
				 <tr>
				   <td><b>Your contribution to the Budget</b></td>
				   </tr>
				 <tr>
                  <td>It should be noted that the individual contribution by a resident to the gross service charge may  be calculated in a variety of ways and will be set out within the legal documentation (Lease) provided when purchasing the property. The charge may be divided equally amongst all units making an equal proportion of one (or more) of the above mentioned charges; it may be based on the size of the unit or even on the number of bedrooms a property has. The individual proportion will usually have been defined prior to the development of the unit.</td>
                </tr>
              </table></td>
              <td valign="top" background="images/budgets/woman_calculator.jpg" bgcolor="#F5F7FB" style="border-left:1px solid #eaeaea;background-repeat:no-repeat;"><p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <table width="348" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><b><span class="subt036">Budget Documents</span></b><br><br>
      Below are the specific Budget documents that relate to your Management Company.
      </td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    </tr>
	<? if($num_budgets > 0){?>
  <tr>
    <td>
	
	<table width="347" border="0" cellspacing="0" cellpadding="2">
      <tr>
        <td width="139" bgcolor="#f1f1f1" style="border-left:1px solid #cccccc;border-top:1px solid #cccccc;border-bottom:1px solid #cccccc;">&nbsp;<strong><span class="text036">Budget for period</span></strong></td>
        <td width="208" bgcolor="#f1f1f1" style="border-right:1px solid #cccccc;border-top:1px solid #cccccc;border-bottom:1px solid #cccccc;">&nbsp;<strong><span class="text036">&nbsp;Budget Type</span></strong></td>
      </tr>
	  <?
		  while($row_budget = @mysql_fetch_array($result_budget)){
			?>
			<tr>
			<td height="20" bgcolor="#ffffff" style="border-bottom:1px solid #eaeaea;border-left:1px solid #cccccc;">&nbsp;<?=substr($row_budget['budget_from'],6,2)."/".substr($row_budget['budget_from'],4,2)."/".substr($row_budget['budget_from'],2,2)." - ".substr($row_budget['budget_to'],6,2)."/".substr($row_budget['budget_to'],4,2)."/".substr($row_budget['budget_to'],2,2)?></td>
			<td bgcolor="#ffffff" style="border-bottom:1px solid #eaeaea;border-right:1px solid #cccccc;">&nbsp;&nbsp;<a href="<?=$UTILS_HTTP_ADDRESS?>show_file.php?type=budget&id=<?=$row_budget['budget_file_stamp']?>&rmc_num=<?=$_SESSION['rmc_num']?>" target="_blank" class="link416CA0" style="text-decoration:underline "><?=$row_budget['budget_type_name']?></a>&nbsp;<?="(".get_file_size($UTILS_PRIVATE_FILE_PATH."budgets/".$row_budget['budget_file']).")"?></td>
			</tr>
			<? }?>
	  </table>
	
	</td>
  </tr>
  <?
  }else{
	  ?>
	  <tr>
	  <td height="30">No budgets have been uploaded for your Management Company. Please contact our Property Management department on <?=$UTILS_TEL_MAIN?> if you need to obtain a copy of this years budget.</td>
	  </tr>
	  <?
	  }
	?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr><td><table width="344" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="98" style="vertical-align:middle;"><a href="<?=$UTILS_ADOBE_URL?>" target="_blank"><img src="images/getacrobat.gif" alt="Get Adobe Reader" width="88" height="31" border="0" style="vertical-align:middle;"></a></td>
      <td width="250" style="vertical-align:middle;">These documents can be viewed using Adobe Reader. <a href="<?=$UTILS_ADOBE_URL?>" target="_blank">Click to download</a>. </td>
    </tr>
  </table></td></tr>
</table>
</td>
              </tr>
          </table></td>
        </tr>
		<tr><td height="5" bgcolor="#416CA0" style="border:1px solid #003366;"><img src="images/spacer.gif" alt=""></td>
		</tr>
      </table>
    <!-- InstanceEndEditable --></td>
  </tr>
  <tr>
    <td height="25"></td>
  </tr>
  <tr>
    <td><table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="445" height="14"><a href="about_us.php" class="link416CA0">About RMG Living</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="contact_us.php" class="link416CA0">Contact Us</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="privacy.php" class="link416CA0">Legal Information</a></td>
        <td width="315" align="right">Copyright <?=date("Y", time())?> &copy; <a href="http://www.rmgltd.co.uk">Residential Management Group Ltd</a></td>
      </tr>
	  <tr><td colspan="2">&nbsp;</td></tr>
	  <tr><td colspan="2" style="text-align:left;"><p>&nbsp;</p>
	    </td>
	  </tr>
    </table></td>
  </tr> 
  <tr><td>&nbsp;</td></tr>
</table>
</body>
<!-- InstanceEnd --></html>
