<?
session_start();

//=======================================
// Connect to the DB & set global vars
//=======================================

if( preg_match("/Sites\\\\rmgliving.co.uk/", $_SERVER['PATH_TRANSLATED']) ){

    $site_locality 				= "remote";
    $dbname 					= "cpm_living";
    $UTILS_INTRANET_DB_LINK 	= @mysql_connect("localhost:3307","remote","d3vmy5ql");
    @mysql_select_db("intranet", $UTILS_INTRANET_DB_LINK);


    $UTILS_HTTP_ADDRESS 		= "http://".$_SERVER['HTTP_HOST'].'/';
    $UTILS_HTTPS_ADDRESS = "http://" . $_SERVER['HTTP_HOST'].'/';
    $UTILS_PAYPOINT_HTTPS_ADDRESS = "http://".$_SERVER['HTTP_HOST'];

    $UTILS_FILE_PATH = "D:/sites/rmgliving.co.uk/httpdocs/";
    $UTILS_PRIVATE_FILE_PATH = "D:/sites/rmgliving.co.uk/httpdocs/library/private/";
    $UTILS_INS_SCHED_PATH = "D:/sites/rmgliving.co.uk/private/insur_sched/";
    $UTILS_SITE_VISITS_PATH = "D:/sites/rmgliving.co.uk/private/site_visits/";
    $UTILS_MEETING_NOTES_PATH = "D:/sites/rmgliving.co.uk/private/meeting_notes/";
    $UTILS_WEBROOT = "/";
    $UTILS_SERVER_PATH = "D:/sites/rmgliving.co.uk/httpdocs/";
    $UTILS_TEMPLATE_PATH = "D:/sites/rmgliving.co.uk/httpdocs/library/templates/";
    $UTILS_CLASS_PATH = "D:/sites/rmgliving.co.uk/httpdocs/library/classes/";
    $UTILS_ECS_EXPORT_PATH = "D:\\sites\\rmgliving.co.uk\\private\\ecs_export\\";

}elseif( preg_match("/Sites\\\\stage.rmgliving.co.uk/", $_SERVER['PATH_TRANSLATED']) ){

    $site_locality 				= "local";
    $dbname 					= "cpm_living_dev";

    $UTILS_INTRANET_DB_LINK = @mysql_connect("localhost:3308","remote","d3vmy5ql");
    @mysql_select_db("intranet", $UTILS_INTRANET_DB_LINK);
    $server_name = "stage.rmgliving.co.uk";
    $UTILS_FILE_PATH = "D:/sites/".$server_name."/httpdocs/";
    $UTILS_PRIVATE_FILE_PATH = "D:/sites/".$server_name."/httpdocs/library/private/";
    $UTILS_INS_SCHED_PATH = "D:/sites/".$server_name."/private/insur_sched/";
    $UTILS_SITE_VISITS_PATH = "D:/sites/".$server_name."/private/site_visits/";
    $UTILS_MEETING_NOTES_PATH = "D:/sites/".$server_name."/private/meeting_notes/";
    $UTILS_WEBROOT = "/";
    $UTILS_TEMPLATE_PATH = "D:/sites/".$server_name."/httpdocs/library/templates/";
    $UTILS_CLASS_PATH = "D:/sites/".$server_name."/httpdocs/library/classes/";
    $UTILS_SERVER_PATH = "D:/sites/".$server_name."/httpdocs/";
    $UTILS_HTTP_ADDRESS = "http://".$server_name.'/';
    $UTILS_HTTPS_ADDRESS = "http://".$server_name.'/';
    $UTILS_PAYPOINT_HTTPS_ADDRESS = "http://".$server_name;
    $UTILS_ECS_EXPORT_PATH = "D:\\sites\\".$server_name."\\private\\ecs_export\\";

}


$dbhost 		= "localhost";
$dbuser 		= "cpmlivingmysql";
$dbpwd 			= "cpml1v1n9my5ql";
$dblink_id 		= @mysql_connect($dbhost, $dbuser, $dbpwd);
@mysql_select_db($dbname, $dblink_id);


//=========================
// Site-wide constants
//=========================
$UTILS_DB_ENCODE 			= "4Ri9hsjl";
$UTILS_LOGIN_REQUEST_EMAIL 	= "loginrequests@rmgliving.co.uk";
$UTILS_ADMIN_EMAIL 			= "itservicedesk@rmguk.com";
$UTILS_CCC_EMAIL			= "customerservice@rmguk.com";
$UTILS_FAS_EMAIL			= "customerservice@fspropertymanagement.co.uk";
$UTILS_IMPORT_EMAIL 		= "webteam@rmguk.com";
$UTILS_ADOBE_URL 			= "http://get.adobe.com/reader";
$UTILS_IP_ADDRESS 			= '94.250.233.138';
$thistime 					= time();

$UTILS_TEL_MAIN = '0345 002 4444';
$UTILS_TEL_MAIN_FAX = '0345 002 4455';

?>