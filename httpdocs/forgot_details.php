<?
require("utils.php");
require_once($UTILS_CLASS_PATH."login.class.php");
require_once($UTILS_CLASS_PATH."data.class.php");
require_once($UTILS_FILE_PATH."library/securimage/securimage.php");	 
$login = new login;
$data = new data;
$securimage = new Securimage();


// Do reminder
if($_REQUEST['a'] == "s"){
	
	$save_result = $login->forgot_check_fields($_POST, $securimage);
	if($save_result[0] === true){
		
		$save_result = $login->do_forgot($_POST);
	}
	else{
		$save_result = $save_result[1];	
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>RMG Living - Forgotten Details</title>
	<link href="/css/reset.css" rel="stylesheet" type="text/css" />
	<!--<link href="styles.css" rel="stylesheet" type="text/css">-->
	<link href="/css/common.css" rel="stylesheet" type="text/css" />
	<!--[if lte IE 8]> 
	<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<!--[if lte IE 7]> 
	<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
	<![endif]-->

	<script type="text/javascript" src="library/jscript/jquery-1.4.2.min.js"></script>
	<script type="text/JavaScript">
	//<!--
	function do_forgot(){
		document.form1.submit();
	}

	function toggle_up(){

		if( $("#up_type_2").attr("checked") == true ){
			$("#username_row").hide();
			$("#password_row").show();
		}
		else{
			$("#username_row").show();
			$("#password_row").hide();
		}
	}

	function toggle_account_types(){

		if( $("#account_type_2").attr("checked") == true ){
			$("#lessee_row").hide();
			toggle_up();
			$("#email_row").show();
			$("#up_block").show();
		}
		else if( $("#account_type_1").attr("checked") == true ){
			$("#lessee_row").show();
			$("#username_row").hide();
			$("#password_row").hide();
			$("#email_row").show();
			$("#up_block").hide();
		}else{
			$("#lessee_row").hide();
			$("#username_row").hide();
			$("#password_row").hide();
			$("#email_row").show();
			$("#up_block").hide();
		}
	}

	$(document).ready(function() {
		$("#account_type_1").attr("checked", 'checked');
		toggle_account_types();
	});

	//-->
	</script>
	<? require_once($UTILS_FILE_PATH."includes/analytics.php");?>
</head>
<body>

	<div id="wrapper">
		
		<? require_once($UTILS_FILE_PATH."includes/header.php");?>
		
		<div id="content">
			
			<table width="760" cellspacing="0">
				<tr>
					<td><a href="index.php" class="crumbs">Home</a>&nbsp;>&nbsp;Forgotten Details</td>
					<td style="text-align:right;" nowrap="nowrap">&nbsp;</td>
				</tr>
			</table>
						
			<table width="760" cellspacing="0" style="clear:both; margin-top:13px;" id="content_box_1_top">
				<tr>
					<td width="15"><img src="/images/dblue_box_top_left_corner.jpg" width="15" height="33" /></td>
					<td width="730" style="border-top:1px solid #003366; background-color:#416CA0; vertical-align:middle">&nbsp;</td>
					<td width="15" style="text-align:right;"><img src="/images/dblue_box_top_right_corner.jpg" width="15" height="33" /></td>
				</tr>
			</table>	
			
			<div class="content_box_1" style="min-height:300px;">
			
				<form action="<?=$UTILS_HTTPS_ADDRESS?>forgot_details.php" name="form1" id="form1" method="post">
					<input type="hidden" name="a" id="a" value="s">
				
					<h4>Forgotten Details?</h4>
					<p style="width:370px;"></p>
						
					<?
					if($_REQUEST['a'] == "s" && $save_result !== true){
						?>
						<p class="msg_fail" style="margin-top:20px;margin-bottom:20px;"><?=$save_result?></p>
						<?
					}
					if($_REQUEST['a'] == "s" && $save_result === true){
						?>
						<p class="msg_success" style="margin-top:20px;margin-bottom:20px;">An email containing your login details has been sent to your email account.</p>
						<?
					}
					?>
				
					
					<p>Ooops, so you've forgotten your login details ... Ok, firstly we just need to establish which type of account you are using to access RMG Living - this will either be a 'Normal' residential account or a 'Master Account'.</p>
					<br />
					<p>If you are a living in a rented residence, are a Resident Director or own a single property you are likely to be accessing RMG Living using a 'Normal' account. If you have created a Master Account to manage multiple-properties on RMG Living, please select 'Master Account'.</p>
					<br />
					<p>Choose which type of account you use to access RMG Living:</p>
					
					<table class="table_3" width="770" cellspacing="0">
						<tr>
							<td style="width:140px;"><input onclick="toggle_account_types()" type="radio" name="account_type" id="account_type_1" value="normal" <? if($_REQUEST['account_type'] == "normal"){?>checked="checked"<? }?> />&nbsp;Normal Account</td>
							<td style="width:140px;"><input onclick="toggle_account_types()" type="radio" name="account_type" id="account_type_2" value="master" <? if($_REQUEST['account_type'] == "master"){?>checked="checked"<? }?> />&nbsp;Master Account</td>
							<td><input onclick="toggle_account_types()" type="radio" name="account_type" id="account_type_3" value="contractor" <? if($_REQUEST['account_type'] == "contractor"){?>checked="checked"<? }?> />&nbsp;Contractor</td>
						</tr>
					</table>
					
					<div id="up_block" style="margin-top:15px; clear:both;">
						<p>Which part of your login details do you want to retreive ?</p>
						<table class="table_3" width="770" cellspacing="0">
							<tr>
								<td style="width:140px;"><input onclick="toggle_up()" type="radio" name="up_type" id="up_type_1" value="password" <? if($_REQUEST['up_type'] != "username"){?>checked="checked"<? }?> />&nbsp;Password</td>
								<td><input onclick="toggle_up()" type="radio" name="up_type" id="up_type_2" value="username" <? if($_REQUEST['up_type'] == "username"){?>checked="checked"<? }?> />&nbsp;Username</td>
							</tr>
						</table>
					</div>
					
					<div style="width:370px;margin-top:30px;margin-bottom:20px;">
						<p>Enter your details into the fields below.</p>					
						<table class="table_3" cellspacing="0" style="margin-top:5px;">
							<tr id="lessee_row">
								<td style="width:110px;">Lessee Id:</td>
								<td><input type="text" name="lessee_id" id="lessee_id" value="<?=$_REQUEST['lessee_id']?>" size="20" maxlength="30" autocomplete="off" /></td>
							</tr>
							<tr id="username_row">
								<td style="width:110px;">Username:</td>
								<td><input type="text" name="username" id="username" value="<?=$_REQUEST['username']?>" size="20" maxlength="30" autocomplete="off" /></td>
							</tr>
							<tr id="password_row">
								<td style="width:110px;">Password:</td>
								<td><input type="password" name="password" id="password" value="<?=$_REQUEST['password']?>" size="20" maxlength="30" autocomplete="off" /></td>
							</tr>
							<tr id="email_row">
								<td style="width:110px;">Email address:</td>
								<td><input type="text" name="email" id="email" value="<?=$_REQUEST['email']?>" size="40" maxlength="100" autocomplete="off" /></td>
							</tr>
						</table>
					</div>
					
					
					<div style="position:relative;left:-3px;margin-bottom:20px; margin-top:30px;">
						<p style="margin-bottom:12px;">Enter the characters that displayed below into the box provided. These characters are case-sensitive.</p>
						
						<!-- CAPTCHA facility -->
						<div>
							<input type="text" name="captcha_code" size="10" maxlength="6" />&nbsp;[&nbsp;<a href="#" onclick="document.getElementById('captcha').src = '/library/securimage/securimage_show.php?' + Math.random(); return false">Change Image</a>&nbsp;]
						</div>
						<img id="captcha" src="/library/securimage/securimage_show.php" alt="CAPTCHA Image" style="clear:both;margin-top:15px;" />

					</div>
					
					
					<a href="Javascript:;" onClick="do_forgot();return false;" ><img src="images/go_button.jpg" border="0"></a>
				
					
				</form>
			
			</div>			
			
		</div>
		
		<? require_once($UTILS_FILE_PATH."includes/footer.php");?>
		
	</div>

</body>
</html>
