<?
require("utils.php");

header("Location: /op/paypoint/r1_norm.php");
exit;

require($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."security.class.php");
$security = new security;
$website = new website;

// Determine if allowed access into 'your community' section
$website->allow_community_access();

// Get details of transaction
$sql = "SELECT * FROM cpm_orders WHERE order_ref='".$security->clean_query($_REQUEST['oid'])."'";
$result = @mysql_query($sql);
$row = @mysql_fetch_array($result);

?>
<html>
<head>
<title>RMG Living - Make a Payment</title>
<link href="../styles.css" type="text/css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<form>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
				  <td rowspan="2" valign="top" bgcolor="#5589B8" style="border-right:1px solid #cccccc;"><img src="../images/make_a_payment/online_payment_side_bar.jpg" width="122" height="450"></td>
				  <td width="100%" valign="top" style="padding-right:8px;padding-bottom:12px;padding-top:8px;padding-left:18px;border-bottom:1px solid #cccccc;"><table cellpadding="6" cellspacing="0" border="0" width="100%">
                    <?=$message?>
                    <tr>
                      <td bgcolor="#FFFFFF"><img src="../images/make_a_payment/online_payments.gif"></td>
                    </tr>
                    <tr>
                      <td valign="top" bgcolor="#FFFFFF">
					  	<p><br>
						<?
						// Display warning if problem with Barclays System
						if(empty($row['order_transactionstatus'])){
						
							
						}
						else{
						
							//==============================================
							// Show success/failure message for transaction
							//==============================================
							if(preg_match("/Success/i", $row['order_transactionstatus']) !== 1){
							
								# Determine order status message
								switch($row['order_transactionstatus']){
									case "0" :	$status_msg = "This card does not support authentication.";break;
									case "1" :	$status_msg = "This card has not been enrolled in Internet Authentication. Please contact you card issuer for more details.";break;
									case "2" :	$status_msg = "Authentication successful.";break;
									case "3" :	$status_msg = "Unsuccessful Internet Authentication. Please see below for possible reasons.";break;
									case "4" :	$status_msg = "Unknown. Please see below for possible reasons.";break;
									case "5" :	$status_msg = "BIN range is not enrolled. Please contact you card issuer for more details.";break;
									case "6" :	$status_msg = "Either your card issuer or cardholder are not enrolled in Internet Authentication. Please try paying with an alternative credit/debit card, or contact your card issuer for more details.";break;
									default	 :	$status_msg = "Unknown";break;
								}
								
								if($row['order_ref'] == ""){
									$row['order_ref'] = "None";
								}
								print "<img src=\"../images/help_16.gif\" align=\"absmiddle\">&nbsp;<font color=\"#cc0000\">Unfortunately your payment request could not be completed.</font><br><br><strong>Reason:</strong> ".$status_msg."<br><br><strong>Order ref:</strong> ".$row['order_ref'];
								
								?>
								
								<p>
								If your transaction was unsuccessful, it may be due to one of the following reasons:
								<ul>
								<li>Unusually high transaction amount for this card</li>
								<li>Address details entered does not match those that the card was registered with</li>
								<li>Cardholder name entered does not match those that the card was registered with</li>
								<li>Card has expired</li>
								<li>The passcode associated with this card is not being entered correctly</li>
								</ul>
								</p>
								
								<?
								
							}
							else{
								print "<font color=\"#669966\">Your payment was successful.</font> A receipt of the transaction has been sent to the email address that you supplied. Please make a note of the order reference below.<br><br><strong>Order ref:</strong> ".$row['order_ref'];
							}
						
						}
						?>
						<br>
						</p>
					  </td>
                    </tr>
                  </table></td>
			  </tr>
				<tr>
				  <td height="100%" valign="top" bgcolor="#ECF0F4" style="padding-right:8px;padding-bottom:8px;padding-top:12px;padding-left:18px;border-bottom:1px solid #cccccc;">
					
						<table cellpadding="6" cellspacing="0" border="0" width="550">
							<tr>
							  <td colspan="2"><p>
							  <? if(preg_match("/SUCCESS/i", $row['order_transactionstatus']) === 1){?>
							  If you wish to make a further payment, <strong><a href="<?=$UTILS_HTTPS_ADDRES?>op/payment.php">click here</a></strong>.<br>
						      <? }?>
						         </p>
						      </td>
						  </tr>
							<tr>
							  <td colspan="2"><a href="#" onClick="window.close();" style="text-decoration:underline;">Click here to close this window</a></td>
						  </tr>
							<tr>
							  <td width="27%">&nbsp;</td>
						      <td width="73%" align="right">&nbsp;</td>
							</tr>
							<tr>
							  <td nowrap>&nbsp;</td>
							  <td align="right">&nbsp;</td>
						  </tr>
							<tr>
							  <td nowrap>&nbsp;</td>
							  <td align="right">&nbsp;</td>
						  </tr>
							<tr>
							  <td>&nbsp;</td>
						    <td align="right">&nbsp;</td>
							</tr>
							<tr>
							  <td colspan="2" height="15"></td>
						  </tr>
							<tr>
							  <td>&nbsp;</td>
							  <td align="right">&nbsp;</td>
							</tr>
							<tr align="right">
							  <td colspan="2">&nbsp;</td>
						  </tr>
							<tr>
							  <td>&nbsp;</td>
							  <td align="right">&nbsp;</td>
						  </tr>
							<tr>
							  <td>&nbsp;</td>
							  <td align="right">&nbsp;</td>
						  </tr>
							<tr>
							  <td>&nbsp;</td>
							  <td align="right">&nbsp;</td>
						  </tr>
							<tr>
							  <td>&nbsp;</td>
							  <td align="right">&nbsp;</td>
						  </tr>
							<tr>
							  <td>&nbsp;</td>
							  <td align="right">&nbsp;</td>
						  </tr>
							<tr>
							  <td>&nbsp;</td>
							  <td align="right">&nbsp;</td>
						  </tr>
							<tr>
							  <td>&nbsp;</td>
							  <td align="right">&nbsp;</td>
						  </tr>
							<tr>
							  <td>&nbsp;</td>
							  <td align="right">&nbsp;</td>
						  </tr>
							<tr>
							  <td>&nbsp;</td>
							  <td align="right">&nbsp;</td>
						  </tr>
							<tr>
							  <td>&nbsp;</td>
							  <td align="right">&nbsp;</td>
						  </tr>
							<tr>
							  <td>&nbsp;</td>
							  <td align="right">&nbsp;</td>
						  </tr>
							
						</table>
						
					 
				  </td>
				</tr>
</table>
</form>
</body>
</html>