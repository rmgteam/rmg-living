<?
require_once("utils.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."order.class.php");
$website = new website;
$order = new order;


// Determine if allowed access into 'your community' section
$website->allow_community_access();


// Get total + charge
if( $_REQUEST['a'] == "gc" ){
	
	$subtotal = str_replace(",", "", $_REQUEST['subtotal']);
	$result_array = array();
	$charge = $order->calc_payment_charge($_REQUEST['card_type'], $_REQUEST['subtotal']);
	
	$result_array['charge'] = $charge;
	$result_array['total'] = $subtotal + $charge;
	
	echo json_encode($result_array);
	exit;
}


// Makes sure that values are zero if they are negative
if($_REQUEST['service_charge'] < 0){$_REQUEST['service_charge'] = 0;}
if($_REQUEST['ground_rent'] < 0){$_REQUEST['ground_rent'] = 0;}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>RMG Living - Make a Payment</title>
	<link href="/css/reset.css" rel="stylesheet" type="text/css" />
	<link href="styles.css" rel="stylesheet" type="text/css" />
	<link href="/css/common.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="/css/custom-theme/jquery-ui-1.8.16.custom.css" />
	<!--[if lte IE 8]> 
	<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<!--[if lte IE 7]> 
	<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<script type="text/javascript" src="/library/jscript/jquery-1.6.2.min.js"></script>
	<script type="text/javascript" src="/library/jscript/jquery-ui-1.8.16.custom.min.js"></script>
	<script type="text/javascript" src="/library/jscript/functions/format_as_money.js"></script>
	<script type="text/javascript">
	function make_payment(){
		if( $('#card_type').val() == "" ){alert("Please select the type of Card you are paying with.");return;}
		document.form1.action = "<?=$UTILS_HTTPS_ADDRESS?>op/paypoint/s1_norm.php";
		document.form1.submit();
	}
	function go_to_step_1(){
		
		document.form1.action = "<?=$UTILS_HTTPS_ADDRESS?>op/paypoint/step1.php";
		document.form1.submit();
	}
	function calc_charges(){
		$('#payment_button').hide();
		var charge=0, total=0;
		
		if($('#card_type').val() == ""){
			
			$('#total').val("0.00");
			$('#charge').val("0.00");
			$('#chargeTXT').html("0.00");
			$('#totalTXT').html("0.00");
		}
		else{

			if($('#subtotal').val() != ""){
				$('#pay_loading').show();
				$.ajax({
					url: "/op/paypoint/payment.php",
					data: { a:'gc' , subtotal: $('#subtotal').val(), card_type: $('#card_type').val() },
					dataType: 'json',
					type: 'POST',
					success: function(data){
						
						if(data){
							
							$('#chargeTXT').html( format_as_money( data['charge'] ) );
							$('#charge').val( format_as_money( data['charge'] ) );
							$('#totalTXT').html( format_as_money( data['total'] ) );
							$('#total').val( format_as_money( data['total'] ) );
							$('#payment_button').show();
							$('#pay_loading').hide();
						}
					},
					error: function(){
					
						$('#total').val("0.00");
						$('#charge').val("0.00");
						$('#chargeTXT').html("0.00");
						$('#totalTXT').html("0.00");
						$('#card_type').val("");
						
						alert("There was a problem contacting the server. Please try again.");
					}
				});
			}
		}		
	}
		
	$(document).ready(function() {
		$('#payment_button').hide();
		calc_charges();
	});
	</script>
</head>
<body>

	<div id="wrapper">
	
		<? require_once($UTILS_FILE_PATH."includes/header.php");?>
		<? require_once($UTILS_FILE_PATH."includes/msg_dialog.php");?>
		<? require_once($UTILS_FILE_PATH."includes/freeholder_dialog.php");?>
		
		<div id="content">
		
			<table width="760" cellspacing="0">
				<tr>
					<td><a href="/building_details.php" class="crumbs">Your Community</a>&nbsp;&gt;&nbsp;Make a Payment</td>
					<td style="text-align:right;" nowrap="nowrap"><? if(!empty($_SESSION['resident_session'])){?><a href="/index.php?logoff=Y" class="crumbs">Log Off</a><? }?></td>
				</tr>
			</table>

				
			<table width="760" cellspacing="0" cellpadding="0" style="margin-top:12px;">
				<tr>
					<td width="15"><img src="/images/dblue_box_top_left_corner.jpg" width="15" height="33" /></td>
					<td width="730" style="background-color:#416CA0;border-top:1px solid #003366; vertical-align:middle"><img src="/images/meetings/meetings_symbol.jpg" width="22" height="22" style="vertical-align:middle;">&nbsp;&nbsp;<span class="box_title">Make a Payment </span></td>
					<td width="15" align="right"><img src="/images/dblue_box_top_right_corner.jpg" width="15" height="33" /></td>
				</tr>
			</table>
						
			
			<div class="content_box_1 clearfix" style="padding:0;width:758px; vertical-align:top;">
								
				<div id="content_left" style="width:726px;padding:15px; min-height:350px; vertical-align:top;">
					
					<form action="<?=$UTILS_HTTPS_ADDRESS?>op/paypoint/payment.php" method="POST" name="form1" id="form1">
					
						<input type="hidden" name="charge" id="charge" />
						<input type="hidden" name="ground_rent" id="ground_rent" value="<?=$_REQUEST['ground_rent']?>" />
						<input type="hidden" name="service_charge" id="service_charge" value="<?=$_REQUEST['service_charge']?>" />
						<input type="hidden" name="subtotal" id="subtotal" value="<?=$_REQUEST['subtotal']?>" />
						<input type="hidden" name="payment_plan" id="payment_plan" value="<?=$_REQUEST['payment_plan']?>" />
						<input type="hidden" name="total" id="total" />


						<p>An additional handling charge is made for using this online facility. In order for this charge to be calculated, please select the type of card you are using below. This charge will be included into the Total amount. Once you are ready to pay, please click the 'Pay' button.<br></p>
	
	
						<table cellspacing="0" width="550" style="margin-top:20px;">
							<tr>
								<td style="padding-top:5px;" width="155">Card Type * </td>
								<td style="padding-top:5px;">
									<select name="card_type" id="card_type" onchange="calc_charges();">
										<option value="">-</option>
										<?
										$result = $order->get_order_card_type_list();
										while($row = @mysql_fetch_array($result)){
											?>
											<option value="<?=$row['order_card_type_id']?>"><?=stripslashes($row['order_card_type_label'])?></option>
											<?
										}
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td style="padding-top:5px;">Amount to pay </td>
								<td style="padding-top:5px;font-weight:bold;">&pound;<span id="subtotalTXT"><?=$_REQUEST['subtotal']?></span></td>
							</tr>
							<tr>
								<td style="padding-top:5px;">Handling charges </td>
								<td style="padding-top:5px;font-weight:bold;">&pound;<span id="chargeTXT">0.00</span></td>
							</tr>
						</table>
						
							
						<div style="margin-top:20px;margin-bottom:20px;">
							<table cellspacing="0" width="296">
								<tr>
									<td width="138" style="vertical-align:top;padding:7px 5px;background-color:#CED9E3;border-left:1px solid #999999;border-top:1px solid #999999;border-bottom:1px solid #999999;font-weight:normal; font-size:16px;">To Pay</td>
									<td style="vertical-align:top;padding:7px 5px;background-color:#CED9E3;border-right:1px solid #999999;border-top:1px solid #999999;border-bottom:1px solid #999999;font-weight:bold; font-size:16px;">&pound;<span id="totalTXT">0.00</span></td>
								</tr>
							</table>
						</div>
						
						
						<p>Upon clicking 'Pay' you will be re-directed to the SecPay's Paypoint Online Payment System to complete your transaction. Payments should be reflected on your statement of account within 48 hours. As RMG's offices are open weekdays only, please allow extra time if paying over the weekend.</p>
						
						
						<div style="clear:both; margin-top:80px;">
							<a href="Javascript:;" style="float:left;" onclick="go_to_step_1();return false;"><img src="/images/make_a_payment/back.png" border="0"></a>
							<div style="float:right;"><img id="pay_loading" style="display:none;" src="/images/ajax-loader.gif" /><a id="payment_button" href="Javascript:;" onclick="make_payment();return false;"><img src="/images/make_a_payment/pay.png" border="0"></a></div>
						</div>

					
					</form>
					
				</div>
			
			</div>

		</div>
	
		<? require_once($UTILS_FILE_PATH."includes/footer.php");?>

	</div>

</body>
</html>
