<?
require_once("utils.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."resident.class.php");
$resident = new resident($_SESSION['resident_num']);
$website = new website;
Global $UTILS_TEL_MAIN;
// Determine if allowed access into 'your community' section
$website->allow_community_access();

// Makes sure that values are zero if they are negative
if($_REQUEST['service_charge'] < 0){$_REQUEST['service_charge'] = 0;}
if($_REQUEST['ground_rent'] < 0){$_REQUEST['ground_rent'] = 0;}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>RMG Living - Make a Payment</title>
	<link href="/css/reset.css" rel="stylesheet" type="text/css" />
	<link href="styles.css" rel="stylesheet" type="text/css" />
	<link href="/css/common.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="/css/custom-theme/jquery-ui-1.8.16.custom.css" />
	<!--[if lte IE 8]> 
	<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<!--[if lte IE 7]> 
	<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<script type="text/javascript" src="/library/jscript/jquery-1.6.2.min.js"></script>
	<script type="text/javascript" src="/library/jscript/jquery-ui-1.8.16.custom.min.js"></script>
	<script type="text/javascript" src="/library/jscript/functions/format_as_money.js"></script>
	<script type="text/javascript">
	function calc_total(){
		
		if($('#ground_rent').length > 0 && $('#service_charge').length > 0){
			
			if($('#ground_rent').val() == ""){$('#ground_rent').val("0.00");}
			if($('#service_charge').val() == ""){$('#service_charge').val("0.00");}	
			
			var gr = $('#ground_rent').val();
			var sc = $('#service_charge').val();
			var total = parseFloat( gr.replace(/,/g,"") ) + parseFloat( sc.replace(/,/g,"") );
			total = format_as_money(total);
			$('#subtotal').val( total );
			$('#subtotalTXT').html( total );
		}
	}
	
	function go_to_charges(){
		
		if($('#ground_rent').length > 0 && $('#service_charge').length > 0){
		
			if( ( $('#ground_rent').val() == 0 && $('#service_charge').val() == 0 ) || ( $('#ground_rent').val() == "" && $('#service_charge').val() == "") ){	
				alert("Please complete the amount(s) that you are paying for this transaction.");
			}
			else{
				document.form1.submit();
			}
		}
	}
	
	$(document).ready(function() {
	
		calc_total();			
	});
	</script>
</head>
<body>

	<div id="wrapper">
	
		<? require_once($UTILS_FILE_PATH."includes/header.php");?>
		<? require_once($UTILS_FILE_PATH."includes/msg_dialog.php");?>
		<? require_once($UTILS_FILE_PATH."includes/freeholder_dialog.php");?>
		
		<div id="content">
		
			<table width="760" cellspacing="0">
				<tr>
					<td><a href="/building_details.php" class="crumbs">Your Community</a>&nbsp;&gt;&nbsp;Make a Payment</td>
					<td style="text-align:right;" nowrap="nowrap"><? if(!empty($_SESSION['resident_session'])){?><a href="/index.php?logoff=Y" class="crumbs">Log Off</a><? }?></td>
				</tr>
			</table>

				
			<table width="760" cellspacing="0" cellpadding="0" style="margin-top:12px;">
				<tr>
					<td width="15"><img src="/images/dblue_box_top_left_corner.jpg" width="15" height="33" /></td>
					<td width="730" style="background-color:#416CA0;border-top:1px solid #003366; vertical-align:middle"><img src="/images/meetings/meetings_symbol.jpg" width="22" height="22" style="vertical-align:middle;">&nbsp;&nbsp;<span class="box_title">Make a Payment </span></td>
					<td width="15" align="right"><img src="/images/dblue_box_top_right_corner.jpg" width="15" height="33" /></td>
				</tr>
			</table>
						
			
			<div class="content_box_1 clearfix" style="padding:0;width:758px; vertical-align:top;">
								
				<div id="content_left" style="width:726px;padding:15px; min-height:350px; vertical-align:top;">
					
					<form action="<?=$UTILS_HTTPS_ADDRESS?>op/paypoint/payment.php" method="POST" name="form1" id="form1">
					
						<input type="hidden" name="subtotal" id="subtotal" value="<?=$_REQUEST['subtotal']?>">
						<input type="hidden" name="payment_plan" id="payment_plan" value="<?=$_REQUEST['payment_plan']?>">

						<?
						if($resident->resident_status == "Debt Collect"){
							?>
							<p style="color:#CC0000;">Online payment is not available at this time. Please contact Customer Services on <?=$UTILS_TEL_MAIN?> .</p>
							<?
						}
						else{
							
							?>
							
							<p style="margin-bottom:10px;">Welcome to the Online Payments facility for Residential Management Group. Please enter below the amount(s) that you wish to pay.</p>
							
							<?
							if( ($_REQUEST['ground_rent'] == "" || $_REQUEST['ground_rent'] == "0") && ($_REQUEST['service_charge'] == "" || $_REQUEST['service_charge'] == "0") ){
								?>
								<p>It appears that you do not need to make a payment to your account at this time.</p>
								<?
							}
							else{
								
								?>
							
								<p style="margin-bottom:5px;">It is advisable that you check your statement before making a payment, to ensure that you are aware of the outstanding balance. Payment made using this site are secure.</p>	
															
								<div style="<? if($_REQUEST['ground_rent'] == '' || $_REQUEST['ground_rent'] == '0'){?>display:none;<? }?>margin-top:20px;border:1px solid #999999;background-color:#ECF0F4; padding:15px; float:left; position:relative; width:450px;">
									<h6 style="margin-top:0;">Ground Rent</h6>
									<p>Please enter the amount of  Ground Rent, if any, that you are paying.</p>
									<div style="margin-top:12px;">
										<span style="font-weight:bold; margin-right:5px; font-size:14px;">&pound;</span><input type="text" name="ground_rent" id="ground_rent" style="font-size:12px; color:#003366; text-align:right; border:1px solid #999; padding:5px; " onblur="$('#ground_rent').val( format_as_money( $('#ground_rent').val() ));calc_total();" onkeyup="calc_total();" value="<?=$_REQUEST['ground_rent']?>" size="9" />
									</div>
								</div>
								
								
								<div style="<? if($_REQUEST['service_charge'] == '' || $_REQUEST['service_charge'] == '0'){?>display:none;<? }?>margin-top:20px;border:1px solid #999999;background-color:#ECF0F4; padding:15px; float:left; position:relative; width:450px;">	
									<h6 style="margin-top:0;">Service Charge</h6>
									<p>Please enter the amount of Service Charge, if any, that you are paying.</p>
									<div style="margin-top:12px;">
										<span style="font-weight:bold; margin-right:5px; font-size:14px;">&pound;</span><input type="text" name="service_charge" <? if($_REQUEST['payment_plan'] == 'N'){?> readonly="readonly" <? }?> id="service_charge" style="font-size:12px; color:#003366; text-align:right; border:1px solid #999; padding:5px;" onblur="$('#service_charge').val( format_as_money( $('#service_charge').val() ));calc_total();" onkeyup="calc_total();" value="<?=$_REQUEST['service_charge']?>" size="9" />
									</div>
								</div>	
								
								
								<div style="margin-top:20px;border:1px solid #999999;background-color:#CED9E3; padding:15px; float:left; position:relative; width:450px;">
									<div style="float:left;font-weight:bold; color:#003366; padding-right:20px; width:200px;">Total amount due</div>
									<div style="float:right;text-align:right; color:#003366; font-weight:bold; font-size:16px; width:200px; letter-spacing:0.03em;">&pound; <span id="subtotalTXT"><? if($_REQUEST['subtotal'] != ""){print $_REQUEST['subtotal'];}else{print "0.00";}?></span></div>
								</div>
								
								<div style="clear:both; margin-top:20px; float:left; position:relative;">
									<p>Payment will be made into the account of your Resident Management Company: <br>
										<span style="font-weight:bold;"><?=$_SESSION['rmc_name']?></span>
									</p>
								</div>
								
								<div style="margin-bottom:20px; clear:both; margin-top:30px; float:right; position:relative;">  
									<a href="Javascript:;" onclick="go_to_charges();return false;"><img src="/images/make_a_payment/next.png" border="0"></a>
								</div>
								
								<?
							}
						}
						?>
					
					</form>
					
				</div>
			
			</div>

		</div>
	
		<? require_once($UTILS_FILE_PATH."includes/footer.php");?>

	</div>

</body>
</html>
