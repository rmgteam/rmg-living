<?


// THIS PAGE TAKES PAYMENTS FOR INSURANCE SCHEDULES FROM rmgliving.co.uk
require("utils.php");

require_once($UTILS_CLASS_PATH."data.class.php");
require_once($UTILS_CLASS_PATH."security.class.php");
require_once($UTILS_CLASS_PATH."resident.class.php");
require_once($UTILS_CLASS_PATH."subsidiary.class.php");
require_once($UTILS_CLASS_PATH."ins_sched_order.class.php");
$resident = new resident($_SESSION['resident_num']);
$ins_sched_order = new ins_sched_order;
$security = new security;
$subsidiary = new subsidiary($resident->subsidiary_id);
$data = new data;


// Determine if allowed access into 'your community' section
if(preg_match("/rmgliving/i", $_SERVER['HTTP_REFERER']) === 1){
	
	require($UTILS_CLASS_PATH."website.class.php");
	$website = new website;
	$website->allow_community_access();
}


// Save order
$price = 30;
$remote_pass = "FP1oZkl36^99Dq"; // Obtained from the Paypoint extranet 
$vat_perc = $ins_sched_order->get_vat_perc();
$vat_amount = number_format( ($price * ($vat_perc/100)),  2, ".", "");
$subtotal = $price + $vat_amount;
$charge = $ins_sched_order->calc_payment_charge($_REQUEST['card_type'], $subtotal);
$charge = number_format($charge, 2, ".", "");
$total = $subtotal + $charge;
$total = number_format($total, 2, ".", "");

$request['oid'] =  $ins_sched_order->gen_order_num(8);
$request['net'] = $price;
$request['vat_perc'] = $vat_perc;
$request['vat_amount'] = $vat_amount;
$request['subtotal'] = $subtotal;
$request['charge'] = $charge;
$request['total'] = $total;
$request['digest'] = md5($request['oid'].$total.$remote_pass);
$request['resident_num'] = $_SESSION['resident_num'];
$request['order_ins_sched_id'] = $_REQUEST['isi'];
$request['order_card_type_pre_selected'] = $_REQUEST['card_type'];

// Generate product detail string
// Note: for Director accounts, we look at the associated resident account and send that ref instead!
if($resident->is_resident_director == "Y"){
	$assoc_resident = new resident($resident->director_assoc_resident_num);
	$prod_detail_str = $subsidiary->subsidiary_ecom_code." ".$assoc_resident->resident_ref." eCom IS";
	$charge_detail_str = $subsidiary->subsidiary_ecom_code." ".$assoc_resident->resident_ref." eCom CH";
}
else{
	$prod_detail_str = $subsidiary->subsidiary_ecom_code." ".$resident->resident_ref." eCom IS";
	$charge_detail_str = $subsidiary->subsidiary_ecom_code." ".$resident->resident_ref." eCom CH";
}

if($ins_sched_order->save($request) === true){

	?>
	
	<html>
	<head>
	<title>Online Payment Service</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
	<body onLoad="document.form1.submit();">
	<form action="https://www.secpay.com/java-bin/ValCard" method="POST" name="form1" id="form1">
		
		<!-- Uncomment this when testing transactions... 
		<input type="hidden" name="test_status" value="true">
		-->
		
		<input type="hidden" name="callback" value="<?=$UTILS_HTTP_ADDRESS?>op/paypoint/r1_insur_sched.php">
		<input type="hidden" name="digest" value="<?=$request['digest']?>">
		<input type="hidden" name="amount" value="<?=$total?>">
		<input type="hidden" name="trans_id" value="<?=$request['oid']?>">
		<input type="hidden" name="merchant" value="reside01">
		<input type="hidden" name="order" value="prod=<?=$prod_detail_str?>,item_amount=<?=$price?>;prod=TAX@<?=$vat_perc?>%,amount=<?=$request['vat_amount']?>;prod=<?=$charge_detail_str?>,item_amount=<?=$charge?>">
		<input type="hidden" name="template" value="http://www.secpay.com/users/reside01/paypoint_template_insur_sched.html">
		<input type="hidden" name="cb_card_type" value="true">
		<input type="hidden" name="customfld1" value="<?=$ins_sched_order->order_serial?>">
		<input type="hidden" name="customfld2" value="<?=$_REQUEST['isi']?>">
		<input type="hidden" name="customfld3" value="<?=$_REQUEST['ist']?>">
		<input type="hidden" name="customfld4" value="<?=$_REQUEST['lookup_id']?>">
		<input type="hidden" name="rmc_num" value="<?=$_REQUEST['rmc_num']?>">
		<input type="hidden" name="rid" value="<?=$_REQUEST['resident_num']?>">
		<input type="hidden" name="owner_id" value="<?=$_REQUEST['owner_id']?>">	
        <input type="hidden" name="ctype" value="<?=$_REQUEST['card_type']?>">
		<input type="hidden" name="cb_flds" value="customfld1:customfld2:customfld3:customfld4:rmc_num:rid:owner_id:ctype">
	</form>
	</body>
	</html>
	
	<?
}
?>
