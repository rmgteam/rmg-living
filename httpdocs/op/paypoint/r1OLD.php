<?
require("../../utils.php");
require_once($UTILS_CLASS_PATH."security.class.php");
require_once($UTILS_CLASS_PATH."ins_sched_order.class.php");
$security = new security;
$ins_sched_order = new ins_sched_order;


// First, check that order exists...
$ins_sched_order->order($_REQUEST['customfld1'], "serial");
if($ins_sched_order->order_id != ""){
	
	// Now update order with rest of details...
	$request['order_transactionstatus'] = $_REQUEST['message'];
	if($_REQUEST['valid'] == "true"){
		$request['order_transactionstatus'] = "success";	
	}
	$request['order_bank_auth_code'] = $_REQUEST['auth_code'];
	$request['order_cv2avs'] = $_REQUEST['cv2avs'];
	$request['order_trans_code'] = $_REQUEST['code'];
	$request['order_test_status'] = $_REQUEST['test_status'];
	$request['order_response_hash'] = $_REQUEST['hash'];
	$ins_sched_order->save($request);
}


foreach($_REQUEST as $key=>$val){
	
	print "KEY:".$key.", VAL:".$val."<br />";
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>RMG Living - Payment Confirmation</title>
	<link href="<?=$UTILS_HTTP_ADDRESS?>styles.css" type="text/css" rel="stylesheet">
</head>
<body>

	<table width="100%" cellpadding="0" cellspacing="0" border="0">
		<tr>
		  <td rowspan="2" valign="top" bgcolor="#5589B8" style="border-right:1px solid #cccccc;"><img src="<?=$UTILS_HTTP_ADDRESS?>images/make_a_payment/online_payment_side_bar.jpg" width="122" height="450"></td>
		  <td width="100%" valign="top" style="padding-right:8px;padding-bottom:12px;padding-top:8px;padding-left:18px;border-bottom:1px solid #cccccc;">
		  
			<table cellpadding="6" cellspacing="0" border="0" width="100%">
			<tr>
			  <td style="vertical-align:top; background-color:#fff;">
				<p><br />
				<?
				
					//==============================================
					// Show success/failure message for transaction
					//==============================================
					if($_REQUEST['valid'] == "true"){
					
						print "<img src=\"".$UTILS_HTTP_ADDRESS."images/help_16.gif\" align=\"absmiddle\">&nbsp;<font color=\"#cc0000\">Unfortunately your payment request could not be completed.</font><br /><br /><strong>Order ref:</strong> ".$ins_sched_order->order_ref;
					}
					else{
						
						print "<font color=\"#669966\">Your payment was successful.</font> A receipt of the transaction has been sent to the email address that you supplied. Please make a note of the order reference below.<br><br><strong>Order ref:</strong> ".$ins_sched_order->order_ref;
					}
				
				
				?>
				<br />
				</p>
			  </td>
			</tr>
		  </table>
		  
			</td>
		</tr>
	</table>

</body>
</html>