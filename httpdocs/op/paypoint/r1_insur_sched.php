<?
require_once("utils.php");
require_once($UTILS_CLASS_PATH."security.class.php");
require_once($UTILS_CLASS_PATH."ins_sched_order.class.php");
$ins_sched_order = new ins_sched_order;
$ins_sched_order->order($_REQUEST['customfld1'], "serial");
$security = new security;

// First, check that order exists...
if($ins_sched_order->order_id != ""){
	
	// Now update order with rest of details...
	$request['order_transactionstatus'] = $_REQUEST['message'];
	if($_REQUEST['valid'] === "true"){
		$request['order_transactionstatus'] = "Success";	
	}
	$request['order_bank_auth_code'] = $_REQUEST['auth_code'];
	$request['order_cv2avs'] = $_REQUEST['cv2avs'];
	$request['order_trans_code'] = $_REQUEST['code'];
	$request['order_test_status'] = $_REQUEST['test_status'];
	$request['order_response_hash'] = $_REQUEST['hash'];
	$request['order_card_type_paid_with'] = $_REQUEST['card_type'];
	$ins_sched_order->save($request);
	
	//print "customfld3:".$_REQUEST['customfld3']."<br>";
	
	// Need to insert record into ins_sched_download_log (Intranet) 
	if( $_REQUEST['customfld3'] == "u" ){
		
		$sql_num = "
		SELECT count(*) 
		FROM ins_sched_download_log 
		WHERE 
		ins_sched_id = ".$security->clean_query($_REQUEST['customfld2'])." AND 
		unit_num = ".$security->clean_query($_REQUEST['customfld4'])." AND 
		resident_num = ".$security->clean_query($_REQUEST['rid'])." AND 
		order_ref = '".$security->clean_query($_REQUEST['trans_id'])."' 
		";
		$result_num = @mysql_query($sql_num, $UTILS_INTRANET_DB_LINK);
		$row_num = @mysql_fetch_row($result_num);
		if($row_num[0] == 0){
		
			$sql = "
			INSERT INTO ins_sched_download_log (
				ins_sched_id,
				owner_id,
				rmc_num,
				unit_num,
				resident_num,
				order_ref,
				order_ts,
				order_total
			)VALUES(
				".$security->clean_query($_REQUEST['customfld2']).",
				".$security->clean_query($_REQUEST['owner_id']).",
				".$security->clean_query($_REQUEST['rmc_num']).",
				".$security->clean_query($_REQUEST['customfld4']).",
				".$security->clean_query($_REQUEST['rid']).",
				'".$security->clean_query($_REQUEST['trans_id'])."',
				'".time()."',
				'".$security->clean_query($_REQUEST['amount'])."'
			)
			";
			@mysql_query($sql, $UTILS_INTRANET_DB_LINK);
		}
	}
	elseif( $_REQUEST['customfld3'] == "d" ){
		
		$sql_num = "
		SELECT count(*) 
		FROM ins_sched_download_log 
		WHERE 
		ins_sched_id = ".$security->clean_query($_REQUEST['customfld2'])." AND 
		director_id = ".$security->clean_query($_REQUEST['customfld4'])." AND 
		order_ref = '".$security->clean_query($_REQUEST['trans_id'])."' 
		";
		//print $sql_num."<br>";
		$result_num = @mysql_query($sql_num, $UTILS_INTRANET_DB_LINK);
		$row_num = @mysql_fetch_row($result_num);
		if($row_num[0] == 0){
		
			$sql = "
			INSERT INTO ins_sched_download_log (
				ins_sched_id,
				owner_id,
				rmc_num,
				director_id,
				order_ref,
				order_ts,
				order_total
			)VALUES(
				".$security->clean_query($_REQUEST['customfld2']).",
				".$security->clean_query($_REQUEST['owner_id']).",
				".$security->clean_query($_REQUEST['rmc_num']).",
				".$security->clean_query($_REQUEST['customfld4']).",
				'".$security->clean_query($_REQUEST['trans_id'])."',
				'".time()."',
				'".$security->clean_query($_REQUEST['amount'])."'
			)
			";
			//print $sql."<br>";
			@mysql_query($sql, $UTILS_INTRANET_DB_LINK);
		}
	}
}

//foreach($_REQUEST as $key=>$val){
//	print "KEY:".$key.", VAL:".$val."<br />";
//}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>RMG Living - Insurance Schedules</title>
	<link href="<?=$UTILS_HTTPS_ADDRESS?>css/reset.css" rel="stylesheet" type="text/css" />
	<link href="<?=$UTILS_HTTPS_ADDRESS?>styles.css" rel="stylesheet" type="text/css" />
	<link href="<?=$UTILS_HTTPS_ADDRESS?>css/common.css" rel="stylesheet" type="text/css" />
	<!--[if lte IE 8]> 
	<link href="<?=$UTILS_HTTPS_ADDRESS?>lte-ie8.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<!--[if lte IE 7]> 
	<link href="<?=$UTILS_HTTPS_ADDRESS?>lte-ie7.css" rel="stylesheet" type="text/css">
	<![endif]-->

	<script type='text/javascript' src="<?=$UTILS_HTTPS_ADDRESS?>library/jscript/jquery-1.6.2.min.js"></script>

</head>
<body>

	<div id="wrapper">
	
		<? 
		$header_img_ssl = true;
		require_once($UTILS_FILE_PATH."includes/header.php");
		?>
		
		<div id="content">
		
			<table width="760" cellspacing="0">
				<tr>
					<td><a href="<?=$UTILS_HTTPS_ADDRESS?>building_details.php" class="crumbs">Your Community</a>&nbsp;&gt;&nbsp;Insurance Schedules</td>
					<td style="text-align:right;" nowrap="nowrap"><? if(!empty($_SESSION['resident_session'])){?><a href="<?=$UTILS_HTTPS_ADDRESS?>index.php?logoff=Y" class="crumbs">Log Off</a><? }?></td>
				</tr>
			</table>

				
			<table width="760" cellspacing="0" cellpadding="0" style="margin-top:12px;">
				<tr>
					<td width="15"><img src="<?=$UTILS_HTTPS_ADDRESS?>images/dblue_box_top_left_corner.jpg" width="15" height="33" /></td>
					<td width="730" style="background-color:#416CA0;border-top:1px solid #003366; vertical-align:middle"><img src="<?=$UTILS_HTTPS_ADDRESS?>images/meetings/meetings_symbol.jpg" width="22" height="22" style="vertical-align:middle;">&nbsp;&nbsp;<span class="box_title">Insurance Schedules </span></td>
					<td width="15" align="right"><img src="<?=$UTILS_HTTPS_ADDRESS?>images/dblue_box_top_right_corner.jpg" width="15" height="33" /></td>
				</tr>
			</table>
						
			
			<div class="content_box_1 clearfix" style="padding:0;width:758px; vertical-align:top;">
								
				<div id="content_left" style="float:left;width:377px;padding:15px; vertical-align:top;">
					
					<? if($_REQUEST['valid'] !== "true"){?>
					<p style="color:#cc0000;"><img src="<?=$UTILS_HTTPS_ADDRESS?>images/help_16.gif" style="margin-right:10px;" />Unfortunately your payment request could not be completed.</p>
					<p style="font-weight:bold;">Order ref: <?=$ins_sched_order->order_ref?></p>
					<? }else{?>
					<p style="color:#669966;">Your payment was successful.</p>
					<p>Please make a note of the order reference <span style="font-weight:bold;"><?=$ins_sched_order->order_ref?></span>.</p>
					<p>&nbsp;</p>
					<p><a href="<?=$UTILS_HTTPS_ADDRESS?>ins_sched.php">Click here to return to your Insurance Schedule(s).</a></p>
					<? }?>
					
				</div>
	
				<div id="content_right" style="vertical-align:top;width:237px; min-height:400px;float:right;vertical-align:top;background-image:url(<?=$UTILS_HTTPS_ADDRESS?>images/ins_sched_folders.jpg);border-left:1px solid #eaeaea;background-repeat:no-repeat;padding:0;">
					&nbsp;					
				</div>
			
			</div>

		</div>
	
		<? require_once($UTILS_FILE_PATH."includes/footer.php");?>

	</div>

</body>
</html>
