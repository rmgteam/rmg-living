<?
require("utils.php");

header("Location: /op/paypoint/step_1.php");
exit;
Global $UTILS_TEL_MAIN;
require($UTILS_FILE_PATH."library/functions/clean_request_vars.php");
require($UTILS_CLASS_PATH."website.class.php");
require($UTILS_CLASS_PATH."resident.class.php");
$resident = new resident($_SESSION['resident_ref'],"ref");
$website = new website;

if($resident->resident_status == "Debt Collect"){
	?>
	<p style="color:#CC0000;">Online payment is not available at this time. Please contact Customer Services on <?=$UTILS_TEL_MAIN?>.</p>
	<?
	exit;
}

// Determine if allowed access into 'your community' section
$website->allow_community_access();

// Makes sure that values are zero if they are negative
if($_REQUEST['service_charge'] < 0){$_REQUEST['service_charge'] = 0;}
if($_REQUEST['ground_rent'] < 0){$_REQUEST['ground_rent'] = 0;}


?>
<html>
<head>
<title>RMG Living - Make a Payment</title>
<link href="../styles.css" type="text/css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="../library/jscript/functions/format_as_money.js"></script>
<script language="javascript">
function calc_total(){
	
	if(document.forms[0].ground_rent.value == ""){document.forms[0].ground_rent.value = "0.00";}
	if(document.forms[0].service_charge.value == ""){document.forms[0].service_charge.value = "0.00";}
	total = parseFloat(document.forms[0].ground_rent.value.replace(/,/g,"")) + parseFloat(document.forms[0].service_charge.value.replace(/,/g,""));
	document.forms[0].subtotal.value = format_as_money(total);
	document.getElementById('subtotalTXT').innerHTML = format_as_money(total);
}
function go_to_charges(){
	if((parseInt(document.forms[0].ground_rent.value) == 0 && parseInt(document.forms[0].service_charge.value) == 0) || (document.forms[0].ground_rent.value == "" && document.forms[0].service_charge.value == "")){
		alert("Please complete the amount(s) that you are paying for this transaction.");
	}
	else{
		document.forms[0].submit();
	}
}
</script>
</head>
<body>
<form action="<?=$UTILS_HTTPS_ADDRESS?>op/payment.php" method="POST">

			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
				  <td rowspan="2" valign="top" bgcolor="#5589B8" style="border-right:1px solid #cccccc;"><img src="../images/make_a_payment/online_payment_side_bar.jpg" width="122" height="450"></td>
				  <td valign="top" style="padding-right:8px;padding-bottom:12px;padding-top:8px;padding-left:18px;border-bottom:1px solid #cccccc;"><table cellpadding="6" cellspacing="0" border="0" width="100%">
                    <?=$message?>
                    <tr>
                      <td bgcolor="#FFFFFF"><img src="../images/make_a_payment/online_payments.gif"></td>
                    </tr>
                    <tr>
                      <td bgcolor="#FFFFFF">
					  	<p>				  	    Welcome to the Online Payments facility for Residential Management Group. Please enter below the amount(s) that you wish to pay.</p>
						<p>It is advisable that you check your statement before making a payment, to ensure that you are aware of the outstanding balance. Payment made using this site are secure.					    <br>
					  </p></td>
                    </tr>
                  </table></td>
			  </tr>
				<tr>
				  <td height="100%" valign="top" bgcolor="#ECF0F4" style="padding-right:8px;padding-bottom:8px;padding-top:12px;padding-left:18px;border-bottom:1px solid #cccccc;">
					
						<table cellpadding="6" cellspacing="0" border="0" width="550">
							<tr <? if($_REQUEST['ground_rent'] == "" || $_REQUEST['ground_rent'] == "0"){?>style="display:none;"<? }?>>
							  <td colspan="2"><strong class="subt036">Ground Rent </strong><br>Please enter the amount of  Ground Rent, if any, that you are paying. </td>
						  	</tr>
							<tr <? if($_REQUEST['ground_rent'] == "" || $_REQUEST['ground_rent'] == "0"){?>style="display:none;"<? }?>>
							  <td colspan="2" nowrap><font size="2"><strong>&pound;</strong></font>
								<input name="ground_rent" type="text" id="ground_rent" style="font-size:12px; font-weight:bold; color:#003366; text-align:right; " onBlur="document.forms[0].ground_rent.value = format_as_money(document.forms[0].ground_rent.value);calc_total();" onKeyUp="calc_total();" value="<?=$_REQUEST['ground_rent']?>" size="9"></td>
						  	</tr>
							<tr>
							  <td colspan="2" height="15"></td>
						  	</tr>
							<tr <? if($_REQUEST['service_charge'] == "" || $_REQUEST['service_charge'] == "0"){?>style="display:none;"<? }?>>
							  <td colspan="2" nowrap><strong class="subt036">Service Charge </strong><br>
						      Please enter the amount of Service Charge, if any, that you are paying.</td>
						  	</tr>
							<tr <? if($_REQUEST['service_charge'] == "" || $_REQUEST['service_charge'] == "0"){?>style="display:none;"<? }?>>
							  <td colspan="2" nowrap><font size="2"><strong>&pound;</strong></font>
                                <input name="service_charge" type="text" id="service_charge" style="font-size:12px; font-weight:bold; color:#003366; text-align:right; " onBlur="document.forms[0].service_charge.value = format_as_money(document.forms[0].service_charge.value);calc_total();" onKeyUp="calc_total();" value="<?=$_REQUEST['service_charge']?>" size="9"></td>
						  	</tr>
							<tr>
							  <td width="27%" nowrap>&nbsp;</td>
							  <td width="73%" align="right">&nbsp;</td>
						  	</tr>
							<tr bgcolor="#CED9E3">
							  <td nowrap style="border-left:1px solid #999999;border-top:1px solid #999999;border-bottom:1px solid #999999;"><strong class="subt036">Total Payment</strong></td>
						    <td style="border-right:1px solid #999999;border-top:1px solid #999999;border-bottom:1px solid #999999;" align="right" class="text036"><strong><font size="2">&pound;<span id="subtotalTXT"><? if($_REQUEST['subtotal'] != ""){print $_REQUEST['subtotal'];}else{print "0.00";}?></span></font></strong></td></tr>
							<tr>
							  <td colspan="2" height="15"></td>
						  	</tr>
							<tr align="right">
                              <td colspan="2"><p>Payment will be made into the account of your Resident Management Company: <br>
                                      <font size="2"><strong>
                                      <?=$_SESSION['rmc_name']?>
                                  </strong></font></p></td>
						  	</tr>
							<tr align="right">
							  <td colspan="2"><a href="#" onClick="go_to_charges();"><img src="../images/make_a_payment/next.gif" border="0"></a></td>
							</tr>
							<tr>
							  <td>&nbsp;</td>
							  <td align="right">&nbsp;</td>
							</tr>
							<tr>
							  <td>&nbsp;</td>
							  <td align="right">&nbsp;</td>
							</tr>
							<tr>
							  <td>&nbsp;</td>
							  <td align="right">&nbsp;</td>
							</tr>
							<tr>
							  <td>&nbsp;</td>
							  <td align="right">&nbsp;</td>
							</tr>
							<tr>
							  <td>&nbsp;</td>
							  <td align="right">&nbsp;</td>
							</tr>
							<tr>
							  <td>&nbsp;</td>
							  <td align="right">&nbsp;</td>
							</tr>
							
						</table>
						
					 
				  </td>
				</tr>
</table>
<input type="hidden" name="subtotal" id="subtotal" value="<?=$_REQUEST['subtotal']?>">
</form>

<script type="text/javascript">
calc_total();
</script>

</body>
</html>