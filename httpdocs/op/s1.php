<?
// THIS PAGE TAKES PAYMENTS FROM rmgliving.co.uk AND RMGLTD.CO.UK (the later redirects back to site)
require("utils.php");

header("Location: /op/paypoint/s1_norm.php");
exit;

require_once($UTILS_CLASS_PATH."security.class.php");
$security = new security;

// Determine if allowed access into 'your community' section (RMG only)
if(preg_match("/rmgliving/i", $_SERVER['HTTP_REFERER']) === 1){
	require($UTILS_CLASS_PATH."website.class.php");
	$website = new website;
	$website->allow_community_access();
}

# Password generating function
$rand_value[] = "A";
$rand_value[] = "B";
$rand_value[] = "C";
$rand_value[] = "D";
$rand_value[] = "E";
$rand_value[] = "F";
$rand_value[] = "G";
$rand_value[] = "H";
$rand_value[] = "J";
$rand_value[] = "K";
$rand_value[] = "M";
$rand_value[] = "N";
$rand_value[] = "P";
$rand_value[] = "Q";
$rand_value[] = "R";
$rand_value[] = "T";
$rand_value[] = "U";
$rand_value[] = "V";
$rand_value[] = "W";
$rand_value[] = "X";
$rand_value[] = "Y";
$rand_value[] = "2";
$rand_value[] = "3";
$rand_value[] = "4";
$rand_value[] = "6";
$rand_value[] = "7";
$rand_value[] = "8";
$rand_value[] = "9";

function get_rand_id($length){
	global $rand_value;
	$rand_id="";
	for($i=1; $i<=$length; $i++){
		mt_srand((double)microtime() * 1000000);
		$num = mt_rand(1, count($rand_value) );
		$rand_id .= $rand_value[$num-1];
	}
	return $rand_id;
}

#==========================================================================
# The following function performs an HTTP Post and returns the whole response
#==========================================================================
function pullpage( $host, $usepath, $postdata = "" ) {
 
	# open socket to filehandle(epdq encryption cgi)
	$fp = fsockopen( $host, 80, &$errno, &$errstr, 60 );

	#check that the socket has been opened successfully
	if( !$fp ) {
    	print "$errstr ($errno)<br>\n";
	}
	else {

    	#write the data to the encryption cgi
    	fputs( $fp, "POST $usepath HTTP/1.0\n");
    	$strlength = strlen( $postdata );
    	fputs( $fp, "Content-type: application/x-www-form-urlencoded\n" );
    	fputs( $fp, "Content-length: ".$strlength."\n\n" );
    	fputs( $fp, $postdata."\n\n" );

		#clear the response data
		$output = "";
 
		#read the response from the remote cgi 
		#while content exists, keep retrieving document in 1K chunks
		while( !feof( $fp ) ) {
			$output .= fgets( $fp, 1024);
		}

		#close the socket connection
		fclose( $fp);
	}

	#return the response
	return $output;
}

#define the remote cgi in readiness to call pullpage function 
$server="secure2.epdq.co.uk";
$url="/cgi-bin/CcxBarclaysEpdqEncTool.e";

# Get an order id
$oid_unique = "N";
while($oid_unique == "N"){

	$oid = get_rand_id(8);
	$sql = "SELECT order_ref FROM cpm_orders WHERE order_ref = '".$oid."'";
	$result = mysql_query($sql);
	$oid_exists = mysql_num_rows($result);
	if($oid_exists < 1){
		$oid_unique = "Y";
	}
}

if(preg_match("/rmgltd/i", $_SERVER['HTTP_REFERER']) === 1){
	
	# Get details about property
	$sql_prop_data = "SELECT * FROM cpm_pservices_property_data WHERE cpm_pservices_property_id = ".$security->clean_query($_REQUEST['pid']);
	$result_prop_data = mysql_query($sql_prop_data);
	$row_prop_data = mysql_fetch_array($result_prop_data);
	
	# Calculate charge based on card type chosen
	$subtotal = 0;
	
	if($row_prop_data['cpm_pservices_auto_payment_allocation'] != "Y"){
	
		if($row_prop_data['cpm_pservices_service_charge_amount'] != "" && $row_prop_data['cpm_pservices_service_charge_amount'] > 0){
			$subtotal += $row_prop_data['cpm_pservices_service_charge_amount'];
		}
		if($row_prop_data['cpm_pservices_reserve_charge_amount'] != "" && $row_prop_data['cpm_pservices_reserve_charge_amount'] > 0){
			$subtotal += $row_prop_data['cpm_pservices_reserve_charge_amount'];
		}
		if($row_prop_data['cpm_pservices_insurance_amount'] != "" && $row_prop_data['cpm_pservices_insurance_amount'] > 0){
			$subtotal += $row_prop_data['cpm_pservices_insurance_amount'];
		}
		if($row_prop_data['cpm_pservices_ground_rent_amount'] != "" && $row_prop_data['cpm_pservices_ground_rent_amount'] > 0){
			$subtotal += $row_prop_data['cpm_pservices_ground_rent_amount'];
		}
		if($row_prop_data['cpm_pservices_rent_amount'] != "" && $row_prop_data['cpm_pservices_rent_amount'] > 0){
			$subtotal += $row_prop_data['cpm_pservices_rent_amount'];
		}
		if($row_prop_data['cpm_pservices_garage_amount'] != "" && $row_prop_data['cpm_pservices_garage_amount'] > 0){
			$subtotal += $row_prop_data['cpm_pservices_garage_amount'];
		}
		if($row_prop_data['cpm_pservices_major_works_amount'] != "" && $row_prop_data['cpm_pservices_major_works_amount'] > 0){
			$subtotal += $row_prop_data['cpm_pservices_major_works_amount'];
		}
		if($row_prop_data['cpm_pservices_eoy_adjust_amount'] != "" && $row_prop_data['cpm_pservices_eoy_adjust_amount'] > 0){
			$subtotal += $row_prop_data['cpm_pservices_eoy_adjust_amount'];
		}
		if($row_prop_data['cpm_pservices_other_amount'] != "" && $row_prop_data['cpm_pservices_other_amount'] > 0){
			$subtotal += $row_prop_data['cpm_pservices_other_amount'];
		}
	}
	else{
		$subtotal = $row_prop_data['cpm_pservices_invoice_total_amount'];
	}

	$charge = 0;
	$total = 0;

	if($_REQUEST['card_type'] == "1"){
		$barc_charge = 0.025;
		$charge = ($subtotal * $barc_charge);
		$total = $subtotal + $charge;
		$total = number_format($total, 2, ".", "");
	}
	elseif($_REQUEST['card_type'] == "2"){
		$barc_charge = 0.03;
		$charge = ($subtotal * $barc_charge);
		$total = $subtotal + $charge;
		$total = number_format($total, 2, ".", "");
	}
	elseif($_REQUEST['card_type'] == "3"){
		$barc_charge = 0.35;
		$charge = $barc_charge + ($subtotal * 0.01);
		$total = $subtotal + $charge;
		$total = number_format($total, 2, ".", "");
	}

	# Insert new order into database
	$sql_no = "
	INSERT INTO cpm_orders(
		order_type,
		brand_code,
		order_ref,
		order_charge,
		order_ground_rent,
		order_service_charge,
		order_reserve_charge_amount,
		order_insurance_amount,
		order_rent_amount,
		order_garage_amount,
		order_major_works_amount,
		order_eoy_amount,
		order_other_amount,
		order_subtotal,
		order_total,
		order_card_type,
		browser_version,
		resident_num
	)
	VALUES(
		2,
		'".$row_prop_data['cpm_pservices_brand_code']."',
		'$oid',
		'".$charge."',
		'".$row_prop_data['cpm_pservices_ground_rent_amount']."',
		'".$row_prop_data['cpm_pservices_service_charge_amount']."',
		'".$row_prop_data['cpm_pservices_reserve_charge_amount']."',
		'".$row_prop_data['cpm_pservices_insurance_amount']."',
		'".$row_prop_data['cpm_pservices_rent_amount']."',
		'".$row_prop_data['cpm_pservices_garage_amount']."',
		'".$row_prop_data['cpm_pservices_major_works_amount']."',
		'".$row_prop_data['cpm_pservices_eoy_adjust_amount']."',
		'".$row_prop_data['cpm_pservices_other_amount']."',
		'".$subtotal."',
		'".$total."',
		'".$security->clean_query($_REQUEST['card_type'])."',
		'".$_SERVER['HTTP_USER_AGENT']."',
		'".$row_prop_data['cpm_pservices_tenant_ref']."'
	)
	";
	mysql_query($sql_no);
	
	$sql_insid = "SELECT LAST_INSERT_ID() FROM cpm_orders";
	$result_insid = mysql_query($sql_insid);
	$row_insid = mysql_fetch_row($result_insid);
	$order_id = $row_insid[0];
	
	# Update order ID back in cpm_pservices_table
	$sql_insp = "
	UPDATE cpm_pservices SET
	order_id = ".$order_id."
	WHERE cpm_pservices_serial = '".$security->clean_query($_REQUEST['pss'])."'";
	mysql_query($sql_insp);
}
else{

	$subtotal = str_replace(",","", $security->clean_query($_POST['subtotal']));
	$charge = 0;
	$total = 0;

	if($_REQUEST['card_type'] == "1"){
		$barc_charge = 0.025;
		$charge = ($subtotal * $barc_charge);
		$total = $subtotal + $charge;
		$total = number_format($total, 2, ".", "");
	}
	elseif($_REQUEST['card_type'] == "2"){
		$barc_charge = 0.035;
		$charge = ($subtotal * $barc_charge);
		$total = $subtotal + $charge;
		$total = number_format($total, 2, ".", "");
	}
	elseif($_REQUEST['card_type'] == "3"){
		$barc_charge = 0.35;
		$charge = $barc_charge + ($subtotal * 0.01);
		$total = $subtotal + $charge;
		$total = number_format($total, 2, ".", "");
	}

	# Insert new order into database
	$sql_no = "
	INSERT INTO cpm_orders(
		order_type,
		brand_code,
		order_ref,
		order_charge,
		order_ground_rent,
		order_service_charge,
		order_subtotal,
		order_total,
		order_card_type,
		browser_version,
		resident_num
	)
	VALUES(
		1,
		'CPM',
		'$oid',
		'".$charge."',
		'".$security->clean_query($_POST['ground_rent'])."',
		'".$security->clean_query($_POST['service_charge'])."',
		'".$subtotal."',
		'".$total."',
		'".$security->clean_query($_POST['card_type'])."',
		'".$_SERVER['HTTP_USER_AGENT']."',
		'".$_SESSION['resident_num']."'
	)
	";
	mysql_query($sql_no);

	
}


#the following parameters have been obtained earlier in the merchant's webstore
#clientid, passphrase, oid, currencycode, total
$params="clientid=17141";
$params.="&password=cpmlivingcpi";
$params.="&oid=$oid";
$params.="&chargetype=Auth";
$params.="&currencycode=826";
$params.="&subtotal=".$subtotal;
$params.="&total=".$total;

if(preg_match("/rmgliving/i", $_SERVER['HTTP_REFERER']) === 1){
	$params.="&ground_rent=".$security->clean_query($_POST['ground_rent']);
	$params.="&service_charge=".$security->clean_query($_POST['service_charge']);
}


#perform the HTTP Post
$response = pullpage( $server,$url,$params );

# Insert response into database
$sql_response = "
UPDATE cpm_orders SET
	order_s1_response = '".$security->clean_query($response)."'
WHERE order_ref = '".$oid."'";
@mysql_query($sql_response);
   
#split the response into separate lines
$response_lines=explode("\n",$response);

#for each line in the response check for the presence of the string 'epdqdata'
#this line contains the encrypted string
$response_line_count=count($response_lines);
for ($i=0;$i<$response_line_count;$i++){
    if (preg_match('/epdqdata/',$response_lines[$i])){
        $strEPDQ=$response_lines[$i];
    }
}


if(preg_match("/rmgltd/i", $_SERVER['HTTP_REFERER']) === 1 ){
	?>
	<html>
	<head>
	<title>Online Payment Service</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
	<body onLoad="document.form1.submit();">
	<form action="https://secure2.epdq.co.uk/cgi-bin/CcxBarclaysEpdq.e" method="POST" name="form1" id="form1">
	<?php print "$strEPDQ"; ?>
	<input type="hidden" name="returnurl" value="http://www.rmgltd.co.uk/services/private_sector/payment_response.php">
	<input type="hidden" name="merchantdisplayname" value="RMG">
	<input type="hidden" name="cpi_textcolor" value="#003366">
	<input type="hidden" name="supportedcardtypes" id="supportedcardtypes" value="125">
	<input type="hidden" name="cpi_logo" value="https://www.rmgliving.co.uk/images/cpi/cpi_rmg_logo.jpg">
	</form>
	</body>
	</html>
	<?
}
else{
	?>
	<html>
	<head>
	<title>RMG Living - Online Payments</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></head>
	<body onLoad="document.form1.submit();">
	<form action="https://secure2.epdq.co.uk/cgi-bin/CcxBarclaysEpdq.e" method="POST" name="form1" id="form1">
	<?php print "$strEPDQ"; ?>
	<input type="hidden" name="returnurl" value="http://www.rmgliving.co.uk/op/r1.php">
	<input type="hidden" name="merchantdisplayname" value="RMG">
	<input type="hidden" name="cpi_textcolor" value="#003366">
	<input type="hidden" name="supportedcardtypes" id="supportedcardtypes" value="125">
	<input type="hidden" name="cpi_logo" value="https://www.rmgliving.co.uk/images/cpi/cpi_rmg_logo.jpg">
	</form>
	</body>
	</html>
	<?
}
?>