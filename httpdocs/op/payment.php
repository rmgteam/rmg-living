<?
require("utils.php");

header("Location: /op/paypoint/payment.php");
exit;

require($UTILS_FILE_PATH."library/functions/clean_request_vars.php");
require($UTILS_CLASS_PATH."website.class.php");
$website = new website;

// Determine if allowed access into 'your community' section
$website->allow_community_access();

?>
<html>
<head>
<title>RMG Living - Make a Payment</title>
<link href="../styles.css" type="text/css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="../library/jscript/functions/format_as_money.js"></script>
<script language="javascript">
function make_payment(){
	if(document.forms[0].card_type.value == "-"){alert("Please select the type of Card you are paying with.");return;}
	document.forms[0].action = "<?=$UTILS_HTTPS_ADDRESS?>op/s1.php";
	document.forms[0].submit();
}
function go_to_step_1(){
	document.forms[0].action = "<?=$UTILS_HTTPS_ADDRESS?>op/step1.php";
	document.forms[0].submit();
}
function calc_charges(){
	
	var barc_charge;
	var charge=0, total=0;
	
	if(document.forms[0].card_type.value == "-"){
		document.forms[0].total.value = "0.00";
		document.forms[0].charge.value = "0.00";
		document.getElementById('chargeTXT').innerHTML = "0.00";
		document.getElementById('totalTXT').innerHTML = "0.00";
	}
	else{
		if(document.forms[0].subtotal.value != ""){
		
			if(document.forms[0].card_type.value == "1"){
				barc_charge = 0.025;
				charge = (parseFloat(document.forms[0].subtotal.value.replace(/,/g,"")) * barc_charge);
				document.forms[0].charge.value = format_as_money(charge);
				document.getElementById('chargeTXT').innerHTML = format_as_money(charge);
				total = parseFloat(document.forms[0].subtotal.value.replace(/,/g,"")) + parseFloat(charge);
				document.getElementById('totalTXT').innerHTML = format_as_money(total);
				document.forms[0].total.value = format_as_money(total);
			}
			else if(document.forms[0].card_type.value == "2"){
				barc_charge = 0.03;
				charge = (parseFloat(document.forms[0].subtotal.value.replace(/,/g,"")) * barc_charge);
				document.forms[0].charge.value = format_as_money(charge);
				document.getElementById('chargeTXT').innerHTML = format_as_money(charge);
				total = parseFloat(document.forms[0].subtotal.value.replace(/,/g,"")) + parseFloat(charge);
				document.getElementById('totalTXT').innerHTML = format_as_money(total);
				document.forms[0].total.value = format_as_money(total);
			}
			else if(document.forms[0].card_type.value == "3"){
				barc_charge = 0.35;
				document.getElementById('chargeTXT').innerHTML = format_as_money(barc_charge + (parseFloat(document.forms[0].subtotal.value.replace(/,/g,"")) * 0.01));
				document.forms[0].charge.value = format_as_money(barc_charge + (parseFloat(document.forms[0].subtotal.value.replace(/,/g,"")) * 0.01));
				total = parseFloat(document.forms[0].subtotal.value.replace(/,/g,"")) + barc_charge + (parseFloat(document.forms[0].subtotal.value.replace(/,/g,"")) * 0.01);
				document.getElementById('totalTXT').innerHTML = format_as_money(total);
				document.forms[0].total.value = format_as_money(total);
			}			
		}
	}
}
</script>
</head>
<body>
<form method="POST">
<input type="hidden" name="charge" id="charge">
<input type="hidden" name="ground_rent" value="<?=$_REQUEST['ground_rent']?>">
<input type="hidden" name="service_charge" value="<?=$_REQUEST['service_charge']?>">
<input type="hidden" name="payment_plan" value="<?=$_REQUEST['payment_plan']?>">
<input type="hidden" name="subtotal" id="subtotal" value="<?=$_REQUEST['subtotal']?>">
<input type="hidden" name="total" id="total">
<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
				  <td rowspan="2" valign="top" bgcolor="#5589B8" style="border-right:1px solid #cccccc;"><img src="../images/make_a_payment/online_payment_side_bar.jpg" width="122" height="450"></td>
				  <td valign="top" style="padding-right:8px;padding-bottom:12px;padding-top:8px;padding-left:18px;border-bottom:1px solid #cccccc;"><table cellpadding="6" cellspacing="0" border="0" width="100%">
                    <?=$message?>
                    <tr>
                      <td bgcolor="#FFFFFF"><img src="../images/make_a_payment/online_payments.gif"></td>
                    </tr>
                    <tr>
                      <td bgcolor="#FFFFFF"><p>An additional handling charge is made for using this online facility. In order for this charge to be calculated, please select the type of card you are using below. This charge will be included into the Total amount. Once you are ready to pay, please click the 'Pay' button.<br>
					  </p>
                      </td>
                    </tr>
                  </table></td>
			  </tr>
				<tr>
				  <td height="100%" valign="top" bgcolor="#ECF0F4" style="padding-right:8px;padding-bottom:8px;padding-top:12px;padding-left:18px;border-bottom:1px solid #cccccc;">
					
						<table cellpadding="6" cellspacing="0" border="0" width="550">
							<tr>
							  <td width="27%">Card Type * </td>
						      <td width="73%" align="right"><select name="card_type" id="card_type" onChange="calc_charges();">
						        <option value="-">-</option>
						        <option value="1">Visa/Mastercard Credit Cards</option>
						        <option value="2">Visa/Mastercard Business/Purchasing/Corporate Cards</option>
						        <option value="3">Visa Debit/UK Maestro Debit Cards</option>
					          </select></td></tr>
							<tr>
							  <td nowrap>Amount to pay </td>
							  <td align="right"><font size="2"><strong>&pound;<span id="subtotalTXT"><?=$_REQUEST['subtotal']?></span></strong></font>
							</td>
						  </tr>
							<tr>
							  <td nowrap>Barclaycard charges </td>
							  <td align="right"><strong><font size="2">&pound;<span id="chargeTXT">0.00</span></font></strong></td>
						  </tr>
							<tr>
							  <td nowrap>&nbsp;</td>
							  <td align="right"></td>
						  </tr>
							<tr bgcolor="#CED9E3">
							  <td nowrap style="border-left:1px solid #999999;border-top:1px solid #999999;border-bottom:1px solid #999999;"><strong>TOTAL
						      (incl. charges)</strong></td>
						    <td style="border-right:1px solid #999999;border-top:1px solid #999999;border-bottom:1px solid #999999;" align="right"><strong><font size="2">&pound;<span id="totalTXT">0.00</span></font></strong></td></tr>
							<tr>
							  <td colspan="2" height="15"></td>
						  </tr>
							<tr align="right">
							  <td colspan="2">Upon clicking 'Pay' you will be re-directed to the SecPay's Paypoint Online Payment System to complete your transaction.</td>
						  </tr>
							<tr>
							  <td><a href="#" onClick="go_to_step_1();"><img type="image" src="../images/make_a_payment/back.gif" border="0"></a></td>
							  <td align="right"><a href="#" onClick="make_payment();"><img type="image" src="../images/make_a_payment/pay.gif" border="0"></a>
						     </td>
						  </tr>
							<tr align="right">
							  <td colspan="2">Payments should be reflected on your statement of account within 48 hours. As RMG's offices are open weekdays only, please allow extra time if paying over the weekend.</td>
						  </tr>
							<tr>
							  <td>&nbsp;</td>
							  <td align="right">&nbsp;</td>
						  </tr>
							<tr>
							  <td>&nbsp;</td>
							  <td align="right">&nbsp;</td>
						  </tr>
							<tr>
							  <td>&nbsp;</td>
							  <td align="right">&nbsp;</td>
						  </tr>
							<tr>
							  <td>&nbsp;</td>
							  <td align="right">&nbsp;</td>
						  </tr>
							<tr>
							  <td>&nbsp;</td>
							  <td align="right">&nbsp;</td>
						  </tr>
							<tr>
							  <td>&nbsp;</td>
							  <td align="right">&nbsp;</td>
						  </tr>
							<tr>
							  <td>&nbsp;</td>
							  <td align="right">&nbsp;</td>
						  </tr>
							
						</table>
						
					 
				  </td>
				</tr>
</table>
</form>
</body>
</html>