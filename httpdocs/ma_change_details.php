<?
require("utils.php");
require($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."master_account.class.php");
$website = new website;
$master_account = new master_account($_SESSION['master_account_serial']);

// Determine if allowed access into 'your community' section
$website->allow_community_access();

// Save
if($_REQUEST['a'] == "s"){
	
	$redirect = "N";
	if($master_account->master_account_allow_password_reset == "Y"){
		$redirect = "Y";
	}
	
	$save_result = $master_account->check_fields($_REQUEST);
	if($save_result === true){
		
		$save_result = $master_account->save($_REQUEST);
	}
}

if($_REQUEST['a'] != "s"){
	
	$_REQUEST['mausername'] = $master_account->master_account_username;
	$_REQUEST['security_question_1'] = $master_account->master_account_question_id_1;
	$_REQUEST['security_answer_1'] = $master_account->master_account_answer_1;
	$_REQUEST['security_question_2'] = $master_account->master_account_question_id_2;
	$_REQUEST['security_answer_2'] = $master_account->master_account_answer_2;
	$_REQUEST['tel'] = $master_account->master_account_tel;
	$_REQUEST['mobile'] = $master_account->master_account_mobile;
	$_REQUEST['email'] = $master_account->master_account_email;
	$_REQUEST['optout_marketing'] = $master_account->master_account_optout_marketing;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>RMG Living - Change Details</title>
	<link href="/css/reset.css" rel="stylesheet" type="text/css" />
	<!--<link href="styles.css" rel="stylesheet" type="text/css">-->
	<link href="/css/common.css" rel="stylesheet" type="text/css" />
	<!--[if lte IE 8]> 
	<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<!--[if lte IE 7]> 
	<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
	<![endif]-->
	
	<script type="text/JavaScript">
	<!--
	function do_save(){
		
		document.form1.submit();
	}
	//-->
	</script>

	<script type='text/javascript' src="<?=$UTILS_HTTPS_ADDRESS?>library/jscript/jquery-1.6.2.min.js"></script>

	<? require_once($UTILS_FILE_PATH."includes/analytics.php");?>
</head>
<body>

	<div id="wrapper">
		
		<? require_once($UTILS_FILE_PATH."includes/header.php");?>
		
		<div id="content">
			
			<? if($master_account->master_account_allow_password_reset != "Y"){?>
			<table width="760" cellspacing="0">
				<tr>
					<td><a href="/building_details.php" class="crumbs">Your Community</a>&nbsp;>&nbsp;Master Account&nbsp;>&nbsp;My Details</td>
					<td style="text-align:right;" nowrap="nowrap"><? if(!empty($_SESSION['resident_session'])){?><a href="index.php?logoff=Y" class="crumbs">Log Off</a><? }?></td>
				</tr>
			</table>
			<? }?>
			
			
			<? if($master_account->master_account_allow_password_reset == "Y"){?>
			<table width="760" cellspacing="0" style="clear:both; margin-top:13px;">
    			<tr>
    				<td width="15"><img src="images/dblue_box_top_left_corner.jpg" width="15" height="33" /></td>
    				<td width="348" style="background-color:#426B9F;border-top:1px solid #003366; vertical-align:middle"><img src="images/person_icon.png" width="22" height="22" style="vertical-align:middle;margin-right:8px;" /><span class="box_title">My Details</span></td>
    				<td width="15"><img src="images/dblue_box_top_right_corner.jpg" width="15" height="33" /></td>
    				<td width="382"></td>
    			</tr>
    		</table>
			<? }else{?>
			<table width="760" cellspacing="0" style="clear:both; margin-top:13px;">
    			<tr>
    				<td width="15"><img src="images/dblue_box_top_left_corner.jpg" width="15" height="33" /></td>
    				<td width="348" style="background-color:#426B9F;border-top:1px solid #003366; vertical-align:middle"><img src="images/person_icon.png" width="22" height="22" style="vertical-align:middle;margin-right:8px;" /><span class="box_title">My Details</span></td>
    				<td width="34"><img src="images/your_community/tab_join.jpg" width="34" height="33" /></td>
    				<td width="348" style="background-color:#669ACC;border-top:1px solid #6699cc; vertical-align:middle"><img src="images/property_icon.png" width="22" height="22" style="vertical-align:middle;margin-right:8px;" /><a href="my_properties.php" class="box_title linkbox">My Properties</a></td>
    				<td width="15" style="text-align:right;" align="right"><img src="images/lblue_box_top_right_corner.jpg" width="15" height="33" /></td>
    			</tr>
    		</table>	
    		<? }?>
	
			
			<div class="content_box_1">
			
				<form action="<?=$UTILS_HTTPS_ADDRESS?>ma_change_details.php" name="form1" id="form1" method="post">
					<input type="hidden" name="a" id="a" value="s">
				
					<table class="table_1" width="727" cellspacing="0">
						<tr>
							<td colspan="2"><h4>My Details</h4></td>
						</tr>
						<tr>
							<td colspan="2" style="padding-bottom:10px;">
								<? if($master_account->master_account_allow_password_reset == "Y"){?>
								Please update your password and click 'Save'. (* required fields)
								<? }else{?>
								Below are your existing account details. If you wish to change any of the details, including creating a new password, amend the form below and click 'Save'. (* required fields)
								<? }?>
							</td>
						</tr>
						
						<?
						if($_REQUEST['a'] == "s" && $save_result !== true){
							?>
							<tr>
								<td colspan="2" class="msg_fail" style="vertical-align:middle; padding-top:20px; padding-bottom:20px;"><?=$save_result?></td>
							</tr>
							<?
						}
						if($_REQUEST['a'] == "s" && $save_result === true){
							?>
							<tr>
								<td colspan="2" style="vertical-align:middle; padding-top:20px; padding-bottom:20px;"><img src="images/tick_20.gif" style="margin-right:10px;float:left;" /><span class="msg_success" style="margin-bottom:6px;">Your details have been updated.<? if($redirect == "Y"){?> <a href="my_properties.php">Click here to go to your properties.</a><? }?></span></td>
							</tr>
							<?
						}
						?>
						
						<tr>
							<td width="155">&nbsp;</td>
							<td width="572">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2"><span class="subt036"><b>Master Account Username</b></span></td>
						</tr>
						<tr>
							<td colspan="2">Your username must be between 8 and 30 characters in length and can only be made up of numbers, letters, underscores and hyphens.</td>
						</tr>
						<tr>
							<td colspan="2" height="5"></td>
						</tr>
						<tr>
							<td>New username *</td>
							<td><input type="text" name="mausername" id="mausername" value="<?=$_REQUEST['mausername']?>" size="32" maxlength="30" autocomplete="off" /></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2"><span class="subt036"><b>Master Account Password</b></span></td>
						</tr>
						<tr>
							<td colspan="2"><? if($master_account->master_account_allow_password_reset != "Y"){?>Complete the fields below if you wish to change your password. <? }?>Your password must be between 8-16 characters in length and contain at least one number. Your password can also only be made up of numbers, letters, underscores and hyphens.</td>
						</tr>
						<tr>
							<td colspan="2" height="5"></td>
						</tr>
						<tr>
							<td>New password<? if($master_account->master_account_allow_password_reset == "Y"){print "&nbsp;*";}?></td>
							<td><input type="password" name="new_password" id="new_password" value="<?=$_REQUEST['new_password']?>" size="18" maxlength="16" autocomplete="off" /></td>
						</tr>
						<tr>
							<td nowrap>Confirm new password<? if($master_account->master_account_allow_password_reset == "Y"){print "&nbsp;*";}?></td>
							<td><input type="password" name="new_password_confirm" id="new_password_confirm" value="<?=$_REQUEST['new_password_confirm']?>" size="18" maxlength="16" autocomplete="off" /></td>
						</tr>
						
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td><span class="subt036"><b>Security Questions</b></span></td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2" height="5"></td>
						</tr>
						<tr>
							<td colspan="2">These may be used to verify you against our records if you call our Customer Services department, or if you forget your password.</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>Question 1 * </td>
							<td>
								<select name="security_question_1" id="security_question_1">
								<option value="" selected>-</option>
								<? 
								$sql_sq = "SELECT * FROM cpm_security_questions";
								$result_sq = @mysql_query($sql_sq);
								while($row_sq = @mysql_fetch_array($result_sq)){
									?>
									<option value="<?=$row_sq['question_id']?>" <? if($_REQUEST['security_question_1'] == $row_sq['question_id']){print "selected";}?>><?=$row_sq['question']?></option>
									<?
								}
								?>
								</select>
							</td>
						</tr>
						<tr>
							<td>Answer 1 * </td>
							<td><input name="security_answer_1" type="text" id="security_answer_1" value="<?=$_REQUEST['security_answer_1']?>" /></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>Question 2 * </td>
							<td><select name="security_question_2" id="security_question_2">
								<option value="" selected>-</option>
								<? 
								$sql_sq = "SELECT * FROM cpm_security_questions";
								$result_sq = @mysql_query($sql_sq);
								while($row_sq = @mysql_fetch_array($result_sq)){
									?>
									<option value="<?=$row_sq['question_id']?>" <? if($_REQUEST['security_question_2'] == $row_sq['question_id']){print "selected";}?>>
									<?=$row_sq['question']?>
									</option>
									<?
								}
								?>
								</select>
							</td>
						</tr>
						<tr>
							<td>Answer 2 * </td>
							<td><input name="security_answer_2" type="text" id="security_answer_2" value="<?=$_REQUEST['security_answer_2']?>" /></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td><span class="subt036"><b>Contact Details</b></span></td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2" height="5"></td>
						</tr>
						<tr>
							<td>Home telephone * </td>
							<td><input name="tel" type="text" id="tel" value="<?=$_REQUEST['tel']?>" /></td>
						</tr>
						<tr>
							<td>Mobile telephone</td>
							<td><input name="mobile" type="text" id="mobile" value="<?=$_REQUEST['mobile']?>" /></td>
						</tr>
						<tr>
							<td>Email address * </td>
							<td><input name="email" type="text" id="email" size="40" value="<?=$_REQUEST['email']?>" /></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2">
							We may contact you from time to time to inform you about offers we think may be of interest to you as a home owner. If you do not wish to receive this information, please tick the box.
							<input type="checkbox" name="optout_marketing" id="optout_marketing" value="Y" <? if($_REQUEST['optout_marketing'] == "Y"){?>checked<? }?>></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td><a href="Javascript:;" onClick="do_save();return false;"><img name="save_button" id="save_button" src="images/your_details/save_button.jpg" width="50" height="20" border="0"></a></td>
							<td>&nbsp;</td>
						</tr>
					</table>
				
				</form>
			
			</div>			
			
		</div>
		
		<? require_once($UTILS_FILE_PATH."includes/footer.php");?>
		
	</div>
		
</body>
</html>
