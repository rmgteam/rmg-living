<?
require_once("utils.php");
require($UTILS_CLASS_PATH."website.class.php");
$website = new website;

// Determine if allowed access into 'your community' section
$website->allow_community_access();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>RMG Living - Advice</title>
	<link href="/css/reset.css" rel="stylesheet" type="text/css" />
	<link href="/css/common.css" rel="stylesheet" type="text/css" />
	<!--[if lte IE 8]> 
	<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<!--[if lte IE 7]> 
	<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<script type='text/javascript' src="<?=$UTILS_HTTPS_ADDRESS?>library/jscript/jquery-1.6.2.min.js"></script>

	<? require_once($UTILS_FILE_PATH."includes/analytics.php");?>
</head>
<body>

	<div id="wrapper">
	
		<? require_once($UTILS_FILE_PATH."includes/header.php");?>
	
		<div id="content">
			
	
			<table width="760" cellspacing="0">
				<tr>
					<td><a href="/building_details.php" class="crumbs">Your Community</a>&nbsp;>&nbsp;Advice &amp; FAQ</td>
					<td style="text-align:right;" nowrap="nowrap"><? if(!empty($_SESSION['resident_session'])){?><a href="index.php?logoff=Y" class="crumbs">Log Off</a><? }?></td>
				</tr>
			</table>
			
	
			<ul class="your_comm_tabs">
				<li class="tab_yc tab_yc_inactive"><a href="/building_details.php">Your Community</a></li>
				<li class="divider divider_inactive_l_active_r"></li>
				<li class="tab_advice tab_advice_active"><a href="/advice.php">Advice/FAQ</a></li>
				<li class="divider divider_active_l_inactive_r"></li>
				<li class="tab_gi tab_gi_inactive"><a href="/general_info.php">General Info.</a></li>
				<li class="divider divider_inactive_l_inactive_r_a"></li>
				<li class="tab_end"></li>
			</ul>
	
			<div class="content_box_1" style="padding:0;width:758px;background-color:#F5F7FB;">
				
				<table width="758" cellspacing="0">
					<tr>
						<td width="380" style="vertical-align:top;padding:9px;">
						
						
							<table width="349" cellspacing="0">
								<tr>
									<td valign="top"><span class="subt036" style="font-weight:bold;">Frequently Asked Questions</span></td>
								</tr>
								<tr>
									<td valign="top">&nbsp;</td>
								</tr>
								<tr>
									<td valign="top">Below are some of the most frequently asked questions (FAQ's) that we receive from our existing customers. We hope that they will help you in answering any questions you have about the development that you live in.</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>
							
										<table width="348" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="text036" style="font-weight:bold;" colspan="2">Questions</td>
											</tr>
											<tr>
												<td colspan="2" height="10"></td>
											</tr>
											<?
											// Get faqs
											$sql_faqs = "SELECT * FROM cpm_faqs";
											$result_faqs = @mysql_query($sql_faqs);
											$q_counter=1;
											while($row_faqs = @mysql_fetch_array($result_faqs)){
												?>
												<tr valign="top">
													<td width="17" class="text036"><strong><?=$q_counter?>.</strong></td>
													<td width="331"><a href="#<?=$q_counter?>" class="link416CA0" style="text-decoration:underline;"><?=$row_faqs['faq_question']?></a></td>
												</tr>
												<? 
												$q_counter++;
											}
											?>
											<tr>
												<td colspan="2">&nbsp;</td>
											</tr>
											<tr>
												<td colspan="2">&nbsp;</td>
											</tr>
											<tr>
												<td colspan="2">&nbsp;</td>
											</tr>
											<tr>
												<td colspan="2">&nbsp;</td>
											</tr>
											<tr>
												<td class="text036" style="font-weight:bold;" colspan="2">Answers</td>
											</tr>
											<tr>
												<td colspan="2" height="10"></td>
											</tr>
											<?
											@mysql_data_seek($result_faqs, 0);
											$q_counter=1;
											while($row_faqs = @mysql_fetch_array($result_faqs)){
												?>
												<tr valign="top">
													<td class="text036"><strong><?=$q_counter?>.</strong></td>
													<td><span style="font-weight:bold;" id="#<?=$q_counter?>"><?=$row_faqs['faq_question']?></span></td>
													</tr>
												<tr valign="top">
													<td colspan="2"><?=$row_faqs['faq_answer']?></td>
													</tr>
												<tr valign="top">
													<td colspan="2">&nbsp;</td>
													</tr>
												<tr>
													<td align="right" colspan="2"></td>
													</tr>
												<? 
												$q_counter++;
											}
											?>
											<tr>
												<td align="right" colspan="2"><a href="#">Back to top</a></td>
											</tr>
										</table>
								
									</td>
								</tr>
							</table>
							
						</td>
						<td style="vertical-align:top; background-image:url(/images/advice/faq_on_sofa.jpg); padding:9px; background-repeat:no-repeat; background-position:top right; background-color:#F5F7FB; border-left:1px solid #eaeaea;background-repeat:no-repeat;"><img src="images/spacer.gif" alt="Answers at your fingertips" width="348" height="280" style="margin-left:10px;" /><br>
							
							<table width="348" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td><span class="subt036" style="font-weight:bold;">Useful Links</span></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td><span class="text036" style="font-weight:bold;">Directgov</span><br>For information and links to your local authority and Department of Environmental Health.<br><a href="http://www.directgov.org.uk" target="_blank" class="link036">www.directgov.org.uk</a></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td><span class="text036" style="font-weight:bold;">Lease Advice</span><br>
									The Leasehold Advisory Service provides free advice on the law affecting residential leasehold and commonhold property in England and Wales.<br>
									<a href="http://www.lease-advice.org" target="_blank" class="link036">www.lease-advice.org</a></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td><span class="text036" style="font-weight:bold;">Landlord Zone</span><br>
									It's an on-line community for landlords involved in letting property, novice and experienced alike, providing free access to information, resources and contacts.<br>
									<a href="http://www.landlordzone.co.uk" target="_blank" class="link036">www.landlordzone.co.uk</a></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>
									<span class="text036" style="font-weight:bold;">uSwitch.com</span><br>
									Take advantage of the best prices, offers and services from every gas, electricity, telephone and digital TV supplier in your area.<br>
									<a href="http://www.uSwitch.com" target="_blank" class="link036">www.uSwitch.com</a>					</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>
									<span class="text036" style="font-weight:bold;">homecheck.co.uk</span><br>
									Enter your postcode and find out if your property is at risk from flooding, landslip, air pollution, radon gas, landfill, waste sites and more. Also gives detailed information about your neighbourhood<br>
									<a href="http://www.homecheck.co.uk" target="_blank" class="link036">www.homecheck.co.uk</a>					</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>
									<span class="text036" style="font-weight:bold;">iammoving.com</span><br>
									Fill in your new address, work your way through a checklist of all the companies you need to notify and then let the site contact them all on your behalf<br>
									<a href="http://www.ihavemoved.com" target="_blank" class="link036">www.iammoving.com</a>					</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>
									<span class="text036" style="font-weight:bold;">Yell</span><br>
									The website of Yellow Pages.<br>
									<a href="http://www.yell.co.uk" target="_blank" class="link036">www.yell.co.uk</a>					</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>
									<span class="text036" style="font-weight:bold;">whatmortgage.co.uk</span><br>
									You can find out more about all aspects of homebuying on the What Mortgage website. The site features mortgage calculators, tips on the homebuying process and an archive of features from the magazine.<br>
									<a href="http://www.whatmortgage.co.uk" target="_blank" class="link036">www.whatmortgage.co.uk</a>				  </td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td><span class="text036" style="font-weight:bold;">Residential Landlords Association</span><br>
									The RLA is the effective voice for landlords having established our roots 35 years ago, offering landlord support and advice, as well as landlord training opportunities and innovative ideas for improving standards and saving money.<br>
									<a href="http://www.rla.org.uk" class="link036" target="_blank">www.rla.org.uk</a>					</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>
									<span class="text036" style="font-weight:bold;">Moneysupermarket.com </span><br>
									Moneysupermarket.com was launched in December 1999 and our purpose in life is to make it easy for you to compare personal finance products and services in the market<br>
									<a href="http://www.moneysupermarket.com" target="_blank" class="link036">www.moneysupermarket.com</a>					</td>
								</tr>
							</table>
											   
						</td>
					</tr>
				</table>
				
			</div>
				
		</div>
		
		<? require_once($UTILS_FILE_PATH."includes/footer.php");?>

	</div>

</body>
</html>
