<?
ini_set("max_execution_time","120");

require("../utils.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."resident.class.php");
$website = new website;
$resident = new resident;

// Determine if allowed access into content management system
$website->allow_cms_access();

// Check access privilege
if($_SESSION['allow_report'] != 1 && $_SESSION['report_2'] != '1'){header("Location:index.php");}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>RMG Living - Report</title>
<link href="../styles.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="/css/custom-theme/jquery-ui-1.8.16.custom.css"/>
<script type="text/javascript" language="JavaScript" src="/library/jscript/jquery-1.6.2.min.js"></script>
<script type="text/javascript" language="JavaScript" src="/library/jscript/jquery-ui-1.8.16.custom.min.js"></script>
<style type="text/css" media="screen">

body {
	padding:20px;
}
.norm_table {
	max-width:800px;
}
.style1 {font-size: 12px}
.style2 {
	color: #336633;
	font-size: 12px;
}
.style3 {
	color: #CC3333;
	font-size: 12px;
}

.ui-widget { font-family: Verdana,Arial,sans-serif; font-size: 0.7em; }
</style>
<style type="text/css" media="print">
body {
	padding:0;
}
.norm_table {
	max-width:800px;
}
.style1 {font-size: 12px}
.style2 {
	color: #336633;
	font-size: 12px;
}
.style3 {
	color: #CC3333;
	font-size: 12px;
}
#filter_table {
	display:none;
}
</style>
<script type="text/javascript">
function do_filter(){
	
	if(document.getElementById('chk_summary').checked == false){
		if(document.getElementById('date_from').value == "" || document.getElementById('date_to').value == ""){
			alert("You must complete both the 'From' and 'To' date fields.");
			return false;
		}
	}
	
	document.getElementById('whichaction').value = "filter";
	document.form1.submit();
}
function reset_filter(){
	document.getElementById('date_from').value = "";
	document.getElementById('date_to').value = "";
	document.getElementById('whichaction').value = "show_report";
	document.form1.submit();
}
$(document).ready(function(){			
	$(".date").datepicker({ dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true });
});
</script>
</head>

<body>

	<form id="form1" name="form1">
		
		<table id="filter_table" class="norm_table" border="0" align="center" cellpadding="8" cellspacing="0" style="background-color:#f1f1f1;border:1px solid #999999;margin-bottom:5px;">
			<tr>
				<td>
				<? if ($_REQUEST['chk_summary'] != ''){ $checked = ' checked="checked';}?>
					From:&nbsp;<input type="text" class="date" name="date_from" id="date_from" size="10" maxlength="10" value="<?=$_REQUEST['date_from']?>" />&nbsp;(dd/mm/yyyy)&nbsp;&nbsp;&nbsp;&nbsp;
					To:&nbsp;<input type="text" class="date" name="date_to" id="date_to" size="10" maxlength="10" value="<?=$_REQUEST['date_to']?>" />&nbsp;(dd/mm/yyyy)&nbsp;&nbsp;&nbsp;&nbsp;
					Summary <input type="checkbox" id="chk_summary" name="chk_summary"<?=$checked?> value="summary" />
					<input type="button" name="filter_button" id="filter_button" value="Filter" onClick="do_filter()" />
					<input type="button" name="reset_button" id="reset_button" value="Reset" onClick="reset_filter()" />
				</td>
			</tr>
		</table>
		<?
		if($_REQUEST['whichaction'] == "filter"){
			$page_num = 1;
			
			if($_REQUEST['date_from'] != ""){
				$from_split = explode("/",$_REQUEST['date_from']);
				$from_ymd = $from_split[2].$from_split[1].$from_split[0];
				$from_clause = "AND CONCAT(
									SUBSTRING_INDEX( SUBSTRING(e.date_created, 7) , '/', -1 ), 
									SUBSTRING_INDEX(SUBSTRING_INDEX( SUBSTRING(e.date_created, 7) , '/', 2 ),'/',-1), 
									SUBSTRING_INDEX( SUBSTRING(e.date_created, 7) , '/', 1 )
								) >= ".$from_ymd." ";
			}
			if($_REQUEST['date_to'] != ""){
				$to_split = explode("/",$_REQUEST['date_to']);
				$to_ymd = $to_split[2].$to_split[1].$to_split[0];
				$to_clause = " AND CONCAT(
									SUBSTRING_INDEX( SUBSTRING(e.date_created, 7) , '/', -1 ), 
									SUBSTRING_INDEX(SUBSTRING_INDEX( SUBSTRING(e.date_created, 7) , '/', 2 ),'/',-1), 
									SUBSTRING_INDEX( SUBSTRING(e.date_created, 7) , '/', 1 )
								) <= ".$to_ymd." ";
			}
			
			$sql_rep = "
			SELECT *,
			SUBSTRING(e.date_created, 7) as dc
			FROM cpm_residents r
			INNER JOIN cpm_residents_extra e
			ON r.resident_num = e.resident_num
			WHERE
			e.email <> '' AND
			e.email <> 'webmaster@rmgliving.co.uk' AND
			r.resident_is_active = '1' 
			$from_clause 
			$to_clause 
			ORDER BY SUBSTRING_INDEX( SUBSTRING(e.date_created, 7) , '/', -1 ), SUBSTRING_INDEX(SUBSTRING_INDEX( SUBSTRING(e.date_created, 7) , '/', 2 ),'/',-1), SUBSTRING_INDEX( SUBSTRING(e.date_created, 7) , '/', 1 ) ASC";
			//print $sql_rep;
			$result_rep = @mysql_query($sql_rep);
			$num_rep = @mysql_num_rows($result_rep);
			$row_counter=0;
			$break_counter=0;
			
			if($num_rep == 0){
		
			?>
			<table class="norm_table" border="0" align="center" cellpadding="4" cellspacing="0">
				<tr>
				<td><span class="style1">&nbsp;
				There are no transactions between the date(s) provided.
				</span></td>
				</tr>
			</table>
			<?
			}
			else{
		
			?>
			<table class="norm_table" border="0" align="center" cellpadding="4" cellspacing="0">
			  <tr>
				<td colspan="3"><span class="style1"><strong>Board Report (RMG Living) printed: <?=date("d/m/y H:i:s", $thistime)?></strong></span></td>
				<td width="92" colspan="3" align="right" ><span class="style1">Page <?=$page_num?></span></td>
			</tr>
			  <tr>
				  <td colspan="6" height="25"></td>
			  </tr>
				<tr><td colspan="6" style="background-color:#333333;border-bottom:1px solid #333333;" height="1"></td></tr>
				<? if($_REQUEST['chk_summary'] != ""){ ?>
				<tr>
				  <td colspan="3" nowrap style="border-bottom:1px solid #333333;border-right:1px solid #cccccc;border-left:1px solid #cccccc;"><strong>Month</strong></td>
				  <td width="80" nowrap style="border-bottom:1px solid #333333;border-right:1px solid #cccccc;"><strong>Is Director</strong></td>
				  <td width="80" style="border-bottom:1px solid #333333;border-right:1px solid #cccccc"><strong>Is Lessee</strong></td>
				  <td width="80" style="border-bottom:1px solid #333333;border-right:1px solid #cccccc"><strong>Is Sub-Tenant</strong></td>
				</tr>
				<? }else{ ?>
				<tr>
				  <td width="80" nowrap style="border-bottom:1px solid #333333;border-right:1px solid #cccccc;border-left:1px solid #cccccc;"><strong>Resident Ref.</strong></td>
				  <td width="220" nowrap style="border-bottom:1px solid #333333;border-right:1px solid #cccccc;"><strong>Resident Name</strong></td>
				  <td width="80" nowrap style="border-bottom:1px solid #333333;border-right:1px solid #cccccc;"><strong>Date</strong></td>
				  <td width="80" nowrap style="border-bottom:1px solid #333333;border-right:1px solid #cccccc;"><strong>Is Director</strong></td>
				  <td width="80" style="border-bottom:1px solid #333333;border-right:1px solid #cccccc"><strong>Is Lessee</strong></td>
				  <td width="80" style="border-bottom:1px solid #333333;border-right:1px solid #cccccc"><strong>Is Sub-Tenant</strong></td>
				</tr>
				
				<?
				}
				$no_of_directors = 0;
				$no_of_Lessees = 0;
				$no_of_subtenants = 0;
				$month_of_directors = 0;
				$month_of_Lessees = 0;
				$month_of_subtenants = 0;
				$old_month = '';
				$old_year = '';
				$last_date = '';
				
				while($row_rep = @mysql_fetch_array($result_rep)){
				  
				  	if( preg_match("/t/",$row_rep['resident_num']) !== 1 ){
						
						// Added this in becasue the resident record may no longer exist, so the 'resident' object cannot be used here.
						$sql = "SELECT * FROM cpm_lookup_residents WHERE resident_lookup = ".$row_rep['resident_num'];
						$result = @mysql_query($sql);
						$row = @mysql_fetch_array($result);
						$resident_ref = $row['resident_ref'];
					}
					else{
						$resident_ref = $row_rep['resident_num'];
					}
				  
					// If order is placed via RMG site, place total into service charge column
					if($row_rep['is_resident_director'] == "N" && $row_rep['is_subtenant_account'] == "N"){
						$is_Lessee = 'Y';
						$is_subtenant = 'N';
						$no_of_Lessees ++;
					}elseif($row_rep['is_resident_director'] == "Y" && $row_rep['is_subtenant_account'] == "N"){
						$is_Lessee = 'N';
						$is_subtenant = 'N';
						$no_of_directors ++;
					}else{
						$is_Lessee = 'N';
						$is_subtenant = 'Y';
						$no_of_subtenants ++;
					}
			  		
					if($_REQUEST['chk_summary'] != ""){ 
						$date = explode("/", $row_rep['dc']);
						if ($date[1] != $old_month){
							
							if($old_month <> ''){
								$timestamp = mktime(0, 0, 0, $old_month, 1, $date[2]);
					?>
					<tr <? if($row_counter%2 == 0){print "bgcolor='#f1f1f1'";}?>>
					  <td colspan="3" style="border-right:1px solid #cccccc;border-left:1px solid #cccccc;"><?=date("F", $timestamp)?></td>
					  <td style="border-right:1px solid #cccccc;text-align:right;"><?=$month_of_directors?>&nbsp;</td>
					  <td style="border-right:1px solid #cccccc;text-align:right;"><?=$month_of_Lessees?></td>
					  <td style="border-right:1px solid #cccccc;text-align:right;"><?=$month_of_subtenants?></td>
					</tr>
					<?
							}
							$month_of_Lessees = 0;
							$month_of_directors = 0;
							$month_of_subtenants = 0;
							if ($date[2] != $old_year){
					?>
					<tr bgcolor='#FFFFFF'>
					  <td colspan="6" style="border:1px solid #cccccc;"><strong><?=$date[2]?></strong></td>
					</tr>
					<?	
							}
							$row_counter++;
							$break_counter++;
						}
						
						if($row_rep['is_resident_director'] == "N" && $row_rep['is_subtenant_account'] == "N"){
							$month_of_Lessees ++;
						}elseif($row_rep['is_resident_director'] == "Y" && $row_rep['is_subtenant_account'] == "N"){
							$month_of_directors ++;
						}else{
							$month_of_subtenants ++;
						}
						
						if($break_counter > 37){
							$break_counter = 0;
							$page_num++;
					?>
					<tr>
							<td colspan="6" style="background-color:#333333;border-bottom:1px solid #333333;" height="1"></td>
						</tr>
						<tr>
							<td colspan="6">&nbsp;</td>
						</tr>
						<tr>
						   <td colspan="3">&nbsp;</td>
						   <td align="right"><strong><?=$no_of_directors;?></strong></td>
						   <td align="right"><strong><?=$no_of_Lessees;?></strong></td>
						   <td align="right"><strong><?=$no_of_subtenants;?></strong></td>
						</tr>
						<tr>
						   <td colspan="6">&nbsp;</td>
						</tr>
						<tr>
						   <td colspan="6">&nbsp;</td>
						</tr>
						<tr>
						   <td colspan="6">&nbsp;</td>
						</tr>
						</table>
						
						<p class='break'><!--&nbsp;--></p>
						
						<table class="norm_table" border="0" align="center" cellpadding="4" cellspacing="0">
						<tr>
							<td colspan="3" ><span class="style1"><strong>Board Report (RMG Living) printed: <?=date("d/m/y H:i:s", $thistime)?></strong></span></td>
							<td width="92" colspan="3" align="right" ><span class="style1">Page <?=$page_num?></span></td>
						</tr>
						<tr>
							<td colspan="6" height="5"></td>
						</tr>
						<tr><td colspan="6" style="background-color:#333333;border-bottom:1px solid #333333;" height="1"></td></tr>				
					<tr>
					  <td colspan="3" nowrap style="border-bottom:1px solid #cccccc;border-left:1px solid #cccccc;border-right:1px solid #cccccc;"><strong>Month</strong></td>
					  <td width="80" nowrap style="border-bottom:1px solid #cccccc;border-right:1px solid #cccccc;"><strong>Is Director</strong></td>
					  <td width="80" style="border-bottom:1px solid #cccccc;border-right:1px solid #cccccc"><strong>Is Lessee</strong></td>
					  <td width="80" style="border-bottom:1px solid #333333;border-right:1px solid #cccccc"><strong>Is Sub-Tenant</strong></td>
					</tr>
					<? 
						}
					}else{ 
					?>
					<tr <? if($row_counter%2 == 0){print "bgcolor='#f1f1f1'";}?>>
					  <td style="border-right:1px solid #cccccc;border-left:1px solid #cccccc;"><?=$resident_ref?></td>
					  <td style="border-right:1px solid #cccccc;"><?=$row_rep['resident_name']?></td>
					  <td style="border-right:1px solid #cccccc;"><?=$row_rep['dc']?></td>
					  <td style="border-right:1px solid #cccccc;"><?=$row_rep['is_resident_director']?>&nbsp;</td>
					  <td style="border-right:1px solid #cccccc;"><?=$is_Lessee?></td>
					  <td style="border-right:1px solid #cccccc;"><?=$is_subtenant?></td>
					</tr>
					<?
					
						$row_counter++;
						$break_counter++;
						if($break_counter > 37){
					
							$break_counter=0;
							$page_num++;
						?>
						<tr>
							<td colspan="6" style="background-color:#333333;border-bottom:1px solid #333333;" height="1"></td>
						</tr>
						<tr>
							<td colspan="6">&nbsp;</td>
						</tr>
						<tr>
						   <td>&nbsp;</td>
						   <td>&nbsp;</td>
						   <td>&nbsp;</td>
						   <td align="right"><strong><?=$no_of_directors;?></strong></td>
						   <td align="right"><strong><?=$no_of_Lessees;?></strong></td>
						   <td align="right"><strong><?=$no_of_subtenants;?></strong></td>
						</tr>
						<tr>
						   <td colspan="6">&nbsp;</td>
						</tr>
						<tr>
						   <td colspan="6">&nbsp;</td>
						</tr>
						<tr>
						   <td colspan="6">&nbsp;</td>
						</tr>
						</table>
						
						<p class='break'><!--&nbsp;--></p>
						
						<table class="norm_table" border="0" align="center" cellpadding="4" cellspacing="0">
						<tr>
							<td colspan="3" ><span class="style1"><strong>Board Report (RMG Living) printed: <?=date("d/m/y H:i:s", $thistime)?></strong></span></td>
							<td width="92" colspan="3" align="right" ><span class="style1">Page <?=$page_num?></span></td>
						</tr>
						<tr>
							<td colspan="6" height="5"></td>
						</tr>
						<tr><td colspan="6" style="background-color:#333333;border-bottom:1px solid #333333;" height="1"></td></tr>
						<tr>
						  <td width="80" nowrap style="border-bottom:1px solid #333333;border-right:1px solid #cccccc;border-left:1px solid #cccccc;"><strong>Resident Ref.</strong></td>
						  <td width="220" nowrap style="border-bottom:1px solid #333333;border-right:1px solid #cccccc;"><strong>Resident Name</strong></td>
						  <td width="80" nowrap style="border-bottom:1px solid #333333;border-right:1px solid #cccccc;"><strong>Date</strong></td>
						  <td width="80" nowrap style="border-bottom:1px solid #333333;border-right:1px solid #cccccc;"><strong>Is Director</strong></td>
						  <td width="80" style="border-bottom:1px solid #333333;border-right:1px solid #cccccc"><strong>Is Lessee</strong></td>
						  <td width="80" style="border-bottom:1px solid #333333;border-right:1px solid #cccccc"><strong>Is Sub-Tenant</strong></td>
						</tr>
					<?
						}
					}
					$old_month = $date[1];
					$old_year = $date[2];
					$last_date = $date;
				}
				
				if($_REQUEST['chk_summary'] != ""){
					if ($last_date[2] != $old_year){
						?>
						<tr bgcolor='#FFFFFF'>
						  <td colspan="6" style="border:1px solid #cccccc;"><strong><?=$last_date[2]?></strong></td>
						</tr>
						<?	
					}
						
					if($old_month <> ''){
						$timestamp = mktime(0, 0, 0, $old_month, 1, $date[2]);
						?>
						<tr <? if($row_counter%2 == 0){print "bgcolor='#f1f1f1'";}?>>
						  <td colspan="3" style="border-right:1px solid #cccccc;border-left:1px solid #cccccc;"><?=date("F", $timestamp)?></td>
						  <td style="border-right:1px solid #cccccc;text-align:right;"><?=$month_of_directors?>&nbsp;</td> <td style="border-right:1px solid #cccccc;text-align:right;"><?=$month_of_Lessees?></td>
<td style="border-right:1px solid #cccccc;text-align:right;"><?=$month_of_subtenants?></td>
						</tr>
						<?
					}
				}
				?>
			  
				<tr>
					<td colspan="6" style="background-color:#333333;border-bottom:1px solid #333333;" height="1"></td>
				</tr>
				<tr>
					<td colspan="6">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				   	<td>&nbsp;</td>
				   	<td>&nbsp;</td>
				   	<td align="right"><strong><?=$no_of_directors;?></strong></td>
				   	<td align="right"><strong><?=$no_of_Lessees;?></strong></td>
					<td align="right"><strong><?=$no_of_subtenants;?></strong></td>
				</tr>
				<tr>
					<td colspan="6">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="6">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="6">&nbsp;</td>
				</tr>
			</table>
			<?
			}
		}
		?>		
		<input type="hidden" id="whichaction" name="whichaction" />
	</form>	
</body>
</html>
