<?
ini_set("max_execution_time","14400");
ini_set("memory_limit","-1");

require("utils.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."resident.class.php");
require_once($UTILS_CLASS_PATH."data.class.php");
require_once($UTILS_CLASS_PATH."excel.class.php");
$website = new website;
$resident = new resident;

// Determine if allowed access into content management system
$website->allow_cms_access();

// Check access privilege
if($_SESSION['allow_report'] != 1 && $_SESSION['report_7'] != '1'){header("Location:index.php");}

if($_REQUEST['whichaction'] == "filter"){
	
	header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header("Content-disposition: attachment; filename=optin_report-".date("d-F-Y", time()).".xlsx");
	header("Cache-Control: maxage=1");
	header("Pragma: public");
		
	$data = new data;
	$header_array = array();
	$data_array = array();
	
	array_push($header_array, 'Property Ref.');
	array_push($header_array, 'Property Name');
	array_push($header_array, 'Resident Ref');
	array_push($header_array, 'Resident Name');
	array_push($header_array, 'Survey Opt In');
	array_push($header_array, 'Announcement Opt In');

	$page_num = 1;
		
	$sql_rep = "
	SELECT *
	FROM cpm_residents re
	INNER JOIN cpm_lookup_residents lr ON lr.resident_lookup = re.resident_num
	INNER JOIN cpm_rmcs r ON re.rmc_num = r.rmc_num
	INNER JOIN cpm_lookup_rmcs l ON l.rmc_lookup = r.rmc_num
	INNER JOIN cpm_residents_extra e ON re.resident_num = e.resident_num
	WHERE r.rmc_is_active = '1'
	AND r.property_manager <> 'NO LONGER MANAGED' 
	AND r.property_manager <> 'Ground Rent Only' 
	AND l.rmc_ref NOT LIKE 'YYY%' 
	AND l.rmc_ref NOT LIKE 'RMG-%'
	AND re.resident_is_active = '1'";

	$result_rep = @mysql_query($sql_rep);
	$num_rep = @mysql_num_rows($result_rep);
		
	if($num_rep > 0){
		$i = 0;

		while($row_rep = @mysql_fetch_array($result_rep)){

			$data_array[$i][] = $row_rep['rmc_ref'];
				
			$data_array[$i][] = $row_rep['rmc_name'];
				
			$data_array[$i][] = $row_rep['resident_ref'];
				
			$data_array[$i][] = $row_rep['resident_name'];
				
			$data_array[$i][] = ($row_rep['survey_optout'] == 'N' ? 'Yes' : 'No');
				
			$data_array[$i][] = ($row_rep['announce_optin'] == 'Y' ? 'Yes' : 'No');
			
			$i++;
		}
	}

	$report_name = "Tenant Opt In Report";
	$report = new excel($report_name);
	$report->headers($header_array);
	$report->data($data_array);
	$report->save();
	exit;
}else{
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>RMG Living - Report</title>
<link href="../styles.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="/css/custom-theme/jquery-ui-1.8.16.custom.css"/>
<script type="text/javascript" language="JavaScript" src="/library/jscript/jquery-1.6.2.min.js"></script>
<script type="text/javascript" language="JavaScript" src="/library/jscript/jquery-ui-1.8.16.custom.min.js"></script>
<style type="text/css" media="screen">

body {
	padding:20px;
}
.norm_table {
	max-width:800px;
}
.style1 {font-size: 12px}
.style2 {
	color: #336633;
	font-size: 12px;
}
.style3 {
	color: #CC3333;
	font-size: 12px;
}

.ui-widget { font-family: Verdana,Arial,sans-serif; font-size: 0.7em; }
</style>
<style type="text/css" media="print">
body {
	padding:0;
}
.norm_table {
	max-width:800px;
}
.style1 {font-size: 12px}
.style2 {
	color: #336633;
	font-size: 12px;
}
.style3 {
	color: #CC3333;
	font-size: 12px;
}
#filter_table {
	display:none;
}
</style>
<script type="text/javascript">
function do_filter(){
	
	document.getElementById('whichaction').value = "filter";
	document.form1.submit();
}
$(document).ready(function(){			
	$(".date").datepicker({ dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true });
});
</script>
</head>

<body>

	<form id="form1" name="form1">
		
		<table id="filter_table" class="norm_table" border="0" align="center" cellpadding="8" cellspacing="0" style="background-color:#f1f1f1;border:1px solid #999999;margin-bottom:5px;">
			<tr>
				<td>
					<input type="button" name="filter_button" id="filter_button" value="Filter" onClick="do_filter()" />
				</td>
			</tr>
		</table>	
		<input type="hidden" id="whichaction" name="whichaction" />
	</form>	
</body>
</html>
<? } ?>