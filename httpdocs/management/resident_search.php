<?
require("utils.php");
require_once($UTILS_CLASS_PATH."website.class.php");
$website = new website;

// Determine if allowed access into content management system
$website->allow_cms_access();

// Check access privilege
if($_SESSION['allow_residents'] != 1){header("Location:index.php");}
?>
<html>
<head>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style>
</head>
<body>
<style type="text/css">
<!--
.style1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
}
-->
</style>
<table width="449"  border="0" cellspacing="0" cellpadding="4">
<?
#====================================================
# Get list of residents based on search term
#====================================================
if($_REQUEST['whichaction'] == "search"){
	
	if($_REQUEST['rmc_num'] != ""){
		$rmc_clause = " res.rmc_num = ".$_REQUEST['rmc_num']." AND ";
	}
	if($_REQUEST['resident_num'] != ""){
		$main_clause = " res.resident_num = ".$_REQUEST['resident_num']." AND ";
	}
	else{
		if($_REQUEST['resident_search'] != ""){
			// Do fulltext search
			$main_clause_array = array();
			$search_parts = explode(" ", $_REQUEST['resident_search']);
			if( count($search_parts) ){
				
				for( $p=0 ; $p < count($search_parts) ; $p++ ){
					
					$match_str .= $search_parts[$p]." ";
					$match_boolean_str .= $search_parts[$p]."* "; 
				}
				
				$main_clause = "
				(( 
					MATCH (res.resident_address_postcode, res.resident_name) AGAINST ('".$match_str."') OR 
					MATCH (res.resident_address_postcode, res.resident_name) AGAINST ('".$match_boolean_str."' IN BOOLEAN MODE) 
				) OR lres.resident_ref LIKE '".$_REQUEST['resident_search']."%' 
				) AND ";
				
				$nothing = ") OR (
					MATCH (lres.resident_ref) AGAINST ('".$match_str."') OR 
					MATCH (lres.resident_ref) AGAINST ('".$match_boolean_str."' IN BOOLEAN MODE) 
				)) AND ";
			}
		}
	}
	$sql = "
	SELECT res.rmc_num, res.resident_num, res.resident_name, res.is_resident_director, rex.is_demo_resident_account, lres.resident_ref, lres.resident_lookup 
	FROM cpm_residents res, cpm_residents_extra rex, cpm_lookup_residents lres
	WHERE 
	".$rmc_clause." 
	".$main_clause." 
	lres.resident_lookup=res.resident_num AND 
	lres.resident_lookup=rex.resident_num AND
	(res.resident_status ='Current' OR res.resident_is_active='1')  
	ORDER BY res.resident_name ASC
	";
	//print $sql;
	//exit;
	$result = @mysql_query($sql);
	$num_res = @mysql_num_rows($result);
	if($num_res < 1){
		?>
		<tr>
			<td colspan="3" class="style1" style="padding:5px;"><img src="../images/about_16.gif" align="absmiddle">&nbsp;<span class="style2">The system could not find any Residents for this RMC</span></td>
		</tr>
		<?
	}
	else{
		while($row = @mysql_fetch_row($result)){
			?>
			<tr onClick="parent.document.forms[0].rmc_num.value='<?=$row[0]?>';parent.document.forms[0].resident_num.value='<?=$row[1]?>';parent.changeResident();" bgcolor="#f3f3f3" onMouseOver="this.style.background='#D5E2FF'" onMouseOut="this.style.background='#f3f3f3'">
			<td width="84" class="style1" style="cursor:hand;border-bottom:1px solid #cccccc;border-right:1px solid #cccccc; vertical-align:top;"><? if($row[3] == "Y"){print "Director";}elseif($row[4] == "Y"){print "Demo";}else{print "Standard";}?></td>
			<td width="75" class="style1" style="cursor:hand;border-bottom:1px solid #cccccc;border-right:1px solid #cccccc; vertical-align:top;"><?=$row[5]?>&nbsp;</td>
			<td width="262" class="style1" style="cursor:hand;border-bottom:1px solid #cccccc;border-right:1px solid #cccccc; vertical-align:top;"><? print substr($row[2],0,35);if(strlen($row[2]) > 35){print "...";}?>&nbsp;</td>
			</tr>
			<?
		}
	}
}
else{
	?>
	<tr>
		<td colspan="3" class="style1" style="padding:5px;"><img src="../images/about_16.gif" align="absmiddle">&nbsp;<span class="style2">Enter the Lessee name/number in the field above</span></td>
	</tr>
	<?
}
?>
</table>
</body>
</html>