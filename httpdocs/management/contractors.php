<?
require("../utils.php");
require($UTILS_FILE_PATH."management/gen_password.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."contractors.class.php");
require_once($UTILS_CLASS_PATH."security.class.php");
$website = new website;
$contractors = new contractors('');
$security = new security;

// Determine if allowed access into content management system
$website->allow_cms_access();

// Check access privilege
if($_SESSION['allow_contractor'] != 1){header("Location:index.php");}

#===================================
# Get user
#===================================

if($_REQUEST['whichaction'] == "edit"){
	$user_id = $_REQUEST['user_id'];
	
	$result = $contractors->get_user($user_id);
	$num_rows = @mysql_num_rows($result);

	if($num_rows > 0){
		$result_array['success'] = 'Y';
		while($row = @mysql_fetch_array($result)){
			$result_array['name'] = $row['cpm_contractors_user_name'];
			$result_array['email'] = $row['cpm_contractors_user_email'];
			$result_array['disabled'] = strtolower($row['cpm_contractors_user_disabled']);
		}
	}else{
		$result_array['success'] = 'N';	
	}
	
	echo json_encode($result_array);
	exit;
}

#===================================
# Get contractor
#===================================

if($_REQUEST['whichaction'] == "contractor"){
	$contractors = new contractors($_REQUEST['contractor_id']);
	$result_array['email'] = $contractors->contractor_email;	
	$result_array['disabled'] = $contractors->contractor_disabled;	
	
	echo json_encode($result_array);
		
	exit;
}

#===================================
# Get Users
#===================================

if($_REQUEST['whichaction'] == "users"){
	$result = $contractors->get_users($_REQUEST['contractor_id']);
	$num_rows = @mysql_num_rows($result);
	
	$output = '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_results">';

	if($num_rows > 0){
		$result_array['success'] = 'Y';
		while($row = @mysql_fetch_array($result)){

			$background = '';
			
			if($row['cpm_contractors_user_disabled'] == 'True'){
				$background = ' style="background-color:#e94f4f;color:#FFFFFF;"';
			}
			
			$output .= '<tr onclick="edit_user('."'".$row['cpm_contractors_user_ref']."'".');">';
			$output .= '<td width="90"'.$background.'>'.$row['cpm_contractors_user_ref'].'</td>';
			$output .= '<td width="180"'.$background.'>'.$row['cpm_contractors_user_name'].'</td>';
			$output .= '<td width="180"'.$background.'>'.$row['cpm_contractors_user_email'].'</td>';
			$output .= '</tr>';
		}
	}else{
		$result_array['success'] = 'N';	
	}
	
	$output .= '</table>';
	
	$result_array['results'] = $output;
	
	$last_login = '';
	
	$result = $contractors->get_user($_REQUEST['contractor_id']);
	$num_rows = @mysql_num_rows($result);
	if($num_rows > 0){
		while($row = @mysql_fetch_array($result)){
			$last_login = $row['last_login'];
		}
	}
	
	$result_array['last_login'] = $last_login;
	
	echo json_encode($result_array);
		
	exit;
}

#===================================
# Get contractors
#===================================

if($_REQUEST['whichaction'] == "contractors"){
	
	$output = '<table width="486" border="0" cellpadding="0" cellspacing="0" class="table_results">';
	
	$result = $contractors->contractor_list($_REQUEST);
	$num_results = @mysql_num_rows($result);
	if($num_results > 0){			
		while($row = @mysql_fetch_array($result)){	
			$output .= '<tr id="'.$row['cpm_contractors_qube_id'].'" onclick="show_details('."'".$row['cpm_contractors_qube_id']."'".');">';
			$output .= '<td width="100">'.$row['cpm_contractors_qube_id'].'</td><td width="195">'.$row['cpm_contractors_name'].'</td></tr>';
		}
	}
	
	$output .= '</table>';
	
	$result_array['results'] = $output;
	
	echo json_encode($result_array);
		
	exit;
}

#===================================
# Check new user
#===================================

if($_REQUEST['whichaction'] == "check"){
	
	$result_array = $contractors->check_user($_REQUEST['contractor_id'], $_REQUEST);
	
	echo json_encode($result_array);
	exit;
}

#===================================
# Reset Password
#===================================

if($_REQUEST['whichaction'] == "reset"){
	
	$result_array = $contractors->reset_password($_REQUEST);
	
	echo json_encode($result_array);
	exit;
}

#===================================
# Save user
#===================================

if($_REQUEST['whichaction'] == "save"){
	
	$result_array = $contractors->save($_REQUEST['contractor_id'], $_REQUEST);
	
	echo json_encode($result_array);
	exit;
}

#===================================
# Save contractor
#===================================

if($_REQUEST['whichaction'] == "save_contractor"){
	
	$result_array = $contractors->save_contractor($_REQUEST['contractor_id'], $_REQUEST);
	
	echo json_encode($result_array);
	exit;
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>RMG Living - Contractors</title>
	<link href="../styles.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="/css/admin_extra.css"/>
	<link rel="stylesheet" type="text/css" href="/css/custom-theme/jquery-ui-1.8.16.custom.css"/>
	<script type="text/javascript" language="JavaScript" src="/library/jscript/jquery-1.6.2.min.js"></script>
	<script type="text/javascript" language="JavaScript" src="/library/jscript/jquery-ui-1.8.16.custom.min.js"></script>
	<script language="JavaScript" type="text/javascript">
		$(document).ready(function(){			
			$("#processing_dialog").dialog({  
				modal: true,
				open: function(event, ui) { $(".ui-dialog-titlebar-close").hide();},
				autoOpen: false
			});	
			
			$("#error_dialog").dialog({  
				modal: true,
				open: function(event, ui) { $(".ui-dialog-titlebar-close").show();},
				buttons: {
					Ok: function() {
						$(this).dialog('close');
					}
				},
				autoOpen: false
			});	
			
			load_anchors();
			
			$("#results_container").hide();	
			$("#details_container").hide();	
			$("#last_login").hide();
		});
		
		function load_anchors(){
			$('A[rel="_blank"]').each(function(){
				$(this).unbind('click');
				
				$(this).bind("click", function(){
					window.open($(this).attr('href'),"PASSWIN","width=730, height=550, scrollbars=yes, resizable=yes, status=yes");
					return false;
				});
			});
		}
		
		function do_search() {
			$("#processing_dialog").dialog('open');
			$('#whichaction').val('contractors');
			
			$.post("contractors.php", 
			$("#form1").serialize(), 
			function(data){	
				$("#results_container").show();	
				$('#results').html(data['results']);
				$("#processing_dialog").dialog('close');
				
				var total_view_height = 0;
				
				$("#results table tr").each(function(){
		            total_view_height += parseFloat( $(this).css("height").replace("px","") );
				});
				
				if(total_view_height > 300){
					total_view_height = 300; 
				}
			    
				$("#results").animate({			
					"height": total_view_height
				});
				
				$("#results").css("overflow","scroll");
				$("#results").css("overflow-x","hidden");
				//$("#dialog_audit").dialog("option", "title", "Jobs Within PO");
			}, 
			"json");
		}
		
		function save_contractor(){
			$("#processing_dialog").dialog('open');
			$('#whichaction').val('save_contractor');
			
			$.post("contractors.php", 
			$("#form1").serialize(), 
			function(data){	
				$("#processing_dialog").dialog('close');
				
				if(data["success"] == "Y"){
					$('#error_message').html("Successfully Saved!");
					$("#error_dialog").dialog("option", "title", "Saved!");
				}else{
					$('#error_message').html("Apologies. We seem to have a technical fault.");
					$("#error_dialog").dialog("option", "title", "Failed!");
				}
				$("#error_dialog").dialog('open');
			}, 
			"json");
		}
		
		function save(){
			$("#processing_dialog").dialog('open');
			$('#whichaction').val('check');
			
			$.post("contractors.php", 
			$("#form1").serialize(), 
			function(data){	
				$("#processing_dialog").dialog('close');
				
				if (data["results"] != ""){
					err_message = "<p><strong>Please correct the following errors:</strong></p><p>" + data["results"] + "</p>"
					$('#error_message').html(err_message);
					$("#error_dialog").dialog("option", "title", "Errors!");
					$("#error_dialog").dialog('open');
				}else{
					$("#add_dialog").dialog('close');
					do_save()
				}
			}, 
			"json");
		}
		
		function do_save(){
			$("#processing_dialog").dialog('open');
			$('#whichaction').val('save');
				
			$.post("contractors.php", 
			$("#form1").serialize(), 
			function(data){	
				$("#processing_dialog").dialog('close');
				if(data["success"] == "Y"){
					$('#error_message').html("Successfully Saved!");
					$("#error_dialog").dialog("option", "title", "Saved!");
				}else{
					$('#error_message').html("Apologies. We seem to have a technical fault.");
					$("#error_dialog").dialog("option", "title", "Failed!");
				}
				$("#error_dialog").dialog('open');
				get_users();
			}, 
			"json");
		}
		
		function reset_password(){
			$("#add_dialog").dialog('close');
			$("#processing_dialog").dialog('open');
			$('#whichaction').val('reset');
			
			$.post("contractors.php", 
			$("#form1").serialize(),
			function(data){	
				$("#processing_dialog").dialog('close');
			}, 
			"json");
		}
		
		function show_details(contractor_id) {
			
			$('#results table tr').each(function() {
				if($(this).attr('id') == contractor_id){
					$(this).find('td').css({'background-color':'#d5eaf8'});	
				}else{
					$(this).find('td').css({'background-color':'#FFFFFF'})
				}
			});	
			
			$('#last_link').html('<a href="password_slip.php?template=contractor&all=1&type=6&contractor_id=' + contractor_id + '" rel="_blank">(Re)Send Welcome Letter</a>');
			load_anchors();
			
			$("#processing_dialog").dialog('open');
			$('#whichaction').val('contractor');
			$('#contractor_id').val(contractor_id);
			
			$.post("contractors.php", 
			$("#form1").serialize(), 
			function(data){	
				$('#email_address').html(data['email']);
				if (data['disabled'] == 'False'){
					$('#chk_disabled').attr('checked', false);
				}else{
					$('#chk_disabled').attr('checked', true);
				}
				get_users();
			}, 
			"json");
		}
		
		function get_users() {
			$("#processing_dialog").dialog('open');
			$('#whichaction').val('users');
			
			$.post("contractors.php", 
			$("#form1").serialize(), 
			function(data){	
				$("#details_container").show();	
				$('#user_results').html(data['results']);
				$("#processing_dialog").dialog('close');
				
				var total_view_height = 0;
				
				$("#user_results table tr").each(function(){
		            total_view_height += parseFloat( $(this).css("height").replace("px","") );
				});
				
				if(total_view_height > 300){
					total_view_height = 300; 
				}
			            
				$("#user_results").animate({			
					"height": total_view_height
				});
				
				$("#user_results").css("overflow","scroll");
				$("#user_results").css("overflow-x","hidden");
				
				if (data['last_login'] == ""){
					$('#last_login').show();
				}else{
					$('#last_login').hide();
				}
			}, 
			"json");
		}
		
		function edit_user(id){
			
			$("#processing_dialog").dialog('open');
			
			var ad = $("#add_dialog").dialog({  
				modal: true,
				width: "500px",
				open: function(event, ui) { $(".ui-dialog-titlebar-close").show();},
				buttons: {
					Ok: function() {
						save();
					},
					"Reset Password": function() {
						reset_password();
					}
				},
				autoOpen: false
			});	
			
			ad.parent().appendTo('form:first');	
			
			$('#user_id').val(id);
			$('#whichaction').val('edit');
			
			$('#name').val("");
			$('#email').val("");
			$('#disabled_row').show();
			
			$.post("contractors.php", 
			$("#form1").serialize(),
			function(data){	
				$("#processing_dialog").dialog('close');
				if(data['success'] == 'Y'){
					$('#name').val(data['name']);
					$('#email').val(data['email']);
					if(data['disabled'] == 'true'){
						$("#disabled").prop("checked", true);
					}else{
						$("#disabled").prop("checked", false);
					}
					$("#add_dialog").dialog("option", "title", "Edit User");
					$('#add_dialog').dialog('open');
				}
			}, 
			"json");
		}
	</script>
	<style>
		.ui-widget { font-family: Verdana,Arial,sans-serif; font-size: 0.7em; }
	</style>
</head>
<body class="management_body">
<form id="form1" method="post">
	<div id="processing_dialog" title="Processing" style="display:none;">
		<p style="text-align:center;display:none;margin-bottom:15px;" id="processing_dialog_message"></p>
		<p style="text-align:center;"><img src="/images/ajax-loader.gif" /></p>
	</div>
	<div id="error_dialog" title="Errors" style="display:none;">
		<span id="error_message"></span>
	</div>
	<div id="add_dialog" title="Add User" style="display:none;">
		<table cellspacing="0" width="400">
			<tr>
				<td width="100">
					Name
				</td>
				<td width="300">
					<input type="text" id="name" name="name" />
				</td>
			</tr>
			<tr>
				<td width="200">
					Email
				</td>
				<td width="300">
					<input type="text" id="email" name="email" />
				</td>
			</tr>
			<tr id="disabled_row">
				<td width="200">
					Disabled
				</td>
				<td width="300">
					<input type="checkbox" id="disabled" name="disabled" value="True" />
				</td>
			</tr>
		</table>
	</div>
	<? require($UTILS_FILE_PATH."management/menu.php");?>
	<table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr valign="top">
			<td>
				<table border="0" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="border: 1px #000000 solid" width="100%">
					<tr>
						<td align="center"><img src="../images/management/icons/contractors_icon_new.gif" width="90" height="50" /></td>
					</tr>
					<tr>
						<td align="center"><a href="index.php"><b>&lt;&lt; Back to main menu</b></a></td>
					</tr>
					<tr>
						<td bgcolor="#ffffff" valign="top" align="center"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr valign="top">
			<td>&nbsp;</td>
		</tr>
		<tr valign="top">
			<td>
				<table width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="border: 1px #333333 solid">
					<tr>
						<td colspan="3"><strong>Search for a Contractor</strong></td>
					</tr>
					<tr>
						<td>Contractor name/reference&nbsp;&nbsp;</td>
						<td><input id="search_term" name="search_term" type="text" size="30" /></td>
						<td><input onclick="do_search()" type="button" name="go_button" value=" Go " /></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr valign="top">
			<td>&nbsp;</td>
		</tr>
		<tr id="results_container">
			<td>
				<table width="100%" border="0" cellpadding="0" cellspacing="0"  bgcolor="#FFFFFF" style="border: 1px #333333 solid; margin-bottom:15px;">
					<tr>
						<td style="padding:10px;">
							<table width="100%" border="0" cellpadding="0" cellspacing="0" class="header_results">
								<tr>
									<td width="95">Contractor Ref</td>
									<td width="195">Contractor Name</td>
								</tr>
							</table>
							<div id="results" style="overflow-y: scroll; overflow-x: hidden;">
							</div>
							</td>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr id="details_container">
			<td>
				<table width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="border: 1px #333333 solid; margin-bottom:15px;">
					<tr>
						<td width="33%">
							Email Address
						</td>
						<td>
							<span id="email_address"></span>
						</td>
					</tr>
					<tr>
						<td width="33%">
							Disabled
						</td>
						<td>
							<input type="checkbox" id="chk_disabled" name="chk_disabled" />
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<input type="button" id="btn_save" name="btn_save" onclick="save_contractor()" value=" Save " />
						</td>
					</tr>
				</table>
				<table width="100%" border="0" cellpadding="0" cellspacing="0"  bgcolor="#FFFFFF" style="border: 1px #333333 solid; margin-bottom:15px;">
					<tr>
						<td align="center" style="padding:10px;"><strong>Users</strong></td>
					</tr>
					<tr>
						<td style="padding:10px;">
							<table width="100%" border="0" cellpadding="0" cellspacing="0" class="header_results">
								<tr>
									<td width="90">Username</td>
									<td width="180">Name</td>
									<td width="180">Email</td>
								</tr>
							</table>
							<div id="user_results">
							</div>
							<div>
								<div style="background-color:#e94f4f; text-align:center; line-height:20px; color:#FFFFFF; border:1px solid #999; width:55px; height:20px; float:left; margin:5px 8px 0 0;">
									Disabled
								</div>
								<div style="background-color:#FFFFFF; text-align:center; line-height:20px; border:1px solid #999; width:55px; height:20px; float:left; margin:5px 8px 0 0;">
									Enabled
								</div>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr id="last_login">
			<td>
				<table width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="border: 1px #333333 solid; margin-bottom:15px;">
					<tr>
						<td align="center" id="last_link"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="border: 1px #333333 solid">
					<tr>
						<td align="center"><a href="contractor_reports.php"><img src="../images/management/icons/reports_icon_new.gif" width="70" height="50" /></a></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<input type="hidden" name="contractor_id" id="contractor_id" />
	<input type="hidden" name="user_id" id="user_id" />
	<input type="hidden" name="whichaction" id="whichaction" />
</form>
</body>
</html>
