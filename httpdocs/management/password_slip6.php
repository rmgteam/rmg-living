<?
ini_set("max_execution_time", "300");
ob_start();
require("utils.php");
require_once($UTILS_CLASS_PATH."letter.class.php");
require_once($UTILS_CLASS_PATH."resident.class.php");
require_once($UTILS_CLASS_PATH."rmc.class.php");
require_once($UTILS_CLASS_PATH."unit.class.php");
require_once($UTILS_CLASS_PATH."data.class.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."contractors.class.php");
require_once($UTILS_CLASS_PATH."security.class.php");
require_once($UTILS_CLASS_PATH."encryption.class.php");
require_once($UTILS_CLASS_PATH."letters/letters.class.php");
$website = new website;
$rmc = new rmc();
$unit = new unit();
$resident = new resident();
$letter = new letter();
$security = new security;
$crypt = new encryption_class;
$data = new data();

// Determine if allowed access into content management system
$website->allow_cms_access();


//=============================================
// Update print job table
//=============================================
if($_REQUEST['print_status'] == "done"){
	
	$thistime = time();
	
	
	$resident_nums = explode(",", $_POST['resident_nums']);
	$resident_passwords = explode(",", $_POST['resident_passwords']);
	
	for($i=0;$i < count($resident_nums);$i++){
		
		$contractors = new contractors($resident_nums[$i]);
		
		if(strtoupper($contractors->contractor_pcm) != 'POST'){
			$password = $resident->get_rand_id(8, 'ALPHA', 'UPPER');
			
			$letter->set_contractor($contractors);
			
			$letter->set_body("letter_cpm_living_welcome_contractors.php");
			
			// Make letter reference
			$your_ref = stripslashes($resident_nums[$i]);
		
			//Use the appropriate letter format
			ob_start();
			require($letter->get_body());
			$letter_content = ob_get_contents();
			ob_end_clean();
			
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= 'From: customerservice@rmguk.com' . "\r\n";
			$headers .= 'Reply-To: customerservice@rmguk.com' . "\r\n";
			$headers .= 'X-Mailer: PHP/' . phpversion();
			
			$sql = "INSERT INTO cpm_mailer SET
			mail_to = '".$contractors->contractor_email."',
			mail_from = 'customerservice@rmguk.com',
			mail_subject = 'Welcome to RMG Living',
			mail_headers = '".$headers."',
			mail_message = '".$letter_content."'";
			
			@mysql_query($sql);
		}
		
		$sql = "UPDATE cpm_contractors SET 
		cpm_contractors_letter_sent = 'Y' 
		WHERE cpm_contractors_qube_id = '".$resident_nums[$i]."'";
		@mysql_query($sql);
		
		$the_password = $resident_passwords[$i];
		
		if(strtoupper($contractors->contractor_pcm) != 'POST'){
			$the_password = $password;
		}
		
		$sql = "SELECT *
		FROM cpm_contractors_user
		WHERE cpm_contractors_user_ref = '".$_POST['contractor_id']."'";
		$result = @mysql_query($sql);
		$num_rows = @mysql_num_rows($result);
	
		if($num_rows == 0){
			$sql = "INSERT INTO cpm_contractors_user SET 
			cpm_contractors_user_ref = '".$resident_nums[$i]."', 
			cpm_contractors_user_qube_ref = '".$resident_nums[$i]."', 
			cpm_contractors_user_disabled = 'False',
			cpm_contractors_user_parent = '0', ";
		}else{
			$sql = "UPDATE cpm_contractors_user SET ";
		}
		
		$sql .= "cpm_contractors_user_password = '".$security->clean_query($crypt->encrypt($UTILS_DB_ENCODE, $the_password))."'";
		
		if($num_rows > 0){
			$sql .= "WHERE cpm_contractors_user_ref = '".$_POST['contractor_id']."'";
		}
		
		@mysql_query($sql);
		
		$date = new DateTime();
		
		$sql = "
		INSERT INTO cpm_print_contractors SET
		contractor_id = '".$resident_nums[$i]."',
		user_name = '".$_SESSION['user_id']."',
		print_job_time = '".$date->format('Y-m-d H:i:s')."'";
		@mysql_query($sql);
			
		
	}
}

$thistime = time();

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles.css" rel="stylesheet" type="text/css">
<link href="../css/letter_print.css" rel="stylesheet" type="text/css" media="print">
<style type="text/css">
<!--
.style4 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
}
.style11 {
	font-size: 10px;
	font-weight: bold;
	font-family: Verdana, Arial, Helvetica, sans-serif;
}
.style12 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #036;
}

-->
</STYLE>

<script language="javascript">
function print_slip(){

	document.getElementById('header_table').style.display='none';
	document.getElementById('done_table').style.display='none';
	document.getElementById('print_table').style.display='none';
	document.getElementById('warning_table').style.display='none';
	document.getElementById('spacer_table').style.display='none';
	window.print();
	document.getElementById('header_table').style.display='block';
	document.getElementById('done_table').style.display='block';
	document.getElementById('print_table').style.display='block';
	document.getElementById('warning_table').style.display='block';
	document.getElementById('spacer_table').style.display='block';
}

function confirm_print(){
	document.forms[0].action = "password_slip6.php";
	document.forms[0].target = "_self";
	document.forms[0].submit();
}
</script>
</head>
<body>
<? if($_REQUEST['print_status'] == "done"){?>
<table width="100%" height="100%" border="0" align="center" cellpadding="0" cellspacing="10" bgcolor="#C5D9B3" id="close_table" style="border:1px solid #666666;">
<tr onMouseOver="this.style.cursor='hand'">
<td align="center" onClick="window.close();"><p class="style11"><? if($_REQUEST['type'] == "resident"){?>The record for this resident has<? }else{?>All resident records have<? }?> been updated. Click <a href="#" onClick="window.close();">HERE</a> to close this window.</p>
  </td>
</tr>
</table>
<? exit;}?>

<?
//==========================================================
// Collect dynamic data to populate password slip
//==========================================================

// Get single resident print job
// Get single RMC print job

	
if($_REQUEST['contractor_id'] != ""){	
	$sql_resident = "
	SELECT * 
	FROM cpm_contractors
	WHERE 
	cpm_contractors_qube_id = '".$_REQUEST['contractor_id']."'
	LIMIT 1 ";		
}else{
	$sql_resident = "
	SELECT * 
	FROM cpm_contractors
	WHERE 
	cpm_contractors_letter_sent = 'N'
	LIMIT 100 ";	
}

$result_resident = @mysql_query($sql_resident);
$num_slips = @mysql_num_rows($result_resident);
?>
<form action="password_slip6.php" method="post">
<div id="header_table" class="style12" style="display:none;height:20px;vertical-align:top;border:1px solid #666666;padding:10px;">
	<div style="width:500px;float:left;">There are&nbsp;<span id="num_to_print_off"></span>&nbsp;password slips to print off</div>	
</div>

<table id="print_table" width="100%" border="0" cellpadding="0" cellspacing="10" bgcolor="#CFD8EB" style="display:none;border:1px solid #666666;">
  <tr onMouseOver="this.style.cursor='hand'">
    <td onClick="print_slip();" style="text-align:left;"><span class="style11">STEP 1: </span><span class="style4">Click here to print this batch of password slips</span></td>
  </tr>
</table>
<table id="warning_table" width="100%" border="0" cellpadding="0" cellspacing="10" bgcolor="#CFD8EB" style="display:none;border:1px solid #666666;">
  <tr>
    <td style="text-align:left;"><span class="style11">STEP 2: </span><span class="style4">Do</span><span class="style11"> NOT</span><span class="style4"> close this window using the close button! You will be given a specific close screen after completeing step 3.</span></td>
  </tr>
</table>
<table id="done_table" width="100%" border="0" cellpadding="0" cellspacing="10" bgcolor="#CFD8EB" style="display:none;border:1px solid #666666;">
  <tr onMouseOver="this.style.cursor='hand'">
    <td onClick="confirm_print();" style="text-align:left;"><span class="style11">STEP 3: </span><span class="style4">Click here to confirm that <strong>ALL</strong> the password slips have printed successfully </span></td>
  </tr>
</table>

<table id="spacer_table" width="100%" border="0" align="center" cellpadding="0" cellspacing="5">
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
<?
//=================================================
// Start printing the letters to screen
//=================================================
$this_slip_num = 1;
$email_slips = 1;
$result_resident = mysql_query($sql_resident);

$letter_pdf = new letters('P', 'mm', 'A4');
$letter_pdf->SetDefaultFont('Arial', '', 10);
$letter_pdf->setStyle("b","Arial","B");
$letter_pdf->setStyle("i","Arial","I");
$letter_pdf->bMargin = 15;
$letter_pdf->header_template = "headed_paper";
$letter_pdf->line_height = 4.5;
$letter_pdf->header_email = $UTILS_INFO_EMAIL;
$letter_pdf->header_phone = $UTILS_TEL_MAIN_TEL;

while($row_resident = mysql_fetch_array($result_resident)){
	$loop_ts = microtime(true);
	
	if ($this_slip_num <= 1) {
		$letter_pdf->parse_html("<p>&nbsp;</p>");
	}

	
	$letter_pdf->include_header = true;
	$letter_pdf->SetMargins(12.7, 0, 12.7);
	echo 'came';exit;
	$letter_pdf->add_page();
			
	$password = $resident->get_rand_id(8, 'ALPHA', 'UPPER');
	
	$all_resident_nums .= $row_resident['cpm_contractors_qube_id'].",";
	
	$all_resident_passwords .= $password.",";
	
	$contractors = new contractors($row_resident['cpm_contractors_qube_id']);
	
	
	$set_contractors = $letter->set_contractors($contractors);
	
	$letter_pdf->parse_html($set_contractors);
	
	
	$letter->set_body("letter_cpm_living_welcome_contractors.php");
	
	$letter_pdf->parse_html($letter->get_body);
	
	
	
	// Make letter reference
	$your_ref = stripslashes($row_resident['cpm_contractors_qube_id']);

	//Use the appropriate letter format
	if(strtoupper($row_resident['cpm_contractors_pcm']) == 'POST'){
		$template_path = $UTILS_TEMPLATE_PATH."letter_A5_windowed.php";
		$letter_pdf->parse_html($template_path);
		if($this_slip_num < $num_slips){
			$letter_pdf->parse_html("<p class='break'>&nbsp;</p>");
		}
	
		$this_slip_num++;
		
	}else{
		$email_slips++;	
	}
	
}
$letter_pdf->Output();

$final_slip = "<strong>" . ($email_slips-1) . "</strong> password slips to be emailed and <strong>" . ($this_slip_num-1) . "</strong>";

if($all_resident_nums != ''){
	$all_resident_nums = substr($all_resident_nums,0,-1);
}
if($all_resident_passwords != ''){
	$all_resident_passwords = substr($all_resident_passwords,0,-1);
}

?>
<input type="hidden" name="print_status" id="print_status" value="done">
<input type="hidden" name="resident_nums" id="resident_nums" value="<?=$all_resident_nums?>">
<input type="hidden" name="resident_passwords" id="resident_passwords" value="<?=$all_resident_passwords?>">
<input type="hidden" name="rmc_num" id="rmc_num" value="<?=$_REQUEST['rmc_num']?>">
<input type="hidden" name="rmc_nums" id="rmc_nums" value="<?=$_REQUEST['rmc_nums']?>">
<input type="hidden" name="contractor_id" id="contractor_id" value="<?=$_REQUEST['contractor_id']?>">
<input type="hidden" name="type" id="type" value="<?=$_REQUEST['type']?>">
<input type="hidden" name="template" id="template" value="<?=$_REQUEST['template']?>">
<input type="hidden" name="whichaction" id="whichaction">
<input type="hidden" name="is_print_history" id="is_print_history" value="<?=$_REQUEST['is_print_history']?>">
</form>

<script language="javascript">
document.getElementById('header_table').style.display = "block";
<? if($num_slips > 0){?>
document.getElementById('print_table').style.display = "block";
document.getElementById('warning_table').style.display = "block";
document.getElementById('done_table').style.display = "block";
<? }?>
document.getElementById('num_to_print_off').innerHTML = '<?=$final_slip?>';
</script>
</body>

</html>
<? ob_flush(); ?>
