<?
ini_set("max_execution_time","60");
require("utils.php");
require_once($UTILS_CLASS_PATH."website.class.php");
$website = new website;

// Determine if allowed access into content management system
$website->allow_cms_access();

function list_reports(){
	$options = '<table width="480" border="0" cellpadding="3" cellspacing="0">';
	$sql = "SELECT *
	FROM cpm_report r 
	INNER JOIN cpm_report_access a 
	ON a.cpm_report_access_report_id = r.cpm_report_id
	WHERE 
	cpm_report_discon = 'N' AND 
	cpm_report_access_user_id = '".$_SESSION['user_id']."'";
	$result = @mysql_query($sql);
	$num_rows = @mysql_num_rows($result);
	if($num_rows > 0){
		while($row = @mysql_fetch_array($result)){
			
			if( $row['cpm_report_id'] == "1" ){continue;}
			
			$options .= '<tr><td width="235"><a href="javascript:;" class="link036" onclick="gen_report('."'".$row['cpm_report_id'];
			$options .= "'".')"><strong>'.$row['cpm_report_name'].'</strong></a></td></tr>';
		}
	}
	
	$options .= '</table>';
	
	return $options;
}

// Check access privilege
if($_SESSION['allow_report'] != 1){header("Location:index.php");}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>RMG Living - Report</title>
<link href="../styles.css" rel="stylesheet" type="text/css">
<script type="text/JavaScript">
var date_regex = /\d\d\/\d\d\/\d\d\d\d/;
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function gen_report(reportID){
	if (reportID == '1'){
		//document.forms[0].action = 'print_report.php';
	}else if (reportID == '2'){
		document.forms[0].action = 'board_report.php';
	}else if (reportID == '3'){
		document.forms[0].action = 'access_report.php';	
	}else if (reportID == '4'){
		document.forms[0].action = 'user_report.php';
	}else if (reportID == '5'){
		document.forms[0].action = 'property_report.php';
	}else if (reportID == '6'){
		document.forms[0].action = 'tenant_report.php';
	}else if (reportID == '7'){
		document.forms[0].action = 'optin_report.php';
	}else{
		document.forms[0].action = '';
	}
	document.forms[0].submit();
}
//-->
</script>
</head>
<body class="management_body">
<form method="post" action="print_report.php" target="_blank">
<? require($UTILS_FILE_PATH."management/menu.php");?>

<table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr valign="top">
	  <td><table width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="border: 1px #000000 solid">
        <tr>
          <td align="center"><img src="../images/management/icons/reports_icon_new.gif" width="70" height="50"></td>
        </tr>
        <tr>
          <td align="center"><a href="index.php"><b><< Back to main menu</b></a></td>
        </tr>
      </table></td>
    </tr>
	<tr valign="top">
	  <td>&nbsp;</td>
    </tr>
	<tr valign="top">
		<td>
			<table width="500" border="0" align="center" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="border: 1px #000000 solid">
				<tr valign="top">
					<td>
						<table border="0" cellpadding="0" cellspacing="0">
							<tr valign="top">
								<td>
									<?
										print list_reports();
									?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
		  </table>
	  </td>
	</tr>
</table>
<input type="hidden" name="whichaction" value="show_report" />
<input type="hidden" name="whichreport" value="" />
</form>
</body>
</html>
