<?
ini_set("max_execution_time","120");

require("utils.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."resident.class.php");
$website = new website;
$resident = new resident;

// Determine if allowed access into content management system
$website->allow_cms_access();

// Check access privilege
if($_SESSION['allow_report'] != 1 && $_SESSION['report_2'] != '1'){header("Location:index.php");}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>RMG Living - Active User Report</title>
<link href="../styles.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="/css/custom-theme/jquery-ui-1.8.16.custom.css"/>
<script type="text/javascript" language="JavaScript" src="/library/jscript/jquery-1.6.2.min.js"></script>
<script type="text/javascript" language="JavaScript" src="/library/jscript/jquery-ui-1.8.16.custom.min.js"></script>
<style type="text/css" media="screen">

body {
	padding:20px;
}
.norm_table {
	max-width:800px;
}
.style1 {font-size: 12px}
.style2 {
	color: #336633;
	font-size: 12px;
}
.style3 {
	color: #CC3333;
	font-size: 12px;
}

.ui-widget { font-family: Verdana,Arial,sans-serif; font-size: 0.7em; }
</style>
<style type="text/css" media="print">
body {
	padding:0;
}
.norm_table {
	max-width:800px;
}
.style1 {font-size: 12px}
.style2 {
	color: #336633;
	font-size: 12px;
}
.style3 {
	color: #CC3333;
	font-size: 12px;
}
#filter_table {
	display:none;
}
</style>
<script type="text/javascript">
function do_filter(){	
	document.getElementById('whichaction').value = "filter";
	document.form1.submit();
}

</script>
</head>

<body>

	<form id="form1" name="form1">
		<table id="filter_table" class="norm_table" border="0" align="center" cellpadding="8" cellspacing="0" style="background-color:#f1f1f1;border:1px solid #999999;margin-bottom:5px;">
			<tr>
				<td>
					Has logged in: <input type="checkbox" id="check_log" name="check_log" />
					<input type="button" name="filter_button" id="filter_button" value="Filter" onClick="do_filter()" />
				</td>
			</tr>
		</table>
		<?
		if($_REQUEST['whichaction'] == "filter"){
			
			$sql_join = '';
			$sql_query = '';
			
			if($_REQUEST['check_log'] == 'on'){
				$sql_join = "
				INNER JOIN cpm_residents_extra e ON e.resident_num = res.resident_num";
				
				$sql_query = "
				AND e.is_first_login = 'N'";
			}
			
			$sql_rep = "SELECT res.resident_name,
			res.is_resident_director,
			res.is_subtenant_account
			FROM cpm_residents res
			INNER JOIN cpm_rmcs r ON r.rmc_num = res.rmc_num
			" . $sql_join . "
			WHERE res.resident_is_active = '1'
			AND r.rmc_is_active = '1'
			AND res.resident_name <> 'RMG Test User'
			AND r.rmc_num <> '9999999'
			AND r.rmc_num <> '10003232'
			" . $sql_query . "
			ORDER BY res.is_resident_director ASC, res.is_subtenant_account ASC";
			//print $sql_rep;
			$result_rep = @mysql_query($sql_rep);
			$num_rep = @mysql_num_rows($result_rep);
			$row_counter=0;
			$break_counter=0;
			
			if($num_rep == 0){
		
			?>
			<table class="norm_table" border="0" align="center" cellpadding="4" cellspacing="0">
				<tr>
				<td><span class="style1">&nbsp;
				There are no transactions between the date(s) provided.
				</span></td>
				</tr>
			</table>
			<?
			}
			else{
		
			?>
			<table class="norm_table" border="0" align="center" cellpadding="4" cellspacing="0">
			  <tr>
				<td colspan="3"><span class="style1"><strong>Active User Report (RMG Living) printed: <?=date("d/m/y H:i:s", time())?></strong></span></td>
				<td width="92" colspan="3" align="right" ></td>
			</tr>
			  <tr>
				  <td colspan="6" height="25"></td>
			  </tr>
				<tr><td colspan="6" style="background-color:#333333;border-bottom:1px solid #333333;" height="1"></td></tr>
				<tr>
				  <td colspan="3" nowrap style="border-bottom:1px solid #333333;border-right:1px solid #cccccc;border-left:1px solid #cccccc;"><strong>Type</strong></td>
				  <td colspan="3" width="80" style="border-bottom:1px solid #333333;border-right:1px solid #cccccc"><strong>Number</strong></td>
				</tr>
				
				<?
				$no = 0;
				$total_no = 0;
				
				$row_rep = @mysql_fetch_array($result_rep);
				
				$old_director = $row_rep['is_resident_director'];
				$old_sub_tenant = $row_rep['is_subtenant_account'];
				
				mysql_data_seek($result_rep, 0);
				while($row_rep = @mysql_fetch_array($result_rep)){
				  
				  	if($old_director != $row_rep['is_resident_director'] || $old_sub_tenant != $row_rep['is_subtenant_account']){
				  		if($old_director == 'N' && $old_sub_tenant == 'N'){
				  			$type = "Tenant";
				  		}elseif($old_director == 'N' && $old_sub_tenant == 'Y'){
				  			$type = "Sub Tenant";
				  		}else{
				  			$type = "Director";
				  		}
					?>
					<tr>
					  <td colspan="3" nowrap style="border-bottom:1px solid #333333;border-right:1px solid #cccccc;border-left:1px solid #cccccc;"><?=$type;?></td>
					  <td colspan="3" width="80" style="border-bottom:1px solid #333333;border-right:1px solid #cccccc"><?=$no;?></td>
					</tr>
					<?
						$no = 0;
					}
					
					$no ++;
					$total_no ++;
					
					$old_director = $row_rep['is_resident_director'];
					$old_sub_tenant = $row_rep['is_subtenant_account'];
					
				}
		  		
				if($old_director == 'N' && $old_sub_tenant == 'N'){
		  			$type = "Tenant";
		  		}elseif($old_director == 'N' && $old_sub_tenant == 'Y'){
		  			$type = "Sub Tenant";
		  		}else{
		  			$type = "Director";
		  		}
		  		
				?>
				<tr>
				  <td colspan="3" nowrap style="border-bottom:1px solid #333333;border-right:1px solid #cccccc;border-left:1px solid #cccccc;"><?=$type;?></td>
				  <td colspan="3" width="80" style="border-bottom:1px solid #333333;border-right:1px solid #cccccc"><?=$no;?></td>
				</tr>
				<tr>
				  <td colspan="3" nowrap style="border-bottom:1px solid #333333;border-right:1px solid #cccccc;border-left:1px solid #cccccc;"><strong>Total</strong></td>
				  <td colspan="3" width="80" style="border-bottom:1px solid #333333;border-right:1px solid #cccccc"><?=$total_no;?></td>
				</tr>
				<?
			}
		}
		?>	
		</table>
		<input type="hidden" id="whichaction" name="whichaction" />
	</form>	
</body>
</html>
