<?
require("utils.php");
require($UTILS_FILE_PATH."management/gen_password.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."rmc.class.php");
$rmc = new rmc;
$rmc->set_rmc($_REQUEST['rmc_num']);
$website = new website;

// Determine if allowed access into content management system
$website->allow_cms_access();

// Check access privilege
if($_SESSION['allow_rmc'] != 1){header("Location:index.php");}


//===================================
// Generates approval stamp
//===================================
function gen_approval_stamp(){

	// Generate random approval id used when authenticating announcement
	$stamp_unique = "N";
	while($stamp_unique == "N"){
		$accept_stamp = get_rand_id(20, '', '');
		$sql = "SELECT announce_id FROM cpm_announcements WHERE announce_accept_stamp = '".$accept_stamp."'";
		$result = mysql_query($sql);
		$stamp_exists = mysql_num_rows($result);
		if($stamp_exists < 1){
			$stamp_unique = "Y";
		}
	}
	return $accept_stamp;
}


//===================================
// Generates reject stamp
//===================================
function gen_reject_stamp(){

	$stamp_unique = "N";
	while($stamp_unique == "N"){
		$reject_stamp = get_rand_id(20, '', '');
		$sql = "SELECT announce_id FROM cpm_announcements WHERE announce_reject_stamp = '".$reject_stamp."'";
		$result = mysql_query($sql);
		$stamp_exists = mysql_num_rows($result);
		if($stamp_exists < 1){
			$stamp_unique = "Y";
		}
	}
	return $reject_stamp;
}


//======================================
// Send approval email for announcement
//======================================
function send_announce_approve_email($accept_stamp, $reject_stamp){

	$rmc = new rmc;
	$rmc->set_rmc($_REQUEST['rmc_num']);
	
	// Accept/Reject URL's
	$accept_url = $GLOBALS['UTILS_HTTPS_ADDRESS']."management/announce_moderator.php?stamp=".$accept_stamp;
	$reject_url = $GLOBALS['UTILS_HTTPS_ADDRESS']."management/announce_moderator.php?stamp=".$reject_stamp;
	
	// Create email message
	$message = "Man. Co: ".$rmc->rmc['rmc_ref']."\r\n\r\nTitle:\r\n".addslashes($_REQUEST['announce_title'])."\r\n\r\nText:\r\n".addslashes($_REQUEST['announce_text'])."\r\n\r\nACCEPT: ".$accept_url."\r\n\r\nREJECT: ".$reject_url."\r\n\r\n"; 
		
	//send message to regional manager
	$rm_parts = explode(" ", $rmc->rmc['regional_manager']);
	$rm_email = strtolower($rm_parts[0].".".$rm_parts[1])."@rmguk.com";
	//$rm_email = "support@rmgliving.co.uk";
	if( !mail($rm_email, "RMG Living - Accept/Reject", $message, "From:".$_SESSION['user_email'])){
		mail("support@rmgliving.co.uk", "RMG Living - Accept/Reject", $message, "From:".$_SESSION['user_email']);
	}
}

Global $UTILS_TEL_MAIN;

#===================================
# Add anouncement
#===================================
if($_REQUEST['which_action'] == "add_announce"){
	
	$thistime = time();

	$announce_date = date("Ymd", $thistime);
	$add_success = "Y";
	
	// Generate accept/reject stamps
	$accept_stamp = gen_approval_stamp();
	$reject_stamp = gen_reject_stamp();


	// Insert announcment (pre-approval)
	$sql = "
	INSERT INTO cpm_announcements(
		rmc_num,
		user_id,
		added_ts,
		announce_title,
		announce_text,
		announce_date,
		announce_approved,
		announce_accept_stamp,
		announce_reject_stamp
	)
	VALUES(
		".$_REQUEST['rmc_num'].",
		".$_SESSION['user_id'].",
		'".$thistime."',
		'".addslashes($_REQUEST['announce_title'])."',
		'".str_replace('£', '&pound;', addslashes($_REQUEST['announce_text']))."',
		'".$announce_date."',
		'Y',
		'".$accept_stamp."',
		'".$reject_stamp."'
	)
	";
	mysql_query($sql) or $add_success = "N";
	
	// Send email to support team for approval
	if($add_success == "Y"){

		$last_id = '';
		
		$sql = "SELECT MAX(announce_id) as max_id FROM cpm_announcements";
		$result = @mysql_query($sql);
		$num_rows = @mysql_num_rows($result);
		if($num_rows > 0){
			while($row = @mysql_fetch_array($result)){
				$last_id = $row['max_id'];
			}
		}
		send_email($last_id, $_REQUEST['rmc_num'], $_REQUEST['announce_text']);
		
	}
	
}

function send_email($annouce_id, $rmc_num, $text){
	
	Global $UTILS_TEL_MAIN;
	
	$last_id = $annouce_id;
	
	$sql = "SELECT resident_num
		FROM cpm_residents
		WHERE rmc_num = ".$rmc_num;
	$result = @mysql_query($sql);
	while($row = @mysql_fetch_row($result)){
	
		$sql2 = "
			INSERT INTO cpm_announcements_read(
				announce_id,
				resident_num,
				announce_read
			)
			VALUES(
				".$last_id.",
				".$row[0].",
				'N'
			)
			";
		@mysql_query($sql2);
	}
	
	$sql3 = "
	UPDATE cpm_announcements SET
	announce_accept_stamp = '',
	announce_reject_stamp = '',
	announce_approved = 'Y'
	WHERE announce_id = '".$last_id."'";
	@mysql_query($sql3);
	
	
	//========================================
	// Send announcement to all lessees
	//========================================
	
	// Get Property Manager details
	$sql = "SELECT rmc_name, property_manager, managed_office FROM cpm_rmcs WHERE rmc_num = '".$rmc_num."'";
	$result = @mysql_query($sql);
	$row = @mysql_fetch_row($result);
	
	$from_email = "customerservice@rmguk.com";
	
	if($row[1] != ""){$property_manager = "\n".$row[1];}
	
	$subject = $row[0]." - Announcement";
	$message = "From: Residential Management Group Ltd
To:   All Lessees of ".$row[0]."
	
".str_replace('£', '&pound;', addslashes($text))."
	
Kind Regards,".$property_manager."
	
	Residential Management Group Ltd
	
	Customer Services Department
	Tel: '.$UTILS_TEL_MAIN.'
	Email: customerservice@rmguk.com
	";
	
	$sql = "SELECT DISTINCT email FROM cpm_residents res, cpm_residents_extra rex WHERE res.resident_num=rex.resident_num AND (res.resident_status = 'Current' OR res.resident_is_active = '1') AND rex.email <> '' AND rex.email <> 'webmaster@rmgliving.co.uk' AND rex.announce_optin='Y' AND res.rmc_num = '".$rmc_num."'";
	$result = @mysql_query($sql);
	$mail_counter = 0;
	while($row = @mysql_fetch_row($result)){
		$mail_counter++;
		@mail($row[0],$subject,$message,"From:".$from_email);
	}
}

#===================================
# Modify announcement
#===================================
if($_REQUEST['which_action'] == "modify_announce"){
	
	$save_success = "Y";
	
	// Generate accept/reject stamps
	$accept_stamp = gen_approval_stamp();
	$reject_stamp = gen_reject_stamp();
	
	// Update announcement (set for awaiting approval)
	$sql = "
	UPDATE cpm_announcements SET
		user_id = ".$_SESSION['user_id'].",
		added_ts = '".$thistime."',
		announce_title = '".addslashes($_REQUEST['announce_title'])."',
		announce_text = '".addslashes($_REQUEST['announce_text'])."',
		announce_approved = 'Y',
		announce_accept_stamp = '".$accept_stamp."',
		announce_reject_stamp = '".$reject_stamp."'
	WHERE announce_id = ".$_REQUEST['announce_id']."
	";
	@mysql_query($sql) or $save_success = "N";
	
	// Send email to support team for approval
	if($save_success == "Y"){
		send_email($_REQUEST['announce_id'], $_REQUEST['rmc_num'], $_REQUEST['announce_text']);
	}

}


#===================================
# remove announcements
#===================================
if($_REQUEST['which_action'] == "remove_announce"){

	$sql = "DELETE FROM cpm_announcements WHERE announce_id = ".$_REQUEST['announce_id'];
	mysql_query($sql);
	$sql = "DELETE FROM cpm_announcements_read WHERE announce_id = ".$_REQUEST['announce_id'];
	mysql_query($sql);
	$_REQUEST['announce_id'] = "";
	
	print "
	<script type='text\javascript'>
	parent.document.forms[0].announce_title.value = '';
	parent.document.forms[0].announce_text.value = '';
	parent.document.forms[0].announce_id.value = '';
	</script>
	";

}

?>
<html>
<head>
<link href="../styles.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="/css/custom-theme/jquery-ui-1.8.16.custom.css"/>
	<script type="text/javascript" language="JavaScript" src="/library/jscript/jquery-1.6.2.min.js"></script>
	<script type="text/javascript" language="JavaScript" src="/library/jscript/jquery-ui-1.8.16.custom.min.js"></script>
</head>
<body>
<form action="announce.php">
<table border="0" cellpadding="3" cellspacing="0" width="100%">
<?

#===================================
# Get announcement
#===================================
if($_REQUEST['which_action'] == "get_announce"){

	$sql = "SELECT * FROM cpm_announcements WHERE announce_id = ".$_REQUEST['announce_id'];
	$result = @mysql_query($sql);
	$row_announce = @mysql_fetch_array($result);

	print "
	<script language=Javascript>
	parent.document.forms[0].announce_title.value = \"".$row_announce['announce_title']."\";
	$('#announce_text', parent.document).val(".json_encode($row_announce['announce_text']).");
	parent.document.forms[0].announce_id.value = \"".$row_announce['announce_id']."\";

	</script>
	";

}

#===============================================
# Collect all announcements for this RMC
#===============================================
$sql = "SELECT * FROM cpm_announcements WHERE rmc_num = ".$_REQUEST['rmc_num']." ORDER BY announce_date DESC";
$result = mysql_query($sql);
$num_announce = mysql_num_rows($result);
if($num_announce < 1){
	print "<tr><td>There are no announcements for this RMC</td></tr>";
}
else{
	while($row = @mysql_fetch_array($result)){
		?>
		<tr <? if($row['announce_approved'] == "N"){?>bgcolor="#FFD5D5"<? }else{?>bgcolor="#f3f3f3"<? }?>><td style="border-bottom:1px solid #cccccc"><?=substr($row['announce_date'],6,2)."/".substr($row['announce_date'],4,2)."/".substr($row['announce_date'],0,4)?> - <a href="#" onClick="document.forms[0].announce_id.value='<?=$row['announce_id']?>';document.forms[0].which_action.value='get_announce';document.forms[0].submit();"><? print substr($row['announce_title'], 0, 30);if(strlen($row['announce_title']) > 30){print "...";}?></a></td>
		  <td align="right" style="border-bottom:1px solid #cccccc"><input onClick="if(confirm('Remove announcement ?')){document.forms[0].announce_id.value='<?=$row['announce_id']?>';document.forms[0].which_action.value='remove_announce';document.forms[0].submit();}" type="button" name="Button" value="remove"></td>
		</tr>
		<?
	}
}
?>
</table>
<input type="hidden" id="announce_id" name="announce_id">
<input type="hidden" id="rmc_num" name="rmc_num" value="<?=$_REQUEST['rmc_num']?>">
<input type="hidden" name="which_action" value="remove_announce">
</form>
</body>
</html>