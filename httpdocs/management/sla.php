<?
require("utils.php");
require_once($UTILS_FILE_PATH."library/functions/check_file_extension.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."rmc.class.php");
$website = new website;
$rmc = new rmc;

// Determine if allowed access into content management system
$website->allow_cms_access();

// Check access privilege
if($_SESSION['allow_rmc'] != 1){header("Location:index.php");}

//=========================================
// Delete SLA file
//=========================================
if($_REQUEST['whichaction'] == "delete_sla"){

	$del_error = "N";

	$sql = "SELECT * FROM cpm_sla WHERE sla_id=".$_REQUEST['sla_file_id'];
	$result = @mysql_query($sql) or $del_error = "Y";
	$row = @mysql_fetch_array($result);
	$file_name = $row['sla_file'];

	$sql = "DELETE FROM cpm_sla WHERE sla_id=".$_REQUEST['sla_file_id'];
	@mysql_query($sql) or $del_error = "Y";
	
	if($del_error == "N"){
		@unlink($UTILS_PRIVATE_FILE_PATH."sla/$file_name");
	}
	
}


#=========================================
# Add SLA file
#=========================================
if($_REQUEST['which_action'] == "add_sla"){
	
	// Check that no SLA currently exists
	$sql = "SELECT * FROM cpm_sla WHERE rmc_num=".$_REQUEST['rmc_num'];
	$result = @mysql_query($sql);
	$sla_exists = @mysql_num_rows($result);
	if($sla_exists == 0){
	
		$file_stamp = time();
		
		# Save SLA file to correct location
		$file_ext[] = "pdf";
		if(check_file_extension($_FILES['sla_file']['name'], $file_ext)){
			$sla_file = "sla_".$rmc->file_friendly_name()."_".$file_stamp.".pdf";
			$sla_file_path = $UTILS_PRIVATE_FILE_PATH."sla/".$sla_file;
			copy($_FILES['sla_file']['tmp_name'], $sla_file_path);
	
			# Format dates
			$sla_from_date = explode("/", $_REQUEST['sla_from_date']);
			$sla_to_date = explode("/", $_REQUEST['sla_to_date']);
		
			# Insert SLA details
			$sql = "
			INSERT INTO cpm_sla(
				rmc_num,
				sla_file,
				sla_file_stamp,
				sla_from_date,
				sla_to_date
			)
			VALUES(
				".$_REQUEST['rmc_num'].",
				'".$sla_file."',
				'".$file_stamp."',
				'".$sla_from_date[2].$sla_from_date[1].$sla_from_date[0]."',
				'".$sla_to_date[2].$sla_to_date[1].$sla_to_date[0]."'
			)
			";
			mysql_query($sql);
		}
	}
		
}
?>
<html>
<head>
<link href="../styles.css" rel="stylesheet" type="text/css">
<?
// Alert user that they must remove existing SLA
if($_REQUEST['which_action'] == "add_sla" && $sla_exists > 0){
?>
<script language="javascript">
alert("There can be only one Management Agreement per Management Company. Please remove the existing one first before attempting to upload a new version.");
</script>
<?
}
?>
</head>
<body>
<form action="sla.php">
<table border="0" cellpadding="3" cellspacing="0" width="100%">
<?
#===============================================
# Collect all SLA's for this RMC
#===============================================
$sql = "SELECT * FROM cpm_sla WHERE rmc_num = ".$_REQUEST['rmc_num'];
$result = @mysql_query($sql);
$num_sla = @mysql_num_rows($result);
if($num_sla < 1){
	print "<tr><td>There are no SLA's for this RMC</td></tr>";
}
else{
	while($row = @mysql_fetch_array($result)){
		?>
		<tr bgcolor="#f3f3f3">
			<td style="border-bottom:1px solid #cccccc">SLA (<? print substr($row['sla_from_date'],6,2)."/".substr($row['sla_from_date'],4,2)."/".substr($row['sla_from_date'],0,4)." to ".substr($row['sla_to_date'],6,2)."/".substr($row['sla_to_date'],4,2)."/".substr($row['sla_to_date'],0,4);?>)</td>
		  <td align="right" style="border-bottom:1px solid #cccccc"><input onClick="if(confirm('Remove SLA for this RMC ?')){document.forms[0].sla_file_id.value='<?=$row['sla_id']?>';document.forms[0].submit();}" type="button" name="Button" value="remove"></td>
		</tr>
		<?
	}
}
?>
</table>
<input type="hidden" id="sla_file_id" name="sla_file_id">
<input type="hidden" id="rmc_num" name="rmc_num" value="<?=$_REQUEST['rmc_num']?>">
<input type="hidden" name="whichaction" value="delete_sla">
</form>
</body>
</html>