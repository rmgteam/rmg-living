<?
require("utils.php");
require($UTILS_FILE_PATH."management/gen_password.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."encryption.class.php");
$crypt = new encryption_class;
$website = new website;

// Determine if allowed access into content management system
$website->allow_cms_access();

// Check access privilege
if($_SESSION['allow_users'] != 1){header("Location:index.php");}

$send_email = "N";

//===================================
// Save exisiting member
//===================================
if($_REQUEST['which_action']=="add"){

	$save_error = "N";
	$password = get_rand_id(8, "", "UPPER");
	$sql = "
	INSERT INTO cpm_backend (
		f_name,
		l_name,
		email, 
		user_name,
		password,
		allow_pms,
		allow_rmc,
		allow_residents,
		allow_letters,
		allow_letters_toggle,
		allow_news,
		allow_passwords,
		allow_report,
		allow_developers,
		allow_letters_module,
		allow_users,
		allow_announcements,
		allow_contractor,
		member_id,
		is_sys_admin,
		user_type_id
	)
	VALUES(
		'".$_REQUEST['f_name']."',
		'".$_REQUEST['l_name']."',
		'".$_REQUEST['email']."',
		'".$_REQUEST['user_name']."',
		'".$crypt->encrypt($UTILS_DB_ENCODE, $password)."',
		".$_REQUEST['allow_pms'].",
		".$_REQUEST['allow_rmc'].",
		".$_REQUEST['allow_residents'].",
		".$_REQUEST['allow_letters'].",
		".$_REQUEST['allow_letters_toggle'].",
		".$_REQUEST['allow_news'].",
		".$_REQUEST['allow_passwords'].",
		".$_REQUEST['allow_report'].",
		".$_REQUEST['allow_developers'].",
		".$_REQUEST['allow_letters_module'].",
		".$_REQUEST['allow_users'].",
		".$_REQUEST['allow_announcements'].",
		".$_REQUEST['allow_contractor'].",
		1,
		'".$_REQUEST['is_sys_admin']."',
		".$_REQUEST['user_type_id']."
	)
	";
	mysql_query($sql) or $save_error = "Y";
	
	if($save_error == "N"){
		$sql = "SELECT LAST_INSERT_ID() FROM cpm_backend";
		$result = mysql_query($sql);
		$row = mysql_fetch_row($result);
		$_REQUEST['user_id'] = $row[0];
		$send_email = "Y";
		if ($_REQUEST['allow_report'] == '1'){
			check_reports($_REQUEST['user_id']);
		}
	}else{
		echo "error";
	}
	
}
elseif($_REQUEST['which_action']=="modify"){
	
	$save_error = "N";
	$sql = "
	UPDATE cpm_backend SET
	f_name='".$_REQUEST['f_name']."',
	l_name='".$_REQUEST['l_name']."',
	email = '".$_REQUEST['email']."',
	user_name= '".$_REQUEST['user_name']."',
	allow_pms=".$_REQUEST['allow_pms'].",
	allow_rmc=".$_REQUEST['allow_rmc'].",
	allow_residents=".$_REQUEST['allow_residents'].",
	allow_letters=".$_REQUEST['allow_letters'].",
	allow_letters_toggle=".$_REQUEST['allow_letters_toggle'].",
	allow_news=".$_REQUEST['allow_news'].",
	allow_passwords=".$_REQUEST['allow_passwords'].",
	allow_report=".$_REQUEST['allow_report'].",
	allow_developers=".$_REQUEST['allow_developers'].",
	allow_letters_module=".$_REQUEST['allow_letters_module'].",
	allow_users=".$_REQUEST['allow_users'].",
	allow_announcements=".$_REQUEST['allow_announcements'].",
	allow_contractor=".$_REQUEST['allow_contractor'].",
	is_sys_admin='".$_REQUEST['is_sys_admin']."',
	user_type_id = ".$_REQUEST['user_type_id']."
	WHERE user_id = ".$_REQUEST['user_id']."
	";
	mysql_query($sql) or $save_error = "Y";
	
	if ($_REQUEST['allow_report'] == '1'){
		check_reports($_REQUEST['user_id']);
	}else{
		clear_reports($_REQUEST['user_id']);
	}

}

if($send_email == "Y"){

	$msg = "
Your login details for RMG Living are as follows:

Username: ".$_REQUEST['user_name']."
Password: ".$password."


This email was generated automatically from the RMG Living Management System. If you have any queries about this email, please contact support@rmgliving.co.uk
	";
	//mail("joe.privett@rmgltd.co.uk", "RMG Living - Login details", $msg, "From: support@rmgliving.co.uk"); 
	mail($_REQUEST['email'], "RMG Living - Login details", $msg, "From: support@rmgliving.co.uk");
	
}

#===================================
# remove user
#===================================
if($_REQUEST['which_action']=="remove"){

	$sql = "DELETE FROM cpm_backend WHERE user_id = ".$_REQUEST['user_id'];
	@mysql_query($sql)||die(mysql_error());
	clear_reports($_REQUEST['user_id']);
	$_REQUEST['user_id'] = "";
}

#===================================
# Get user
#===================================
if($_REQUEST['user_id']<>""){

	$sql = "
	SELECT f_name, l_name, email, user_name, password, allow_pms, allow_rmc, allow_residents, allow_passwords, allow_news,
	allow_users, is_sys_admin, allow_report, allow_letters, allow_developers, user_type_id, allow_letters_module, allow_announcements, allow_contractor, allow_letters_toggle 
	FROM cpm_backend WHERE user_id=".$_REQUEST['user_id'];
	$result = @mysql_query($sql);
	$row_user = @mysql_fetch_row($result);
}

function check_reports($user_id){
	$sql = "
	SELECT *
	FROM cpm_report";
	$result = @mysql_query($sql);
	$num_rows = @mysql_num_rows($result);
	if($num_rows > 0){
		while($row = @mysql_fetch_array($result)){			
			if ($_REQUEST['chk_report_'.$row['cpm_report_id']] != $row['cpm_report_name']){
				$sql = "DELETE FROM cpm_report_access ";
				$sql .= "WHERE cpm_report_access_user_id = '".$user_id."' ";
				$sql .= "AND cpm_report_access_report_id = '".$row['cpm_report_id']."'";
			}else{
				$sql = "
				SELECT *
				FROM cpm_report_access
				WHERE cpm_report_access_user_id = '".$user_id."' AND
				cpm_report_access_report_id = '".$row['cpm_report_id']."'";
				$result2 = @mysql_query($sql);
				$num_rows = @mysql_num_rows($result2);
				if($num_rows > 0){
				}else{
					$sql = "INSERT INTO cpm_report_access SET ";
					$sql .= "cpm_report_access_user_id = '".$user_id."', ";
					$sql .= "cpm_report_access_report_id = '".$row['cpm_report_id']."'";
				}
			}
			mysql_query($sql) or $save_error = "Y";
		}
	}
	return true;
}

function clear_reports($user_id){
	$sql = "DELETE FROM cpm_report_access WHERE cpm_report_access_user_id = '".$user_id."'";
	mysql_query($sql) or $save_error = "Y";
	return true;
}

function get_reports(){
	$options = '<table width="100%" border="0" cellpadding="0" cellspacing="0">';
	
	$sql = "
	SELECT *
	FROM cpm_report";
	$result = @mysql_query($sql);
	$num_rows = @mysql_num_rows($result);
	if($num_rows > 0){
		while($row = @mysql_fetch_array($result)){
			$checked = '';
			$sql2 = "SELECT *
					FROM cpm_report_access
					WHERE cpm_report_access_user_id = '".$_REQUEST['user_id']."'
					AND cpm_report_access_report_id = '".$row['cpm_report_id']."'";
			$result2 = @mysql_query($sql2);
			$num_rows2 = @mysql_num_rows($result2);
			if($num_rows2 > 0){
				while($row2 = @mysql_fetch_array($result2)){
					$checked = ' checked="checked"';
				}
			}
			$options .= '<tr><td width="235">'.$row['cpm_report_name'].'</td>';
			$options .= '<td><input id="chk_report_'.$row['cpm_report_id'].'" name="chk_report_'.$row['cpm_report_id'];
			$options .= '" value="'.$row['cpm_report_name'].'" type="checkbox"'.$checked.' /></td></tr>';
		}
	}
	
	$options .= '</table>';
	$options .= '<script language="javascript">toggleReports();</script>';
	
	return $options;
}

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>RMG Living - Buildings</title>
<link href="../styles.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function save(){

	var bad=0;

	if(document.forms[0].user_type_id.value=="-"){bad=1;}
	if(document.forms[0].f_name.value==""){bad=1;}
	if(document.forms[0].l_name.value==""){bad=1;}
	if(document.forms[0].user_name.value==""){bad=1;}
	if(document.forms[0].email.value==""){bad=1;}

	if(bad==1){alert("Please enter a value in all fields marked with *");return;}

	if(document.forms[0].user_id.value==""){
		if(!confirm("Add this user to the system?")){return;}
		document.forms[0].which_action.value="add";
	}
	else{
		if(!confirm("Save your changes?")){return;}
		document.forms[0].which_action.value="modify";
	}
	document.forms[0].submit();
}

function new_user(){
	document.location.replace("users.php");
}

function remove_user(){

	if(document.forms[0].user_id.value==""){alert("Please select a user to remove.");return;}
	if(!confirm("Remove this user?")){return;}
	document.forms[0].which_action.value="remove";
	document.forms[0].submit();
}

function changeUser(id){
	location.href="users.php?user_id="+id;
}

function change_pass(){
	
	if(confirm("Overwrite this users existing password?")){
		document.getElementById('password_frame').src = "user_password.php?user_id=<?=$_REQUEST['user_id']?>&gen=Y";
	}
}

function set_username(){
	var un = document.forms[0].email.value.split("@");
	document.forms[0].user_name.value = un[0];
	
}

function toggleReports(){
	if (document.getElementById('allow_report').value == '1'){
		document.getElementById('reports_tr').style.display = 'table-row';
	}else{
		document.getElementById('reports_tr').style.display = 'none';
	}
}

//-->
</script>
</head>
<body class="management_body">
<form method="post" action="users.php">
<iframe frameborder="0" height="0" id="password_frame" name="password_frame" scrolling="no" width="0" src="blank.php"></iframe>
<? require($UTILS_FILE_PATH."management/menu.php");?>
<table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr valign="top">
	  <td><table width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="border: 1px #000000 solid">
        <tr>
          <td align="center"><img src="../images/management/icons/users_icon_new.gif" width="70" height="50" border="0"></td>
        </tr>
        <tr>
          <td align="center"><a href="index.php"><b><< Back to main menu</b></a></td>
        </tr>
      </table></td>
    </tr>
	<tr valign="top">
	  <td>&nbsp;</td>
    </tr>
	<tr valign="top">
	  <td><table width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="border: 1px #999999 solid">
        <tr>
          <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td height="10"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="34%"><img src="../images/srch_16.gif" width="16" height="16" align="absmiddle">&nbsp;&nbsp;Search for a User </td>
                      <td width="56%"><input name="user_search" type="text" size="30"></td>
                      <td width="10%" align="right"><input onClick="document.getElementById('user_search_frame').src = 'users_search.php?user_search=' + document.forms[0].user_search.value;" type="button" name="go_button" value="  Go  "></td>
                    </tr>
                </table></td>
              </tr>
              <tr>
                <td height="10"></td>
              </tr>
              <tr>
                <td height="10"><iframe src="users_search.php" frameborder="1" height="100" id="user_search_frame" scrolling="yes" width="470"></iframe></td>
              </tr>
              <tr>
                <td height="10"></td>
              </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
	<tr valign="top">
	  <td>&nbsp;</td>
    </tr>
	<tr valign="top">
		<td>
			<table width="100%" border="0" cellpadding="10" cellspacing="0" bgcolor="#FFFFFF" style="border: 1px #666666 solid" align="center">
				<tr>
					<td>
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr><td colspan="3"><b>Users</b></td></tr>
							<tr><td colspan="3" height="10"></td></tr>
							<tr valign="top">
								<td>
									<table width="100%" border="0" cellpadding="3" cellspacing="0">
										<tr>
										  <td colspan="2">&nbsp;</td>
									  </tr>
										<tr>
										  <td width="50%">User type * </td>
										  <td>
										  <select name="user_type_id" id="user_type_id">
										  <option value="-" selected>-</option>
										  <?
										  $sql_ut = "SELECT * FROM cpm_user_types ORDER BY user_type ASC";
										  $result_ut = @mysql_query($sql_ut);
										  while($row_ut = @mysql_fetch_array($result_ut)){?>
										  <option value="<?=$row_ut['user_type_id']?>" <? if($row_user[15] == $row_ut['user_type_id']){print "selected";}?>><?=$row_ut['user_type']?></option>
										  <? }?>
									      </select>
										  </td>
									  </tr>
										<tr>
											<td width="27%">First Name *</td>
											<td width="73%"><input name="f_name" type="text" style="width:150px" value="<?=$row_user[0]?>"></td>
										</tr>
										<tr>
											<td>Last Name *</td>
											<td><input name="l_name" type="text" style="width:150px" value="<?=$row_user[1];?>"></td>
										</tr>
										<tr>
										  <td>Email*</td>
										  <td><input onBlur="set_username()" name="email" type="text" style="width:150px" value="<?=$row_user[2];?>"></td>
									  </tr>
										<tr>
											<td>User name *</td>
											<td><input name="user_name" type="text" style="width:150px" value="<?=$row_user[3];?>"></td>
										</tr>
										<tr>
										  <td valign="top">&nbsp;</td>
										  <td>&nbsp;</td>
									  </tr>
									  <? if($_REQUEST['user_id'] && $_REQUEST['user_id'] != ""){?>
										
										<tr>
										  <td colspan="2" valign="top"><input type="button" value="Generate New Password" onClick="change_pass()"></td>
									  </tr>
									  <? }?>
										<tr>
										  <td colspan="2">Note: You cannot view the password of a user </td>
									  </tr>
										<tr><td colspan="2"><hr></td></tr>
										<tr>
										  <td colspan="2"><b>Allow access to  these modules ? </b></td>
										</tr>
										<tr>
										  <td>PM Staff </td>
										  <td><select name="allow_pms">
                                            <option value="0" <? if($row_user[5]==0){?>selected<? }?>>No</option>
                                            <option value="1" <? if($row_user[5]==1){?>selected<? }?>>Yes</option>
                                          </select></td>
									  </tr>
										<tr>
											<td>RMC's</td>
											<td>
												<select name="allow_rmc">
													<option value="0" <? if($row_user[6]==0){?>selected<? }?>>No</option>
													<option value="1" <? if($row_user[6]==1){?>selected<? }?>>Yes</option>
												</select>
											</td>
										</tr>
										<tr>
											<td>Lessees</td>
											<td>
												<select name="allow_residents">
													<option value="0" <? if($row_user[7]==0){?>selected<? }?>>No</option>
													<option value="1" <? if($row_user[7]==1){?>selected<? }?>>Yes</option>
												</select>
											</td>
										</tr>
										<tr>
										  <td valign="top">Print Letters (RMC &amp; Lessee modules) </td>
										  <td valign="top"><select name="allow_letters">
                                            <option value="0" <? if($row_user[13]==0){?>selected<? }?>>No</option>
                                            <option value="1" <? if($row_user[13]==1){?>selected<? }?>>Yes</option>
                                          </select></td>
									  </tr>
									  <tr>
										  <td valign="top">Allow to switch letter printing on/off (RMC &amp; Lessee modules) </td>
										  <td valign="top"><select name="allow_letters_toggle">
                                            <option value="0" <? if($row_user[19]==0){?>selected<? }?>>No</option>
                                            <option value="1" <? if($row_user[19]==1){?>selected<? }?>>Yes</option>
                                          </select></td>
									  </tr>
										<tr>
										  <td>Issue password</td>
										  <td><select name="allow_passwords">
                                            <option value="0" <? if($row_user[8]==0){?>selected<? }?>>No</option>
                                            <option value="1" <? if($row_user[8]==1){?>selected<? }?>>Yes</option>
                                          </select></td>
									  </tr>
										<tr>
											<td>News</td>
											<td>
												<select name="allow_news">
													<option value="0" <? if($row_user[9]==0){?>selected<? }?>>No</option>
													<option value="1" <? if($row_user[9]==1){?>selected<? }?>>Yes</option>
												</select>
											</td>
										</tr>
										<tr>
										  	<td>Reports</td>
										  	<td>
											  	<select name="allow_report" id="allow_report" onchange="toggleReports()">
                                           	 		<option value="0" <? if($row_user[12]==0){?>selected<? }?>>No</option>
                                           	 		<option value="1" <? if($row_user[12]==1){?>selected<? }?>>Yes</option>
												</select>
										  	</td>
									  	</tr>
									  	<tr id="reports_tr">
									  		<td colspan="2">
												<?
													echo get_reports();
												?>
											</td>
									  	</tr>
										<tr>
										  <td>Developers</td>
										  <td><select name="allow_developers">
                                            <option value="0" <? if($row_user[14]==0){?>selected<? }?>>No</option>
                                            <option value="1" <? if($row_user[14]==1){?>selected<? }?>>Yes</option>
                                          </select></td>
									  </tr>
										<tr>
										  <td>Letters</td>
										  <td><select name="allow_letters_module">
                                            <option value="0" <? if($row_user[16]==0){?>selected<? }?>>No</option>
                                            <option value="1" <? if($row_user[16]==1){?>selected<? }?>>Yes</option>
                                          </select></td>
									  </tr>
										<tr>
											<td>Backend Users</td>
											<td>
												<select name="allow_users">
													<option value="0" <? if($row_user[10]==0){?>selected<? }?>>No</option>
													<option value="1" <? if($row_user[10]==1){?>selected<? }?>>Yes</option>
												</select>
											</td>
										</tr>
										<tr>
											<td>Announcements</td>
											<td>
												<select name="allow_announcements">
													<option value="0" <? if($row_user[17]==0){?>selected<? }?>>No</option>
													<option value="1" <? if($row_user[17]==1){?>selected<? }?>>Yes</option>
												</select>
											</td>
										</tr>
										<tr>
											<td>Contractors</td>
											<td>
												<select name="allow_contractor">
													<option value="0" <? if($row_user[18]==0){?>selected<? }?>>No</option>
													<option value="1" <? if($row_user[18]==1){?>selected<? }?>>Yes</option>
												</select>
											</td>
										</tr>
										<tr>
											<td>System Admin</td>
											<td>
												<select name="is_sys_admin" <? if($_SESSION['is_sys_admin'] != "Y"){?>disabled<? }?>>
													<option value="N" selected>No</option>
													<option value="Y" <? if($row_user[11]=="Y"){?>selected<? }?>>Yes</option>
												</select>
											</td>
										</tr>
										<tr><td colspan="2" height="10"></td></tr>
										<tr>
											<td>
												<input type="button" value="Save" onClick="save()">
												<input <? if(!$_REQUEST['user_id']){?>disabled="disabled"<? }?> type="button" value="Remove" onClick="remove_user()">
										  </td>
										    <td><input type="button" value="New User" onClick="new_user()"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="user_id" id="user_id" value="<?=$_REQUEST['user_id']?>" />
<input type="hidden" name="which_action" id="which_action" />
</form>
<p>&nbsp;</p>
</body>
</html>