<?
require("utils.php");
require_once($UTILS_CLASS_PATH."website.class.php");
$website = new website;

// Determine if allowed access into content management system
$website->allow_cms_access();

#===================================
# Save news
#===================================

if($_REQUEST['which_action']=="add"){
	$title = str_replace("'","",str_replace("\"","",$_REQUEST['title']));
	$story = str_replace("'","",str_replace("\"","",$_REQUEST['story']));
	$short_story = str_replace("'","",str_replace("\"","",$_REQUEST['short_story']));
	$time_now = mktime(0,0,0,$_REQUEST['month'],$_REQUEST['day'],$_REQUEST['year']);
	
	$sql = "INSERT into cpm_news (
	title,
	short_story,
	story,
	date_of
	)values(
	'$title',
	'$short_story',
	'$story',
	'$time_now'
	)
	";
	mysql_query($sql) || die(mysql_error());
	
	$sql_insid = "SELECT LAST_INSERT_ID() FROM cpm_news";
	$result_insid = mysql_query($sql_insid);
	$row_insid = mysql_fetch_row($result_insid);
	$news_id = $row_insid[0];
	
	replacePage();
}

#===================================
# Save exisiting news
#===================================

if($_REQUEST['which_action']=="modify"){
	
	$news_id = $_REQUEST['news_id'];
	$title = str_replace("'","&quot;",$_REQUEST['title']);
	$story = str_replace("'","&quot;",$_REQUEST['story']);
	$short_story = str_replace("'","&quot;",$_REQUEST['short_story']);
	$time_now = mktime(0,0,0,$_REQUEST['month'],$_REQUEST['day'],$_REQUEST['year']);
	
	$sql = "
	UPDATE cpm_news SET
	title = '".$title."',
	short_story = '".$short_story."',
	story = '".$story."',
	date_of = '$time_now'
	WHERE news_id = $news_id	
	";
	mysql_query($sql) || die($sql);
	replacePage();
}

#===================================
# remove news
#===================================

if($_REQUEST['which_action']=="remove"){

	# Remove all attachments
	$sql_att = "SELECT * FROM cpm_news WHERE news_id = $news_id";
	$result_att = @mysql_query($sql_att);
	$row_att = @mysql_fetch_array($result_att);
	if($row_att['attached_files'] != ""){
		$del_f = explode("<@>", $row_att['attached_files']);
		for($i=0;$i<count($del_f);$i++){
			//unlink("../");
		}
	}
	
	
	$news_id = $_REQUEST['news_id'];
	$sql = "DELETE FROM cpm_news WHERE news_id = $news_id";
	mysql_query($sql)||die(mysql_error());

	replacePage();
}

#===================================
# replace page
#===================================

function replacePage(){
	global $news_id;
	print "<script language='javascript'>location.replace('news.php?news_id=$news_id')</script>";
	exit;
}

#===================================
# Get news
#===================================

if($_REQUEST['news_id']<>""){
	$news_id = $_REQUEST['news_id'];
	$sql = "SELECT * FROM cpm_news WHERE news_id=$news_id";
	$result = mysql_query($sql);
	while($row = mysql_fetch_row($result)){
		$title = $row[1];
		$short_story = $row[2];
		$story = $row[3];
		$date_of = $row[5];
	}
	$date = date("d/m/Y", $date_of);
	$date_parts = explode("/",$date);
	$d_day = $date_parts[0];
	$d_month = $date_parts[1];
	$d_year = $date_parts[2];
	
}


?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>RMG Living - News</title>
<link href="../styles.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function save(){

	var bad=0;

	if(document.forms[0].title.value==""){bad=1;}
	if(document.forms[0].short_story.value==""){bad=1;}
	if(document.forms[0].story.value==""){bad=1;}

	if(bad==1){alert("Please enter a value in all fields marked with *");return;}

	if(document.forms[0].news_id.value==""){
		if(!confirm("Save this news item to the database?")){return;}
		document.forms[0].which_action.value="add";
	}
	else{
		if(!confirm("Save your changes?")){return;}
		document.forms[0].which_action.value="modify";
	}
	document.forms[0].submit();
}

function remove(){
	if(document.forms[0].news_id.value==""){alert("Please select a news item to remove.");return;}
	if(!confirm("Remove this news item?")){return;}
	document.forms[0].which_action.value="remove";
	document.forms[0].submit();
}

function changeNews(id){
	location.href="news.php?news_id="+id;
}

function addFile(){
	window.open("add_news_file.php?news_id=<?=$_REQUEST['news_id']?>","anf","width=300,height=150,status=no,resizable=no,scrollbars=no");
}
function removeFile(){
	var filename = document.forms[0].attached_files.value;
	if(filename==""){return;}
	if(!confirm("Remove this file?")){return;}
	window.open("remove_news_file.php?news_id=<?=$_REQUEST['news_id']?>&filename="+filename,"anf","width=300,height=150,status=no,resizable=no,scrollbars=no");
}
function openFile(file){
	if(file!=""){window.open("../news_files/"+file,"nfs","width=760,height=550,resizable=yes,scrollbars=yes");}
}

//-->
</script>
</head>
<body class="management_body">
<form method="post" action="news.php">
<? require($UTILS_FILE_PATH."management/menu.php");?>

<table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr valign="top">
	  <td><table width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="border: 1px #000000 solid">
        <tr>
          <td align="center"><img src="../images/icons/news_icon_new.gif" width="70" height="50"></td>
        </tr>
        <tr>
          <td align="center"><a href="index.php"><b><< Back to main menu</b></a></td>
        </tr>
      </table></td>
    </tr>
	<tr valign="top">
	  <td>&nbsp;</td>
    </tr>
	<tr valign="top">
		<td>
			<table width="500" border="0" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="border: 1px #000000 solid">
				<tr valign="top">
					<td>
						<table border="0" cellpadding="0" cellspacing="0">
							<tr><td><b><i>News stories...</i></b></td></tr>
							<tr><td height="10"></td></tr>
							<tr valign="top">
								<td>
									<table width="480" border="0" cellpadding="3" cellspacing="0">
										<tr>
											<td width="83">Please select</td>
											<td width="385">
												<select name="news_id" onChange="changeNews(this.value)">
													<option value="">ADD NEWS ITEM</option>
													<?
													$sql = "SELECT news_id, title FROM cpm_news";
													$result = mysql_query($sql);
													while($row = mysql_fetch_row($result)){
													?>
														<option value="<?=$row[0]?>"<? if($news_id==$row[0]){?>selected<? }?>><?=$row[1]?></option>
													<?
													}
													?>
												</select>
											</td>
										</tr>
										<tr>
											<td>Title *</td>
											<td><input name="title" type="text" style="width:150px" value="<?=$title?>" maxlength="50"></td>
										</tr>
										<tr>
											<td>Short Story *</td>
											<td><input name="short_story" type="text" style="width:300px" value="<?=$short_story?>" maxlength="80"></td>
										</tr>
										<tr valign="top">
											<td>Story *</td>
											<td><textarea name="story" style="width:300px;height:70px"><?=$story?></textarea></td>
										</tr>
										<tr valign="top">
											<td>Date *</td>
											<td>
												<select name="day">
												<?
													for($i=1;$i<=31;$i++){
														if($i<10){$day="0".$i;}
														else{$day=$i;}
														?>
														<option value="<?=$day?>" <? if($d_day==""){if(date("d")==$day){?>selected<? }}else{if($d_day==$day){?>selected<? }}?>><?=$day?></option>
														<?
													}
												?>
												</select>
												<select name="month">
												<?
													for($i=1;$i<=12;$i++){
														if($i<10){$month="0".$i;}
														else{$month=$i;}
														?>
														<option value="<?=$month?>" <? if($d_month==""){if(date("m")==$month){?>selected<? }}else{if($d_month==$month){?>selected<? }}?>><?=$month?></option>
														<?
													}
												?>
												</select>
												<select name="year">
												<?
													for($i=(date("Y")-5);$i<=(date("Y")+5);$i++){
														$year=$i;
														?>
														<option value="<?=$year?>" <? if($d_year==""){if(date("Y")==$year){?>selected<? }}else{if($d_year==$year){?>selected<? }}?>><?=$year?></option>
														<?
													}
												?>
												</select>
											</td>
										</tr>
										
										<?
										if($news_id){
										?>
										<tr valign="top">
											<td>Attached Files</td>
											<td>
												<table cellpadding="0" cellspacing="0" border="0">
													<tr valign="top">
														<td>
															<select size="5" name="attached_files" onDblClick="openFile(this.value)">
																<?
																	$sql="SELECT attached_file_names, attached_files FROM cpm_news WHERE news_id = $news_id";
																	$result = @mysql_query($sql);
																	$row = @mysql_fetch_row($result);
																	if($row[0] != ""){$a_names = explode("<@>",trim($row[0]));}
																	if($row[1] != ""){$a_files = explode("<@>",trim($row[1]));}
																	
																	for($i=0;$i<count($a_names);$i++){
																		if((trim($a_names[$i])<>"")&&(trim($a_files[$i])<>"")){
																			if(is_file("../news_files/".$a_files[$i])){
																				?>
																				<option value="<?=trim($a_files[$i])?>"><?=trim($a_names[$i])?></option>
																				<?
																			}
																		}
																	}
																?>
															</select>
													  </td>
															<td width="5"></td>
															<td>
																<a href="#" onclick="addFile()" style="color:#0066cc;text-decoration:underline">Add a new file</a>
																<br>
																<? if(count($a_names) > 0){?><a href="#" onclick="removeFile()" style="color:#0066cc;text-decoration:underline">Remove selected file</a><? }?>
															</td>
												  </tr>
											  </table>
											</td>
										</tr>
										<? }?>
										<tr>
											<td></td>
											<td>
												<input type="button" value="Save" onClick="save()">
												<input type="button" value="Remove" onClick="remove()">
											</td>
										</tr>
								  </table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
		  </table>
		</td>
	</tr>
</table>
<input type="hidden" name="which_action">
</form>
</body>
</html>
