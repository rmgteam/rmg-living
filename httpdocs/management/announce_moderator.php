<?
require("utils.php");
Global $UTILS_TEL_MAIN,$UTILS_CLASS_PATH;
require_once($UTILS_CLASS_PATH."website.class.php");
$website = new website;

// Determine if allowed access into content management system
//$website->allow_cms_access();

//===========================================================================================
// This script will check the incoming request and accept/reject an announcement accordingly
//===========================================================================================
if($_REQUEST['stamp'] && $_REQUEST['stamp'] != ""){
	
	$sql_accept = "SELECT * FROM cpm_announcements WHERE announce_accept_stamp = '".$_REQUEST['stamp']."'";
	$result_accept = mysql_query($sql_accept);
	$accept = mysql_num_rows($result_accept);
	if($accept > 0){
	
		print "<p>Accepted</p>";
		$row_accept = @mysql_fetch_array($result_accept);
		$sql = "SELECT resident_num FROM cpm_residents WHERE rmc_num = ".$row_accept['rmc_num'];
		$result = @mysql_query($sql);
		while($row = @mysql_fetch_row($result)){
		
			$sql2 = "
			INSERT INTO cpm_announcements_read(
				announce_id,
				resident_num,
				announce_read
			)
			VALUES(
				".$row_accept['announce_id'].",
				".$row[0].",
				'N'
			)
			";
			@mysql_query($sql2);
		}
		
		$sql3 = "
		UPDATE cpm_announcements SET
		announce_accept_stamp = '',
		announce_reject_stamp = '',
		announce_approved = 'Y'
		WHERE announce_accept_stamp = '".$_REQUEST['stamp']."'";
		@mysql_query($sql3);
		
		
		//========================================
		// Send announcement to all lessees
		//========================================
		
		// Get Property Manager details
		$sql = "SELECT rmc_name, property_manager, managed_office FROM cpm_rmcs WHERE rmc_num = ".$row_accept['rmc_num'];
		$result = @mysql_query($sql);
		$row = @mysql_fetch_row($result);
		
		// Get email address of user who created it
		$sql_from = "SELECT email FROM cpm_backend WHERE user_id = ".$row_accept['user_id'];
		$result_from = @mysql_query($sql_from);
		$row_from = @mysql_fetch_row($result_from);
		
		// Set from email
/*		if(!empty($row_from[0])){
			$from_email = $row_from[0];
		}
		else{
			$from_email = "customerservice@rmguk.com";
		}
*/
		$from_email = "customerservice@rmguk.com";
		
		if($row[1] != ""){$property_manager = "\n".$row[1];}
		
		$subject = $row[0]." - Announcement";
$message = "From: Residential Management Group Ltd
To:   All Lessees of ".$row[0]."

".$row_accept['announce_text']."

Kind Regards,".$property_manager."

Residential Management Group Ltd

Customer Services Department
Tel: '.$UTILS_TEL_MAIN.'
Email: customerservice@rmguk.com
";
		
		$sql = "SELECT DISTINCT email FROM cpm_residents res, cpm_residents_extra rex WHERE res.resident_num=rex.resident_num AND (res.resident_status = 'Current' OR res.resident_is_active = '1') AND rex.email <> '' AND rex.email <> 'webmaster@rmgliving.co.uk' AND rex.announce_optin='Y' AND res.rmc_num = ".$row_accept['rmc_num'];
		$result = @mysql_query($sql);
		$mail_counter = 0;
		while($row = @mysql_fetch_row($result)){
		
			print $row[0]."<br>";
			$mail_counter++;
			@mail($row[0],$subject,$message,"From:".$from_email);
		}
		print "<b>".$mail_counter." emails sent</b>";
		
	}
	else{
	
		$sql_announce = "SELECT * FROM cpm_announcements WHERE announce_reject_stamp = '".$_REQUEST['stamp']."'";
		$result_announce = @mysql_query($sql_announce);
		$row_announce = @mysql_fetch_array($result_announce);
	
		?>
		<script type="text/javascript">
		function reject(){
			document.forms[0].whichaction.value = "reject";
			document.forms[0].submit();
		}
		</script>
		<form action="announce_moderator.php">
		<input type="text" style="width:500; " name="announce_title" value="<?=$row_announce['announce_title']?>"><br><br>
		<textarea name="announce_text" style="width:500 " rows="10"><?=$row_announce['announce_text']?></textarea><br><br>
		<input type="hidden" name="announce_id" value="<?=$row_announce['announce_id']?>">
		<input type="hidden" name="announce_accept_stamp" value="<?=$row_announce['announce_accept_stamp']?>">
		<input type="hidden" name="whichaction" value="save">
		<input type="submit" value="Accept"><input type="button" onClick="reject()" value="Reject">
		</form>
		<?
	}

}
elseif($_REQUEST['whichaction'] == "save"){

	$save_success = "Y";
	$sql_u = "
	UPDATE cpm_announcements SET
	announce_title = '".$_REQUEST['announce_title']."',
	announce_text = '".$_REQUEST['announce_text']."'
	WHERE announce_id = ".$_REQUEST['announce_id'];
	@mysql_query($sql_u) or $save_success = "N";
	
	if($save_success == "Y"){
		header("Location: announce_moderator.php?stamp=".$_REQUEST['announce_accept_stamp']);
	}
	
}
elseif($_REQUEST['whichaction'] == "reject"){

	// Delete announcement from database
	$sql_reject = "DELETE FROM cpm_announcements WHERE announce_id = ".$_REQUEST['announce_id'];
	@mysql_query($sql_reject);
	
	print "Rejected";
}
?>