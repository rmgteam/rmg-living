<?
ini_set("max_execution_time", "300");
require("utils.php");
require_once($UTILS_CLASS_PATH."website.class.php");
$website = new website;

// Determine if allowed access into content management system
$website->allow_cms_access();

// Check access privilege
if($_SESSION['allow_letters_module'] != 1){header("Location:index.php");}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>RMG Living - Letters</title>
<link href="../styles.css" rel="stylesheet" type="text/css">
<script language="javascript">
function show_password_slip(template, type){		
	window.open("password_slip.php?template=" + template + "&all=1&type=" + type + "&resident_num=&is_print_history=N","PASSWIN","width=900, height=900, scrollbars=yes, resizable=yes, status=yes");
}
</script>
</head>
<body class="management_body">
<form method="post">
<? require($UTILS_FILE_PATH."management/menu.php");?>

<table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr valign="top">
	  <td><table width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="border: 1px #000000 solid">
        <tr>
          <td align="center"><img src="../images/management/icons/letters_icon_new.gif" width="70" height="50"></td>
        </tr>
        <tr>
          <td align="center"><a href="index.php"><b><< Back to main menu</b></a></td>
        </tr>
      </table></td>
    </tr>
	<tr valign="top">
	  <td>&nbsp;</td>
    </tr>
	<tr valign="top">
		<td>
<table width="700" border="0" align="center" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="border: 1px #000000 solid">
				<tr valign="top">
					<td>
						<table border="0" cellpadding="0" cellspacing="0">
							<tr valign="top">
								<td>
									<?
									$num_standard_pass_slips = 0;
									   
									# Determine number of standard password slips needed to be sent for this RMC
									$sql_resident = "
									SELECT * FROM cpm_residents re
                                    INNER JOIN cpm_rmcs rmc ON re.rmc_num = rmc.rmc_num
                                    INNER JOIN cpm_residents_extra rex ON re.rmc_num = rex.rmc_num AND re.resident_num = rex.resident_num
                                    INNER JOIN cpm_lookup_residents lre ON re.resident_num = lre.resident_lookup
                                    INNER JOIN cpm_rmcs_extra rmcx ON rmc.rmc_num = rmcx.rmc_num
                                    INNER JOIN cpm_lookup_rmcs lrmc ON rmc.rmc_num = lrmc.rmc_lookup
                                    WHERE rex.password_to_be_sent='Y' AND re.is_subtenant_account = 'N' AND re.is_resident_director = 'N' AND rmc.rmc_status <> 'Lost'
                                    AND rmc.rmc_is_active = '1' AND rex.is_linked_to_master_account <> 'Y' AND lre.resident_ref NOT LIKE 't0%' AND re.resident_is_developer = 'N' AND re.resident_name <> 'Developer'
                                    AND rmcx.prevent_printing_of_letters <> 'Y' AND re.resident_status = 'Current' AND re.resident_is_active='1'
                                    LIMIT 100";
									$result_resident = mysql_query($sql_resident);
									$num_standard_pass_slips = mysql_num_rows($result_resident);
									
									$sql_contractor = "
									SELECT cpm_contractors_qube_id 
									FROM cpm_contractors
									WHERE 
									cpm_contractors_letter_sent = 'N'
									LIMIT 100";
									$result_contractor = mysql_query($sql_contractor);
									$num_contractor_pass_slips = mysql_num_rows($result_contractor);
									?>
									<table width="100%" border="0" cellspacing="0" cellpadding="5">
                                      <tr>
                                        <td width="7%" onMouseOver="this.style.cursor='hand';"><img src="../images/help_24.gif" width="24" height="24" border="0" align="absmiddle"></td>
                                        <td width="93%" onMouseOver="this.style.cursor='hand';">There are <b>
                                          <?=$num_standard_pass_slips?>
                                        </b> Lessees who have not been sent their <strong>login details</strong> letter. <? if($num_standard_pass_slips > 0){?><a href="Javascript:;" onClick="show_password_slip('standard', '5');return false;">Click <strong>here</strong> to print these letters.</a><? }?></td>
                                      </tr>
									  <tr>
                                        <td width="7%" onMouseOver="this.style.cursor='hand';"><img src="../images/help_24.gif" width="24" height="24" border="0" align="absmiddle"></td>
                                        <td width="93%" onMouseOver="this.style.cursor='hand';">There are <b>
                                          <?=$num_contractor_pass_slips?>
                                        </b> contractors who have not been sent their <strong>login details</strong> letter. <? if($num_contractor_pass_slips > 0){?><a href="Javascript:;" onClick="show_password_slip('contractor', '6');return false;">Click <strong>here</strong> to print these letters.</a><? }?></td>
                                      </tr>
                                    </table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
		  </table>
	  </td>
	</tr>
</table>
<input type="hidden" name="whichaction">
</form>
</body>
</html>
