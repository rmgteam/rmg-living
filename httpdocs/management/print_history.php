<?
require("utils.php");
require_once($UTILS_CLASS_PATH."website.class.php");
$website = new website;

// Determine if allowed access into content management system
$website->allow_cms_access();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles.css" rel="stylesheet" type="text/css">
<title>Print History</title>
<script language="javascript">
function hide_letter_box(template, todays_date){
	opener.hide_letter_box(template, todays_date);
}

function show_password_slip(print_type, print_job_id, resident_num, rmc_num, template){
	window.open("password_slip.php?template=" + template + "&rmc_num=" + rmc_num + "&resident_num=" + resident_num + "&is_print_history=Y&print_job_id=" + print_job_id + "&type=" + print_type,"PASSWIN","width=900, height=900, scrollbars=yes, resizable=yes");
}
</script>
<style type="text/css">
<!--
.style1 {
	font-size: 12px;
	font-weight: bold;
}
-->
</style>
</head>
<body class="management_body">
<?
// Select all print jobs
if($_REQUEST['type'] == "1"){
	$sql_oldres = "SELECT voyager_resident_num FROM cpm_residents WHERE resident_num = ".$_REQUEST['resident_num'];
	$result_oldres = @mysql_query($sql_oldres);
	$row_oldres = @mysql_fetch_row($result_oldres);
	if($row_oldres[0] != ""){
		$resident_clause = " (resident_nums LIKE '%".$_REQUEST['resident_num']."%' OR resident_nums LIKE '%".$row_oldres[0]."%')";
	}
	else{
		$resident_clause = " resident_nums LIKE '%".$_REQUEST['resident_num']."%' ";
	}
}
if($_REQUEST['type'] == "2"){
	$rmc_clause = " (rmc_num = ".$_REQUEST['rmc_num'].")";
}
$sql = "SELECT * FROM cpm_print_jobs WHERE $resident_clause $rmc_clause ORDER BY print_job_time DESC";
$result = @mysql_query($sql);
$num_jobs = @mysql_num_rows($result);
?>
<table width="583" border="0" cellspacing="0" cellpadding="3">
  <tr bgcolor="#FFFFFF">
    <td colspan="3"><img src="../images/management/print_history.gif" width="32" height="32" align="absmiddle">&nbsp;&nbsp;<span class="style1">Print History</span> </td>
  </tr>
  <tr>
    <td height="19" width="221" background="../images/site/bar_bg.jpg" bgcolor="#CCCCCC"><strong>Print title</strong></td>
    <td background="../images/site/bar_bg.jpg" bgcolor="#CCCCCC" width="192" nowrap style="border-left:1px solid #cccccc;"><strong>User</strong></td>
    <td background="../images/site/bar_bg.jpg" bgcolor="#CCCCCC" width="152" nowrap style="border-left:1px solid #cccccc;"><strong>Time printed/sent</strong></td>
  </tr>
  <?
  if($num_jobs > 0){
  	while($row = @mysql_fetch_array($result)){	
	?>
	<tr bgcolor="#FFFFFF" onClick="show_password_slip(<?=$_REQUEST['type']?>,'<?=$row['print_job_id']?>','<?=$_REQUEST['resident_num']?>','<?=$_REQUEST['rmc_num']?>','<?=$row['template']?>')" onMouseOver="this.style.cursor='hand';this.style.background='#eaeaea';" onMouseOut="this.style.background='#ffffff';">
    <td style="border-bottom:1px solid #cccccc;" valign="top"><?=$row['print_job_title']?></td>
    <td style="border-bottom:1px solid #cccccc;border-left:1px solid #eaeaea;" valign="top"><?=$row['user_name']?></td>
    <td style="border-bottom:1px solid #cccccc;border-left:1px solid #eaeaea;" valign="top"><?=date("H:i:s d/m/Y",$row['print_job_time'])?></td>
  	</tr>
  	<?
  	}
  }
  else{
 	?>
  	<tr><td colspan="3">No print history available</td></tr>
  	<?
  }
  ?>
</table>
</body>
</html>
