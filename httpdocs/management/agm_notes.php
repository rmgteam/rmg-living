<?
require("utils.php");
require_once($UTILS_FILE_PATH."library/functions/check_file_extension.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."rmc.class.php");
$website = new website;
$rmc = new rmc;
$rmc->set_rmc($_REQUEST['rmc_num']);

// Determine if allowed access into content management system
$website->allow_cms_access();

// Check access privilege
if($_SESSION['allow_rmc'] != 1){header("Location:index.php");}

#=========================================
# Delete agm file
#=========================================
if($_REQUEST['whichaction'] == "delete_agm_notes"){

	$del_error = "N";

	$sql = "SELECT * FROM cpm_agm WHERE agm_id=".$_REQUEST['agm_file_id'];
	$result = @mysql_query($sql) or $del_error = "Y";
	$row = @mysql_fetch_array($result);
	$file_name = $row['agm_file'];

	$sql = "DELETE FROM cpm_agm WHERE agm_id=".$_REQUEST['agm_file_id'];
	@mysql_query($sql) or $del_error = "Y";
	
	if($del_error == "N"){
		@unlink($UTILS_MEETING_NOTES_PATH.$file_name);
	}
	
}


#=========================================
# Add agm file
#=========================================
if($_REQUEST['which_action'] == "add_agm_notes"){

	# Check that meeting for this date doesn't already exist, otherwise warn user
	$sql = "SELECT * FROM cpm_agm WHERE rmc_num = ".$_REQUEST['rmc_num']." AND meeting_type_id = ".$_REQUEST['agm_notes_type']." AND agm_date='".$_REQUEST['agm_year']."'";
	$result = @mysql_query($sql);
	$agm_exists = @mysql_num_rows($result);
	
	
	if($agm_exists < 1){
	
		$file_stamp = time();
	
		// Save AGM file to correct location
		$file_ext[] = "pdf";
		if(check_file_extension($_FILES['agm_file']['name'], $file_ext)){
			
			$agm_file_path = $UTILS_MEETING_NOTES_PATH."meeting_notes_".$rmc->file_friendly_name()."_".$file_stamp.".pdf";
			$agm_file_name = "meeting_notes_".$rmc->rmc['rmc_ref']."_".$file_stamp.".pdf";
			copy($_FILES['agm_file']['tmp_name'], $agm_file_path);
			
			// Get date of meeting
			$notes_date_parts = explode("/", $_REQUEST['agm_year']);
		
			// Insert AGM details into agm table
			$sql = "
			INSERT INTO cpm_agm(
				rmc_num,
				agm_file,
				agm_file_stamp,
				agm_year,
				agm_date,
				meeting_type_id
			)
			VALUES(
				".$_REQUEST['rmc_num'].",
				'$agm_file_name',
				'".$file_stamp."',
				'".$notes_date_parts[2]."',
				'".$notes_date_parts[2].$notes_date_parts[1].$notes_date_parts[0]."',
				".$_REQUEST['agm_notes_type']."
			)
			";
			mysql_query($sql) || die($sql);
		}
	}
}
?>
<html>
<head>
<link href="../styles.css" rel="stylesheet" type="text/css">
</head>
<body>
<? if($_REQUEST['which_action'] == "add_agm_notes" && $agm_exists > 0){?>
<script language="javascript">
alert("An meeting notes file already exists for this date.  If you wish to replace it, please remove the existsing one first.");
</script>
<? }?>
<form action="agm_notes.php">
<table border="0" cellpadding="3" cellspacing="0" width="100%">
<?
#===============================================
# Collect all agm notes for this RMC
#===============================================
$sql = "SELECT * FROM cpm_agm a, cpm_meeting_types mt WHERE a.meeting_type_id=mt.meeting_type_id AND a.rmc_num = ".$_REQUEST['rmc_num']." ORDER BY a.agm_date DESC";
$result = @mysql_query($sql);
$num_agm = @mysql_num_rows($result);
if($num_agm < 1){
	print "<tr><td>There are no meeting notes for this Man. Co.</td></tr>";
}
else{
	while($row = @mysql_fetch_array($result)){
		?>
		<tr bgcolor="#f3f3f3"><td style="border-bottom:1px solid #cccccc"><?=$row['meeting_type']?> (<?=substr($row['agm_date'],6,2)."/".substr($row['agm_date'],4,2)."/".substr($row['agm_date'],0,4)?>)</td>
		  <td align="right" style="border-bottom:1px solid #cccccc"><input onClick="if(confirm('Remove meeting notes for this Man. Co. ?')){document.forms[0].agm_file_id.value='<?=$row['agm_id']?>';document.forms[0].submit();}" type="button" name="Button" value="remove"></td>
		</tr>
		<?
	}
}
?>
</table>
<input type="hidden" id="agm_file_id" name="agm_file_id">
<input type="hidden" id="rmc_num" name="rmc_num" value="<?=$_REQUEST['rmc_num']?>">
<input type="hidden" name="whichaction" value="delete_agm_notes">
</form>
</body>
</html>