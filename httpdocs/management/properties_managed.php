<?
require("utils.php");
require_once($UTILS_CLASS_PATH."website.class.php");
$website = new website;

// Determine if allowed access into content management system
$website->allow_cms_access();

// Check access privilege
if($_SESSION['allow_pms'] != 1){header("Location:index.php");}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>RMG Living - Properties Managed</title>
<link href="../styles.css" rel="stylesheet" type="text/css">
</head>
<body>
<table width="100%" cellpadding="15" border="0">
<tr><td>

<table width="600" cellpadding="2" border="0">
<tr>
<td nowrap style="border-bottom:1px solid #cccccc;"><b>Man. Co.</b></td>
<td nowrap style="border-bottom:1px solid #cccccc;"><b>RMG Living Status</b></td>
<td nowrap style="border-bottom:1px solid #cccccc;"><b>Voyager Status</b></td>
</tr>
<?
// Get properties managed by selected staff member
$sql_ac = "SELECT lrmc.rmc_ref, rmc.rmc_name, rmc.rmc_status, rmc.rmc_is_active FROM cpm_lookup_rmcs lrmc, cpm_rmcs rmc, cpm_rmcs_extra rex WHERE lrmc.rmc_lookup=rmc.rmc_num AND rmc.rmc_num=rex.rmc_num AND rex.member_id = ".$_REQUEST['member_id']." ORDER BY rmc.rmc_name ASC";
$result_ac = @mysql_query($sql_ac);
$num_managed = @mysql_num_rows($result_ac);
if($num_managed > 0){

	while($row_ac = @mysql_fetch_row($result_ac)){
	
	if($row_ac[3] == "1"){
		$row_ac[3] = "Active";
	}
	else{
		$row_ac[3] = "Inactive";
	}
	
	?>
	<tr><td style="border-bottom:1px solid #eaeaea;">(<?=$row_ac[0]?>) <?=$row_ac[1]?></td><td style="border-bottom:1px solid #eaeaea;">&nbsp;</td><td style="border-bottom:1px solid #eaeaea;"><?=$row_ac[2]?></td></tr>
	<?
	}
}
else{
?>
<tr><td colspan="3">No properties found for this member of staff.</td></tr>
<? }?>
</table>

</td></tr>
</table>
</body>
</html>
