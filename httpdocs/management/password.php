<?
require("utils.php");
require($UTILS_FILE_PATH."management/gen_password.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."encryption.class.php");
require_once($UTILS_CLASS_PATH."resident.class.php");
require_once($UTILS_CLASS_PATH."master_account.class.php");
$website = new website;
$crypt = new encryption_class;
$resident = new resident($_REQUEST['resident_num']);
$master_account = new master_account;
Global $UTILS_TEL_MAIN;


// Determine if allowed access into content management system
$website->allow_cms_access();

#=========================================
# Generate new password
#=========================================
if($_REQUEST['gen'] == "Y"){

	$password = get_rand_id(8, 'ALPHA', 'UPPER');
	
	// If linked to master account, overwrite Master Account record
	if($resident->is_linked_to_master_account == "Y"){
		
		$master_account->master_account($resident->get_master_account_id(), "id");
		
		$sql2 = "
		UPDATE cpm_master_accounts SET
		master_account_password = '".$crypt->encrypt($UTILS_DB_ENCODE, trim($password))."',
		master_account_allow_password_reset = 'Y'
		WHERE master_account_id = ".$master_account->master_account_id;
		@mysql_query($sql2) or $save_success = "N";
		
		$message = "
You have requested your password to be reset for the RMG Living website. Below are the details for your Master Account: 

Login Id: ".$master_account->master_account_username."
Password:  ".$password."

Please follow the link below to login to your account;
http://www.rmgliving.co.uk


If you experience any issues with the above, please do not hesitate to
contact our Customer Contact Centre on '.$UTILS_TEL_MAIN.'. Alternatively, you
can contact by email to customerservice@rmguk.com.

Yours sincerely,

Customer Service
http://www.rmgliving.co.uk
";

			
		$send_status = @mail($master_account->master_account_email,"RMG Living details ...",$message,"From:customerservice@rmguk.com");
		if($send_status === true){
			
			print "
			<script language=Javascript>
			alert(\"A new password for the associated Master Account has been successfully sent to ".$master_account->master_account_email.".\");
			</script>
			";
			
		}
		else{
			print "
			<script language=Javascript>
			alert(\"There was a problem sending a new password for the associated Master Account. Please notify customerservice@rmguk.com that this error has occurred.\");
			</script>
			";
		}

	}
	else{
	
		$save_success = "Y";
		$sql2 = "
		UPDATE cpm_residents_extra SET
		password = '".$crypt->encrypt($UTILS_DB_ENCODE, trim($password))."',
		allow_password_reset = 'Y'
		WHERE resident_num = ".$_REQUEST['resident_num'];
		@mysql_query($sql2) or $save_success = "N";
		
		if($save_success == "Y"){
			
			if($_REQUEST['send_type'] == "letter"){
			
				$sql = "UPDATE cpm_residents_extra SET password_to_be_sent='Y' WHERE resident_num = ".$_REQUEST['resident_num'];
				@mysql_query($sql);
					
				print "
				<script language=Javascript>
				parent.document.getElementById('standard_password_status').style.display=\"block\";
				parent.document.getElementById('standard_sent_row').style.display=\"none\";
				</script>
				";
			}
			elseif($_REQUEST['send_type'] == "email"){
			
				$sql_p = "
				SELECT rex.password, rex.email, rex.rmc_num, u.unit_address_1, u.unit_description, u.unit_address_2, r.resident_address_1, r.resident_address_2 
				FROM cpm_residents_extra rex, cpm_residents r LEFT JOIN cpm_units u ON (r.resident_num=u.resident_num)  
				WHERE 
				rex.resident_num=r.resident_num AND 
				rex.resident_num <> '' AND 
				rex.resident_num = ".$_REQUEST['resident_num'];
				//print $sql_p;
				$result_p = @mysql_query($sql_p);
				$row_p = @mysql_fetch_row($result_p);
				
				
				if($row_p[4] != "" && $row_p[3] == ""){
					$unit_address = stripslashes($row_p[4]);		
				}
				elseif($row_p[4] != "" && $row_p[3] != ""){
					$unit_address = stripslashes($row_p[4].", ".$row_p[3]);		
				}
				elseif($row_p[3] != "" && $row_p[5] != ""){
					$unit_address = stripslashes($row_p[3].", ".$row_p[5]);		
				}
				elseif($row_p[4] == "" && $row_p[3] != ""){
					$unit_address = stripslashes($row_p[3]);		
				}
				elseif($row_p[4] == "" && $row_p[3] == "" && $row_p[6] != "" && $row_p[7] == ""){
					$unit_address = stripslashes($row_p[6]);
				}
				elseif($row_p[4] == "" && $row_p[3] == "" && $row_p[6] != "" && $row_p[7] != ""){
					$unit_address = stripslashes($row_p[6].", ".$row_p[7]);
				}
				elseif($row_p[4] == "" && $row_p[3] == "" && $row_p[6] == "" && $row_p[7] != ""){
					$unit_address = stripslashes($row_p[7]);
				}
				
			
$message = "
Thank you for requesting access to the RMG Living service. I can confirm your registered login details for ".$unit_address." are as follows:

Login Id: ".$resident->resident_ref."
Password:  ".$password."

Please follow the link below to login to your account;
http://www.rmgliving.co.uk

When you have logged into your account for the first time, you will be
prompted to reset the password to one of your choice. Please ensure all login details are safely noted.

If you experience any issues with the above, please do not hesitate to
contact our Customer Contact Centre on '.$UTILS_TEL_MAIN.'. Alternatively, you
can contact by email to customerservice@rmguk.com.

Yours sincerely,

Customer Service
http://www.rmgliving.co.uk
";

			
				$send_status = @mail($row_p[1],"RMG Living details ...",$message,"From:customerservice@rmguk.com");
				if($send_status === true){
				
					# State that password for this user has been sent
					$sql = "UPDATE cpm_residents_extra SET password_to_be_sent='N' WHERE resident_num = '".$_REQUEST['resident_num']."'";
					@mysql_query($sql);
					
					# Add to print job table
					$sql_pj = "SELECT print_type_title FROM cpm_print_types WHERE print_type_id = 4";
					$result_pj = @mysql_query($sql_pj);
					$row_pj = @mysql_fetch_row($result_pj);
		
					$sql = "
					INSERT INTO cpm_print_jobs(
						print_job_title,
						template,
						resident_nums,
						rmc_num,
						user_name,
						print_job_time
					)
					VALUES(
						'".$row_pj[0]."',
						'".$_REQUEST['template']."',
						'".$_REQUEST['resident_num']."',
						".$row_p[2].",
						'".addslashes($_SESSION['user_name'])."',
						'$thistime'
					)
					";
					@mysql_query($sql);
					
					if($_SESSION['allow_letters'] == "1"){
						print "
						<script language=Javascript>
						parent.hide_letter_box('".$_REQUEST['template']."', '".date("d/m/Y", $thistime)."');
						</script>
						";
					}
					print "
					<script language=Javascript>
					alert(\"Password successfully sent.\");
					</script>
					";
					
				}
				else{
					print "
					email: ".$row_p[1]."
					<script language=Javascript>
					alert(\"There was a problem sending this Lessee their new password. Please notify customerservice@rmguk.com that this error has occurred.\");
					</script>
					";
				}
			}
		}
		else{
			print "
			<script language=Javascript>
			alert(\"There was a problem generating a new password for this user. Please notify customerservice@rmguk.com that this error has occurred.\");
			</script>
			";
		}
	}
}
?>