<?php
ini_set("max_execution_time", "300");
ob_start();
require("utils.php");
require_once($UTILS_CLASS_PATH."letter.class.php");
require_once($UTILS_CLASS_PATH."resident.class.php");
require_once($UTILS_CLASS_PATH."rmc.class.php");
require_once($UTILS_CLASS_PATH."unit.class.php");
require_once($UTILS_CLASS_PATH."data.class.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."contractors.class.php");
require_once($UTILS_CLASS_PATH."security.class.php");
require_once($UTILS_CLASS_PATH."encryption.class.php");
require_once( $UTILS_CLASS_PATH."webservice.class.php" );
$website = new website;
$rmc = new rmc();
$unit = new unit();
$resident = new resident();
$letter = new letter();
$security = new security;
$crypt = new encryption_class;
$data = new data();

// Determine if allowed access into content management system
$website->allow_cms_access();


//=============================================
// Update print job table
//=============================================
if($_REQUEST['print_status'] == "done") {

        $thistime = time();

        if ( $_REQUEST['type'] != "6" ) {

            $resultArray['subsidiary'] = array();
            $resultArray['output'] = array();
            $resultArray['reference'] = array();
            $resultArray['xml'] = array();
            $resultArray['review'] = array();

            $sql = "SELECT * FROM cpm_subsidiary";
            $result = @mysql_query($sql);
            while($row = @mysql_fetch_array($result)) {
                $resultArray['subsidiary'][] = $row['subsidiary_id'];

                $xmlFound = false;

                $sql_pwd = "SELECT lr.resident_ref,re.password,re.resident_num,
                    case
                       when r.is_subtenant_account = 'Y' then 'S'
                       when r.is_resident_director = 'Y' then 'D'
                       else 'T'
                    end as type
                    FROM cpm_lookup_residents lr
                    INNER JOIN cpm_residents r ON r.resident_num = lr.resident_lookup
                    INNER JOIN cpm_residents_extra re ON r.resident_num = re.resident_num
                    WHERE re.resident_num IN (" . $_REQUEST['resident_nums'] . ")  AND r.subsidiary_id = '" . $row['subsidiary_id'] . "'";
               // echo $sql_pwd;
               // echo "\r\n";
                $result_pwd = @mysql_query( $sql_pwd );
                $xml_pwd = '<parameters>';
                $xml_pwd .= '<update>1</update>';
                while ( $row_pwd = @mysql_fetch_array( $result_pwd ) ) {
                    $xmlFound = true;
                    $xml_pwd .= '<tenants>';
                    $xml_pwd .= '<reference>' . $row_pwd['resident_ref'] . '</reference>';
                    $xml_pwd .= '<password>' . $resident->get_password($row_pwd['resident_num']) . '</password>';
                    $xml_pwd .= '<type>' . $row_pwd['type'] . '</type>';
                    $xml_pwd .= '</tenants>';
                }
                $xml_pwd .= '</parameters>';
                if ( $xmlFound ) {
                    $webservice = new webservice();
                    //echo "<pre>" . htmlentities( $xml_pwd ) . "</pre>";
                    $resultArray['output'] = $output = $webservice->qube_execute( 2, 'putpass', $xml_pwd, $row['subsidiary_ecs_login_group'] );
                   // echo '<pre>'; print_r( $output );

                    if ( !is_null( $output['code'] ) ) {
                        $resultArray['reason'][] = $row['subsidiary_ecs_login_group'] . " - XML is invalid/Curl is unavailable. Http response: " . $output['code'] . " XML = " . $output['desc'];
                        $resultArray['result'] = 'error';
                    } else {

                        $return_code = $output['soap:Envelope']['soap:Body'][0]['QubeProcess-3Response'][0]['QubeProcess-3Result'][0]['data'][0]['results'][0]['code'];
                        $review_code = $output['soap:Envelope']['soap:Body'][0]['QubeProcess-3Response'][0]['QubeProcess-3Result'][0]['data'][0]['results'][0]['review-code'];

                        if ( $return_code == "00" ) {
                            $resultArray['result'] = true;
                            $resultArray['xml'][] = $xml_pwd;
                            if ( $review_code != '' ) {
                                $resultArray['review'][] = $row['subsidiary_name'] . ' - ' . $review_code;
                            }
                        } else {
                            $resultArray['result'] = 'error';
                            $resultArray['reason'][] = $row['subsidiary_ecs_login_group'] . " - Error code: " . $return_code;
                        }
                    }
                    if ( $resultArray['result'] == true ) {

                        // Get ALL resident nums posted
                        $resident_nums = explode( ",", $_POST['resident_nums'] );

                        // Get title for this print job
                        $sql_pj = "SELECT print_type_title FROM cpm_print_types WHERE print_type_id = " . $_REQUEST['type'];
                        $result_pj = @mysql_query( $sql_pj );
                        $row_pj = @mysql_fetch_row( $result_pj );

                        // Set residents record to say that they have been sent password
                        for ( $i = 0; $i < count( $resident_nums ); $i++ ) {

                            $sql = "UPDATE cpm_residents_extra SET password_to_be_sent='N' WHERE resident_num = " . $resident_nums[$i];
                            @mysql_query( $sql );
                        }

                        // Set rmc_num
                        $rmc_num = "NULL";
                        if ( $_REQUEST['rmc_num'] != "" ) {
                            $rmc_num = $_REQUEST['rmc_num'];
                        }

                        // Include print job in cpm_print_jobs table
                        if ( $_REQUEST['type'] == "1" || $_REQUEST['type'] == "2" || $_REQUEST['type'] == "5" ) {

                            $sql = "
                    INSERT INTO cpm_print_jobs(
                        print_job_title,
                        template,
                        resident_nums,
                        rmc_num,
                        user_name,
                        print_job_time
                    )
                    VALUES(
                        '" . $row_pj[0] . "',
                        '" . $_REQUEST['template'] . "',
                        '" . $_REQUEST['resident_nums'] . "',
                        " . $rmc_num . ",
                        '" . addslashes( $_SESSION['user_name'] ) . "',
                        '$thistime'
                    )
                    ";
                            @mysql_query( $sql );

                        } else if ( $_REQUEST['type'] == "3" ) {

                            $_REQUEST['print_job_title'] .= " (Multiple RMC'S)";
                            $rmc_nums = explode( ",", $_POST['rmc_nums'] );
                            for ( $i = 0; $i < count( $rmc_nums ); $i++ ) {

                                $sql = "
                        INSERT INTO cpm_print_jobs(
                            print_job_title,
                            template,
                            resident_nums,
                            rmc_num,
                            user_name,
                            print_job_time
                        )
                        VALUES(
                            '" . $row_pj[0] . "',
                            '" . $_REQUEST['template'] . "',
                            '" . $_REQUEST['resident_nums'] . "',
                            " . $rmc_nums[$i] . ",
                            '" . addslashes( $_SESSION['user_name'] ) . "',
                            '$thistime'
                        )
                        ";
                                @mysql_query( $sql );

                            }
                        }
                    }
                }
            }
        } else {
            $resident_nums = explode( ",", $_POST['resident_nums'] );
            $resident_passwords = explode( ",", $_POST['resident_passwords'] );

            for ( $i = 0; $i < count( $resident_nums ); $i++ ) {

                $contractors = new contractors( $resident_nums[$i] );

                if ( strtoupper( $contractors->contractor_pcm ) != 'POST' ) {
                    $password = $resident->get_rand_id( 8, 'ALPHA', 'UPPER' );

                    $letter->set_contractor( $contractors );

                    $letter->set_body( "letter_cpm_living_welcome_contractors.php" );

                    // Make letter reference
                    $your_ref = stripslashes( $resident_nums[$i] );

                    //Use the appropriate letter format
                    ob_start();
                    require( $letter->get_body() );
                    $letter_content = ob_get_contents();
                    ob_end_clean();

                    $headers = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                    $headers .= 'From: customerservice@rmguk.com' . "\r\n";
                    $headers .= 'Reply-To: customerservice@rmguk.com' . "\r\n";
                    $headers .= 'X-Mailer: PHP/' . phpversion();

                    $sql = "INSERT INTO cpm_mailer SET
				mail_to = '" . $contractors->contractor_email . "',
				mail_from = 'customerservice@rmguk.com',
				mail_subject = 'Welcome to RMG Living',
				mail_type = 'Contractor Welcome Letter',
				mail_headers = '" . $headers . "',
				mail_message = '" . $letter_content . "'";

                    @mysql_query( $sql );
                }

                $sql = "UPDATE cpm_contractors SET
			cpm_contractors_letter_sent = 'Y' 
			WHERE cpm_contractors_qube_id = '" . $resident_nums[$i] . "'";
                @mysql_query( $sql );

                $the_password = $resident_passwords[$i];

                if ( strtoupper( $contractors->contractor_pcm ) != 'POST' ) {
                    $the_password = $password;
                }

                $sql = "SELECT *
			FROM cpm_contractors_user
			WHERE cpm_contractors_user_ref = '" . $_POST['contractor_id'] . "'";
                $result = @mysql_query( $sql );
                $num_rows = @mysql_num_rows( $result );

                if ( $num_rows == 0 ) {
                    $sql = "INSERT INTO cpm_contractors_user SET
				cpm_contractors_user_ref = '" . $resident_nums[$i] . "',
				cpm_contractors_user_qube_ref = '" . $resident_nums[$i] . "',
				cpm_contractors_user_disabled = 'False',
				cpm_contractors_user_parent = '0', ";
                } else {
                    $sql = "UPDATE cpm_contractors_user SET ";
                }

                $sql .= "cpm_contractors_user_password = '" . $security->clean_query( $crypt->encrypt( $UTILS_DB_ENCODE, $the_password ) ) . "'";

                if ( $num_rows > 0 ) {
                    $sql .= "WHERE cpm_contractors_user_ref = '" . $_POST['contractor_id'] . "'";
                }

                @mysql_query( $sql );

                $date = new DateTime();

                $sql = "
			INSERT INTO cpm_print_contractors SET
			contractor_id = '" . $resident_nums[$i] . "',
			user_name = '" . $_SESSION['user_id'] . "',
			print_job_time = '" . $date->format( 'Y-m-d H:i:s' ) . "'";
                @mysql_query( $sql );

            }
        }

    $thistime = time();
}

?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="../styles.css" rel="stylesheet" type="text/css">
    <link href="../css/letter_print.css" rel="stylesheet" type="text/css" media="print">
    <style type="text/css">
        <!--
        .style4 {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 10px;
        }
        .style11 {
            font-size: 10px;
            font-weight: bold;
            font-family: Verdana, Arial, Helvetica, sans-serif;
        }
        .style12 {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 12px;
            color: #036;
        }

        -->
    </STYLE>
    <script type="text/javascript" language="JavaScript" src="/library/jscript/jquery-1.6.2.min.js"></script>
    <script type="text/javascript" language="JavaScript" src="/library/jscript/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" language="JavaScript"  src="/library/jscript/jquery.pdfEmbed.js"></script>
    <script language="javascript">
        $(document).ready(function(){
            $('#gDocs').pdfEmbed({ width: 850, height: 700});
        });
        function print_slip(){

            document.getElementById('header_table').style.display='none';
            document.getElementById('done_table').style.display='none';
            document.getElementById('print_table').style.display='none';
            document.getElementById('warning_table').style.display='none';
            document.getElementById('spacer_table').style.display='none';
            window.print();
            document.getElementById('header_table').style.display='block';
            document.getElementById('done_table').style.display='block';
            document.getElementById('print_table').style.display='block';
            document.getElementById('warning_table').style.display='block';
            document.getElementById('spacer_table').style.display='block';
        }

        function confirm_print(){
            <? if($_REQUEST['type'] != "5" && $_REQUEST['type'] != "6"){?>
            opener.hide_letter_box('<?=$_REQUEST['template']?>', '<?=date("d/m/Y", $thistime)?>');
            <? }?>
            document.forms[0].action = "password_slip.php";
            document.forms[0].target = "_self";
            document.forms[0].submit();
        }
    </script>
</head>
<body>
<? if($_REQUEST['print_status'] == "done"){?>
    <table width="100%" height="100%" border="0" align="center" cellpadding="0" cellspacing="10" bgcolor="#C5D9B3" id="close_table" style="border:1px solid #666666;">
        <tr onMouseOver="this.style.cursor='hand'">
            <td align="center" onClick="window.close();"><p class="style11"><? if($_REQUEST['type'] == "resident"){?>The record for this resident has<? }else{?>All resident records have<? }?> been updated. Click <a href="#" onClick="window.close();">HERE</a> to close this window.</p>
            </td>
        </tr>
    </table>
    <? exit;}?>

<?php
//==========================================================
// Collect dynamic data to populate password slip
//==========================================================

// Get single resident print job
if($_REQUEST['type'] == "1"){

    $template_clause = " rex.password_to_be_sent='Y' AND ";

    if($_REQUEST['is_print_history'] == "Y"){
        // Build sql query
        $sql_resident = "
		SELECT * 
		FROM cpm_residents re, cpm_residents_extra rex, cpm_rmcs rmc, cpm_rmcs_extra rmcx   
		WHERE 
		rmc.rmc_num = rmcx.rmc_num AND 
		re.rmc_num=rmc.rmc_num AND 
		re.resident_num=rex.resident_num AND 
		rmcx.prevent_printing_of_letters <> 'Y' AND 
		(re.resident_status = 'Current' OR re.resident_is_active='1') AND 
		re.resident_num = ".$_REQUEST['resident_num'];

    }
    else{
        // Build sql query
        $sql_resident = "
		SELECT * 
		FROM cpm_residents re, cpm_residents_extra rex, cpm_rmcs rmc, cpm_rmcs_extra rmcx  
		WHERE 
		$template_clause 
		rmc.rmc_num = rmcx.rmc_num AND 
		re.rmc_num=rmc.rmc_num AND 
		(rmc.rmc_status = 'Active' OR rmc.rmc_is_active = '1') AND 
		rex.is_linked_to_master_account <> 'Y' AND 
		rmcx.prevent_printing_of_letters <> 'Y' AND 
		re.resident_num=rex.resident_num AND 
		(re.resident_status = 'Current' OR re.resident_is_active='1') AND 
		re.resident_num = ".$_REQUEST['resident_num'];
        //print $sql_resident;
    }
}
elseif($_REQUEST['type'] == "2" || $_REQUEST['type'] == "5"){
// Get single RMC print job

    if($_REQUEST['is_print_history'] == "Y"){

        // Get all resident nums
        $sql_pj = "SELECT resident_nums FROM cpm_print_jobs WHERE print_job_id = ".$_REQUEST['print_job_id'];
        $result_pj = @mysql_query($sql_pj);
        $row_pj = @mysql_fetch_row($result_pj);

        // Build 'where' clause
        $residents = explode(",", $row_pj[0]);
        $i=0;
        $whereclause = "(";
        for($i=0;$i<(count($residents)-1);$i++){
            $whereclause .= " re.resident_num = ".$residents[$i]." OR ";
        }
        $whereclause .= " re.resident_num = ".$residents[$i].") ";

        // Build sql query
        $sql_resident = "
		SELECT *
		FROM cpm_residents re
		INNER JOIN cpm_rmcs rmc ON re.rmc_num = rmc.rmc_num
		INNER JOIN cpm_residents_extra rex ON re.rmc_num = rex.rmc_num AND re.resident_num = rex.resident_num
		INNER JOIN cpm_lookup_residents lre ON re.resident_num = lre.resident_lookup
		INNER JOIN cpm_rmcs_extra rmcx ON rmc.rmc_num = rmcx.rmc_num
		INNER JOIN cpm_lookup_rmcs lrmc  ON rmc.rmc_num = lrmc.rmc_lookup
		WHERE rmc.rmc_status <> 'Lost' AND rmc.rmc_is_active = '1' AND
		lre.resident_ref NOT LIKE 't0%' AND
		rmcx.prevent_printing_of_letters <> 'Y' AND
		rex.is_linked_to_master_account <> 'Y' AND
		re.resident_is_developer = 'N' AND
		re.resident_status = 'Current' AND re.resident_is_active='1' AND
		rex.hide_letter='N' AND $whereclause";

    }
    else{

        $template_clause = " rex.password_to_be_sent='Y' AND ";

        if(!isset($_REQUEST['all']) || $_REQUEST['all'] == ""){
            $all = "AND re.rmc_num = ".$_REQUEST['rmc_num'];
        }
        else{
            $all = "ORDER BY re.rmc_num,re.resident_num ASC";
        }


        if($_REQUEST['letter_filter'] == "1"){
            $letter_filter = " re.is_subtenant_account = 'Y' AND ";
        }
        elseif($_REQUEST['letter_filter'] == "2"){
            $letter_filter = " re.is_resident_director = 'Y' AND ";
        }
        else{
            $letter_filter = " re.is_subtenant_account = 'N' AND re.is_resident_director = 'N' AND ";
        }

        //portfolio filter
        if($_REQUEST['portfolio_filter'] == "CSJ"){
            $letter_filter = " rmc.rmc_region = 'CSJ' AND ";
        }
        elseif($_REQUEST['portfolio_filter'] == "FAS"){
            $letter_filter .= " rmc.rmc_region = 'FAS' AND ";
        }elseif($_REQUEST['portfolio_filter'] == "PFP"){
            $letter_filter .= " rmc.rmc_region LIKE 'PFP%' AND ";
        }elseif($_REQUEST['portfolio_filter'] == "RMG"){
            $letter_filter .= " rmc.rmc_region != 'FAS' AND  rmc.rmc_region != 'CSJ' AND rmc.rmc_region NOT LIKE 'PFP%' AND  ";
        }
        else{
            $letter_filter .= " ";
        }

        $sql_resident = "
		SELECT *
		FROM cpm_residents re
		INNER JOIN cpm_rmcs rmc ON re.rmc_num = rmc.rmc_num
		INNER JOIN cpm_residents_extra rex ON re.rmc_num = rex.rmc_num AND re.resident_num = rex.resident_num
		INNER JOIN cpm_lookup_residents lre ON re.resident_num = lre.resident_lookup
		INNER JOIN cpm_rmcs_extra rmcx ON rmc.rmc_num = rmcx.rmc_num
		INNER JOIN cpm_lookup_rmcs lrmc  ON rmc.rmc_num = lrmc.rmc_lookup
		WHERE $template_clause $letter_filter rmc.rmc_status <> 'Lost' AND rmc.rmc_is_active = '1' AND
		rex.is_linked_to_master_account <> 'Y' AND
		lre.resident_ref NOT LIKE 't0%' AND
		re.resident_is_developer = 'N' AND
		re.resident_name <> 'Developer' AND
		rmcx.prevent_printing_of_letters <> 'Y' AND
		re.resident_status = 'Current' AND re.resident_is_active='1' $all LIMIT 100 ";

    }
}
elseif($_REQUEST['type'] == "6"){

    if($_REQUEST['contractor_id'] != ""){
        $sql_resident = "
		SELECT * 
		FROM cpm_contractors
		WHERE 
		cpm_contractors_qube_id = '".$_REQUEST['contractor_id']."'
		LIMIT 1 ";
    }else{
        $sql_resident = "
		SELECT * 
		FROM cpm_contractors
		WHERE 
		cpm_contractors_letter_sent = 'N'
		LIMIT 100 ";
    }
}

$result_resident = @mysql_query($sql_resident);
$num_slips = @mysql_num_rows($result_resident);
?>
<form action="password_slip.php" method="post">
    <div id="header_table" class="style12" style="display:none;height:25px;vertical-align:top;border:1px solid #666666;padding:20px;">
        <div style="width:500px;float:left;">There are&nbsp;<span id="num_to_print_off"></span>&nbsp;password slips to print off</div>
        <? if($_REQUEST['type'] == "2" || $_REQUEST['type'] == "5"){?>
            <div id="letter_filter_div" style="padding:0; margin:0; float:right; width:500px; text-align:right; ">
                <b>Portfolio:</b>&nbsp;<select name="portfolio_filter" id="portfolio_filter">
                    <option value="All">All</option>
                    <option value="RMG" <? if($_REQUEST['portfolio_filter'] == "RMG"){?>selected<? }?>>RMG</option>
                    <option value="CSJ" <? if($_REQUEST['portfolio_filter'] == "CSJ"){?>selected<? }?>>CSJ</option>
                    <option value="FAS" <? if($_REQUEST['portfolio_filter'] == "FAS"){?>selected<? }?>>F&S</option>
                    <option value="PFP" <? if($_REQUEST['portfolio_filter'] == "PFP"){?>selected<? }?>>PFP</option>
                </select>
                <b>Letter Type:</b>&nbsp;<select name="letter_filter" id="letter_filter">
                    <option value="3">Resident letters ONLY</option>
                    <option value="1" <? if($_REQUEST['letter_filter'] == "1"){?>selected<? }?>>Sub-tenant letters ONLY</option>
                    <option value="2" <? if($_REQUEST['letter_filter'] == "2"){?>selected<? }?>>Director letters ONLY</option>
                </select>
                <button type="button" onclick="document.getElementById('print_status').value = '';document.forms[0].submit();"><b>Go</b></button>
            </div>
        <? }?>
    </div>

    <table id="print_table" width="100%" border="0" cellpadding="0" cellspacing="10" bgcolor="#CFD8EB" style="display:none;border:1px solid #666666;">
        <tr onMouseOver="this.style.cursor='hand'">
            <td style="text-align:left;"><span class="style11">STEP 1: </span><span class="style4">Hover on the text at the bottom left corner of pdf to see the icons. Then click on the print icon to print this batch of password slips</span></td>
        </tr>
    </table>
    <table id="warning_table" width="100%" border="0" cellpadding="0" cellspacing="10" bgcolor="#CFD8EB" style="display:none;border:1px solid #666666;">
        <tr>
            <td style="text-align:left;"><span class="style11">STEP 2: </span><span class="style4">Do</span><span class="style11"> NOT</span><span class="style4"> close this window using the close button! You will be given a specific close screen after completeing step 3.</span></td>
        </tr>
    </table>
    <table id="done_table" width="100%" border="0" cellpadding="0" cellspacing="10" bgcolor="#CFD8EB" style="display:none;border:1px solid #666666;">
        <tr onMouseOver="this.style.cursor='hand'">
            <td onClick="confirm_print();" style="text-align:left;"><span class="style11">STEP 3: </span><span class="style4">Click here to confirm that <strong>ALL</strong> the password slips have printed successfully </span></td>
        </tr>
    </table>

    <table id="spacer_table" width="100%" border="0" align="center" cellpadding="0" cellspacing="5">
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
    <div id="gDocs"></div>

    <?php
    //echo $sql_resident;exit;
    //=================================================
    // Start printing the letters to screen
    //=================================================
    $this_slip_num = 1;
    $email_slips = 1;
    $result_resident = @mysql_query($sql_resident);
    while($row_resident = @mysql_fetch_array($result_resident)){

        $loop_ts = microtime(true);

        if($_REQUEST['type'] != "6"){

            $all_resident_nums .= $row_resident['resident_num'].",";

            if($_REQUEST['is_print_history'] == "Y" && $resident->is_first_login == "N"){
                // Do not print include this as this affects the print-out (password has been sent and user has already logged on)
            }
            else{
                $this_slip_num++;
            }
        }else{

            $password = $resident->get_rand_id(8, 'ALPHA', 'UPPER');

            $all_resident_nums .= $row_resident['cpm_contractors_qube_id'].",";

            $all_resident_passwords .= $password.",";

            //Use the appropriate letter format
            if(strtoupper($row_resident['cpm_contractors_pcm']) == 'POST'){
                $this_slip_num++;
            }else{
                $email_slips++;
            }
        }
    }
    if($_REQUEST['type'] != "6"){
        $final_slip = "<strong>" . ($this_slip_num-1) . "</strong>";
    }else{
        $final_slip = "<strong>" . ($email_slips-1) . "</strong> password slips to be emailed and <strong>" . ($this_slip_num-1) . "</strong>";
    }

    if($all_resident_nums != ''){
        $all_resident_nums = substr($all_resident_nums,0,-1);
    }
    if($all_resident_passwords != ''){
        $all_resident_passwords = substr($all_resident_passwords,0,-1);
    }

    ?>
    <input type="hidden" name="print_status" id="print_status" value="done">
    <input type="hidden" name="resident_nums" id="resident_nums" value="<?=$all_resident_nums?>">
    <input type="hidden" name="resident_passwords" id="resident_passwords" value="<?=$all_resident_passwords?>">
    <input type="hidden" name="rmc_num" id="rmc_num" value="<?=$_REQUEST['rmc_num']?>">
    <input type="hidden" name="rmc_nums" id="rmc_nums" value="<?=$_REQUEST['rmc_nums']?>">
    <input type="hidden" name="contractor_id" id="contractor_id" value="<?=$_REQUEST['contractor_id']?>">
    <input type="hidden" name="type" id="type" value="<?=$_REQUEST['type']?>">
    <input type="hidden" name="template" id="template" value="<?=$_REQUEST['template']?>">
    <input type="hidden" name="whichaction" id="whichaction">
    <input type="hidden" name="is_print_history" id="is_print_history" value="<?=$_REQUEST['is_print_history']?>">
    <input type="hidden" name="rmc_op_director_name" id="rmc_op_director_name" value="<?=$rmc->rmc['rmc_op_director_name']?>">
    <input type="hidden" name="all" id="all" value="<?=$_REQUEST['all']?>">

</form>

<script language="javascript">
    document.getElementById('header_table').style.display = "block";
    <? if($num_slips > 0){?>
    document.getElementById('print_table').style.display = "block";
    document.getElementById('warning_table').style.display = "block";
    document.getElementById('done_table').style.display = "block";
    <? }?>
    document.getElementById('num_to_print_off').innerHTML = '<?=$final_slip?>';

    $(document).ready(function(){
        //$url = '/management/view_letters.php?resident_nums='+$('#resident_nums').val()
        console.log("/management/view_letters_test.php?type="+$('#type').val()+"&resident_nums="+$('#resident_nums').val()+"&resident_passwords="+$('#resident_passwords').val()+"&rmc_op_director_name="+$('#rmc_op_director_name').val()+"&rmc_num="+$('#rmc_num').val()+"&is_print_history="+$('#is_print_history').val());
        $('#gDocs').pdfEmbed('load', "/management/view_letters.php?type="+$('#type').val()+"&resident_nums="+$('#resident_nums').val()+"&resident_passwords="+$('#resident_passwords').val()+"&rmc_op_director_name="+$('#rmc_op_director_name').val()+"&rmc_num="+$('#rmc_num').val()+"&is_print_history="+$('#is_print_history').val());
    });
</script>
</body>

</html>
<? ob_flush(); ?>
