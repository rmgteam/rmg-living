<?
require("utils.php");
require_once($UTILS_FILE_PATH."library/functions/check_file_extension.php");
require_once($UTILS_FILE_PATH."management/gen_password.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."security.class.php");
require_once($UTILS_CLASS_PATH."encryption.class.php");
require_once($UTILS_CLASS_PATH."developer.class.php");
require_once($UTILS_CLASS_PATH."developer_user.class.php");
$developer = new developer($_REQUEST['developer_id']);
$developer_user = new developer_user($_REQUEST['resident_num']);
$website = new website;
$crypt = new encryption_class;
$security = new security;



// Determine if allowed access into content management system
$website->allow_cms_access();


// Check access privilege
if($_SESSION['allow_developers'] != 1){header("Location:index.php");exit;}



//===================================
// Save developer
//===================================
if($_REQUEST['which_action'] == "s"){
	
	$save_result = $developer->check_fields($_REQUEST, $_FILES);
	if($save_result === true){
		$save_result = $developer->save($_REQUEST, $_FILES);
	}
}


//==========================================
// Remove developer (and associated logins)
//==========================================
if($_REQUEST['which_action'] == "d"){

	$save_result = $developer->delete();
	if( $save_result === true ){
		$_REQUEST['developer_id'] = "";
	}
}


//===================================
// Save existing developer login
//===================================
if($_REQUEST['which_action'] == "su"){

	$save_result = $developer_user->check_fields($_REQUEST);
	if($save_result === true){
		$save_result = $developer_user->save($_REQUEST);
	}
}

#===================================
# remove user
#===================================
if($_REQUEST['which_action'] == "du"){

	$save_result = $developer_user->delete();
	if( $save_result === true ){
			
		$_REQUEST['username'] = "";
		$_REQUEST['password'] = "";
		$_REQUEST['notes'] = "";
		$_REQUEST['login_expiry_YMD'] = "";
	}	
}


#===================================
# Get developer login
#===================================
$first_login = "N/A";
if($_REQUEST['which_action'] != "s" && $_REQUEST['which_action'] != "su"){

	
	// Get developer user info...
	$sql = "
	SELECT x.password, x.login_expiry_YMD, x.notes, lr.resident_ref, x.developer_id
	FROM cpm_residents_extra x, cpm_lookup_residents lr
	WHERE 
	lr.resident_lookup=x.resident_num AND 
	x.resident_num = ".$_REQUEST['resident_num'];
	$result = @mysql_query($sql);
	$row_developer_user = @mysql_fetch_row($result);
	
	$_REQUEST['username'] = $row_developer_user[3];
	$_REQUEST['login_expiry_YMD'] = $row_developer_user[1];
	$_REQUEST['notes'] = $row_developer_user[2];
	
	// Get password
	if($row_developer_user[0] != ""){
		$_REQUEST['password'] = $crypt->decrypt($UTILS_DB_ENCODE, $row_developer_user[0]);
	}
	
	// Get first time logged on
	$sql_fl = "SELECT stat_ts FROM cpm_stats WHERE resident_num = ".$_REQUEST['resident_num']." AND stat_type=3 ORDER BY stat_ts ASC LIMIT 1";
	$result_fl = @mysql_query($sql_fl);
	$row_fl = @mysql_fetch_row($result_fl);
	if($row_fl[0] != ""){$first_login = date("g:iA d/m/Y", $row_fl[0]);}else{$first_login = "Not yet logged in";}
	
}


?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>RMG Living - Developers</title>
<link href="../styles.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../library/jscript/functions/date_auto_format.js"></script>
<script language="JavaScript" type="text/JavaScript">
<!--

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function save_developer(){
	
	if(document.forms[0].developer_id.value==""){
		document.forms[0].which_action.value = "s";
	}
	else{
		document.forms[0].which_action.value = "s";
	}
	document.forms[0].submit();
}

function save_login(){
	document.forms[0].which_action.value = "su";
	document.forms[0].submit();
}

function new_developer(){
	document.location.replace("developers.php");
}

function remove_developer(){
	if(!confirm("Remove this Developer?")){return;}
	document.forms[0].which_action.value="d";
	document.forms[0].submit();
}

function remove_login(){ 
	
	if(!confirm("Remove this login account?")){return;}
	document.forms[0].which_action.value="du";
	document.forms[0].submit();
}

function get_login(resident_num){
	document.forms[0].which_action.value = "get_login";
	document.forms[0].resident_num.value = resident_num;
	document.forms[0].submit();
}

function openPictureWindow_Fever(imageType,imageName,imageWidth,imageHeight,alt,posLeft,posTop) {  // v4.01
	newWindow = window.open("","newWindow","width="+imageWidth+",height="+imageHeight+",scrollbars=no,left="+posLeft+",top="+posTop);
	newWindow.document.open();
	newWindow.document.write('<html><title>'+alt+'</title><body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" onBlur="self.close()">'); 
	if (imageType == "swf"){
	newWindow.document.write('<object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" codebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0\" width=\"'+imageWidth+'\" height=\"'+imageHeight+'\">');
	newWindow.document.write('<param name=movie value=\"'+imageName+'\"><param name=quality value=high>');
	newWindow.document.write('<embed src=\"'+imageName+'\" quality=high pluginspage=\"http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash\" type=\"application/x-shockwave-flash\" width=\"'+imageWidth+'\" height=\"'+imageHeight+'\">');
	newWindow.document.write('</embed></object>');	}else{
	newWindow.document.write('<img src=\"'+imageName+'\" width='+imageWidth+' height='+imageHeight+' alt=\"'+alt+'\">'); 	}
	newWindow.document.write('</body></html>');
	newWindow.document.close();
	newWindow.focus();
}

function new_login(){
	document.forms[0].username.value = "";
	document.forms[0].resident_num.value = "";
	document.forms[0].password.value = "";
	document.forms[0].login_expiry_YMD.value = "";
	document.forms[0].notes.value = "";
	document.getElementById('first_login_dateTXT').innerHTML = "N/A";
	document.forms[0].new_login_button.disabled = false;
	document.forms[0].save_login_button.disabled = false;
	document.forms[0].remove_login_button.disabled = true;
}
//-->
</script>
</head>
<body class="management_body">
<form action="developers.php" method="post" enctype="multipart/form-data">
<? require($UTILS_FILE_PATH."management/menu.php");?>
<table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr valign="top">
	  <td><table width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="border: 1px #000000 solid">
        <tr>
          <td align="center"><img src="../images/management/icons/developers_icon_new.gif" width="70" height="50" border="0"></td>
        </tr>
        <tr>
          <td align="center"><a href="index.php"><b><< Back to main menu</b></a></td>
        </tr>
      </table></td>
    </tr>
	<tr valign="top">
	  <td>&nbsp;</td>
    </tr>
	<tr valign="top">
	  <td><table width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="border: 1px #999999 solid">
        <tr>
          <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td height="10"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="36%"><img src="../images/srch_16.gif" width="16" height="16" align="absmiddle">&nbsp;&nbsp;Search for a Developer </td>
                      <td width="54%"><input name="developer_search" type="text" size="25"></td>
                      <td width="10%" align="right"><input onClick="document.getElementById('developer_search_frame').src = 'developer_search.php?developer_search=' + document.forms[0].developer_search.value;" type="button" name="go_button" value="  Go  "></td>
                    </tr>
                </table></td>
              </tr>
              <tr>
                <td height="10"></td>
              </tr>
              <tr>
                <td height="10"><iframe src="developer_search.php" frameborder="1" height="100" id="developer_search_frame" scrolling="yes" width="470"></iframe></td>
              </tr>
              <tr>
                <td height="10"><iframe src="blank.php" frameborder="0" height="0" id="developer_get_frame" width="0"></iframe></td>
              </tr>
              <tr>
                <td><table width="300" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="25" style="border:1px solid #666666;" bgcolor="#e6f1e2">&nbsp;</td>
					<td width="275">&nbsp;Indicates they have logged in</td>
				  </tr>
				</table>
				</td>
              </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
	<tr valign="top">
	  <td>&nbsp;</td>
    </tr>
	<tr valign="top">
		<td>
			<table width="100%" border="0" cellpadding="10" cellspacing="0" bgcolor="#FFFFFF" style="border: 1px #666666 solid" align="center">
				<tr>
					<td>
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr valign="top">
								<td>
									<table width="100%" border="0" cellpadding="3" cellspacing="0">
									<tr>
										  <td colspan="2"><strong>Developer details </strong></td>
									  </tr>
									  

											<? if( ($_REQUEST['which_action'] == "s" || $_REQUEST['which_action'] == "d") && $save_result !== true){?>
										  <tr>
										    <td colspan="2"><span class="msg_fail"><b><?=$save_result?></b></span></td>
									      </tr>
										  <tr>
										    <td colspan="2" height="10"></td>
									      </tr>
										  <? }?>
										  
									  
									  <tr>
									    <td colspan="2" height="10"></td>
								      </tr>
									  <tr>
									    <td width="27%">Developer name&nbsp;*</td>
								        <td><input name="developer_name" type="text" id="developer_name" size="30" value="<?=stripslashes($developer->developer_name)?>"></td>
									  </tr>
									  <tr style="display:none;">
											<td>Man. Co. name&nbsp;*</td>
											<td><input name="developer_dummy_rmc_name" type="text" id="developer_dummy_rmc_name" size="30" value="<?=$developer->rmc_name?>"></td>
										</tr>
									  <tr valign="top" style="display:none;">
									    <td>Developer Website </td>
									    <td><input name="developer_url" type="text" id="developer_url" size="30" value="<?=$developer->developer_url?>"></td>
								      </tr>
									 
									  <tr valign="top" style="display:none;">
										  <td>Header image<br>
									      (135w x 70h)</td>
									      <td><input name="developer_header_image" type="file" id="developer_header_image">
										  <?
										  if($developer->developer_id != ""){
										  
										  		$header_src = "../images/developers/".$developer->developer_id."/header_small_".$developer->developer_id.".".$developer->developer_header_small_image_ext;
										  		if(file_exists($header_src)){
													$header_image_wh = @getimagesize($header_src);
													?>
													<a onClick="openPictureWindow_Fever('undefined','<?=$header_src?>','<?=$header_image_wh[0]?>','<?=$header_image_wh[1]?>','Header Image','','')" style="text-decoration:underline;cursor:hand;">View</a>
													<?
										  		}
										  }
										  ?> 
									      </td>
									  </tr>
									  <tr style="display:none;">
										  <td>General image </td>
									      <td><input name="developer_general_image" type="file" id="developer_general_image">
										  <?
										  if($developer->developer_id != ""){
										  	
												$general_src = "../images/developers/".$developer->developer_id."/general_".$developer->developer_id.".jpg";
										  
												if(file_exists($general_src)){
													$general_image_wh = @getimagesize($general_src);
													?>
													<a onClick="openPictureWindow_Fever('undefined','<?=$general_src?>','<?=$general_image_wh[0]?>','<?=$general_image_wh[1]?>','General Info Image','','')" style="text-decoration:underline;cursor:hand;">View</a>
													<?
												}
										  }
										  ?>
										  </td>
									  </tr>
									  <tr><td colspan="2">&nbsp;</td></tr>										
									  <tr><td colspan="2" height="10"></td></tr>
									  <tr>
										  <td colspan="2"><input type="button" name="save_dev_button" value="Save Developer" onClick="save_developer()">
                                            <input <? if($developer->developer_id == ""){?>disabled<? }?> type="button" name="remove_dev_button" value="Remove Developer" onClick="remove_developer()">
                                            <input type="button" value="New Developer" onClick="new_developer()" name="new_dev_button"></td>
									  </tr>
									  
									  
									  
									  <? 
									  if($developer->developer_id != ""){?>
									  <tr>
										  <td colspan="2">&nbsp;</td>
									  </tr>
									  <tr>
										  <td colspan="2">&nbsp;</td>
									  </tr>
									  
									  
									  	<? if( ($_REQUEST['which_action'] == "su" || $_REQUEST['which_action'] == "du") && $save_result !== true){?>
										<tr>
											<td colspan="2" height="10"></td>
									    </tr>
										<tr>
											<td colspan="2"><span class="msg_fail"><b><?=$save_result?></b></span></td>
									    </tr>
										<tr>
											<td colspan="2" height="10"></td>
									    </tr>
										<? }?>
									  
									  
									  
									  <tr>
									    <td colspan="2"><strong>Login details for this developer </strong></td>
									  </tr>
									  <tr>
									    <td colspan="2" height="10"></td>
								      </tr>
									  <tr>
									    <td colspan="2"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                          <tr>
                                            <td height="10">
                                            
                                            	<iframe src="developer_login_search.php?developer_id=<?=$developer->developer_id?>" frameborder="1" height="100" id="developer_login_search_frame" scrolling="yes" width="470"></iframe>
                                            	
                                            </td>
                                          </tr>
                                          <tr>
                                            <td height="10"></td>
                                          </tr>
                                          <tr>
                                            <td><table width="300" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                  <td width="25" style="border:1px solid #666666;" bgcolor="#e6f1e2">&nbsp;</td>
                                                  <td width="275">&nbsp;Indicates they have logged in</td>
                                                </tr>
                                            </table></td>
                                          </tr>
                                        </table></td>
								      </tr>
									  <tr><td colspan="2" height="15"></td></tr>
									  <tr>
									  	<td colspan="2">
									  		The Username and Password must be greater than 6 characters in length. (*) required fields.
									  	</td>
									  </tr>
									  <tr><td colspan="2" height="15"></td></tr>
										<tr>
											<td width="27%">Username *</td>
											<td width="73%"><input name="username" type="text" style="width:200px" value="<?=$_REQUEST['username']?>"></td>
										</tr>
										<tr>
											<td>Password *</td>
											<td><input name="password" type="text" style="width:200px" value="<?=$_REQUEST['password']?>"></td>
										</tr>
										<tr>
										  <td>Expiry date *</td>
										  <td><input name="login_expiry_YMD" type="text" maxlength="10" size="11" value="<?=$_REQUEST['login_expiry_YMD']?>"> 
										  (dd/mm/yyyy) </td>
									  </tr>
										<tr>
											<td valign="top">Notes</td>
											<td valign="top"><textarea name="notes" cols="40" rows="5"><?=$_REQUEST['notes']?></textarea></td>
										</tr>
										<tr>
										  <td valign="top">First login date </td>
										  <td><span id="first_login_dateTXT"><?=$first_login?></span></td>
									  </tr>
										<tr>
										  <td valign="top">&nbsp;</td>
										  <td>&nbsp;</td>
									  </tr>
										<tr><td colspan="2" height="10"></td></tr>
										<tr>
											<td colspan="2">
												<input type="button" name="save_login_button" value="Save Login" onClick="save_login()">
										  <input <? if($_REQUEST['resident_num'] == ""){?>disabled<? }?> name="remove_login_button" type="button" value="Remove Login" onClick="remove_login()">									          <input name="new_login_button" type="button" value="New Login" onClick="new_login()"></td>
									    </tr>
										<? }?>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" name="resident_num" id="resident_num" value="<?=$_REQUEST['resident_num']?>" />
<input type="hidden" name="developer_id" id="developer_id" value="<?=$developer->developer_id?>">
<input type="hidden" name="which_action" id="which_action">
</form>
<p>&nbsp;</p>
</body>
</html>
