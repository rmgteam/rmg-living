<?
require("../utils.php");
require_once($UTILS_FILE_PATH."library/functions/check_file_extension.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."rmc.class.php");
$website = new website;
$rmc = new rmc;
$rmc->set_rmc($_REQUEST['rmc_num']);

// Determine if allowed access into content management system
$website->allow_cms_access();

// Check access privilege
if($_SESSION['allow_rmc'] != 1){header("Location:index.php");}

#=========================================
# Delete insurance cert file
#=========================================
if($_REQUEST['whichaction'] == "delete_insur_cert"){

	$del_error = "N";

	$sql = "SELECT * FROM cpm_insur_certs WHERE insur_cert_id=".$_REQUEST['insur_cert_file_id'];
	$result = @mysql_query($sql) or $del_error = "Y";
	$row = @mysql_fetch_array($result);
	$file_name = $row['insur_cert_file'];

	$sql = "DELETE FROM cpm_insur_certs WHERE insur_cert_id=".$_REQUEST['insur_cert_file_id'];
	@mysql_query($sql) or $del_error = "Y";
	
	if($del_error == "N"){
		@unlink($UTILS_PRIVATE_FILE_PATH."insur_certs/$file_name");
	}
	
}


#=========================================
# Add insurance cert file
#=========================================
if($_REQUEST['which_action'] == "add_insur_cert"){
	
	$file_stamp = time();
	
	# Save insurance cert file to correct location
	$file_ext[] = "pdf";
	if(check_file_extension($_FILES['insur_cert_file']['name'], $file_ext)){
		$insur_cert_file = "insur_cert_".$rmc->rmc['rmc_ref']."_".$file_stamp.".pdf";
		$insur_cert_file_path = $UTILS_PRIVATE_FILE_PATH."insur_certs/".$insur_cert_file;
		copy($_FILES['insur_cert_file']['tmp_name'], $insur_cert_file_path);	
	
		# Insert insurance cert details
		$sql = "
		INSERT INTO cpm_insur_certs(
			rmc_num,
			insur_cert_type_id,
			insur_cert_file,
			insur_cert_file_stamp
		)
		VALUES(
			".$_REQUEST['rmc_num'].",
			'".$_REQUEST['insur_cert_type']."',
			'".$insur_cert_file."',
			'".$file_stamp."'
		)
		";
		mysql_query($sql) || die($sql);
		}
}
?>
<html>
<head>
<link href="../styles.css" rel="stylesheet" type="text/css">
</head>
<body>
<form action="insur_certs.php">
<table border="0" cellpadding="3" cellspacing="0" width="100%">
<?
#===============================================
# Collect all insurance certs for this RMC
#===============================================
$sql = "SELECT * FROM cpm_insur_certs ic, cpm_insur_cert_types it WHERE ic.insur_cert_type_id=it.insur_cert_type_id AND ic.rmc_num = ".$_REQUEST['rmc_num'];
$result = @mysql_query($sql);
$num_agm = @mysql_num_rows($result);
if($num_agm < 1){
	print "<tr><td>There are no insurance certificates for this RMC</td></tr>";
}
else{
	while($row = @mysql_fetch_array($result)){
		?>
		<tr bgcolor="#f3f3f3"><td style="border-bottom:1px solid #cccccc"><?=$row['insur_cert_type']?></td>
		  <td align="right" style="border-bottom:1px solid #cccccc"><input onClick="if(confirm('Remove <?=$row['insur_cert_type']?> insurance certificate for this RMC ?')){document.forms[0].insur_cert_file_id.value='<?=$row['insur_cert_id']?>';document.forms[0].submit();}" type="button" name="Button" value="remove"></td>
		</tr>
		<?
	}
}
?>
</table>
<input type="hidden" id="insur_cert_file_id" name="insur_cert_file_id">
<input type="hidden" id="rmc_num" name="rmc_num" value="<?=$_REQUEST['rmc_num']?>">
<input type="hidden" name="whichaction" value="delete_insur_cert">
</form>
</body>
</html>