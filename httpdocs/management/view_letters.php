<?php
require_once("utils.php");
require_once($UTILS_CLASS_PATH . "letters/letters.class.php");
require_once($UTILS_CLASS_PATH . "contractors.class.php");
require_once($UTILS_CLASS_PATH."resident.class.php");
require_once($UTILS_CLASS_PATH."rmc.class.php");
require_once($UTILS_CLASS_PATH . "data.class.php");
require_once($UTILS_CLASS_PATH . "letter.class.php");
require_once($UTILS_CLASS_PATH."unit.class.php");
require_once($UTILS_CLASS_PATH . "pdf/html2text.class.php");
require_once($UTILS_CLASS_PATH . "template.class.php");


$letter = new letter();
$data = new data();
$html_to_text = new html2text();
$template = new template();
$unit = new unit();
$resident = new resident();
$rmc = new rmc();

$letter_pdf = new letters('P', 'mm', 'A4');
$letter_pdf->SetDefaultFont('Arial', '', 11);
$letter_pdf->setStyle("b", "Arial", "B");
$letter_pdf->setStyle("i", "Arial", "I");
$margin = 35;
$letter_pdf->bMargin = $margin;

$letter_pdf->include_header = true;
$letter_pdf->line_height = 4.5;

$date_today = date('d M Y');
if(trim($_REQUEST['resident_nums'])!='')
    $resident_nums = explode(",", $_REQUEST['resident_nums']);
if(trim($_REQUEST['resident_passwords'])!='')
    $resident_passwords = explode(",", $_REQUEST['resident_passwords']);
$resident_nums_cnt = COUNT($resident_nums);


//=================================================
// Start printing the letters to screen
//=================================================
if($resident_nums_cnt==0)
    exit;
foreach ($resident_nums as $key => $residentnum) {

    if ($_REQUEST['type'] != "6") {

        if ($_REQUEST['type'] == "1" || $_REQUEST['type'] == "2" || $_REQUEST['type'] == "5") {
            $unit->set_unit($residentnum);
            if ($unit->unit_ref == "9999") {
                continue;
            }
        }
        if ($_REQUEST['is_print_history'] == "Y" && $resident->is_first_login == "N") {
            // Do not print include this as this affects the print-out (password has been sent and user has already logged on)
        } else {
            // Set various objects to use in the letter
            $resident->resident($residentnum);
            $rmc->set_rmc($resident->rmc_num);
            // Set unit label
            $unit_label = "";
            if ($resident->is_subtenant_account == "Y") {
                $unit_label = stripslashes($resident->resident_address_1);
                if ($resident->resident_address_2 != "") {
                    $unit_label .= ", " . stripslashes($resident->resident_address_2);
                }
            } else {
                if ($unit->unit['unit_description'] != "") {
                    $unit_label = stripslashes($unit->unit['unit_description']);
                } else {
                    $unit_label = stripslashes($unit->unit['unit_address_1']);
                }
            }

            $letter_pdf->SetMargins(30, $margin, 30);

            if($rmc->subsidiary_code == "sco")
            {
                $letter_pdf->header_template = $UTILS_TEMPLATE_PATH . "RMG_Scotland_Headed_Paper.pdf";
            }else if( $rmc->region == 'FAS'){
                $letter_pdf->header_template = $UTILS_TEMPLATE_PATH . "FS_Letter_Headed_Paper.pdf";
            }else if( $rmc->region == 'CSJ'){
                $letter_pdf->header_template = $UTILS_TEMPLATE_PATH . "CSJ_Letter_Head_Paper.pdf";
            }else if( $rmc->region == 'LON'){
                $letter_pdf->header_template = $UTILS_TEMPLATE_PATH . "PLYM_Letter_Headed_Paper.pdf";
            }else{
                $letter_pdf->header_template = $UTILS_TEMPLATE_PATH . "RMG_Letter_Headed_Paper.pdf";
            }
            $letter_pdf->add_page();

            $letter_pdf->Ln($letter_pdf->line_height);
            $letter_pdf->Ln($letter_pdf->line_height);
            $letter_pdf->Ln($letter_pdf->line_height);
            $letter_pdf->SetFont('Arial', '', 10);
            $letter_pdf->line_height = 3.5;
            // Make letter reference
            $letter_pdf->parse_html("<p>" . $resident->resident_name . "</p>");

            $address = $resident->get_address('array');
            $address_lines = count($address);
            foreach($address as $key=>$address_row){
                $letter_pdf->parse_html("<p>" . $address_row . "</p>");
            }
            $letter_pdf->line_height = 4.5;
            $letter_pdf->SetFont('Arial', '', 11);
            for($i=$address_lines;$i<=10;$i++){
                $letter_pdf->Ln();
            }

            $letter_pdf->parse_html("<p>" . $date_today . "</p>");
            $letter_pdf->Ln();
            $your_unit = 'Property: '.$unit_label;
            $letter_pdf->parse_html("<p><b>" . $your_unit . "</b></p>");
            $letter_pdf->Ln();
            $your_ref = 'Ref: '.stripslashes($rmc->rmc_ref . "-" . $resident->resident_ref);
            $letter_pdf->parse_html("<p><b>" . $your_ref . "</b></p>");
            $letter_pdf->Ln();
            $letter_pdf->add_salutation($resident->resident_name);

            $template1 = new template();

            //fs letters body
              /*if( $rmc->region == 'FAS') {
                $prefilename = $UTILS_TEMPLATE_PATH . "letter_cpm_living_welcome_fsbody.php";
                $template1->parse(get_defined_vars());
                $html_content = $template1->fetch($prefilename);

                $letter_pdf->parse_html($html_content);

                $letter_pdf->add_page();

                $template2 = new template();
                $prefilename2 = $UTILS_TEMPLATE_PATH . "letter_cpm_living_welcome_fsbody2.php";
                $template2->parse(get_defined_vars());
                $html_content = $template1->fetch($prefilename2);

                $letter_pdf->parse_html($html_content);
                $letter_pdf->add_page();
              }*/

            //FS Ground Rent Properties
            if( $rmc->region == 'FAS' && $rmc->property_manager == 'Ground Rent Only') {

                $prefilename = $UTILS_TEMPLATE_PATH . "letter_cpm_living_welcome_fs_gr_body.php";

                $template1->parse(get_defined_vars());
                $html_content = $template1->fetch($prefilename);

                $letter_pdf->parse_html($html_content);

                $letter_pdf->add_page();

            }

            if ($resident->is_resident_director == "Y") {
                $filename = $UTILS_TEMPLATE_PATH . "letter_cpm_living_welcome_body_3_new.php";
            } elseif ($resident->is_subtenant_account == "Y") {
                $filename = $UTILS_TEMPLATE_PATH . "letter_cpm_living_welcome_body_4_new.php";
            } else {
                $filename = $UTILS_TEMPLATE_PATH . "letter_cpm_living_welcome_body_1_new.php";
            }


            $template->parse(get_defined_vars());
            $html_content = $template->fetch($filename);

            $letter_pdf->parse_html($html_content);
            if( $rmc->rmc['rmc_op_director_name'] == 'Ground Rent Only') {
                $letter_pdf->parse_html( "<p><br />Yours sincerely,</p><p>Sarah Lamprell<br />Ground Rent Analyst</p>" );
            }else {
                $letter_pdf->parse_html( "<p><br />Yours sincerely,</p><p>" . stripslashes( $rmc->rmc['rmc_op_director_name'] ) . "<br />Operations Director</p>" );
            }
        }
    } else { //type is 6

        $password = $resident_passwords[$key];
        $contractors = new contractors($residentnum);

        if ($contractors) {
            if(strtoupper($contractors->contractor_pcm) == 'POST')
            {
                $letter_pdf->SetMargins(30,$margin, 30);
                $letter_pdf->header_template = $UTILS_TEMPLATE_PATH . "RMG_Letter_Headed_paper.pdf";
                $letter_pdf->add_page();
                $letter_pdf->Ln($letter_pdf->line_height);
                $letter_pdf->Ln($letter_pdf->line_height);
                $letter_pdf->Ln($letter_pdf->line_height);
                $letter_pdf->SetFont('Arial', '', 10);
                $letter_pdf->line_height = 3.5;
                $letter_pdf->parse_html("<p>" . $contractors->contractor_name . "</p>");
                $address = preg_split( '/\n\r|\r\n/', $contractors->contractor_address);
                $address_lines = count($address);
                foreach($address as $key=>$address_row){
                    $letter_pdf->parse_html("<p>" . $address_row . "</p>");
                }
                $letter_pdf->line_height = 4.5;
                $letter_pdf->SetFont('Arial', '', 11);
                for($i=$address_lines;$i<=9;$i++){
                    $letter_pdf->Ln();
                }
                $letter_pdf->parse_html("<p>" . $date_today . "</p>");
                $letter_pdf->Ln();
                $letter_pdf->parse_html("<p><b>Ref: " . $contractors->contractor_qube_id . "</b></p>");
                $letter_pdf->Ln();
                $letter_pdf->add_salutation($contractors->contractor_name);
                //get letter template
                $filename = $UTILS_TEMPLATE_PATH . "letter_cpm_living_welcome_contractors_new.php";
                //replace template variables
                $template->parse(get_defined_vars());
                $html_content = $template->fetch($filename);
                $letter_pdf->parse_html($html_content);

                $letter_pdf->parse_html("<p><br />Yours sincerely,</p><p>Mark Abram<br />Business Support Director</p>");
            }
        }
    }
}
$letter_pdf->Output();
?>