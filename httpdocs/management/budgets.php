<?
require("utils.php");
include($UTILS_FILE_PATH."library/convert_db_dates.php");
require_once($UTILS_FILE_PATH."library/functions/check_file_extension.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."rmc.class.php");
$website = new website;
$rmc = new rmc;
$rmc->set_rmc($_REQUEST['rmc_num']);

// Determine if allowed access into content management system
$website->allow_cms_access();

// Check access privilege
if($_SESSION['allow_rmc'] != 1){header("Location:index.php");}

#=========================================
# Delete budget file
#=========================================
if($_REQUEST['whichaction'] == "delete_budget"){

	$del_error = "N";

	$sql = "SELECT * FROM cpm_budgets WHERE budget_id=".$_REQUEST['budget_id'];
	$result = @mysql_query($sql) or $del_error = "Y";
	$row = @mysql_fetch_array($result);
	$file_name = $row['budget_file'];

	$sql = "DELETE FROM cpm_budgets WHERE budget_id=".$_REQUEST['budget_id'];
	@mysql_query($sql) or $del_error = "Y";
	
	if($del_error == "N"){
		@unlink($UTILS_PRIVATE_FILE_PATH."budgets/$file_name");
	}
	
}


#=========================================
# Add budget file
#=========================================
if($_REQUEST['which_action'] == "add_budget"){
	
	$file_stamp = time();
	
	# Save budget file to correct location
	$file_ext[] = "pdf";
	if(check_file_extension($_FILES['budget_file']['name'], $file_ext)){
		$budget_file = "budget_".$rmc->file_friendly_name()."_".$file_stamp.".pdf";
		$budget_file_path = $UTILS_PRIVATE_FILE_PATH."budgets/".$budget_file;
		copy($_FILES['budget_file']['tmp_name'], $budget_file_path);


		# Insert budget details
		$sql = "
		INSERT INTO cpm_budgets(
			rmc_num,
			budget_type_id,
			budget_from,
			budget_to,
			budget_file,
			budget_file_stamp
		)
		VALUES(
			".$_REQUEST['rmc_num'].",
			".$_REQUEST['budget_type'].",
			'".substr($_REQUEST['budget_from'], 6, 4).substr($_REQUEST['budget_from'], 3, 2).substr($_REQUEST['budget_from'], 0, 2)."',
			'".substr($_REQUEST['budget_to'], 6, 4).substr($_REQUEST['budget_to'], 3, 2).substr($_REQUEST['budget_to'], 0, 2)."',
			'".$budget_file."',
			'".$file_stamp."'
		)
		";
		mysql_query($sql) || die($sql);
	}
		
}
?>
<html>
<head>
<link href="../styles.css" rel="stylesheet" type="text/css">
</head>
<body>
<form action="budgets.php">
<table border="0" cellpadding="3" cellspacing="0" width="100%">
<?
#===============================================
# Collect all budgets for this RMC
#===============================================
$sql = "SELECT * FROM cpm_budgets b, cpm_budget_types bt WHERE b.budget_type_id=bt.budget_type_id AND b.rmc_num = ".$_REQUEST['rmc_num'];
$result = @mysql_query($sql);
$num_agm = @mysql_num_rows($result);
if($num_agm < 1){
	print "<tr><td style=\"padding:10px;\"><img src=\"../images/about_16.gif\" align=\"absmiddle\">&nbsp;There are no budgets for this RMC</td></tr>";
}
else{
	while($row = @mysql_fetch_array($result)){
		?>
		<tr bgcolor="#f3f3f3"><td style="border-bottom:1px solid #cccccc"><?=$row['budget_type_name']?> (<?=convert_date_from_db_1($row['budget_from'])." - ".convert_date_from_db_1($row['budget_to'])?>)</td>
		  <td align="right" style="border-bottom:1px solid #cccccc"><input onClick="if(confirm('Remove budget for this RMC ?')){document.forms[0].budget_id.value='<?=$row['budget_id']?>';document.forms[0].submit();}" type="button" name="Button" value="remove"></td>
		</tr>
		<?
	}
}
?>
</table>
<input type="hidden" id="budget_id" name="budget_id">
<input type="hidden" id="rmc_num" name="rmc_num" value="<?=$_REQUEST['rmc_num']?>">
<input type="hidden" name="whichaction" value="delete_budget">
</form>
</body>
</html>