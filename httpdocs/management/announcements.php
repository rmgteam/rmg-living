<?
ini_set("max_execution_time", "1200");
require("utils.php");
Global $UTILS_TEL_MAIN, $UTILS_CLASS_PATH;
require_once($UTILS_CLASS_PATH."website.class.php");
$website = new website;


// Determine if allowed access into content management system
$website->allow_cms_access();

// Check access privilege
if($_SESSION['allow_announcements'] != 1){header("Location:index.php");}

// get number of lessees
if($_REQUEST['whichaction'] == "presend"){

	$announce_date = date("Ymd", $thistime);
	$add_success = "Y";
	$total_sent = 0;

	// Get all RMC numbers
	$sql_rmc_num = "SELECT rmc_num, rmc_name FROM cpm_rmcs WHERE (rmc_status = 'Active' || rmc_is_active = '1')";
	$result_rmc_num = @mysql_query($sql_rmc_num);
	while($row_rmc_num = @mysql_fetch_row($result_rmc_num)){
		
		$sql = "SELECT rex.email FROM cpm_residents res, cpm_residents_extra rex WHERE res.resident_num=rex.resident_num AND (res.resident_status = 'Current' OR res.resident_is_active='1') AND rex.is_developer <> 'Y' AND rex.email <> '' AND rex.email IS NOT NULL AND rex.email <> 'webmaster@rmgliving.co.uk' AND rex.announce_optin='Y' AND res.rmc_num = ".$row_rmc_num[0];
		$result = @mysql_query($sql);
		$num_residents = @mysql_num_rows($result);
		
		$total_sent += $num_residents;
	}
	
	$result_array['total'] = $total_sent;
	
	echo json_encode($result_array);
	exit;
}

// Send announcements to all lessees
if($_REQUEST['whichaction'] == "send"){

	$announce_date = date("Ymd", $thistime);
	$add_success = "Y";
	$total_sent = 0;

	// Get all RMC numbers
	$sql_rmc_num = "SELECT rmc_num, rmc_name FROM cpm_rmcs WHERE (rmc_status = 'Active' || rmc_is_active = '1')";
	$result_rmc_num = @mysql_query($sql_rmc_num);
	while($row_rmc_num = @mysql_fetch_row($result_rmc_num)){

		// Insert announcment (pre-approval)
		$sql = "
		INSERT INTO cpm_announcements(
			rmc_num,
			announce_title,
			announce_text,
			announce_date,
			announce_approved,
			announce_accept_stamp,
			announce_reject_stamp
		)
		VALUES(
			".$row_rmc_num[0].",
			'".addslashes($_REQUEST['announce_title'])."',
			'".addslashes($_REQUEST['announce_text'])."',
			'".$announce_date."',
			'Y',
			'',
			''
		)
		";
		@mysql_query($sql) or $add_success = "N";
		
		$sql_insid = "SELECT LAST_INSERT_ID() FROM cpm_announcements";
		$result_insid = mysql_query($sql_insid);
		$row_insid = mysql_fetch_row($result_insid);
		$announce_id = $row_insid[0];
	
	
		// Add announcement to all Lessee accounts
		$sql = "SELECT resident_num FROM cpm_residents WHERE rmc_num = ".$row_rmc_num[0];
		$result = @mysql_query($sql);
		while($row = @mysql_fetch_row($result)){
			$sql2 = "
			INSERT INTO cpm_announcements_read(
				announce_id,
				resident_num,
				announce_read
			)
			VALUES(
				".$announce_id.",
				".$row[0].",
				'N'
			)
			";
			@mysql_query($sql2);
		}
		
		
		// Send announcement to all lessees
		$message = "From: Residential Management Group Ltd
To:   All Lessees of ".$row_rmc_num[1]."

".addslashes($_REQUEST['announce_text'])."

Kind Regards,

Technical Support Team
Residential Management Group Ltd

www.rmgliving.co.uk
Tel. '.$UTILS_TEL_MAIN.'
";
		
		$sql = "SELECT rex.email FROM cpm_residents res, cpm_residents_extra rex WHERE res.resident_num=rex.resident_num AND (res.resident_status = 'Current' OR res.resident_is_active='1') AND rex.is_developer <> 'Y' AND rex.email <> '' AND rex.email IS NOT NULL AND rex.email <> 'webmaster@rmgliving.co.uk' AND rex.announce_optin='Y' AND res.rmc_num = ".$row_rmc_num[0];
		$result = @mysql_query($sql);
		$num_residents = @mysql_num_rows($result);
		while($row = @mysql_fetch_row($result)){
		
			$sql = "
			INSERT INTO cpm_mailer (
			mail_to,
			mail_from,
			mail_subject,
			mail_message
			)
			VALUES(
			'".$row[0]."',
			'customerservice@rmguk.com',
			'".addslashes($_REQUEST['announce_title'])."',
			'".$message."'
			)
			";
			@mysql_query($sql);

			
		}
		
		$total_sent += $num_residents;
	}
	
	$result_array['total'] = $total_sent;
	
	echo json_encode($result_array);
	exit;
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>RMG Living - Announcements</title>
<link href="../styles.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="/css/custom-theme/jquery-ui-1.8.16.custom.css"/>
<style>
.ui-widget { font-family: Verdana,Arial,sans-serif; font-size: 0.7em; }
</style>
<script type="text/javascript" language="JavaScript" src="/library/jscript/jquery-1.6.2.min.js"></script>
<script type="text/javascript" language="JavaScript" src="/library/jscript/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#processing_dialog").dialog({  
			modal: true,
			open: function(event, ui) { $(".ui-dialog-titlebar-close").hide();},
			autoOpen: false
		});		
		
		$("#dialog_alert").dialog({  
			modal: true,
			open: function(event, ui) { $(".ui-dialog-titlebar-close").show();},
			buttons: {
				Cancel: function() {
					$( this ).dialog( "close" );
				},
				Ok: function() {
					$( this ).dialog( "close" );
					doSend();
				}
			},
			autoOpen: false
		});
	});
	
	function checkSend() {
		$("#processing_dialog").dialog('open');
		
		$('#whichaction').val('presend');
		
		$.post("announcements.php", 
		$("#form1").serialize(), 
		function(data){			
			$('#error_message').html(data['total']);
			$("#processing_dialog").dialog('close');
			$('#dialog_alert').dialog('open');
		}, 
		"json");
	}
	
	function doSend() {
		$("#processing_dialog").dialog('open');
		
		$('#whichaction').val('send');
		
		$.post("announcements.php", 
		$("#form1").serialize(), 
		function(data){	
			var strHtml = "<b>This announcement has been sent to " + data['total'] + " residents.</b>";
			$('#messages_sent').html(strHtml);
			$("#processing_dialog").dialog('close');
		}, 
		"json");
	}

</script>
</head>
<body class="management_body">
<form id="form1" method="post">
	<div id="processing_dialog" title="Processing" style="display:none;">
		<p style="text-align:center;display:none;margin-bottom:10px;" id="processing_dialog_message"></p>
		<p style="text-align:center;"><img src="/images/ajax-loader.gif" /></p>
	</div>
	<div id="dialog_alert" title="Are you sure?" style="display:none;">
		<p>Are you sure you wish to continue?</p>
		<p>This will send an email to <span id="error_message"></span> lessees</p>
	</div>
	<? require($UTILS_FILE_PATH."management/menu.php");?>
	<table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr valign="top">
			<td><table width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="border: 1px #000000 solid">
					<tr>
						<td align="center"><img src="../images/management/icons/announce_icon.gif" width="70" height="50" /></td>
					</tr>
					<tr>
						<td align="center"><a href="index.php"><b>&lt;&lt; Back to main menu</b></a></td>
					</tr>
				</table></td>
		</tr>
		<tr valign="top">
			<td>&nbsp;</td>
		</tr>
		<tr valign="top">
			<td><table width="700" border="0" align="center" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="border: 1px #000000 solid">
					<tr valign="top">
						<td><table border="0" cellpadding="0" cellspacing="0">
								<tr valign="top">
									<td>
										<span id="messages_sent"></span>
										<table width="680" border="0" cellspacing="0" cellpadding="5">
											<tr>
												<td colspan="2">Enter the announcement details into the fields below, then click 'Send' to email it to all registered Lessees and post it to all Management Company websites. </td>
											</tr>
											<tr>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td valign="top" style="vertical-align:top">Title</td>
												<td valign="top" style="vertical-align:top"><input name="announce_title" type="text" size="70" /></td>
											</tr>
											<tr>
												<td valign="top" style="vertical-align:top">Announcement Text </td>
												<td valign="top" style="vertical-align:top"><textarea name="announce_text" cols="80" rows="10" style="text-align:left"></textarea></td>
											</tr>
											<tr>
												<td width="124">&nbsp;</td>
												<td width="536"><input name="send_button" type="button" onclick="checkSend()" id="send_button" value=" Send " /></td>
											</tr>
										</table></td>
								</tr>
							</table></td>
					</tr>
				</table></td>
		</tr>
	</table>
	<input type="hidden" id="whichaction" name="whichaction" value="" />
</form>
</body>
</html>
