<?
require("utils.php");
require_once($UTILS_FILE_PATH."library/functions/check_file_extension.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."rmc.class.php");
$website = new website;
$rmc = new rmc;
$rmc->set_rmc($_REQUEST['rmc_num']);

// Determine if allowed access into content management system
$website->allow_cms_access();

// Check access privilege
if($_SESSION['allow_rmc'] != 1){header("Location:index.php");}

#=========================================
# Delete car park file
#=========================================
if($_REQUEST['whichaction'] == "delete_car_park"){

	$del_error = "N";

	$sql = "SELECT * FROM cpm_car_park WHERE car_park_id=".$_REQUEST['car_park_file_id'];
	$result = @mysql_query($sql) or $del_error = "Y";
	$row = @mysql_fetch_array($result);
	$file_name = $row['car_park_file'];

	$sql = "DELETE FROM cpm_car_park WHERE car_park_id=".$_REQUEST['car_park_file_id'];
	@mysql_query($sql) or $del_error = "Y";
	
	if($del_error == "N"){
		unlink($UTILS_PRIVATE_FILE_PATH."car_park_plans/$file_name");
	}
	
}


#=========================================
# Add car park file
#=========================================
if($_REQUEST['which_action'] == "add_car_park"){
	
	$file_stamp = time();

	# Save car park file to correct location
	$file_ext[] = "pdf";
	if(check_file_extension($_FILES['carpark_file']['name'], $file_ext)){
		
		$car_park_file_path = $UTILS_PRIVATE_FILE_PATH."car_park_plans/car_park_plans_".$_REQUEST['rmc_num']."_".$file_stamp.".pdf";
		//print $car_park_file_path;
		//exit;
		$car_park_file_name = "car_park_plans_".$rmc->file_friendly_name()."_".$file_stamp.".pdf";
		copy($_FILES['carpark_file']['tmp_name'], $car_park_file_path);
	
		# Insert AGM details into agm table
		$sql = "
		INSERT INTO cpm_car_park(
			rmc_num,
			car_park_file,
			car_park_type_id,
			car_park_file_stamp
		)
		VALUES(
			".$_REQUEST['rmc_num'].",
			'$car_park_file_name',
			".$_REQUEST['car_park_type'].",
			'".$file_stamp."'
		)
		";
		mysql_query($sql);
	}
}
?>
<html>
<head>
<link href="../styles.css" rel="stylesheet" type="text/css">
</head>
<body>
<form action="car_park.php">
<table border="0" cellpadding="3" cellspacing="0" width="100%">
<?
#===============================================
# Collect all car par plans for this RMC
#===============================================
$sql = "SELECT * FROM cpm_car_park WHERE rmc_num = ".$_REQUEST['rmc_num']." ORDER BY car_park_file_stamp DESC";
$result = @mysql_query($sql);
$num_car_park = @mysql_num_rows($result);
if($num_car_park < 1){
	print "<tr><td>There are no Car Park plans for this RMC</td></tr>";
}
else{
	$i=1;
	while($row = @mysql_fetch_array($result)){
		?>
		<tr bgcolor="#f3f3f3"><td style="border-bottom:1px solid #cccccc">&nbsp;Car Park Plan (<?=$i?>)</td>
		  <td align="right" style="border-bottom:1px solid #cccccc"><input onClick="if(confirm('Remove Car Park plan for this RMC ?')){document.forms[0].car_park_file_id.value='<?=$row['car_park_id']?>';document.forms[0].submit();}" type="button" name="Button" value="remove"></td>
		</tr>
		<?
		$i++;
	}
}
?>
</table>
<input type="hidden" id="car_park_file_id" name="car_park_file_id">
<input type="hidden" id="rmc_num" name="rmc_num" value="<?=$_REQUEST['rmc_num']?>">
<input type="hidden" name="whichaction" value="delete_car_park">
</form>
</body>
</html>