<?
require("utils.php");
require_once($UTILS_CLASS_PATH."website.class.php");
$website = new website;

// Determine if allowed access into content management system
$website->allow_cms_access();

// Check access privilege
if($_SESSION['allow_rmc'] != 1 && $_SESSION['allow_residents'] != 1){header("Location:index.php");}
?>
<html> 
<head>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.style2 {color: #003366}
.style3 {color: #9E0C0C}
-->
</style>
</head>
<body>
<style type="text/css">
<!--
.style1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #003366;
}
-->
</style>
<table width="100%"  border="0" cellspacing="0" cellpadding="4">
<?
if($_REQUEST['whichaction'] == "search"){
	
	#====================================================
	# Get list of rmc's based on search term
	#====================================================
	if($_REQUEST['rmc_num'] != ""){
		$whereclause = "rmc.rmc_num = ".$_REQUEST['rmc_num'];
	}
	else{
		$whereclause = "(rmc.rmc_name LIKE '%".$_REQUEST['rmc_search']."%' OR lrmc.rmc_ref = '".$_REQUEST['rmc_search'] . "')";
	}

	$sql = "
	SELECT rmc.rmc_num, rmc.rmc_name, lrmc.rmc_ref 
	FROM cpm_rmcs rmc, cpm_lookup_rmcs lrmc 
	WHERE 
	lrmc.rmc_lookup=rmc.rmc_num AND 
	(rmc.rmc_status = 'Active' OR rmc.rmc_is_active = '1') AND 
	$whereclause 
	ORDER BY rmc.rmc_name ASC
	";
	
	$result = @mysql_query($sql);
	$num_rmcs = @mysql_num_rows($result);

	if($num_rmcs > 0){
		while($row = @mysql_fetch_row($result)){
			?>
		  	<tr onClick="parent.document.forms[0].rmc_num.value='<?=$row[0]?>';parent.changeRmc('<?=$row[0]?>', '<?=$_REQUEST['rmc_search']?>');"  onMouseOver="this.style.background='#D5E2FF'" bgcolor="#f3f3f3" onMouseOut="this.style.background='#f3f3f3'">
		    <td style="cursor:hand;border-bottom:1px solid #cccccc;" class="style1"><?=$row[1]?>&nbsp;(<?=$row[2]?>)</td>
		  	</tr>
			<?
		}
	}
	else{
		?>
		<tr><td class="style1" style="padding:5px;"><img src="../images/help_16.gif" width="16" height="16" align="left"><span class="style3">The system could not find any RMC's matching your search criteria</span></td>
		</tr>
		<?
	}
}
else{
	?>
	<tr><td class="style1" style="padding:5px;"><img src="../images/about_16.gif" align="absmiddle">&nbsp;<span class="style2">Enter the RMC name/number in the field above</span></td>
	</tr>
	<?
}
?>
</table>
</body>
</html>