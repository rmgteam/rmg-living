<?
require("utils.php");
require_once($UTILS_FILE_PATH."library/functions/check_file_extension.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."rmc.class.php");
$website = new website;
$rmc = new rmc;
$rmc->set_rmc($_REQUEST['rmc_num']);

// Determine if allowed access into content management system
$website->allow_cms_access();

// Check access privilege
if($_SESSION['allow_rmc'] != 1){header("Location:index.php");}

#=========================================
# Delete newsletter file
#=========================================
if($_REQUEST['whichaction'] == "delete_newsletter"){

	$del_error = "N";

	$sql = "SELECT * FROM cpm_newsletters WHERE newsletter_id=".$_REQUEST['newsletter_file_id'];
	$result = @mysql_query($sql) or $del_error = "Y";
	$row = @mysql_fetch_array($result);
	$file_name = $row['newsletter_file'];

	$sql = "DELETE FROM cpm_newsletters WHERE newsletter_id=".$_REQUEST['newsletter_file_id'];
	@mysql_query($sql) or $del_error = "Y";
	
	if($del_error == "N"){
		@unlink($UTILS_PRIVATE_FILE_PATH."newsletters/$file_name");
	}
	
}


#=========================================
# Add newsletter file
#=========================================
if($_REQUEST['which_action'] == "add_newsletter"){

	$issue_date = explode("/", $_REQUEST['newsletter_issue_date']);
	$thistime = time();
	
	# Save newsletter file to correct location
	$file_ext[] = "pdf";
	if(check_file_extension($_FILES['newsletter_file']['name'], $file_ext)){
		$newsletter_file = "newsletter_".$rmc->file_friendly_name()."_".$thistime.".pdf";
		$newsletter_file_path = $UTILS_PRIVATE_FILE_PATH."newsletters/".$newsletter_file;
		copy($_FILES['newsletter_file']['tmp_name'], $newsletter_file_path);

		# Insert newsletter details
		$sql = "
		INSERT INTO cpm_newsletters(
			rmc_num,
			newsletter_title,
			newsletter_issue_date,
			newsletter_file,
			newsletter_file_stamp
		)
		VALUES(
			".$_REQUEST['rmc_num'].",
			'".addslashes($_REQUEST['newsletter_title'])."',
			'".$issue_date[2].$issue_date[1].$issue_date[0]."',
			'".$newsletter_file."',
			'".$thistime."'
		)
		";
		mysql_query($sql) || die($sql);
	}
}
?>
<html>
<head>
<link href="../styles.css" rel="stylesheet" type="text/css">
</head>
<body>
<form action="newsletters.php">
<table border="0" cellpadding="3" cellspacing="0" width="100%">
<?
#===============================================
# Collect all newsletters for this RMC
#===============================================
$sql = "SELECT * FROM cpm_newsletters WHERE rmc_num = ".$_REQUEST['rmc_num'];
$result = @mysql_query($sql);
$num_newsletters = @mysql_num_rows($result);
if($num_newsletters < 1){
	print "<tr><td>There are no newsletters for this RMC</td></tr>";
}
else{
	while($row = @mysql_fetch_array($result)){
		?>
		<tr bgcolor="#f3f3f3">
			<td style="border-bottom:1px solid #cccccc"><? print substr($row['newsletter_title'],0,50);print "(".substr($row['newsletter_issue_date'],6,2)."/".substr($row['newsletter_issue_date'],4,2)."/".substr($row['newsletter_issue_date'],2,2).")";?></td>
		  <td align="right" style="border-bottom:1px solid #cccccc"><input onClick="if(confirm('Remove newsletter for this RMC ?')){document.forms[0].newsletter_file_id.value='<?=$row['newsletter_id']?>';document.forms[0].submit();}" type="button" name="Button" value="remove"></td>
		</tr>
		<?
	}
}
?>
</table>
<input type="hidden" id="newsletter_file_id" name="newsletter_file_id">
<input type="hidden" id="rmc_num" name="rmc_num" value="<?=$_REQUEST['rmc_num']?>">
<input type="hidden" name="whichaction" value="delete_newsletter">
</form>
</body>
</html>