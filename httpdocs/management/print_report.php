<?
ini_set("max_execution_time","120");

require("utils.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."resident.class.php");
$website = new website;
$resident = new resident;

// Determine if allowed access into content management system
$website->allow_cms_access();

// Check access privilege
if($_SESSION['allow_report'] != 1 && $_SESSION['report_1'] != '1'){header("Location:index.php");}

if($_REQUEST['whichaction'] == "done"){

	for($o=0;$o < count($_REQUEST['done']);$o++){
		$sql = "
		UPDATE cpm_orders SET
		order_voyager_entered_ts = '".$thistime."',
		order_voyager_entered = 'Y',
		order_voyager_entered_user_id = ".$_SESSION['user_id']."
		WHERE order_id = ".$_REQUEST['done'][$o];
		@mysql_query($sql) or $error_array[] = $_REQUEST['done'][$o];
	}
	
	if(count($error_array) > 0){
		$message = "The following transactions could not be updated:\\n";
		for($m=0;$m<count($error_array);$m++){
			$message .= "Order Id: ".$m."\\n";
		}
		mail("support@rmgliving.co.uk","Update Failed for Online Payments Receivables Report",$message);
		$update_success = "N";
	}
	else{
		$update_success = "Y";
	}
}

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>RMG Living - Report</title>
<link href="../styles.css" rel="stylesheet" type="text/css">
<style type="text/css" media="screen">

body {
	padding:20px;
}
.norm_table {
	max-width:800px;
}
.style1 {font-size: 12px}
.style2 {
	color: #336633;
	font-size: 12px;
}
.style3 {
	color: #CC3333;
	font-size: 12px;
}
</style>
<style type="text/css" media="print">
body {
	padding:0;
}
.norm_table {
	max-width:800px;
}
.style1 {font-size: 12px}
.style2 {
	color: #336633;
	font-size: 12px;
}
.style3 {
	color: #CC3333;
	font-size: 12px;
}
#filter_table {
	display:none;
}
</style>
<script type="text/javascript">
function save_changes(){

	var at_least_one_checked = false;

	for(a=0;a<document.forms[0].elements.length;a++){
		if(document.forms[0].elements[a].type == "checkbox"){
			if(document.forms[0].elements[a].checked == true){
				at_least_one_checked = true;
				break;
			}
		}
	}
	
	if(at_least_one_checked !== true){
		alert("You have not ticked any of the transactions to be processed.");
		return false;
	}

	if(confirm("Are you sure you want to submit this information? Click 'Ok' to continue.")){
		document.getElementById('whichaction').value = "done";
		document.form1.submit();
	}
}
function do_filter(){
	
	if(document.getElementById('date_from').value == "" || document.getElementById('date_to').value == ""){
		alert("You must complete both the 'From' and 'To' date fields.");
		return false;
	}
	
	document.getElementById('whichaction').value = "filter";
	document.form1.submit();
}
function reset_filter(){
	document.getElementById('date_from').value = "";
	document.getElementById('date_to').value = "";
	document.getElementById('whichaction').value = "show_report";
	document.form1.submit();
}
</script>
</head>

<body>

	<form id="form1" name="form1">
		
		<table class="norm_table" border="0" align="center" cellpadding="4" cellspacing="0">
		  <tr>
			  <td ><span class="style1"><strong>Online Payments Report (RMG Living) printed: <?=date("d/m/y H:i:s", $thistime)?></strong></span></td>
			  <td align="right" ><span class="style1">Page 1</span></td>
		  </tr>
		  <tr>
			  <td colspan="10" height="25"></td>
		  </tr>
		</table>
			
		<? 
		if($_REQUEST['whichaction'] == "done" && $update_success == "Y"){
			?>
			<table class="norm_table" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td><span class="style2"><img src="../images/tick_20.gif" />&nbsp;These transactions have been successfully updated.</span></td>
			</tr>
			<tr>
				<td colspan="10" height="25"></td>
		  	</tr>
			</table>
			<? 
		}
		elseif($_REQUEST['whichaction'] == "done" && $update_success == "N"){
			?>
			<table class="norm_table" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td><span class="style3"><img src="../images/del_16.gif" />&nbsp;These transactions could not be updated. The system <a href="mailto:support@rmgliving.co.uk">administrator</a> has been notified.</span></td>
			</tr>
			<tr>
				<td colspan="10" height="25"></td>
		  	</tr>
			</table>
			<?
		}
		
		$page_num = 1;
		$sc_total = 0;
		$gr_total = 0;
		$charge_total = 0;
		
		$processed_clause = " order_voyager_entered <> 'Y' AND ";
		
		if($_REQUEST['whichaction'] == "filter"){
			
			if($_REQUEST['date_from'] != ""){
				$from_split = explode("/",$_REQUEST['date_from']);
				$from_ymd = $from_split[2].$from_split[1].$from_split[0];
				$from_clause = " order_YMD >= ".$from_ymd." AND ";
			}
			if($_REQUEST['date_to'] != ""){
				$to_split = explode("/",$_REQUEST['date_to']);
				$to_ymd = $to_split[2].$to_split[1].$to_split[0];
				$to_clause = " order_YMD <= ".$to_ymd." AND ";
			}
			$processed_clause = "";
		}
		
		$sql_rep = "
		SELECT *  
		FROM cpm_orders 
		WHERE 
		$from_clause 
		$to_clause 
		$processed_clause 
		order_transactionstatus = 'Success' 
		ORDER BY order_TS DESC
		";
		//print $sql_rep;
		$result_rep = @mysql_query($sql_rep);
		$num_rep = @mysql_num_rows($result_rep);
		$row_counter=0;
		$break_counter=0;
		
		if($num_rep == 0){
		
			?>
			<table class="norm_table" border="0" align="center" cellpadding="4" cellspacing="0">
				<tr>
				<td><span class="style1">&nbsp;
				<? if($_REQUEST['whichaction'] == "show_report"){?>
				There are currently no new transactions since the last report.
				<? }else{?>
				There are no transactions between the date(s) provided.
				<? }?>
				</span></td>
				</tr>
			</table>
			<?
		}
		else{
		
			?>
	 
			<table id="filter_table" class="norm_table" border="0" align="center" cellpadding="8" cellspacing="0" style="background-color:#f1f1f1;border:1px solid #999999;">
				<tr>
					<td>From:&nbsp;<input type="text" name="date_from" id="date_from" size="10" maxlength="10" value="<?=$_REQUEST['date_from']?>" />&nbsp;(dd/mm/yyyy)&nbsp;&nbsp;&nbsp;&nbsp;To:&nbsp;<input type="text" name="date_to" id="date_to" size="10" maxlength="10" value="<?=$_REQUEST['date_to']?>" />&nbsp;(dd/mm/yyyy)&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" name="filter_button" id="filter_button" value="Filter" onClick="do_filter()" /><input type="button" name="reset_button" id="reset_button" value="Reset" onClick="reset_filter()" /></td>
					<td align="right"></td>
				</tr>
			</table>
	
			<table class="norm_table" border="0" align="center" cellpadding="4" cellspacing="0">
				<tr>
				  <td colspan="10" height="15"></td>
				</tr>
				<tr>
				  <td colspan="10">
				  
				  <table  border="0" cellspacing="0" cellpadding="0">
					<tr>
					  <td width="50%"><input name="check_all_button" type="button" id="check_all_button" value="Check All" onClick="check_all()">
						  <input name="uncheck_all_button" type="button" id="uncheck_all_button" value="Uncheck All" onClick="uncheck_all()"></td>
					  <td width="50%" align="right"><input name="completed_button" type="button" id="completed_button" value="Mark as Completed" onClick="save_changes()" /></td>
					</tr>
				  </table>
				  
				  </td>
				</tr>
				<tr><td colspan="10" bgcolor="#333333" height="1"></td></tr>
				<tr>
				  <td width="32" align="center" nowrap style="border-left:1px solid #333333;border-bottom:1px solid #333333;border-right:1px solid #cccccc;"><img title="Completed" src="../images/checklist_16.gif" width="16" height="16" align="absmiddle" border="0"></td>
				  <td width="39" nowrap style="border-bottom:1px solid #333333;border-right:1px solid #cccccc;"><strong>No.</strong></td>
				  <td width="80" nowrap style="border-bottom:1px solid #333333;border-right:1px solid #cccccc;"><strong>Date</strong></td>
				  <td width="50" nowrap style="border-bottom:1px solid #333333;border-right:1px solid #cccccc;"><strong>Brand</strong></td>
				  <td width="80" style="border-bottom:1px solid #333333;border-right:1px solid #cccccc"><strong>Tenant ID </strong></td>
				  <td width="80" style="border-bottom:1px solid #333333;border-right:1px solid #cccccc"><strong>Order Ref </strong></td>
				  <td width="90" align="right" style="border-bottom:1px solid #333333;border-right:1px solid #cccccc"><strong>S' Charge(&pound;)</strong></td>
				  <td width="162" colspan="2" align="right" style="border-bottom:1px solid #333333;border-right:1px solid #cccccc"><strong>G' Rent(&pound;)</strong></td>
				  <td width="77" align="right" style="border-bottom:1px solid #333333;border-right:1px solid #333333"><strong>Charge(&pound;)</strong></td>
				</tr>
				
				<?
				while($row_rep = @mysql_fetch_array($result_rep)){
				  
				  	if(preg_match("/t/",$row_rep['resident_num']) !== 1){
				  		
						//$resident->resident($row_rep['resident_num']);
						//$resident_ref = $resident->resident_ref;
						
						// Added this in becasue the resident record may no longer exist, so the 'resident' object cannot be used here.
						$sql = "SELECT * FROM cpm_lookup_residents WHERE resident_lookup = ".$row_rep['resident_num'];
						$result = @mysql_query($sql);
						$row = @mysql_fetch_array($result);
						$resident_ref = $row['resident_ref'];
						
						// Need to show resident ref from the linked account, rather than Director ref
						$dir_resident = new resident($row_rep['resident_num']);
						if( $dir_resident->is_resident_director == "Y" ){
							
							$real_resident = new resident( $dir_resident->director_assoc_resident_num );
							$resident_ref = $real_resident->resident_ref;
						}
					}
					else{
						$resident_ref = $row_rep['resident_num'];
					}
				  
					// If order is placed via RMG site, place total into service charge column
					if($row_rep['order_type'] == "2"){
						$row_rep['order_service_charge'] = $row_rep['order_subtotal'];
					}
			  		
					?>
					<tr <? if($row_counter%2 == 0){print "bgcolor='#f1f1f1'";}?>>
					  <td align="center" style="border-left:1px solid #333333;border-right:1px solid #cccccc;">
					  <? if($row_rep['order_voyager_entered'] != "Y"){?>
					  <input name="done[]" type="checkbox" value="<?=$row_rep['order_id']?>">
					  <? }else{?>
					  <img src="/images/tick_12.gif" border="0" />
					  <? }?>
					  </td>
					  <td style="border-right:1px solid #cccccc;"><?=($row_counter+1)?></td>
					  <td style="border-right:1px solid #cccccc;"><?=date("d/m/Y", $row_rep['order_TS'])?></td>
					  <td style="border-right:1px solid #cccccc;"><?=$row_rep['brand_code']?>&nbsp;</td>
					  <td style="border-right:1px solid #cccccc;"><?=$resident_ref?></td>
					  <td style="border-right:1px solid #cccccc;"><?=$row_rep['order_ref']?></td>
					  <td style="border-right:1px solid #cccccc;" align="right"><? if($row_rep['order_service_charge'] != ""){print number_format(str_replace(",","",$row_rep['order_service_charge']),"2",".",",");$sc_total += str_replace(",","",$row_rep['order_service_charge']);}else{print "0.00&nbsp;";}?></td>
					  <td colspan="2" align="right" style="border-right:1px solid #cccccc;"><? if($row_rep['order_ground_rent'] != ""){print number_format(str_replace(",","",$row_rep['order_ground_rent']),"2",".",",");$gr_total += str_replace(",","",$row_rep['order_ground_rent']);}else{print "0.00&nbsp;";}?></td>
					  <td align="right" style="border-right:1px solid #333333;"><? if($row_rep['order_charge'] != ""){print number_format(str_replace(",","",$row_rep['order_charge']),"2",".",",");$charge_total += str_replace(",","",$row_rep['order_charge']);}else{print "&nbsp;";}?></td>
					</tr>
					<?
					
					$row_counter++;
					$break_counter++;
					if($break_counter > 20){
				
						$break_counter=0;
						$page_num++;
						?>
						<tr>
							<td colspan="10" bgcolor="#333333" height="1"></td>
						</tr>
						<tr>
							<td colspan="10">&nbsp;</td>
						</tr>
						<tr>
						   <td>&nbsp;</td>
						   <td>&nbsp;</td>
						   <td>&nbsp;</td>
						   <td>&nbsp;</td>
						   <td>&nbsp;</td>
						   <td>&nbsp;</td>
						   <td align="right"><strong><?=number_format($sc_total,2,".",",")?></strong></td>
						   <td colspan="2" align="right"><strong><?=number_format($gr_total,2,".",",")?></strong></td>
						   <td align="right"><strong><?=number_format($charge_total,2,".",",")?></strong></td>
						</tr>
						<tr>
						   <td colspan="10">&nbsp;</td>
						</tr>
						<tr>
						   <td colspan="10">&nbsp;</td>
						</tr>
						<tr>
						   <td colspan="10">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="10">
							
								<table width="500" border="0" cellspacing="0" cellpadding="0">
									<tr>
									<td width="41">Date:</td>
									<td width="126">____________</td>
									<td width="77" align="right">Signature:</td>
									<td width="256">_________________________________</td>
									</tr>
								</table>
						 
							</td>
						</tr>
						</table>
						
						<p class='break'><!--&nbsp;--></p>
						
						<table class="norm_table" border="0" align="center" cellpadding="4" cellspacing="0">
						<tr>
							<td colspan="8" ><span class="style1"><strong>Online Payments Report (RMG Living) printed: <?=date("d/m/y H:i:s", $thistime)?></strong></span></td>
							<td width="92" colspan="2" align="right" ><span class="style1">Page <?=$page_num?></span></td>
						</tr>
						<tr>
							<td colspan="10" height="5"></td>
						</tr>
						<tr>
							<td colspan="5" style="border-bottom:1px solid #333333;">&nbsp;</td>
							<td colspan="5" style="border-bottom:1px solid #333333;">&nbsp;</td>
						</tr>
						<tr>
							<td width="37" align="center" nowrap style="border-left:1px solid #333333;border-bottom:1px solid #333333;border-right:1px solid #cccccc;"><img title="Completed" src="../images/checklist_16.gif" width="16" height="16" align="absmiddle"></td>
							<td width="41" nowrap style="border-bottom:1px solid #333333;border-right:1px solid #cccccc;"><strong>No.</strong></td>
							<td width="95" nowrap style="border-bottom:1px solid #333333;border-right:1px solid #cccccc;"><strong>Date</strong></td>
							<td width="50" nowrap style="border-bottom:1px solid #333333;border-right:1px solid #cccccc;"><strong>Brand</strong></td>
							<td width="113" style="border-bottom:1px solid #333333;border-right:1px solid #cccccc"><strong>Tenant ID </strong></td>
							<td width="103" style="border-bottom:1px solid #333333;border-right:1px solid #cccccc"><strong>Order Ref </strong></td>
							<td width="102" align="right" style="border-bottom:1px solid #333333;border-right:1px solid #cccccc"><strong>S' Charge(&pound;)</strong></td>
							<td colspan="2" align="right" style="border-bottom:1px solid #333333;border-right:1px solid #cccccc"><strong>G' Rent(&pound;)</strong></td>
							<td align="right" style="border-bottom:1px solid #333333;border-right:1px solid #333333"><strong>Charge(&pound;)</strong></td>
						</tr>
						<?
					}
				}
				?>
			  
				<tr>
					<td colspan="10" bgcolor="#333333" height="1"></td>
				</tr>
				<tr>
					<td colspan="10">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td align="right"><strong><?=number_format($sc_total,"2",".",",")?></strong></td>
					<td colspan="2" align="right"><strong><?=number_format($gr_total,"2",".",",")?></strong></td>
					<td align="right"><strong><?=number_format($charge_total,"2",".",",")?></strong></td>
				</tr>
				<tr>
					<td colspan="10">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="10">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="10">&nbsp;</td>
				</tr>
				<tr>
				<td colspan="9">
					
					<table width="500" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="41">Date:</td>
						<td width="126">____________</td>
						<td width="77" align="right">Signature:</td>
						<td width="256">_________________________________</td>
					</tr>
					</table>
					
				</td>
			</tr> 
			</table>
			<?
		}
		?>

		<input type="hidden" name="whichaction" id="whichaction" value="done">
		
	</form>
	<script language="javascript">
	function check_all(){
		for(a=0;a<document.forms[0].elements.length;a++){
			if(document.forms[0].elements[a].type == "checkbox"){
				document.forms[0].elements[a].checked = true;
			}
		}
	}
	function uncheck_all(){
		for(a=0;a<document.forms[0].elements.length;a++){
			if(document.forms[0].elements[a].type == "checkbox"){
				document.forms[0].elements[a].checked = false;
			}
		}
	}
	</script>
	
</body>
</html>
