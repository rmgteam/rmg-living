<?
require("utils.php");
require_once($UTILS_FILE_PATH."library/functions/check_file_extension.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."rmc.class.php");
$website = new website;
$rmc = new rmc;

// Determine if allowed access into content management system
$website->allow_cms_access();

// Check access privilege
if($_SESSION['allow_rmc'] != 1){header("Location:index.php");}

//=========================================
// Delete client file
//=========================================
if($_REQUEST['whichaction'] == "delete_client"){

	$del_error = "N";

	$sql = "SELECT * FROM cpm_client WHERE client_id=".$_REQUEST['client_file_id'];
	$result = @mysql_query($sql) or $del_error = "Y";
	$row = @mysql_fetch_array($result);
	$file_name = $row['client_file'];

	$sql = "DELETE FROM cpm_client WHERE client_id=".$_REQUEST['client_file_id'];
	@mysql_query($sql) or $del_error = "Y";
	
	if($del_error == "N"){
		@unlink($UTILS_PRIVATE_FILE_PATH."client/$file_name");
	}
	
}


#=========================================
# Add client file
#=========================================
if($_REQUEST['which_action'] == "add_client"){

	// Check that no document currently exists
	/// WO 68154 add ability to add multiple documents.
	/*$sql = "SELECT * FROM cpm_client WHERE rmc_num=".$_REQUEST['rmc_num'];
	$result = @mysql_query($sql);
	$client_exists = @mysql_num_rows($result);
	if($client_exists == 0){*/
		
		$file_stamp = time();
		
		# Save client file to correct location
		$file_ext[] = "pdf";
		if(check_file_extension($_FILES['client_file']['name'], $file_ext)){
			$client_file = "client_".$rmc->file_friendly_name()."_".$file_stamp.".pdf";
			$client_file_path = $UTILS_PRIVATE_FILE_PATH."client/".$client_file;
			copy($_FILES['client_file']['tmp_name'], $client_file_path);
					
			# Format dates
			$client_from_date = explode("/", $_REQUEST['client_from_date']);
			$client_to_date = explode("/", $_REQUEST['client_to_date']);
		
		
			# Insert client details
			$sql = "
			INSERT INTO cpm_client(
				rmc_num,
				client_file,
				client_file_stamp,
				client_from_date,
				client_to_date,
				client_title
			)
			VALUES(
				".$_REQUEST['rmc_num'].",
				'".$client_file."',
				'".$file_stamp."',
				'".$client_from_date[2].$client_from_date[1].$client_from_date[0]."',
				'".$client_to_date[2].$client_to_date[1].$client_to_date[0]."',
				'".addslashes($_REQUEST['client_title'])."'
			)
			";
			mysql_query($sql);
		}
	//}
		
}
?>
<html>
<head>
<link href="../styles.css" rel="stylesheet" type="text/css">
</head>
<body>
<form action="client.php">
<table border="0" cellpadding="3" cellspacing="0" width="100%">
<?
#===============================================
# Collect all SLA's for this RMC
#===============================================
$sql = "SELECT * FROM cpm_client WHERE rmc_num = ".$_REQUEST['rmc_num'];
$result = @mysql_query($sql);
$num_client = @mysql_num_rows($result);
if($num_client < 1){
	print "<tr><td>There are no Client Documents for this RMC</td></tr>";
}
else{
	while($row = @mysql_fetch_array($result)){
		?>
		<tr bgcolor="#f3f3f3">
			<td style="border-bottom:1px solid #cccccc"><?=$row['client_title'];?>(<? print substr($row['client_from_date'],6,2)."/".substr($row['client_from_date'],4,2)."/".substr($row['client_from_date'],0,4)." to ".substr($row['client_to_date'],6,2)."/".substr($row['client_to_date'],4,2)."/".substr($row['client_to_date'],0,4);?>)</td>
		  <td align="right" style="border-bottom:1px solid #cccccc"><input onClick="if(confirm('Remove <?=$row['client_title'];?>?')){document.forms[0].client_file_id.value='<?=$row['client_id']?>';document.forms[0].submit();}" type="button" name="Button" value="remove"></td>
		</tr>
		<?
	}
}
?>
</table>
<input type="hidden" id="client_file_id" name="client_file_id">
<input type="hidden" id="rmc_num" name="rmc_num" value="<?=$_REQUEST['rmc_num']?>">
<input type="hidden" name="whichaction" value="delete_client">
</form>
</body>
</html>