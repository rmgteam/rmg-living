<?
require("utils.php");
require_once($UTILS_FILE_PATH."library/functions/check_file_extension.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."rmc.class.php");
$website = new website;
$rmc = new rmc;

// Determine if allowed access into content management system
$website->allow_cms_access();

// Check access privilege
if($_SESSION['allow_rmc'] != 1){header("Location:index.php");}

//=========================================
// Delete SSM file
//=========================================
if($_REQUEST['whichaction'] == "delete_ssm"){

	$del_error = "N";

	$sql = "SELECT * FROM cpm_ssm WHERE ssm_id=".$_REQUEST['ssm_file_id'];
	$result = @mysql_query($sql) or $del_error = "Y";
	$row = @mysql_fetch_array($result);
	$file_name = $row['ssm_file'];

	$sql = "DELETE FROM cpm_ssm WHERE ssm_id=".$_REQUEST['ssm_file_id'];
	@mysql_query($sql) or $del_error = "Y";
	
	if($del_error == "N"){
		@unlink($UTILS_PRIVATE_FILE_PATH."ssm/$file_name");
	}
	
}

#=========================================
# Add SSM file
#=========================================
if($_REQUEST['which_action'] == "add_ssm"){

	// Check that no document currently exists
	/// WO 68154 add ability to add multiple documents.
	/*$sql = "SELECT * FROM cpm_ssm WHERE rmc_num=".$_REQUEST['rmc_num'];
	$result = @mysql_query($sql);
	$ssm_exists = @mysql_num_rows($result);
	if($ssm_exists == 0){*/
		
		$file_stamp = time();
		
		# Save SSM file to correct location
		$file_ext[] = "pdf";
		if(check_file_extension($_FILES['ssm_file']['name'], $file_ext)){
			$ssm_file = "ssm_".$rmc->file_friendly_name()."_".$file_stamp.".pdf";
			$ssm_file_path = $UTILS_PRIVATE_FILE_PATH."ssm/".$ssm_file;
			copy($_FILES['ssm_file']['tmp_name'], $ssm_file_path);
					
			# Format dates
			$ssm_from_date = explode("/", $_REQUEST['ssm_from_date']);
			$ssm_to_date = explode("/", $_REQUEST['ssm_to_date']);
		
		
			# Insert SSM details
			$sql = "
			INSERT INTO cpm_ssm(
				rmc_num,
				ssm_file,
				ssm_file_stamp,
				ssm_from_date,
				ssm_to_date
			)
			VALUES(
				".$_REQUEST['rmc_num'].",
				'".$ssm_file."',
				'".$file_stamp."',
				'".$ssm_from_date[2].$ssm_from_date[1].$ssm_from_date[0]."',
				'".$ssm_to_date[2].$ssm_to_date[1].$ssm_to_date[0]."'
			)
			";
			mysql_query($sql);
		}
	//}
		
}
?>
<html>
<head>
<link href="../styles.css" rel="stylesheet" type="text/css">
</head>
<body>
<form action="ssm.php">
<table border="0" cellpadding="3" cellspacing="0" width="100%">
<?
#===============================================
# Collect all SLA's for this RMC
#===============================================
$sql = "SELECT * FROM cpm_ssm WHERE rmc_num = ".$_REQUEST['rmc_num'];
$result = @mysql_query($sql);
$num_ssm = @mysql_num_rows($result);
if($num_ssm < 1){
	print "<tr><td>There are no Maintenance Files for this RMC</td></tr>";
}
else{
	while($row = @mysql_fetch_array($result)){
		?>
		<tr bgcolor="#f3f3f3">
			<td style="border-bottom:1px solid #cccccc">Maintenance File (<? print substr($row['ssm_from_date'],6,2)."/".substr($row['ssm_from_date'],4,2)."/".substr($row['ssm_from_date'],0,4)." to ".substr($row['ssm_to_date'],6,2)."/".substr($row['ssm_to_date'],4,2)."/".substr($row['ssm_to_date'],0,4);?>)</td>
		  <td align="right" style="border-bottom:1px solid #cccccc"><input onClick="if(confirm('Remove Maintenance File for this RMC ?')){document.forms[0].ssm_file_id.value='<?=$row['ssm_id']?>';document.forms[0].submit();}" type="button" name="Button" value="remove"></td>
		</tr>
		<?
	}
}
?>
</table>
<input type="hidden" id="ssm_file_id" name="ssm_file_id">
<input type="hidden" id="rmc_num" name="rmc_num" value="<?=$_REQUEST['rmc_num']?>">
<input type="hidden" name="whichaction" value="delete_ssm">
</form>
</body>
</html>