<?
require("utils.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."rmc.class.php");
require_once($UTILS_CLASS_PATH."resident.class.php");
require_once($UTILS_CLASS_PATH."master_account.class.php");
require_once($UTILS_CLASS_PATH."security.class.php");
$security = new security;
$rmc = new rmc( $security->clean_query($_REQUEST['rmc_num']) );
$website = new website;
$resident = new resident;
$master_account = new master_account;


// Determine if allowed access into content management system
$website->allow_cms_access();

// Check access privilege
if($_SESSION['allow_residents'] != 1){header("Location:index.php");}



//===================================
// Save exisiting resident
//===================================
if($_REQUEST['which_action'] == "modify"){
	
	$save_error = "Technical fault - there was a problem updating these details.";
	
	$resident->resident($_REQUEST['resident_num']);
	
	if( $resident->is_linked_to_master_account == "Y" ){
	
		$master_account->master_account($resident->get_master_account_id(), "id");
		if($master_account->is_email_unique($_REQUEST['email']) === true){
		
			$sql = "
			UPDATE cpm_master_accounts SET
			master_account_tel = '".$security->clean_query($_REQUEST['tel'])."',
			master_account_mobile = '".$security->clean_query($_REQUEST['mobile'])."',
			master_account_email = '".$security->clean_query($_REQUEST['email'])."'
			WHERE master_account_id = ".$master_account->master_account_id;
			if( @mysql_query($sql) ){
				$save_error = "N";	
			}
			else{
				$save_error = "Technical fault - there was a problem updating these details.";
			}
		}
		else{
			$save_error = "The email you supplied is already in use by another account. Please provide an alternative one.";
		}
	}
	else{
		
		if($resident->is_email_unique($email) === true){
		
			$sql = "
			UPDATE cpm_residents_extra SET
			tel = '".$security->clean_query($_REQUEST['tel'])."',
			mobile = '".$security->clean_query($_REQUEST['mobile'])."',
			email = '".$security->clean_query($_REQUEST['email'])."'
			WHERE resident_num = ".$security->clean_query($_REQUEST['resident_num']);
			if( @mysql_query($sql) ){
				$save_error = "N";	
			}
			else{
				$save_error = "Technical fault - there was a problem updating these details.";
			}
		}
		else{
			$save_error = "The email you supplied is already in use by another account. Please provide an alternative one.";
		}
	}
}


#===================================
# Get resident
#===================================
if($_REQUEST['resident_num'] <> ""){
	
	$sql_resident = "
	SELECT * 
	FROM cpm_lookup_residents lres, cpm_residents re, cpm_residents_extra rex 
	WHERE 
	lres.resident_lookup=re.resident_num AND 
	re.resident_num=rex.resident_num AND 
	re.resident_num = ".$security->clean_query($_REQUEST['resident_num']);
	$result_resident = @mysql_query($sql_resident);
	$row_resident = @mysql_fetch_array($result_resident);
	$resident->resident($_REQUEST['resident_num']);

	// Get unit info
	$sql_unit = "SELECT * FROM cpm_units WHERE resident_num = ".$row_resident['resident_num'];
	$result_unit = @mysql_query($sql_unit);
	$row_unit = @mysql_fetch_array($result_unit);
	
	// Get Master account details, if link exists
	if($resident->is_linked_to_master_account == "Y"){
		$master_account->master_account($resident->get_master_account_id(), "id");
	}
}


?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>RMG Living - Lessees</title>
<link href="../styles.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../library/jscript/functions/valid_email_check.js"></script>
<script language="javascript" src="../library/jscript/functions/hide_letter_box.js"></script>
<script language="JavaScript" type="text/JavaScript">

function save(){
	if(!confirm("Save your changes?")){return;}
	document.forms[0].which_action.value="modify";
	document.forms[0].submit();
}

function remove(){
	if(!confirm("Remove this Lessee?")){return;}
	document.forms[0].which_action.value="remove";
	document.forms[0].submit();
}

function changeRmc(rmc_num, rmc_search){
	location.href = "residents.php?rmc_num=" + rmc_num + "&rmc_search=" + rmc_search;
}

function changeResident(id){
	location.href = "residents.php?resident_num=" + document.forms[0].resident_num.value + "&rmc_num="+ document.forms[0].rmc_num.value;
}

function show_password_slip(template){
	window.open("password_slip.php?template=" + template + "&type=1&rmc_num=<?=$row_resident['rmc_num']?>&resident_num=" + document.forms[0].resident_num.value,"PASSWIN","width=730, height=550, scrollbars=yes, resizable=yes, status=yes");
}

function show_print_history(){
	window.open("print_history.php?type=1&rmc_num=<?=$row_resident['rmc_num']?>&resident_num=" + document.forms[0].resident_num.value,"PRINTHISWIN","width=600, height=550, scrollbars=yes, resizable=yes, status=yes");
}
</script>
<style type="text/css">
<!--
.style1 {
	color: #FFFFFF;
	font-weight: bold;
}
.style2 {color: #669966}
.style4 {color: #006600}
-->
</style>
</head>
<body class="management_body">
<form method="post" action="residents.php">
<? require($UTILS_FILE_PATH."menu.php");?>
<table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr valign="top">
	  <td><table width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="border: 1px #000000 solid">
        <tr>
          <td align="center"><img src="../images/management/icons/lessees_icon_new.gif" width="70" height="50"></td>
        </tr>
        <tr>
          <td align="center"><a href="index.php"><b><< Back to main menu</b></a></td>
        </tr>
      </table></td>
    </tr>
	<tr valign="top">
	  <td>&nbsp;</td>
    </tr>
	<tr valign="top">
		<td>
			<table width="500" border="0" align="center" cellpadding="10" cellspacing="0" bgcolor="#FFFFFF" style="border: 1px #333333 solid">
				<tr>
					<td>
						<table width="480" border="0" cellpadding="0" cellspacing="0">
							
							<tr valign="top">
								<td>
									<table width="480" border="0" cellpadding="3" cellspacing="0">
										<tr>
										  <td colspan="2"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                              <td height="10"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                                  <tr>
                                                    <td colspan="3"><strong>Search for a Man. Co. </strong></td>
                                                  </tr>
                                                  <tr>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td align="right">&nbsp;</td>
                                                  </tr>
                                                  <tr>
                                                    <td width="34%">Man. Co.  name/number </td>
                                                    <td width="39%"><input name="rmc_search" type="text" size="30" value="<?=$_REQUEST['rmc_search']?>" ></td>
                                                    <td width="27%" align="right"><input onClick="document.getElementById('rmc_search_frame').src = 'rmc_search.php?whichaction=search&rmc_search=' + document.forms[0].rmc_search.value;" type="button" name="go_button" value="  Go  ">
                                                    <input onClick="document.forms[0].rmc_num.value = '';document.getElementById('rmc_search_frame').src = 'rmc_search.php';" type="button" name="clear_button" value="Clear"></td>
                                                  </tr>
                                              </table></td>
                                            </tr>
                                            <tr>
                                              <td height="10"><input type="hidden" name="rmc_num" id="rmc_num" value="<?=$_REQUEST['rmc_num']?>"></td>
                                            </tr>
                                            <tr>
                                              <td style="border:1px solid #666666;"><iframe src="rmc_search.php<? if($rmc->rmc_num != ""){print "?whichaction=search&rmc_num=".$rmc->rmc_num;}?>" frameborder="0" height="100" id="rmc_search_frame" scrolling="yes" width="470"></iframe></td>
                                            </tr>
                                            <tr>
                                              <td height="10"></td>
                                            </tr>

                                            <tr>
                                              <td height="10"></td>
                                            </tr>
                                          </table></td>
									  </tr>
										
										<tr>
										  <td colspan="2">&nbsp;</td>
										</tr>
										<tr>
										  <td colspan="2"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                              <td height="10"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                                  <tr>
                                                    <td><strong>Search for a Lessee </strong></td>
                                                    <td>&nbsp;</td>
                                                    <td align="right">&nbsp;</td>
                                                  </tr>
                                                  <tr>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td align="right">&nbsp;</td>
                                                  </tr>
                                                  <tr valign="top">
                                                    <td width="34%">Lessee name, ID or Unit Postcode </td>
                                                    <td width="39%"><input name="resident_search" type="text" size="30"></td>
                                                    <td width="27%" align="right"><input onClick="document.getElementById('resident_search_frame').src = 'resident_search.php?whichaction=search&rmc_num=' + document.forms[0].rmc_num.value + '&resident_search=' + document.forms[0].resident_search.value;" type="button" name="go_button2" value="  Go  "></td>
                                                  </tr>
                                              </table></td>
                                            </tr>
                                            <tr>
                                              <td height="10">&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td bgcolor="#003366" style="border-left:1px solid #999999;border-top:1px solid #999999;border-right:1px solid #999999;">
											  <table border="0" cellpadding="2" width="460">
											  <tr><td style="border-right:1px solid #cccccc;" width="86"><span class="style1">&nbsp;Lessee Type</span></td>
											  <td style="border-right:1px solid #cccccc;" width="77"><span class="style1">&nbsp;Lessee Id</span></td>
											  <td width="277"><span class="style1">&nbsp;Lessee Name</span></td>
											  </tr>
											  </table>
											  </td>
                                            </tr>
                                            <tr>
                                              <td style="border:1px solid #666666;"><iframe src="resident_search.php<? if($_REQUEST['rmc_num']){?>?whichaction=search&rmc_num=<?=$_REQUEST['rmc_num']?>&resident_num=<?=$_REQUEST['resident_num']?><? }?>" frameborder="0" height="100" id="resident_search_frame" scrolling="yes" width="470"></iframe></td>
                                            </tr>
                                            <tr>
                                              <td height="1"><input type="hidden" name="resident_num" id="resident_num" value="<?=$_REQUEST['resident_num']?>"></td>
                                            </tr>
                                          </table></td>
									  </tr>
									  
									  </table>
									  
									  </td>
									  </tr>
									  </table>
									  </td>
									  </tr>
									  </table>
									  </td>
									  </tr>
									<tr><td>&nbsp;</td></tr>
									
									
									<? if($_REQUEST['resident_num'] && $_REQUEST['resident_num'] !=""){?>
									<tr valign="top">
	  									<td>
									  <table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
									
									<tr><td>
									
									
									
									<!-- START OF DETAILS SECTION-->
									<table width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#f1f1f1" style="border: 1px #333333 solid">
                                <tr>
                                  <td width="74%"><strong><img src="../images/user_16.gif" width="16" height="16" align="absmiddle">&nbsp;&nbsp;<img src="../images/management/lessees/lessee_details.gif" width="200" height="16" align="absmiddle"></strong></td>
                                  <td width="26%" align="right">&nbsp;</td>
                                </tr>
                              </table></td></tr>
									<tr valign="top">
	  								<td><table width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="border-left: 1px #333333 solid;border-right: 1px #333333 solid;border-bottom: 1px #333333 solid;border-top: 1px #cccccc solid;">
										<tr>
										<td>
										
										
									  <table width="480" border="0" cellpadding="3" cellspacing="0">
										
										<? if($_REQUEST['which_action'] == "modify" && $save_error != "N"){?>
										<tr>
											<td colspan="2" class="msg_fail"><?=$save_error?></td>
										</tr>
										<tr>
											<td colspan="2">&nbsp;</td>
										</tr>
										<? }?>
										
										<tr>
											<td colspan="2"><a href="resident_auto_login.php?<? if($row_resident['is_linked_to_master_account'] == "Y"){print "ma=y&s=".$master_account->master_account_serial;}else{ print "s=".$row_resident['resident_serial'];}?>" target="_blank">Click here to view this <? if($row_resident['is_linked_to_master_account'] == "Y"){?>Master Account<? }else{?>Lessee's account<? }?></a></td>
										</tr>
										<tr>
											<td colspan="2">&nbsp;</td>
										</tr>
										<tr>
										  <td width="170">RMC Ref.</td>
										  <td width="298"><?=$rmc->rmc_ref?></td>
									  </tr>
										<tr>
											<td>Lessee Ref.</td>
											<td><?=stripslashes($row_resident['resident_ref'])?></td>
										</tr>
										<tr>
                                          <td>Name</td>
                                          <td><?=stripslashes($row_resident['resident_name'])?></td>
									    </tr>
										<tr>
										  <td nowrap>Resident Director ? </td>
										  <td><?
										  if($row_resident['is_resident_director'] == "Y"){print "Yes";}
										  else{print "No";}
										  ?></td>
									  </tr>
										<tr>
										  <td nowrap>Linked to Master Account ? </td>
										  <td><?
										  if($row_resident['is_linked_to_master_account'] == "Y"){print "Yes (Username: ".$master_account->master_account_username.")";}
										  else{print "No";}
										  ?></td>
									  </tr>
										<tr>
										  <td>&nbsp;</td>
										  <td>&nbsp;</td>
									    </tr>
										<tr valign="top">
										  <td>Unit Address </td>
										  <td>
										  <?
										  $unit_address = array();
										  
										  if($row_resident['is_subtenant_account'] == "Y"){
											
											  if($row_resident['resident_address_1'] != ""){$unit_address[] = stripslashes($row_resident['resident_address_1']);}
											  if($row_resident['resident_address_2'] != ""){$unit_address[] = stripslashes($row_resident['resident_address_2']);}
											  if($row_resident['resident_address_3'] != ""){$unit_address[] = stripslashes($row_resident['resident_address_3']);}
											  if($row_resident['resident_address_4'] != ""){$unit_address[] = stripslashes($row_resident['resident_address_4']);}
											  if($row_resident['resident_address_city'] != ""){$unit_address[] = stripslashes($row_resident['resident_address_city']);}
											  if($row_resident['resident_address_county'] != ""){$unit_address[] = stripslashes($row_resident['resident_address_county']);}
											  if($row_resident['resident_address_postcode'] != ""){$unit_address[] = stripslashes($row_resident['resident_address_postcode']);}
										  }
										  else{
											  
										  	if($row_unit['unit_description'] != ""){$unit_address[] = stripslashes($row_unit['unit_description']);}
										  	if($row_unit['unit_address_1'] != ""){$unit_address[] = stripslashes($row_unit['unit_address_1']);}
										  	if($row_unit['unit_address_2'] != ""){$unit_address[] = stripslashes($row_unit['unit_address_2']);}
										  	if($row_unit['unit_postcode'] != ""){$unit_address[] = stripslashes($row_unit['unit_postcode']);}
										  }
										  
										  print implode("<br />", $unit_address);
										  
										  ?>
										  </td>
									    </tr>
										<tr>
										  <td>&nbsp;</td>
										  <td>&nbsp;</td>
									    </tr>
										<tr>
										  <td><strong>Contact Details</strong></td>
										  <td>&nbsp;</td>
									    </tr>
										<tr>
											<td>Address 1</td>
											<td><?=$row_resident['resident_address_1']?></td>
										</tr>
										<tr>
											<td>Address 2</td>
											<td><?=$row_resident['resident_address_2']?></td>
										</tr>
										<tr>
											<td>Address 3</td>
											<td><?=$row_resident['resident_address_3']?></td>
										</tr>
										<tr>
											<td>Address 4</td>
											<td><?=$row_resident['resident_address_4']?></td>
										</tr>
										<tr>
											<td>County</td>
											<td><?=$row_resident['resident_address_city']?></td>
										</tr>
										<tr>
											<td>City</td>
											<td><?=$row_resident['resident_address_county']?></td>
										</tr>
										<tr>
											<td>Post code</td>
											<td><?=$row_resident['resident_address_postcode']?></td>
										</tr>
										<tr>
										  <td colspan="2">&nbsp;</td>
									  </tr>
									  <?
									  	$tel = $row_resident['tel'];
										$mobile =  $row_resident['mobile'];
										$email = $row_resident['email'];
									  	if($row_resident['is_linked_to_master_account'] == "Y"){
										  
										  	$tel = $master_account->master_account_tel;
											$mobile = $master_account->master_account_mobile;
											$email = $master_account->master_account_email;
										  ?>
										<tr>
										  <td colspan="2">The details below are specific to the Master Account that is linked to this record.</td>
									    </tr>
										<? }?>
										<tr>
										  <td>Telephone:</td>
										  <td><input type="text" name="tel" style="width:150px" value="<?=$tel?>"></td>
									  </tr>
										<tr>
										  <td>Mobile telephone: </td>
										  <td><input type="text" name="mobile" style="width:150px" value="<?=$mobile?>"></td>
									  </tr>
										<tr>
										  <td>Email</td>
										  <td><input name="email" type="text" value="<?=$email?>" size="40"></td>
									  </tr>
										
									  <tr><td colspan="2">&nbsp;</td></tr>
										<tr><td colspan="2"><input <? if(!$_REQUEST['resident_num']){?>disabled<? }?> type="button" value=" Save " onClick="save()"></td></tr>
									  

									  </table>
									  
									  </td>
									  </tr>
									  </table>
									  <!-- END OF DETAILS SECTION-->
									  
									  
									  
									  </td>
									  </tr>
									  </table>
									  </td>
									  </tr>
									  
									  
									<tr>
									  <td>&nbsp;</td>
									</tr>
									
									<? if($_SESSION['is_sys_admin'] == "Y"){?>
									<tr><td><table width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#f1f1f1" style="border: 1px #333333 solid">
                                      <tr>
                                        <td width="74%"><strong><img src="../images/lock_16.gif" width="16" height="16" align="absmiddle">&nbsp;&nbsp;<img src="../images/management/lessees/security_questions.gif" width="200" height="16" align="absmiddle"></strong></td>
                                        <td width="26%" align="right">&nbsp;</td>
                                      </tr>
                                    </table></td></tr><tr valign="top">
	  									<td>
									  <table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
									
									<tr><td>

									<table width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="border-left: 1px #333333 solid;border-right: 1px #333333 solid;border-bottom: 1px #333333 solid;border-top: 1px #cccccc solid;">
        								<tr>
										<td>
										
										<?
										$question_id_1 = $row_resident['question_id_1'];
										$answer_1 = $row_resident['answer_1'];
										$question_id_2 = $row_resident['question_id_2'];
										$answer_2 = $row_resident['answer_2'];
										if($row_resident['is_linked_to_master_account'] == "Y"){
											
											$question_id_1 = $master_account->master_account_question_id_1;
											$answer_1 = $master_account->master_account_answer_1;
											$question_id_2 = $master_account->master_account_question_id_2;
											$answer_2 = $master_account->master_account_answer_2;
											
											?>
											<p>The details below are specific to the Master Account that is linked to this record.</p>
											<?
										}
										?>
										
									  <table width="480" border="0" cellpadding="3" cellspacing="0">
										
										<tr>
										  <td>Question 1 </td>
										  <td><select name="security_question_1" id="security_question_1" disabled>
                                            <option value="-" selected>-</option>
                                            <? 
										  $sql_sq = "SELECT * FROM cpm_security_questions";
										  $result_sq = @mysql_query($sql_sq);
										  while($row_sq = @mysql_fetch_array($result_sq)){?>
                                            <option value="<?=$row_sq['question_id']?>" <? if($question_id_1 == $row_sq['question_id']){print "selected";}?>>
                                            <?=$row_sq['question']?>
                                            </option>
                                            <? }?>
                                          </select></td>
										</tr>
										<tr>
										  <td>Answer 1 </td>
										  <td><input name="answer_1" type="text" id="answer_1" value="<?=$answer_1?>" disabled></td>
										</tr>
										<tr>
										  <td>Question 2 </td>
										  <td><select name="security_question_2" id="security_question_2" disabled>
                                            <option value="-" selected>-</option>
                                            <? 
										  $sql_sq = "SELECT * FROM cpm_security_questions";
										  $result_sq = @mysql_query($sql_sq);
										  while($row_sq = @mysql_fetch_array($result_sq)){?>
                                            <option value="<?=$row_sq['question_id']?>" <? if($question_id_2 == $row_sq['question_id']){print "selected";}?>>
                                            <?=$row_sq['question']?>
                                            </option>
                                            <? }?>
                                          </select></td>
										</tr>
										<tr>
										  <td>Answer 2 </td>
										  <td><input name="answer_2" type="text" id="answer_2" value="<?=$answer_2?>" disabled></td>
										</tr>
																	  
									  </table>
									  
									  </td>
									  </tr>
									  </table>
									  </td>
									  </tr>
									  </table>
									  </td>
									  </tr>
									<tr>
									  <td>&nbsp;</td>
									</tr>
									<? }?>
	
									<? if($_SESSION['allow_passwords'] == "1"){?>
									<tr><td>
									<table width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#f1f1f1" style="border: 1px #333333 solid">
                                      <tr>
                                        <td width="74%"><strong><img src="../images/hd_16.gif" width="16" height="16" align="absmiddle">&nbsp;&nbsp;<img src="../images/management/lessees/login_details.gif" width="200" height="16" align="absmiddle"></strong></td>
                                        <td width="26%" align="right">&nbsp;</td>
                                      </tr>
                                    </table>
									</td></tr>
									<tr valign="top">
	  									<td>
									  <table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
									<tr><td>
									<table width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="border-left: 1px #333333 solid;border-right: 1px #333333 solid;border-bottom: 1px #333333 solid;border-top: 1px #cccccc solid;">
        								<tr>
										<td>
									  <table width="480" border="0" cellpadding="0" cellspacing="0">
										 <td colspan="2">
										 <iframe name="password_frame" id="password_frame" src="blank.php" frameborder="0" height="0" width="0"></iframe>
										  <table width="100%"  border="0" cellspacing="0" cellpadding="3">
                                            <? if($email != ""){?>
											<tr>
                                              <td>
											  <? if($row_resident['is_linked_to_master_account'] == "Y"){?>
											  <p>Clicking the button below will reset the password for the Master Account that is linked to this record.</p>
											  <? }?>
											  <input type="button" value="Send New Login Details via Email" onClick="if(confirm('Generating a new password for this user will overwrite\ntheir existing password and require them to set their\nown password next time they log in.\n\nClick OK to continue.')){document.getElementById('password_frame').src='password.php?template=standard&resident_num=<?=$_REQUEST['resident_num']?>&gen=Y&send_type=email';}"></td>
                                            </tr>
                                            <tr>
                                              <td>&nbsp;</td>
                                            </tr>
											<? }?>
											<? 
											if($row_resident['is_linked_to_master_account'] != "Y"){
												
												if($_SESSION['allow_letters'] == "1"){
													?>
													<tr>
													  <td><input type="button" value="Generate New Login Details Letter" onClick="if(confirm('Generating a new password for this user will overwrite\ntheir existing password and require them to set their\nown password next time they log in.\n\nClick OK to continue.')){document.getElementById('password_frame').src='password.php?template=standard&resident_num=<?=$_REQUEST['resident_num']?>&gen=Y&send_type=letter';}"></td>
													</tr>
													<? }else{?>
													<tr>
													  <td><input type="button" value="Send Password via Post" onClick="if(confirm('You are about to send a request for the login details of this Lessee to be sent to their mailing address.\n\nClick OK to continue.')){document.getElementById('password_frame').src='../request.php?lessee_id=<?=$_REQUEST['resident_num']?>&whichaction=login_request&from=cpm';}"></td>
													</tr>
													<?
												}
											}
											?>
                                          </table>
										  </td>
									  </tr>
									  </table>
									  </td>
									  </tr>
									  </table>
									  </td>
									  </tr>
									  </table>
									  </td>
									</tr>
									<tr>
									  <td>&nbsp;</td>
									</tr>
									<? }?>
									
									<? if($_SESSION['allow_letters'] == "1" && $row_resident['is_linked_to_master_account'] != "Y"){?>
									<tr><td>
									<table width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#f1f1f1" style="border: 1px #333333 solid">
                                      <tr>
                                        <td width="74%"><strong><img src="../images/mail_16.gif" width="16" height="16" align="absmiddle">&nbsp;&nbsp;<img src="../images/management/lessees/letters.gif" width="200" height="16" align="absmiddle"></strong></td>
                                        <td width="26%" align="right">&nbsp;</td>
                                      </tr>
                                    </table></td></tr>
									<tr valign="top">
	  									<td>
									  
									  <table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
									<tr><td>
									<table width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="border-left: 1px #333333 solid;border-right: 1px #333333 solid;border-bottom: 1px #333333 solid;border-top: 1px #cccccc solid;">
        								<tr>
										<td>
									  <table width="480" border="0" cellpadding="3" cellspacing="0">
										<tr>
										  <td colspan="2" style="border-bottom:1px #003366 solid;"><img src="../images/management/lessees/first_login_details.gif" width="142" height="16"></td>
									  </tr>
										<tr>
										  <td colspan="2" height="10"></td>
									  </tr>
										<tr>
										  <td colspan="2">Lessee passwords cannot be viewed. If a Lessee has forgotten their password, you can click the 'Generate New Password' button above.</td>
									  </tr>
									<tr>
										  <td colspan="2" height="5"></td>
									  </tr>
									  
									  <?
									  // Get last date that this letter was sent
									  $sql_letter_sent = "SELECT print_job_time FROM cpm_print_jobs WHERE resident_nums LIKE '%".$row_resident['resident_num']."%' ORDER BY print_job_time DESC";
									  $result_letter_sent = @mysql_query($sql_letter_sent);
									  $num_sent = @mysql_num_rows($result_letter_sent);
									  if($num_sent > 0){
											
									  		$row_letter_sent = @mysql_fetch_row($result_letter_sent);
											$last_sent_date = date("d/m/Y", $row_letter_sent[0]);
									  }
									  ?>
									  
										<tr id="standard_sent_row" style="display:<? if($row_resident['password_to_be_sent'] == "N"){?>block<? }else{?>none<? }?>;">
										  <td colspan="2">
										  <table width="100%" border="0" cellspacing="0" cellpadding="5">
										    <tr>
										      <td colspan="2"><span class="style2"><img src="../images/tick.gif" width="24" height="24" align="absmiddle"><span class="style4">This Lessee has been sent their login details <? if($last_sent_date != ""){?>on <span id="last_sent_date"><?=$last_sent_date?></span><? }else{print "(date unknown)";}?>.</span></span></td>
									        </tr>
										  </table>
										  </td>
								  	  </tr>
									  
									  
										<tr id="standard_password_status" style="display:<? if($row_resident['password_to_be_sent'] == "Y" && $row_resident['hide_letter'] == "N"){?>block<? }else{?>none<? }?>;">
										  <td colspan="2">
										  <table width="100%" border="0" cellspacing="0" cellpadding="5">
										    <tr>
                                              <td width="7%" align="left" onClick="show_password_slip('standard')" onMouseOver="this.style.cursor='hand';"><img src="../images/help_24.gif" width="24" height="24" border="0" align="absmiddle"></td>
									          <td width="93%" onClick="show_password_slip('standard')" onMouseOver="this.style.cursor='hand';"><font color="#FF0000">This Lessee has not been sent their login details.<br>Click <strong>here</strong> to print this letter.</font></td>
										    </tr>
										  </table>
										  </td>
									  	</tr>
										
										
										<tr>
										  <td colspan="2">&nbsp;</td>
									    </tr>
									  
									  
									  <!-- Set as  display:none as deemed not necessary -->
									  <tr style="display:none;">
										  <td colspan="2" style="border-bottom:1px #003366 solid;"><img src="../images/management/lessees/site_activation.gif" width="142" height="16"></td>
									  </tr>
										<tr style="display:none;">
										  <td colspan="2" height="10"></td>
									    </tr>
										<tr style="display:none;">
										  <td colspan="2">This letter is sent to Resident Directors once their micro-site has been activated.</td>
									  </tr>
										<tr style="display:none;">
										  <td colspan="2" height="5"></td>
									  </tr>
										<tr id="director_password_status" style="display:none;">
										  <td colspan="2">
										  <table width="100%" border="0" cellspacing="0" cellpadding="5">
										    <tr>
                                              <td width="7%" align="left" onClick="show_password_slip('director')" onMouseOver="this.style.cursor='hand';"><img src="../images/help_24.gif" width="24" height="24" border="0" align="absmiddle"></td>
									          <td width="93%" onClick="show_password_slip('director')" onMouseOver="this.style.cursor='hand';"><font color="#FF0000">This Director has not been sent their 'Site Activation' notification.><br></b>Click <strong>here</strong> to print this letter.</font></td>
										    </tr>
										  </table>
										  </td>
									  	</tr>
										<tr id="director_sent_row" style="display:none;">
										  <td colspan="2">
										  <table width="100%" border="0" cellspacing="0" cellpadding="5">
										    <tr>
										      <td colspan="2"><span class="style4"><img src="../images/tick.gif" width="24" height="24" align="absmiddle">This Director has been sent their 'Site Active' notification.</span></td>
									        </tr>
										  </table>
										  </td>
								  	  </tr>
									  
										<tr>
										  <td colspan="2">&nbsp;</td>
									  </tr>
										<tr>
										  <td colspan="2" style="border-bottom:1px #003366 solid;"><img src="../images/management/lessees/print_history.gif" width="142" height="16"></td>
									  </tr>
										<tr>
										  <td colspan="2" height="10"></td>
									  </tr>
										<tr>
										  <td colspan="2" onMouseOver="this.style.cursor='hand';" onClick="show_print_history('standard');">Click <strong>here</strong> to view the print history for this Lessee. 
										</td>
									  </tr>
									 
								  </table>
							  </td>
							</tr>
					  </table>
					</td>
				</tr>
		  </table>
		        </td>
    </tr>
<? }?>
		<? }?>
</table>
<input type="hidden" name="which_action">
<input type="hidden" name="template" value="<? if($row_resident['is_resident_director'] == "Y"){print "director";}else{print "standard";}?>">
</form>
</body>
</html>
