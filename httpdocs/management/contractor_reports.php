<?
ini_set("max_execution_time", "1440000");
require("utils.php");
require_once($UTILS_CLASS_PATH."data.class.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."contractors.reports.class.php");
require_once($UTILS_CLASS_PATH."contractors.class.php");
$website = new website;
$contractors = new contractors('');
$contractor_report = new contractor_report();

// Determine if allowed access into content management system
$website->allow_cms_access();

// Check access privilege
if($_SESSION['allow_contractor'] != 1){header("Location:index.php");}

#===================================
# Get contractors
#===================================

function get_contractors(){
	$contractors = new contractors('');
	$request['search_term'] = '';
	$result = $contractors->contractor_list($request);
	$options = "";
	
	$num_rows = @mysql_num_rows($result);
	if($num_rows > 0){
		$options .= '<option value="">Please select</option>';
		while($row = @mysql_fetch_array($result)){
			$options .= '<option value="'.$row['cpm_contractors_qube_id'].'">'.$row['cpm_contractors_name'].'</option>';
		}
	}
	
	echo $options;
}

#===================================
# Get orders
#===================================

if ($_REQUEST['whichaction'] == 'order'){
	
	$data = new data;
	
	$result = $contractor_report->purchase_order($_REQUEST['contractor_id'], $_REQUEST);
	$num_results = @mysql_num_rows($result);
	if($num_results > 0){
		
		$output = '<div class="header_row">';
		$output .= '<div class="form_label border" style="width:90px;">PO Number</div>';
		$output .= '<div class="form_label border" style="width:80px;">Date</div>';
		$output .= '<div class="form_label border" style="width:180px;">Property</div>';
		$output .= '<div class="form_label" style="width:75px;">Lines Open</div>';
		$output .= '</div>';
		
		$querystring = $contractor_report->http_parse_query($_REQUEST);
		
		while($row = @mysql_fetch_array($result)){
			
			$lines = $row['no_complete'] + $row['no_incomplete'];
			
			$output .= '<a class="form_row" onclick="javascript:do_job('."'".$row['cpm_po_id']."', '".$querystring."'".');">';
			$output .= '<div class="form_label border" style="width:90px;">'.$row['cpm_po_number'].'</div>';
			$output .= '<div class="form_label border" style="width:80px;">'.$data->ymd_to_date($row['cpm_po_date_raised']).'</div>';
			$output .= '<div class="form_label border" style="width:180px;">'.$row['rmc_name'].'</div>';
			$output .= '<div class="form_label" style="width:75px;">'.$row['no_incomplete']."/".$lines.'</div>';
			$output .= '</a>';
			
		}
	}
	
	$result_array['results'] = $output;
	
	echo json_encode($result_array);
	exit;
}

#===================================
# Get jobs
#===================================

if ($_REQUEST['whichaction'] == 'job'){
	$data = new data;
	
	$result_job = $contractor_report->job($_REQUEST['po_no'], $_REQUEST);
	
	$num_result_jobs = @mysql_num_rows($result_job);
	$per_page = 12;
	$pages = ceil($num_result_jobs/$per_page);
	$page_count = 1;
	$i = 0;
	if($num_result_jobs > 0){
		$output .= '<div class="header_row" style="width:565px;text-align:left;">';
		$output .= '<div class="form_label border" style="width:85px;">Job Number</div>';
		$output .= '<div class="form_label border" style="width:155px;">Expected Completion</div>';
		$output .= '<div class="form_label border" style="width:50px;">Quote</div>';
		$output .= '<div class="form_label border" style="width:120px;">Closed By</div>';
		$output .= '<div class="form_label" style="width:50px;">On</div>';
		$output .= '</div>';
			
		$output .= '<ul class="paging" id="job_pages"><li id="page-1">';
		
		while($row_job = @mysql_fetch_array($result_job)){
			
			$i += 1; 
			
			$output .= '<div class="form_row" style="width:565px;">';
			$output .= '<div class="form_label border" style="width:85px;">'.$row_job['cpm_po_job_no'].'</div>';
			$output .= '<div class="form_label border" style="width:155px;">'.$data->ymd_to_date($row_job['cpm_po_job_completion_date']).'</div>';
			$output .= '<div class="form_label border" style="width:50px;">'.$row_job['cpm_po_job_amount'].'</div>';
			$output .= '<div class="form_label border" style="width:120px;">'.$contractor_report->is_blank($row_job['user_name'], "Not Complete").'</div>';
			$output .= '<div class="form_label" style="width:50px;">'.$contractor_report->is_blank($data->ymd_to_date($row_job['cpm_po_job_ts'])).'</div>';
			$output .= '</div>';
			
			if($row_job['cpm_po_job_reason_id'] != ""){
				$output .= '<div class="sub_header_row" style="width:565px;text-align:left;">';
				$output .= '<div class="form_label" style="width:555px;">Reason for Incompletion</div>';
				$output .= '</div><div class="form_row" style="width:565px;text-align:left;">';
				$output .= '<div class="form_label" style="width:555px;"><strong>'.$row_job['the_reason'].":</strong><br />".$row_job['cpm_po_job_reason'].'</div>';
				$output .= '</div>';	
			}
			
			if($i == $per_page){
				$i = 0;
				$page_count += 1;
				$output .= '</li><li id="page-'.$page_count.'">';
			}
		}	
		
		$output .= '</li></ul>';
		$output .= '<script language="javascript">$(document).ready(function(){ $("#job_pages").paging({pageName : "jobination"}); });</script>';
	}else{
		$output .= $sql_job;	
	}
	
	$output .= '</div>';
	
	$result_array['results'] = $output;
	
	$output = '<ul class="pagination" id="jobination">';
	
	for($i=1; $i<=$pages; $i++){
		$output .= '<li id="'.$i.'"';
		if($i == 1){
			$output .= ' class="active"';
		}
		$output .= ' onclick="javascript:$('."'#job_pages'".').setPage('."'".$i."'".');">'.$i.'</li>';
	}
	$output .= '</ul>';
	
	$result_array['paging'] = $output;
	
	echo json_encode($result_array);
	exit;
}

if ($_REQUEST['whichaction'] == 'excel'){
	header("Content-type: application/vnd.ms-excel");
	header("Content-disposition: attachment; filename=contractor-".date("d-F-Y", time()).".xls");
	header("Cache-Control: maxage=1");
	header("Pragma: public");
	
	$data = new data;
	
	$result = $contractor_report->purchase_order($_REQUEST['contractor_id'], $_REQUEST);
	$num_results = @mysql_num_rows($result);
	if($num_results > 0){
		
		$output = '<table width="100%" border="1" cellspacing="0" cellpadding="4">';
		$output .= '<tr valign="top">';
		$output .= '<td style="color:#fff; background-color:#0066CC; border-color: #333333;">PO Number</td>';
		$output .= '<td style="color:#fff; background-color:#0066CC; border-color: #333333;">Date Raised</td>';
		$output .= '<td style="color:#fff; background-color:#0066CC; border-color: #333333;">Management Company</td>';
		$output .= '<td style="color:#fff; background-color:#0066CC; border-color: #333333;">Type</td>';
		$output .= '<td style="color:#fff; background-color:#0066CC; border-color: #333333;">Description</td>';
		$output .= '<td style="color:#fff; background-color:#0066CC; border-color: #333333;">Job Number</td>';
		$output .= '<td style="color:#fff; background-color:#0066CC; border-color: #333333;">Quote</td>';
		$output .= '<td style="color:#fff; background-color:#0066CC; border-color: #333333;">Expected Completion Date</td>';
		$output .= '<td style="color:#fff; background-color:#0066CC; border-color: #333333;">Status</td>';
		$output .= '<td style="color:#fff; background-color:#0066CC; border-color: #333333;">Outcome</td>';
		$output .= '</tr>';
		
		while($row = @mysql_fetch_array($result)){
			
			$result_job = $contractor_report->job($row['cpm_po_id'], $_REQUEST);
	
			$num_result_jobs = @mysql_num_rows($result_job);
			if($num_result_jobs > 0){
				
				while($row_job = @mysql_fetch_array($result_job)){
					
					$status = "";
			
					if ($row_job['cpm_po_job_complete'] == "True"){
						$status = "Closed";
					}else{
						$status = "Open";
					}
					
					$output .= '<tr valign="top">';
					$output .= '<td>'.$row['cpm_po_number'].'</td>';
					$output .= '<td>'.$row['cpm_po_date_raised'].'</td>';
					$output .= '<td>'.$row['rmc_name'].'</td>';
					$output .= '<td>';
					if ($row['cpm_po_is_contract'] == '0'){
						$output .= "Reactive";
					}else{
						$output .= "Contract";
					}
					$output .= '</td>';
					$output .= '<td>'.$row['cpm_po_description'].'</td>';
					$output .= '<td>'.$row_job['cpm_po_job_no'].'</td>';
					$output .= '<td>'.$row_job['cpm_po_job_amount'].'</td>';
					$output .= '<td>'.$row_job['cpm_po_job_completion_date'].'</td>';
					$output .= '<td>'.$status.'</td>';
					
					if($row_job['cpm_po_job_reason_id'] != ""){
						$output .= '<td><strong>Incomplete - '.$row_job['the_reason'].":</strong> ".$row_job['cpm_po_job_reason'].'</td>';
					}else{
						if ($row_job['user_name'] == ""){
							$output .= '<td>&nbsp;</td>';
						}else{
							$output .= '<td>Completed by '.$row_job['user_name'].' on '.$contractor_report->is_blank($data->ymd_to_date($row_job['cpm_po_job_ts'])).'</td>';
						}
					}
					$output .= '</tr>';
					
				}		
			}
		}
		$output .= '</table>';
	}
	
	echo $output;
	
	exit;
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>RMG Living - Contractors</title>
	<link href="../styles.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="/css/admin_extra.css"/>
	<link rel="stylesheet" type="text/css" href="/css/custom-theme/jquery-ui-1.8.16.custom.css"/>
	<script type="text/javascript" language="JavaScript" src="/library/jscript/jquery-1.6.2.min.js"></script>
	<script type="text/javascript" language="JavaScript" src="/library/jscript/jquery-ui-1.8.16.custom.min.js"></script>
	<script type="text/javascript" language="JavaScript" src="/library/jscript/jquery.page.js"></script>
	<script language="JavaScript" type="text/javascript">
		$(document).ready(function(){			
			$("#processing_dialog").dialog({  
				modal: true,
				open: function(event, ui) { $(".ui-dialog-titlebar-close").hide();},
				autoOpen: false
			});	
			
			$(".date").datepicker({ dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true });
		});
		
		function show_dialog(the_width){
			var ad = $("#dialog_audit").dialog({  
				modal: true,
				width: the_width,
				open: function(event, ui) { $(".ui-dialog-titlebar-close").show();},
				buttons: {
					Ok: function() {
						$( this ).dialog( "close" );
					}
				}
			});
			
			ad.parent().appendTo('form:first');		
		}
		
		function excel_dump(){
			$('#whichaction').val("excel");
			window.open('contractor_reports.php?' + $("#form1").serialize(),'mywindow','width=400,height=200')
		}
		
		function do_search() {
			$("#processing_dialog").dialog('open');
			toggle_complete();
			$('#whichaction').val('order');
			
			$.post("contractor_reports.php", 
			$("#form1").serialize(), 
			function(data){	
				$('#results').html(data['results']);
				$("#processing_dialog").dialog('close');
			}, 
			"json");
		}
		
		function do_job(id, request) {
			$("#processing_dialog").dialog('open');
			$('#user_id').val(id);
			
			var new_request = request.replace("whichaction=order&", "");
			
			$.post("contractor_reports.php", 
			new_request.replace("amp;", "") + "&whichaction=job&po_no=" + id, 
			function(data){	
				$('#audit_results').html(data['results']);
				$('#audit_paging').html(data['paging']);
				$('#audit_results').css('width', '565px');
				$('#audit_paging').css('width', '565px');
				$("#processing_dialog").dialog('close');
				$("#dialog_audit").dialog("option", "title", "Jobs Within PO");
				show_dialog("600px");
			}, 
			"json");
		}
		
		function toggle_complete(){
			
			$('#purchase_row').hide();
			
			if( $('#is_completed').val() == 'True'){
				$('#purchase_row').show();
			}else{
				$('#date_from').val('');
				$('#date_to').val('');
			}
		}
	</script>
	<style>
		.ui-widget { font-family: Verdana,Arial,sans-serif; font-size: 0.7em; }
	</style>
</head>
<body class="management_body">
<form id="form1" method="post">
	<div id="processing_dialog" title="Processing" style="display:none;">
		<p style="text-align:center;display:none;margin-bottom:15px;" id="processing_dialog_message"></p>
		<p style="text-align:center;"><img src="/images/ajax-loader.gif" /></p>
	</div>
	<div id="dialog_audit" title="Audit Trail" style="display:none;">
		<div class="results" style="width:470px;" id="audit_results">
		</div>
		<div class="results" style="border:none; width:470px;" id="audit_paging">
		</div>
	</div>
	<? require($UTILS_FILE_PATH."management/menu.php");?>
	<table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr valign="top">
			<td>
				<table border="0" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="border: 1px #000000 solid" width="100%">
					<tr>
						<td align="center"><img src="../images/management/icons/contractors_icon_new.gif" width="90" height="50" /><img src="../images/management/icons/reports_icon_new.gif" width="70" height="50" /></td>
					</tr>
					<tr>
						<td align="center"><a href="index.php"><b>&lt;&lt; Back to main menu</b></a></td>
					</tr>
					<tr>
						<td bgcolor="#ffffff" valign="top" align="center"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr valign="top">
			<td>&nbsp;</td>
		</tr>
		<tr valign="top">
			<td>
				<table width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="border: 1px #333333 solid">
					<tr>
						<td><strong>Search for a Contractor</strong></td>
					</tr>
					<tr>
						<td>
							<div class="form_row">
								<div class="form_label">
									Contractor
								</div>
								<div class="form_item">
									<select id="contractor_id" name="contractor_id">
										<?php get_contractors(); ?>
									</select>
								</div>
							</div>
							<div class="form_row">
								<div class="form_label">
									Term
								</div>
								<div class="form_item">
									<input type="text" id="search_term" name="search_term" />
								</div>
							</div>
							<div class="form_row">
								<div class="form_label">
									Closed
								</div>
								<div class="form_item">
									<select id="is_completed" name="is_completed" onchange="toggle_complete()">
										<option value="True">Yes</option>
										<option value="False">No</option>
										<option value="">Either</option>
									</select>
								</div>
								<div class="form_label">
									Type of PO
								</div>
								<div class="form_item">
									<select id="contract" name="contract">
										<option value="2">All</option>
										<option value="1">Contract</option>
										<option value="0">Reactive</option>
									</select>
								</div>
							</div>
							<div class="form_row" id="purchase_row">
								<div class="form_label">
									Date From
								</div>
								<div class="form_item">
									<input class="date" type="text" id="date_from" name="date_from" />
								</div>
								<div class="form_label">
									Date To
								</div>
								<div class="form_item">
									<input class="date" type="text" id="date_to" name="date_to" />
								</div>
							</div>
							<div class="form_row" style="border-bottom:1px solid #999;margin-bottom:10px;">
								<div class="form_label">
									<input type="button" id="search_button" name="search_button" value="Search" onclick="do_search()" />
								</div>
								<div class="form_item">
									&nbsp;
								</div>
								<div class="form_label">
									&nbsp;
								</div>
								<div class="form_item" style="text-align:right;">
									<input type="button" id="excel_button" name="excel_button" value="Export to Excel" onclick="excel_dump()" />
								</div>
							</div>
							<div id="results" class="form_row" style="border:none; border-bottom:1px solid #999;">
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr valign="top">
			<td>&nbsp;</td>
		</tr>
	</table>
	<input type="hidden" name="whichaction" id="whichaction" />
</form>
</body>
</html>
