<?
ini_set("max_execution_time", "300");
ob_start();
require("utils.php");
require_once($UTILS_CLASS_PATH."letter.class.php");
require_once($UTILS_CLASS_PATH."resident.class.php");
require_once($UTILS_CLASS_PATH."rmc.class.php");
require_once($UTILS_CLASS_PATH."unit.class.php");
require_once($UTILS_CLASS_PATH."data.class.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."contractors.class.php");
require_once($UTILS_CLASS_PATH."security.class.php");
require_once($UTILS_CLASS_PATH."encryption.class.php");
$website = new website;
$rmc = new rmc();
$unit = new unit();
$resident = new resident();
$letter = new letter();
$security = new security;
$crypt = new encryption_class;
$data = new data();

// Determine if allowed access into content management system
$website->allow_cms_access();


//=============================================
// Update print job table
//=============================================
if($_REQUEST['print_status'] == "done"){
	
	if($_REQUEST['type'] != "6"){
		
		// Get ALL resident nums posted
		$resident_nums = explode(",", $_POST['resident_nums']);
		
		// Get title for this print job
		$sql_pj = "SELECT print_type_title FROM cpm_print_types WHERE print_type_id = ".$_REQUEST['type'];
		$result_pj = @mysql_query($sql_pj);
		$row_pj = @mysql_fetch_row($result_pj);
		
		// Set residents record to say that they have been sent password
		for($i=0;$i < count($resident_nums);$i++){
		
			$sql = "UPDATE cpm_residents_extra SET password_to_be_sent='N' WHERE resident_num = ".$resident_nums[$i];
			@mysql_query($sql);
		}
		
		// Set rmc_num
		$rmc_num = "NULL";
		if($_REQUEST['rmc_num'] != ""){
			$rmc_num = $_REQUEST['rmc_num'];
		}
		
		// Include print job in cpm_print_jobs table
		if($_REQUEST['type'] == "1" || $_REQUEST['type'] == "2" || $_REQUEST['type'] == "5"){	
			
			$sql = "
			INSERT INTO cpm_print_jobs(
				print_job_title,
				template,
				resident_nums,
				rmc_num,
				user_name,
				print_job_time
			)
			VALUES(
				'".$row_pj[0]."',
				'".$_REQUEST['template']."',
				'".$_REQUEST['resident_nums']."',
				".$_REQUEST['rmc_num'].",
				'".addslashes($_SESSION['user_name'])."',
				'$thistime'
			)
			";
			@mysql_query($sql);
			
		}
		else if($_REQUEST['type'] == "3"){
			
			$_REQUEST['print_job_title'] .= " (Multiple RMC'S)";
			$rmc_nums = explode(",", $_POST['rmc_nums']);
			for($i=0;$i < count($rmc_nums);$i++){
			
				$sql = "
				INSERT INTO cpm_print_jobs(
					print_job_title,
					template,
					resident_nums,
					rmc_num,
					user_name,
					print_job_time
				)
				VALUES(
					'".$row_pj[0]."',
					'".$_REQUEST['template']."',
					'".$_REQUEST['resident_nums']."',
					".$rmc_nums[$i].",
					'".addslashes($_SESSION['user_name'])."',
					'$thistime'
				)
				";
				@mysql_query($sql);
			
			}
		}
	}else{
		$resident_nums = explode(",", $_POST['resident_nums']);
		$resident_passwords = explode(",", $_POST['resident_passwords']);
		
		for($i=0;$i < count($resident_nums);$i++){
			
			$contractors = new contractors($resident_nums[$i]);
			
			if(strtoupper($contractors->contractor_pcm) != 'POST'){
				$password = $resident->get_rand_id(8, 'ALPHA', 'UPPER');
				
				$letter->set_contractor($contractors);
				
				$letter->set_body("letter_cpm_living_welcome_contractors.php");
				
				// Make letter reference
				$your_ref = stripslashes($resident_nums[$i]);
			
				//Use the appropriate letter format
				ob_start();
				require($letter->get_body());
				$letter_content = ob_get_contents();
				ob_end_clean();
				
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				$headers .= 'From: customerservice@rmguk.com' . "\r\n";
				$headers .= 'Reply-To: customerservice@rmguk.com' . "\r\n";
				$headers .= 'X-Mailer: PHP/' . phpversion();
				
				$sql = "INSERT INTO cpm_mailer SET
				mail_to = '".$contractors->contractor_email."',
				mail_from = 'customerservice@rmguk.com',
				mail_subject = 'Welcome to RMG Living',
				mail_headers = '".$headers."',
				mail_message = '".$letter_content."'";
				
				@mysql_query($sql);
			}
			
			$sql = "UPDATE cpm_contractors SET 
			cpm_contractors_letter_sent = 'Y' 
			WHERE cpm_contractors_qube_id = '".$resident_nums[$i]."'";
			@mysql_query($sql);
			
			$the_password = $resident_passwords[$i];
			
			if(strtoupper($resident_pcm[$i]) != 'POST'){
				$the_password = $password;
			}
			
			$sql = "SELECT *
			FROM cpm_contractors_user
			WHERE cpm_contractors_user_ref = '".$_POST['contractor_id']."'";
			$result = @mysql_query($sql);
			$num_rows = @mysql_num_rows($result);
		
			if($num_rows == 0){
				$sql = "INSERT INTO cpm_contractors_user SET 
				cpm_contractors_user_ref = '".$resident_nums[$i]."', 
				cpm_contractors_user_qube_ref = '".$resident_nums[$i]."', 
				cpm_contractors_user_disabled = 'False',
				cpm_contractors_user_parent = '0', ";
			}else{
				$sql = "UPDATE cpm_contractors_user SET ";
			}
			
			$sql .= "cpm_contractors_user_password = '".$security->clean_query($crypt->encrypt($UTILS_DB_ENCODE, $the_password))."'";
			
			if($num_rows > 0){
				$sql .= "WHERE cpm_contractors_user_ref = '".$_POST['contractor_id']."'";
			}
			
			@mysql_query($sql);
			
			$date = new DateTime();
			
			$sql = "
			INSERT INTO cpm_print_contractors SET
			contractor_id = '".$resident_nums[$i]."',
			user_name = '".$_SESSION['user_id']."',
			print_job_time = '".$date->format('Y-m-d H:i:s')."'";
			@mysql_query($sql);
			
		}
	}
}

$thistime = time();

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../styles.css" rel="stylesheet" type="text/css">
<link href="../css/letter_print.css" rel="stylesheet" type="text/css" media="print">
<style type="text/css">
<!--
.style4 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
}
.style11 {
	font-size: 10px;
	font-weight: bold;
	font-family: Verdana, Arial, Helvetica, sans-serif;
}
.style12 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #036;
}

-->
</STYLE>

<script language="javascript">
function print_slip(){

	document.getElementById('header_table').style.display='none';
	document.getElementById('done_table').style.display='none';
	document.getElementById('print_table').style.display='none';
	document.getElementById('warning_table').style.display='none';
	document.getElementById('spacer_table').style.display='none';
	window.print();
	document.getElementById('header_table').style.display='block';
	document.getElementById('done_table').style.display='block';
	document.getElementById('print_table').style.display='block';
	document.getElementById('warning_table').style.display='block';
	document.getElementById('spacer_table').style.display='block';
}

function confirm_print(){
	<? if($_REQUEST['type'] != "5" && $_REQUEST['type'] != "6"){?>
	opener.hide_letter_box('<?=$_REQUEST['template']?>', '<?=date("d/m/Y", $thistime)?>');
	<? }?>
	document.forms[0].action = "password_slip.php";
	document.forms[0].target = "_self";
	document.forms[0].submit();
}
</script>
</head>
<body>
<? if($_REQUEST['print_status'] == "done"){?>
<table width="100%" height="100%" border="0" align="center" cellpadding="0" cellspacing="10" bgcolor="#C5D9B3" id="close_table" style="border:1px solid #666666;">
<tr onMouseOver="this.style.cursor='hand'">
<td align="center" onClick="window.close();"><p class="style11"><? if($_REQUEST['type'] == "resident"){?>The record for this resident has<? }else{?>All resident records have<? }?> been updated. Click <a href="#" onClick="window.close();">HERE</a> to close this window.</p>
  </td>
</tr>
</table>
<? exit;}?>

<?
//==========================================================
// Collect dynamic data to populate password slip
//==========================================================

// Get single resident print job
if($_REQUEST['type'] == "1"){

	$template_clause = " rex.password_to_be_sent='Y' AND ";

	if($_REQUEST['is_print_history'] == "Y"){
		// Build sql query
		$sql_resident = "
		SELECT * 
		FROM cpm_residents re, cpm_residents_extra rex, cpm_rmcs rmc, cpm_rmcs_extra rmcx   
		WHERE 
		rmc.rmc_num = rmcx.rmc_num AND 
		re.rmc_num=rmc.rmc_num AND 
		re.resident_num=rex.resident_num AND 
		rmcx.prevent_printing_of_letters <> 'Y' AND 
		(re.resident_status = 'Current' OR re.resident_is_active='1') AND 
		re.resident_num = ".$_REQUEST['resident_num'];
		
	}
	else{
		// Build sql query
		$sql_resident = "
		SELECT * 
		FROM cpm_residents re, cpm_residents_extra rex, cpm_rmcs rmc, cpm_rmcs_extra rmcx  
		WHERE 
		$template_clause 
		rmc.rmc_num = rmcx.rmc_num AND 
		re.rmc_num=rmc.rmc_num AND 
		(rmc.rmc_status = 'Active' OR rmc.rmc_is_active = '1') AND 
		rex.is_linked_to_master_account <> 'Y' AND 
		rmcx.prevent_printing_of_letters <> 'Y' AND 
		re.resident_num=rex.resident_num AND 
		(re.resident_status = 'Current' OR re.resident_is_active='1') AND 
		re.resident_num = ".$_REQUEST['resident_num'];
		//print $sql_resident;
	}
}

// Get single RMC print job
elseif($_REQUEST['type'] == "2" || $_REQUEST['type'] == "5"){

	if($_REQUEST['is_print_history'] == "Y"){
		
		// Get all resident nums
		$sql_pj = "SELECT resident_nums FROM cpm_print_jobs WHERE print_job_id = ".$_REQUEST['print_job_id'];
		$result_pj = @mysql_query($sql_pj);
		$row_pj = @mysql_fetch_row($result_pj);
		
		// Build 'where' clause
		$residents = explode(",", $row_pj[0]);
		$i=0;
		$whereclause = "(";
		for($i=0;$i<(count($residents)-1);$i++){
			$whereclause .= " re.resident_num = ".$residents[$i]." OR ";
		}
		$whereclause .= " re.resident_num = ".$residents[$i].") ";
		
		// Build sql query
		$sql_resident = "
		SELECT * 
		FROM cpm_residents re, cpm_residents_extra rex, cpm_lookup_residents lre, cpm_rmcs_extra rmcx    
		WHERE 
		re.rmc_num = rmcx.rmc_num AND 
		re.resident_num = lre.resident_lookup AND 
		re.resident_num = rex.resident_num AND 
		lre.resident_ref NOT LIKE 't0%' AND 
		rmcx.prevent_printing_of_letters <> 'Y' AND 
		rex.is_linked_to_master_account <> 'Y' AND 
		(re.resident_status = 'Current' OR re.resident_is_active='1') AND 
		rex.hide_letter='N' AND $whereclause";
		
	}
	else{
		
		$template_clause = " rex.password_to_be_sent='Y' AND ";
		
		if(!isset($_REQUEST['all']) || $_REQUEST['all'] == ""){
			$all = "AND re.rmc_num = ".$_REQUEST['rmc_num'];
		}
		else{
			$all = "ORDER BY re.rmc_num ASC";
		}
		
		if($_REQUEST['letter_filter'] == "1"){
			$letter_filter = " re.is_subtenant_account = 'Y' AND ";
		}
		elseif($_REQUEST['letter_filter'] == "2"){
			$letter_filter = " re.is_resident_director = 'Y' AND ";
		}
		elseif($_REQUEST['letter_filter'] == "3"){
			$letter_filter = " re.is_subtenant_account = 'N' AND re.is_resident_director = 'N' AND ";
		}
		
		$sql_resident = "
		SELECT * 
		FROM cpm_residents re, cpm_residents_extra rex, cpm_rmcs rmc, cpm_lookup_residents lre, cpm_rmcs_extra rmcx  
		WHERE 
		$template_clause 
		$letter_filter 
		rmc.rmc_num = rmcx.rmc_num AND 
		re.rmc_num=rmc.rmc_num AND 
		re.resident_num=lre.resident_lookup AND 
		(rmc.rmc_status = 'Active' OR rmc.rmc_is_active = '1') AND 
		re.resident_num=rex.resident_num AND 
		rex.is_linked_to_master_account <> 'Y' AND 
		lre.resident_ref NOT LIKE 't0%' AND 
		re.resident_name <> 'Developer' AND 
		rmcx.prevent_printing_of_letters <> 'Y' AND 
		(re.resident_status = 'Current' OR re.resident_is_active='1') $all LIMIT 100 
		";
		
	}
}
elseif($_REQUEST['type'] == "6"){
	
	if($_REQUEST['contractor_id'] != ""){	
		$sql_resident = "
		SELECT * 
		FROM cpm_contractors
		WHERE 
		cpm_contractors_qube_id = '".$_REQUEST['contractor_id']."'
		LIMIT 1 ";		
	}else{
		$sql_resident = "
		SELECT * 
		FROM cpm_contractors
		WHERE 
		cpm_contractors_letter_sent = 'N'
		LIMIT 100 ";	
	}
}

$result_resident = @mysql_query($sql_resident);
$num_slips = @mysql_num_rows($result_resident);
?>
<form action="password_slip.php" method="post">
<div id="header_table" class="style12" style="display:none;height:20px;vertical-align:top;border:1px solid #666666;padding:10px;">
	<div style="width:500px;float:left;">There are&nbsp;<span id="num_to_print_off"></span>&nbsp;password slips to print off</div>
	<? if($_REQUEST['type'] == "2" || $_REQUEST['type'] == "5"){?>
	<div id="letter_filter_div" style="padding:0; margin:0; float:right; width:350px; text-align:right; ">
		Letter type filter:&nbsp;<select name="letter_filter" id="letter_filter" onChange="document.getElementById('print_status').value = '';document.forms[0].submit();">
			<option value="" selected>Show ALL letters</option>
			<option value="3" <? if($_REQUEST['letter_filter'] == "3"){?>selected<? }?>>Resident letters ONLY</option>
			<option value="1" <? if($_REQUEST['letter_filter'] == "1"){?>selected<? }?>>Sub-tenant letters ONLY</option>
			<option value="2" <? if($_REQUEST['letter_filter'] == "2"){?>selected<? }?>>Director letters ONLY</option>
		</select>
	</div>
	<? }?>
</div>

<table id="print_table" width="100%" border="0" cellpadding="0" cellspacing="10" bgcolor="#CFD8EB" style="display:none;border:1px solid #666666;">
  <tr onMouseOver="this.style.cursor='hand'">
    <td onClick="print_slip();" style="text-align:left;"><span class="style11">STEP 1: </span><span class="style4">Click here to print this batch of password slips</span></td>
  </tr>
</table>
<table id="warning_table" width="100%" border="0" cellpadding="0" cellspacing="10" bgcolor="#CFD8EB" style="display:none;border:1px solid #666666;">
  <tr>
    <td style="text-align:left;"><span class="style11">STEP 2: </span><span class="style4">Do</span><span class="style11"> NOT</span><span class="style4"> close this window using the close button! You will be given a specific close screen after completeing step 3.</span></td>
  </tr>
</table>
<table id="done_table" width="100%" border="0" cellpadding="0" cellspacing="10" bgcolor="#CFD8EB" style="display:none;border:1px solid #666666;">
  <tr onMouseOver="this.style.cursor='hand'">
    <td onClick="confirm_print();" style="text-align:left;"><span class="style11">STEP 3: </span><span class="style4">Click here to confirm that <strong>ALL</strong> the password slips have printed successfully </span></td>
  </tr>
</table>

<table id="spacer_table" width="100%" border="0" align="center" cellpadding="0" cellspacing="5">
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
<?
//=================================================
// Start printing the letters to screen
//=================================================
$this_slip_num = 1;
$email_slips = 1;
$result_resident = mysql_query($sql_resident);
while($row_resident = mysql_fetch_array($result_resident)){
	
	$loop_ts = microtime(true);
	
	if ($this_slip_num <= 1) {
		print "<p>&nbsp;</p>";
	}
	
	if($_REQUEST['type'] != "6"){
		
		if($_REQUEST['type'] == "1" || $_REQUEST['type'] == "2" || $_REQUEST['type'] == "5"){
			$unit->set_unit($row_resident['resident_num']);
			if($unit->unit_ref == "9999"){continue;}
		}
	
		$all_resident_nums .= $row_resident['resident_num'].",";
	
		//print "<p>".(microtime(true)-$loop_ts)." sec</p>";
		
		// Set various objects to use in the letter
		$resident->resident($row_resident['resident_num']);
		$letter->set_recipient($resident);	
		$rmc->set_rmc($row_resident['rmc_num']);
		
		// Set unit label
		$unit_label = "";
		if($resident->is_subtenant_account == "Y"){
			$unit_label = stripslashes($resident->resident_address_1);
			if($resident->resident_address_2 != ""){
				$unit_label .= ", ".stripslashes($resident->resident_address_2);
			}
		}
		else{
			if($unit->unit['unit_description'] != ""){
				$unit_label = stripslashes($unit->unit['unit_description']);
			}else{
				$unit_label = stripslashes($unit->unit['unit_address_1']);
			}
		}
		
		
		if($resident->is_resident_director == "Y"){
			$letter->set_body("letter_cpm_living_welcome_body_3.php");
		}
		elseif($resident->is_subtenant_account == "Y"){		
			$letter->set_body("letter_cpm_living_welcome_body_4.php");
		}
		else{
			$letter->set_body("letter_cpm_living_welcome_body_1.php");
		}
		
		// Make letter reference
		$your_ref = stripslashes($rmc->rmc_ref."-".$resident->resident_ref);
	
		//Use the appropriate letter format
		include($UTILS_TEMPLATE_PATH."letter_A5_windowed.php");
	
	
		if($_REQUEST['is_print_history'] == "Y" && $resident->is_first_login == "N"){
			// Do not print include this as this affects the print-out (password has been sent and user has already logged on)
		}
		else{
			if($this_slip_num < $num_slips){
				print "<p class='break'>&nbsp;</p>";
			}
		
			$this_slip_num++;
		}
	}else{
			
		$password = $resident->get_rand_id(8, 'ALPHA', 'UPPER');
		
		$all_resident_nums .= $row_resident['cpm_contractors_qube_id'].",";
		
		$all_resident_passwords .= $passwords.",";
		
		$contractors = new contractors($row_resident['cpm_contractors_qube_id']);
		
		$letter->set_contractor($contractors);
		
		$letter->set_body("letter_cpm_living_welcome_contractors.php");
		
		// Make letter reference
		$your_ref = stripslashes($row_resident['cpm_contractors_qube_id']);
	
		//Use the appropriate letter format
		if(strtoupper($row_resident['cpm_contractors_pcm']) == 'POST'){
			ob_start();
			require($UTILS_TEMPLATE_PATH."letter_A5_windowed.php");
			$letter_content = ob_get_contents();
			ob_end_clean();

			echo $letter_content;
			if($this_slip_num < $num_slips){
				print "<p class='break'>&nbsp;</p>";
			}
		
			$this_slip_num++;
		}else{
			$email_slips++;	
		}
	}
}
if($_REQUEST['type'] != "6"){
	$final_slip = "<strong>" . $this_slip_num-1 . "</strong>";
}else{
	$final_slip = "<strong>" . ($email_slips-1) . "</strong> password slips to be emailed and <strong>" . ($this_slip_num-1) . "</strong>";
}

if($all_resident_nums != ''){
	$all_resident_nums = substr($all_resident_nums,0,-1);
}
if($all_resident_passwords != ''){
	$all_resident_passwords = substr($all_resident_passwords,0,-1);
}

?>
<input type="hidden" name="print_status" id="print_status" value="done">
<input type="hidden" name="resident_nums" id="resident_nums" value="<?=$all_resident_nums?>">
<input type="hidden" name="resident_passwords" id="resident_passwords" value="<?=$all_resident_passwords?>">
<input type="hidden" name="rmc_num" id="rmc_num" value="<?=$_REQUEST['rmc_num']?>">
<input type="hidden" name="rmc_nums" id="rmc_nums" value="<?=$_REQUEST['rmc_nums']?>">
<input type="hidden" name="contractor_id" id="contractor_id" value="<?=$_REQUEST['contractor_id']?>">
<input type="hidden" name="type" id="type" value="<?=$_REQUEST['type']?>">
<input type="hidden" name="template" id="template" value="<?=$_REQUEST['template']?>">
<input type="hidden" name="whichaction" id="whichaction">
<input type="hidden" name="is_print_history" id="is_print_history" value="<?=$_REQUEST['is_print_history']?>">
</form>

<script language="javascript">
document.getElementById('header_table').style.display = "block";
<? if($num_slips > 0){?>
document.getElementById('print_table').style.display = "block";
document.getElementById('warning_table').style.display = "block";
document.getElementById('done_table').style.display = "block";
<? }?>
document.getElementById('num_to_print_off').innerHTML = '<?=$final_slip?>';
</script>
</body>

</html>
<? ob_flush(); ?>
