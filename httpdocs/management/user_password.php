<?
require("utils.php");
require($UTILS_FILE_PATH."management/gen_password.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."encryption.class.php");
$crypt = new encryption_class;
$website = new website;

// Determine if allowed access into content management system
$website->allow_cms_access();

#=========================================
# Generate new password
#=========================================
if($_REQUEST['gen'] == "Y"){

	$password_unique = "N";
	while($password_unique == "N"){
		$password = get_rand_id(8, 'ALPHA', 'UPPER');
		$sql = "SELECT password FROM cpm_backend WHERE password = '".$crypt->encrypt($UTILS_DB_ENCODE, $password)."'";
		$result = mysql_query($sql);
		$password_exists = mysql_num_rows($result);
		if($password_exists < 1){
			$password_unique = "Y";
		}
	}
	
	$pass_success = "Y";
	
	# Update residents record with new password
	$sql2 = "
	UPDATE cpm_backend SET
	is_first_logon = 'Y',
	password = '".$crypt->encrypt($UTILS_DB_ENCODE, $password)."'
	WHERE user_id=".$_REQUEST['user_id'];
	@mysql_query($sql2) or $pass_success = "N";

	if($pass_success == "Y"){
		
		$sql3 = "SELECT email, user_name FROM cpm_backend WHERE user_id=".$_REQUEST['user_id'];
		$result3 = @mysql_query($sql3);
		$row3 = @mysql_fetch_row($result3);
		$message = "
Your RMG Living Login is:
		
Username: ".$row3[1]."
Password: ".$password."
		
This email was automatically generated from the RMG Living Admin system.
		";
		
		
		mail($row3[0], "RMG Living Login", $message);
		
		print "
		<script language=javascript>
		alert(\"A new password has been generated for this user and has been sent to them via email.\");
		</script>
		";
	}
	else{
		print "
		<script language=javascript>
		alert(\"There was a problem setting a new password for this user.\");
		</script>
		";
	}

}
?>