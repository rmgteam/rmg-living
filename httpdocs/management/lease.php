<?
require("utils.php");
require_once($UTILS_FILE_PATH."library/functions/check_file_extension.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."rmc.class.php");
$rmc = new rmc;
$rmc->set_rmc($_REQUEST['rmc_num']);
$website = new website;

// Determine if allowed access into content management system
$website->allow_cms_access();

// Check access privilege
if($_SESSION['allow_rmc'] != 1){header("Location:index.php");}

#=========================================
# Delete lease file
#=========================================
if($_REQUEST['whichaction'] == "delete_lease"){

	$del_error = "N";

	$sql = "SELECT * FROM cpm_leases WHERE lease_id=".$_REQUEST['lease_file_id'];
	$result = @mysql_query($sql) or $del_error = "Y";
	$row = @mysql_fetch_array($result);
	$file_name = $row['lease_file'];

	$sql = "DELETE FROM cpm_leases WHERE lease_id=".$_REQUEST['lease_file_id'];
	@mysql_query($sql) or $del_error = "Y";
	
	if($del_error == "N"){
		@unlink($UTILS_PRIVATE_FILE_PATH."leases/$file_name");
	}
	
}


#=========================================
# Add lease file
#=========================================
if($_REQUEST['which_action'] == "add_lease"){
	
	$file_stamp = time();
	
	# Save insurance cert file to correct location
	$file_ext[] = "pdf";
	if(check_file_extension($_FILES['lease_file']['name'], $file_ext)){
		$lease_file = "lease_".$rmc->rmc['rmc_ref']."_".$file_stamp.".pdf";
		$lease_file_path = $UTILS_PRIVATE_FILE_PATH."leases/".$lease_file;
		copy($_FILES['lease_file']['tmp_name'], $lease_file_path);

		# Insert insurance cert details
		$sql = "
		INSERT INTO cpm_leases(
			rmc_num,
			lease_type,
			lease_file,
			lease_file_stamp
		)
		VALUES(
			".$_REQUEST['rmc_num'].",
			'".$_REQUEST['lease_type']."',
			'".$lease_file."',
			'".$file_stamp."'
		)
		";
		mysql_query($sql);
	}
}
?>
<html>
<head>
<link href="../styles.css" rel="stylesheet" type="text/css">
</head>
<body>
<form action="lease.php">
<table border="0" cellpadding="3" cellspacing="0" width="100%">
<?
#===============================================
# Collect all leases for this RMC
#===============================================
$sql = "SELECT * FROM cpm_leases WHERE rmc_num = ".$_REQUEST['rmc_num'];
$result = @mysql_query($sql);
$num_agm = @mysql_num_rows($result);
if($num_agm < 1){
	print "<tr><td>There are no Leases for this RMC</td></tr>";
}
else{
	while($row = @mysql_fetch_array($result)){
		?>
		<tr bgcolor="#f3f3f3">
			<td style="border-bottom:1px solid #cccccc"><?=$row['lease_type']?></td>
		  <td align="right" style="border-bottom:1px solid #cccccc"><input onClick="if(confirm('Remove Lease for this RMC ?')){document.forms[0].lease_file_id.value='<?=$row['lease_id']?>';document.forms[0].submit();}" type="button" name="Button" value="remove"></td>
		</tr>
		<?
	}
}
?>
</table>
<input type="hidden" id="lease_file_id" name="lease_file_id">
<input type="hidden" id="rmc_num" name="rmc_num" value="<?=$_REQUEST['rmc_num']?>">
<input type="hidden" name="whichaction" value="delete_lease">
</form>
</body>
</html>