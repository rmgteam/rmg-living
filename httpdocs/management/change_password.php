<?
require("utils.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."encryption.class.php");
$website = new website;
$crypt = new encryption_class;

// Determine if allowed access into content management system
$website->allow_cms_access();


#===================================
# Save new password
#===================================

if($_REQUEST['which_action']=="save"){
	
	#========================
	# Check password
	#========================
	$sql = "SELECT password FROM cpm_backend WHERE user_id=".$_SESSION['user_id']." AND password = '".$crypt->encrypt($UTILS_DB_ENCODE, trim($_REQUEST['old_password']))."'";
	$result = @mysql_query($sql);
	$password_exists = @mysql_num_rows($result);

	if($password_exists > 0){
	
		// Update user record
		$pass_error = "N";
		$sql = "UPDATE cpm_backend SET is_first_logon='N', password = '".$crypt->encrypt($UTILS_DB_ENCODE, trim($_REQUEST['new_password']))."' WHERE user_id = ".$_SESSION['user_id'];
		@mysql_query($sql) or $pass_error = "Y";
		
		$_SESSION['is_first_logon'] = "N";
	
	}
	
}

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>RMG Living - Change Password</title>
<link href="../styles.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function save(){

	if(document.forms[0].old_password.value == ""){alert("Enter your old password in the field provided.");return;}
	if(document.forms[0].new_password.value == ""){alert("Enter your new password in the field provided.");return;}
	if(document.forms[0].new_password2.value == ""){alert("Confirm your new password in the field provided.");return;}
	if(document.forms[0].new_password.value.length < 8){alert("Your password needs to be at least 8 characters.");return;}
	if(document.forms[0].new_password.value != document.forms[0].new_password2.value){alert("The new passwords you have entered do not match, please re-type them and try again.");return;}
	document.forms[0].submit();
}

//-->
</script>
</head>
<body class="management_body">
<form method="post" action="change_password.php">
<? require($UTILS_FILE_PATH."management/menu.php");?>

<table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr valign="top">
	  <td><table width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="border: 1px #000000 solid">
        <tr>
          <td align="center"><img src="../images/management/icons/my_password_icon_new.gif" width="90" height="50"></td>
        </tr>
        <tr>
          <td align="center"><? if($_SESSION['is_first_logon'] == "N"){?><a href="index.php"><b><< Back to main menu</b></a><? }?></td>
        </tr>
      </table></td>
    </tr>
	<tr valign="top">
	  <td>&nbsp;</td>
    </tr>
	<tr valign="top">
	  <td><table width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#eaeaea" style="border: 1px #999999 solid">
        <tr>
          <td><strong>Change My Password </strong></td>
        </tr>
      </table></td>
    </tr>
	<tr valign="top">
		<td>
			<table border="0" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="border: 1px #999999 solid">
				<tr>
					<td>
						<table border="0" cellpadding="0" cellspacing="0">
							<tr valign="top">
								<td>
									<table border="0" cellpadding="3" cellspacing="0">
									<? if($_REQUEST['which_action']=="save"){?>
									<? if($password_exists > 0 && $pass_error == "N"){?>
									<tr>
										<td colspan="2"><font color="#006633"><strong>Your password has been changed</strong></font></td>
									</tr>
									
									<? }else{?>
									<tr>
										<td colspan="2"><font color="#CC0000"><strong>Your password could not been changed. If you continue to experience problems, please contact <a href="mailto:customerservice@rmguk.com">customerservice@rmguk.com</a>.</strong></font></td>
									</tr>
									
									<? }?>
									<tr>
										<td colspan="2">&nbsp;</td>
									</tr>
									<? }?>
										<tr>
										  <td colspan="2">Passwords needs to be at least 8 characters long. </td>
									  </tr>
										<tr>
										  <td width="133">&nbsp;</td>
										  <td width="333">&nbsp;</td>
									  </tr>
										<tr>
										  <td>Current password: </td>
										  <td><input name="old_password" type="password" id="old_password"></td>
									  </tr>
										<tr>
										  <td>New password: </td>
										  <td><input name="new_password" type="password" id="new_password"></td>
									  </tr>
										<tr>
										  <td>Confirm new password: </td>
										  <td><input name="new_password2" type="password" id="new_password2"></td>
									  </tr>
										<tr>
										  <td>&nbsp;</td>
										  <td>&nbsp;</td>
									  </tr>
										<tr>
											<td colspan="2">
												<input type="button" value=" Save " onClick="save()">
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="which_action" value="save">
</form>
</body>
</html>
