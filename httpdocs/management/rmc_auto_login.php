<?
require("utils.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."encryption.class.php");
require_once($UTILS_CLASS_PATH."security.class.php");
$crypt = new encryption_class;
$security = new security;
$website = new website;


// Determine if allowed access into content management system
$website->allow_cms_access();

// Check access privilege
if($_SESSION['allow_rmc'] != 1){header("Location:index.php");}

// Logs PM team member into RMG Living
if($_REQUEST['type'] == "dir"){
	$sql = "
	SELECT * 
	FROM cpm_lookup_residents lres, cpm_residents_extra rex, cpm_residents re 
	WHERE 
	lres.resident_lookup=re.resident_num  AND 
	rex.resident_num=re.resident_num AND 
	re.is_resident_director = 'Y' AND 
	re.is_subtenant_account <> 'Y' AND 
	lres.resident_ref LIKE 't0%' AND 
	re.resident_is_developer = 'N' AND 
	rex.rmc_num=".$security->clean_query($_REQUEST['rmc_num']);
}
elseif($_REQUEST['type'] == "lessee"){
	$sql = "
	SELECT * 
	FROM cpm_lookup_residents lres, cpm_residents_extra rex, cpm_residents re 
	WHERE 
	lres.resident_lookup=re.resident_num AND 
	rex.resident_num=re.resident_num AND 
	re.is_resident_director <> 'Y' AND 
	re.is_subtenant_account <> 'Y' AND 
	lres.resident_ref LIKE 't0%' AND 
	re.resident_is_developer = 'N' AND 
	rex.rmc_num=".$security->clean_query($_REQUEST['rmc_num']);
}
elseif($_REQUEST['type'] == "subtenant"){
	$sql = "
	SELECT * 
	FROM cpm_lookup_residents lres, cpm_residents_extra rex, cpm_residents re 
	WHERE 
	lres.resident_lookup=re.resident_num AND 
	rex.resident_num=re.resident_num AND 
	re.is_resident_director <> 'Y' AND 
	rex.is_subtenant_account = 'Y' AND 
	lres.resident_ref LIKE 't0%' AND 
	re.resident_is_developer = 'N' AND 
	rex.rmc_num=".$security->clean_query($_REQUEST['rmc_num']);
}

$result = @mysql_query($sql);
$row = @mysql_fetch_array($result);

print "
<html>
<body>
<form name=\"auto_form\" id=\"auto_form\" action=\"".$UTILS_HTTPS_ADDRESS."index.php\" method=\"post\">
<input type='hidden' name='username' value='".$row['resident_ref']."'>
<input type='hidden' name='password' value='".$crypt->decrypt($UTILS_DB_ENCODE, $row['password'])."'>
<input type='hidden' name='whichaction' value='login'>
</form>
<script type=\"text/javascript\">
document.auto_form.submit();
</script>
</body>
</html>
";
?>