<?
require("utils.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."encryption.class.php");
require_once($UTILS_CLASS_PATH."security.class.php");
require_once($UTILS_CLASS_PATH."login.class.php");
$crypt = new encryption_class;
$security = new security;
$website = new website;
$login = new login;

// Use the logout process to remove unecesarry sessions
$login->do_lessee_logout();

// Determine if allowed access into content management system
$website->allow_cms_access();

// Check access privilege
//if($_SESSION['allow_residents'] != 1){header("Location:index.php");exit;}

// Logs lessee member into RMG Living
if($_REQUEST['s'] != ""){
	
	if($_REQUEST['ma'] == "y"){
		
		$sql = "
		SELECT * 
		FROM cpm_master_accounts 
		WHERE 
		master_account_serial = '".$security->clean_query($_REQUEST['s'])."'";
		$result = @mysql_query($sql);
		$row = @mysql_fetch_array($result);
		
		$username = $row['master_account_username'];
		$password = $crypt->decrypt($UTILS_DB_ENCODE, $row['master_account_password']);
		
		$result_array = $login->do_master_account_login($username, $password);
		if( $result_array[0] === true ){
	
			header("Location: ".$UTILS_HTTPS_ADDRESS."building_details.php");
			exit;
		}
	}
	else{
		
		$sql = "
		SELECT * 
		FROM cpm_lookup_residents lres, cpm_residents_extra rex, cpm_residents re 
		WHERE 
		lres.resident_lookup=re.resident_num AND 
		rex.resident_num=re.resident_num AND 
		re.resident_serial = '".$security->clean_query($_REQUEST['s'])."'";
		//print $sql;
		$result = @mysql_query($sql);
		$row = @mysql_fetch_array($result);
		
		// If not a test account request, check that user has access to Lessees area...
		if($row['related_to_test_account'] != "Y" && $_SESSION['allow_residents'] != 1){
			print "Access denied by RMG Living privileges.";
			exit;
		}
		
		$resident_username = $row['resident_ref'];
		$resident_password = $crypt->decrypt($UTILS_DB_ENCODE, $row['password']);
		
		$result_array = $login->do_lessee_login($resident_username, $resident_password);
		if( $result_array[0] === true ){
	
			header("Location: ".$UTILS_HTTPS_ADDRESS."building_details.php");
			exit;
		}
	}
}
?>