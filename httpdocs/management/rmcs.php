<?
require("utils.php");
require($UTILS_FILE_PATH."management/gen_password.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."rmc.class.php");
require_once($UTILS_CLASS_PATH."subsidiary.class.php");
require_once($UTILS_CLASS_PATH."security.class.php");
$website = new website;
$rmc = new rmc;
$security = new security;

// Determine if allowed access into content management system
$website->allow_cms_access();

// Set RMC
$rmc->set_rmc($_REQUEST['rmc_num']);
$subsidiary = new subsidiary($rmc->rmc['subsidiary_id']);

// Check access privilege
if($_SESSION['allow_rmc'] != 1){header("Location:index.php");}


#===================================
# resize image
#===================================
function image_createThumb($src,$dest,$maxWidth,$maxHeight,$quality=100){ 

	if(isset($dest)) {
	
		$destInfo  = pathinfo($dest); 
		$srcSize  = getimagesize($src); 
		$srcRatio  = $srcSize[0]/$srcSize[1];
		if($srcSize[0]<$maxWidth){$maxWidth=$srcSize[0];}
		if($srcSize[1]<$maxHeight){$maxHeight=$srcSize[1];}
		$destRatio = $maxWidth/$maxHeight; 
		if ($destRatio > $srcRatio) { 
			$destSize[1] = $maxHeight; 
			$destSize[0] = $maxHeight*$srcRatio; 
		}
		else { 
			$destSize[0] = $maxWidth; 
			$destSize[1] = $maxWidth/$srcRatio; 
		} 
		if ($destInfo['extension'] == "gif") { 
			$dest = substr_replace($dest, 'jpg', -3); 
		} 
		$destImage = imagecreatetruecolor($destSize[0],$destSize[1]); 
		imageantialias($destImage,true); 
	
		switch ($srcSize[2]) { 
			case 1:
			$srcImage = imagecreatefromgif($src); 
			break; 

			case 2:
			$srcImage = imagecreatefromjpeg($src); 
			break; 
		
			case 3:
			$srcImage = imagecreatefrompng($src); 
			break; 
		
			default: 
			return false; 
			break; 
		} 
		imagecopyresampled($destImage, $srcImage, 0, 0, 0, 0,$destSize[0],$destSize[1],$srcSize[0],$srcSize[1]); 
		switch ($srcSize[2]) { 
			case 1: 
			case 2: 
			imagejpeg($destImage,$dest,$quality); 
			break; 
			
			case 3: 
			imagepng($destImage,$dest); 
			break; 
		} 
		return true;
	} 
	else {return false;}
	return false;
}


#===================================
# Save exisiting rmc detail
#===================================
if($_REQUEST['which_action'] == "modify"){

	$file_stamp = time();
	
	$developer = $_REQUEST['developer_id'];
	
	if($developer == '-') {
		$developer = 'NULL';
	}
	
	$sql = "
	UPDATE cpm_rmcs_extra SET
	developer_id = ".$developer.",
	dev_description = '".addslashes($_REQUEST['dev_description'])."'";
	
	if($_SESSION['allow_letters_toggle'] == "1"){
		",
		prevent_printing_of_letters = '".$_REQUEST['prevent_printing_of_letters']."'";
	}
	
	$sql .= "
	WHERE rmc_num = '".$_REQUEST['rmc_num']."'";
	mysql_query($sql);
	//print($sql);
	// Save property image
	if ($_FILES['image_file'] && $_FILES['image_file']['tmp_name'] != ""){
		$file_path = "../building_images/".$rmc->rmc['rmc_num'].".jpg";
		$temp_name = $_FILES['image_file']['tmp_name'];
		image_createThumb($temp_name,$file_path,160,200);
	}

}

//===================================
// Get Property Manager details
//===================================
if($_REQUEST['rmc_num']<>""){
	
	if(is_file("../building_images/".$rmc->rmc['rmc_num'].".jpg")){
		$image = "<img src='../building_images/".$rmc->rmc['rmc_num'].".jpg"."' style='border: 1px #000000 solid'>";
	}
}

if(!$image){$image = "<img src='../images/management/rmcs/no_image.gif' style='border: 1px #000000 solid'>";}


$rmc->set_rmc($_REQUEST['rmc_num']);
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>RMG Living - Resident Management Companies</title>
<link href="../styles.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../library/jscript/functions/hide_letter_box.js"></script>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function create_new_announce(){
	
	document.forms[0].announce_title.value = "";
	document.forms[0].announce_text.value = "";
	document.forms[0].announce_id.value=-1;
	
}

function save_announce(){
	var bad=0;
	if(document.forms[0].announce_title.value==""){bad=1;}
	if(document.forms[0].announce_text.value==""){bad=1;}
	
	if(bad==1){alert("Please complete all fields marked with *.");return;}
	
	if(document.forms[0].announce_id.value == -1 || document.forms[0].announce_id.value == ""){
		if(!confirm("Add this announcement?")){return;}
		document.forms[0].which_action.value="add_announce";
	}
	else{
		if(!confirm("Save details for this entry?")){return;}
		document.forms[0].which_action.value="modify_announce";
	}
	document.forms[0].target="announce_frame";
	document.forms[0].action="announce.php";
	document.forms[0].submit();
}

function create_new_PM(){
	
	document.forms[0].planned_start_date.value = "";
	document.forms[0].planned_finish_date.value = "";
	document.forms[0].planned_service_name.value = "";
	document.forms[0].planned_company_name.value = "";
	document.forms[0].planned_other.value = "";
	document.forms[0].service_add_button.disabled = false;
	document.forms[0].service_remove_button.disabled = true;
	document.forms[0].pm_id.value=-1;
	
}

function change_planned(id){
	location.href="rmcs.php?pm_id="+ id +"&rmc_num="+ document.forms[0].rmc_num.value;
}

function remove_planned(){
	if(!confirm("Remove this entry?")){return;}
	document.forms[0].which_action.value="remove_planned";
	document.forms[0].submit();
}

function save_planned(){
	var bad=0;
	if(document.forms[0].planned_service_name.value==""){bad=1;}
	if(document.forms[0].planned_company_name.value==""){bad=1;}
		if(document.forms[0].planned_finish_date.value==""){bad=1;}
	
	if(bad==1){alert("Please complete all fields marked with *.");return;}
	
	if(document.forms[0].pm_id.value == -1 || document.forms[0].pm_id.value == ""){
		if(!confirm("Add this planned maintenance entry?")){return;}
		document.forms[0].which_action.value="add_planned";
	}
	else{
		if(!confirm("Save details for this entry?")){return;}
		document.forms[0].which_action.value="modify_planned";
	}
	
	document.forms[0].submit();
}

function save(){

	var bad=0;	
	if(!confirm("Save your changes?")){return;}
	
	document.forms[0].which_action.value="modify";
	document.forms[0].action="rmcs.php";
	document.forms[0].target="_self";
	document.forms[0].submit();
}

function remove(){

	var allowed = "<?=$remove?>";
	if(document.forms[0].rmc_num.value==""){alert("Please select an RMC to remove.");return;}
	if(allowed=="no"){alert("Please assign all of the buildings associated with this RMC\nto another before removing it.");return;}
	if(!confirm("Remove this RMC?")){return;}
	document.forms[0].which_action.value="remove";
	document.forms[0].action="rmcs.php";
	document.forms[0].target="_self";
	document.forms[0].submit();
}

function changeRmc(id,rmc_search){
	if(id != ""){
		location.href="rmcs.php?rmc_search="+rmc_search+"&rmc_num="+ id;
	}
	else{
		location.href="rmcs.php";
	}
}

function check_agm(){
	if(document.forms[0].agm_notes_type.value == "-"){
		alert("Please select the type of meeting notes file.");
		return true;
	}
	else if(document.forms[0].agm_file.value != "" && document.forms[0].agm_year.value == ""){
		alert("You have selected to upload an meeting notes file, but\nhave not filled in the date of the meeting.");
		return true;
	}
	else if(!document.forms[0].agm_year.value.match("../../....")){
		alert("The date needs to be in dd/mm/yyyy format.");
		return true;
	}
	return false;
}

function add_agm(){
	if(check_agm()){
		return false;
	}
	else{
		if(document.forms[0].agm_file.value == "" || document.forms[0].agm_year.value == "" || document.forms[0].agm_notes_type.value == "-"){
			alert("Please fill in the details for uploading the AGM file.");
			return false;
		}
		document.forms[0].which_action.value = "add_agm_notes";
		document.forms[0].action="agm_notes.php";
		document.forms[0].target="agm_frame";
		document.forms[0].submit();
	}
}

function check_site_visit(){
	if(document.forms[0].site_visit_file.value != "" && document.forms[0].site_visit_date.value == ""){
		alert("You have selected to upload a site visit file, but\nhave not filled in the date of the visit.");
		return true;
	}
	else if(document.forms[0].site_visit_file.value != "" && !document.forms[0].site_visit_file.value.match("\.pdf")){
		alert("The site visits file you have selected can only be uploaded in Adobe PDF format.  Please re-format the existing file to this format.");
		return true;
	}
	else if(!document.forms[0].site_visit_date.value.match("../../....")){
		alert("The date needs to be in dd/mm/yyyy format.");
		return true;
	}
	return false;
}

function add_site_visit(){
	if(check_site_visit()){
		return false;
	}
	else{
		if(document.forms[0].site_visit_file.value == "" || document.forms[0].site_visit_date.value == ""){
			alert("Please fill in the details for uploading the AGM file.");
			return false;
		}
		document.forms[0].which_action.value = "add_site_visit";
		document.forms[0].action="site_visits.php";
		document.forms[0].target="site_visit_frame";
		document.forms[0].submit();
	}
}

function add_car_park(){

	if(document.forms[0].car_park_type.value == "-"){
		alert("Please select the type of Car Park document you are uploading.");
		return false;
	}
	if(document.forms[0].carpark_file.value == ""){
		alert("Please first select a Car Park Plan to upload.");
		return false;
	}
	
	document.forms[0].which_action.value = "add_car_park";
	document.forms[0].action="car_park.php";
	document.forms[0].target="car_park_frame";
	document.forms[0].submit();
	
}

function check_newsletter(){
	if(document.forms[0].newsletter_title.value == ""){
		alert("Please insert a title for this Newsletter.");
		return true;
	}
	else if(document.forms[0].newsletter_issue_date.value == "" || !document.forms[0].newsletter_issue_date.value.match("../../....")){
		alert("Please insert the date of this Newsletter in the format (dd/mm/yyyy).");
		return true;
	}
	else if(document.forms[0].newsletter_file.value !="" && !document.forms[0].newsletter_file.value.match("\.pdf")){
		alert("The Newsletter file you have selected can only be uploaded in Adobe PDF format.  Please re-format the existing file to this format.");
		return true;
	}
	return false;
}

function add_newsletter(){
	if(check_newsletter()){
		return false;
	}
	else{
		document.forms[0].which_action.value = "add_newsletter";
		document.forms[0].action="newsletters.php";
		document.forms[0].target="newsletter_frame";
		document.forms[0].submit();
	}
}

function add_lease(){

	if(document.forms[0].lease_file.value == ""){
		alert("Please browse for the Lease which is to be uploaded.");
		return false;
	}
	document.forms[0].which_action.value = "add_lease";
	document.forms[0].action="lease.php";
	document.forms[0].target="lease_frame";
	document.forms[0].submit();

}

function add_circ_letter(){

	if(document.forms[0].circ_letter_file.value == ""){
		alert("Please browse for the Letter which is to be uploaded.");
		return false;
	}
	document.forms[0].which_action.value = "add_circ_letter";
	document.forms[0].action="circ_letters.php";
	document.forms[0].target="circ_letters_frame";
	document.forms[0].submit();

}

function add_sla(){

	if(document.forms[0].sla_file.value == ""){
		alert("Please browse for the SLA which is to be uploaded.");
		return false;
	}
	document.forms[0].which_action.value = "add_sla";
	document.forms[0].action="sla.php";
	document.forms[0].target="sla_frame";
	document.forms[0].submit();

}

function add_ssm(){

	if(document.forms[0].ssm_file.value == ""){
		alert("Please browse for the Site Specific Maintenance File which is to be uploaded.");
		return false;
	}
	document.forms[0].which_action.value = "add_ssm";
	document.forms[0].action="ssm.php";
	document.forms[0].target="ssm_frame";
	document.forms[0].submit();

}

function add_client(){

	if(document.forms[0].client_file.value == ""){
		alert("Please browse for the Client Document which is to be uploaded.");
		return false;
	}
	if(document.forms[0].client_title.value == ""){
		alert("Please enter a title for the Client Document which is to be uploaded.");
		return false;
	}
	document.forms[0].which_action.value = "add_client";
	document.forms[0].action="client.php";
	document.forms[0].target="client_frame";
	document.forms[0].submit();

}

function check_budget(){

	var from_date = document.forms[0].budget_from.value.replace(/\//g, "");
	var to_date = document.forms[0].budget_to.value.replace(/\//g, "");
	
	if(document.forms[0].budget_from.value != "" && document.forms[0].budget_to.value != ""){
		from_date = parseFloat("" + from_date.substr(4,4) + from_date.substr(2,2) + from_date.substr(0,2));
		to_date = parseFloat("" + to_date.substr(4,4) + to_date.substr(2,2) + to_date.substr(0,2));
	}
	
	if(document.forms[0].budget_from.value == "" || document.forms[0].budget_to.value == ""){
		alert("Please select the period to which this budget applies.");
		return true;
	}
	else if(from_date >= to_date){
		alert("The period you have specified is incorrect.");
		return true;
	}
	else if(document.forms[0].budget_type.value == "-"){
		alert("Please select the type of Budget you are uploading.");
		return true;
	}
	else if(document.forms[0].budget_file.value == ""){
		alert("Please select a budget file.");
		return true;
	}
	else if(document.forms[0].budget_file.value !="" && !document.forms[0].budget_file.value.match("\.pdf")){
		alert("The budget file you have selected can only be uploaded in Adobe PDF format.  Please re-format the existing file to this format.");
		return true;
	}
	else if(!document.forms[0].budget_from.value.match("../../....") || !document.forms[0].budget_to.value.match("../../....")){
		alert("The date needs to be in 'dd/mm/yyyy' format.");
		return true;
	}
	return false;
}

function add_budget(){
	if(check_budget()){
		return false;
	}
	else{
		document.forms[0].which_action.value = "add_budget";
		document.forms[0].action="budgets.php";
		document.forms[0].target="budget_frame";
		document.forms[0].submit();
	}
}

function show_password_slip(template){
	window.open("password_slip.php?template=" + template + "&type=2&rmc_num=" + document.forms[0].rmc_num.value,"PASSWIN","width=900, height=900, scrollbars=yes, resizable=yes, status=yes");
}

function show_print_history(){
	window.open("print_history.php?type=2&rmc_num=" + document.forms[0].rmc_num.value,"PRINTHISWIN","width=600, height=550, scrollbars=yes, resizable=yes, status=yes");
}

//-->
</script>
<script language="javascript" src="../library/jscript/functions/date_auto_format.js"></script>
<style type="text/css">
<!--
.style1 {
	color: #006633;
	font-weight: bold;
}
-->
</style>
</head>
<body class="management_body">
<form method="post" action="rmcs.php" enctype="multipart/form-data">
<? require($UTILS_FILE_PATH."management/menu.php");?>

<table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
	    <tr valign="top">
          <td><table border="0" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="border: 1px #000000 solid" width="100%">
            <tr>
              <td align="center"><img src="../images/management/icons/rmcs_icon_new.gif" width="70" height="50"></td>
            </tr>
            <tr>
              <td align="center"><a href="index.php"><b><< Back to main menu</b></a></td>
            </tr>
            <tr>
              <td bgcolor="#ffffff" valign="top" align="center"></td>
            </tr>
            </table></td>
    </tr>
      <tr valign="top">
        <td>&nbsp;</td>
    </tr>
    <tr valign="top">
	  <td><table width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="border: 1px #333333 solid">
        <tr>
          <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td height="10"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td colspan="3"><strong>Search for a Management Company</strong></td>
                      </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td align="right">&nbsp;</td>
                    </tr>
                    <tr>
                      <td>Man co. name/number&nbsp;&nbsp;</td>
                      <td><input name="rmc_search" type="text" size="30" value="<?=$_REQUEST['rmc_search']?>"></td>
                      <td><input onClick="document.getElementById('rmc_search_frame').src = 'rmc_search.php?whichaction=search&rmc_search=' + document.forms[0].rmc_search.value;" type="button" name="go_button" value=" Go "></td>
                    </tr>
                </table></td>
              </tr>
              <tr>
                <td height="10"></td>
              </tr>
              <tr>
                <td style="border:1px solid #666666;"><iframe src="rmc_search.php<? if($_REQUEST['rmc_num'] != ""){print "?whichaction=search&rmc_num=".$_REQUEST['rmc_num'];}?>" frameborder="0" height="100" id="rmc_search_frame" scrolling="yes" width="480"></iframe></td>
              </tr>
              
              
           </table></td>
        </tr>
      </table></td>
    </tr>

	
	<? if($rmc->rmc['rmc_num'] != ""){?>
	<tr valign="top">
	  <td>&nbsp;</td>
    </tr>
	<tr valign="top">
	  <td><table width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#f1f1f1" style="border: 1px #333333 solid">
        <tr>
          <td width="72%" class="titles">Visibility</td>
          <td width="28%" align="right"><img onMouseOver="this.style.cursor='hand';" onClick="if(this.src.match('expand.gif')){this.src = '../images/expand_down.gif';document.getElementById('visibility_table').style.display='block';}else{this.src = '../images/expand.gif';document.getElementById('visibility_table').style.display='none';}" src="../images/expand.gif" width="16" height="16"></td>
        </tr>
      </table></td>
    </tr>
	<tr valign="top">
	  <td><table id="visibility_table" width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="display:none;border-left: 1px #333333 solid;border-right: 1px #333333 solid;border-bottom: 1px #333333 solid;border-top: 1px #cccccc solid;">
        <tr>
          <td>
		  <table width="100%" border="0" cellpadding="3" cellspacing="0">
              
			<tr>
				<td></td>
				<td style="vertical-align:top;">
				<span style="font-weight:bold;">Site Login</span>
				
				<?
				$num_links = 0;
				
				// Get Lessee account serial...
				$sql_auto_1 = "
				SELECT resident_serial  
				FROM cpm_lookup_residents lres, cpm_residents_extra rex, cpm_residents re 
				WHERE 
				lres.resident_lookup=re.resident_num AND 
				rex.resident_num=re.resident_num AND 
				re.is_resident_director <> 'Y' AND 
				re.is_subtenant_account <> 'Y' AND 
				lres.resident_ref LIKE 't0%' AND 
				re.resident_is_developer = 'N' AND 
				rex.rmc_num=".$security->clean_query($rmc->rmc['rmc_num']);
				$result_auto_1 = @mysql_query($sql_auto_1);
				$row_auto_1 = @mysql_fetch_row($result_auto_1);
				if($row_auto_1[0] != ""){
					$num_links++;
				}
				
				// Get Sub-tenant account serial...
				$sql_auto_2 = "
				SELECT resident_serial  
				FROM cpm_lookup_residents lres, cpm_residents_extra rex, cpm_residents re 
				WHERE 
				lres.resident_lookup=re.resident_num AND 
				rex.resident_num=re.resident_num AND 
				re.is_resident_director <> 'Y' AND 
				rex.is_subtenant_account = 'Y' AND 
				lres.resident_ref LIKE 't0%' AND 
				re.resident_is_developer = 'N' AND 
				rex.rmc_num=".$security->clean_query($rmc->rmc['rmc_num']);
				$result_auto_2 = @mysql_query($sql_auto_2);
				$row_auto_2 = @mysql_fetch_row($result_auto_2);
				if($row_auto_2[0] != ""){
					$num_links++;
				}
				
				// Get Director account serial...
				$sql_auto_3 = "
				SELECT resident_serial 
				FROM cpm_lookup_residents lres, cpm_residents_extra rex, cpm_residents re 
				WHERE 
				lres.resident_lookup=re.resident_num  AND 
				rex.resident_num=re.resident_num AND 
				re.is_resident_director = 'Y' AND 
				re.is_subtenant_account <> 'Y' AND 
				lres.resident_ref LIKE 't0%' AND 
				re.resident_is_developer = 'N' AND 
				rex.rmc_num=".$security->clean_query($rmc->rmc['rmc_num']);
				$result_auto_3 = @mysql_query($sql_auto_3);
				$row_auto_3 = @mysql_fetch_row($result_auto_3);
				if($row_auto_3[0] != ""){
					$num_links++;
				}
				
				
				if($num_links > 0){
					?>
					<p>Below are some example links of what the various types of user will see when logged into their account. <strong>Note:</strong> these are only example accounts and do not show any information that is specific to a particular user of RMG Living.</p>
					<?
					
					if($row_auto_1[0] != ""){
						?>
						<br>
						<a href="<?=$UTILS_HTTPS_ADDRESS?>management/resident_auto_login.php?s=<?=$row_auto_1[0]?>" style="text-decoration:underline " target="_blank">Log in as Standard Lessee</a>
						<?
					}
					if($row_auto_2[0] != ""){
						?>
						<br>
						<a href="<?=$UTILS_HTTPS_ADDRESS?>management/resident_auto_login.php?s=<?=$row_auto_2[0]?>" style="text-decoration:underline " target="_blank">Log in as Sub-tenant</a>
						<?
					}
					if($row_auto_3[0] != ""){
						?>
						<br>
						<a href="<?=$UTILS_HTTPS_ADDRESS?>management/resident_auto_login.php?s=<?=$row_auto_3[0]?>" style="text-decoration:underline " target="_blank">Log in as Director</a></td>
						<?
					}
				}
				else{
					?>
					<p>This Management Company does not have any test login accounts.</p>
					<?	
				}
				?>
			</tr>

          </table></td>
        </tr>
      </table></td>
    </tr>
	<tr valign="top">
	  <td>&nbsp;</td>
    </tr>
	<tr valign="top">
	  <td>
	  
	  
	  <table width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#f1f1f1" style="border: 1px #333333 solid">
				<tr>
					<td width="73%" class="titles">Details</td>
				    <td width="27%" align="right"><img onMouseOver="this.style.cursor='hand';" onClick="if(this.src.match('expand.gif')){this.src = '../images/expand_down.gif';document.getElementById('rmc_details_table').style.display='block';}else{this.src = '../images/expand.gif';document.getElementById('rmc_details_table').style.display='none';}" src="../images/expand.gif" width="16" height="16"></td>
				</tr>
		</table>
	  
	  
	  
	  </td>
    </tr>
	<tr valign="top">
		<td>
			<table id="rmc_details_table" width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="display:none;border-left: 1px #333333 solid;border-right: 1px #333333 solid;border-bottom: 1px #333333 solid;border-top: 1px #cccccc solid;">
				<tr>
					<td>
					<table width="480" border="0" cellpadding="0" cellspacing="0">
							
							
							<tr valign="top">
								<td>
									<table width="480" border="0" cellpadding="3" cellspacing="0">
										<tr>
										  <td colspan="2"></td>
									  </tr>
									  <tr>
										  <td><strong>Brand</strong></td>
										  <td><?=$subsidiary->subsidiary_name?></td>
									  </tr>
									  <tr>
										  <td><strong>Man. Co. status</strong></td>
										  <td>
										  <?
										  if($rmc->rmc['rmc_is_active'] == "1"){
										  	print "Active";
										  }
										  else{
										  	print "Inactive";
										  }
										  ?>
										  </td>
										  </tr>
										<tr>
										  <td width="110"><strong>Man. Co. name</strong></td>
										  <td width="356"><?=$rmc->rmc['rmc_name']?></td>
									  </tr>
										<tr>
										  <td><strong>Man. Co. no.</strong></td>
										  <td><?=$rmc->rmc['rmc_ref']?></td>
									  </tr>
									  <tr>
									  <td style="vertical-align:top;"><strong>Property Manager</strong></td>
									  <td style="vertical-align:top;"><?=$rmc->rmc['property_manager']?></td>
									  </tr>
										<tr>
										  <td><strong>No. Lessees</strong></td>
										  <td>
										  <?
										  // Get number of residents in this RMC
										  $sql_num_res = "
										  SELECT count(resident_id) 
										  FROM cpm_residents 
										  WHERE 
										  resident_name <> 'Developer' AND 
										  resident_name <> 'RMG Test User' AND 
										  (resident_status = 'Current' OR resident_is_active = '1') AND 
										  is_resident_director = 'N' AND 
										  rmc_num = ".$rmc->rmc['rmc_num'];
										  $result_num_res = @mysql_query($sql_num_res);
										  $row_num_res = @mysql_fetch_row($result_num_res);
										  print $row_num_res[0];
										  ?></td>
									  </tr>
									  <tr>
										  <td valign="top"><strong>No. Lessees using RMG Living</strong></td>
										  <td valign="top">
										  <?
										  // Get number of residents in this RMC
										  $sql_num_active_res = "SELECT count(res.resident_id) FROM cpm_residents res, cpm_residents_extra rex WHERE res.resident_num=rex.resident_num AND res.resident_name <> 'RMG Test User' AND rex.email <> '' AND rex.email <> 'webmaster@rmgliving.co.uk' AND res.resident_name <> 'Developer' AND (res.resident_status = 'Current' OR res.resident_is_active='1') AND res.rmc_num = ".$rmc->rmc['rmc_num'];
										  $result_num_active_res = @mysql_query($sql_num_active_res);
										  $row_num_active_res = @mysql_fetch_row($result_num_active_res);
										  print $row_num_active_res[0];
										  ?>
										  </td>
									  </tr>
										<tr>
										  <td>&nbsp;</td>
										  <td>&nbsp;</td>
									  </tr>
										
										<tr>
										  <td>&nbsp;</td>
										  <td>&nbsp;</td>
									  </tr>
										
										
										<tr>
                                          <td colspan="2"><strong>Development description</strong></td>
                                      </tr>
										<tr valign="top">
                                          <td colspan="2"><textarea name="dev_description" cols="70" rows="7" id="dev_description"><?=stripslashes($rmc->rmc['dev_description'])?></textarea></td>
									  </tr>
										<tr>
										  <td>&nbsp;</td>
										  <td>&nbsp;</td>
									  </tr>
									  <tr><td colspan="2">
									  <strong>Property Image</strong><br /><br />
									  Images must be in either .gif or .jpeg format ONLY. The system will automatically resize the image once uploaded, but the original must be at least 125 pixels wide by 100 pixels high.
									  </td></tr>
										<tr>
											<td colspan="2"><input type="file" id="image_file" name="image_file"></td>
										</tr>
										<tr valign="top">
										  <td colspan="2"><?=$image?></td>
									  </tr>
									    <tr>
									      <td colspan="2">&nbsp;</td>
								      </tr>
									  <tr>
									  <td colspan="2">
									  <strong>Developer</strong><br /><br />
									  Please select the name of the Developer who built this development. If you require a new developer to be added to this list, please contact <a href="mailto:support@rmgliving.co.uk">support@rmgliving.co.uk</a>
									  </td>
									  </tr>
									    <tr>
									      <td colspan="2">
										  <select name="developer_id" id="developer_id">
										  <option value="-">-</option>
										  <?
										  $sql_developer = "SELECT * FROM cpm_developers ORDER BY developer_name ASC";
										  $result_developer = @mysql_query($sql_developer);
										  while($row_developer = @mysql_fetch_array($result_developer)){?>
										  <option value="<?=$row_developer['developer_id']?>" <? if($row_developer['developer_id'] == $rmc->rmc['developer_id']){print "selected";}?>><?=$row_developer['developer_name']?></option>
										  <? }?>
							              </select>
										  </td>
								      </tr>
								      <tr>
									  <td colspan="2">&nbsp;</td>
									  </tr>
									  <tr>
									  <td colspan="2" align="right"><input type="button" value=" Save " onClick="save()"></td>
									  </tr>
								  </table>
							  </td>
					  </tr>
					  </table>
				  </td>
			  </tr>
		  </table>
	  </td>
	</tr>
							
							<tr>
							  <td>&nbsp;</td>
    						</tr>
							<tr>
							  <td><table width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#f1f1f1" style="border: 1px #333333 solid">
                                <tr>
                                  <td width="74%" class="titles">Announcements</td>
                                  <td width="26%" align="right"><img onMouseOver="this.style.cursor='hand';" onClick="if(this.src.match('expand.gif')){this.src = '../images/expand_down.gif';document.getElementById('announce_table').style.display='block';}else{this.src = '../images/expand.gif';document.getElementById('announce_table').style.display='none';}" src="<? if($_REQUEST['announce_id'] != ""){?>../images/expand_down.gif<? }else{?>../images/expand.gif<? }?>" width="16" height="16"></td>
                                </tr>
                              </table></td>
    						</tr>
							<tr>
							  <td><table id="announce_table" width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="display:none;border-left: 1px #333333 solid;border-right: 1px #333333 solid;border-bottom: 1px #333333 solid;border-top: 1px #cccccc solid;">
                                <tr>
                                  <td><table border="0" cellpadding="3" cellspacing="0" width="100%">
								  	<tr>
									<td colspan="2"><strong>Current Announcements</strong></td>
									</tr>
									<tr>
									  <td colspan="2" height="10"></td>
									  </tr>
                                      <tr>
                                        <td colspan="2" style="border:1px solid #999999;"><iframe src="announce.php?rmc_num=<?=$rmc->rmc['rmc_num']?>" frameborder="1" height="100" id="announce_frame" name="announce_frame" scrolling="yes" width="475"></iframe>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2" height="5"></td>
                                      </tr>
                                      <tr>
									  	<td colspan="2">
										
										<table width="100%" border="0">
										<tr>
										<td align="right">Awaiting approval&nbsp;&nbsp;</td>
										<td width="20" style="border:1px solid #666666;" bgcolor="#FFD5D5" height="15">&nbsp;</td>
										</tr>
										</table>
										
										</td>
                                      <tr>
                                        <td nowrap>&nbsp;</td>
                                        <td>&nbsp;</td>
                                      </tr>
									  <tr>
									  <td colspan="2">&nbsp;</td>
									  </tr>
									  <tr>
									  <td colspan="2"><strong>Add an Announcement</strong></td>
									  </tr>
									  <tr>
									  <td colspan="2" height="10"></td>
									  </tr>
                                      <tr>
                                        <td width="11%" nowrap>Title * </td>
                                        <td width="89%"><input name="announce_title" type="text" id="announce_title" value="<?=stripslashes($row_announce['announce_title'])?>" style="width:410px; "></td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">Details of the announcement * </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2"><textarea id="announce_text" name="announce_text" style="width:463px;" rows="8"><?=stripslashes($row_announce['announce_text'])?></textarea></td>
                                      </tr>
                                      <tr>
                                        <td colspan="2">
										<input type="hidden" name="announce_id" id="announce_id">
										<input id="announce_add_button" type="button" value=" Save " onClick="save_announce()">
                                          <input type="button" value="Create new" onClick="create_new_announce()">
										</td>
                                      </tr>
                                  </table></td>
                                </tr>
                              </table></td>
   							</tr>
							<tr>
							  <td>&nbsp;</td>
    						</tr>
							<tr>
							  <td>
							  
		
							<tr>
							  <td><table width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#f1f1f1" style="border: 1px #333333 solid">
                                <tr>
                                  <td width="74%" class="titles">Uploads</td>
                                  <td width="26%" align="right"><img onMouseOver="this.style.cursor='hand';" onClick="if(this.src.match('expand.gif')){this.src = '../images/expand_down.gif';document.getElementById('downloads_table').style.display='block';}else{this.src = '../images/expand.gif';document.getElementById('downloads_table').style.display='none';}" src="../images/expand.gif" width="16" height="16"></td>
                                </tr>
                              </table></td>
    </tr>
							<tr>
							  <td>
							  
							  
							  <table id="downloads_table" width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="display:none;border-left: 1px #333333 solid;border-right: 1px #333333 solid;border-bottom: 1px #333333 solid;border-top: 1px #cccccc solid;">
				<tr>
					<td>
					
					
<table width="100%" border="0" cellpadding="3" cellspacing="0">
						
<tr>
  <td colspan="2" height="10"></td>
</tr>
<tr>		  
<td colspan="2" style="border:1px #003366 solid;padding:5px;background-color:#003366" class="subtitles">Meeting Notes</td>
</tr>
					  
<tr>
						  
<td colspan="2" height="10"></td>
</tr>
						
<tr>
						  
<td colspan="2" style="border:1px solid #999999;"><iframe src="agm_notes.php?rmc_num=<?=$rmc->rmc['rmc_num']?>" frameborder="1" height="100" id="agm_frame" name="agm_frame" scrolling="yes" width="475"></iframe>
</td>
</tr>
						
<tr>
						  
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
						
<tr>
						  
<td>Type of meeting </td>
<td nowrap>
						  
<select name="agm_notes_type" id="agm_notes_type">
						    
<option value="-" selected>-</option>
							
<?
							// Get meeting types
							$sql_mt = "SELECT * FROM cpm_meeting_types";
							$result_mt = @mysql_query($sql_mt);
							while($row_mt = @mysql_fetch_array($result_mt)){
							?>
							
<option value="<?=$row_mt['meeting_type_id']?>">
<?=$row_mt['meeting_type']?>
</option>
							
<?
							}
							?>
					      
</select>
						  </td>
</tr>
						
<tr>
						  
<td>Meeting notes file<br>(PDF only)</td>
<td nowrap>
<input type="file" name="agm_file">
</td>
</tr>
						
<tr>
						  
<td>Date of meeting <br>(dd/mm/yyyy)</td>
<td>
<input type="text" name="agm_year" onBlur="check_and_format_date(this,this.value)">
</td>
</tr>
						
<tr>
						  
<td colspan="2" height="10"></td>
</tr>





<tr>						  
<td colspan="2">
<input onClick="add_agm()" type="button" name="add_agm_button" value=" Add ">
</td>
</tr>


<tr>
  <td colspan="2">&nbsp;</td>
</tr>
<tr>
  <td colspan="2">&nbsp;</td>
</tr>
<tr>
  <td colspan="2" style="border:1px #003366 solid;padding:5px;background-color:#003366" class="subtitles">Site Visits</td>
</tr>
<tr>
  <td colspan="2" height="10"></td>
</tr>
<tr>
  <td colspan="2" style="border:1px solid #999999;"><iframe src="site_visits.php?rmc_num=<?=$rmc->rmc['rmc_num']?>" frameborder="1" height="100" id="site_visit_frame" name="site_visit_frame" scrolling="yes" width="475"></iframe></td>
</tr>
<tr>
  <td colspan="2">&nbsp;</td>
</tr>
<tr>
  <td>Site visit  file<br>
    (PDF only)</td>
  <td><input type="file" name="site_visit_file"></td>
</tr>
<tr>
  <td>Date of site visit <br>
    (dd/mm/yyyy)</td>
  <td><input type="text" name="site_visit_date" onBlur="check_and_format_date(this,this.value)"></td>
</tr>
<tr>
						  
<td colspan="2" height="10"></td>
</tr>
<tr>
  <td colspan="2"><input onClick="add_site_visit()" type="button" name="add_site_visit_button" value=" Add "></td>
</tr>
<tr>
  <td colspan="2">&nbsp;</td>
</tr>


<tr>
  <td colspan="2">&nbsp;</td>
</tr>
<tr>
  <td colspan="2" style="border:1px #003366 solid;padding:5px;background-color:#003366" class="subtitles">Circulated Letters</td>
</tr>
<tr>
  <td colspan="2" height="10"></td>
</tr>
<tr>
  <td colspan="2" style="border:1px solid #999999;"><iframe src="circ_letters.php?rmc_num=<?=$rmc->rmc['rmc_num']?>" frameborder="1" height="100" id="circ_letters_frame" name="circ_letters_frame" scrolling="yes" width="475"></iframe>
</td>
</tr>
<tr>
  <td colspan="2">&nbsp;</td>
</tr>
<tr>
  <td>Type of letter </td>
  <td><select name="circ_letter_type" id="circ_letter_type">
    <option value="-" selected>-</option>
	<?
	$sql_letter = "SELECT * FROM cpm_letter_types ORDER BY letter_type ASC";
	$result_letter = @mysql_query($sql_letter);
	while($row_letter = @mysql_fetch_array($result_letter)){
	?>
	<option value="<?=$row_letter['letter_type_id']?>" selected><?=$row_letter['letter_type']?></option>
	<?
	}
	?>
    </select></td>
</tr>
<tr>
  <td>File (PDF format ony) </td>
  <td><input type="file" name="circ_letter_file"></td>
</tr>
<tr>
  <td colspan="2" height="10"></td>
  </tr>
<tr>
  <td colspan="2"><input onClick="add_circ_letter()" type="button" name="add_circ_letter_button" value=" Add "></td>
</tr>

<tr>
  <td colspan="2">&nbsp;</td>
</tr>



<tr>	  
<td colspan="2">&nbsp;
</td>
</tr>
						
<tr>
						  
<td colspan="2" style="border:1px #003366 solid;padding:5px;background-color:#003366" class="subtitles">Car Park Plan</td>
</tr>
<tr>
  <td colspan="2" height="10"></td>
</tr>
<tr>
  <td colspan="2" style="border:1px solid #999999;"><iframe src="car_park.php?rmc_num=<?=$rmc->rmc['rmc_num']?>" frameborder="1" height="100" id="car_park_frame" name="car_park_frame" scrolling="yes" width="475"></iframe>
</td>
</tr>
<tr>
  <td colspan="2">&nbsp;</td>
</tr>
<tr>
  <td>Type of document </td>
  <td><select name="car_park_type" id="car_park_type">
    <option value="-" selected>-</option>
	<?
	$sql_cptype = "SELECT * FROM cpm_car_park_types ORDER BY car_park_type ASC";
	$result_cptype = @mysql_query($sql_cptype);
	while($row_cptype = @mysql_fetch_array($result_cptype)){
	?>
	<option value="<?=$row_cptype['car_park_type_id']?>"><?=$row_cptype['car_park_type']?></option>
	<?
	}
	?>
    </select></td>
</tr>
<tr>
						  
<td width="30%">Car park file<br>(PDF only) </td>
<td width="70%">
						  
<input type="file" name="carpark_file">
						  </td>
</tr>
						
<tr>
  <td colspan="2" height="10"></td>
</tr>
<tr>
  <td colspan="2">&nbsp;</td>
</tr>
<tr>
  <td colspan="2"><input onClick="add_car_park()" type="button" name="add_car_park_button" value=" Add "></td>
</tr>
<tr>
  <td colspan="2">&nbsp;</td>
</tr>

<tr>
  <td colspan="2">&nbsp;</td>
</tr>

<tr>
  <td colspan="2" style="border:1px #003366 solid;padding:5px;background-color:#003366" class="subtitles">Management Agreements</td>
</tr>
<tr>
  <td colspan="2" height="10"></td>
</tr>
<tr>
  <td colspan="2" style="border:1px solid #999999;"><iframe src="sla.php?rmc_num=<?=$rmc->rmc['rmc_num']?>" frameborder="1" height="100" id="sla_frame" name="sla_frame" scrolling="yes" width="475"></iframe>
</td>
</tr>
<tr>
  <td colspan="2">&nbsp;</td>
</tr>
<tr>
  <td>From (dd/mm/yyyy) : </td>
  <td><input name="sla_from_date" type="text" id="sla_from_date" onBlur="check_and_format_date(this,this.value)"></td>
</tr>
<tr>
  <td>To (dd/mm/yyyy) :</td>
  <td><input name="sla_to_date" type="text" id="sla_to_date" onBlur="check_and_format_date(this,this.value)"></td>
</tr>
<tr>
  <td>File (PDF format ony) : </td>
  <td><input type="file" name="sla_file"></td>
</tr>
<tr>
  <td colspan="2" height="10"></td>
  </tr>
<tr>
  <td colspan="2"><input onClick="add_sla()" type="button" name="add_sla_button" value=" Add "></td>
</tr>


<tr>
  <td colspan="2">&nbsp;</td>
</tr>
<tr>
  <td colspan="2">&nbsp;</td>
</tr>
<tr>
  <td colspan="2" style="border:1px #003366 solid;padding:5px;background-color:#003366" class="subtitles">Site Specific Maintenance</td>
</tr>
<tr>
  <td colspan="2" height="10"></td>
</tr>
<tr>
  <td colspan="2" style="border:1px solid #999999;"><iframe src="ssm.php?rmc_num=<?=$rmc->rmc['rmc_num']?>" frameborder="1" height="100" id="ssm_frame" name="ssm_frame" scrolling="yes" width="475"></iframe>
</td>
</tr>
<tr>
  <td colspan="2">&nbsp;</td>
</tr>
<tr>
  <td>From (dd/mm/yyyy) : </td>
  <td><input name="ssm_from_date" type="text" id="ssm_from_date" onBlur="check_and_format_date(this,this.value)"></td>
</tr>
<tr>
  <td>To (dd/mm/yyyy) :</td>
  <td><input name="ssm_to_date" type="text" id="ssm_to_date" onBlur="check_and_format_date(this,this.value)"></td>
</tr>
<tr>
  <td>File (PDF format ony) : </td>
  <td><input type="file" name="ssm_file"></td>
</tr>
<tr>
  <td colspan="2" height="10"></td>
  </tr>
<tr>
  <td colspan="2"><input onClick="add_ssm()" type="button" name="add_ssm_button" value=" Add "></td>
</tr>

<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td colspan="2">&nbsp;
	</td>
</tr>
<tr>
	<td colspan="2" style="border:1px #003366 solid;padding:5px;background-color:#003366" class="subtitles">Client Documents</td>
</tr>
<tr>
	<td colspan="2" height="10"></td>
</tr>
<tr>
	<td colspan="2" style="border:1px solid #999999;"><iframe src="client.php?rmc_num=<?=$rmc->rmc['rmc_num']?>" frameborder="1" height="100" id="client_frame" name="client_frame" scrolling="yes" width="475"></iframe>
	</td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
	<td>Title</td>
	<td><input name="client_title" type="text" id="client_title" maxlength="30"></td>
</tr>
<tr>
	<td>From (dd/mm/yyyy) : </td>
	<td><input name="client_from_date" type="text" id="client_from_date" onBlur="check_and_format_date(this,this.value)"></td>
</tr>
<tr>
	<td>To (dd/mm/yyyy) :</td>
	<td><input name="client_to_date" type="text" id="client_to_date" onBlur="check_and_format_date(this,this.value)"></td>
</tr>
<tr>
	<td>File (PDF format ony) : </td>
	<td><input type="file" name="client_file"></td>
</tr>
<tr>
	<td colspan="2" height="10"></td>
</tr>
<tr>
	<td colspan="2"><input onClick="add_client()" type="button" name="add_client_button" value=" Add "></td>
</tr>
<tr>
  <td colspan="2">&nbsp;</td>
</tr>
<tr>
<td colspan="2">&nbsp;
</td>
</tr>
<tr>
  <td colspan="2" style="border:1px #003366 solid;padding:5px;background-color:#003366" class="subtitles">Newsletters</td>
  </tr>
<tr>
  <td colspan="2" height="10"></td>
  </tr>
<tr>
  <td colspan="2" style="border:1px solid #999999;"><iframe src="newsletters.php?rmc_num=<?=$rmc->rmc['rmc_num']?>" frameborder="1" height="100" id="newsletter_frame" name="newsletter_frame" scrolling="yes" width="475"></iframe></td>
  </tr>
<tr>
  <td>&nbsp;</td>
  <td></td>
</tr>
<tr>
  <td>Title</td>
  <td><input name="newsletter_title" type="text" id="newsletter_title"></td>
</tr>
<tr>
  <td>Date of issue<br>(dd/mm/yyyy) </td>
  <td><input name="newsletter_issue_date" type="text" id="newsletter_issue_date" onBlur="check_and_format_date(this,this.value)"></td>
</tr>
<tr>
  <td>Newsletter file<br>(PDF format only) </td>
  <td><input type="file" name="newsletter_file"></td>
</tr>
<tr>
  <td colspan="2" height="10"></td>
  </tr>
<tr>
  <td><input onClick="add_newsletter()" type="button" name="add_newsletter_button" value=" Add "></td>
  <td></td>
</tr>
<tr>
  <td colspan="2">&nbsp;</td>
</tr>
<tr>
  <td colspan="2">&nbsp;</td>
  </tr>
<tr>
<td colspan="2" style="border:1px #003366 solid;padding:5px;background-color:#003366" class="subtitles">Budgets</td>
</tr>
<tr>
<td colspan="2" height="10"></td>
</tr>
<tr>
                          
<td colspan="2" style="border:1px solid #999999;">
<iframe src="budgets.php?rmc_num=<?=$rmc->rmc['rmc_num']?>" frameborder="1" height="100" id="budget_frame" name="budget_frame" scrolling="yes" width="475"></iframe>
</td>
</tr>
<tr>
<td>&nbsp;</td>
<td></td>
</tr>
<tr>
<td>Budget from (dd/mm/yyyy)</td>
<td>
<input name="budget_from" type="text" id="budget_from" onBlur="check_and_format_date(this,this.value)">
</td>
</tr>
<tr>
<td>Budget to (dd/mm/yyyy)</td>
<td>
<input name="budget_to" type="text" id="budget_to" onBlur="check_and_format_date(this,this.value)">
</td>
</tr>
<tr>
<td>Budget type</td>
<td>
<select name="budget_type" id="budget_type">
<option value="-" selected>-</option>
<?
$sql_bt = "SELECT * FROM cpm_budget_types ORDER BY budget_type_name ASC";
$result_bt = @mysql_query($sql_bt);
while($row_bt = @mysql_fetch_array($result_bt)){
?>
<option value="<?=$row_bt['budget_type_id']?>"><?=$row_bt['budget_type_name']?></option>
<? }?>
</select>
</td>
</tr>
<tr>
<td>Budget file</td>
<td>
<input type="file" name="budget_file">
</td>
</tr>
<tr>
<td colspan="2" height="10"></td>
</tr>
<tr>
<td>
<input onClick="add_budget()" type="button" name="add_budget_button" value=" Add ">
</td>
<td></td>
</tr>
<tr>
					  
<td>&nbsp;</td>
<td></td>
</tr>
					
</table>
					
					
					</td>
					</tr>
					</table>
							  
							  
							  
							  </td>
    </tr>
							<tr>
							  <td>&nbsp;</td>
    </tr>
							<? if($_SESSION['allow_letters'] == "1"){?>
							<tr>
							  <td><table width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#f1f1f1" style="border: 1px #333333 solid">
                                <tr>
                                  <td width="74%" class="titles">Letters</td>
                                  <td width="26%" align="right"><img onMouseOver="this.style.cursor='hand';" onClick="if(this.src.match('expand.gif')){this.src = '../images/expand_down.gif';document.getElementById('letters_table').style.display='block';}else{this.src = '../images/expand.gif';document.getElementById('letters_table').style.display='none';}" src="../images/expand.gif" width="16" height="16"></td>
                                </tr>
                              </table></td>
    </tr>
							
							<tr>
							  <td height="5">
							  
							  
							  
							  
							  <table id="letters_table" width="100%"  border="0" cellpadding="10" cellspacing="0" bgcolor="#FFFFFF" style="display:none;border-left: 1px #333333 solid;border-top: 1px #cccccc solid;border-right: 1px #333333 solid;border-bottom: 1px #333333 solid;">
                                <tr>
                                  <td>
								  
								  <? if($_SESSION['allow_letters_toggle'] == "1"){?>
								  <p><input type="checkbox" name="prevent_printing_of_letters" id="prevent_printing_of_letters" value="Y" <? if($rmc->rmc['prevent_printing_of_letters'] == "Y"){?>checked="checked"<? }?> />&nbsp;Prevent users from printing these letters.</p>
								  <? }?>
								  
								  <table width="100%"  border="0" cellspacing="0" cellpadding="0">
									<tr style="display:none;">
									    <td>&nbsp;</td>
								    </tr>
									<tr style="display:none;">
									    <td style="border-bottom:1px #003366 solid;"><img src="../images/management/rmcs/site_activation.gif" width="200" height="16"></td>
								    </tr>
									  <tr style="display:none;">
									    <td height="10"></td>
								    </tr>
									  <tr style="display:none;">
									    <td>These letters need to be sent out once the micro-site for this Resident Management Company has been activated. </td>
								    </tr>
									  <tr style="display:none;">
									    <td>&nbsp;</td>
								    </tr>
									 <? 
									 $num_director_pass_slips = 0;
									# Determine number of director password slips needed to be sent for this RMC
									$sql_dresident = "
                                        SELECT *
                                        FROM cpm_residents re
                                        INNER JOIN cpm_rmcs rmc ON re.rmc_num = rmc.rmc_num
                                        INNER JOIN cpm_residents_extra rex ON re.rmc_num = rex.rmc_num AND re.resident_num = rex.resident_num
                                        INNER JOIN cpm_lookup_residents lre ON re.resident_num = lre.resident_lookup
                                        INNER JOIN cpm_rmcs_extra rmcx ON rmc.rmc_num = rmcx.rmc_num
                                        INNER JOIN cpm_lookup_rmcs lrmc  ON rmc.rmc_num = lrmc.rmc_lookup
                                        WHERE rex.password_to_be_sent='Y' AND rmc.rmc_status <> 'Lost' AND rmc.rmc_is_active = '1' AND
                                        rex.is_linked_to_master_account <> 'Y' AND
                                        lre.resident_ref NOT LIKE 't0%' AND
                                        re.resident_is_developer = 'N' AND
                                        re.resident_name <> 'Developer' AND
                                        re.is_resident_director='Y' AND
                                        re.resident_status = 'Current' AND re.resident_is_active='1' AND re.rmc_num =".$_REQUEST['rmc_num'];
									$result_dresident = @mysql_query($sql_dresident);
									$num_director_pass_slips = @mysql_num_rows($result_dresident);
									if($num_director_pass_slips > 0){
									?>
									  <tr style="display:none;" id="director_password_status">
                                      <td>
									  <table width="100%" border="0" cellspacing="0" cellpadding="5">
									<tr>
									  <td width="7%" onClick="show_password_slip('director')" onMouseOver="this.style.cursor='hand';"><img src="../images/help_24.gif" width="24" height="24" border="0" align="absmiddle"></td>
									  <td width="93%" onClick="show_password_slip('director')" onMouseOver="this.style.cursor='hand';"><font color="#FF0000">There are <b>
									  <?=$num_director_pass_slips?>
									  </b>  Directors who have not been sent their '<strong>Site Activation</strong>' letter. Click <strong>here</strong> to print these letters.</font>
								      </td>
									  </tr>
								  </table>
									  </td>
                                    </tr>
									<? }
									?><tr style="display:none;">
									   <td id="director_sent_row" style="display:<? if($num_director_pass_slips == 0){?>block<? }else{?>none<? }?>;"><span class="style1">There are no letters to print off at this time.</span></td>
								    </tr>
									
                                    <tr>
                                      <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td style="border-bottom:1px #003366 solid;"><img src="../images/management/rmcs/first_login_details.gif" width="200" height="16"></td>
                                    </tr>
                                    <tr>
                                      <td height="10"></td>
                                    </tr>
                                    <?
									if($rmc->rmc['prevent_printing_of_letters'] != "Y"){
	
										?>									
										<tr>
                                      		<td>These letters are to be issued to all Lessees. </td>
                                    	</tr>
                                    	<tr>
                                      		<td>&nbsp;</td>
								    	</tr>
										<?
									
									   	$num_standard_pass_slips = 0;
									   
										# Determine number of standard password slips needed to be sent for this RMC
										$sql_resident = "
										SELECT *
                                        FROM cpm_residents re
                                        INNER JOIN cpm_rmcs rmc ON re.rmc_num = rmc.rmc_num
                                        INNER JOIN cpm_residents_extra rex ON re.rmc_num = rex.rmc_num AND re.resident_num = rex.resident_num
                                        INNER JOIN cpm_lookup_residents lre ON re.resident_num = lre.resident_lookup
                                        INNER JOIN cpm_rmcs_extra rmcx ON rmc.rmc_num = rmcx.rmc_num
                                        INNER JOIN cpm_lookup_rmcs lrmc  ON rmc.rmc_num = lrmc.rmc_lookup
                                        WHERE rex.password_to_be_sent='Y' AND rmc.rmc_status <> 'Lost' AND rmc.rmc_is_active = '1' AND
                                        rex.is_linked_to_master_account <> 'Y' AND
                                        lre.resident_ref NOT LIKE 't0%' AND
                                        re.resident_is_developer = 'N' AND
                                        re.resident_name <> 'Developer' AND
                                        rmcx.prevent_printing_of_letters <> 'Y' AND
                                        re.resident_status = 'Current' AND re.resident_is_active='1' AND re.rmc_num =".$_REQUEST['rmc_num'];
										$result_resident = @mysql_query($sql_resident);
										$num_standard_pass_slips = @mysql_num_rows($result_resident);
										if($num_standard_pass_slips > 0){
											?>
											<tr id="standard_password_status">
												<td>
													<table width="100%" border="0" cellspacing="0" cellpadding="5">
														<tr>
															<td width="7%" onClick="show_password_slip('standard')" onMouseOver="this.style.cursor='hand';"><img src="../images/help_24.gif" width="24" height="24" border="0" align="absmiddle"></td>
															<td width="93%" onClick="show_password_slip('standard')" onMouseOver="this.style.cursor='hand';"><font color="#FF0000">There are <b><?=$num_standard_pass_slips?></b> Lessees who have not been sent their <strong>login details</strong> letter. Click <strong>here</strong> to print these  letters.</font></td>
														</tr>
													</table>
												</td>
											</tr>
											<?
										}
										?>
										
										<tr id="standard_sent_row" style="display:<? if($num_standard_pass_slips == 0){?>block<? }else{?>none<? }?>;">
											<td><span class="style1">There are no letters to print off at this time.</span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td style="border-bottom:1px #003366 solid;"><img src="../images/management/rmcs/print_history.gif" width="200" height="16"></td>
										</tr>
										<tr>
											<td height="10"></td>
										</tr>
										<tr>
											<td onMouseOver="this.style.cursor='hand';" onClick="show_print_history();">Click <strong>here</strong> to view the print history for this RMC.</td>
										</tr>
										
										<?
									}
									else{
										?>
										<tr><td>Printing of these letters has been disabled.</td></tr>
										<?	
									}
									?>
									
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
									   <td><? if($_SESSION['allow_letters_toggle'] == "1"){?><input type="button" value=" Save " onClick="save()"><? }?>&nbsp;</td>
								    </tr>
                                  </table></td>
                                </tr>
                              </table></td>
							</tr>
							<? }?>
							<tr>
							  <td>&nbsp;</td>
							</tr>
							
							<? }?>
  </table>
					</td>
				</tr>
			</table>
	  </td>
		<td width="20"></td>
  <td>&nbsp;  </td>
	</tr>
</table>
<input type="hidden" name="rmc_num" id="rmc_num" value="<?=$rmc->rmc['rmc_num']?>">
<input type="hidden" name="which_action" id="which_action">
</form>
</body>
</html>
