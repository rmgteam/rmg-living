<?
require("utils.php");
require_once($UTILS_FILE_PATH."library/functions/check_file_extension.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."rmc.class.php");
$website = new website;
$rmc = new rmc;
$rmc->set_rmc($_REQUEST['rmc_num']);

// Determine if allowed access into content management system
$website->allow_cms_access();

// Check access privilege
if($_SESSION['allow_rmc'] != 1){header("Location:index.php");}

//=========================================
// Delete circ letter file
//=========================================
if($_REQUEST['whichaction'] == "delete_circ_letter"){

	$del_error = "N";

	$sql = "SELECT * FROM cpm_letters WHERE letter_id=".$_REQUEST['circ_letter_file_id'];
	$result = @mysql_query($sql) or $del_error = "Y";
	$row = @mysql_fetch_array($result);
	$file_name = $row['letter_file'];

	$sql = "DELETE FROM cpm_letters WHERE letter_id=".$_REQUEST['circ_letter_file_id'];
	@mysql_query($sql) or $del_error = "Y";
	
	if($del_error == "N"){
		@unlink($UTILS_PRIVATE_FILE_PATH."letters/$file_name");
	}
	
}


#=========================================
# Add letter file
#=========================================
if($_REQUEST['which_action'] == "add_circ_letter"){
	
	$file_stamp = time();
	
	# Save letter file to correct location
	$file_ext[] = "pdf";
	if(check_file_extension($_FILES['circ_letter_file']['name'], $file_ext)){
		$letter_file = "letter_".$rmc->file_friendly_name()."_".$file_stamp.".pdf";
		$letter_file_path = $UTILS_PRIVATE_FILE_PATH."letters/".$letter_file;
		copy($_FILES['circ_letter_file']['tmp_name'], $letter_file_path);

		# Insert letter details
		$sql = "
		INSERT INTO cpm_letters(
			rmc_num,
			letter_type_id,
			letter_file,
			letter_file_stamp,
			letter_uploaded_ts
		)
		VALUES(
			".$_REQUEST['rmc_num'].",
			'".$_REQUEST['circ_letter_type']."',
			'".$letter_file."',
			'".$file_stamp."',
			'".$file_stamp."'
		)
		";
		mysql_query($sql);
	}
}
?>
<html>
<head>
<link href="../styles.css" rel="stylesheet" type="text/css">
</head>
<body>
<form action="circ_letters.php">
<table border="0" cellpadding="3" cellspacing="0" width="100%">
<?
#===============================================
# Collect all letters for this RMC
#===============================================
$sql = "SELECT * FROM cpm_letters l, cpm_letter_types lt WHERE l.letter_type_id=lt.letter_type_id AND l.rmc_num = ".$_REQUEST['rmc_num'];
$result = @mysql_query($sql);
$num_letters = @mysql_num_rows($result);
if($num_letters < 1){
	print "<tr><td>There are no Letters for this RMC</td></tr>";
}
else{
	while($row = @mysql_fetch_array($result)){
		?>
		<tr bgcolor="#f3f3f3">
			<td style="border-bottom:1px solid #cccccc"><?=$row['letter_type']?> (<?=date("d/m/Y", $row['letter_file_stamp'])?>)</td>
		  <td align="right" style="border-bottom:1px solid #cccccc"><input onClick="if(confirm('Remove Letter for this RMC ?')){document.forms[0].circ_letter_file_id.value='<?=$row['letter_id']?>';document.forms[0].submit();}" type="button" name="Button" value="remove"></td>
		</tr>
		<?
	}
}
?>
</table>
<input type="hidden" id="circ_letter_file_id" name="circ_letter_file_id">
<input type="hidden" id="rmc_num" name="rmc_num" value="<?=$_REQUEST['rmc_num']?>">
<input type="hidden" name="whichaction" value="delete_circ_letter">
</form>
</body>
</html>