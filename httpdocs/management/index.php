<?
require_once("../utils.php");
require_once($UTILS_CLASS_PATH."website.class.php");
$website = new website;


// Determine if allowed access into content management system
$website->allow_cms_access();

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>RMG Living - Content Management System</title>
<link href="../styles.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

//-->
</script>
<style type="text/css">
<!--
.style1 {
	font-family: Arial, Helvetica, sans-serif;
	font-weight: bold;
	font-size: 16px;
}
-->
</style>
</head>
<body class="management_body">
<? require($UTILS_FILE_PATH."management/menu.php");?>
<table border="0" cellpadding="10" cellspacing="0" width="600" bgcolor="#FFFFFF" style="border: 1px #000000 solid" align="center">
	<tr>
		<td align="center">
			<table border="0" cellpadding="5" cellspacing="0">
				<tr>
				  <td align="center"><span class="style1">Main Menu </span></td>
			  </tr>
				<tr>
				  <td align="center">&nbsp;</td>
			  </tr>
				<tr>
					<td align="center">
						<table border="0" align="center" cellpadding="15" cellspacing="0">
							<tr valign="bottom">
								<? $shown=0;?>
								<? if($shown==4){?></tr><tr><? $shown=0;}?>
								<? if($_SESSION['allow_pms']==1){?><td width="100" align="center" valign="bottom"><a href="pmt_members.php"><img src="../images/management/icons/staff_icon_new.gif" alt="Staff" width="70" height="50" border="0" align="absmiddle"></a></td>
								<? $shown++;}?>
								<? if($shown==4){?></tr><tr><? $shown=0;}?>
								<? if($_SESSION['allow_rmc']==1){?><td width="100" align="center" valign="bottom"><a href="rmcs.php"><img src="../images/management/icons/rmcs_icon_new.gif" alt="Resident Management Companies" width="70" height="50" border="0" align="absmiddle"></a></td>
								<? $shown++;}?>
								<? if($shown==4){?></tr><tr><? $shown=0;}?>
								<? if($_SESSION['allow_residents']==1){?><td width="100" align="center" valign="bottom"><a href="residents.php"><img src="../images/management/icons/lessees_icon_new.gif" alt="Lessees" width="70" height="50" border="0" align="absmiddle"></a></td>
								<? $shown++;}?>
								<? if($shown==4){?></tr><tr><? $shown=0;}?>
								<? if($_SESSION['allow_users']==1){?><td width="100" align="center" valign="bottom"><a href="users.php"><img src="../images/management/icons/users_icon_new.gif" alt="Users" width="70" height="50" border="0" align="absmiddle"></a></td>
								<? $shown++;}?>
								<? if($shown==4){?></tr><tr><? $shown=0;}?>
								<? if($_SESSION['allow_letters_module']==1){?><td width="100" align="center" valign="bottom"><a href="letters.php"><img src="../images/management/icons/letters_icon_new.gif" alt="Letters" width="70" height="50" border="0" align="absmiddle"></a></td>
								<? $shown++;}?>
								<? if($shown==4){?></tr><tr><? $shown=0;}?>
								<? if($_SESSION['allow_report']==1){?><td width="100" align="center" valign="bottom"><a href="report.php"><img src="../images/management/icons/reports_icon_new.gif" alt="Reports" width="70" height="50" border="0" align="absmiddle"></a></td>
								<? $shown++;}?>
								<? if($shown==4){?></tr><tr><? $shown=0;}?>
								<? if($_SESSION['allow_developers']==1){?><td width="100" align="center" valign="bottom"><a href="developers.php"><img src="../images/management/icons/developers_icon_new.gif" alt="Developers" width="70" height="50" border="0" align="absmiddle"></a></td>
								<? $shown++;}?>
								<? if($shown==4){?></tr><tr><? $shown=0;}?>
								<? if($_SESSION['allow_announcements']==1){?><td width="100" align="center" valign="bottom"><a href="announcements.php"><img src="../images/management/icons/announce_icon.gif" alt="Announcements" width="70" height="50" border="0" align="absmiddle"></a></td>
								<? $shown++;}?>
								<? if($shown==4){?></tr><tr><? $shown=0;}?>
								<td width="100" align="center" valign="bottom"><a href="change_password.php"><img src="../images/management/icons/my_password_icon_new.gif" alt="My Password" width="90" height="50" border="0" align="absmiddle"></a></td>
								<? $shown++;?>
								<? if($shown==4){?></tr><tr><? $shown=0;}?>
								<? if($_SESSION['allow_contractor']==1){?><td width="100" align="center" valign="bottom"><a href="contractors.php"><img src="../images/management/icons/contractors_icon_new.gif" alt="Contractors" width="90" height="50" border="0" align="absmiddle"></a></td>
								<? $shown++;}?>
								<? if($shown==4){?></tr><tr><? $shown=0;}?>
								<td width="100" align="center" valign="bottom"><a href="/index.php?logoff=Y"><img src="../images/management/icons/log_off_icon_new.gif" alt="Log Off" width="90" height="50" border="0" align="absmiddle"></a></td>
								<? $shown++;?>
								</tr>
					  </table>
					</td>
				</tr>
				<tr><td height="10"></td></tr>
				<tr>
				  <td>If you need help with any aspect of this system, please contact <a href="mailto:customerservice@rmguk.com">customerservice@rmguk.com</a> </td>
				</tr>
			</table>
	  </td>
	</tr>
</table>
</body>
</html>