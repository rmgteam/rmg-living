<?

require("utils.php");
require_once($UTILS_CLASS_PATH."website.class.php");
$website = new website;

// Determine if allowed access into content management system
$website->allow_cms_access();


$news_id = $_REQUEST['news_id'];

if($_FILES['file_name']['tmp_name']){

	$parts = pathinfo($_FILES['file_name']['name']);
	$ext = $parts['extension'];
	$new_path = "../news_files/news_file_".$news_id."_".time().".".$ext;
	$new_name = "news_file_".$news_id."_".time().".".$ext;
	copy($_FILES['file_name']['tmp_name'],$new_path);
	
	$sql = "SELECT attached_file_names, attached_files FROM cpm_news WHERE news_id = $news_id";
	$result = mysql_query($sql);
	while($row = mysql_fetch_row($result)){
		$a_names = explode("<@>",$row[0]);
		$a_files = explode("<@>",$row[1]);
	}
	
	$a_names[] = $_REQUEST['display_name'];
	$a_files[] = $new_name;
	
	$new_a_names = join($a_names,"<@>");
	$new_a_files = join($a_files,"<@>");
	
	$sql = "UPDATE cpm_news SET attached_file_names='$new_a_names',attached_files='$new_a_files' WHERE news_id = $news_id";
	mysql_query($sql) || die($sql);
	?>
	<script language="javascript">
	opener.location.reload();
	window.close();
	</script>
	<?
	exit;
}



?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Add new file</title>
<link href="../styles.css" rel="stylesheet" type="text/css">
<script language="javascript">

function addFile(){
	if(document.forms[0].display_name.value==""){alert("Please enter a display name.");return;}
	if(document.forms[0].file_name.value==""){alert("Please select a file.");return;}
	document.forms[0].submit();
}

</script>
</head>

<body style="margin:10px">
<form enctype="multipart/form-data" method="post" action="add_news_file.php">
<table cellpadding="5" cellspacing="0" border="0">
	<tr><td colspan="2" align="center"><b>Add a new file</b></td></tr>
	<tr>
		<td align="right" nowrap>Display Name</td>
		<td><input type="text" name="display_name"></td>
	</tr>
	<tr>
		<td align="right">File</td>
		<td><input type="file" name="file_name"></td>
	</tr>
	<tr>
		<td align="right"></td>
		<td><input type="button" value="Add this file" onClick="addFile()"></td>
	</tr>
</table>
<input type="hidden" name="news_id" value="<?=$news_id?>">
</form>
</body>
</html>
