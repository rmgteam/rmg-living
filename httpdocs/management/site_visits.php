<?
require("utils.php");
require_once($UTILS_FILE_PATH."library/functions/check_file_extension.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."rmc.class.php");
$website = new website;
$rmc = new rmc;
$rmc->set_rmc($_REQUEST['rmc_num']);

// Determine if allowed access into content management system
$website->allow_cms_access();

// Check access privilege
if($_SESSION['allow_rmc'] != 1){header("Location:index.php");}

//=========================================
// Delete site visit file
//=========================================
if($_REQUEST['whichaction'] == "delete_site_visit"){

	$del_error = "N";

	$sql = "SELECT * FROM cpm_site_visits WHERE site_visit_id=".$_REQUEST['site_visit_file_id'];
	$result = @mysql_query($sql) or $del_error = "Y";
	$row = @mysql_fetch_array($result);
	$file_name = $row['site_visit_file'];

	$sql = "DELETE FROM cpm_site_visits WHERE site_visit_id=".$_REQUEST['site_visit_file_id'];
	@mysql_query($sql) or $del_error = "Y";
	
	if($del_error == "N"){
		@unlink($UTILS_SITE_VISITS_PATH.$file_name);
	}
	
}


#=========================================
# Add site visit file
#=========================================
if($_REQUEST['which_action'] == "add_site_visit"){

	# Check that site visit for this date doesn't already exist, otherwise warn user
	$sql = "SELECT * FROM cpm_site_visits WHERE rmc_num = ".$_REQUEST['rmc_num']." AND site_visit_date='".$_REQUEST['site_visit_date']."'";
	$result = @mysql_query($sql);
	$site_visit_exists = @mysql_num_rows($result);
	
	
	if($site_visit_exists < 1){
	
		$file_stamp = time();
	
		# Save site visit file to correct location
		$file_ext[] = "pdf";
		if(check_file_extension($_FILES['site_visit_file']['name'], $file_ext)){
			$site_visit_file_path = $UTILS_SITE_VISITS_PATH."site_visit_".$rmc->file_friendly_name()."_".$file_stamp.".pdf";
			$site_visit_file_name = "site_visit_".$rmc->rmc['rmc_ref']."_".$file_stamp.".pdf";
			copy($_FILES['site_visit_file']['tmp_name'], $site_visit_file_path);
		
			# Get date of site_visit
			$site_visit_date_parts = explode("/", $_REQUEST['site_visit_date']);
		
			# Insert site visit details into agm table
			$sql = "
			INSERT INTO cpm_site_visits(
				rmc_num,
				site_visit_file,
				site_visit_file_stamp,
				site_visit_date
			)
			VALUES(
				".$_REQUEST['rmc_num'].",
				'$site_visit_file_name',
				'".$file_stamp."',
				'".$site_visit_date_parts[2].$site_visit_date_parts[1].$site_visit_date_parts[0]."'
			)
			";
			mysql_query($sql) || die($sql);
		}
	}
}
?>
<html>
<head>
<link href="../styles.css" rel="stylesheet" type="text/css">
</head>
<body>
<? if($_REQUEST['which_action'] == "add_site_visit" && $site_visit_exists > 0){?>
<script language="javascript">
alert("An site visit file already exists for this date.  If you wish to replace it, please remove the existsing one first.");
</script>
<? }?>
<form action="site_visits.php">
<table border="0" cellpadding="3" cellspacing="0" width="100%">
<?
#===============================================
# Collect all site visits for this RMC
#===============================================
$sql = "SELECT * FROM cpm_site_visits WHERE rmc_num = ".$_REQUEST['rmc_num']." ORDER BY site_visit_date DESC";
$result = @mysql_query($sql);
$num_site_visits = @mysql_num_rows($result);
if($num_site_visits < 1){
	print "<tr><td>There are no site visit for this Man. Co.</td></tr>";
}
else{
	while($row = @mysql_fetch_array($result)){
		?>
		<tr bgcolor="#f3f3f3"><td style="border-bottom:1px solid #cccccc"><?=substr($row['site_visit_date'],6,2)."/".substr($row['site_visit_date'],4,2)."/".substr($row['site_visit_date'],0,4)?></td>
		  <td align="right" style="border-bottom:1px solid #cccccc"><input onClick="if(confirm('Remove site visit for this Man. Co. ?')){document.forms[0].site_visit_file_id.value='<?=$row['site_visit_id']?>';document.forms[0].submit();}" type="button" name="Button" value="remove"></td>
		</tr>
		<?
	}
}
?>
</table>
<input type="hidden" id="site_visit_file_id" name="site_visit_file_id">
<input type="hidden" id="rmc_num" name="rmc_num" value="<?=$_REQUEST['rmc_num']?>">
<input type="hidden" name="whichaction" value="delete_site_visit">
</form>
</body>
</html>