<?
require("utils.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."encryption.class.php");
$crypt = new encryption_class;
$website = new website;

// Determine if allowed access into content management system
$website->allow_cms_access();

// Check access privilege
if($_SESSION['allow_pms'] != 1){header("Location:index.php");}

// resize image
function image_createThumb($src,$dest,$maxWidth,$maxHeight,$quality=100){ 

	if(isset($dest)) {
		
		$destInfo  = pathinfo($dest);
		$srcSize  = getimagesize($src); 
		$srcRatio  = $srcSize[0]/$srcSize[1];
		if($srcSize[0]<$maxWidth){$maxWidth=$srcSize[0];}
		if($srcSize[1]<$maxHeight){$maxHeight=$srcSize[1];}
		$destRatio = $maxWidth/$maxHeight; 
		if ($destRatio > $srcRatio) { 
			$destSize[1] = ceil($maxHeight); 
			$destSize[0] = ceil($maxHeight*$srcRatio); 
		} 
		else { 
			$destSize[0] = ceil($maxWidth); 
			$destSize[1] = ceil($maxWidth/$srcRatio); 
		} 
		if ($destInfo['extension'] == "gif") { 
			$dest = substr_replace($dest, 'jpg', -3); 
		} 
		$destImage = imagecreatetruecolor($destSize[0],$destSize[1]);
		imageantialias($destImage,true); 
	
		switch ($srcSize[2]) { 
			case 1:
			$srcImage = imagecreatefromgif($src); 
			break; 

			case 2:
			$srcImage = imagecreatefromjpeg($src); 
			break; 
		
			case 3:
			$srcImage = imagecreatefrompng($src); 
			break; 
		
			default: 
			return false; 
			break; 
		} 
		imagecopyresampled($destImage, $srcImage, 0, 0, 0, 0,$destSize[0],$destSize[1],$srcSize[0],$srcSize[1]);

		switch ($srcSize[2]) { 
			case 1: break;
			case 2: 
			imagejpeg($destImage,$dest,$quality); 
			break; 
			
			case 3: 
			imagepng($destImage,$dest); 
			break; 
		} 
		return true; 
	} 
	else {return false;}
	return false;
}



#===================================
# Save new member
#===================================
if($_REQUEST['which_action'] == "add"){

	# Check for duplicate email address
	$is_duplicate = 0;
	$sql = "SELECT email FROM cpm_pmt_members WHERE email = '".$_REQUEST['email']."'";
	$result = @mysql_query($sql);
	$is_duplicate = @mysql_num_rows($result);
	
	if($is_duplicate < 1){
	
		// Get username from email address
		$username = explode("@", $_REQUEST['email']);
	
		// Insert new member
		$sql = "
		INSERT INTO cpm_pmt_members(
			username,
			first_name,
			last_name,
			email,
			tel,
			fax
		)
		VALUES(
			'".$username[0]."',
			'".addslashes($_REQUEST['first_name'])."',
			'".addslashes($_REQUEST['last_name'])."',
			'".$_REQUEST['email']."',
			'".$_REQUEST['tel']."',
			'".$_REQUEST['fax']."'
		)
		";
		@mysql_query($sql);
		
		# Get insert id
		$sql = "SELECT LAST_INSERT_ID() FROM cpm_pmt_members";
		$result = @mysql_query($sql);
		$row = @mysql_fetch_row($result);
		$_REQUEST['member_id'] = $row[0];
		
		
		# Gen password
		require($UTILS_FILE_PATH."management/gen_password.php");
		$password_unique = "N";
		while($password_unique == "N"){
			$password = get_rand_id(8, 'ALPHA', 'UPPER');
			$sql = "SELECT password FROM cpm_backend WHERE password = '".$crypt->encrypt($UTILS_DB_ENCODE, $password)."'";
			$result = @mysql_query($sql);
			$password_exists = @mysql_num_rows($result);
			if($password_exists < 1){
				$password_unique = "Y";
			}
		}

		
		// Set default backend privileges for team member
		$sql = "
		INSERT INTO cpm_backend(
			member_id,
			f_name,
			l_name,
			user_name,
			password,
			email,
			allow_rmc,
			allow_residents,
			allow_letters,
			allow_news,
			allow_passwords,
			allow_users,
			is_sys_admin
		)
		VALUES(
			'".$_REQUEST['member_id']."',
			'".addslashes($_REQUEST['first_name'])."',
			'".addslashes($_REQUEST['last_name'])."',
			'".$username[0]."',
			'".$crypt->encrypt($UTILS_DB_ENCODE, $password)."',
			'".$_REQUEST['email']."',
			1,
			1,
			1,
			0,
			1,
			0,
			'N'
		)
		";
		@mysql_query($sql);
		
		
# Email user/pass to member
$message = "Your login details for the RMG Living Management System are:
	
Username: ".$username[0]."
Password: $password
	
Access to the RMG Living Management System can be gained by logging into the RMG Living website:
http://www.rmgliving.co.uk
	

	
This email was generated automatically from the RMG Living Management System.
";
@mail($_REQUEST['email'],"RMG Living Login Details...",$message,"From: customerservice@rmguk.com\r\nReply-To: customerservice@rmguk.com");
		
			
		# Save staff photo
		if($_FILES['image_file']['tmp_name'] && $_FILES['image_file']['tmp_name'] != ""){
			$file_path = "../pmt_photos/image_".$_REQUEST['member_id'].".jpg";
			$temp_name = $_FILES['image_file']['tmp_name'];
			image_createThumb($temp_name,$file_path,80,80);
		}
		
	}
	else{
		$error_message = "<tr><td colspan=2 style='color:#990000'><b>Duplicate email address! Action cancelled!</b></td></tr><tr><td colspan=2 height=10></td></tr>";
	}
}

#===================================
# Save exisiting member
#===================================
if($_REQUEST['which_action'] == "modify"){

	# Check for duplicate email address
	$sql = "SELECT email FROM cpm_pmt_members WHERE email = '".$_REQUEST['email']."' AND member_id != ".$_REQUEST['member_id'];
	$result = @mysql_query($sql);
	$is_duplicate = @mysql_num_rows($result);
	
	if($is_duplicate < 1){
	
		// Get username from email address
		$username = explode("@", $_REQUEST['email']);
		
		// Update member record
		$sql = "
		UPDATE cpm_pmt_members SET
		username = '".$username[0]."',
		first_name = '".$_REQUEST['first_name']."',
		last_name = '".$_REQUEST['last_name']."',
		email = '".$_REQUEST['email']."',
		tel = '".$_REQUEST['tel']."',
		fax = '".$_REQUEST['fax']."'
		WHERE member_id = ".$_REQUEST['member_id'];
		@mysql_query($sql);		
		
		if ($_FILES['image_file']['tmp_name'] && $_FILES['image_file']['tmp_name'] != ""){
			$file_path = "../pmt_photos/image_".$_REQUEST['member_id'].".jpg";
			$temp_name = $_FILES['image_file']['tmp_name'];
			image_createThumb($temp_name,$file_path,80,80);
		}
	
	}
	else{
		$error_message = "<tr><td colspan=2 style='color:#990000'><b>Duplicate email address! Action cancelled!</b></td></tr><tr><td colspan=2 height=10></td></tr>";
	}
	
}

//===================================
// Remove staff member from system
//===================================
if($_REQUEST['which_action'] == "remove"){

	// First check to see if this staff member is assigned to active RMC's
	$sql_ac = "SELECT lrmc.rmc_ref, rmc.rmc_name FROM cpm_lookup_rmcs lrmc, cpm_rmcs rmc, cpm_rmcs_extra rex WHERE lrmc.rmc_lookup=rmc.rmc_num AND rmc.rmc_num=rex.rmc_num AND (rmc.rmc_status = 'Active' OR rmc.rmc_is_active = '1') AND rex.member_id = ".$_REQUEST['member_id'];
	$result_ac = @mysql_query($sql_ac);
	$num_managed = @mysql_num_rows($result_ac);
	if($num_managed > 0){
	
		while($row_ac = @mysql_fetch_row($result_ac)){
			$rmcs_managed .= "\\n(".$row_ac[0].") ".$row_ac[1];
		}
	
		print "
		<script language=javascript>
		alert(\"This Property Manager is currently assigned to the Management Companies below. You must first re-assign new Property Managers to these companies before you can delete this Property Manager:\\n$rmcs_managed\");
		</script>
		";
	}
	else{
	
		$sql = "DELETE FROM cpm_pmt_members WHERE member_id = ".$_REQUEST['member_id'];
		@mysql_query($sql);
		
		$sql = "DELETE FROM cpm_backend WHERE member_id = ".$_REQUEST['member_id'];
		@mysql_query($sql);
		
		if(is_file("../pmt_photos/image_".$_REQUEST['member_id'].".jpg")){
			unlink("../pmt_photos/image_".$_REQUEST['member_id'].".jpg");
		}
		$_REQUEST['member_id'] = "";
		
	}

}

//===================================
// Get details of staff member
//===================================
if($_REQUEST['member_id'] != "" ){
	
	$sql_member = "SELECT * FROM cpm_pmt_members WHERE member_id = ".$_REQUEST['member_id'];
	$result_member = @mysql_query($sql_member);
	$row_member = @mysql_fetch_array($result_member);

	if($row_member['manager'] == "1"){
		if(is_file("../pmt_photos/image_".$_REQUEST['member_id'].".jpg")){
			$image = "<img style='border:1px solid #666666;' src='../pmt_photos/image_".$_REQUEST['member_id'].".jpg'>";
		}
		else{
			$image = "<img style='border:1px solid #666666;' src='../images/image_anonymous.jpg'>";
		}
	}
}


?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>RMG Living - Staff</title>
<link href="../styles.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function save(){

	var bad=0;

	if(document.forms[0].first_name.value==""){bad=1;}
	if(document.forms[0].last_name.value==""){bad=1;}
	if(document.forms[0].email.value==""){bad=1;}
	if(document.forms[0].tel.value==""){bad=1;}

	if(bad==1){alert("Please enter a value in all fields marked with *");return;}

	if(document.forms[0].member_id.value==""){
		if(!confirm("Save this staff member to the database?")){return;}
		document.forms[0].which_action.value="add";
	}
	else{
		if(!confirm("Save your changes?")){return;}
		document.forms[0].which_action.value="modify";
	}
	document.forms[0].submit();
}

function remove(){

	if(document.forms[0].member_id.value == ""){alert("Please select which Property Manager you wish to remove.");return;}
	if(!confirm("Remove this Property Manager?")){return;}
	document.forms[0].which_action.value="remove";
	document.forms[0].submit();
}

function changeMember(id){
	location.href = "pmt_members.php?member_id=" + id;
}

function view_properties(member_id){
	open("<?=$UTILS_HTTPS_ADDRESS?>management/properties_managed.php?member_id=" + member_id,"VIEWPROP", "width=400, height=400, scrollbars=yes, menubar=yes, status=yes, resizable=yes");
}
//-->
</script>
</head>
<body class="management_body">
<form method="post" action="pmt_members.php" enctype="multipart/form-data">
<? require($UTILS_FILE_PATH."management/menu.php");?>
<table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr valign="top">
	  <td><table width="100%" border="0" cellpadding="10" cellspacing="0"  bgcolor="#FFFFFF" style="border: 1px #000000 solid">
        <tr>
          <td align="center"><img src="../images/management/icons/staff_icon_new.gif" width="70" height="50" border="0"></td>
        </tr>
        <tr>
          <td align="center"><a href="index.php"><b><< Back to main menu</b></a></td>
        </tr>
      </table></td>
    </tr>
	<tr valign="top">
	  <td>&nbsp;</td>
    </tr>
	<tr valign="top">
	  <td><table width="500" border="0" cellpadding="10" cellspacing="0"  bgcolor="#eaeaea" style="border: 1px #999999 solid">
        <tr>
          <td><strong>Property Management Staff </strong></td>
        </tr>
      </table></td>
    </tr>
	<tr valign="top">
		<td>
			<table width="100%" border="0" cellpadding="10" cellspacing="0" bgcolor="#FFFFFF" style="border: 1px #999999 solid" align="center">
				<tr>
					<td>
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr valign="top">
								<td>
									<table width="100%" border="0" cellpadding="3" cellspacing="0">
										<?=$error_message?>
										<tr>
											<td width="24%">Property Manager </td>
											<td width="76%">
												<select name="member_id" id="member_id" onChange="changeMember(this.value)">
													<? if($m_id == ""){?>
													<option value="">ADD NEW TEAM MEMBER</option>
													<? }else{?>
													<option value="">-</option>
													<?
													}
													$sql = "SELECT member_id, first_name, last_name FROM cpm_pmt_members ORDER BY first_name ASC";
													$result = @mysql_query($sql);
													while($row = @mysql_fetch_row($result)){
													?>
														<option value="<?=$row[0]?>" <? if($_REQUEST['member_id'] == $row[0]){?>selected<? }?>><?=stripslashes($row[1]." ".$row[2])?></option>
													<?
													}
													?>
												</select>
											</td>
										</tr>
										<tr>
										  <td>&nbsp;</td>
										  <td>&nbsp;</td>
									  </tr>
									 
										<tr>
											<td>First name *</td>
											<td><input name="first_name" type="text" style="width:150px" value="<?=$row_member['first_name']?>" maxlength="35"></td>
										</tr>
										<tr>
										  <td>Last name *</td>
										  <td><input name="last_name" type="text" style="width:150px" value="<?=stripslashes($row_member['last_name'])?>" maxlength="35"></td>
									  </tr>
										<tr>
										  <td>Email address * </td>
										  <td><input name="email" type="text" value="<?=$row_member['email']?>" size="40"></td>
									  </tr>
										<tr>
										  <td nowrap>Telephone No. * </td>
										  <td><input name="tel" type="text" style="width:150px" value="<?=$row_member['tel']?>" maxlength="35"></td>
									  </tr>
										<tr>
										  <td>Fax</td>
										  <td><input name="fax" type="text" style="width:150px" value="<?=$row_member['fax']?>" maxlength="35"></td>
									  </tr>
										<tr>
											<td>Photo</td>
											<td><input type="file" name="image_file"><br>(64 px W x 80 px H - .jpeg format only)</td>
										</tr>
										<tr id="photo_cell" style="display:<? if($row_member['manager'] == 1){?>block<? }else{?>none<? }?>">
										  <td></td>
										  <td><?=$image?></td>
									  </tr>
										<tr>
										  <td>&nbsp;</td>
									      <td>&nbsp;</td>
									  </tr>
										<tr>
										  <td colspan="2">
												<input type="button" value="Save" onClick="save()">
												<input <? if(!$_REQUEST['member_id']){?>disabled<? }?> type="button" value="Remove" onClick="remove()">
												<input <? if(!$_REQUEST['member_id']){?>disabled<? }?> type="button" value="View properties..." onClick="view_properties(<?=$_REQUEST['member_id']?>)">
												
										  </td>
										</tr>
										
									</table>
								</td>
								
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="which_action" id="which_action">
</form>
</body>
</html>
