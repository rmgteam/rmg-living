<?
require_once("utils.php");
require_once($UTILS_SERVER_ROOT."library/functions/get_file_size.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."rmc.class.php");
require_once($UTILS_CLASS_PATH."resident.class.php");
require_once($UTILS_CLASS_PATH."unit.class.php");
require_once($UTILS_CLASS_PATH."data.class.php");
$resident = new resident($_SESSION['resident_num']);
$rmc = new rmc;
$data = new data;
$rmc->set_rmc($_SESSION['rmc_num']);
$unit = new unit;
$unit->set_unit($_SESSION['resident_num']);
$website = new website;
$thistime = time();

// Determine if allowed access into 'your community' section
$website->allow_community_access();

// Do not allow for Ground Rent only properties...
if($rmc->is_ground_rent_only() === true && $resident->is_subtenant_account == "N"){
	header("Location: building_details.php");
	exit;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>RMG Living - Insurance Schedules</title>
	<link href="/css/reset.css" rel="stylesheet" type="text/css" />
	<link href="styles.css" rel="stylesheet" type="text/css" />
	<link href="/css/common.css" rel="stylesheet" type="text/css" />
	<link href="/css/custom-theme/jquery-ui-1.8.16.custom.css" rel="stylesheet" type="text/css" />
	<!--[if lte IE 8]> 
	<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<!--[if lte IE 7]> 
	<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<script type="text/javascript" language="JavaScript" src="/library/jscript/jquery-1.6.2.min.js"></script>

	<script type="text/javascript" language="JavaScript" src="/library/jscript/jquery-ui-1.8.16.custom.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {

		l_height = $("#content_left").height();
		r_height = $("#content_right").height();

		if(l_height < r_height){
			$("#content_left").height(r_height - 30);
		}
		else{
			$("#content_right").height(l_height + 30);
		}

		/*
		$("#processing_dialog").dialog({
			modal: true,
			autoOpen: false
		});
		*/

		$("#msg_dialog").dialog({
			modal: true,
			autoOpen: false,
			width: 350
		});

		$("#card_type_dialog").dialog({
			modal: true,
			autoOpen: false,
			width: 350
		});

		$("#freeholder_dialog").dialog({
			modal: true,
			autoOpen: false
		});

	});


	function show_freeholder(fs){

		$("#freeholder_dialog").dialog('close');
		$.ajax({
			type: "POST",
		  	url: "/insur_sched/insur_sched_get_freeholder_info.php",
		  	data: { s: fs },
		  	dataType: "json",
		  	success: function(data){

		  		if(data['get_result'] == "success"){

					$("#freeholder_address").html(data['freeholder_address']);
					$("#freeholder_dialog").dialog('open');
				}
		  	}
		});
	}


	function mark_downloaded(ud, ins_id, lookup_id, ins_s, oid, rmc_num, resident_num, owner_id){

		$.ajax({
			type: "POST",
		  	url: "/insur_sched/insur_sched_mark_download.php",
		  	data: { ud: ud, id: ins_id, lid: lookup_id, oid: oid, rmc_num: rmc_num, rid: resident_num, owner_id: owner_id },
		  	dataType: "json",
		  	success: function(data){

				file_url = "<?=$UTILS_HTTP_ADDRESS?>show_file.php?owid=" + owner_id + "&ud=" + ud + "&type=ins_sched&s=" + ins_s;

				if(data['save_result'] == "success"){
					window.open(file_url);
				}
				else{
		  			alert("There was a problem retrieving this Insurance Schedule.");
				}
		  	}
		});
	}


	function pre_pay_q(ist, isi, lookup_id, rmc_num, resident_num, owner_id){

		$("#msg_dialog_message").html("Are you requesting this as part of a sale or subletting transaction?");
		$("#msg_dialog").dialog({
			buttons: [
				{
					text: "Yes",
					click: function() {

						$("#msg_dialog_message").html("In order to ensure that you are not in breach of your lease and possibly incur additional costs you should contact our Solicitor Enquiries Department on either <?=$UTILS_TEL_MAIN;?> or <a href='mailto:sed@rmguk.com'>sed@rmguk.com</a> to obtain a full information pack.");
						$(this).dialog({
							buttons: [
								{
									text: "Close",
									click: function() {	$(this).dialog('close');}
								}
							]
						});
					}
				},
				{
					text: "No",
					click: function() {

						$(this).dialog("close");

						$("#card_type_dialog").dialog({

							buttons: [
								{
									text: "Continue",
									click: function() {

										if( $("#card_type").val() != "" ){

											$(this).dialog("close");
											do_pay(ist, isi, lookup_id, rmc_num, resident_num, owner_id);
										}
									}
								},
								{
									text: "Cancel",
									click: function() {

										$(this).dialog("close");
									}
								}
							]
						});

						$("#card_type_dialog").dialog('open');
					}
				}
			]
		});
		$("#msg_dialog_message").show();
		$("#msg_dialog").dialog('open');

	}



	function do_pay(ist, isi, lookup_id, rmc_num, resident_num, owner_id){

		location.href = "/op/paypoint/s1_insur_sched.php?card_type=" + $("#card_type").val() + "&isi=" + isi + "&ist=" + ist + "&lookup_id=" + lookup_id + "&rmc_num=" + rmc_num + "&resident_num=" + resident_num + "&owner_id=" + owner_id;
	}
	</script>
	<? require_once($UTILS_FILE_PATH."includes/analytics.php");?>
</head>
<body>

	<div id="wrapper">
	
		<? require_once($UTILS_FILE_PATH."includes/header.php");?>
		<? require_once($UTILS_FILE_PATH."includes/msg_dialog.php");?>
		<? require_once($UTILS_FILE_PATH."includes/card_type_dialog.php");?>
		<? require_once($UTILS_FILE_PATH."includes/freeholder_dialog.php");?>
		
		<div id="content">
		
			<table width="760" cellspacing="0">
				<tr>
					<td><a href="/building_details.php" class="crumbs">Your Community</a>&nbsp;&gt;&nbsp;Insurance Schedules</td>
					<td style="text-align:right;" nowrap="nowrap"><? if(!empty($_SESSION['resident_session'])){?><a href="index.php?logoff=Y" class="crumbs">Log Off</a><? }?></td>
				</tr>
			</table>

				
			<table width="760" cellspacing="0" cellpadding="0" style="margin-top:12px;">
				<tr>
					<td width="15"><img src="images/dblue_box_top_left_corner.jpg" width="15" height="33" /></td>
					<td width="730" style="background-color:#416CA0;border-top:1px solid #003366; vertical-align:middle"><img src="images/meetings/meetings_symbol.jpg" width="22" height="22" style="vertical-align:middle;">&nbsp;&nbsp;<span class="box_title">Insurance Schedules </span></td>
					<td width="15" align="right"><img src="images/dblue_box_top_right_corner.jpg" width="15" height="33" /></td>
				</tr>
			</table>
						
			
			<div class="content_box_1 clearfix" style="padding:0 0 15px 0;width:758px; vertical-align:top;">
								
				<div id="content_left" style="float:left;width:377px;padding:15px; vertical-align:top;">
					
					<p>Below is a list of Insurance Schedules that are applicable to your Management Company. You are granted 1 FREE download of each schedule. For subsequent downloads a charge of &pound;30 + VAT applies. All purchased schedules will be available to download for a period of 48 hours after the purchase was made.</p>
					
					<h6 style="margin-top:20px;">Schedules available</h6>
					<table width="475" cellspacing="0" class="table_2" style="margin-top:4px;">
					
						<?
						// Check if there are any schedules that can be downloaded...
						$num_ins_sched = 0;
						if($resident->is_resident_director == "Y"){
							$num_ins_sched = $resident->get_num_director_ins_sched();
						}
						else{
							$num_ins_sched = $unit->get_num_unit_ins_sched();
						}
						if( $num_ins_sched > 0 ){
						
							?>
							<tr>			
								<th>Schedule</th>
								<th>Period</th>
								<th class="end">&nbsp;</th>
							</tr>
							
							<?
							if($resident->is_resident_director != "Y"){
								
								// List Unit-related insurance schedules available for download by this person...
								$result_unit_ins = $unit->get_unit_ins_sched();
								while($row_unit_ins = @mysql_fetch_array($result_unit_ins)){
									
									?>
									<tr>
										<td><?=stripslashes($row_unit_ins['ins_sched_type_label'])?><?php if( $row_unit_ins['ins_sched_block_label'] != "" ){print " (".stripslashes($row_unit_ins['ins_sched_block_label']).")";} ?></td>
										<td><?=$data->ymd_to_date($row_unit_ins['ins_sched_period_from_ymd'])?> to <?=$data->ymd_to_date($row_unit_ins['ins_sched_period_to_ymd'])?></td>
										<td class="end">
										
											<?
											if($row_unit_ins['ins_sched_freeholder_to_insure'] == "Y"){
												?>											
												<a href="Javascript:;" onclick="show_freeholder('<?=$row_unit_ins['freeholder_serial']?>');return false;" class="link416CA0" style="text-decoration:underline;">More info...</a>
												<?
											}
											else{
												
												$num_ord = 0;
												$num_sched_downloads = $unit->get_num_sched_downloads($row_unit_ins['rmc_num'], $row_unit_ins['ins_sched_id'], $row_unit_ins['unit_num'], $row_unit_ins['int_resident_num']);
												if( $num_sched_downloads > 0 ){
													$result_ord = $unit->get_available_sched_downloads($row_unit_ins['rmc_num'], $row_unit_ins['ins_sched_id'], $row_unit_ins['unit_num'], $row_unit_ins['int_resident_num']);
													$num_ord = @mysql_num_rows($result_ord);
												}
												if( $num_sched_downloads == 0 || $num_ord > 0 ){
													
													$row_ord = @mysql_fetch_array($result_ord);
													
													?>
													<a href="Javascript:;" onclick="mark_downloaded('u','<?=$row_unit_ins['ins_sched_id']?>','<?=$row_unit_ins['unit_num']?>','<?=$row_unit_ins['ins_sched_serial']?>','<?=$row_ord['order_ref']?>','<?=$row_unit_ins['rmc_num']?>','<?=$row_unit_ins['int_resident_num']?>','<?=$row_unit_ins['owner_id']?>');return false;" style="text-decoration:underline; color:#C00;">Download</a><br /><?="(".get_file_size($UTILS_INS_SCHED_PATH.$row_unit_ins['ins_sched_filename']).")"?>
													<?
												}else{
													?>
													<a href="Javascript:;" onclick="pre_pay_q('u','<?=$row_unit_ins['ins_sched_id']?>','<?=$row_unit_ins['unit_num']?>','<?=$row_unit_ins['rmc_num']?>','<?=$row_unit_ins['int_resident_num']?>','<?=$row_unit_ins['owner_id']?>');return false;" style="text-decoration:underline;color:#C00;">Purchase...</a>
													<?
												}
											}
											?>
										
										</td>			
									</tr>
									<?
								}
							}
							else{
								
								// List Director-related insurance schedules available for download by this person...
								$result_director_ins = $resident->get_director_ins_sched();
								while($row_director_ins = @mysql_fetch_array($result_director_ins)){
									
									?>
									<tr>
										<td><?=stripslashes($row_director_ins['ins_sched_type_label'])?><?php if( $row_director_ins['ins_sched_block_label'] != "" ){print " (".stripslashes($row_director_ins['ins_sched_block_label']).")";} ?></td>
										<td><?=$data->ymd_to_date($row_director_ins['ins_sched_period_from_ymd'])?> to <?=$data->ymd_to_date($row_director_ins['ins_sched_period_to_ymd'])?></td>
										<td class="end">
										
											<? 
											if($row_director_ins['ins_sched_freeholder_to_insure'] == "Y"){
												?>											
												<a href="Javascript:;" onclick="show_freeholder('<?=$row_director_ins['freeholder_serial']?>');return false;" class="link416CA0" style="text-decoration:underline;">More info...</a>
												<?
											}
											else{

												$owner_id = $row_director_ins['i_oid'];
												if($owner_id == ""){
													$owner_id = $row_director_ins['r_oid'];
												}
												
												$num_ord = 0;
												$num_sched_downloads = $resident->get_num_sched_downloads($row_director_ins['owner_id'], $row_director_ins['rmc_num'], $row_director_ins['ins_sched_id'], $row_director_ins['int_director_id']);
												if( $num_sched_downloads > 0 ){
													$result_ord = $resident->get_available_sched_downloads($row_director_ins['owner_id'], $row_director_ins['rmc_num'], $row_director_ins['ins_sched_id'], $row_director_ins['int_director_id']);
													$num_ord = @mysql_num_rows($result_ord);
												}
												if( $num_sched_downloads == 0 || $num_ord > 0 ){
													
													$row_ord = @mysql_fetch_array($result_ord);
													
													?>
													<a href="Javascript:;" onclick="mark_downloaded('d','<?=$row_director_ins['ins_sched_id']?>','<?=$row_director_ins['int_director_id']?>','<?=$row_director_ins['ins_sched_serial']?>','<?=$row_ord['order_ref']?>','<?=$row_director_ins['rmc_num']?>','','<?=$owner_id?>');return false;" style="text-decoration:underline;color:#C00;">Download</a><br /><?="(".get_file_size($UTILS_INS_SCHED_PATH.$row_director_ins['ins_sched_filename']).")"?>
													<?
												}else{
													?>
													<a href="Javascript:;" onclick="pre_pay_q('d','<?=$row_director_ins['ins_sched_id']?>','<?=$row_director_ins['int_director_id']?>','<?=$row_director_ins['rmc_num']?>','','<?=$owner_id?>');return false;" style="text-decoration:underline;color:#C00;">Purchase...</a>
													<?
												}
											}
											?>
											
										</td>
									</tr>
									<?
								}
							}							
						}
						else{
							?>
							<tr>
								<td colspan="3" class="end" style="height:30px; border:none;"><img src="images/about_16.gif" width="16" height="16" style="vertical-align:middle;">&nbsp;No Insurance Schedules found</td>
							</tr>
							<?
						}
						?>
						
					</table>
					
					
					<h6 style="margin-top:20px;">Order History</h6>
					<table width="475" cellspacing="0" class="table_2" style="margin-top:4px;">
						<tr>
							<th>Order Ref.</th>
							<th>Order Date</th>
							<th>Schedule</th>
							<th>Period</th>
							<th class="end" style="width:55px; text-align:right;">Price (&pound;)</th>
						</tr>
						<?
						// List order history
						$sql_ord = "
						(
						SELECT d.order_ref, t.ins_sched_type_label, i.ins_sched_period_from_ymd, i.ins_sched_period_to_ymd, d.order_ts   
						FROM ins_sched_download_log d, lookup_resident lres, ins_sched i, subsidiary s, ins_sched_types t   
						WHERE 
						lres.subsidiary_id = s.subsidiary_id AND 
						i.ins_sched_type_id = t.ins_sched_type_id AND 
						d.resident_num = lres.resident_lookup AND 
						d.ins_sched_id = i.ins_sched_id AND 
						s.subsidiary_code <> '' AND 
						s.subsidiary_code IS NOT NULL AND 
						s.subsidiary_code = '".$rmc->subsidiary_code."' AND 
						lres.resident_ref <> '' AND 
						lres.resident_ref IS NOT NULL AND 
						lres.resident_ref = '".$resident->resident_ref."' AND  
						d.order_ref <> '' AND 
						d.order_ref NOT LIKE 'CREDIT_%' AND 
						d.order_ref IS NOT NULL 
						)
						UNION 
						(
						SELECT d.order_ref, t.ins_sched_type_label, i.ins_sched_period_from_ymd, i.ins_sched_period_to_ymd, d.order_ts     
						FROM ins_sched_download_log d, ins_sched i, ins_sched_types t, owner o, directors dr, subsidiary s 
						WHERE 
						d.owner_id = o.owner_id AND 
						o.subsidiary_id = s.subsidiary_id AND 
						s.subsidiary_code = '".$rmc->subsidiary_code."' AND 
						d.director_id = dr.director_id AND 
						dr.director_ref = '".$resident->resident_ref."' AND 
						i.ins_sched_type_id = t.ins_sched_type_id AND 
						d.ins_sched_id = i.ins_sched_id AND 
						d.order_ref <> '' AND 
						d.order_ref NOT LIKE 'CREDIT_%' AND 
						d.order_ref IS NOT NULL 
						)
						ORDER BY order_ts DESC 
						";
						//print $sql_ord;
						$result_ord = @mysql_query($sql_ord, $UTILS_INTRANET_DB_LINK);
						$num_history = @mysql_num_rows($result_ord);
						if($num_history > 0){
							
							while( $row_ord = @mysql_fetch_row($result_ord) ){
								
								// Get order info.
								$sql_oi = "SELECT * FROM cpm_orders WHERE order_ref = '".$row_ord[0]."'";
								$result_oi = @mysql_query($sql_oi);
								$row_oi = @mysql_fetch_array($result_oi);
							
								?>
								<tr>
									<td><?=$row_ord[0]?></td>
									<td><?=date("d/m/Y",$row_oi['order_TS'])?></td>
									<td><?=stripslashes($row_ord[1])?></td>
									<td><?=$data->ymd_to_date($row_ord[2])." to ".$data->ymd_to_date($row_ord[3])?></td>
									<td class="end" style="text-align:right;"><?=number_format($row_oi['order_total'],2,".",",")?></td>
								</tr>
								<?
							}
						}
						else{
							?>
							<tr>
								<td class="end" colspan="5">You do not have any order history.</td>
							</tr>							
							<?	
						}
						?>
					</table>
					
					
				</div>
	
				<div id="content_right" style="vertical-align:top;width:237px; min-height:400px;float:right;vertical-align:top;background-image:url(/images/ins_sched_folders.jpg);border-left:1px solid #eaeaea;background-repeat:no-repeat;padding:0;">
						
					<table cellspacing="0" style="margin-top:200px;margin-right:20px;margin-left:15px;">
						<tr>
							<td style="vertical-align:top;">These documents can be viewed using Adobe Reader.<br /><a href="<?=$UTILS_ADOBE_URL?>" target="_blank">Click to download</a>. </td>
							<td style="width:90px;text-align:right;vertical-align:top;"><a href="<?=$UTILS_ADOBE_URL?>" target="_blank"><img src="images/getacrobat.gif" alt="Get Adobe Reader" width="88" height="31" border="0" style="vertical-align:middle;"></a></td>
						</tr>
					</table>
					
				</div>
			
			</div>

		</div>
	
		<? require_once($UTILS_FILE_PATH."includes/footer.php");?>

	</div>

</body>
</html>
