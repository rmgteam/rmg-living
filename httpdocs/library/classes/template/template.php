<?php

namespace template;

class template
{
	var $vars = array(); /// Holds all the template variables
	var $file;
	var $main_content = '';
	var $var_content = '';

	/**
	 * Constructor
	 *
	 * @param $file string the file name you want to load
	 */
	function __construct( $file = null )
	{
		$this->file = $file;
	}

	/**
	 * Set a template variable.
	 *
	 * @param    string $name
	 * @param    string $value
	 */
	function set( $name, $value )
	{
		$this->vars[$name] = $value;
	}

	/**
	 * Set a template variable.
	 *
	 * @param    string $name
	 */
	function get( $name )
	{
		return $this->vars[$name];
	}

	/**
	 * Parse a variable array
	 * Keeps adding to array with every call
	 *
	 * @param    array $value
	 */
	function parse( $value )
	{
		if ( is_array( $value ) ) {
			$this->vars = array_merge( $this->vars, $value );
		}
	}

	/**
	 * Open, parse, and return the template file.
	 *
	 * @param    string $file the template file name
	 * @throws    \Exception
	 * @return    string        $contents
	 */
	function fetch( $file = null )
	{

		$this->vars['template'] = $this;

		if ( !$file ) {
			$file = $this->file;
			if ( $this->main_content != '' && $this->var_content != '' ) {
				$this->getFile( $this->main_content, $this->var_content );
			}
		}

		if ( is_file( $file ) ) {

			extract( $this->vars );          // Extract the vars to local namespace
			ob_start();                    // Start output buffering
			include( $file );                // Include the file
			$contents = ob_get_contents(); // Get the contents of the buffer
			ob_end_clean();                // End buffering and discard
			return $contents;              // Return the contents
		}
		throw new \Exception ( $file . " not found" );
	}

	/**
	 * Open, parse, and set variable with the file content.
	 *
	 * @param    string $filename the template file name
	 * @param    string $variable the string to be set with the file contents
	 */
	function getFile( $filename, $variable )
	{
		$this->set( $variable, $this->fetch( $filename ) );
	}

	/**
	 * Prepare Includes - turns all files in includes folder in to variables to use in template
	 *
	 * @param    string $folder Start Folder
	 * @param    string $prefix Variable prefix
	 * @throws    \Exception
	 */
	function prepareIncludes( $folder = "", $prefix = "" )
	{
		if ( $folder == '' ) {
			$folder = $GLOBALS['paths']['server'] . 'includes/';
		}

		$dir = new \DirectoryIterator( $folder );

		foreach ( $dir as $fileInfo ) {

			if ( !$fileInfo->isDot() ) {

				if ( $fileInfo->isFile() ) {

					$filename = $fileInfo->getFilename();

					$ext = explode( '.', $filename );
					$ext = "." . $ext[count( $ext ) - 1];

					$file = str_replace( $ext, "", $filename );
					$this->set( $prefix . ucwords( $file ), $this->fetch( $folder . $filename ) );
				} else {
					if( $fileInfo->getFilename() != 'graph' )
					$this->prepareIncludes( $folder . $fileInfo->getFilename() . '/', $prefix . $fileInfo->getFilename() );
				}
			}
		}
	}

	/**
	 * Get Include
	 * replaces {{ID}} in include
	 *
	 * @param    string $name Variable Name of template
	 * @param    string $id   id attribute
	 */
	function includeTemplate( $name, $id )
	{
		$templateValue = $this->get( $name );
		$templateValue = str_replace( "{{ID}}", $id, $templateValue );
		print( $templateValue );
	}
}

?>