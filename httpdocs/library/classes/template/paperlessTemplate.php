<?
namespace template;

require_once("utils.php");
require_once($UTILS_CLASS_PATH."template/template.php");

class paperlessTemplate extends template {
	function __construct( $file = null ) {
		Global $UTILS_FILE_PATH;
		parent::__construct($UTILS_FILE_PATH . 'paperless/designs/body.tpl');
		$this->getFile( $UTILS_FILE_PATH . 'includes/chat.php', 'chat' );
	}

	function setPage( $page, $title ){
		Global $UTILS_FILE_PATH;
		$this->getFile($UTILS_FILE_PATH . 'paperless/designs/'.$page.'.tpl', 'content');
		$script = $page . '.js';
		$this->parse( get_defined_vars() );
	}
}