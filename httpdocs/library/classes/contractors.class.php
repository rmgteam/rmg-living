<?
require_once($UTILS_FILE_PATH."management/gen_password.php");
require_once($UTILS_CLASS_PATH."field.class.php");
require_once($UTILS_CLASS_PATH."data.class.php");
require_once($UTILS_CLASS_PATH."encryption.class.php");

class contractors {
	
	var $contractor_qube_id;
	var $contractor_name;
	var $contractor_disabled;
	var $contractor_email;
	var $contractor_address;
	var $contractor_pcm;
	
	function contractors($contractor_ref){
		
		$this->contractor_qube_id = '';
		$this->contractor_name = '';
		$this->contractor_disabled = '';
		$this->contractor_email = '';
		$this->contractor_address = '';
		$this->contractor_pcm = '';
		
		$sql = "
		SELECT * 
		FROM cpm_contractors
		WHERE
		cpm_contractors_qube_id = '".$contractor_ref."'";
		$result = @mysql_query($sql);
		$num_results = @mysql_num_rows($result);
		if($num_results > 0){			
			while($row = @mysql_fetch_array($result)){	
				$this->contractor_qube_id = $row['cpm_contractors_qube_id'];
				$this->contractor_name = $row['cpm_contractors_name'];
				$this->contractor_disabled = $row['cpm_contractors_disabled'];
				$this->contractor_email = $row['cpm_contractors_email'];
				$this->contractor_address = $row['cpm_contractors_address'];
				$this->contractor_pcm = $row['cpm_contractors_pcm'];
			}
		}
	}
	
	function contractor_list($request){
		$sql = "SELECT *
		FROM cpm_contractors
		WHERE (";
		
		$search_term = explode(" ", $request['search_term']);
		
		foreach ($search_term as &$term){
			$sql .= "cpm_contractors_name LIKE '%".$term."%' OR ";
			$sql .= "cpm_contractors_qube_id LIKE '%".$term."%' OR ";
		}
		
		$sql = substr($sql,0,-3);
		
		$sql .=")";
		
		$result = @mysql_query($sql);
		
		return $result;
	}
	
	function get_users($contractor_ref){
		$sql = "SELECT * 
		FROM cpm_contractors_user 
		WHERE cpm_contractors_user_qube_ref = '".$contractor_ref."'";
		
		$result = @mysql_query($sql);
		
		return $result;
	}
	
	function get_user($user_ref){
		$sql = "
		SELECT u.*, 
		IFNULL(
			(
				SELECT t.cpm_contractors_user_trail_login
				FROM cpm_contractors_user_trail t
				WHERE t.cpm_contractors_user_trail_user_ref = u.cpm_contractors_user_ref
				ORDER BY t.cpm_contractors_user_trail_id DESC
				LIMIT 1
			),
			''
		)as last_login
		FROM cpm_contractors_user u
		WHERE
		u.cpm_contractors_user_ref = '".$user_ref."'";
		
		$result = @mysql_query($sql);
		
		return $result;
	}
	
	function check_user($contractor_ref, $request){
		$field = new field;
		$err_message = "";
	
		if($request['name'] == ""){
			$err_message .= "&bull; Please enter a name<br />";
		}
		
		if($request['email'] == ""){
			$err_message .= "&bull; Please enter an email address<br />";
		}else{
			if ($field->is_valid_email(trim($request['email'])) == false){
				$err_message .= "&bull; Email address is not a valid email address<br />";
			}
		}
		
		$parent_domain = '';
		
		if(strpos($request['user_id'], '-') !== false){
		
				$sql = "SELECT * 
				FROM cpm_contractors_user 
				WHERE cpm_contractors_user_qube_ref = '".$contractor_ref."'
				AND cpm_contractors_user_qube_ref = cpm_contractors_user_ref";
				$result = @mysql_query($sql);
				$num_rows = @mysql_num_rows($result);
				if($num_rows > 0){
					while($row = @mysql_fetch_array($result)){
						$parent_email = explode("@", $row['cpm_contractors_user_email']);
						$parent_domain = $parent_email[1];
					}
				}
			
			if(strpos($_REQUEST['email'], $parent_domain) === false){
				$err_message .= "&bull; Email address must have the same domain as the parent account: ".$parent_domain."<br />";
			}
		}
		
		$result_array['results'] = $err_message;
		
		return $result_array;	
	}
	
	function save($contractor_ref, $request){
		$crypt = new encryption_class;
		$data = new data;
		Global $UTILS_TEL_MAIN;
		
		$sql = "SELECT * 
		FROM cpm_contractors_user 
		WHERE cpm_contractors_user_qube_ref = '".$contractor_ref."'
		ORDER BY cpm_contractors_user_ref DESC
		LIMIT 1";
		
		$result = @mysql_query($sql);
		$num_rows = @mysql_num_rows($result);
		$last_id = "";
		$new_id = "";
		$pass_error = 'Y';
	
		if($num_rows > 0){
			$result_array['success'] = 'Y';
			while($row = @mysql_fetch_array($result)){
				$last_id = $row['cpm_contractors_user_ref'];
			}
		}else{
			$result_array['success'] = 'N';	
		}
		
		if(strpos($last_id, "-") === false){
			$new_id = $contractor_ref . "-001";
		}else{
			$last_id_str = explode("-", $last_id);
			$new_id = $contractor_ref . "-" . str_pad(($last_id_str[1] + 1), 3, "0", STR_PAD_LEFT);
		}
		
		$password = get_rand_id(8, "ALPHA", "UPPER");
		
		if ($_REQUEST['user_id'] == ""){
			$sql_insert = "INSERT INTO cpm_contractors_user SET cpm_contractors_user_ref = '".$new_id."', cpm_contractors_user_password = '".$crypt->encrypt($UTILS_DB_ENCODE, $password)."',  
		cpm_contractors_user_parent = '".$contractor_ref."', ";
		}else{
			$sql_insert = "UPDATE cpm_contractors_user SET ";
		}
		$sql_insert .= "cpm_contractors_user_qube_ref = '".$contractor_ref."', 
		cpm_contractors_user_name = '".$request['name']."',
		cpm_contractors_user_email = '".$request['email']."'";
		
		if ($request['user_id'] != ""){
			$disabled = "True";
			if ($request['disabled'] == ''){
				$disabled = "False";
				$sql_insert .= ", cpm_contractors_user_disabled_date = ''";
			}else{
				$datetime = new DateTime();
				$sql_insert .= ", cpm_contractors_user_disabled_date = '".$datetime->format('Ymd')."'";	
			}
			$sql_insert .= ", cpm_contractors_user_disabled = '".$disabled."' 
			WHERE cpm_contractors_user_ref = '".$request['user_id']."'";
		}
		
		@mysql_query($sql_insert) or $pass_error = $sql_insert;
		
		if ($request['user_id'] == ""){
		
			$mail_message = 'Dear '.$request['name'].',
			
		Welcome to the RMG Supplier Portal.
			
		Your username is: '.$new_id.'
		Your password is: '.$password.'
		
		To log in, go to http://www.rmgsuppliers.co.uk/
			
			
		Kind Regards,
		
		Technical Support Team
		Residential Management Group Ltd
		
		http://www.rmgsuppliers.co.uk
		Tel. '.$UTILS_TEL_MAIN;
			
			$sql = "INSERT INTO cpm_mailer SET
			mail_to = '".$request['email']."',
			mail_from = 'customerservice@rmguk.com',
			mail_subject = 'New RMG Living Login',
			mail_message = '".$mail_message."'";
			
			@mysql_query($sql);
		}
		
		$result_array['success'] = $pass_error;
		
		return $result_array;
	}
	
	function reset_password($request){
		$crypt = new encryption_class;
		Global $UTILS_TEL_MAIN;
		$password = get_rand_id(8, "ALPHA", "UPPER");
	
		if ($request['user_id'] != ""){
		
			$sql_insert = "UPDATE cpm_contractors_user SET 
			cpm_contractors_user_password = '".$crypt->encrypt($UTILS_DB_ENCODE, $password)."' 
			WHERE cpm_contractors_user_ref = '".$request['user_id']."'";
			@mysql_query($sql_insert) or $pass_error = $sql_insert;
		
			$mail_message = 'Dear '.$request['name'].',
			
			Update from RMG Living Contractors Portal.
				
			Your password has been reset to: '.$password.'
			
			To log in, go to http://www.rmgliving.co.uk/
				
				
			Kind Regards,
			
			Technical Support Team
			Residential Management Group Ltd
			
			www.rmgliving.co.uk
			Tel. '.$UTILS_TEL_MAIN;
				
			$sql = "INSERT INTO cpm_mailer SET
			mail_to = '".$request['email']."',
			mail_from = 'customerservice@rmguk.com',
			mail_subject = 'New RMG Living Password',
			mail_message = '".$mail_message."'";
			
			@mysql_query($sql);
		}
		
		$result_array['success'] = $pass_error;	
		
		return $result_array;
	}
	
	function save_contractor($contractor_ref, $request){
		
		$pass_error = "Y";
		
		$sql_insert = "UPDATE cpm_contractors SET ";
		
		$disabled = "True";
		
		if ($request['chk_disabled'] == ''){
			$disabled = "False";
			$sql_insert .= "cpm_contractors_disabled_date = '', ";
		}else{
			$datetime = new DateTime();
			$sql_insert .= "cpm_contractors_disabled_date = '".$datetime->format('Ymd')."', ";	
		}
		
		$sql_insert .= "cpm_contractors_disabled = '".$disabled."'
		WHERE cpm_contractors_qube_id = '".$contractor_ref."'";
		
		@mysql_query($sql_insert) or $pass_error = $sql_insert;
		
		$result_array['success'] = $pass_error;
		
		return $result_array;
	}
}

?>