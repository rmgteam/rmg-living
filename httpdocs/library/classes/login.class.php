<?
require_once($UTILS_CLASS_PATH."security.class.php");
require_once($UTILS_CLASS_PATH."encryption.class.php");
require_once($UTILS_CLASS_PATH."unit.class.php");
require_once($UTILS_CLASS_PATH."master_account.class.php");
require_once($UTILS_CLASS_PATH."data.class.php");
require_once($UTILS_CLASS_PATH."field.class.php");
require_once($UTILS_CLASS_PATH."resident.class.php");

class login {
		
	function do_lessee_login($username="", $password=""){
		
		global $UTILS_DB_ENCODE;
		$security = new security;
		$crypt = new encryption_class;
		$resident = new resident;
		$result_array = array();
		
		$result_array[0] = false;
		$result_array[1] = "";
		
		// Firstly, check for resident login
		$sql_resident = "
		SELECT *
		FROM cpm_residents re, cpm_lookup_residents lre, cpm_residents_extra rex, cpm_rmcs rmc, cpm_lookup_rmcs lrmc 
		WHERE 
		lrmc.rmc_lookup=rmc.rmc_num AND 
		lre.resident_lookup=re.resident_num AND 
		rex.password = '".$security->clean_query($crypt->encrypt($UTILS_DB_ENCODE, $password))."' AND 
		rex.password <> '' AND 
		re.rmc_num=rmc.rmc_num AND 
		re.resident_num=rex.resident_num AND 
		(rmc.rmc_status = 'Active' OR rmc.rmc_is_active = '1') AND 
		(re.resident_status = 'Current' OR re.resident_is_active = '1') AND 
		lre.resident_ref='".$security->clean_query($username)."' AND 
		lre.resident_ref <> ''
		";
		$result_resident = @mysql_query($sql_resident);
		$num_valid_users = @mysql_num_rows($result_resident);
		if($num_valid_users > 0){
		
			$row_resident = @mysql_fetch_array($result_resident);
			
			// If assoc to master account
			if( $row_resident['is_linked_to_master_account'] == 'Y' ){
				$result_array[0] = false;
				$result_array[1] = "This account is no longer active";
			}
			else{
	
				$_SESSION['resident_session'] = session_id();
				$_SESSION['rmc_num'] = $row_resident['rmc_num'];
				$_SESSION['rmc_ref'] = $row_resident['rmc_ref'];
				$_SESSION['is_developer'] = $row_resident['is_developer'];
				$_SESSION['paperless'] = $row_resident['paperless'];
				
				// Check login expiry date (for developer logins)
				$thisYMD = date("Ymd");
				if($row_resident['login_expiry_YMD'] == "" || $thisYMD < $row_resident['login_expiry_YMD']){
					
					// Get certain RMC info
					$sql_rmc = "
					SELECT * 
					FROM cpm_rmcs r, cpm_rmcs_extra rex  
					WHERE 
					r.rmc_num = rex.rmc_num AND 
					r.rmc_num = ".$security->clean_query($row_resident['rmc_num']);
					$result_rmc = @mysql_query($sql_rmc);
					$row_rmc = @mysql_fetch_array($result_rmc);
					
					// Get certain UNIT info (add in a clause that, if this is a Director then get info for their normal resident unit)...
					$resident = new resident($row_resident['resident_num']);
					if( $resident->is_resident_director == "Y" && preg_match("/^D.+-\d\d\d/" , $resident->resident_ref) > 0 ){
						
						$norm_unit_ref = substr($resident->resident_ref, 1, -4);
						$unit_ref_clause = " ( u.resident_num = ".$security->clean_query($row_resident['resident_num'])." OR lu.unit_ref = '".$norm_unit_ref."' ) AND ";
					}
					else{
						$unit_ref_clause = " u.resident_num = ".$security->clean_query($row_resident['resident_num'])." AND ";
					}
					$sql_unit = "
					SELECT * 
					FROM cpm_units u, cpm_lookup_units lu  
					WHERE 
					".$unit_ref_clause."  
					u.rmc_num = ".$security->clean_query($row_resident['rmc_num'])." AND 
					u.unit_num = lu.unit_lookup  
					";
					$result_unit = @mysql_query($sql_unit);
					$row_unit = @mysql_fetch_array($result_unit);
					
					$_SESSION['unit_ref'] = $row_unit['unit_ref'];
					$_SESSION['is_demo_account'] = $row_rmc['is_demo_account'];
					$_SESSION['rmc_name'] = $row_rmc['rmc_name'];
					$_SESSION['dev_description'] = $row_rmc['dev_description'];
					$_SESSION['member_id'] = $row_rmc['member_id'];		
					$_SESSION['is_resident_director'] = $row_resident['is_resident_director'];
					$_SESSION['resident_num'] = $row_resident['resident_num'];
					$_SESSION['resident_ref'] = $row_resident['resident_ref'];
					$_SESSION['resident_name'] = $row_resident['resident_name'];
					$_SESSION['subsidiary_id'] = $row_rmc['subsidiary_id'];	
					$_SESSION['login'] = session_id();
					
					$result_array[0] = true;
				}
				else{
					$result_array[0] = false;
					$result_array[1] = "Your login details have expired";
				}
				
			}
		}
		
		return $result_array;
	}
	
	
	function do_master_account_login($username="", $password="", $do_merge=""){
		
		global $UTILS_DB_ENCODE;
		$security = new security;
		$crypt = new encryption_class;
		$result_array = array();
		$master_account  = new master_account;
		
		$result_array[0] = false;
		$result_array[1] = "";
		
		// Firstly, check to see if login exists
		$sql_ma = "
		SELECT * 
		FROM cpm_master_accounts ma  
		WHERE 
		ma.master_account_username IS NOT NULL AND 
		ma.master_account_username <> '' AND 
		ma.master_account_username = '".$security->clean_query($username)."' AND 
		ma.master_account_password = '".$security->clean_query($crypt->encrypt($UTILS_DB_ENCODE, $password))."' AND 
		ma.master_account_password <> '' AND 
		ma.master_account_password IS NOT NULL 
		";
		$result_ma = @mysql_query($sql_ma);
		$num_valid_users = @mysql_num_rows($result_ma);
		if($num_valid_users > 0){
		
			$row_ma = @mysql_fetch_array($result_ma);
			$master_account->master_account($row_ma['master_account_id'],"id");
			
			// If requested by user, automatically add the property they are logged into
			if($do_merge == "Y"){
				
				$resident = new resident($_SESSION['resident_num']);
				if( $resident->link_to_master_account() !== true || $master_account->assoc_resident_to_master_account() !== true ){
					
					$result_array[0] = false;
					$result_array[1] = "This account could not be added to your Master Account.";
					return $result_array;
				}
			}
			
			// Get default associated resident accounts, if any... (this helps to check against any inactive rmcs/residents)
			$row_ma_default_resident = $master_account->get_master_account_default_assoc_resident();
			if($row_ma_default_resident !== false){

				$_SESSION['master_account_serial'] = $master_account->master_account_serial;
				$_SESSION['resident_session'] = session_id();
				$_SESSION['is_resident_director'] = $row_ma_default_resident['is_resident_director'];
				$_SESSION['resident_num'] = $row_ma_default_resident['resident_num'];
				$_SESSION['resident_ref'] = $row_ma_default_resident['resident_ref'];
				$_SESSION['resident_name'] = $row_ma_default_resident['resident_name'];
				$_SESSION['rmc_num'] = $row_ma_default_resident['rmc_num'];
				$_SESSION['rmc_ref'] = $row_ma_default_resident['rmc_ref'];
				$_SESSION['is_developer'] = $row_ma_default_resident['is_developer'];
				
				// Get certain RMC info
				$sql_rmc = "
				SELECT * 
				FROM cpm_rmcs r, cpm_rmcs_extra rex 
				WHERE r.rmc_num=rex.rmc_num AND r.rmc_num = ".$security->clean_query($row_ma_default_resident['rmc_num']);
				$result_rmc = @mysql_query($sql_rmc);
				$row_rmc = @mysql_fetch_array($result_rmc);
				
				// Get certain UNIT info
				$sql_unit = "
				SELECT * 
				FROM cpm_units u, cpm_lookup_units lu 
				WHERE 
				u.unit_num = lu.unit_lookup AND 
				u.resident_num = ".$security->clean_query($row_ma_default_resident['resident_num']);
				$result_unit = @mysql_query($sql_unit);
				$row_unit = @mysql_fetch_array($result_unit);
					
				$_SESSION['unit_ref'] = $row_unit['unit_ref'];
				$_SESSION['is_demo_account'] = $row_rmc['is_demo_account'];
				$_SESSION['rmc_name'] = $row_rmc['rmc_name'];
				$_SESSION['dev_description'] = $row_rmc['dev_description'];
				$_SESSION['member_id'] = $row_rmc['member_id'];		
				$_SESSION['subsidiary_id'] = $row_rmc['subsidiary_id'];	
				$_SESSION['login'] = session_id();
				
				$result_array[0] = true;
			}
			else{
				$result_array[0] = false;
				$result_array[1] = "You do not have any active properties associated with this account.";
			}
		}
		else{
			$result_array[0] = false;
			$result_array[1] = "Your login details were not recognised";
		}
		
		return $result_array;
	}
	
	function do_contractor_login($username="", $password=""){
		
		global $UTILS_DB_ENCODE;
		$security = new security;
		$crypt = new encryption_class;
		$resident = new resident;
		$result_array = array();
		
		$result_array[0] = false;
		$result_array[1] = "";
		
		// Firstly, check for resident login
		$sql_contractor = "
		SELECT u.*,c.cpm_contractors_disabled, 
		IFNULL(
			(
				SELECT t.cpm_contractors_user_trail_login
				FROM cpm_contractors_user_trail t
				WHERE t.cpm_contractors_user_trail_user_ref = u.cpm_contractors_user_ref
				ORDER BY t.cpm_contractors_user_trail_id DESC
				LIMIT 1
			),
			''
		)as last_login
		FROM cpm_contractors_user u
		INNER JOIN cpm_contractors c
		ON c.cpm_contractors_qube_id = u.cpm_contractors_user_qube_ref
		WHERE 
		u.cpm_contractors_user_password = '".$security->clean_query($crypt->encrypt($UTILS_DB_ENCODE, $password))."'
		AND u.cpm_contractors_user_ref = '".$username."'";
		
		$result_contractor = @mysql_query($sql_contractor);
		$num_valid_users = @mysql_num_rows($result_contractor);
		if($num_valid_users > 0){
		
			$row_contractor = @mysql_fetch_array($result_contractor);
			
			if( $row_contractor['cpm_contractors_user_disabled'] == 'True' ){
				$result_array[0] = false;
				$result_array[1] = "This account is no longer active";
			}elseif( $row_contractor['cpm_contractors_disabled'] == 'True' ){
				$result_array[0] = false;
				$result_array[1] = "This contractor is no longer active";
			}
			else{
	
				$data = new data;
				
				$_SESSION['contractor_session'] = session_id();
				$_SESSION['contractors_username'] = $row_contractor['cpm_contractors_user_ref'];
				$_SESSION['contractors_parent'] = $row_contractor['cpm_contractors_user_parent'];
				$_SESSION['contractors_qube_id'] = $row_contractor['cpm_contractors_user_qube_ref'];
				$_SESSION['contractors_last_login'] = $row_contractor['last_login'];
				
				if($row_contractor['cpm_contractors_user_password_ts'] != ""){
				
					$sql_days = "SELECT * FROM cpm_password_days";
					
					$result_days = @mysql_query($sql_days);
					$num_days = @mysql_num_rows($result_days);
					if($num_days > 0){
					
						$row_days = @mysql_fetch_array($result_days);
						$_SESSION['contractors_next_change'] = $data->date_plus_days($row_contractor['cpm_contractors_user_password_ts'], $row_days['cpm_password_days'], "Ymd", "Ymd");
						
					}
				}else{
					$_SESSION['contractors_next_change'] = "";	
				}
				
				$datetime = new DateTime(); 
				
				if ($row_contractor['last_login'] != ""){
				
					$sql_insert = "INSERT INTO cpm_contractors_user_trail SET
					cpm_contractors_user_trail_login = '".$datetime->format('Y-m-d-H-i-s')."',
					cpm_contractors_user_trail_user_ref = '".$row_contractor['cpm_contractors_user_ref']."',
					cpm_contractors_user_trail_ip = '".$_SERVER["REMOTE_ADDR"]."'";
					mysql_query($sql_insert) or $has_error = $sql_insert;
				}
				
				$result_array[0] = true;
			}
		}else{
			if(isset($_SESSION['contractor_failed_ref'])){
				if ($_SESSION['contractor_failed_ref'] != $username){
					$_SESSION['contractor_failed'] = 0;	
				}
			}
			
			$_SESSION['contractor_failed_ref'] = $username;	
			
			if(isset($_SESSION['contractor_failed'])){
				$_SESSION['contractor_failed'] = $_SESSION['contractor_failed'] + 1;
			}else{
				$_SESSION['contractor_failed'] = 1;	
			}
			
			if($_SESSION['contractor_failed'] == 5){
				$result_array[1] = $this->disable_user($username);
			}
			
			$result_array[0] = false;
		}
		
		return $result_array;
	} 
	
	
	function disable_user($username){
		Global $UTILS_TEL_MAIN;
		$sql = "SELECT IFNULL((SELECT cpm_contractors_user_email FROM cpm_contractors_user WHERE cpm_contractors_user_ref = c.cpm_contractors_user_parent), '' ) as parent_email, 
		* 
		FROM cpm_contractors_user c 
		WHERE cpm_contractors_user_ref = '".$username."'";
		$result = @mysql_query($sql);
		$num_valid_users = @mysql_num_rows($result);
		if($num_valid_users > 0){
			while ($row = @mysql_fetch_array($result)){
				$parent_email = $row['parent_email'];
				$user_name = $row['cpm_contractors_user_name'];
				$user_email = $row['cpm_contractors_user_email'];
				$user_disabled = $row['cpm_contractors_user_disabled'];
			}
		}
		
		if ($user_disabled == 'True'){
			$sql = "UPDATE cpm_contractors_user SET cpm_contractors_user_disabled = 'True' WHERE cpm_contractors_user_ref = '".$username."'";
			@mysql_query($sql);
			
			$mail_message = 'Dear '.$user_name.',
					
			Update from RMG Living Contractors Portal.
				
			Your account has been locked, due to 5 failed log in attempts. Please contact your supervisor.
				
				
			Kind Regards,
			
			Technical Support Team
			Residential Management Group Ltd
			
			www.rmgliving.co.uk
			Tel. '.$UTILS_TEL_MAIN;
				
			$sql = "INSERT INTO cpm_mailer SET
			mail_to = '".$user_email.", ".$parent_email.", itservicedesk@rmguk.com',
			mail_from = 'customerservice@rmguk.com',
			mail_subject = 'New RMG Living Password',
			mail_message = '".$mail_message."'";
			
			@mysql_query($sql);
		}
		
		return "Account disabled for 5 failed login attempts";
	}

	function forgot_check_fields($post, $securimage){
		
		$data = new data;
		$field = new field;
		$unit = new unit;
		$save_result = array();
		$save_result[0] = true;
		
		// Check mandatory fields
		if($post['account_type'] == "normal"){
		
			if($post['lessee_id'] == ""){
				$save_result[0] = false;
				$save_result[1] = "Please enter your lessee Id.";
				return $save_result;
			}
			if($post['email'] == "" || !$field->is_valid_email($post['email'])){
				$save_result[0] = false;
				$save_result[1] = "Please enter a valid email address.";
				return $save_result;
			}
		}
		elseif($post['account_type'] == "master"){
			
			if($post['email'] == "" || !$field->is_valid_email($post['email'])){
				$save_result[0] = false;
				$save_result[1] = "Please enter a valid email address.";
				return $save_result;
			}

			if($post['up_type'] == "password"){
				if($post['username'] == ""){
					$save_result[0] = false;
					$save_result[1] = "Please enter your Username.";
					return $save_result;
				}
			}
			else{
				if($post['password'] == ""){
					$save_result[0] = false;
					$save_result[1] = "Please enter your Password.";
					return $save_result;
				}	
			}
		}else{
			if($post['email'] == "" || !$field->is_valid_email($post['email'])){
				$save_result[0] = false;
				$save_result[1] = "Please enter a valid email address.";
				return $save_result;
			}
		}
		
		// Check CAPTCHA 
		if ($securimage->check($post['captcha_code']) == false) {
			$save_result[0] = false;
			$save_result[1] = "The words you typed into the Captcha field below are incorrect. Please try again.";
			return $save_result;
		}
		
		return $save_result;
	}
	
	/**
	 * Function called when password/username details are requested 
	 * 
	 * Types of user
	 * Normal
	 * Master
	 * 	- Request password
	 *  - Request username
	 * Contractor
	 * 
	 * @return true or reason for fail
	 */
	function do_forgot($post){
		$security = new security;
		$master_account = new master_account;
		$unit = new unit;
		$crypt = new encryption_class;
		$resident = new resident();
		global $UTILS_DB_ENCODE;
		
		// 'Normal' account
		if($post['account_type'] == "normal"){
			$email  = $security->clean_query($post['email']);
			$sql = "
			SELECT re.resident_num 
			FROM cpm_residents re, cpm_residents_extra rex, cpm_lookup_residents lre 
			WHERE 
			lre.resident_lookup=re.resident_num AND 
			re.resident_num=rex.resident_num AND 
			rex.email <> '' AND 
			lre.resident_ref = '".$security->clean_query($post['lessee_id'])."' AND 
			rex.email = '".$email."'";
			
			$result = @mysql_query($sql);
			$num = @mysql_num_rows($result);
			if($num > 0){
				
				$row = @mysql_fetch_row($result);
				
				// If property is associated to Master account
				if( $master_account->does_assoc_resident_exist($row[0]) === true ){
					return "The account you are trying to access is no longer accessible because it has been linked to a Master account. If you have forgotten your Master account login details, please use the facility below to retreive them.";
				}
				
				$password = $security->gen_serial(8, false, true);
				$unit->set_unit($row[0]);
				
				// Update resident account
				$sql2 = "
				UPDATE cpm_residents_extra SET
				allow_password_reset = 'Y',
				password = '".$crypt->encrypt($UTILS_DB_ENCODE, $password)."'
				WHERE resident_num = ".$row[0];
				@mysql_query($sql2);

					
				$msg = "
				Below is your password for the RMG Living website - www.rmgliving.co.uk
				This will give you access for your property \'".htmlentities($unit->get_unit_desc())."\':
								
				".$password."
								
				Once logged in, you can change you password to something more memorable in the \'My Account\' section. If you experience problems accessing the RMG Living website, please contact our Customer Services Team via customerservice@rmguk.com
								
				Kind Regards,
				RMG Living Support
								
				(This email was generated automatically.)
				";
				
				$sql = "INSERT INTO cpm_mailer SET
					mail_type = 'RMG Living Password Reset',
					mail_to = '".$email."',
					
					mail_from = 'customerservice@rmguk.com',
					mail_subject = 'RMG Living details...',
					mail_message = '".$msg."'";
					
				@mysql_query($sql);				
				
				//echo $sql;
				return true;	
			}
			else{
				return "This account does not exist.";	
			}
		}
		// 'Master' account 
		elseif($post['account_type'] == "master"){
			
			// Now check the master accounts...
			//requesting password
			if($post['up_type'] == "password"){
			
				$sql_ma = "
				SELECT * 
				FROM cpm_master_accounts ma 
				WHERE 
				ma.master_account_username IS NOT NULL AND 
				ma.master_account_username <> '' AND 
				ma.master_account_username = '".$security->clean_query($post['username'])."' AND 
				ma.master_account_email IS NOT NULL AND 
				ma.master_account_email <> '' AND 
				ma.master_account_email = '".$security->clean_query($post['email'])."' 
				";
				$result_ma = @mysql_query($sql_ma);
				$num_ma = @mysql_num_rows($result_ma);
				if($num_ma > 0){
					
					$row_ma = @mysql_fetch_array($result_ma);
					$master_account->master_account($row_ma['master_account_id'],"id");
					
					// If no properties are associated with the Master account
					if( $master_account->does_master_account_default_assoc_resident_exist() !== true ){
						return "The account that you are trying to access is no longer active.";
					}
					
					if( $master_account->send_forgot_password($post['email']) === true ){
						return true;
					}
					else{
						return "There was a problem sending you your details. Please try again.";
					}
				}
				else{
					return "The details you entered were not recognised.";
				}
			}
			//requested username
			else{
				
				$sql_ma = "
				SELECT * 
				FROM cpm_master_accounts ma 
				WHERE 
				ma.master_account_username IS NOT NULL AND 
				ma.master_account_username <> '' AND 
				ma.master_account_password <> '' AND  
				ma.master_account_password = '".$security->clean_query($crypt->encrypt($UTILS_DB_ENCODE, $post['password']))."' AND 
				ma.master_account_email IS NOT NULL AND 
				ma.master_account_email <> '' AND 
				ma.master_account_email = '".$security->clean_query($post['email'])."' 
				";
				$result_ma = @mysql_query($sql_ma);
				$num_ma = @mysql_num_rows($result_ma);
				if($num_ma > 0){
					
					$row_ma = @mysql_fetch_array($result_ma);
					$master_account->master_account($row_ma['master_account_id'],"id");
					if( $master_account->send_forgot_username($post['email']) === true ){
						return true;
					}
					else{
						return "There was a problem sending you your details. Please try again.";
					}
				}
				else{
					return "The details you entered were not recognised.";
				}
			}
		}
		//Contractor Account
		else{
			$email = $security->clean_query($post['email']);
			$sql = "
			SELECT * 
			FROM cpm_contractors_user
			WHERE 
			cpm_contractors_user_email = '".$email."'
			";
			
			$result = @mysql_query($sql);
			$num = @mysql_num_rows($result);
			if($num > 0){
				while($row = @mysql_fetch_array($result)){
					
					$password = $resident->get_rand_id(8, "ALPHA", "UPPER");
						
					$sql_insert = "UPDATE cpm_contractors_user SET 
					cpm_contractors_user_password = '".$crypt->encrypt($UTILS_DB_ENCODE, $password)."' 
					WHERE cpm_contractors_user_ref = '".$row['cpm_contractors_user_ref']."'";
					@mysql_query($sql_insert) or $pass_error = $sql_insert;
				
					$mail_message = 'Dear '.$row['cpm_contractors_user_name'].',
						
					Update from RMG Living Contractors Portal.
						
					Your username is: '.$row['cpm_contractors_user_ref'].'
					Your password has been reset to: '.$password.'
					
					To log in, go to http://www.rmgsuppliers.co.uk/
						
						
					Kind Regards,
					
					Technical Support Team
					Residential Management Group Ltd
					
					http://www.rmgsuppliers.co.uk/
					Tel. '.$UTILS_TEL_MAIN;
						
					$sql = "INSERT INTO cpm_mailer SET
					mail_to = '".$email."',
					mail_from = 'customerservice@rmguk.com',
					mail_subject = 'New RMG Living Password',
					mail_message = '".$mail_message."'";
					
					@mysql_query($sql);
					
					return "Your details have been emailed to you, including a new password.";
				}
			}else{
				return "Your email address has not been found";	
			}
		}
	
		return "This account does not exist.";
	}
	
	
	function do_lessee_logout(){
		
		unset($_SESSION['resident_session']);
		unset($_SESSION['rmc_num']);
		unset($_SESSION['rmc_ref']);
		unset($_SESSION['unit_ref']);
		unset($_SESSION['is_demo_account']);
		unset($_SESSION['is_director']);
		unset($_SESSION['is_developer']);
		unset($_SESSION['rmc_name']);
		unset($_SESSION['dev_description']);
		unset($_SESSION['member_id']);
		unset($_SESSION['resident_num']);
		unset($_SESSION['resident_ref']);
		unset($_SESSION['resident_name']);
		unset($_SESSION['login']);
		unset($_SESSION['subsidiary_id']);
		unset($_SESSION['master_account_serial']);
		unset($_SESSION['contractor_session']);		
		unset($_SESSION['contractors_username']);
		unset($_SESSION['contractors_qube_id']);		
	}

}

?>