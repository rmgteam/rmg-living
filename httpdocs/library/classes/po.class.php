<?
require_once($UTILS_CLASS_PATH."security.class.php");
require_once($UTILS_CLASS_PATH."xml.class.php");
require_once($UTILS_CLASS_PATH."xmltoarray.class.php");
require_once($UTILS_CLASS_PATH."data.class.php");

class po {
	
	function po (){
		
	}
	
	function get_list ($contractor_qube_ref, $search_term, $closed=''){
		
		$data = new data;
	
		$output = "";
		
		$sql = "SELECT 
		p.*, 
		r.rmc_name 
		FROM cpm_po p 
		INNER JOIN cpm_lookup_rmcs l 
		ON l.rmc_ref = p.cpm_po_rmc_id 
		INNER JOIN cpm_rmcs r 
		ON r.rmc_num = l.rmc_lookup 
		INNER JOIN cpm_po_job j 
		ON j.cpm_po_job_po_id = p.cpm_po_id 
		WHERE p.cpm_po_contractors_ref = '".$contractor_qube_ref."' 
		AND j.cpm_po_job_complete = 'False' 
		AND (UCASE(CONVERT(p.cpm_po_description using utf8)) LIKE '%".strtoupper($search_term)."%'
		OR p.cpm_po_number LIKE '%".$search_term."%'
		OR p.cpm_po_rmc_id LIKE '%".$search_term."%'
		OR r.rmc_name LIKE '%".$search_term."%'
		OR j.cpm_po_job_no LIKE '%".$search_term."%')";
		
		if ($closed != ""){
			$sql .= " AND j.cpm_po_job_complete = '".$closed."'";
		}
		
		$sql .= " GROUP BY p.cpm_po_id
		ORDER BY p.cpm_po_number";
		
		//print $sql;
		
		$result = @mysql_query($sql);
		$num_results = @mysql_num_rows($result);
		if($num_results > 0){
			while($row = @mysql_fetch_array($result)){
				
				if(strlen($row['rmc_name']) > 30){
					$prop = substr($row['rmc_name'], 0, 30) . "...";
				}else{
					$prop = $row['rmc_name'];
				}
				
				$output .= '<div class="form_row" id="search_row_'.$row['cpm_po_number'].'">';
				$output .= '<div class="form_label" style="width:15px;"><a href="javascript:expand(\''.$row['cpm_po_number'].'\');">';
				$output .= '<img border="0" id="expand_icon_'.$row['cpm_po_number'].'" src="/images/expand_down.gif" /></a></div>';
				$output .= '<div class="form_label border" style="width:65px;">'.$row['cpm_po_number'].'</div>';
				$output .= '<div class="form_label border" style="width:235px;">'.$prop.'</div>';
				$output .= '<div class="form_label border" style="width:285px;">'.$row['cpm_po_description'].'</div>';
				$output .= '<div class="form_label" style="width:60px;">'.$data->ymd_to_date($row['cpm_po_date_raised']).'</div>';
				$output .= '</div>';
				$output .= '<div class="form_row" id="extra_row_'.$row['cpm_po_number'].'" style="display:none;">';
				$output .= '<div class="form_row" style="overflow:hidden;width:543px;border:none; margin-left:100px;">';
				$output .= '<div class="sub_header_row" style="border:none;">';
				$output .= '<div class="form_label border" style="width:110px;border-left: 1px solid #363366">Job Number</div>';
				$output .= '<div class="form_label border" style="width:185px;">Expected Completion Date</div>';
				$output .= '<div class="form_label border" style="width:110px;">Quoted Amount</div>';
				$output .= '<div class="form_label border" style="width:93px;">Close</div>';
				$output .= '</div>';
				
				$sql_job = "SELECT *
				FROM cpm_po_job
				WHERE cpm_po_job_complete = 'False' AND
				cpm_po_job_po_id = '".$row['cpm_po_id']."'
				AND cpm_po_job_no LIKE '%".$search_term."%'";
				
				$result_job = @mysql_query($sql_job);
				$num_result_jobs = @mysql_num_rows($result_job);
				if($num_result_jobs == 0){
					$sql_job = "SELECT *
					FROM cpm_po_job
					WHERE cpm_po_job_complete = 'False' AND
					cpm_po_job_po_id = '".$row['cpm_po_id']."'";
				}
				
				$result_job = @mysql_query($sql_job);
				$num_result_jobs = @mysql_num_rows($result_job);
				if($num_result_jobs > 0){
					while($row_job = @mysql_fetch_array($result_job)){
						
						$output .= '<div class="form_row">';
						$output .= '<div class="form_label border" style="width:110px;border-left:1px solid #416CA0;">'.$row_job['cpm_po_job_no'].'</div>';
						$output .= '<div class="form_label border" style="width:185px;">'.$this->is_blank($data->ymd_to_date($row_job['cpm_po_job_completion_date']), 'N/A').'</div>';
						$output .= '<div class="form_label border" style="width:110px;">'.$row_job['cpm_po_job_amount'].'</div>';
						$output .= '<div class="form_label border" style="width:93px;">';
						$output .= '<input type="button" id="button_'.$row_job['cpm_po_job_id'].'" name="button_'.$row_job['cpm_po_job_id'].'"';
						$output .= ' onclick="complete_job('."'".$row_job['cpm_po_job_id']."'".')" value="Close" /></div>';
						$output .= '</div>';
					}	
				}
				
				$output .= '</div></div>';
			}
		}else{
			//$output = $sql;
		}
		
		return $output;
		
	}
	
	function is_blank($str, $output){
		if ($str == ''){
			return $output;
		}else{
			return $str;	
		}
	}
	
	function retieve_po ($contractor_qube_ref){
		
		$data = new data;
		$sqls = '';
		
		$po_id_array = array();
		
		$result_array = $this->get_po($contractor_qube_ref, 'getorders', "RMG Live");
		
		for( $c=0;$c<count($result_array['orders']);$c++ ) {
			
			$po_id = '';
			
			$order_number = addslashes(trim( $result_array['orders'][$c]['order-number'] )); if(is_array($result_array['orders'][$c]['order-number'])){$order_number = "";}
			$order_description = addslashes(trim( $result_array['orders'][$c]['description'] )); if(is_array($result_array['orders'][$c]['description'])){$order_description = "";}
			$job_number = addslashes(trim( $result_array['orders'][$c]['job-audit-number'] )); if(is_array($result_array['orders'][$c]['job-audit-number'])){$job_number = "";}
			$action_date = addslashes(trim( $result_array['orders'][$c]['action'] )); if(is_array($result_array['orders'][$c]['action'])){$action_date = "";}
			$job_unique = addslashes(trim( $result_array['orders'][$c]['uniqueid'] )); if(is_array($result_array['orders'][$c]['uniqueid'])){$job_unique = "";}
			$amount = addslashes(trim( $result_array['orders'][$c]['amount'] )); if(is_array($result_array['orders'][$c]['amount'])){$amount = "";}
			$property_ref = addslashes(trim( $result_array['orders'][$c]['property-ref'] )); if(is_array($result_array['orders'][$c]['property-ref'])){$property_ref = "";}
			$contract = addslashes(trim( $result_array['orders'][$c]['is-contract'] )); if(is_array($result_array['orders'][$c]['is-contract'])){$contract = "";}
			$date_raised = addslashes(trim( $result_array['orders'][$c]['date-raised'] )); if(is_array($result_array['orders'][$c]['date-raised'])){$date_raised = "";}
			$brand = addslashes(trim( $result_array['orders'][$c]['brand'] )); if(is_array($result_array['orders'][$c]['brand'])){$brand = "";}
			$sequence = addslashes(trim( $result_array['orders'][$c]['sequence'] )); if(is_array($result_array['orders'][$c]['sequence'])){$sequence = "";}
			$job_status = addslashes(trim( $result_array['orders'][$c]['job-status-str'] )); if(is_array($result_array['orders'][$c]['job-status-str'])){$job_status = "";}
			$job_status_int = addslashes(trim( $result_array['orders'][$c]['job-status-int'] )); if(is_array($result_array['orders'][$c]['job-status-int'])){$job_status_int = "";}
			$completion_date = addslashes(trim( $result_array['orders'][$c]['job-completion'] )); if(is_array($result_array['orders'][$c]['job-completion'])){$completion_date = "";}
			$estimated_date = addslashes(trim( $result_array['orders'][$c]['job-est-comp'] )); if(is_array($result_array['orders'][$c]['job-est-comp'])){$estimated_date = "";}
			
			if($action_date != ""){
				$date_parts = explode("-", $action_date);
				$action_date = $date_parts[0].$date_parts[1].$date_parts[2];
			}
			
			if($date_raised != ""){
				$date_parts = explode("-", $date_raised);
				$date_raised = $date_parts[0].$date_parts[1].$date_parts[2];
			}
			
			if($completion_date != ""){
				$date_parts = explode("-", $completion_date);
				$completion_date = $date_parts[0].$date_parts[1].$date_parts[2];
			}
			
			if($estimated_date != ""){
				$date_parts = explode("-", $estimated_date);
				$estimated_date = $date_parts[0].$date_parts[1].$date_parts[2];
			}
			
			if($contract == ""){
				$contract = 0;
			}
			
			$sql_po = "SELECT *
			FROM cpm_po
			WHERE cpm_po_number = '".$order_number."'";
			
			$result_po = @mysql_query($sql_po);
			$num_result_po = @mysql_num_rows($result_po);
			if($num_result_po == 0){
				$sql = "INSERT INTO cpm_po SET 
				cpm_po_number = '".$order_number."', ";
			}else{				
				$sql = "UPDATE cpm_po SET ";
			}
			
			$sql .= "cpm_po_rmc_id = '".$property_ref."', 
			cpm_po_description = '".$order_description."', 
			cpm_po_contractors_ref = '".$contractor_qube_ref."', 
			cpm_po_is_contract = '".$contract."', 
			cpm_po_date_raised = '".$date_raised."', 
			cpm_po_brand = '".$brand."', 
			last_update = '".$data->now_to_ymdhis()."'";
			
			if($num_result_po == 0){
				@mysql_query($sql);
				
				$sql_id = "SELECT LAST_INSERT_ID() FROM cpm_po";
				$result_id = mysql_query($sql_id);
				$num_rows = @mysql_num_rows($result_id);
				if($num_rows > 0){
					$row = @mysql_fetch_array($result_id);
					$po_id = $row[0];
				}
				
			}else{
				$row_po = @mysql_fetch_array($result_po);
				$po_id = $row_po['cpm_po_id'];
				
				$sql .= " WHERE cpm_po_id = '".$po_id."'";
				@mysql_query($sql);
			}
			
			array_push($po_id_array, $po_id);
			
			$sql_job = "SELECT *
			FROM cpm_po_job
			WHERE cpm_po_job_no = '".$job_number."'";
			
			$result_job = @mysql_query($sql_job);
			$num_result_job = @mysql_num_rows($result_job);
			if($num_result_job == 0){
				$sql = "INSERT INTO cpm_po_job SET 
				cpm_po_job_no = '".$job_number."', 
				cpm_po_job_po_id = '".$po_id."', 
				cpm_po_job_unique = '".$job_unique."', ";
			}else{
				$sql = "UPDATE cpm_po_job SET ";
			}
			
			$sql .= "cpm_po_job_amount = '".$amount."', 
			cpm_po_job_completion_date = '".$estimated_date."',  
			cpm_po_job_sequence = '".$sequence."', 
			cpm_po_job_status = '".$job_status."', 
			cpm_po_job_status_int = '".$job_status_int."',
			last_update = '".$data->now_to_ymdhis()."'";
			
			if($job_status_int == 4 || $job_status_int == 5 || $job_status_int == 6){
				$sql .= ", cpm_po_job_complete = 'True'";
				if($num_result_job > 0){
					$row_job = @mysql_fetch_array($result_job);
					$job_id = $row_job['cpm_po_job_id'];
					
					if ($row_job['cpm_po_job_user_ref'] == ''){
						$sql .= ", cpm_po_job_user_ref = 'Qube'";
					}
					if($completion_date != ""){
						$sql .= ", cpm_po_job_ts = '".$completion_date."'";
					}
				}
			}else{
				$sql .= ", cpm_po_job_complete = 'False', 
				cpm_po_job_user_ref = '', 
				cpm_po_job_reason = '', 
				cpm_po_job_reason_id = '', 
				cpm_po_job_advice = '', 
				cpm_po_job_ts = ''";
			}
			
			if($num_result_job > 0){				
				$sql .= " WHERE cpm_po_job_no = '".$job_number."'";
			}
			
			mysql_query($sql);
		}
		
		$sql = "SELECT j.*
		FROM cpm_po p
		INNER JOIN cpm_po_job j
		ON j.cpm_po_job_po_id = p.cpm_po_id
		WHERE (";
		
		foreach($po_id_array as $id){
			$sql .= "cpm_po_id <> '".$id."' AND ";
		}
		
		$sql = substr($sql,0,-5) . ") AND cpm_po_contractors_ref = '".$contractor_qube_ref."'";
		
		$result = @mysql_query($sql);
		$num_result = @mysql_num_rows($result);
		if($num_result > 0){
			while($row = @mysql_fetch_array($result)){
				$sql_complete = "UPDATE cpm_po_job SET
				cpm_po_job_complete = 'True'";
				
				if ($row['cpm_po_job_user_ref'] == ''){
					$sql_complete .= ", cpm_po_job_user_ref = 'Qube'";
				}
				$sql_complete .= " WHERE cpm_po_job_id = '".$row['cpm_po_job_id']."'";
				@mysql_query($sql_complete);
				$sqls .= preg_replace( '/\n\r|\r\n|\t/', ' ', $sql_complete);
			}
		}
		return $result_array;
	}
	
	function get_po($key, $process, $group="RMG Test"){
		
		GLOBAL $UTILS_IP_ADDRESS;
		
		$security = new security();
		$xml = new xml;
		$data = new data;
		$output_html = "";
		
		$session_id = $security->gen_serial(16);
		$ch = curl_init();
	
		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		$host = "Host: hodd1srctxprs13";
		$content_type = "Content-Type: text/xml; charset=utf-8";
		
		$sql = "SELECT j.last_update 
		FROM cpm_po_job j
		INNER JOIN cpm_po p
		ON p.cpm_po_id = j.cpm_po_job_po_id
		WHERE
		p.cpm_po_contractors_ref = '".$key."' 
		ORDER BY j.cpm_po_job_id DESC 
		LIMIT 1";
		
		$last_update = '2012-11-01';
		
		$result_job = @mysql_query($sql);
		$num_result_job = @mysql_num_rows($result_job);
		if($num_result_job > 0){
			$row_job = @mysql_fetch_array($result_job);
			$last_update = 	$data->ymdhis_to_date($row_job['last_update'], "Y-m-d");
		}
		
		$data = '<parameters>
		<key>'.$key.'</key>
		<filter-date>'.$last_update.'</filter-date>
		</parameters>';
		
		$qube_process_request = '<?xml version="1.0" encoding="utf-8"?>
		<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
		<soap:Body>
		<QubeProcess-3 xmlns="http://qube.qubeglobal.com/ns/webservice/">
		<ClientSessionKey>'.$session_id.'</ClientSessionKey>
		<QubeProcessName>rmgweb:'.$process.'</QubeProcessName>
		<Data>'.$data.'</Data>
		<UserName>rmgweb.servacc</UserName>
		<Password>p4ssw0rd</Password>
		<Group>'.$group.'</Group>
		<Application>QGS Purchase Ledger</Application>
		</QubeProcess-3>
		</soap:Body>
		</soap:Envelope>';
		
		//rmgweb.servacc
						
		$process_headers[] = $host;
		$process_headers[] = $content_type;
		$process_headers[] = "Content-Length : " . strlen ($qube_process_request);
		$process_headers[] = "SOAPAction: \"http://qube.qubeglobal.com/ns/webservice/QubeProcess-3\"";
		
		curl_setopt($ch, CURLOPT_URL, "http://".$UTILS_IP_ADDRESS."/qubews/qubews.asmx");
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $process_headers);
		curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
		curl_setopt($ch, CURLOPT_HEADER, 0);                			// tells curl to include headers in response
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);        			// return into a variable
		curl_setopt($ch, CURLOPT_TIMEOUT, 110);              			// times out after 30 secs
		curl_setopt($ch, CURLOPT_POSTFIELDS, $qube_process_request);  	// adding POST data
		curl_setopt($ch, CURLOPT_POST, 1);								// data sent as POST
		//curl_setopt($ch, CURLOPT_PORT , 443);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		
		$output = curl_exec($ch);
		$info = curl_getinfo($ch);
		
		$output_html = "";
		
		//$xml_result = $xml->xml2array($output);
		$xmlObj = new xml_to_array($output);
		$xml_result = $xmlObj->createArray(); 
		
		if($output === false || $info['http_code'] >= 400){	//curl unavailable	
			$result_array['result'] = "XML is invalid/Curl is unavailable. Http response: " . $info['http_code'] . " XML = " . $qube_process_request;
		}else {			
			$return_code = $xml_result['soap:Envelope']['soap:Body'][0]['QubeProcess-3Response'][0]['QubeProcess-3Result'][0]['data'][0]['suppliervalues'][0]['code'];
			
			if($return_code != "00"){ //service unavailable
				$result_array['result'] = "Error code: " . $return_code . " - " . $xml_result['soap:Envelope']['soap:Body'][0]['QubeProcess-3Response'][0]['QubeProcess-3Result'][0]['data'][0]['suppliervalues'][0]['desc'];
			}else{
				$num_trans = $xml_result['soap:Envelope']['soap:Body'][0]['QubeProcess-3Response'][0]['QubeProcess-3Result'][0]['data'][0]['suppliervalues'][0]['recordcount'];
				
				if($num_trans == 0 || $num_trans == ""){
					
					$result_array['result'] = "There are no po's available.";
					return $result_array;
					exit;
				}
				else{

					$transact_array = $xml_result['soap:Envelope']['soap:Body'][0]['QubeProcess-3Response'][0]['QubeProcess-3Result'][0]['data'][0]['suppliervalues'][0]['orders'];
					
					$result_array['result'] = "success";
					$result_array['orders'] = array();
					$result_array['orders'] = $transact_array;
				}
			}
		}				
		curl_close($ch);
		$this->logout($session_id);
		return $result_array;
		exit;
	}
	
	function complete_job($job_id){
		
		GLOBAL $UTILS_IP_ADDRESS;
		
		$security = new security();
		$xml = new xml;
		$data = new data;
		$output_html = "";
		
		$session_id = $security->gen_serial(16);
		$ch = curl_init();
	
		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		$content_type = "Content-Type: text/xml; charset=utf-8";
		
		$sql = "SELECT j.*, p.cpm_po_brand
		FROM cpm_po_job j
		INNER JOIN cpm_po p
		ON p.cpm_po_id = j.cpm_po_job_po_id
		WHERE
		cpm_po_job_id = '".$job_id."' 
		ORDER BY j.cpm_po_job_id DESC 
		LIMIT 1";
		
		$job_no = '';
		$brand = '';
		$para = '';
		
		$result_job = @mysql_query($sql);
		$num_result_job = @mysql_num_rows($result_job);
		if($num_result_job > 0){
			$row_job = @mysql_fetch_array($result_job);
			$job_no = $row_job['cpm_po_job_no'];
			$brand = $row_job['cpm_po_brand'];
			if ($row_job['cpm_po_job_complete'] == 'True'){
				if ($row_job['cpm_po_job_reason_id'] == ''){
					$complete = 1;	
					
					if($row_job['cpm_po_job_advice'] != ''){
						$para = '
						<advice>true</advice><comments>'.$row_job['cpm_po_job_advice'].'</comments>';	
					}
				}else{
					$complete = $row_job['cpm_po_job_reason_id'];
					
					if($row_job['cpm_po_job_reason'] != ''){
						$para = '
						<comments>'.$row_job['cpm_po_job_reason'].'</comments>';
					}else{
						$para = '
						<comments>-</comments>';
					}
				}
			}
		}
		$group = '';
		
		switch($brand){
			case 'RMG':
				$group = 'RMG Live';
				break;
			case 'GF':
				$group = 'RMG Gross Fine Live';
				break;
			case 'WM':
				$group = 'RMG Woods Management Live"';
				break;
			case 'JC':
				$group = 'RMG Johnson Cooper Live';
				break;
			case 'TF':
				$group = 'RMG Taskfine Live';
				break;
			case 'HPS':
				$group = 'RMG Haywards Live';
				break;
		}
		
		$data = '<parameters>
		<reference>'.$job_no.'</reference>
		<status>'.$complete.'</status>'.$para.'
		</parameters>';
		
		$qube_process_request = '<?xml version="1.0" encoding="utf-8"?>
		<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
		<soap:Body>
		<QubeProcess-3 xmlns="http://qube.qubeglobal.com/ns/webservice/">
		<ClientSessionKey>'.$session_id.'</ClientSessionKey>
		<QubeProcessName>rmgweb:closejob</QubeProcessName>
		<Data>'.$data.'</Data>
		<UserName>rmgweb.servacc</UserName>
		<Password>p4ssw0rd</Password>
		<Group>'.$group.'</Group>
		<Application>QGS Help Desk &amp; Planned Maintenance</Application>
		</QubeProcess-3>
		</soap:Body>
		</soap:Envelope>';
		
		$host = "Host: hodd1srctxprs13";
						
		$process_headers[] = $host;
		$process_headers[] = $content_type;
		$process_headers[] = "Content-Length : " . strlen ($qube_process_request);
		$process_headers[] = "SOAPAction: \"http://qube.qubeglobal.com/ns/webservice/QubeProcess-3\"";
		
		curl_setopt($ch, CURLOPT_URL, "http://".$UTILS_IP_ADDRESS."/qubews/qubews.asmx");
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $process_headers);
		curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
		curl_setopt($ch, CURLOPT_HEADER, 0);                			// tells curl to include headers in response
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);        			// return into a variable
		curl_setopt($ch, CURLOPT_TIMEOUT, 110);              			// times out after 30 secs
		curl_setopt($ch, CURLOPT_POSTFIELDS, $qube_process_request);  	// adding POST data
		curl_setopt($ch, CURLOPT_POST, 1);								// data sent as POST
		//curl_setopt($ch, CURLOPT_PORT , 443);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		
		$output = curl_exec($ch);
		$info = curl_getinfo($ch);
		
		$output_html = "";
		
		//$xml_result = $xml->xml2array($output);
		$xmlObj = new xml_to_array($output);
		$xml_result = $xmlObj->createArray(); 
		
		$result_array['data'] = $data;
		$result_array['group'] = $group;
		
		if($output === false || $info['http_code'] >= 400){	//curl unavailable	
			$result_array['result'] = "XML is invalid/Curl is unavailable. Http response: " . $info['http_code'] . " XML = " . $qube_process_request;
		}else {			
			$return_code = $xml_result['soap:Envelope']['soap:Body'][0]['QubeProcess-3Response'][0]['QubeProcess-3Result'][0]['data'][0]['closeure-result'][0]['code'];
			
			if($return_code != "00"){ //service unavailable
				$result_array['result'] = $info['http_code'] . " - Error code: " . $return_code . " - " . $xml_result['soap:Envelope']['soap:Body'][0]['QubeProcess-3Response'][0]['QubeProcess-3Result'][0]['data'][0]['closeure-result'][0]['desc'] . " XML = " . $qube_process_request;
			}else{
				$result_array['result'] = $xml_result['soap:Envelope']['soap:Body'][0]['QubeProcess-3Response'][0]['QubeProcess-3Result'][0]['data'][0]['closeure-result'][0]['desc'];
			}
		}				
		curl_close($ch);
		$this->logout($session_id);
		return $result_array;
		exit;
	}

	function logout($session_id){
		
		GLOBAL $UTILS_IP_ADDRESS;
		
		$ch = curl_init();
	
		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		//$host = "Host: hodd1svapp1";
		$host = "Host: hodd1srctxprs13";
		$content_type = "Content-Type: text/xml; charset=utf-8";
		
		$qube_process_request = '<?xml version="1.0" encoding="utf-8"?>
		<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
		<soap:Body>
		<Logout xmlns="http://qube.qubeglobal.com/ns/webservice/">
		<ClientSessionKey>'.$session_id.'</ClientSessionKey>
	    </Logout>
		</soap:Body>
		</soap:Envelope>';
		
		//rmgweb.servacc
						
		$process_headers[] = $host;
		$process_headers[] = $content_type;
		$process_headers[] = "Content-Length : " . strlen ($qube_process_request);
		$process_headers[] = "SOAPAction: \"http://qube.qubeglobal.com/ns/webservice/QubeProcess-3\"";
		
		//curl_setopt($ch, CURLOPT_URL, "https://hodd1svapp1/qubews/qubews.asmx");
		curl_setopt($ch, CURLOPT_URL, "http://".$UTILS_IP_ADDRESS."/qubews/qubews.asmx");
		curl_setopt($ch, CURLOPT_HTTPHEADER, $process_headers);
		curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
		curl_setopt($ch, CURLOPT_HEADER, 0); // tells curl to include headers in response
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  // return into a variable
		curl_setopt($ch, CURLOPT_TIMEOUT, 30); // times out after 30 secs
		curl_setopt($ch, CURLOPT_POSTFIELDS, $qube_process_request); // adding POST data
		curl_setopt($ch, CURLOPT_POST, 1);  // data sent as POST
		//curl_setopt($ch, CURLOPT_PORT , 443);
		//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		//curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		
		$output = curl_exec($ch);
		//return str_replace("\r\n","",$output);
		$info = curl_getinfo($ch);
		
		$output_html = "";
		
		if($output === false || $info['http_code'] >= 400){	//curl unavailable	
			$output_html = "XML is invalid/Curl is unavailable. Http response: " . $info['http_code'] . " XML = " . $qube_process_request;
		}elseif($info['http_code'] == 200){
			$output_html = true;
		}				
		curl_close($ch);
		return $output_html;	
	}

	function PrintArray($array){
	
		$info = '';
		
		if(is_array($array)){
			foreach($array as $key=>$value){
				if(is_array($value)){
				// the value of the current array is also a array, so call this function again to process that array
					$this->PrintArray($value);
				}else{
				// This part of the array is just a key/value pair
					$info.= "$key: $value<br>";
				}
			}
		}
		
		return $info;
	}
}

?>	