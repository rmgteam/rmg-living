<?

//======================================================
// Class to extract and manipulate resident data
//======================================================

class rmc {
	
	var $rmc_ref;
	var $owner_id;
	var $rmc_name;
	var $region;
	var $rmc_freeholder_name;
	var $subsidiary_code;
	var $property_manager;
	var $regional_manager;
	var $prevent_printing_of_letters;


	function rmc($rmc_num=""){
		
		 $this->set_rmc($rmc_num);
	}
	
	function file_friendly_name(){
		return str_replace('/', '~', $this->rmc['rmc_ref']);
	}	

	function set_rmc($id){
		if($id != ""){
			
			$sql = "
			SELECT * 
			FROM cpm_lookup_rmcs lrmc, cpm_rmcs rmc, cpm_rmcs_extra rmcex, cpm_subsidiary s 
			WHERE 
			rmc.subsidiary_id = s.subsidiary_id AND 
			rmc.rmc_num=lrmc.rmc_lookup AND 
			rmc.rmc_num=rmcex.rmc_num AND 
			rmc.rmc_num = ".$id;
			$result = mysql_query($sql);
			$this->rmc = mysql_fetch_array($result);
			
			@mysql_data_seek($result,0);
			$row = @mysql_fetch_array($result);
			
			$this->rmc_num = $row['rmc_num'];
			$this->rmc_ref = $row['rmc_ref'];
			$this->owner_id = $row['owner_id'];
			$this->rmc_name = $row['rmc_name'];
			$this->rmc_freeholder_name = $row['rmc_freeholder_name'];
			$this->subsidiary_code = $row['subsidiary_code'];
			$this->property_manager = $row['property_manager'];
			$this->regional_manager = $row['regional_manager'];
			$this->prevent_printing_of_letters = $row['prevent_printing_of_letters'];
			$this->region = $row['rmc_region'];
		}
		else{
			return false;
		}
	}

	function get_num_client_documents(){

		// Letters of Interest
		$sql_letters = "SELECT * FROM cpm_client  WHERE rmc_num = ".$this->rmc['rmc_num'];
		$result_letters = @mysql_query($sql_letters);
		$num_letters = @mysql_num_rows($result_letters);
		return $num_letters;
	}
	
	function get_num_letters_of_interest(){
		
		// Letters of Interest
		$sql_letters = "SELECT * FROM cpm_letters  WHERE rmc_num = ".$this->rmc['rmc_num'];
		$result_letters = @mysql_query($sql_letters);
		$num_letters = @mysql_num_rows($result_letters);
		return $num_letters;
	}
	
	function get_num_newsletters(){
	
		// Newsletters
		$sql_newsletters = "SELECT * FROM cpm_newsletters WHERE rmc_num = ".$this->rmc['rmc_num'];
		$result_newsletters = @mysql_query($sql_newsletters);
		$num_newsletters = @mysql_num_rows($result_newsletters);
		return $num_newsletters;
	}
	
	function get_num_ssm(){
		
		// Site Specific Maintenance documents
		$sql_ssm = "SELECT * FROM cpm_ssm WHERE rmc_num = ".$this->rmc['rmc_num'];
		$result_ssm = @mysql_query($sql_ssm);
		$num_ssm = @mysql_num_rows($result_ssm);
		return $num_ssm;
	}
	
	function get_num_slas(){
		
		// SLA's
		$sql_sla = "SELECT * FROM cpm_sla WHERE rmc_num = ".$this->rmc['rmc_num'];
		$result_sla = @mysql_query($sql_sla);
		$num_slas = @mysql_num_rows($result_sla);
		return $num_slas;
	}
	
	function get_num_car_park_plans(){
				
		// Car park plan(s)
		$sql_car_park = "SELECT * FROM cpm_car_park WHERE rmc_num = ".$this->rmc['rmc_num'];
		$result_car_park = @mysql_query($sql_car_park);
		$num_car_park_plans = @mysql_num_rows($result_car_park);
		return $num_car_park_plans;
	}
	
	function get_num_budgets(){
				
		// Budget(s)
		$sql_budget = "SELECT * FROM cpm_budgets WHERE rmc_num = ".$this->rmc['rmc_num'];
		$result_budget = @mysql_query($sql_budget);
		$num_budgets = @mysql_num_rows($result_budget);
		return $num_budgets;
	}
	
	function get_num_meeting_notes(){
	
		// Meeting notes
		$sql_meeting_notes = "SELECT * FROM cpm_agm WHERE rmc_num = ".$this->rmc['rmc_num'];
		$result_meeting_notes = @mysql_query($sql_meeting_notes);
		$num_meeting_notes = @mysql_num_rows($result_meeting_notes);
		return $num_meeting_notes;
	}
	
	function get_num_site_visits(){
	
		// Site visits
		$sql_site_visits = "SELECT * FROM cpm_site_visits WHERE rmc_num = ".$this->rmc['rmc_num'];
		$result_site_visits = @mysql_query($sql_site_visits);
		$num_site_visits = @mysql_num_rows($result_site_visits);
		return $num_site_visits;
	}
	
	
	function is_ground_rent_only(){
		
		if( preg_match('/^8/', $this->rmc_ref) > 0 && trim($this->property_manager) == "Ground Rent Only"){
			return true;
		}
		return false;
	}
	
	
	// Gets a list of schedules that are insuraed by the freeholder (search Intranet db)
	function get_freeholder_ins_sched(){
		
		global $UTILS_INTRANET_DB_LINK;
		
		$sql = "
		SELECT * 
		FROM ins_sched i, rmcs r, lookup_rmc lrmc, freeholders f, subsidiary s, ins_sched_types t 
		WHERE 
		i.ins_sched_type_id = t.ins_sched_type_id AND 
		i.ins_sched_freeholder_id = f.freeholder_id AND 
		i.rmc_num = lrmc.rmc_lookup AND 
		i.rmc_num = r.rmc_num AND 
		r.subsidiary_id = s.subsidiary_id AND 
		r.rmc_is_active = 1 AND 
		s.subsidiary_code <> '' AND 
		s.subsidiary_code IS NOT NULL AND 
		s.subsidiary_code = '".$this->subsidiary_code."' AND 
		lrmc.rmc_ref <> '' AND 
		lrmc.rmc_ref IS NOT NULL AND 
		lrmc.rmc_ref = '".$this->rmc_ref."' AND 
		i.ins_sched_reviewed = 'N' AND 
		i.ins_sched_discon = 'N' AND 
		i.ins_sched_is_visible = 'Y' AND 
		i.ins_sched_freeholder_to_insure = 'Y'  
		";
		$result = mysql_query($sql, $UTILS_INTRANET_DB_LINK);
		return $result;
	}
	
	function get_num_freeholder_ins_sched(){
		
		global $UTILS_INTRANET_DB_LINK;
		
		$sql = "
		SELECT count(*) 
		FROM ins_sched i, rmcs r, lookup_rmc lrmc, freeholders f, subsidiary s  
		WHERE 
		i.ins_sched_freeholder_id = f.freeholder_id AND 
		i.rmc_num = lrmc.rmc_lookup AND 
		i.rmc_num = r.rmc_num AND 
		r.subsidiary_id = s.subsidiary_id AND 
		r.rmc_is_active = 1 AND 
		s.subsidiary_code <> '' AND 
		s.subsidiary_code IS NOT NULL AND 
		s.subsidiary_code = '".$this->subsidiary_code."' AND 
		lrmc.rmc_ref <> '' AND 
		lrmc.rmc_ref IS NOT NULL AND 
		lrmc.rmc_ref = '".$this->rmc_ref."' AND 
		i.ins_sched_reviewed = 'N' AND 
		i.ins_sched_discon = 'N' AND 
		i.ins_sched_is_visible = 'Y' AND 
		i.ins_sched_freeholder_to_insure = 'Y'  
		";
		//print $sql;
		$result = mysql_query($sql, $UTILS_INTRANET_DB_LINK);
		$row = mysql_fetch_row($result);
		return $row[0];
	}
	
	

}

?>