<?

class field {


	function is_valid_email($email){
	
		if( (preg_match('/(@.*@)|(\.\.)|(@\.)|(\.@)|(^\.)/', $email)) || (preg_match('/^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,3}|[0-9]{1,3})(\]?)$/',$email)) ) { 
			return true;
		}
		return false;
	}
	
	
	function is_valid_date($date, $format="dd/mm/yyyy"){
		
		if($format == "dd/mm/yyyy"){
			if(preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $date)){return true;}
		}
		return false;
	}
	
	
	function is_date_in_future($date, $format="dd/mm/yyyy"){
		
		if($format == "dd/mm/yyyy"){
			
			$today_ymd = date("Ymd");
			$date_parts = explode("/", $date);
			$date_ymd = $date_parts[2].$date_parts[1].$date_parts[0];
			
			if($date_ymd > $today_ymd){
				return true;
			}
		}
		
		return false;
	}
	
	
	function is_number($string){
	
		if(preg_match("/^-{0,1}\d+$/", $string) == 1){
			return true;
		}
		return false;
	}
	
}


?>