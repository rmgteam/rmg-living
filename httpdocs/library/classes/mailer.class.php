<?
require_once($UTILS_SERVER_PATH."library/functions/valid_email_check.php");

class mailer {

	// SET
	function set_mail_type($str){
		$this->row['mail_type'] = $str;
	}

	function set_mail_to($to){
		
		if(is_array($to)){
			foreach($to as $rec){
				if(valid_email_check($rec)){
					$to_str .= $rec.",";
				}
			}
			$to_str = substr($to_str, 0, -1);
		}
		else{
			$to_str = $to;
		}	
		$this->row['mail_to'] = $to_str;
	}
	
	function set_mail_from($from){
		if(valid_email_check($from)){
			$this->row['mail_from'] = $from;
		}
	}
	
	function set_mail_subject($subject){
		$this->row['mail_subject'] = $subject;
	}
	
	function set_mail_message($message){
		$this->row['mail_message'] = $message;
	}
	
	function set_mail_html($message){
		$this->row['mail_html'] = $message;
	}
	
	function set_mail_headers($headers){
		$this->row['mail_headers'] = $headers;
	}
	
	function set_mail_attachment($file, $del){
		$this->row['mail_attachment'] = $file;
		$this->row['mail_del_attach'] = $del;
	}
	
	function set_mail_images($images){
		$this->row['mail_images'] = $images;
	}
	
	function mail_ready_to_send(){
		return true;
	}
	
	function send(){
	
		if($this->mail_ready_to_send()){
		
			$sql = "
			INSERT INTO cpm_mailer SET
			mail_type = '".$this->get_mail_type()."',
			mail_to = '".$this->get_mail_to()."',
			mail_from = '".$this->get_mail_from()."',
			mail_subject = '".addslashes($this->get_mail_subject())."',
			mail_message = '".addslashes($this->get_mail_message())."',
			mail_html = '".addslashes($this->get_mail_html())."',
			mail_images = '".addslashes($this->get_mail_images())."',
			mail_attachment = '".addslashes($this->get_mail_attachment())."',
			mail_del_attach = '".addslashes($this->get_mail_del_attach())."',
			mail_headers = '".addslashes($this->get_mail_headers())."'";
			@mysql_query($sql);
			
			return $sql;
		}else{
			return "failed";
		}
	}
	
	
	
	// GET
	function get_mail_to(){
		return $this->row['mail_to'];
	}
	
	function get_mail_from(){
		return $this->row['mail_from'];
	}
	
	function get_mail_subject(){
		return $this->row['mail_subject'];
	}
	
	function get_mail_type(){
		return $this->row['mail_type'];
	}
	
	function get_mail_message(){
		return $this->row['mail_message'];
	}
	
	function get_mail_html(){
		return $this->row['mail_html'];
	}
	
	function get_mail_images(){
		return $this->row['mail_images'];
	}
	
	function get_mail_attachment(){
		return $this->row['mail_attachment'];
	}
	
	function get_mail_del_attach(){
		if($this->row['mail_del_attach'] == ''){
			$this->row['mail_del_attach'] = 'N';	
		}
		return $this->row['mail_del_attach'];
	}
	
	function get_mail_headers(){
		return $this->row['mail_headers'];
	}

}

?>