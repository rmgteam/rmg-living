<?
require_once($UTILS_CLASS_PATH."data.class.php");
require_once($UTILS_CLASS_PATH."security.class.php");
require_once($UTILS_CLASS_PATH."order.class.php");


class ins_sched_order extends order {

	
	function save($request){
		
		$security = new security;
		$thistime = time();
		
		if( $this->order_id == "" ){
			
			$order_ins_sched_id = "NULL";
			if( $request['order_ins_sched_id'] != "" ){
				$order_ins_sched_id = $request['order_ins_sched_id'];
			}
			
			$sql = "
			INSERT INTO cpm_orders (
				order_serial,
				order_type,
				brand_code,
				order_ref,
				order_net,
				order_vat_perc,
				order_vat,
				order_subtotal,
				order_total,
				order_charge,
				browser_version,
				resident_num,
				order_datetime,
				order_YMD,
				order_TS,
				order_ins_sched_id,
				order_card_type_pre_selected
			)
			VALUES(
				'".$this->gen_unique_serial()."',
				3,
				'RMG',
				'".$security->clean_query($request['oid'])."',
				'".$security->clean_query(str_replace(",","",$request['net']))."',
				'".$security->clean_query(str_replace(",","",$request['vat_perc']))."',
				'".$security->clean_query(str_replace(",","",$request['vat_amount']))."',
				'".$security->clean_query(str_replace(",","",$request['subtotal']))."',
				'".$security->clean_query(str_replace(",","",$request['total']))."',
				'".$security->clean_query(str_replace(",","",$request['charge']))."',
				'".$security->clean_query($_SERVER['HTTP_USER_AGENT'])."',
				'".$security->clean_query($request['resident_num'])."',
				'".date("M d Y H:i:s", $thistime)."',
				".date("Ymd", $thistime).",
				".$thistime.",
				".$order_ins_sched_id.",
				'".$security->clean_query($request['order_card_type_pre_selected'])."'
			)
			";
			if( @mysql_query($sql) !== false ){
			
				$sql_id = "SELECT LAST_INSERT_ID() FROM cpm_orders";
				$result_id = @mysql_query($sql_id);
				$row_id = @mysql_fetch_row($result_id);
			
				$this->order($row_id[0]);
				return true;
			}
		}
		else{
			
			// Save details of completed transaction...
			$sql = "
			UPDATE cpm_orders SET
				order_transactionstatus = '".$request['order_transactionstatus']."',
				order_bank_auth_code = '".$request['order_bank_auth_code']."',
				order_cv2avs = '".$request['order_cv2avs']."',
				order_trans_code = '".$request['order_trans_code']."',
				order_test_status = '".$request['order_test_status']."',
				order_response_hash = '".$request['order_response_hash']."',
				order_card_type_paid_with = '".$request['order_card_type_paid_with']."' 
			WHERE order_id = ".$this->order_id."
			";
			if( @mysql_query($sql) !== false ){
				return true;	
			}
			else{
				//print mysql_error();	
			}
		}
		
		return "Technical fault - this order could not be saved.";
	}
	

}


?>