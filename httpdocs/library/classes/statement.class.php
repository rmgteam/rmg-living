<?
require_once($UTILS_CLASS_PATH."security.class.php");
require_once($UTILS_CLASS_PATH."xml.class.php");


class statement {
	

	function get_statement($type="account", $resident_ref, $group, $date_from){
		
		GLOBAL $UTILS_IP_ADDRESS;
	
		// Note: $date_from must be Y-m-d
	
		$xml = new xml;
		$security = new security();
		$output_html = "";
		$rowcounter = 0;
		
		$qube_date_from_parts = explode("/", $date_from);
		$qube_date_from = $qube_date_from_parts[2]."-".$qube_date_from_parts[1]."-".$qube_date_from_parts[0];
		
		$final_balance = 0;
		$session_id = $security->gen_serial(16);
		$ch = curl_init();
	
		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		$content_type = "Content-Type: text/xml; charset=utf-8";
		//$host = "Host: hodd1svapp1";
		//$webservice_server = "https://81.106.219.24/qubews/qubews.asmx";
		//if($group == "RMG Live"){
			$host = "Host: hodd1srctxprs13";
			$webservice_server = "http://".$UTILS_IP_ADDRESS."/qubews/qubews.asmx";
		//}
		
		$statement_type = "Full";
		if($type == "arrears"){
			$statement_type = "";
		}
		
		$data = '<parameters>
		<Reference>'.$resident_ref.'</Reference>
		<order>t</order>
		<statement>'.$statement_type.'</statement>
		<bfdate>'.$qube_date_from.'</bfdate>
		</parameters>';
		
		$qube_process_request = '<?xml version="1.0" encoding="utf-8"?>
		<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
		<soap:Body>
		<QubeProcess-3 xmlns="http://qube.qubeglobal.com/ns/webservice/">
		<ClientSessionKey>'.$session_id.'</ClientSessionKey>
		<QubeProcessName>rmgweb:tenstat</QubeProcessName>
		<Data>'.$data.'</Data>
		<UserName>rmgweb.servacc</UserName>
		<Password>p4ssw0rd</Password>
		<Group>'.$group.'</Group>
		<Application>QGS Property Management</Application>
		</QubeProcess-3>
		</soap:Body>
		</soap:Envelope>';
		
		$process_headers[] = $host;
		$process_headers[] = $content_type;
		$process_headers[] = "Content-Length : " . strlen ($qube_process_request);
		$process_headers[] = "SOAPAction: \"http://qube.qubeglobal.com/ns/webservice/QubeProcess-3\"";
		
		curl_setopt($ch, CURLOPT_URL, $webservice_server);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $process_headers);
		curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
		curl_setopt($ch, CURLOPT_HEADER, 0);                			// tells curl to include headers in response
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);        			// return into a variable
		curl_setopt($ch, CURLOPT_TIMEOUT, 110);              			// times out after 30 secs
		curl_setopt($ch, CURLOPT_POSTFIELDS, $qube_process_request);  	// adding POST data
		curl_setopt($ch, CURLOPT_POST, 1); 								// data sent as POST
		
		//if($group != "RMG Live"){
		//	curl_setopt($ch, CURLOPT_PORT , 443);
		//	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		//	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		//}
		
		$output = curl_exec($ch);
		//return str_replace("\r\n","",$output);
		$info = curl_getinfo($ch);
		
		if($output === false || $info['http_code'] != 200){
	
			$output = "No cURL data returned - [". $info['http_code']. "]";
			if(curl_error($ch)){
				$output .= "\n". curl_error($ch);
			}
			
			$output_html .= '<tr class="statement_msg_row"><td colspan="7" style="border-right:1px solid #ccc;border-left:1px solid #ccc;">This service is currently unavailable. Please try again later. (1)</td></tr>';
			
		}
		else {
					
			//Put XML result into array
			$xml_result = $xml->xml2array($output);
			$return_code = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['tenantvalues']['code'];
		
			if($return_code != "00"){
				$error_responses = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['returnvalue']['details'];
				$err_str = "The following errors occurred:<br />";
				for($r=0;$r < count($error_responses);$r++){
					$err_str .= $error_responses[$r]['reference'].": ".$error_responses[$r]['comment']."<br />";
				}
				
				$output_html .= '<tr class="statement_msg_row"><td colspan="7" style="border-right:1px solid #ccc;border-left:1px solid #ccc;">This service is currently unavailable. Please try again later.</td></tr>';
				
			}
			else{
			
				$num_trans = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['tenantvalues']['lines'];
				if($num_trans == 0 || $num_trans == ""){
					
					$output_html .= '<tr class="statement_msg_row"><td colspan="7" style="border-right:1px solid #ccc;border-left:1px solid #ccc;">&nbsp;There are no items available.</td></tr>';
					
				}
				else{
					
					if($num_trans == "1"){ // For some reason, the xml2array process does not put only one transaction into an array, so we do this manually
						unset($transact_array);
						$transact_array[0] = array();
						$transact_array[0] = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['tenantvalues']['transact'];
					}
					else{
						$transact_array = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['tenantvalues']['transact'];
					}
					
					$curr_balance = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['tenantvalues']['broughtfwd'];
					
					if($type != "arrears"){
						$output_html .= '
						<tr class="statement_data_row">
							<td class="first" colspan="6" style="border-bottom:2px solid #7EA0BD;">Balance brought forward from before '.$date_from.'</td>
							<td style="text-align:right; width:75px;border-bottom:2px solid #7EA0BD;">&nbsp;'.number_format($curr_balance,2,".",",").'</td>
						</tr>
						';
					}
					
					
					for($t=0;$t < count($transact_array);$t++){
											
						$row_class = "row_off";
						if($rowcounter % 2 == 0){
							$row_class = "row_on";
						}
						
						$this_trans_date = "";
						if(!is_array($transact_array[$t]['date']) && $transact_array[$t]['date'] != ""){
							$this_trans_date_split = explode("-", $transact_array[$t]['date']);
							$this_trans_date = mktime(1, 1, 1, $this_trans_date_split[1], $this_trans_date_split[2], $this_trans_date_split[0]);
							$this_trans_date = date("jS M Y",$this_trans_date);
						}
						
						$this_due_date = "";
						if(!is_array($transact_array[$t]['duedate']) && $transact_array[$t]['duedate'] != ""){
							$this_due_date_split = explode("-", $transact_array[$t]['duedate']);
							$this_due_date = mktime(1, 1, 1, $this_due_date_split[1], $this_due_date_split[2], $this_due_date_split[0]);
							$this_due_date = date("jS M Y",$this_due_date);
						}
						
						$this_from_date = "";
						if($transact_array[$t]['from'] != "" && !is_array($transact_array[$t]['from'])){
							$this_from_date_split = explode("-", $transact_array[$t]['from']);
							$this_from_date = mktime(1, 1, 1, $this_from_date_split[1], $this_from_date_split[2], $this_from_date_split[0]);
						}
						
						$this_to_date = "";
						if($transact_array[$t]['to'] != "" && !is_array($transact_array[$t]['to'])){
							$this_to_date_split = explode("-", $transact_array[$t]['to']);
							$this_to_date = mktime(1, 1, 1, $this_to_date_split[1], $this_to_date_split[2], $this_to_date_split[0]);
						}
						
						$period_date = "";
						if($this_from_date != "" && $this_to_date != ""){
							if($this_from_date != $this_to_date){
								$period_date = "<br />(for period ".date("jS M Y",$this_from_date)." - ".date("jS M Y",$this_to_date).")";
							}
						}
						
						if($type == "arrears"){
							if($transact_array[$t]['outstand'] != "" && is_numeric($transact_array[$t]['outstand']) && $transact_array[$t]['outstand'] != "0.00"){
								$curr_balance += $transact_array[$t]['outstand'];
							}
							
							$output_html .= '
							<tr class="statement_data_row">
								<td class="first '.$row_class.'" style="width:90px;">'.$this_trans_date.'</td>
								<td class="'.$row_class.'">'.ucfirst($transact_array[$t]['comment']).$period_date.'</td>
								<td class="'.$row_class.'" style="width:90px;">'.$this_due_date.'&nbsp;</td>
								<td class="'.$row_class.'" style="text-align:right; width:75px;">&nbsp;';if($transact_array[$t]['charged'] != "" && $transact_array[$t]['charged'] != "0.00"){$output_html .=  $transact_array[$t]['charged'];}$output_html .= '</td>
								<td class="'.$row_class.'" style="text-align:right; width:85px;">&nbsp;';if($transact_array[$t]['received'] != "" && $transact_array[$t]['received'] != "0.00"){$output_html .= $transact_array[$t]['received'];}$output_html .= '</td>
								<td class="'.$row_class.'" style="text-align:right; width:75px;">&nbsp;';if($transact_array[$t]['outstand'] != ""){$output_html .= $transact_array[$t]['outstand'];}$output_html .= '</td>
								<td class="'.$row_class.'" style="text-align:right; width:75px;">&nbsp;'.number_format($curr_balance,2,".",",").'</td>
							</tr>
							';
						}
						else{
							if($transact_array[$t]['charged'] != "" && is_numeric($transact_array[$t]['charged']) && $transact_array[$t]['charged'] != "0.00"){
								$curr_balance += $transact_array[$t]['charged'];
							}
							if($transact_array[$t]['received'] != "" && is_numeric($transact_array[$t]['received']) && $transact_array[$t]['received'] != "0.00"){
								$curr_balance -= $transact_array[$t]['received'];
							}
							
							$output_html .= '
							<tr class="statement_data_row">
								<td class="first '.$row_class.'" style="width:90px;">'.$this_trans_date.'</td>
								<td class="'.$row_class.'" colspan="2">'.(!empty($transact_array[$t]['comment'])?ucfirst($transact_array[$t]['comment']):'').$period_date.'</td>
								<td class="'.$row_class.'" style="width:90px;">'.$this_due_date.'&nbsp;</td>
								<td class="'.$row_class.'" style="text-align:right; width:75px;">&nbsp;';if($transact_array[$t]['charged'] != "" && $transact_array[$t]['charged'] != "0.00"){$output_html .= $transact_array[$t]['charged'];}$output_html .= '</td>
								<td class="'.$row_class.'" style="text-align:right; width:85px;">&nbsp;';if($transact_array[$t]['received'] != "" && $transact_array[$t]['received'] != "0.00"){$output_html .= $transact_array[$t]['received'];}$output_html .= '</td>
								<td class="'.$row_class.'" style="text-align:right; width:75px;">&nbsp;'.number_format($curr_balance,2,".",",").'</td>
							</tr>
							';
						}
						
						$rowcounter++;
					}
				}
			}				
		}

		curl_close($ch);
		////$this->logout($session_id);
		
		if(($output === true || $info['http_code'] == 200) && $return_code == "00"){

			$end_balance = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['tenantvalues']['carriedfwd'];
			$grent_due = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['tenantvalues']['cfgrentdebt'];
			$s_charge_due = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['tenantvalues']['cfothbalnet'];
			
			if($grent_due <= 0){$grent_due = "";}
			if($s_charge_due <= 0){$s_charge_due = "";}
	
			$output_html .= '
			<tr id="statement_totals_row">
				<td colspan="4" style="border-bottom:none;">&nbsp;</td>
				<td colspan="3" style="text-align:right; font-size:16px; font-weight:bold; padding:10px 5px 10px 0;"><span style="color:';if($end_balance > 0){$output_html .= '#CC3300';}else{$output_html .= '#336633';}$output_html .= ';">&pound; '.number_format($end_balance,2,".",",").'</span>&nbsp;</td>
			</tr>
			<script type="text/javascript">
			grent_due = "'.$grent_due.'";
			s_charge_due = "'.$s_charge_due.'";
			</script>
			';
		}
		
		return str_replace("\r\n","",$output_html);
	}
	
	function logout($session_id){
		
		GLOBAL $UTILS_IP_ADDRESS;
		
		$ch = curl_init();
	
		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		//$host = "Host: hodd1svapp1";
		$host = "Host: hodd1srctxprs13";
		$content_type = "Content-Type: text/xml; charset=utf-8";
		
		$qube_process_request = '<?xml version="1.0" encoding="utf-8"?>
		<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
		<soap:Body>
		<Logout xmlns="http://qube.qubeglobal.com/ns/webservice/">
		<ClientSessionKey>'.$session_id.'</ClientSessionKey>
	    </Logout>
		</soap:Body>
		</soap:Envelope>';
		
		//rmgweb.servacc
						
		$process_headers[] = $host;
		$process_headers[] = $content_type;
		$process_headers[] = "Content-Length : " . strlen ($qube_process_request);
		$process_headers[] = "SOAPAction: \"http://qube.qubeglobal.com/ns/webservice/QubeProcess-3\"";
		
		//curl_setopt($ch, CURLOPT_URL, "https://hodd1svapp1/qubews/qubews.asmx");
		curl_setopt($ch, CURLOPT_URL, "http://".$UTILS_IP_ADDRESS."/qubews/qubews.asmx");
		curl_setopt($ch, CURLOPT_HTTPHEADER, $process_headers);
		curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
		curl_setopt($ch, CURLOPT_HEADER, 0); // tells curl to include headers in response
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  // return into a variable
		curl_setopt($ch, CURLOPT_TIMEOUT, 30); // times out after 30 secs
		curl_setopt($ch, CURLOPT_POSTFIELDS, $qube_process_request); // adding POST data
		curl_setopt($ch, CURLOPT_POST, 1);  // data sent as POST
		//curl_setopt($ch, CURLOPT_PORT , 443);
		//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		//curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		
		$output = curl_exec($ch);
		//return str_replace("\r\n","",$output);
		$info = curl_getinfo($ch);
		
		$output_html = "";
		
		if($output === false || $info['http_code'] >= 400){	//curl unavailable	
			$output_html = "XML is invalid/Curl is unavailable. Http response: " . $info['http_code'] . " XML = " . $qube_process_request;
		}elseif($info['http_code'] == 200){
			$output_html = true;
		}				
		curl_close($ch);
		return $output_html;	
	}
	
	function get_grent_scharge_only($type="account", $resident_ref, $group, $date_from){
		
		GLOBAL $UTILS_IP_ADDRESS;
	
		// Note: $date_from must be Y-m-d
	
		$xml = new xml;
		$security = new security();
		$output_html = "";
		$rowcounter = 0;
		
		$qube_date_from_parts = explode("/", $date_from);
		$qube_date_from = $qube_date_from_parts[2]."-".$qube_date_from_parts[1]."-".$qube_date_from_parts[0];
		
		$final_balance = 0;
		$session_id = $security->gen_serial(16);
		$ch = curl_init();
		
		$statement_type = "Full";
		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		$content_type = "Content-Type: text/xml; charset=utf-8";
		//$host = "Host: hodd1svapp1";
		//$webservice_server = "https://81.106.219.24/qubews/qubews.asmx";
		//if($group == "RMG Live"){
			$host = "Host: hodd1srctxprs13";
			$webservice_server = "http://".$UTILS_IP_ADDRESS."/qubews/qubews.asmx";
		//}
		
		$data = '<parameters>
		<Reference>'.$resident_ref.'</Reference>
		<order>t</order>
		<statement>'.$statement_type.'</statement>
		<bfdate>'.$qube_date_from.'</bfdate>
		</parameters>';
		
		$qube_process_request = '<?xml version="1.0" encoding="utf-8"?>
		<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
		<soap:Body>
		<QubeProcess-3 xmlns="http://qube.qubeglobal.com/ns/webservice/">
		<ClientSessionKey>'.$session_id.'</ClientSessionKey>
		<QubeProcessName>rmgweb:tenstat</QubeProcessName>
		<Data>'.$data.'</Data>
		<UserName>rmgweb.servacc</UserName>
		<Password>p4ssw0rd</Password>
		<Group>'.$group.'</Group>
		<Application>QGS Property Management</Application>
		</QubeProcess-3>
		</soap:Body>
		</soap:Envelope>';
		
		$process_headers[] = $host;
		$process_headers[] = $content_type;
		$process_headers[] = "Content-Length : " . strlen ($qube_process_request);
		$process_headers[] = "SOAPAction: \"http://qube.qubeglobal.com/ns/webservice/QubeProcess-3\"";
		
		curl_setopt($ch, CURLOPT_URL, $webservice_server);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $process_headers);
		curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
		curl_setopt($ch, CURLOPT_HEADER, 0);                			// tells curl to include headers in response
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);        			// return into a variable
		curl_setopt($ch, CURLOPT_TIMEOUT, 110);              			// times out after 30 secs
		curl_setopt($ch, CURLOPT_POSTFIELDS, $qube_process_request);  	// adding POST data
		curl_setopt($ch, CURLOPT_POST, 1); 								// data sent as POST
		//if($group != "RMG Live"){
		//	curl_setopt($ch, CURLOPT_PORT , 443);
		//	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		//	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		//}
		
		$output = curl_exec($ch);
		$info = curl_getinfo($ch);
		curl_close($ch);
		//$this->logout($session_id);

		if($output === true || $info['http_code'] == 200){
			
			$xml_result = $xml->xml2array($output);
			$return_code = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['tenantvalues']['code'];
			
			if($return_code != "00"){
				return false;
			}
			else{
				
				$end_balance = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['tenantvalues']['carriedfwd'];
				$grent_due = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['tenantvalues']['cfgrentdebt'];
				$s_charge_due = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['tenantvalues']['cfothbalnet'];
				
				if($grent_due <= 0){$grent_due = 0;}
				if($s_charge_due <= 0){$s_charge_due = 0;}
		
				return array($grent_due, $s_charge_due);
			}
		}
		
		return false;
		
	}
	
}


?>