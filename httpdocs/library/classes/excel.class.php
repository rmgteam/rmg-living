<?php

require_once($UTILS_CLASS_PATH."data.class.php");
require_once($UTILS_CLASS_PATH."PHPExcel.php");

class excel Extends PHPExcel {
	
	var $no_sheets = 1;
	var $num_rows = 0;
	var $num_cols = 0; 
	var $header_line = 0;
	var $data_line = 0;
	var $header = array();
	var $title = array();
	var $info = array();
	var $class = array();
	var $gap = array();
	
	/**
	* Create Object
	* @param		String		$doc_title			String for document title
	* @param		Boolean		$show_titles		Boolean to show title, default false
	*/
	function excel($doc_title = "", $show_titles = false){
		parent:: __construct();
		
		$this->header['bg_color'] = 'FF2A587A';
		$this->header['bg_color'] = 'FF0F61A0';
		$this->header['font_color'] = 'FFFFFFFF';
		$this->header['font_bold'] = true;
		$this->header['font_size'] = 11;
		
		$this->title['bg_color'] = 'FFFFFFFF';
		$this->title['font_color'] = 'FF003366';
		$this->title['font_bold'] = true;
		$this->title['font_size'] = 20;
		
		$this->info['bg_color'] = 'FFFFFFFF';
		$this->info['font_color'] = 'FF000000';
		$this->info['font_bold'] = false;
		$this->info['font_size'] = 11;
		
		$this->group['bg_color'] = 'FFF1F1F1';
		$this->group['font_color'] = 'FF000000';
		$this->group['font_bold'] = true;
		$this->group['font_size'] = 11;
		
		$this->data['bg_color'] = 'FFFFFFFF';
		$this->data['font_color'] = 'FF000000';
		$this->data['font_bold'] = false;
		$this->data['font_size'] = 11;
		
		$this->row['bg_color'] = 'FFFFFFFF';
		$this->row['font_color'] = 'FF000000';
		$this->row['font_bold'] = false;
		$this->row['font_size'] = 11;
		
		$this->class['subtotal']['bg_color'] = 'FFE5EFF6';
		$this->class['subtotal']['font_color'] = 'FF000000';
		$this->class['subtotal']['font_bold'] = false;
		$this->class['subtotal']['font_size'] = 11;
		
		$this->class['total']['bg_color'] = 'FF0B62A6';
		$this->class['total']['font_color'] = 'FFFFFFFF';
		$this->class['total']['font_bold'] = true;
		$this->class['total']['font_size'] = 11;
		
		$this->class['tick']['bg_color'] = 'FF00CC00';
		$this->class['tick']['font_color'] = 'FFFFFFFF';
		$this->class['tick']['font_bold'] = false;
		$this->class['tick']['font_size'] = 11;
		
		$this->class['overdue']['bg_color'] = 'FFEE3C3C';
		$this->class['overdue']['font_color'] = 'FFFFFFFF';
		$this->class['overdue']['font_bold'] = false;
		$this->class['overdue']['font_size'] = 11;
		
		$this->class['greyed']['bg_color'] = 'FFCCCCCC';
		$this->class['greyed']['font_color'] = 'FF000000';
		$this->class['greyed']['font_bold'] = false;
		$this->class['greyed']['font_size'] = 11;
		
		$this->set_defaults();
			
		$this->getProperties()->setTitle($doc_title);
		
		if($doc_title != '' && $show_titles == true){
			$this->set_title($doc_title);	
		}
	}
	
	/**
	* Set Defaults Fonts and Properties
	*/
	function set_defaults(){
		$this->getProperties()->setCreator($_SESSION['personnel_forename']." ".$_SESSION['personnel_surname'])
		 	->setLastModifiedBy($_SESSION['personnel_forename']." ".$_SESSION['personnel_surname']);
		 	
		$this->getDefaultStyle()->getFont()
			->setName('Calibri')
			->setSize($this->data['font_size'])
			->getColor()->setARGB($this->data['font_color']);
		
		$this->getDefaultStyle()->getFont()->setBold($this->data['font_bold']);
		$this->getDefaultStyle()->getAlignment()->setWrapText(true);
	}
	
	/**
	* Set Active Sheet Title
	* @param		String		$string			String for title
	*/
	function set_sheet_title($string){
		$this->getActiveSheet()->setTitle($string);
	}

	/**
	* Main Set Title function - Including page title header
	* @param		String		$string			String for title
	*/
	function set_title($string){
		$this->set_sheet_title($string);
		
		$this->num_rows ++;
		
		$this->getActiveSheet()->getStyle('A'.$this->num_rows)->getAlignment()->setWrapText(false);
		$this->getActiveSheet()->getCell('A'.$this->num_rows)->setValue($string);
		$this->getActiveSheet()->mergeCells('A'.$this->num_rows.':AD'.$this->num_rows);
		$this->getActiveSheet()->getStyle('A'.$this->num_rows)->getFont()->setSize($this->title['font_size']);
		$this->getActiveSheet()->getStyle('A'.$this->num_rows)->getFont()->getColor()->setARGB($this->title['font_color']);
		$this->getActiveSheet()->getStyle('A'.$this->num_rows)->getFont()->setBold($this->title['font_bold']);
		$this->getActiveSheet()->getStyle('A'.$this->num_rows.':AD'.$this->num_rows)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$this->getActiveSheet()->getStyle('A'.$this->num_rows.':AD'.$this->num_rows)->getFill()->getStartColor()->setARGB($this->title['bg_color']);
	}
	
	/**
	* Add a new sheet with main title
	* @param		String		$string			String for title
	*/
	function add_sheet($string){
		$this->no_sheets ++;
		$this->createSheet($this->no_sheets - 1);
		$this->set_title($string);
	}
	
	/**
	* Add an info line
	* @param		String		$string			String for info
	*/
	function set_info($string){
		$this->num_rows ++;
		
		$this->getActiveSheet()->getCell('A'.$this->num_rows)->setValue($string);
		$this->getActiveSheet()->mergeCells('A'.$this->num_rows.':AD'.$this->num_rows);
		$this->getActiveSheet()->getStyle('A'.$this->num_rows)->getFont()->setSize($this->info['font_size']);
		$this->getActiveSheet()->getStyle('A'.$this->num_rows)->getFont()->getColor()->setARGB($this->info['font_color']);
		$this->getActiveSheet()->getStyle('A'.$this->num_rows)->getFont()->setBold($this->info['font_bold']);
		$this->getActiveSheet()->getStyle('A'.$this->num_rows.':AD'.$this->num_rows)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$this->getActiveSheet()->getStyle('A'.$this->num_rows.':AD'.$this->num_rows)->getFill()->getStartColor()->setARGB($this->info['bg_color']);
	}
	
	/**
	* Adds a fully merged row with text
	* @param		String		$string		String for line
	*/
	function full_row($string){
		$this->num_rows ++;
		
		$this->getActiveSheet()->getCell('A'.$this->num_rows)->setValue($string);
		$this->getActiveSheet()->mergeCells('A'.$this->num_rows.':AD'.$this->num_rows);
		$this->getActiveSheet()->getStyle('A'.$this->num_rows)->getFont()->setSize($this->row['font_size']);
		$this->getActiveSheet()->getStyle('A'.$this->num_rows)->getFont()->getColor()->setARGB($this->row['font_color']);
		$this->getActiveSheet()->getStyle('A'.$this->num_rows)->getFont()->setBold($this->row['font_bold']);
		$this->getActiveSheet()->getStyle('A'.$this->num_rows.':AD'.$this->num_rows)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$this->getActiveSheet()->getStyle('A'.$this->num_rows.':AD'.$this->num_rows)->getFill()->getStartColor()->setARGB($this->row['bg_color']);
	}
	
	/**
	* Add a group line
	* @param		Array		$array			Numbered Array for groups
	* @arrayitem	Array						Named Array
	* 				@arrayitem	Integer			start_col		Interger for start column
	* 				@arrayitem	Integer			end_col			Interger for end column
	* 				@arrayitem	String			data			String for group name
	*/
	function group($array){
		$this->num_rows ++;
		
		foreach($array as $item){
			
			$start_col = $this->get_char($item['start_col']);
			$end_col = $this->get_char($item['end_col']);
			
			$this->getActiveSheet()->mergeCells($start_col.$this->num_rows.':'.$end_col.$this->num_rows);
			
			$this->getActiveSheet()->getCell($start_col.$this->num_rows)->setValue($this->html_blank($item['data']));
			$this->getActiveSheet()->getStyle($start_col.$this->num_rows)->getFont()->setSize($this->group['font_size']);
			$this->getActiveSheet()->getStyle($start_col.$this->num_rows)->getFont()->getColor()->setARGB($this->group['font_color']);
			$this->getActiveSheet()->getStyle($start_col.$this->num_rows)->getFont()->setBold($this->group['font_bold']);
			$this->getActiveSheet()->getStyle($start_col.$this->num_rows.':'.$end_col.$this->num_rows)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
			$this->getActiveSheet()->getStyle($start_col.$this->num_rows.':'.$end_col.$this->num_rows)->getFill()->getStartColor()->setARGB($this->group['bg_color']);
			//$this->getActiveSheet()->getStyle($start_col.$this->num_rows.':'.$end_col.$this->num_rows)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		}
	}
	
	/**
	* Add a Header line
	* @param		Array		$array			Numbered Array of headers
	* @arrayitem	String		$header_name	String for header name			
	*/
	function headers($array){
		
		$this->num_rows ++;
		$this->header_line = $this->num_rows;
		
		foreach($array as $item){
			$this->num_cols ++;
			
			$col_letter = $this->get_char($this->num_cols);
			
			$this->getActiveSheet()->getCell($col_letter.$this->num_rows)->setValue($this->html_blank($item));
			$this->getActiveSheet()->getStyle($col_letter.$this->num_rows)->getFont()->setSize($this->header['font_size']);
			$this->getActiveSheet()->getStyle($col_letter.$this->num_rows)->getFont()->getColor()->setARGB($this->header['font_color']);
			$this->getActiveSheet()->getStyle($col_letter.$this->num_rows)->getFont()->setBold($this->header['font_bold']);
			$this->getActiveSheet()->getStyle($col_letter.$this->num_rows)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
			$this->getActiveSheet()->getStyle($col_letter.$this->num_rows)->getFill()->getStartColor()->setARGB($this->header['bg_color']);
			//$this->getActiveSheet()->getStyle($col_letter.$this->num_rows)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		}
	}
	
	/**
	* Add data lines
	* @param		Array		$array		Numbered Array for each row
	* @arrayitem	Array					Numbered Array for each column
	* * or
	* @arrayitem	Array					Named Arrays for data, class. Where data holds a nnumbered array for each column and class is class name.
	*
	* 				@arrayitem	String		$data			String for data	
	* * or
	* 				@arrayitem	Array		Named array
	* 							@arrayitem	String	data		String for data
	* 							@arrayitem	String	format		String for data format
	*/
	function data($array){

		$col_no = 0;
		
		$this->data_line = $this->num_rows + 1;
		
		if(count($array) > 0){
			foreach($array as $item){
				$this->num_rows ++;
				$col_no = 0;
				$next_array = $item;
				
				if(is_array($item['data'])){
					$next_array = $item['data'];
				}
				
				for($i=0;$i<count($next_array);$i++){
					$col = $next_array[$i];
					$col_no ++;
					$col_letter = $this->get_char($col_no);
						
					$format = '';
					
					if(is_array($item['format'])){						
						if(strtolower($item['format'][$i]) == 'currency'){
							$format = '£#,##0.00;[Red]-£#,##0.00';
						}else{
							$format = $item['format'][$i];
						}
					}
					
					if($format != ''){
						$this->getActiveSheet()->getCell($col_letter.$this->num_rows)->setValueExplicit($this->html_blank($col), PHPExcel_Cell_DataType::TYPE_NUMERIC);
						$this->getActiveSheet()->getStyle($col_letter.$this->num_rows)->getNumberFormat()->setFormatCode($format);
					}else{
						if($this->num_check($col)){
							$this->getActiveSheet()->getCell($col_letter.$this->num_rows)->setValueExplicit($col, PHPExcel_Cell_DataType::TYPE_NUMERIC);
							if($col != 0){
								$this->getActiveSheet()->getStyle($col_letter.$this->num_rows)->getNumberFormat()->setFormatCode('#');
							}
						}else{
							if($this->date_check($col)){
								$ExcelDateValue = PHPExcel_Shared_Date::stringToExcel($col); 
								$this->getActiveSheet()->setCellValue($col_letter.$this->num_rows, $ExcelDateValue);
								$this->getActiveSheet()->getStyle($col_letter.$this->num_rows)->getNumberFormat()->setFormatCode('dd/mm/yyyy');	
							}else{
								$this->getActiveSheet()->getCell($col_letter.$this->num_rows)->setValueExplicit($this->html_blank($col), PHPExcel_Cell_DataType::TYPE_STRING);
							}
						}
					}
					
					if(is_array($item['class'])){
						if(!is_null($item['class'][$i])){
							$this->getActiveSheet()->getStyle($col_letter.$this->num_rows)->getFont()->setSize($this->class[$item['class'][$i]]['font_size']);
							$this->getActiveSheet()->getStyle($col_letter.$this->num_rows)->getFont()->getColor()->setARGB($this->class[$item['class'][$i]]['font_color']);
							$this->getActiveSheet()->getStyle($col_letter.$this->num_rows)->getFont()->setBold($this->class[$item['class'][$i]]['font_bold']);
							$this->getActiveSheet()->getStyle($col_letter.$this->num_rows)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
							$this->getActiveSheet()->getStyle($col_letter.$this->num_rows)->getFill()->getStartColor()->setARGB($this->class[$item['class'][$i]]['bg_color']);
						}
					}elseif($item['class'] != ''){
						$this->getActiveSheet()->getStyle($col_letter.$this->num_rows)->getFont()->setSize($this->class[$item['class']]['font_size']);
						$this->getActiveSheet()->getStyle($col_letter.$this->num_rows)->getFont()->getColor()->setARGB($this->class[$item['class']]['font_color']);
						$this->getActiveSheet()->getStyle($col_letter.$this->num_rows)->getFont()->setBold($this->class[$item['class']]['font_bold']);
						$this->getActiveSheet()->getStyle($col_letter.$this->num_rows)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
						$this->getActiveSheet()->getStyle($col_letter.$this->num_rows)->getFill()->getStartColor()->setARGB($this->class[$item['class']]['bg_color']);
					}
					
					//$this->getActiveSheet()->getStyle($col_letter.$this->num_rows)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				}
			}
		}else{
			$this->full_row('No records found');	
		}
	}
	
	/**
	* Save
	* @param		Boolean		$auto_size	Boolean for whether to auto-size all columns
	* @return		Integer		$num_rows	Integer for number of rows
	*/
	function save($auto_size=true){
		
		//Auto Size Each column
		if($auto_size == true){
			for($i=0;$i<$this->no_sheets;$i++){
				$this->setActiveSheetIndex($i);
				for($j=0;$j<$this->num_cols;$j++){
					$this->getActiveSheet()->getColumnDimension($this->get_char($j+1))->setAutoSize(true);
				}
			}
		}
		
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$this->setActiveSheetIndex(0);
		
		// saves ( Excel 2003 - cannot auto-filter or freeze panes )
		$objWriter = PHPExcel_IOFactory::createWriter($this, 'Excel2007');
		$objWriter->save('php://output');
	}
	
	/**
	* Return number of rows
	* @return		Integer		$num_rows	Integer for number of rows
	*/
	function get_no_rows(){
		return $this->num_rows;
	}
	
	/**
	* Return number of columns
	* @return		Integer		$num_cols	Integer for number of columns
	*/
	function get_no_cols(){
		return $this->num_cols;
	}
	
	/**
	* Return character for number
	* @param		Integer		$integer	Integer for column number
	* @return		String		$char		Character for columns number
	*/
	function get_char($integer){
		$num = 0;
		
		if($integer > 26 ){
			$num = floor($integer / 26);
			if($integer % 26 == 0){
				$integer = 1;
			}else{
				$integer = $integer - (26 * $num);
			}
		}
		
		$output = '';
		$char = chr($integer + 64);
		
		if($num > 0){
			$output = $this->get_char($num) . $char;
		}else{
			$output = $char;
		}
		
		return $output;
	}
	
	/**
	* Return blank if &nbsp; and remove html
	* @param		String		$string		Input String
	* @return		String		$string		Output String
	*/
	function html_blank($string){
		if(strtolower(gettype($string)) == "string"){
			if($string == '&nbsp;'){
				$string = '';	
			}
			
			if(substr($string, strlen($string) - 6, 6) == '<br />'){
				$string = substr($string, 0, strlen($string) - 6);
			}
			$string = str_replace('<br />', "\r\n", $string);
			$string = html_entity_decode($string);
		}
		
		return utf8_encode($string);
	}
	
	/**
	* Return type
	* @param		Unknown		$item		Input variable
	* @return		Boolean		$type		Output is number as boolean
	*/
	function num_check($item){
		if(substr($item,0,1) == '0' && strlen($item) != 1){
			return false;	
		}
		
		if(is_numeric($item)){
			return true;	
		}
		
		return false;
	}
	
	/**
	* Return type
	* @param		Unknown		$item		Input variable
	* @return		Boolean		$type		Output is date as boolean
	*/
	function date_check($item){
		if( $item != "" && preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $item) ){
			return true;	
		}
		
		return false;
	}
	
	function load_selectors(){
		Global $UTILS_SERVER_PATH;
		require_once($UTILS_SERVER_PATH."includes/staff_selector.php");	
	}
}

?>