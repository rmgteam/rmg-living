<?php

require_once($UTILS_FILE_PATH."library/classes/array.class.php");
class mysql{
	
	var $conn;			//Variable to hold connection
	var $log;			//Variable to hold Log Object
	var $remove_array;	//Variable to hold chars to remove
	var $replace_array;	//variable to hold chard to replace
		
	/** 
	 * Set connection to global connection on creation of object
	 **/
	function mysql(){
		Global $conn;
		if($conn != ""){
			$this->conn = $conn;	
		}
		//$this->log = new log();
		
		$this->remove_array = array(
			array(
				"'",		//Normal accent
				chr(96),	//Grave Accent
				chr(145),	//Acute Accent
				chr(146),	//Aigu Accent
				chr(180),	//Acute Accent
				"&lsquo;", 	//Acute Accent HTML
				"&rsquo;", 	//Aigu Accent HTML
				chr(147),	//Left Quote
				chr(148),	//Right Quote
				chr(150),	//Long Dash
				chr(151),	//Longer Dash
				chr(133),	//... symbol
				chr(163),	//pound symbol
				chr(174),	//registered symbol
				chr(153),	//trademark symbol
				chr(149),	//bullet symbol
				chr(37),	//percent symbol,
				chr(186),	//degrees symbol
				"\' AND",	//to fix issue
				"\' OR",	//to fix issue
				"FROM \'",	//to fix issue
				"UPDATE \'",//to fix issue
				"INTO \'",	//to fix issue
				"\' SET",	//to fix issue
				"\' WHERE",	//to fix issue
				"\'WHERE",	//to fix issue
				"(\'",		//to fix issue
				"\')",		//to fix issue
				"= \'",		//to fix issue
				"=\'",		//to fix issue
				"\' ,",		//to fix issue
				"'\'",		//to fix issue
				"\''"		//to fix issue
			),
			array(
				"\'",
				"\'",
				"\'",
				"\'",
				"\'",
				"\'",
				"\'",
				'"',
				'"',
				'-',
				'-',
				'...',
				'&pound;',
				'&#174;',
				'&#153;',
				'&#149;',
				'&#37;',
				'&deg;',
				"' AND",
				"' OR",
				"FROM `",
				"UPDATE `",
				"INTO `",
				"` SET",
				"' WHERE",
				"' WHERE",
				"('",
				"')",
				"= '",
				"='",
				"\',",
				"''",
				"''"
			)
		);
		
		$this->replace_array = array(
			array(
				'&pound;',
				'&#169;',
				'&#174;',
				'&#153;',
				'&#149;',
				'&#37;',
				"\'"
			),
			array(
				chr(163),	//pound symbol
				chr(169),	//copyright symbol
				chr(174),	//registered symbol
				chr(153),	//trademark symbol
				chr(149),	//bullet symbol
				chr(37),	//percent symbol
				"'"			//Slash apostrophe issue
			)
		);
	}
	
	/** 
	 * Create new Connection
	 * 
	 * @param string $host		Host Name
	 * @param string $user		Username
	 * @param string $password	Password
	 **/
	function connect($host, $user, $password){
		$this->conn = mysql_connect($host,$user,$password);
		
		mysql_set_charset('utf8');
		
		return $this->conn;
	}
	
	/** 
	 * Select Database
	 * 
	 * @param string $db_name		Database Name
	 **/
	function select_db($db_name){
		mysql_select_db($db_name, $this->conn);
	}
	
	/**
	 * Submit Query
	 * 
	 * @param string $query		SQL Statement
	 * @param string $type		Friendly name to find in logs upon failure
	 **/
	function query($query, $type='SQL Query'){

		$has_error = false;
		
		$result = mysql_query($query, $this->conn) or $has_error = $this->do_log($query, $type, mysql_error($this->conn));
		
		if($has_error == true){
			return false;
		}else{
			return $result;
		}
	}
	
	/** 
	 * Insert
	 * 
	 * @param string $query		SQL Statement
	 * @param string $type		Friendly name to find in logs upon failure
	 **/
	function insert($query, $type_of='SQL Query'){
		
		$has_error = false;

		if(strpos($query, "ALTER") == 0){
			$new_query = $query;
		}else {
			$new_query = $this->remove_chars($query);
		}
		
		$result = mysql_query($new_query, $this->conn) or $has_error = true;
		
		if($has_error === true){
			$has_error = false;
			
			$new_query = $this->remove_chars($query, true);
			
			$result = mysql_query($new_query, $this->conn) or $has_error = true;
		}
		
		if($has_error === true){
			$this->do_log($new_query, $type_of, mysql_error($this->conn));
			return false;
		}else{
			
			//When we move to mySQLi use function not this statement 
			
			$last_id = '';
			
			$sql = "SELECT LAST_INSERT_ID()";
			$result = $this->query($sql, 'Get Last ID');
			$num_rows = $this->num_rows($result);
			if($num_rows > 0){
				while($row = $this->fetch_array($result)){
					$last_id = $row[0];
				}
			}

			return $last_id;
		}
	}
	
	/** 
	 * Retrieve Data
	 * 
	 * @param string $result	MySQL resource
	 * @param string $type_of	Type Description
	 * @param string $type		Type of fetch (MYSQL_BOTH as default)
	 **/
	function fetch_array($result, $type_of='MySQL Fetch Array', $type='MYSQL_BOTH'){
		if (is_bool($result)) {
			$this->do_log('Fetch Array', $type_of, "Invalid resource identifier passed to fetch_array.");
        	return false;
		}else{
			if ($type == 'MYSQL_ASSOC') $row = mysql_fetch_array($result, MYSQL_ASSOC);
			if ($type == 'MYSQL_NUM') $row = mysql_fetch_array($result, MYSQL_NUM);
			if ($type == 'MYSQL_BOTH') $row = mysql_fetch_array($result, MYSQL_BOTH);
	      
	      	if (!$row) return false;
			
	      	$row = $this->return_array($row);
			
			return $row;
		}
	}
	
	/**
	 * Return chars to vars
	 *
	 * @param array $array	MySQL array
	 **/
	function return_array($array, $direction="forward") {
		
		if(is_array($array)){
			$new_array = array();
			foreach ($array as $key => $value) {
				if (is_array($value)) {
					$new_array[$key] = $this->return_array($value);
					continue;
				}
				
				if($value != ''){
					if($direction=="forward"){
								
						$orig = $value;
						
						$arr = new arrays;
						
						if(strpos($value, chr(194)) !== false){
							$value = str_replace("â€“", "-", $value);
							$value = str_replace("Â£", "£", $value);
						}
						
						$inArray = $arr->strposa($value, $this->remove_array[0]);
						
						$utf8 = $this->is_utf8($value);
						
						if(!is_bool($inArray) && is_bool($utf8)){
							$value = $this->remove_chars($value, false, 'out');
						}
						
						$value = str_replace($this->replace_array[0], $this->replace_array[1], $value);
						$value_pr = $value;
						
						$utf8 = $this->is_utf8($value);
						
						if(!is_bool($utf8)){
							$new_value = utf8_encode($value);
							$utf8e = $this->is_utf8($new_value);
							
							if(count($utf8) == count($utf8e)){
								$value = $new_value;
							}
						}
						
						/*if(!is_numeric($value)){
							$value .= ' - ' . $value_pr;
						}*/
						
						$new_array[$key] = $value;

					}else{
						$new_array[$key] = str_replace($this->replace_array[1], $this->replace_array[0], utf8_decode($value));
					}
				}else{
					$new_array[$key] = $value;
				}
			
			}
			 
			return $new_array;
		}else{
			if($array != ''){
				if($direction=="forward"){
					
					$value = $array;
						
					$arr = new arrays;
					
					if($arr->strposa($value, $this->remove_array[0])){
						$value = $this->remove_chars($value, false, 'out');
					}
					
					$value = str_replace($this->replace_array[0], $this->replace_array[1], $value);
					$value_pr = $value;
					
					$utf8 = $this->is_utf8($value);
					
					if(!is_bool($utf8)){
						$new_value = utf8_encode($value);
						$utf8e = $this->is_utf8($new_value);
						
						if(count($utf8) == count($utf8e)){
							$value = $new_value;
						}
					}
					
					return $value;
				}else{
					return str_replace($this->replace_array[1], $this->replace_array[0], utf8_decode($array));
				}
			}else{
				return $array;
			}
		}
	}
	
	/** 
	 * Retrieve Number of Rows
	 * 
	 * @param string $result	MySQL resource
	 * @param string $type_of	Type Description
	 **/
	function num_rows($result, $type_of='MySQL Number of Rows'){
		if (is_bool($result)) {
			$this->do_log('Number of Rows', $type_of, "Invalid resource identifier passed to num_rows.");
        	return false;
		}else{
			return mysql_num_rows($result);
		}
	}
	
	/** 
	 * Seek to row number
	 * 
	 * @param string $result	MySQL resource
	 * @param string $row		Row number (0 as default)
	 * @param string $type_of	Type Description
	 **/
	function data_seek($result, $row=0, $type_of='MySQL Data Seek'){
		if (is_bool($result)) {
			$this->do_log('Data Seek to '.$row, $type_of, "Invalid resource identifier passed to data_seek.");
        	return false;
        }else{
			return mysql_data_seek($result, $row);
        }
	}
	
	/** 
	 * Add to Log
	 * 
	 * @param string $input		Data that was input
	 * @param string $type		Friendly name to find in logs
	 * @param string $output	Outcome of input
	 **/
	function do_log($input, $type, $output){
				
		//$this->log->string_log($input, $type, $output);
		
		return true;
	}
	
	/** 
	 * Remove fancy characters
	 * 
	 * @param string	$input			Data to be input
	 * @param boolean	$no_icon		UTF8 to ISO conversion
	 * @param string	$direction		Insert/Query (in), fetch_array (out)
	 **/
	function remove_chars($string, $no_icon = false, $direction = 'in'){

		$orig = $string;
		
		if($no_icon === false){
			$string = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $string);
			
			$s_array = explode(" ", $string);
			$o_array = explode(" ", $orig);
			if(count($s_array) != count($o_array)){
				$string = $orig;
			}
		}
		
		if($direction == 'in'){
			$input = trim(preg_replace('/\s\s+/', ' ', $string));
		}else{
			$input = trim($string);
		}
		
		$input = str_replace($this->remove_array[0], $this->remove_array[1], $input);
		$input = preg_replace("/\\\{2,}'/", "\\'", $input);
		
		$last = substr(trim($input), -2);
		if($last == "\'"){
			$input = substr($input, 0, -2) . "'";
		}
		
		$input_array = explode("\',", $input);
		$found_array = array();
		
		for($i = 0; $i<count($input_array); $i++){
			$sub_array = explode("'", $input_array[$i]);
			for($j = 0; $j<count($sub_array); $j++){
				if(strpos($sub_array[$j], '=') !== false && strpos($sub_array[$j], '<') === false){
					if(!in_array($i, $found_array)){
						array_push($found_array, $i);
					}
				}
			}
		}
		
		$output = '';
		
		for($i = 0; $i<count($input_array); $i++){
			if($input_array[$i] != ''){
				$output .=  $input_array[$i];
				if($direction == 'in'){
					if(in_array($i, $found_array)){
						$output .= "',";
					}else{
						$output .= "\',";
					}
				}
			}
		};
		
		if(strpos($output, "\r") !== false){
			$output=str_replace("\r", "\n", $output);
		}

		$last = substr(trim($output), -2);
		if($last == "',"){
			$output = substr($output, 0, -2);
		}
		
		$last = substr(trim($output), -2);
		if($last == "\'"){
			$output = substr($output, 0, -2) . "'";
		}elseif($last == "'\\"){
			$output = substr($output, 0, -1);
		}
		
		$last = substr(trim($output), -1);
		if($last == "\\"){
			$output = substr($output, 0, -1);
		}
				
		return $output;
	}
	
	function is_utf8($str) {
		
		$char_array = array("160", "133", "186");
		$found_array = array();
		
		for ($i = 0; $i < strlen($str); $i++){
			$char = ord($str[$i]);
    		if ($char > 127 && $char < 192){
    			$arr = new arrays;
    			if(is_bool($arr->strposa($char, $char_array))){
    				$found_array[] = $char;
    			}
    		}
		}
		if(count($found_array) > 0){
			return $found_array;	
		}else{
			return false;
		}
	}
}

?>
