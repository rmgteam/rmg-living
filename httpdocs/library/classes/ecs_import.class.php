<?php
require_once($UTILS_FILE_PATH."library/classes/field.class.php");
require_once($UTILS_FILE_PATH."library/classes/data.class.php");
require_once($UTILS_FILE_PATH."library/classes/mailer.class.php");


class ecs_import {
	
	var $report;
	var $property_counter;
	var $error_occurred;
	var $curr_brand_name;
	
	function ecs_import($brand){

		global $UTILS_ECS_EXPORT_PATH;
		
		$ftp_folder_path = $UTILS_ECS_EXPORT_PATH;
		$starttime = time();

		// Get a new ECS counter
		$sql_counter = "SELECT ecs_import_counter FROM cpm_system";
		$result_counter = @mysql_query($sql_counter);
		$row_counter = @mysql_fetch_row($result_counter);
		$ecs_import_counter = $row_counter[0] + 1;
		$sql_counter = "UPDATE cpm_system SET ecs_import_counter = ".$ecs_import_counter;
		@mysql_query($sql_counter);
		
		$this->report = '';
		
		// Loop for each version of the ECS database
		$sql_sub = "
		SELECT * 
		FROM cpm_subsidiary 
		WHERE 
		subsidiary_ecs_import = 'Y' AND 
		subsidiary_discon = 'N'
		AND subsidiary_code = '".$brand."'";
		$result_sub = @mysql_query($sql_sub);
		$num_subsidiaries = @mysql_num_rows($result_sub);
		
		if($num_subsidiaries > 0){
			while($row_sub = @mysql_fetch_array($result_sub)){
				
				$this->curr_brand_name = stripslashes($row_sub['subsidiary_name']);
				
				$this->report .= $this->curr_brand_name.":
				=========================================
				";
				
				$this->import_property($row_sub, $ftp_folder_path, $ecs_import_counter);
				
			}
		}
		
		
		$endtime = time();
		$script_duration = ($endtime - $starttime);
		$this->report .= $this->property_counter." PROPERTY ONLY rows imported (Total)
		";
		$this->report .= "SCRIPT DURATION $script_duration sec
		
		";
		
		$this->mail_report();
	}
	
	
	
	function import_property($row_sub, $ftp_folder_path, $ecs_import_counter){

		$field = new field;
		$import_file_name = $ftp_folder_path.$row_sub['subsidiary_code']."_ecs_property.csv";
	
		// If import file does not exist, send error
		if(!file_exists($import_file_name)){
			
			$this->report .= "Import File [$import_file_name] Does Not Exist\n";
			print "FILE (".$row_sub['subsidiary_code'].") DOES NOT EXIST\n";
		}else{
	
			$handle = fopen($import_file_name, "r");
			$property_counter = 0;
			$rows_missed = 0;
			$rows_to_skip = 1;
			while (($data = fgetcsv($handle, 1000, ",", '"')) !== FALSE) {
				
				if($property_counter < $rows_to_skip){
					$property_counter++;continue;
				}
				
				// Do not import any inactive properties
				if( trim($data[13]) != "1" ){
					continue;
				}
				
				$property_counter++;
				
				print $row_sub['subsidiary_name'].":Property Record ".$property_counter."\n";
				
				$rmc_lookup = "";
				$rmc_lookup_exists = "N";
				
				$ecs_rmc_ref = addslashes( ltrim( ltrim( ltrim( ltrim( trim($data[0]), "z-"), "Z-"), "z"), "Z") );
				$ecs_rmc_name = addslashes(trim($data[1]));
				$ecs_pm_name = addslashes(trim($data[2]));
				$ecs_rm_name = trim($data[3]);
				$ecs_od_name = trim($data[4]);
				$ecs_sca = trim($data[5]);
				$ecs_sla_level = addslashes(trim($data[7]));
				$ecs_owner = addslashes( trim($data[11]) );
				$ecs_co_house_num = trim($data[12]);			
				$is_active = trim($data[13]);
				$hcs_dir = trim($data[14]);
				$owner_ref = trim($data[15]);
				$region = trim($data[24]);
				
				$is_z = 'N';
				
				if((substr(trim($data[0]), 0, 1) == 'Z' || substr(trim($data[0]), 0, 1) == 'z') && (substr(trim($data[0]), 1, 1) != '-')){
					$hcs = 'N';
					$dir = 'N';
					$is_z = 'Y';
				}else{
					if(strpos($hcs_dir, "/") > -1 && strtoupper($hcs_dir) != 'N/A'){
						$hcs_dir_arr = explode("/", $hcs_dir);
						$hcs = $hcs_dir_arr[0];
						$dir = $hcs_dir_arr[1];
						
						if($hcs == 'Yes'){
							$hcs = 'Y';
						}else{
							$hcs = 'N';
						}
						
						if($dir == 'Yes'){
							$dir = 'Y';
						}else{
							$dir = 'N';
						}
					}else{
						$dir = 'N';
						$hcs = 'N';
					}
				}
				
				$owner_request['hsc'] = $hcs;
				$owner_request['dir'] = $dir;
				$owner_request['owner_ref'] = $owner_ref;
				$owner_request['co_owner_name'] = $ecs_owner;
				$owner_request['co_house_num'] = $ecs_co_house_num;
				$owner_request['subsidiary_id'] = $row_sub['subsidiary_id'];
				
				$owner_id = $this->owner($owner_request, $is_z);
				$rmc_lookup = $this->rmc_lookup($ecs_rmc_ref, $row_sub['subsidiary_id']);
				
				$rmc_request['rmc_lookup'] = $rmc_lookup;
				$rmc_request['subsidiary_id'] = $row_sub['subsidiary_id'];
				$rmc_request['brand_name'] = '~';
				$rmc_request['rmc_name'] = trim($ecs_rmc_name);
				$rmc_request['property_manager_name'] = trim($ecs_pm_name);
				$rmc_request['rmc_status'] = '~';
				$rmc_request['is_rmc_active'] = $is_active;
				$rmc_request['service_charge_accountant'] = trim($ecs_sca);
				$rmc_request['service_charge_accountant_ref'] = '~';
				$rmc_request['regional_manager'] = trim($ecs_rm_name);
				$rmc_request['ops_director_name'] = $ecs_od_name;
				$rmc_request['sla'] = '~';
				$rmc_request['co_house'] = $ecs_co_house_num;
				$rmc_request['co_owner'] = $ecs_owner;
				$rmc_request['region'] = $region;
				
				$this->rmc($rmc_request, $ecs_import_counter, $owner_id);
				
			}
			
			$this->report .= "PROPERTY ONLY Data Imported: ".$property_counter."
				
			";
			
			$this->property_counter = $property_counter;
		}
	}
	
	
	function rmc($request, $ecs_import_counter, $owner_id='0'){
		
		$sql_rmc = "
		SELECT count(*) 
		FROM cpm_rmcs 
		WHERE 
		rmc_num = '".$request['rmc_lookup']."' AND 
		subsidiary_id = '".$request['subsidiary_id']."'";
		$result_rmc = @mysql_query($sql_rmc);
		$row_rmc = @mysql_fetch_row($result_rmc);
		if($row_rmc[0] == "0"){
			$sql_rmc = "INSERT INTO cpm_rmcs SET
			date_created = '".date("H:i d/m/Y", time())."',";
		}else{
			$sql_rmc = "UPDATE cpm_rmcs SET
			date_last_updated = '".date("H:i d/m/Y", time())."',";
		}
		
		$sql_rmc .= "
		subsidiary_id = '".$request['subsidiary_id']."',
		rmc_num = '".$request['rmc_lookup']."',";
		
		if($request['brand_name'] != '~'){
			$sql_rmc .= "
			brand_name = '".$request['brand_name']."',";
		}
		if($request['rmc_name'] != '~'){
			$sql_rmc .= "
			rmc_name = '".$request['rmc_name']."',";
		}
		if($request['property_manager_name'] != '~'){
			$sql_rmc .= "
			property_manager = '".$request['property_manager_name']."',";
		}
		if($request['rmc_status'] != '~'){
			$sql_rmc .= "
			rmc_status = '".$request['rmc_status']."',";
		}
		if($request['is_rmc_active'] != '~'){
			$sql_rmc .= "
			rmc_is_active = ".$request['is_rmc_active'].",";
		}
		if($request['regional_manager'] != '~'){
			$sql_rmc .= "
			regional_manager = '".$request['regional_manager']."',";
		}
		if($request['ops_director_name'] != '~'){
			$sql_rmc .= "
			rmc_op_director_name = '".$request['ops_director_name']."',";
		}
		if($owner_id != '0'){
			$sql_rmc .= "
			owner_id = '".$owner_id."',";
		}
		if($request['sla'] != '~'){
			$sql_rmc .= "
			rmc_service_level = '".$request['sla']."',";
		}
		if($request['region'] != '~'){
			$sql_rmc .= "
			rmc_region = '".$request['region']."',";
		}
		$sql_rmc .= "
		ecs_import_counter = '".$ecs_import_counter."'";	

		if($row_rmc[0] == "0"){
			if( @mysql_query($sql_rmc) === false ){
				
				$this->error_occurred = "Y";
				$this->report .=  "(1) SQL failed for insert of rmc (lookup id ".$request['rmc_lookup']."): ".$sql_rmc."
				".mysql_error()."
				
				";
			}
		}else{
			$sql_rmc .= "
			WHERE rmc_num = '".$request['rmc_lookup']."' AND subsidiary_id = '".$request['subsidiary_id']."'";
			
			if( @mysql_query($sql_rmc) === false ){
				
				$this->error_occurred = "Y";
				$this->report .=  "(2) SQL failed for update of rmc (lookup id ".$request['rmc_lookup'].") : ".$sql_rmc."
				".mysql_error()."
				
				";
			}
		}
	}
	
	
	function rmc_lookup($rmc_ref, $subsidiary_id){
		
		$sql_lookup = "
		SELECT rmc_lookup 
		FROM cpm_lookup_rmcs 
		WHERE 
		(rmc_ref = '".$rmc_ref."' AND subsidiary_id = '".$subsidiary_id."') 
		";
		$result_lookup = @mysql_query($sql_lookup);
		$ref_exists = @mysql_num_rows($result_lookup);
		if($ref_exists > 0){
			$row_lookup = @mysql_fetch_row($result_lookup);
			$rmc_lookup = $row_lookup[0];
		}else{
			$sql_lookup = "
			INSERT INTO cpm_lookup_rmcs SET 
			rmc_ref = '".$rmc_ref."',
			subsidiary_id = '".$subsidiary_id."',
			date_created = '".date("H:i d/m/Y", time())."' 
			";
			if( mysql_query($sql_lookup) ){
				$sql_lu = "SELECT LAST_INSERT_ID() FROM cpm_lookup_rmcs";
				$result_lu = @mysql_query($sql_lu);
				$row_lu = @mysql_fetch_row($result_lu);
				$rmc_lookup = $row_lu[0];	
			}
		}
		
		return $rmc_lookup;
	}
	
	
	function owner($request, $is_z){
		
		$owner_id = '0';
		
		$sql_lookup = "
		SELECT owner_id
		FROM cpm_owner
		WHERE 
		owner_ref = '".$request['owner_ref']."' AND 
		subsidiary_id = '".$request['subsidiary_id']."'";		
		$result_lookup = @mysql_query($sql_lookup);
		$ref_exists = @mysql_num_rows($result_lookup);
		
		if($ref_exists > 0){
			$row_lookup = @mysql_fetch_row($result_lookup);
			$owner_id = $row_lookup[0];
			
			$sql_lookup = "UPDATE cpm_owner SET 
			date_updated = '".date("H:i d/m/Y", time())."',";
		}else{
			$sql_lookup = "INSERT INTO cpm_owner SET 
			date_created = '".date("H:i d/m/Y", time())."',";
		}
		
		$sql_lookup .= "
		owner_ref = '".$request['owner_ref']."',
		owner_name = '".$request['co_owner_name']."',
		subsidiary_id = '".$request['subsidiary_id']."',
		owner_co_house_num = '".$request['co_house_num']."',
		
		owner_is_hcs = '".$request['hsc']."',
		owner_is_dir = '".$request['dir']."'";
		
		if($is_z == 'N'){
			if($ref_exists > 0){
				$sql_lookup .= "
				WHERE owner_ref = '".$request['owner_ref']."'";
				if( !@mysql_query($sql_lookup) ){
					$this->error_occurred = "Y";
					$this->report .=  "(1) SQL failed for update of owner (owner ref ".$request['owner_ref']."): ".$sql_lookup."
					".mysql_error()."
					
					";	
				}
			}
		}
		
		if($ref_exists == 0){
			if( @mysql_query($sql_lookup) ){
				$sql_lu = "SELECT LAST_INSERT_ID() FROM cpm_owner";
				$result_lu = @mysql_query($sql_lu);
				$row_lu = @mysql_fetch_row($result_lu);
				$owner_id = $row_lu[0];
			}else{
				$this->error_occurred = "Y";
				$this->report .=  "(1) SQL failed for insert of owner (owner ref ".$request['owner_ref']."): ".$sql_lookup."
				".mysql_error()."
				
				";	
			}
		}
		
		return $owner_id;
	}
	
	
	
	// Mail report function
	function mail_report(){
		
		GLOBAL $UTILS_IMPORT_EMAIL;
		
		$email = new mailer();
		$email->set_mail_type("ECS to RMG Living Property ONLY Import Report");
		$email->set_mail_to($UTILS_IMPORT_EMAIL);
		$email->set_mail_from("webteam@rmguk.com");
		$email->set_mail_subject("ECS to RMG Living Property ONLY Import Report (".$this->curr_brand_name.")");
		$email->set_mail_message($this->report);
		$email->set_mail_headers('');
		$email->send();
		
		print $this->report;
	}
		
}

?>