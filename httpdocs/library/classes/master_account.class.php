<?
require_once($UTILS_CLASS_PATH."security.class.php");
require_once($UTILS_CLASS_PATH."encryption.class.php");
require_once($UTILS_CLASS_PATH."field.class.php");
require_once($UTILS_CLASS_PATH."rmc.class.php");
require_once($UTILS_CLASS_PATH."resident.class.php");
require_once($UTILS_CLASS_PATH."unit.class.php");
require_once($UTILS_CLASS_PATH."data.class.php");


class master_account {
	
	var $master_account_id;
	var $master_account_serial;
	var $master_account_username;
	var $master_account_password;
	var $master_account_email;
	var $master_account_tel;
	var $master_account_mobile;
	var $master_account_question_id_1;
	var $master_account_question_id_2;
	var $master_account_answer_1;
	var $master_account_answer_2;
	var $master_account_optout_marketing;
	var $master_account_allow_password_reset;
	var $master_account_created_ts;

	
	function master_account($ref="", $ref_type="serial"){
		
		if($ref_type == "id"){
			$ref_clause = " master_account_id = $ref ";
		}
		else{
			$ref_clause = " master_account_serial = '$ref' ";
		}
		
		$sql = "
		SELECT * 
		FROM cpm_master_accounts 
		WHERE 
		$ref_clause
		";
		$result = @mysql_query($sql);
		$row = @mysql_fetch_array($result);
		
		$this->master_account_id = $row['master_account_id'];
		$this->master_account_serial = $row['master_account_serial'];
		$this->master_account_username = $row['master_account_username'];
		$this->master_account_password = $row['master_account_password'];
		$this->master_account_email = $row['master_account_email'];
		$this->master_account_tel = $row['master_account_tel'];
		$this->master_account_mobile = $row['master_account_mobile'];
		$this->master_account_question_id_1 = $row['master_account_question_id_1'];
		$this->master_account_question_id_2 = $row['master_account_question_id_2'];
		$this->master_account_answer_1 = $row['master_account_answer_1'];
		$this->master_account_answer_2 = $row['master_account_answer_2'];
		$this->master_account_optout_marketing = $row['master_account_optout_marketing'];
		$this->master_account_allow_password_reset = $row['master_account_allow_password_reset'];
		$this->master_account_created_ts = $row['master_account_created_ts'];

	}
	
	
	function check_fields($request){
		
		$field = new field;
		
		if($request['mausername'] == ""){return "Please complete all the required fields.";}
		if(strlen($request['mausername']) < 8 || strlen($request['mausername']) > 30){return "Your username must be between 8 and 30 characters in length.";}
		if( preg_match('[^a-zA-Z0-9-_]', $request['mausername']) ){return "Your username can only contain letters, numbers, hyphens and underscores.";}
		
		if(!$this->is_username_unique($request['mausername'])){return "The username you have supplied is unavailable. Please either enter a different one or <a href='my_properties.php'>click here to log in</a>.";}
		
		if($this->master_account_id == "" || $this->master_account_allow_password_reset == "Y"){
			if($request['new_password'] == ""){return "Please complete all the required fields.";}
			if($request['new_password_confirm'] == ""){return "Please complete all the required fields.";}
		}
		if($request['new_password'] != ""){ // Need this here for update purposes
			if($request['new_password'] != $request['new_password_confirm']){return "The password you specified in the 'New Password' box does not match the one you typed in the 'Confirm New Password' box.";}
			if(strlen($request['new_password']) < 8 || strlen($request['new_password']) > 16){return "Your password must be at least 8 and no more than 16 characters in length.";}
			if( preg_match('/[^a-zA-Z0-9-_]/', $request['new_password']) ){return "Your password can only contain letters, numbers, hyphens and underscores.";}
			if( !preg_match('/[0-9]/', $request['new_password']) ){return "Your password must contain at least one number.";}
		}
		
		if($request['security_question_1'] == ""){return "Please complete all the required fields.";}
		if($request['security_answer_1'] == ""){return "Please complete all the required fields.";}
		if($request['security_question_2'] == ""){return "Please complete all the required fields.";}
		if($request['security_answer_2'] == ""){return "Please complete all the required fields.";}
		
		if($request['security_question_1'] == $request['security_question_2']){return "Please choose two different security questions.";}
		if(($request['security_question_1'] == "4" && !$field->is_valid_date($request['security_answer_1'])) || ($request['security_question_2'] == "4" && !$field->is_valid_date($request['security_answer_2']))){return "Please provide a valid date of birth.";}
		
		if($request['tel'] == ""){return "Please complete all the required fields.";}
		if($request['email'] == ""){return "Please complete all the required fields.";}
		if(!$field->is_valid_email($request['email'])){return "Please provide a valid email address.";}
		if(!$this->is_email_unique($request['email'])){return "The email you have supplied is unavailable. Please either enter a different one or <a href='my_properties.php'>click here to log in</a>.";}
		
		return true;
	}
	
	
	function save($request){
		
		global $UTILS_DB_ENCODE;
		global $UTILS_FILE_PATH;
		$security = new security;
		$crypt = new encryption_class;
		$data = new data;
		
		if($this->master_account_id == ""){
			
			$unit = new unit;
			$unit->set_unit($_SESSION['resident_num']);
			
			// First check to see if resident account is assoc to another Master Account
			if($this->does_assoc_resident_exist($_SESSION['resident_num']) === true){
				return "The property '".stripslashes($unit->get_unit_desc())."' is already associated to another Master Account.";	
			}
			
			$sql = "
			INSERT INTO cpm_master_accounts (
				master_account_serial,
				master_account_username,
				master_account_password,
				master_account_email,
				master_account_tel,
				master_account_mobile,
				master_account_question_id_1,
				master_account_question_id_2,
				master_account_answer_1,
				master_account_answer_2,
				master_account_optout_marketing,
				master_account_created_ts
			)VALUES(
				'".$security->gen_serial(32)."',
				'".$security->clean_query($request['mausername'])."',
				'".$security->clean_query($crypt->encrypt($UTILS_DB_ENCODE, $request['new_password']))."',
				'".$security->clean_query($request['email'])."',
				'".$security->clean_query($request['tel'])."',
				'".$security->clean_query($request['mobile'])."',
				".$security->clean_query($request['security_question_1']).",
				".$security->clean_query($request['security_question_2']).",
				'".$security->clean_query($request['security_answer_1'])."',
				'".$security->clean_query($request['security_answer_2'])."',
				'".$security->clean_query($request['optout_marketing'])."',
				'".time()."'
			)
			";
			if( @mysql_query($sql) ){
			
				$sql_id = "SELECT LAST_INSERT_ID() FROM cpm_master_accounts";
				$result_id = @mysql_query($sql_id);
				$row_id = @mysql_fetch_row($result_id);
				$this->master_account($row_id[0], "id");
			
				// Associate the account they are logged in with
				$this->assoc_resident_to_master_account();
				
				// Send email to user about Master Account being set up
				$star_username = $data->star_str($this->master_account_username, 2);
				include($UTILS_FILE_PATH."includes/ma_new_account_msg.php");
				@mail($this->master_account_email,"RMG Living - Master Account ...",$msg,"From:customerservice@rmguk.com");
				
				// Link to account they had logged with (hence disabling its login)
				$resident = new resident($_SESSION['resident_num']);
				if( $resident->link_to_master_account() === true ){
					
					$_SESSION['master_account_serial'] = $this->master_account_serial;
					return true;
				}
			}
		}
		else{
			
			if($request['new_password'] != ""){
				$password = $security->clean_query($crypt->encrypt($UTILS_DB_ENCODE, $request['new_password']));
			}
			else{
				$password = $this->master_account_password;
			}
			
			$sql = "
			UPDATE cpm_master_accounts SET
				master_account_username = '".$security->clean_query($request['mausername'])."',
				master_account_password = '".$password."',
				master_account_email = '".$security->clean_query($request['email'])."',
				master_account_tel = '".$security->clean_query($request['tel'])."',
				master_account_mobile = '".$security->clean_query($request['mobile'])."',
				master_account_question_id_1 = ".$security->clean_query($request['security_question_1']).",
				master_account_question_id_2 = ".$security->clean_query($request['security_question_2']).",
				master_account_answer_1 = '".$security->clean_query($request['security_answer_1'])."',
				master_account_answer_2 = '".$security->clean_query($request['security_answer_2'])."',
				master_account_optout_marketing = '".$security->clean_query($request['optout_marketing'])."',
				master_account_allow_password_reset = 'N' 
			WHERE master_account_id = ".$this->master_account_id."
			";
			if(@mysql_query($sql)){
				$this->master_account($this->master_account_id, "id");
				return true;
			}
		}
		
		return "Technical fault - there was a problem saving your details.";
	}
	
	
	function assoc_resident_to_master_account(){
		
		// Check if a default property exists, if not, make this one the default...
		$is_default = "N";
		if( $this->does_master_account_default_assoc_resident_exist() !== true ){
			$is_default = "Y";
		}
		
		if( $this->does_assoc_resident_exist($_SESSION['resident_num']) !== true ){
		
			$sql = "
			INSERT INTO cpm_master_account_resident_assoc (
				master_account_id,
				resident_num,
				master_account_resident_assoc_default
			)VALUES(
				".$this->master_account_id.",
				".$_SESSION['resident_num'].",
				'".$is_default."'
			)
			";
			if( @mysql_query($sql) !== false ){
				return true;	
			}
		}
		else{
			return true;	
		}
		
		return false;
	}
	
	
	function is_username_unique($username){
		
		$security = new security;
		
		if($this->master_account_id  != ""){
			$id_clause = " master_account_id <> ".$this->master_account_id." AND ";
		}
		
		$sql = "
		SELECT count(*) 
		FROM cpm_master_accounts 			
		WHERE 
		$id_clause 
		master_account_username = '".$security->clean_query($username)."' 
		";
		$result = @mysql_query($sql);
		$row = @mysql_fetch_row($result);
		if($row[0] > 0){
			return false;
		}
		else{
			
			// Must also check against resident ref's, so as to avoid a conflict when using the login/password reminder process (as these two processes check against residents account first)
			$sql = "
			SELECT count(*) 
			FROM cpm_lookup_residents  			
			WHERE 
			resident_lookup = '".$security->clean_query($username)."' 
			";
			$result = @mysql_query($sql);
			$row = @mysql_fetch_row($result);
			if($row[0] > 0){
				return false;
			}
		
			return true;
		}
	}
	
	
	function is_email_unique($email){
		
		$security = new security;
		
		if($this->master_account_email  != ""){
			$id_clause = " master_account_email <> '".$this->master_account_email."' AND ";
		}
		
		$sql = "
		SELECT count(*) 
		FROM cpm_master_accounts 			
		WHERE 
		$id_clause 
		master_account_email = '".$security->clean_query($email)."'
		";
		$result = @mysql_query($sql);
		$row = @mysql_fetch_row($result);
		if($row[0] > 0){
			return false;
		}
		else{
			return true;
		}
	}
	
	
	function does_assoc_resident_exist($resident_num){
		
		$sql = "
		SELECT count(*) 
		FROM cpm_master_account_resident_assoc maa, cpm_residents r, cpm_rmcs rmc, cpm_lookup_rmcs lrmc 
		WHERE 
		maa.resident_num = r.resident_num AND 
		rmc.rmc_num = lrmc.rmc_lookup AND 
		r.rmc_num = rmc.rmc_num AND 
		rmc.rmc_is_active = 1 AND 
		r.resident_is_active = 1 AND 
		maa.resident_num = ".$resident_num."
		ORDER BY u.unit_description ASC, u.unit_address_1 ASC";
		$result = @mysql_query($sql);
		$row = @mysql_fetch_array($result);
		if($row[0] > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	
	function does_master_account_default_assoc_resident_exist(){
		
		$sql = "
		SELECT count(*) 
		FROM cpm_master_account_resident_assoc maa, cpm_residents r, cpm_rmcs rmc, cpm_lookup_rmcs lrmc 
		WHERE 
		maa.resident_num = r.resident_num AND 
		rmc.rmc_num = lrmc.rmc_lookup AND 
		r.rmc_num = rmc.rmc_num AND 
		rmc.rmc_is_active = 1 AND 
		r.resident_is_active = 1 AND 
		maa.master_account_resident_assoc_default = 'Y' AND 
		maa.master_account_id = ".$this->master_account_id." 
		";
		$result = @mysql_query($sql);
		$row = @mysql_fetch_array($result);
		if($row[0] > 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	
	function get_master_account_default_assoc_resident(){
		
		if( $this->does_master_account_default_assoc_resident_exist() === true ){
			
			$sql = "
			SELECT * 
			FROM cpm_master_account_resident_assoc maa, cpm_residents r, cpm_residents_extra rex, cpm_lookup_residents lres, cpm_rmcs rmc, cpm_lookup_rmcs lrmc 
			WHERE 
			maa.resident_num = r.resident_num AND 
			r.resident_num = rex.resident_num AND 
			r.resident_num = lres.resident_lookup AND 
			r.rmc_num = rmc.rmc_num AND 
			rmc.rmc_num = lrmc.rmc_lookup AND 
			rmc.rmc_is_active = 1 AND 
			r.resident_is_active = 1 AND 
			maa.master_account_resident_assoc_default = 'Y' AND 
			maa.master_account_id = ".$this->master_account_id." 
			";
			//print $sql;exit;
			$result = @mysql_query($sql);
			$row = @mysql_fetch_array($result);
			return $row;
		}
		else{
			
			// As no default exists, create a default...
			$sql = "
			SELECT * 
			FROM cpm_master_account_resident_assoc maa, cpm_residents r, cpm_residents_extra rex, cpm_rmcs rmc, cpm_lookup_rmcs lrmc, cpm_lookup_residents lres 
			WHERE 
			maa.resident_num = r.resident_num AND 
			r.resident_num = rex.resident_num AND 
			r.resident_num = lres.resident_lookup AND 
			rmc.rmc_num = lrmc.rmc_lookup AND 
			r.rmc_num = rmc.rmc_num AND 
			rmc.rmc_is_active = 1 AND 
			r.resident_is_active = 1 AND 
			maa.master_account_resident_assoc_default = 'N' AND 
			maa.master_account_id = ".$this->master_account_id." 
			";
			$result = @mysql_query($sql);
			$num = @mysql_num_rows($result);
			if($num > 0){
			
				$row = @mysql_fetch_array($result);
				
				$sql2 = "
				UPDATE cpm_master_account_resident_assoc SET
					master_account_resident_assoc_default = 'Y'
				WHERE master_account_resident_assoc_id = ".$row['master_account_resident_assoc_id']."
				";
				@mysql_query($sql2);
				
				return $row;
			}
		}
		
		return false;
	}
	
	
	function get_master_account_assoc_residents($rmc_num="", $prop_name=""){
		
		if($rmc_num != ""){
			$rmc_clause = " rmc.rmc_num = ".$rmc_num." AND ";
		}
		if($prop_name != ""){
			$prop_clause = " (u.unit_description LIKE '%".addslashes($prop_name)."%' OR u.unit_address_1 LIKE '%".addslashes($prop_name)."%') AND ";
		}
		
		$sql = "
		SELECT * 
		FROM cpm_master_account_resident_assoc maa, cpm_residents r, cpm_residents_extra rex, cpm_rmcs rmc, cpm_lookup_rmcs lrmc, cpm_units u  
		WHERE 
		$rmc_clause 
		$prop_clause 
		u.resident_num = r.resident_num AND 
		rex.resident_num = r.resident_num AND 
		maa.resident_num = r.resident_num AND 
		rmc.rmc_num = lrmc.rmc_lookup AND 
		r.rmc_num = rmc.rmc_num AND 
		rmc.rmc_is_active = 1 AND 
		r.resident_is_active = 1 AND 
		maa.master_account_id = ".$this->master_account_id." ORDER BY u.unit_description,u.unit_address_1
		";
		$result = @mysql_query($sql);
		return $result;
	}
	
	
	function get_master_account_assoc_rmcs(){
		
		$sql = "
		SELECT * 
		FROM cpm_master_account_resident_assoc maa, cpm_residents r, cpm_rmcs rmc, cpm_lookup_rmcs lrmc 
		WHERE 
		maa.resident_num = r.resident_num AND 
		rmc.rmc_num = lrmc.rmc_lookup AND 
		r.rmc_num = rmc.rmc_num AND 
		rmc.rmc_is_active = 1 AND 
		r.resident_is_active = 1 AND 
		maa.master_account_id = ".$this->master_account_id." 
		GROUP BY rmc.rmc_num 
		ORDER BY rmc.rmc_name ASC
		";
		$result = @mysql_query($sql);
		return $result;
	}
	
	
	function send_forgot_username($email){
		
		global $UTILS_DB_ENCODE;
		$security = new security;
		$crypt = new encryption_class;
		
		// Update master account
		$sql = "
		UPDATE cpm_master_accounts SET
			master_account_allow_password_reset = 'Y'
		WHERE master_account_id = ".$this->master_account_id."
		";
		if( @mysql_query($sql) ){
			
			// Set email headers
			$headers = "From: RMG Living Support <customerservice@rmguk.com>\nReply-To: RMG Living Support <customerservice@rmguk.com>";
			
			// Send email password
$body_pw = "
Below is a reminder of the username for your Master Account on the RMG Living website - www.rmgliving.co.uk

".$this->master_account_username."

If you experience problems accessing the RMG Living website, please contact our Customer Services Team via customerservice@rmguk.com


Kind Regards,
RMG Living Support
	
(This email was generated automatically.)
";

			if( mail($email, "RMG Living", $body_pw, $headers) ){
				return true;	
			}
			else{
				return false;	
			}
		}
		else{
			return false;	
		}
	}
	
	
	function send_forgot_password($email){
		
		global $UTILS_DB_ENCODE;
		$security = new security;
		$crypt = new encryption_class;
		
		$password_unique = "N";
		while($password_unique == "N"){
			
			$password = $security->gen_serial(8, false, true);
			$sql = "
			SELECT master_account_password 
			FROM cpm_master_accounts  
			WHERE 
			master_account_password = '".$security->clean_query($crypt->encrypt($UTILS_DB_ENCODE, $password))."'";
			$result = @mysql_query($sql);
			$password_exists = @mysql_num_rows($result);
			if($password_exists < 1){
				$password_unique = "Y";
				break;
			}
		}
		
		// Update master account
		$sql = "
		UPDATE cpm_master_accounts SET
			master_account_password = '".$security->clean_query($crypt->encrypt($UTILS_DB_ENCODE, $password))."',
			master_account_allow_password_reset = 'Y'
		WHERE master_account_id = ".$this->master_account_id."
		";
		if( @mysql_query($sql) ){
			
			// Set email headers
			$headers = "From: RMG Living Support <customerservice@rmguk.com>\nReply-To: RMG Living Support <customerservice@rmguk.com>";
			
			// Send email password
$body_pw = "
Below is the new password for your Master Account on the RMG Living website - www.rmgliving.co.uk

".$password."

Once logged in, you can change you password to something more memorable in the 'My Account' section. If you experience problems accessing the RMG Living website, please contact our Customer Services Team via customerservice@rmguk.com


Kind Regards,
RMG Living Support
	
(This email was generated automatically.)
";

			if( mail($email, "RMG Living", $body_pw, $headers) ){
				return true;	
			}
			else{
				return false;	
			}
		}
		else{
			return false;	
		}
	}
	
	
	function is_resident_assoc($ref){
		
		$security = new security;
		
		$sql = "
		SELECT count(*) 
		FROM cpm_master_account_resident_assoc maa, cpm_residents r, cpm_rmcs rmc, cpm_lookup_rmcs lrmc 
		WHERE 
		maa.resident_num = r.resident_num AND 
		rmc.rmc_num = lrmc.rmc_lookup AND 
		r.rmc_num = rmc.rmc_num AND 
		rmc.rmc_is_active = 1 AND 
		r.resident_is_active = 1 AND 
		r.resident_serial = '".$security->clean_query($ref)."' AND 
		r.resident_serial <> '' AND 
		r.resident_serial IS NOT NULL AND 
		maa.master_account_id = ".$this->master_account_id." 
		";
		$result = @mysql_query($sql);
		$row = @mysql_fetch_row($result);
		if($row[0] > 0){
			return true;
		}
		return false;
	}
	
	
	function switch_property($resident_serial){
		
		$security = new security;
		
		$sql_resident = "
		SELECT re.rmc_num, rex.allow_password_reset, rex.is_first_login, re.resident_name, rex.is_demo_resident_account, rex.login_expiry_YMD, lrmc.rmc_ref, lre.resident_ref, re.resident_num, rex.is_developer  
		FROM cpm_residents re, cpm_lookup_residents lre, cpm_residents_extra rex, cpm_rmcs rmc, cpm_lookup_rmcs lrmc 
		WHERE 
		lrmc.rmc_lookup=rmc.rmc_num AND 
		lre.resident_lookup=re.resident_num AND 
		re.rmc_num=rmc.rmc_num AND 
		re.resident_num=rex.resident_num AND 
		(rmc.rmc_status = 'Active' OR rmc.rmc_is_active = '1') AND 
		(re.resident_status = 'Current' OR re.resident_is_active = '1') AND 
		re.resident_serial = '".$security->clean_query($resident_serial)."'
		";
		$result_resident = @mysql_query($sql_resident);
		$num_valid_users = @mysql_num_rows($result_resident);
		if($num_valid_users > 0){
		
			$row_resident = @mysql_fetch_row($result_resident);
		
			$_SESSION['resident_session'] = session_id();
			$_SESSION['rmc_num'] = $row_resident[0];
			$_SESSION['rmc_ref'] = $row_resident[6];
			$_SESSION['is_developer'] = $row_resident[9]; 
		
			// Get certain RMC info
			$sql_rmc = "SELECT * FROM cpm_rmcs r, cpm_rmcs_extra rex WHERE r.rmc_num=rex.rmc_num AND r.rmc_num = ".$row_resident[0];
			$result_rmc = @mysql_query($sql_rmc);
			$row_rmc = @mysql_fetch_array($result_rmc);
			$_SESSION['is_demo_account'] = "N";
			$_SESSION['rmc_name'] = $row_rmc['rmc_name'];
			$_SESSION['dev_description'] = $row_rmc['dev_description'];
			$_SESSION['member_id'] = $row_rmc['member_id'];		
			$_SESSION['resident_num'] = $row_resident[8];
			$_SESSION['resident_ref'] = $row_resident[7];
			$_SESSION['resident_name'] = $row_resident[3];
			$_SESSION['resident_num'] = $row_resident[8];
			$_SESSION['login'] = session_id();
			
			return true;
		}
		
		return false;
	}
	
	
	function add_property($request){
		
		if($request['lessee_id'] == "" || $request['password'] == ""){
			return "Please complete both fields.";
		}
		
		global $UTILS_DB_ENCODE;
		$security = new security;
		$crypt = new encryption_class;
		$data = new data;
		$save_result = array();
		
		// Check that property exists...
		$sql = "
		SELECT * 
		FROM cpm_lookup_residents lres, cpm_residents res, cpm_residents_extra rex, cpm_rmcs rmc, cpm_lookup_rmcs lrmc 
		WHERE 
		lres.resident_lookup = res.resident_num AND 
		rex.resident_num = res.resident_num AND 
		rmc.rmc_num = lrmc.rmc_lookup AND 
		res.rmc_num = rmc.rmc_num AND 
		lres.resident_ref = '".$security->clean_query($request['lessee_id'])."' AND 
		rex.password = '".$security->clean_query($crypt->encrypt($UTILS_DB_ENCODE, $request['password']))."' 
		";
		$result = @mysql_query($sql);
		$num_res = @mysql_num_rows($result);
		if($num_res > 0){
			
			$row = @mysql_fetch_array($result);
			
			// Check if resident or rmc is inactive
			if($row['resident_is_active'] != "1" || $row['rmc_is_active'] != "1"){
				$save_result[0] = false;
				$save_result[1] = "This account is no longer active.";
				return $save_result;
			}
			
			// Disallow adding of Director/Sub-tenant accounts to Master Account
			if($row['is_resident_director'] == "Y" || $row['is_subtenant_account'] == "Y"){
				$save_result[0] = false;
				$save_result[1] = "This account has special privileges and cannot be added to your Master Account.";
				return $save_result;
			}
			
			// Check if already associated with master account
			if($this->is_resident_assoc($row['resident_serial']) === true){
				$save_result[0] = false;
				$save_result[1] = "This account is already associated with a Master Account.";
				return $save_result;
			}
			
			// Add property
			$sql2 = "
			INSERT INTO cpm_master_account_resident_assoc (
				master_account_id,
				resident_num
			)VALUES(
				".$this->master_account_id.",
				".$row['resident_num']."
			)
			";
			@mysql_query($sql2);
			
			// Link to account they had logged with (hence disabling its login)
			$resident = new resident($row['resident_num']);
			if( $resident->link_to_master_account() === true ){
				
				$unit = new unit;
				$unit->set_unit($row['resident_num']);
				$save_result[0] = true;
				$save_result[1] = stripslashes($unit->get_unit_desc());
				
				
				// Send email to user about property being added to Master Account
				$star_username = $data->star_str($this->master_account_username, 2);
				include($UTILS_FILE_PATH."includes/ma_add_property_msg.php");
				@mail($this->master_account_email,"RMG Living - Property Added ...",$msg,"From:customerservice@rmguk.com");
				
				
				return $save_result;
			}
			else{
				$save_result[0] = false;
				$save_result[1] = "Technical fault - there was a problem adding the property to your Master Account.";
				return $save_result;
			}
		}
		else{
			$save_result[0] = false;
			$save_result[1] = "These login details could not be found.";
			return $save_result;
		}
	}
	
}


?>