<?php
require_once($UTILS_CLASS_PATH."security.class.php");
require_once($UTILS_CLASS_PATH."resident.class.php");
require_once($UTILS_CLASS_PATH."encryption.class.php");
require_once($UTILS_CLASS_PATH."developer.class.php");



class developer_user extends resident {
	
	
	function developer_user($ref="", $ref_type="resident_num"){
		
		$this->resident($ref);
	}
	
	
	function user_exists($username){
		
		if( $this->resident_id != "" ){
			$user_clause = " resident_lookup <> ".$this->resident_num." AND ";
		}
		
		$sql = "
		SELECT resident_ref 
		FROM cpm_lookup_residents 
		WHERE 
		".$user_clause." 
		resident_ref = '".$username."'";
		$result = @mysql_query($sql);
		$num = @mysql_num_rows($result);
		if($num > 0){
			return true;
		}
		return false;
	}

	
	function check_fields($request){
		
		if( $request['username'] == "" ){return "Please complete all required fields marked with (*).";}
		if( $request['password'] == "" ){return "Please complete all required fields marked with (*).";}
		if( $request['login_expiry_YMD'] == "" ){return "Please complete all required fields marked with (*).";}
		if( $this->user_exists($request['username']) === true ){ return "An account with this username already exists. Please choose another."; }
		
		if( strlen($request['username']) < 6 ){return "The username specified must be 6 or more charcters in length.";}
		if( strlen($request['password']) < 6 ){return "The password specified must be 6 or more charcters in length.";}
		
		return true;
	}
	
	
	function save($request){
		
		$security = new security;
		$crypt = new encryption_class;
		$developer = new developer($request['developer_id']);
		global $UTILS_DB_ENCODE;
		global $UTILS_FILE_PATH;
		global $UTILS_SITE_VISITS_PATH;
		
		$login_expiry_YMD = explode("/", $request['login_expiry_YMD']);
		
		// Insert record
		if( $this->resident_id == "" ){
			
			$sql = "
			INSERT INTO cpm_lookup_residents (
				subsidiary_id,
				resident_ref,
				date_created
			)VALUES(
				1,
				'".$request['username']."',
				'".date("H:i d/m/Y")."'
			)
			";
			if(@mysql_query($sql) !== false){
			
				$sql_id = "SELECT LAST_INSERT_ID() FROM cpm_lookup_residents";
				$result_id = @mysql_query($sql_id);
				$row_id = @mysql_fetch_row($result_id);
				$resident_num = $row_id[0];

				$sqlr = "
				INSERT INTO cpm_residents (
					rmc_num,
					resident_num,
					resident_name,
					resident_address_1,
					resident_address_2,
					resident_address_city,
					resident_address_county,
					resident_address_postcode,
					is_resident_director,
					resident_status,
					resident_is_active,
					resident_is_developer_dummy
				)
				VALUES(
					".$developer->developer_dummy_rmc_num.",
					".$resident_num.",
					'Mr & Mrs C. Kirk',
					'The Gables',
					'1 The Avenue',
					'Hitchin',
					'Hertfordshire',
					'SG4 6TH',
					'Y',
					'Current',
					'1',
					'Y'
				)
				";
				@mysql_query($sqlr);
				
				$sqlx = "
				INSERT INTO cpm_residents_extra (
					rmc_num,
					resident_num,
					password,
					is_first_login,
					is_demo_resident_account,
					login_expiry_YMD,
					is_developer,
					developer_id,
					notes
				)
				VALUES(
					".$developer->developer_dummy_rmc_num.",
					".$resident_num.",
					'".$crypt->encrypt($UTILS_DB_ENCODE, trim($request['password']))."',
					'N',
					'N',
					'".$login_expiry_YMD[2].$login_expiry_YMD[1].$login_expiry_YMD[0]."',
					'Y',
					".$developer->developer_id.",
					'".$security->clean_query($request['notes'])."'
				)
				";
				@mysql_query($sqlx);
				
				// Get last highest unit number
				$sql_u = "SELECT unit_num FROM cpm_units WHERE rmc_num = 9999999 DESC";
				$result_u = @mysql_query($sql_u);
				$row_u = @mysql_query($result_u);
				$new_unit_num = $row_u[0] + 1;
				
				$sqlul = "
				INSERT INTO cpm_lookup_units (
					subsidiary_id,
					rmc_num,
					unit_ref,
					date_created
				)VALUES(
					1,
					".$developer->developer_dummy_rmc_num.",
					'dev-".$new_unit_num."',
					'".date("H:i d/m/Y")."'
				)
				";
				@mysql_query($sqlul);
				
				$sql_insid = "SELECT LAST_INSERT_ID() FROM cpm_lookup_units";
				$result_insid = @mysql_query($sql_insid);
				$row_insid = @mysql_fetch_row($result_insid);
				$unit_lookup_id = $row_insid[0];
				
				$sqlu = "
				INSERT INTO cpm_units (
					unit_num,
					resident_num,
					rmc_num,
					unit_address_1,
					unit_is_developer_dummy
				)
				VALUES(
					".$unit_lookup_id.",
					".$resident_num.",
					".$developer->developer_dummy_rmc_num.",
					'1 Cherry Tree Court',
					'Y'
				)
				";
				@mysql_query($sqlu);
				
			}
			
			return true;
		}
		
		// Update record
		else{
			
			$sql = "
			UPDATE cpm_lookup_residents SET
			resident_ref = '".$request['username']."'
			WHERE resident_lookup = ".$this->resident_num." 
			";
			@mysql_query($sql);
			
			$sql = "
			UPDATE cpm_residents_extra SET
			password = '".$crypt->encrypt($UTILS_DB_ENCODE, trim($request['password']))."',
			login_expiry_YMD = '".$login_expiry_YMD[2].$login_expiry_YMD[1].$login_expiry_YMD[0]."',
			notes = '".$security->clean_query($request['notes'])."'
			WHERE resident_num = ".$this->resident_num;
			@mysql_query($sql);
			
			return true;
		}
	
		return "Technical fault - this user could not be saved.";
	}
	
	
	
	function delete(){
	
		$sql = "DELETE FROM cpm_lookup_residents WHERE resident_lookup = ".$this->resident_num;
		@mysql_query($sql);
		
		$sql = "DELETE FROM cpm_residents WHERE resident_num = ".$this->resident_num;
		@mysql_query($sql);
		
		$sql = "DELETE FROM cpm_residents_extra WHERE resident_num = ".$this->resident_num;
		@mysql_query($sql);
		
		$sql = "DELETE FROM cpm_units WHERE resident_num = ".$this->resident_num;
		@mysql_query($sql);
		
		$sql = "DELETE FROM cpm_stats WHERE resident_num = ".$this->resident_num;
		@mysql_query($sql);		
	
		return true;
	}
	
	
	
	
}


?>