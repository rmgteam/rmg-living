<?php
require_once($UTILS_CLASS_PATH."security.class.php");
require_once($UTILS_CLASS_PATH."data.class.php");
require_once($UTILS_CLASS_PATH."field.class.php");
require_once($UTILS_CLASS_PATH."xmltoarray.class.php");

class webservice{
	
	var $hostname = "hodd1srctxprs13";
	var $hostaddress = "http://94.250.233.138/qubews/qubews.asmx";
	var $process_headers = array();
	var $xml = '';
	var $ssl = false;
	
	function webservice(){
	}
	
	function post($version='CURL_HTTP_VERSION_1_0'){
		$ch = curl_init();
		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		
		curl_setopt($ch, CURLOPT_URL, $this->hostaddress);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $this->process_headers);
		curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
		curl_setopt($ch, CURLOPT_HTTP_VERSION, $version);
		curl_setopt($ch, CURLOPT_HEADER, 0); // tells curl to include headers in response
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  // return into a variable
		curl_setopt($ch, CURLOPT_TIMEOUT, 30); // times out after 30 secs
		curl_setopt($ch, CURLOPT_POSTFIELDS, $this->xml); // adding POST data
		curl_setopt($ch, CURLOPT_POST, 1);  // data sent as POST
		curl_setopt($ch, CURLOPT_FAILONERROR, true);
		
		if($this->ssl == true){
			/*curl_setopt($ch, CURLOPT_PORT , 443);
			 curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);*/
		}
		
		$result_array['output'] = curl_exec($ch);
		$result_array['info'] = curl_getinfo($ch);

		if($result_array['output'] === false || $result_array['info']['http_code'] != 200 ) {
			var_dump(curl_error($ch));
		}
		
		curl_close($ch);
		
		return $result_array;
	}
	
	function execute(){
		
		$result_array = $this->post();
		
		$output = $result_array['output'];
		$info = $result_array['info'];
		
		$xmlObj = new xml_to_array($output);
		$xml_result = $xmlObj->createArray();
		
		//print_r($this->xml);
		
		if($output === false || $info['http_code'] >= 400){
			$error_array = array();
			$error_array['code'] = $info['http_code'];
			$error_array['desc'] = $output;
			return $error_array;
		}else{
			return $xml_result;
		}
	}
	
	function qube_execute($application, $process, $xml, $group="RMG Test", $session_id=''){
		$security = new security();
		
		switch($application){
			Case 1:
				$application_name = "QGS Purchase Ledger";
				break;
			Case 2:
				$application_name = "QGS Property Management";
				break;
			Case 3:
				$application_name = "QGS Help Desk &amp; Planned Maintenance";
				break;
			Case 4:
				$application_name = "QGS General Ledger";
				break;
		}
		
		if($session_id == ''){
			$session_id = $security->gen_serial(16);
		}
		
		$host = "Host: ".$this->hostname;
		$content_type = "Content-Type: text/xml; charset=utf-8";
		
		if($process == 'logout'){
			
			$this->xml = '<?xml version="1.0" encoding="utf-8"?>
			<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
			<soap:Body>
			<Logout xmlns="http://qube.qubeglobal.com/ns/webservice/">
			<ClientSessionKey>'.$session_id.'</ClientSessionKey>
		    </Logout>
			</soap:Body>
			</soap:Envelope>';
			
		}else{
			
			$this->xml = '<?xml version="1.0" encoding="utf-8"?>
			<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
			<soap:Body>
			<QubeProcess-3 xmlns="http://qube.qubeglobal.com/ns/webservice/">
			<ClientSessionKey>'.$session_id.'</ClientSessionKey>
			<QubeProcessName>rmgweb:'.$process.'</QubeProcessName>
			<Data>'.$xml.'</Data>
			<UserName>rmgweb.servacc</UserName>
			<Password>p4ssw0rd</Password>
			<Group>'.$group.'</Group>
			<Application>'.$application_name.'</Application>
			</QubeProcess-3>
			</soap:Body>
			</soap:Envelope>';
			
		}
		
		$this->process_headers = array();
		$this->process_headers[] = $host;
		$this->process_headers[] = $content_type;
		$this->process_headers[] = "Content-Length : " . strlen ($this->xml);
		$this->process_headers[] = "SOAPAction: \"http://qube.qubeglobal.com/ns/webservice/QubeProcess-3\"";
		
		$output = $this->execute();

		return $output;
	}
	
	function logout($session_id){
		
		$output = $this->qube_execute(1, 'logout', '', '', $session_id);
		
		if(!is_null($output['code'])){	//curl unavailable	
			$output_html = "XML is invalid/Curl is unavailable. Http response: " . $output['code'] . " XML = " . $output['desc'];
		}else{
			$output_html = true;
		}
			
	}	
}

?>