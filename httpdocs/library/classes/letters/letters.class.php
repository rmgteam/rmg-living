<?php
require_once($UTILS_CLASS_PATH."pdf/pdf.class.php");
require_once($UTILS_CLASS_PATH."pdf/tfpdf/class.tfpdftable.php");
class letters extends pdf  {

	function letters($orientation='P',$unit='mm',$size='A4'){
		$this->tFPDF($orientation,$unit,$size);
		$this->pdf();
		$this->SetMargins(20,43,20);
	}
	
	function add_letter_address($address_array){
		
		for($a=0;$a < count($address_array);$a++){
			if($address_array[$a] != ''){
				$this->Write($this->line_height, $address_array[$a]);
				$this->Ln();
			}
		}
	}
	
	function add_letter_date($date="", $format="d/m/Y"){
		
		if($date == ""){
			$date = date($format);
		}
		$this->Cell(0,$this->line_height,$date,0,1,'R');
	}
	
	function add_salutation($name="", $sal="Dear"){
		
		$sal = $sal." ".$name.",";
		$this->Cell(0,$this->line_height,$sal,0,1,'L');
		$this->Ln();
	}
	
	function add_ref($str=""){
		
		$str = "Our Ref: ".$str;
		$this->Cell(0, $this->line_height, $str, 0, 1, 'L');
		$this->Ln();
		$this->Ln();
	}
	
	function add_regarding($str=""){
		
		$str = "Re: ".$str;
		$this->SetFont('','BU');
		//$this->Cell(0, $this->line_height, $str, 0, 1, 'L');
		$this->MultiCell(0, $this->line_height, $str, 0, 'L', 0);
		$this->SetFont('','');
		$this->Ln();
	}
	
	function add_closing($object="", $str="Yours sincerely,", $type="rmc"){
		
		$this->Cell(0,$this->line_height,$str,0,1,'L');
		
		if(is_object($object)){
			
			if($type=="rmc"){
				$this->Cell(0,$this->line_height, "For and on behalf of ".stripslashes($object->rmc_name),0,1,'L'); 
			}
			elseif($type=="owner"){
				$this->Cell(0,$this->line_height, "For and on behalf of ".stripslashes($object->owner_name),0,1,'L');	
			}
			$this->Ln();
		}
		elseif(is_string($object) && $object != ""){
			$this->Cell(0,$this->line_height, "For and on behalf of ".stripslashes($object),0,1,'L');
			$this->Ln(); 
		}
		else{
			$this->Ln();
		}
	}
	
	function add_pp($str=""){
		
		$str = "pp ".$str;
		$this->Cell(0,$this->line_height,$str,0,1,'L');
	}
	
	function add_signature($user_id="", $jobtitle=false){
		
		global $UTILS_SERVER_PATH;
		
		$personnel = new personnel($user_id);
		$sig = $UTILS_SERVER_PATH."images/signatures/".$personnel->personnel_signature;
		if(file_exists($sig) && $personnel->personnel_signature != ''){
			$this->Image($sig, null, null, -300);
		}
		$this->Ln();
		$this->Cell(0, $this->line_height, stripslashes($personnel->get_personnel_fullname()), 0, 1, 'L');
		if($jobtitle == true){
			$this->Cell(0, $this->line_height, stripslashes($personnel->personnel_job_string), 0, 1, 'L');
		}
	}
}



?>