<?
require_once($UTILS_CLASS_PATH."data.class.php");
require_once($UTILS_CLASS_PATH."security.class.php");


class order {
	
	var $order_id;
	var $order_serial;
	var $order_type;
	var $order_ref;
	var $brand_code;
	var $resident_num;
	var $order_card_type_paid_with;
	var $order_transactionstatus;
	var $order_trans_code;
	var $order_bank_auth_code;
	var $order_cv2avs;
	var $order_response_hash;
	var $order_ground_rent;
	var $order_service_charge;
	var $order_reserve_charge_amount;
	var $order_insurance_amount;
	var $order_rent_amount;
	var $order_garage_amount;
	var $order_major_works_amount;
	var $order_eoy_amount;
	var $order_other_amount;
	var $order_s1_response;
	var $browser_version;
	var $order_vat_perc;
	var $order_net;
	var $order_vat;
	var $order_subtotal;
	var $order_charge;
	var $order_total;
	var $order_chargetype;
	var $order_datetime;
	var $order_YMD;
	var $order_TS;
	var $order_ins_sched_id;
	var $order_ecistatus;
	var $order_cardprefix;
	var $order_test_status;
	var $order_voyager_entered;
	var $order_voyager_entered_user_id;
	var $order_voyager_entered_ts;

	
	function order($ref="", $ref_type=""){
		
		$security = new security;
		
		if($ref_type == "serial"){
			$ref_clause = " order_serial = '".$security->clean_query($ref)."' ";
		}
		else{
			$ref_clause = " order_id = ".$security->clean_query($ref)." ";
		}
		
		$sql = "
		SELECT * 
		FROM cpm_orders 
		WHERE 
		$ref_clause 
		";
		$result = @mysql_query($sql);
		$row = @mysql_fetch_array($result);
		
		$this->order_id = $row['order_id'];
		$this->order_serial = $row['order_serial'];
		$this->order_type = $row['order_type'];
		$this->order_ref = $row['order_ref'];
		$this->brand_code = $row['brand_code'];
		$this->resident_num = $row['resident_num'];
		$this->order_card_type_paid_with = $row['order_card_type_paid_with'];
		$this->order_transactionstatus = $row['order_transactionstatus'];
		$this->order_trans_code = $row['order_trans_code'];
		$this->order_bank_auth_code = $row['order_bank_auth_code'];
		$this->order_cv2avs = $row['order_cv2avs'];
		$this->order_response_hash = $row['order_response_hash'];
		$this->order_ground_rent = $row['order_ground_rent'];
		$this->order_service_charge = $row['order_service_charge'];
		$this->order_reserve_charge_amount = $row['order_reserve_charge_amount'];
		$this->order_insurance_amount = $row['order_insurance_amount'];
		$this->order_rent_amount = $row['order_rent_amount'];
		$this->order_garage_amount = $row['order_garage_amount'];
		$this->order_major_works_amount = $row['order_major_works_amount'];
		$this->order_eoy_amount = $row['order_eoy_amount'];
		$this->order_other_amount = $row['order_other_amount'];
		$this->order_s1_response = $row['order_s1_response'];
		$this->browser_version = $row['browser_version'];
		$this->order_net = $row['order_net'];
		$this->order_vat_perc = $row['order_vat_perc'];
		$this->order_vat = $row['order_vat'];
		$this->order_subtotal = $row['order_subtotal'];
		$this->order_charge = $row['order_charge'];
		$this->order_total = $row['order_total'];
		$this->order_chargetype = $row['order_chargetype'];
		$this->order_datetime = $row['order_datetime'];
		$this->order_YMD = $row['order_YMD'];
		$this->order_TS = $row['order_TS'];
		$this->order_ins_sched_id = $row['order_ins_sched_id'];
		$this->order_ecistatus = $row['order_ecistatus'];
		$this->order_cardprefix = $row['order_cardprefix'];
		$this->order_test_status = $row['order_test_status'];
		$this->order_voyager_entered = $row['order_voyager_entered'];
		$this->order_voyager_entered_user_id = $row['order_voyager_entered_user_id'];
		$this->order_voyager_entered_ts = $row['order_voyager_entered_ts'];
	}
	
	
	function gen_unique_serial(){
		
		$security = new security;
		$serial_unique = "N";
		while($serial_unique == "N"){
			
			$serial = $security->gen_serial(32);
			
			$sql = "
			SELECT count(*) 
			FROM cpm_orders 
			WHERE 
			order_serial = '".$serial."'
			";
			$result = @mysql_query($sql);
			$row = @mysql_fetch_row($result);
			if($row[0] == 0){
				$serial_unique = "Y";
				return $serial;
				break;
			}
		}
	}
	
	
	function save($request){
		
		$security = new security;
		$thistime = time();
		
		if( $this->order_id == "" ){
			
			$order_ins_sched_id = "NULL";
			if( $request['order_ins_sched_id'] != "" ){
				$order_ins_sched_id = $request['order_ins_sched_id'];
			}
			
			$sql = "
			INSERT INTO cpm_orders (
				order_serial,
				order_type,
				brand_code,
				order_ref,
				order_net,
				order_subtotal,
				order_charge,
				order_total,
				browser_version,
				resident_num,
				order_datetime,
				order_YMD,
				order_TS,
				order_ground_rent,
				order_service_charge,
				order_card_type_pre_selected
			)
			VALUES(
				'".$this->gen_unique_serial()."',
				4,
				'RMG',
				'".$security->clean_query($request['oid'])."',
				'".$security->clean_query(str_replace(",","",$request['net']))."',
				'".$security->clean_query(str_replace(",","",$request['subtotal']))."',
				'".$security->clean_query(str_replace(",","",$request['charge']))."',
				'".$security->clean_query(str_replace(",","",$request['total']))."',
				'".$security->clean_query($_SERVER['HTTP_USER_AGENT'])."',
				'".$security->clean_query($request['resident_num'])."',
				'".date("M d Y H:i:s", $thistime)."',
				".date("Ymd", $thistime).",
				".$thistime.",
				'".$security->clean_query(str_replace(",","",$request['ground_rent']))."',
				'".$security->clean_query(str_replace(",","",$request['service_charge']))."',
				'".$security->clean_query($request['order_card_type_pre_selected'])."'
			)
			";
			if( @mysql_query($sql) !== false ){
			
				$sql_id = "SELECT LAST_INSERT_ID() FROM cpm_orders";
				$result_id = @mysql_query($sql_id);
				$row_id = @mysql_fetch_row($result_id);
			
				$this->order($row_id[0]);
				return true;
			}
		}
		else{
			
			// Save details of completed transaction...
			$sql = "
			UPDATE cpm_orders SET
				order_transactionstatus = '".$request['order_transactionstatus']."',
				order_bank_auth_code = '".$request['order_bank_auth_code']."',
				order_cv2avs = '".$request['order_cv2avs']."',
				order_trans_code = '".$request['order_trans_code']."',
				order_test_status = '".$request['order_test_status']."',
				order_response_hash = '".$request['order_response_hash']."',
				order_card_type_paid_with = '".$request['order_card_type_paid_with']."' 
			WHERE order_id = ".$this->order_id."
			";
			if( @mysql_query($sql) !== false ){
				return true;	
			}
			else{
				//print mysql_error();	
			}
		}
		
		return "Technical fault - this order could not be saved.";
	}
	
	
	function get_order_card_type_list(){
		
		$sql = "
		SELECT * 
		FROM cpm_order_card_types 
		WHERE 
		order_card_type_discon = 'N' 
		";
		$result = @mysql_query($sql);
		return $result;
	}
	
	
	function get_card_type_charge_words($card_type_id){
		
		$charge_words = "";
		$charge_words_array = array();
		
		$sql = "
		SELECT * 
		FROM cpm_order_card_types 
		WHERE 
		order_card_type_id = ".$card_type_id."
		";
		$result = @mysql_query($sql);
		$row = @mysql_fetch_array($result);
		
		if( $row['order_card_type_bcard_charge_perc'] == "0" && $row['order_card_type_bcard_charge_perc'] == "0" ){
			return "FREE";	
		}
		
		if( $row['order_card_type_bcard_charge_perc'] != "" && $row['order_card_type_bcard_charge_perc'] > 0 ){
			
			$charge_words_array[] .= $row['order_card_type_bcard_charge_perc']."%";
		}
		
		if( $row['order_card_type_bcard_charge_pence'] != "" && $row['order_card_type_bcard_charge_pence'] > 0 ){
			
			$charge_words_array[] .= "+ ".number_format($row['order_card_type_bcard_charge_pence'],0,"","")."p";
		}
		
		if( count($charge_words_array) > 0 ){
			$charge_words = implode(" ", $charge_words_array);
		}
		
		return $charge_words;
	}
	
	
	function calc_payment_charge($card_type_id, $val){
	
		$val = str_replace(",","",$val);
		$result = $this->get_order_card_type_list();
		while( $row = @mysql_fetch_array($result) ){
			
			if($card_type_id == $row['order_card_type_id']){

				$charge = 0;

				if( $row['order_card_type_bcard_charge_perc'] > 0 ){			
				
					$charge = ($val * ($row['order_card_type_bcard_charge_perc']/100));
				}
				
				if( $row['order_card_type_bcard_charge_pence'] > 0 ){
					
					$charge += ($row['order_card_type_bcard_charge_pence'] / 100);
				}
				
				return $charge;
			}
		}
	}
	
	
	function get_vat_perc(){
		
		if($ymd == ""){
			$ymd = date("Ymd");
		}
		
		$sql = "
		SELECT * 
		FROM cpm_vat 
		WHERE 
		'".$ymd."' >= vat_period_start AND 
		('".$ymd."' <= vat_period_end OR vat_period_end IS NULL OR vat_period_end = '')
		";
		$result = @mysql_query($sql);
		$row = @mysql_fetch_array($result);
		
		return $row['vat_amount'];
	}
	
	
	function gen_order_num($length){
		
		$rand_value[] = "A";
		$rand_value[] = "B";
		$rand_value[] = "C";
		$rand_value[] = "D";
		$rand_value[] = "E";
		$rand_value[] = "F";
		$rand_value[] = "G";
		$rand_value[] = "H";
		$rand_value[] = "J";
		$rand_value[] = "K";
		$rand_value[] = "M";
		$rand_value[] = "N";
		$rand_value[] = "P";
		$rand_value[] = "Q";
		$rand_value[] = "R";
		$rand_value[] = "T";
		$rand_value[] = "U";
		$rand_value[] = "V";
		$rand_value[] = "W";
		$rand_value[] = "X";
		$rand_value[] = "Y";
		$rand_value[] = "2";
		$rand_value[] = "3";
		$rand_value[] = "4";
		$rand_value[] = "6";
		$rand_value[] = "7";
		$rand_value[] = "8";
		$rand_value[] = "9";

		$num_unique = "N";
		while($num_unique == "N"){

			for($i=1; $i<=$length; $i++){
				
				mt_srand((double)microtime() * 1000000);
				$num = mt_rand(1, count($rand_value) );
				$rand_id .= $rand_value[$num-1];
			}
			
			$sql = "
			SELECT count(*) 
			FROM cpm_orders 
			WHERE 
			order_ref = '".$rand_id."' 
			";
			$result = @mysql_query($sql);
			$row = @mysql_fetch_row($result);
			if($row[0] == 0){
				$num_unique = "Y";
				return $rand_id;
				break;
			}
		}
	}
	
	
	
}


?>