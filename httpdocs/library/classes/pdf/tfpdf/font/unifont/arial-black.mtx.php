<?php

Global $UTILS_SERVER_PATH;

$name='Arial-Black';
$type='TTF';
$desc=array (
  'Ascent' => 1101,
  'Descent' => -310,
  'CapHeight' => 1101,
  'Flags' => 262148,
  'FontBBox' => '[-194 -307 1688 1083]',
  'ItalicAngle' => 0,
  'StemV' => 241,
  'MissingWidth' => 750,
);
$up=-125;
$ut=60;
$ttffile = $UTILS_SERVER_PATH.'library/classes/pdf/tfpdf/font/unifont/Arial-Black.ttf';
$originalsize=119876;
$fontkey='arialBL';
?>