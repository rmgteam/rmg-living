<?php

Global $UTILS_SERVER_PATH;


$name='Arial-ItalicMT';
$type='TTF';
$desc=array (
  'Ascent' => 905,
  'Descent' => -212,
  'CapHeight' => 716,
  'Flags' => 68,
  'FontBBox' => '[-517 -325 1359 998]',
  'ItalicAngle' => -12,
  'StemV' => 87,
  'MissingWidth' => 750,
);
$up=-106;
$ut=73;
$ttffile = $UTILS_SERVER_PATH.'library/classes/pdf/tfpdf/font/unifont/Arial-Italic.ttf';
$originalsize=555588;
$fontkey='arialI';
?>