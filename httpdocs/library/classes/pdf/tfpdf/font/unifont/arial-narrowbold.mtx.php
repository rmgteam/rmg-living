<?php

Global $UTILS_SERVER_PATH;


$name='ArialNarrow-Bold';
$type='TTF';
$desc=array (
  'Ascent' => 936,
  'Descent' => -212,
  'CapHeight' => 936,
  'Flags' => 262148,
  'FontBBox' => '[-137 -307 1000 1109]',
  'ItalicAngle' => 0,
  'StemV' => 165,
  'MissingWidth' => 228,
);
$up=-106;
$ut=73;
$ttffile = $UTILS_SERVER_PATH.'library/classes/pdf/tfpdf/font/unifont/Arial-NarrowBold.ttf';
$originalsize=180740;
$fontkey='arialNB';
?>