<?php

Global $UTILS_SERVER_PATH;


$name='ArialNarrow';
$type='TTF';
$desc=array (
  'Ascent' => 936,
  'Descent' => -212,
  'CapHeight' => 936,
  'Flags' => 4,
  'FontBBox' => '[-182 -307 1000 1086]',
  'ItalicAngle' => 0,
  'StemV' => 87,
  'MissingWidth' => 228,
);
$up=-106;
$ut=73;
$ttffile = $UTILS_SERVER_PATH.'library/classes/pdf/tfpdf/font/unifont/Arial-Narrow.ttf';
$originalsize=175956;
$fontkey='arialN';
?>