<?php

Global $UTILS_SERVER_PATH;


$name='Arial-BoldMT';
$type='TTF';
$desc=array (
  'Ascent' => 905,
  'Descent' => -212,
  'CapHeight' => 716,
  'Flags' => 262148,
  'FontBBox' => '[-628 -376 2000 1018]',
  'ItalicAngle' => 0,
  'StemV' => 165,
  'MissingWidth' => 750,
);
$up=-106;
$ut=105;
$ttffile = $UTILS_SERVER_PATH.'library/classes/pdf/tfpdf/font/unifont/Arial-Bold.ttf';
$originalsize=748720;
$fontkey='arialB';
?>