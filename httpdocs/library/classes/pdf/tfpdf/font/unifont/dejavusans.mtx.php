<?php

Global $UTILS_SERVER_PATH;

$name='DejaVuSans';
$type='TTF';
$desc=array (
  'Ascent' => 928,
  'Descent' => -236,
  'CapHeight' => 928,
  'Flags' => 4,
  'FontBBox' => '[-1021 -415 1681 1167]',
  'ItalicAngle' => 0,
  'StemV' => 87,
  'MissingWidth' => 600,
);
$up=-63;
$ut=44;
$ttffile = $UTILS_SERVER_PATH.'library/classes/pdf/tfpdf/font/unifont/DejaVuSans.ttf';
$originalsize=720012;
$fontkey='dejavusans';
?>