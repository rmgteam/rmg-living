<?php
/**
* FPDF Table - Default definition file
* 
* Version:       4.0.0
* Date:          2012/01/30
* Author:        Andrei Bintintan <andy@interpid.eu>
*/

$aTableConfiguration = array(
    'HEADER' => array(
            'WIDTH'         => 6,                    //cell width
            'TEXT_COLOR'       => array(220,230,240),    //text color
            'TEXT_SIZE'        => 8,                    //font size
            'TEXT_FONT'        => 'Arial',                //font family
            'TEXT_ALIGN'       => 'C',                    //horizontal alignment, possible values: LRC (left, right, center)
            'VERTICAL_ALIGN'       => 'M',                    //vertical alignment, possible values: TMB(top, middle, bottom)
            'T_TYPE'        => 'B',                    //font type
            'LINE_SIZE'       => 4,                    //line size for one row
            'BACKGROUND_COLOR'      => array(41, 80, 132),    //background color
            'BORDER_COLOR'     => array(0,92,177),        //border color
            'BORDER_SIZE'      => 0.2,                    //border size
            'BORDER_TYPE'      => '1',                    //border type, can be: 0, 1 or a combination of: "LRTB"
            'TEXT'          => '',                    //text
            //padding
            'PADDING_TOP'       => 0,
            'PADDING_RIGHT'     => 0,
            'PADDING_LEFT'      => 0,
            'PADDING_BOTTOM'    => 0,

    ),

    'ROW' => array(
            'TEXT_COLOR'       => array(0,0,0),        //text color
            'TEXT_SIZE'        => 6,                    //font size
            'TEXT_FONT'        => 'Arial',                //font family
            'TEXT_ALIGN'       => 'C',                    //horizontal alignment, possible values: LRC (left, right, center)
            'VERTICAL_ALIGN'       => 'M',                    //vertical alignment, possible values: TMB(top, middle, bottom)
            'T_TYPE'        => '',                    //font type
            'LINE_SIZE'       => 4,                    //line size for one row
            'BACKGROUND_COLOR'      => array(255,255,255),    //background color
            'BORDER_COLOR'     => array(0,92,177),        //border color
            'BORDER_SIZE'      => 0.1,                    //border size
            'BORDER_TYPE'      => '1',                    //border type, can be: 0, 1 or a combination of: "LRTB"
            //padding
            'PADDING_TOP'       => 0,
            'PADDING_RIGHT'     => 0,
            'PADDING_LEFT'      => 0,
            'PADDING_BOTTOM'    => 0,
    ),
    
    'TABLE' => array(
            'TABLE_ALIGN'      => 'L',                    //table align on page
            'TABLE_LEFT_MARGIN'      => 25,                    //space to the left margin
            'BORDER_COLOR'     => array(0,92,177),        //border color
            'BORDER_SIZE'      => '0.3',                //border size
    )
);
/*****************************************************
TABLE DEFAULT DEFINES --- END
*****************************************************/
?>