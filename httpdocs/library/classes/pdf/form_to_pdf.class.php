<?php

require_once($UTILS_CLASS_PATH."dom/dom_element_extend.class.php");
require_once($UTILS_CLASS_PATH."pdf/html2fpdf/html2fpdf.php");

class form_to_pdf {
	
	function form_to_pdf($request=null){
		Global $UTILS_SERVER_PATH;
		Global $UTILS_WEBROOT;
		
		if(!is_null($request)){
			$strContent = $request['page_print'];
			
			$remove_array = array();
			
			$dom = new DOMDocument();
			$dom->registerNodeClass('DOMElement', 'extraElement');
			$dom->loadHTML($strContent);
			$count = 0;
			$nodes = $dom->getElementsByTagName("*");
			for($i=0;$i<$nodes->length;$i++) {
				
				$item = $nodes->item($i);
				$style = $item->getAttribute('style');
				
				if((strpos($style, "display: none") !== false) || (strpos($style, "display:none") !== false)){
					array_push($remove_array, $item);
				}elseif($item->tagName == "a"){
					array_push($remove_array, $item);
				}elseif(strpos($item->tagName, "input") !== false){
					$type = $item->getAttribute('type');
					$class = $item->getAttribute('class');
					if(strpos($type, "hidden") !== false){
						array_push($remove_array, $item);
					}elseif(strpos($class, "button") !== false){
						array_push($remove_array, $item);
					}
				}elseif($item->tagName == 'table'){
					$children = $item->getElementsByTagName("table");
					$rows = array();
					for($j=0;$j<$children->length;$j++) {
						$child = $children->item($j);
						
						$loop_node = $child;
						$k = 1;
						while ($k != 0){
						    $loop_node = $loop_node->parentNode;
						    if($loop_node->tagName == 'tr'){
						    	$k = 0;
						    }
						}
						
						$replace_array[$count][0] = $child;
						$replace_array[$count][1] = $item;
						$replace_array[$count][2] = $loop_node;
						$count++;
					}
				}
				
				$id = $item->getAttribute('id');
				if(isset($request[$id])){
					if($item->tagName == 'input'){
						$type = $item->getAttribute('type');
						if($type == 'radio'){
							if(!$item->hasAttribute('checked')){
								//array_push($remove_array, $item);
							}
						}else{
							$item->setAttribute('value', $request[$id]);
						}
					}elseif($item->tagName == 'select'){
						$children = $item->getElementsByTagName("option");
						for($j=0;$j<$children->length;$j++) {
							$child = $children->item($j);					
							$value = $child->getAttribute('value');
							
							if($value == $request[$id]){
								$child->setAttribute('selected', 'selected');
							}else{
								array_push($remove_array, $child);	
							}
						}
					}elseif($item->tagName == 'textarea'){
						$item->parentNode->innerHTML = str_replace("\n\r", '<br />', $request[$id]);
					}
				}
			}
		
			foreach($remove_array as $value){
				
				$old_child = $value->parentNode->removeChild($value);
			}
		
			for($i=0;$i<count($replace_array);$i++) {
				$table = $replace_array[$i][0];
				$holding = $replace_array[$i][1];
				$tr = $replace_array[$i][2];
				$old_child = $table->parentNode->removeChild($table);
				$holding->appendChild($tr);
				$node = $dom->importNode($table, true);
				$dom->documentElement->appendChild($node);
			}
			
			$strContent = $dom->saveHTML();
			$strContent = str_replace('<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">', '', $strContent);
			$strContent = str_replace('<html>', '', $strContent);
			$strContent = str_replace('</html>', '', $strContent);
			$strContent = str_replace('<body>', '', $strContent);
			$strContent = str_replace('</body>', '', $strContent);
			$strContent = str_replace('<tbody>', '', $strContent);
			$strContent = str_replace('</tbody>', '', $strContent);
			$strContent = str_replace('<thead>', '', $strContent);
			$strContent = str_replace('</thead>', '', $strContent);
			$strContent = str_replace('&Acirc;', '', $strContent);
			//print_r($strContent);
			
			$pdf = new HTML2FPDF();
			
			$pdf->AddPage();
			$pdf->WriteHTML($strContent);
			$pdf->Output($UTILS_SERVER_PATH."temp_upload/".$request['file_name'].".pdf", 'F');
		}else{
			$strContent = '';
		}
		
		$result_array['file'] = $UTILS_WEBROOT."temp_upload/".$request['file_name'].".pdf";
		$result_array['page_print'] = $strContent;

		return $result_array;
	}
}
?>