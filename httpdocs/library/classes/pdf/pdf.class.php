<?php

require_once($UTILS_CLASS_PATH."pdf/fpdi/tfpdi.php");
require_once($UTILS_CLASS_PATH."dom/dom_element_extend.class.php");
//require_once($UTILS_SERVER_PATH."library/controllers/graph.controller.php");

class pdf extends tFPDI {

    var $font_name = 'Arial';
    var $font_style = '';
    var $font_size = 10;
    var $font_color = array(0,0,0);

    var $tag_style = array();
    var $logo = "images/logos/rmg_big.png";
    var $include_watermark = false;
    var $include_header = false;
    var $header_template = '';
    var $watermark_text = '';
    var $watermark_color = array(0,0,0);
    var $watermark_angle = 0;
    var $watermark_x = 0;
    var $watermark_y = 0;
    var $watermark_size = 120;
    var $watermark_family = 'Arial';
    var $watermark_style = 'B';
    var $auto_page_break = true;
    var $header_email = 'contractors@rmguk.com';
    var $header_phone = '';
    var $header_title = '';
    var $line_height = 5;

    var $fr_data = array();
    var $fr_border = array();
    var $fr_align = array();
    var $fr_style = array();
    var $fr_maxline = array();
    var $fr_padding_size = array();
    var $fr_padding = array();
    var $fr_fill = array();
    var $fr_fill_color = array();
    var $fr_gradient_color = array();
    var $fr_font_color = array();
    var $fr_line_height = '';

    var $_toc = array();
    var $_numbering = false;
    var $_numberingFooter = false;
    var $_numPageNum = 1;

    var $section_no = 0;
    var $figure_table_no = 0;
    var $figure_no = 0;
    var $level = 0;

    function pdf(){
        GLOBAL $UTILS_SERVER_PATH, $UTILS_TEL_CONTRACTOR_TENDERING;
        //$this->header_phone = $UTILS_TEL_CONTRACTOR_TENDERING;


        $this->fontpath = $UTILS_SERVER_PATH.'library/classes/pdf/tfpdf/font/';
        define("_SYSTEM_TTFONTS", $UTILS_SERVER_PATH.'library/classes/pdf/tfpdf/font/unifont/');
        define("FPDF_FONTPATH", $UTILS_SERVER_PATH.'library/classes/pdf/tfpdf/font');
        $this->AddFont('Arial','','Arial.ttf',true);
        $this->AddFont('Arial','B','Arial-Bold.ttf',true);
        $this->AddFont('Arial','I','Arial-Italic.ttf',true);
        $this->AddFont('Arial','N','Arial-Narrow.ttf',true);
        $this->AddFont('Arial','BI','Arial-BoldItalic.ttf',true);
        $this->AddFont('Arial','NBI','Arial-NarrowBoldItalic.ttf',true);
        $this->AddFont('Arial','NB','Arial-NarrowBold.ttf',true);
        $this->AddFont('Arial','NI','Arial-NarrowItalic.ttf',true);
        $this->AddFont('Arial','BL','Arial-Black.ttf',true);

        $this->colours['light_grey'] = array(204,204,204);
        $this->colours['light_blue_header'] = array(0,157,221);
        $this->colours['light_blue'] = array(222,233,238);
        $this->colours['white'] = array(255,255,255);

        $this->colours['rmg_blue1'] = array(46,44,112);
        $this->colours['rmg_blue2'] = array(0,145,212);
        $this->colours['rmg_blue3'] = array(0,173,233);

        $this->PDF_MemImage();
    }

    function reset_page_style(){

        $this->SetFont($this->font_name,$this->font_style,$this->font_size);
        $this->SetTopMargin($this->tMargin);
        $this->SetLeftMargin($this->lMargin);
        $this->SetRightMargin($this->rMargin);
        $this->SetAutoPageBreak($this->auto_page_break, $this->bMargin);
    }

    function Header(){
        if($this->include_header == true){
            $this->reset_page_style();

            $this->setBackgroundPDF($this->header_template);

        }
        if($this->include_watermark == true){
            $this->SetFont($this->watermark_family,$this->watermark_style,$this->watermark_size);
            $this->SetTextColor($this->watermark_color[0], $this->watermark_color[1], $this->watermark_color[2]);
            $this->RotatedText($this->watermark_x, $this->watermark_y, $this->watermark_text, $this->watermark_angle);
            $this->SetFont($this->font_name,'',$this->font_size);
            $this->SetTextColor($this->font_color[0], $this->font_color[1], $this->font_color[2]);
        }
        $this->reset_page_style();
    }

    function Footer(){

        if( $this->bMargin != 0 ){
            $this->SetY($this->bMargin);
        }else{
            $this->SetY(-15);
        }

        if($this->_numbering == true){
            // Go to 1.5 cm from bottom
            $this->SetY(-15);

            $current_font_colour = $this->TextColor;
            $family = $this->FontFamily;
            $style = $this->FontStyle;
            $size = $this->FontSizePt;

            $this->SetTextColor($this->font_color[0], $this->font_color[1], $this->font_color[2]);
            $this->SetFont($this->font_name,'',$this->font_size);

            $text = 'Page '.$this->numPageNo();

            // Print centered page number
            $this->Cell(0,10,$text,0,0,'C');

            $this->TextColor = $current_font_colour;
            $this->SetFont($family,$style,$size);
        }

        //print( memory_get_usage() . ' - ' . $this->numPageNo() . '<br/>');
    }

    function headed_paper(){

        Global $UTILS_SERVER_PATH, $UTILS_TEL_MAIN_FAX;
        // Logo
        $this->Image($UTILS_SERVER_PATH.$this->logo,140,10,55);

        $this->SetFont($this->font_name,'B',12);

        $this->MultiCell(0,7,'',0,1);
        $this->MultiCell(0,12,$this->header_title,0,1);
        $old_widths = $this->GetWidths();
        $widths = array(12,80);$this->SetWidths($widths);
        $this->SetFont($this->font_name,'',8);
        $aRow = Array();
        $aRow['style'] = array('B', '');
        $aRow['line_height'] = 3.5;
        $caption = array('Tel:
Fax:
Add: 
Email:
',$this->header_phone.'
'.$UTILS_TEL_MAIN_FAX.'
RMG House, Essex Road, Hoddesdon, EN11 0DR
'.$this->header_email.'
');
        $this->add_row($caption, $aRow);

        $this->MultiCell(0,9,'',0,1);
        $this->line(10,37,200,37);
        $this->SetWidths($old_widths);
    }

    function set_watermark($watermark_x, $watermark_y, $watermark_angle, $watermark_text, $watermark_family, $watermark_size, $watermark_style, $watermark_color){
        $this->watermark_x = $watermark_x;
        $this->watermark_y = $watermark_y;
        $this->watermark_text = $watermark_text;
        $this->watermark_angle = $watermark_angle;
        $this->watermark_family = $watermark_family;
        $this->watermark_size = $watermark_size;
        $this->watermark_style = $watermark_style;
        $this->watermark_color = $watermark_color;
        $this->include_watermark = true;
    }

    function SetDefaultFont($family, $style, $size){
        $this->font_name = $family;
        $this->font_style = $style;
        $this->font_size = $size;
    }

    function add_page($orientation='', $size=''){

        $this->reset_page_style();
        $this->AddPage($orientation, $size);
    }

    function importPDF($source, $pages = ''){

        $pagecount = $this->setSourceFile($source);

        if($pagecount !== false){

            $was_true = false;

            if($pages != ''){
                $page_array = explode(',', $pages);
            }else{
                $page_array = array();
                for($pc=1;$pc<=$pagecount;$pc++){
                    $page_array[$pc-1] = $pc;
                }
            }

            if($this->include_header == true){
                $was_true = true;
            }

            $_w =  $this->w;
            $_h =  $this->h;

            $this->include_header = false;
            for($pc=1;$pc<=$pagecount;$pc++){

                if(in_array($pc, $page_array)){

                    $tplidx = $this->importPage($pc, '/MediaBox');

                    $size = $this->getTemplateSize($tplidx, 0, 0);

                    if($size['w'] < $size['h']){
                        $this->DefOrientation = 'P';
                    }else{
                        $this->DefOrientation = 'L';
                    }

                    $this->add_page($this->DefOrientation);
                    $this->useTemplate($tplidx, null, null, 0, 0, true);
                }
            }

            $this->w=$_w;
            $this->h=$_h;

            if($this->w < $this->h){
                $this->DefOrientation = 'P';
            }else{
                $this->DefOrientation = 'L';
            }

            $this->CurOrientation = $this->DefOrientation;

            if($was_true == true){
                $this->include_header = true;
            }

            return true;
        }else{
            return false;
        }
    }

    /**
     * Sets current tag to specified style
     *
     * @accesspublic
     * @param   string $tag - tag name
     * @param   string $name - text font family name
     * @param   string $style - text font style
     * @param   numeric $size - text font size
     * @param   array $color - text color
     * @return  void
     */
    public function SetStyle($tag, $family="", $style, $size="", $color=null){

        if($family == ""){
            $family = $this->font_name;
        }

        if($size == ""){
            $size = $this->font_size;
        }

        if(is_null($color)){
            $color = $this->font_color;
        }

        if ($tag != "") {
            //use case insensitive tags
            $tag=trim(strtoupper($tag));

            if (!isset($this->tag_style[$tag])){
                $this->tag_style[$tag]['name']=trim($family);
                $this->tag_style[$tag]['style']=trim($style);
                $this->tag_style[$tag]['size']=trim($size);
                $this->tag_style[$tag]['color']=$color;
            }
        }
    }


    function RotatedText($x, $y, $txt, $angle){

        //Text rotated around its origin
        $this->Rotate($angle, $x, $y);
        $this->Text($x, $y, $txt);
        $this->Rotate(0);
    }


    function Rotate($watermarkAngle, $x=-1, $y=-1){

        if($x==-1){
            $x=$this->x;
        }
        if($y==-1){
            $y=$this->y;
        }
        if($this->angle!=0){
            $this->_out('Q');
        }
        $this->angle=$watermarkAngle;
        if($watermarkAngle != 0){

            $watermarkAngle*=M_PI/180;
            $c=cos($watermarkAngle);
            $s=sin($watermarkAngle);
            $cx=$x*$this->k;
            $cy=($this->h-$y)*$this->k;
            $this->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm',$c,$s,-$s,$c,$cx,$cy,-$cx,-$cy));
        }
    }

    function heading($title, $size=20, $a='L', $extra_line = true, $TOC=true){
        $this->SetFont($this->font_name,'',$size);
        $this->MultiCell(0, $this->line_height, $title, 0, $a, 0);
        $this->Ln();
        if($extra_line == true){
            $this->Ln();
        }
        $this->SetFont($this->font_name,'',$this->font_size);

        $this->level = 0;

        if($TOC){
            $this->section_no ++;
            $this->TOC_Entry($title, 0);
        }
    }

    function subheading($title, $size=16, $a='L', $extra_line=true, $TOC=true, $toc_level=1){

        if($TOC){
            $this->TOC_Entry($title, $toc_level);
        }

        $this->level = $toc_level;

        $this->SetFont('','B', $size);
        $this->MultiCell(0, $this->line_height, $title, 0, $a, 0);
        $this->Ln(3);
        if($extra_line == true){
            $this->Ln(3);
        }
        $this->SetFont('','',$this->font_size);
    }

    function MultiCell($w, $h, $txt, $border=0, $align='J', $fill=false){
        $txt_array = $this->split_by_tags($txt);
        // Output text with automatic or explicit line breaks
        if($w==0)
            $w = $this->w-$this->rMargin-$this->x;
        $wmax = ($w-2*$this->cMargin);

        $current_font['name'] = $this->FontFamily;
        $current_font['size'] = $this->FontSizePt;
        $current_font['style'] = $this->FontStyle.($this->underline ? 'U' : '');
        $current_font['color'] = $this->TextColor;

        $_x = $this->x;

        $previous_w = 0;
        $_w = 0;

        for($t_no=0;$t_no<count($txt_array);$t_no++){
            if($txt_array[$t_no][1] == ''){
                $this->SetFont($current_font['name'], $current_font['style'], $current_font['size']);
            }else{
                $found = true;
                $thetag = strtoupper($txt_array[$t_no][1]);
                $this->SetFont($this->tag_style[$thetag]['name'], $this->tag_style[$thetag]['style'], $this->tag_style[$thetag]['size']);
                $h = ($this->tag_style[$thetag]['size'] / 2);
            }

            $cw = &$this->CurrentFont['cw'];

            $s = str_replace("\r",'',$txt_array[$t_no][0]);

            if ($this->unifontSubset) {
                $nb=mb_strlen($s, 'utf-8');
                while($nb>0 && mb_substr($s,$nb-1,1,'utf-8')=="\n")	$nb--;
            }else {
                $nb = strlen($s);
                if($nb>0 && $s[$nb-1]=="\n")
                    $nb--;
            }
            $b = 0;
            if($border){
                if($border==1){
                    $border = 'LTRB';
                    $b = 'LRT';
                    $b2 = 'LR';
                }else{
                    $b2 = '';
                    if(strpos($border,'L')!==false)
                        $b2 .= 'L';
                    if(strpos($border,'R')!==false)
                        $b2 .= 'R';
                    $b = (strpos($border,'T')!==false) ? $b2.'T' : $b2;
                }
            }
            $sep = -1;
            $i = 0;
            $j = 0;
            $l = $_w;
            $ns = 0;
            $nl = 1;
            if($t_no > 0){
                $new_x = ($_w + $_x)-1;
                $_h = $h;
                if($new_x > $wmax){
                    $new_x = $this->lMargin;
                    $_h = 0;
                    $l = 0;
                }else{
                    if ($this->unifontSubset) { $new_x += $this->GetStringWidth(' '); }
                    else { $new_x += ($cw[' '])*$this->FontSize/1000; }
                }

                //print($_w . ' - ' . $new_x . ' - ' . $txt_array[$t_no][0] . '<br />');
                $this->SetXY($new_x, ($this->y - $_h));
            }

            $_w = 0;
            while($i<$nb){
                // Get next character
                if ($this->unifontSubset) {
                    $c = mb_substr($s,$i,1,'UTF-8');
                }else {
                    $c=$s[$i];
                }
                if($c=="\n"){
                    // Explicit line break
                    if($this->ws>0)
                    {
                        $this->ws = 0;
                        $this->_out('0 Tw');
                    }

                    if ($this->unifontSubset) {
                        $this->Cell($w,$h,mb_substr($s,$j,$i-$j,'UTF-8'),$b,2,$align,$fill);
                    }
                    else {
                        $this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
                    }
                    $i++;
                    $sep = -1;
                    $j = $i;
                    $l = 0;
                    $ns = 0;
                    $nl++;
                    $_w = 0;
                    if($border && $nl==2)
                        $b = $b2;
                    continue;
                }
                if($c==' '){
                    $sep = $i;
                    $ls = $l;
                    $ns++;
                }

                if ($this->unifontSubset) { $l += $this->GetStringWidth($c); }
                else { $l += $cw[$c]*$this->FontSize/1000; }

                $_w = $l;

                if($l>$wmax){
                    // Automatic line break
                    if($sep==-1){
                        if($i==$j)
                            $i++;
                        if($this->ws>0){
                            $this->ws = 0;
                            $this->_out('0 Tw');
                        }
                        if ($this->unifontSubset) {
                            $this->Cell($w,$h,mb_substr($s,$j,$i-$j,'UTF-8'),$b,2,$align,$fill);
                        }else{
                            $this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
                        }
                    }else{
                        if($align=='J'){
                            $this->ws = ($ns>1) ? ($wmax-$ls)/($ns-1) : 0;
                            $this->_out(sprintf('%.3F Tw',$this->ws*$this->k));
                        }
                        if ($this->unifontSubset) {
                            $this->Cell($w,$h,mb_substr($s,$j,$sep-$j,'UTF-8'),$b,2,$align,$fill);
                        }
                        else {
                            $this->Cell($w,$h,substr($s,$j,$sep-$j),$b,2,$align,$fill);
                        }
                        $i = $sep+1;
                    }
                    $sep = -1;
                    $j = $i;
                    $l = 0;
                    $ns = 0;
                    $nl++;
                    $_w = 0;
                    $this->x = $_x;
                    if($border && $nl==2)
                        $b = $b2;
                }else{
                    $i++;
                }
            }

            // Last chunk
            if($this->ws>0){
                $this->ws = 0;
                $this->_out('0 Tw');
            }
            if($border && strpos($border,'B')!==false)
                $b .= 'B';

            if ($this->unifontSubset) {
                $this->Cell($w,$h,mb_substr($s,$j,$i-$j,'UTF-8'),$b,2,$align,$fill);
            }else{
                $this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
            }

            $previous_w = $_w;
        }
        $this->SetFont($current_font['name'], $current_font['style'], $current_font['size']);
        $this->x = $this->lMargin;
        return $nl;
    }

    function MultiCellBlt($w, $h, $blt, $txt, $indent=0, $maxbul="", $border=0, $align='L', $fill=0){
        //Get bullet width including margins
        $blt_width = $this->GetStringWidth($blt)+$this->cMargin*2;
        if($maxbul != ""){
            $blt_width = $this->GetStringWidth($maxbul)+$this->cMargin*2;
        }

        //Save x
        $bak_x = $this->x;

        if($indent > 0){
            $this->Cell(($blt_width * $indent), $h, ' ', 0, '', $fill);
        }

        //Output bullet
        $this->Cell($blt_width, $h, $blt, 0, '', $fill);
        //Output text
        if($w == 0){
            $w = $this->w-$this->rMargin-$this->x;
            if($indent > 0){
                $w = $w - ($blt_width * $indent);
            }
        }
        $this->MultiCell($w-$blt_width, $h, $txt, $border, $align, $fill);

        //Restore x
        $this->x = $bak_x;
    }

    function SetWidths($w){
        //Set the array of column widths
        $this->widths=$w;
    }

    function GetWidths(){
        //Set the array of column widths
        return $this->widths;
    }

    function SetAligns($a){
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function CheckPageBreak($h, $do=true){
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger){
            if($do == true && $h < $this->PageBreakTrigger){
                $this->add_page($this->CurOrientation);
            }
            return true;
        }else{
            return false;
        }
    }

    function NbLines($w, $txt, $type="text"){
        //Computes the number of lines a MultiCell of width w will take
        $cw = &$this->CurrentFont['cw'];
        if($w==0){
            $w=$this->w-$this->rMargin-$this->x;
        }

        //$wmax = ($w-2*$this->cMargin);
        if($this->cMargin > 0){
            $wmax = ($w/$this->cMargin);
        }else{
            $wmax = 0;
        }
        $s=str_replace("\r", '', $txt);

        if ($this->unifontSubset) {
            $nb=mb_strlen($s, 'utf-8');
            while($nb>0 && mb_substr($s,$nb-1,1,'utf-8')=="\n")	$nb--;
        }
        else {
            $nb = strlen($s);
            if($nb>0 && $s[$nb-1]=="\n")
                $nb--;
        }

        $end_tag = false;
        $is_tag = false;
        $last_tag = '';

        $sep = -1;
        $i = 0;
        $j = 0;
        $l = 0;
        $ns = 0;
        $nl = 1;
        while($i<$nb){
            // Get next character
            if ($this->unifontSubset) {
                $c = mb_substr($s,$i,1,'UTF-8');
            }else{
                $c=$s[$i];
            }
            if($c=="\n"){
                if($type=="text"){
                    $i++;
                    $sep = -1;
                    $j = $i;
                    $l = 0;
                    $ns = 0;
                    $nl++;
                    continue;
                }
            }
            if($c==' '){
                $sep = $i;
                $ls = $l;
                $ns++;
            }
            if($c=='<'){
                if ($this->unifontSubset) {
                    $tempStr = mb_substr($s, $i, $nb,'UTF-8');
                }else{
                    $tempStr = substr($s, $i, $nb);
                }
                if(strpos($tempStr, '>') < 50){
                    $is_tag = true;
                }
            }

            if($is_tag == true){
                if($c=="/"){
                    $end_tag = true;
                }elseif($c=='<'){
                    $last_tag = '';
                }elseif($c=='>'){
                    if($end_tag == true && !isset($this->tag_style[strtoupper($last_tag)])){
                        $i++;
                        $sep = -1;
                        $j = $i;
                        $l = 0;
                        $ns = 0;
                        $nl++;
                        $end_tag = false;
                        $is_tag = false;
                        continue;
                    }else{
                        $is_tag = false;
                    }
                }else{
                    $last_tag .= $c;
                }
                $i++;
            }else{
                if ($this->unifontSubset) { $l += $this->GetStringWidth($c); }
                else { $l += $cw[$c]*$this->FontSize/1000; }

                //if($l>$w){
                if($l>$wmax){
                    // Automatic line break
                    if($sep==-1){
                        if($i==$j)
                            $i++;
                    }else{
                        $i = $sep+1;
                    }
                    $sep = -1;
                    $j = $i;
                    $l = 0;
                    $ns = 0;
                    $nl++;
                }else{
                    $i++;
                }
            }
        }

        if($type == 'html' && $nl > 1){
            $nl--;
        }

        //print($nl . ' - ' . $txt . ' - ' . $wmax . ' - ' . $this->cMargin . ' - ' . $this->GetStringWidth($txt) . ' - ' . $l . ' - ' . $this->FontFamily . ' - ' . $this->FontStyle . ' - ' . $this->FontSize . '<br />');
        return $nl;
    }

    function cell_contents_split($h, $w, $value, $type, $options){
        $darray = array();

        if($type == "text" || $type == "html"){
            $darray = $this->split_by_height($h, $w, $value, $options, $type);
            $darray[4] = true;
        }else{
            $_h = $h;
            //For each list item check whether it will fit on the page
            for($b=0;$b<count($value);$b++){
                $carray = $this->split_by_height($_h, $w, $value[$b], $options, $type);
                //If it does not fit on the page
                if($carray[1] != ''){
                    $darray[0] = array();
                    $darray[1] = array();
                    //Add first half of array to the first item holder
                    for($c=0;$c<$b;$c++){
                        array_push($darray[0], $value[$c]);
                        //Set start variable
                        $darray[2] = $c+1;
                    }
                    //Add first half of last item in first half of array into the first item holder
                    array_push($darray[0], $carray[0]);

                    if($carray[0] != ''){
                        //If first half is not blank and second half of word to second item holder
                        //Set the has split variable to true
                        array_push($darray[1], $carray[1]);
                        $darray[4] = true;
                        $b ++;
                    }else{
                        //If first half is blank and set the has split variable to true
                        $darray[4] = false;
                    }
                    //Add second half of array to the second item holder
                    for($c=$b;$c<count($value);$c++){
                        array_push($darray[1], $value[$c]);
                    }
                    //Set end ariable
                    $darray[3] = $darray[2] + (count($value) - $darray[2]) - 1;
                    break;
                }
                $nb = max($nb, $this->NbLines($w, $value[$b]));
                $height = $nb*$options['line_height'];
                //Make the available height smaller
                $_h -= ($height + $options['line_height']);
            }
        }
        return $darray;
    }

    function split_by_height($h, $w, $txt, $options, $type="text"){
        //Splits content into 2, where the first reaches the height supplied and the second is the remainder.
        $cw = &$this->CurrentFont['cw'];
        if($w==0){
            $w=$this->w-$this->rMargin-$this->x;
        }

        $wmax = ($w);
        $s=str_replace("\r", '', $txt);

        //Get no characters
        if ($this->unifontSubset) {
            $nb=mb_strlen($s, 'utf-8');
            while($nb>0 && mb_substr($s,$nb-1,1,'utf-8')=="\n")	$nb--;
        }
        else {
            $nb = strlen($s);
            if($nb>0 && $s[$nb-1]=="\n")
                $nb--;
        }

        $end_tag = false;
        $is_tag = false;
        $tags = array();
        $taglist = array();
        $last_tag = '';

        $sep = -1;
        $i = 0;
        $j = 0;
        $l = 0;
        $ns = 0;
        $nl = 1;
        $is_tag = false;
        while($i<$nb){
            // Get next character
            if ($this->unifontSubset) {
                $c = mb_substr($s,$i,1,'UTF-8');
            }else{
                $c=$s[$i];
            }
            //If this is text count new lines
            if($c=="\n"){
                if($type!="html"){
                    $i++;
                    $sep = -1;
                    $j = $i;
                    $l = 0;
                    $ns = 0;
                    $nl++;
                    continue;
                }
            }
            if($c==' '){
                $sep = $i;
                $ls = $l;
                $ns++;
            }
            //Test if there is a tag and set that we are looking for one if there is
            if($c=='<'){
                if ($this->unifontSubset) {
                    $tempStr = mb_substr($s, $i, $nb,'UTF-8');
                }else{
                    $tempStr = substr($s, $i, $nb);
                }
                $thepos = strpos($tempStr, '>');
                if($thepos < 50){
                    $is_tag = true;
                    $last_tag = '';
                }
            }

            if($is_tag == true){
                //Build tag and add new line if it is an end tag
                if($c=="/"){
                    $end_tag = true;
                }elseif($c=='<'){
                    $last_tag = '';
                }elseif($c=='>'){
                    if(strpos(strtolower($tags[count($tags)-1]), 'ol') !== false || strpos(strtolower($tags[count($tags)-1]), 'ul') !== false){
                        $wmax = $w - $this->get_list_width($s, $i, $tags[count($tags)-1]);
                    }
                    if($end_tag == true){
                        array_splice($tags,count($tags)-1, 1);
                    }
                    if($last_tag != '' && $end_tag == false){
                        array_push($tags, $last_tag);
                        if(!isset($this->tag_style[strtoupper($last_tag)])){
                            array_push($taglist, $last_tag);
                        }
                    }
                    if($end_tag == true && !isset($this->tag_style[strtoupper($last_tag)])){
                        $i++;
                        $sep = -1;
                        $j = $i;
                        $l = 0;
                        $ns = 0;
                        $nl++;
                        $end_tag = false;
                        $is_tag = false;
                        continue;
                    }else{
                        $is_tag = false;
                    }
                }else{
                    $last_tag .= $c;
                }
                $i++;
            }else{
                if(strpos(strtolower($last_tag), 'ol') !== false || strpos(strtolower($last_tag), 'ul') !== false){
                    $wmax = $w;
                }
                if ($this->unifontSubset) { $l += $this->GetStringWidth($c); }
                else { $l += $cw[$c]*$this->FontSize/1000; }

                if($l>$wmax){
                    // Automatic line break
                    if($sep==-1){
                        if($i==$j)
                            $i++;
                    }else{
                        $i = $sep+1;
                    }
                    $sep = -1;
                    $j = $i;
                    $l = 0;
                    $ns = 0;
                    $nl++;
                }else{
                    $i++;
                }
                //If the number of lines is greater than the height, work backwards to find the next space or line return
                if(($nl * $options['line_height']) > $h){

                    for($f = $i; $f > 0; $f--){
                        if ($this->unifontSubset) {
                            $char = mb_substr($s,$f,1,'UTF-8');
                        }else{
                            $char = $s[$f];
                        }

                        if($char==" " && $f != $i){
                            $f++;
                            $new_l = 0;
                            for($q=0;$q<=$f;$q++){
                                if ($this->unifontSubset) {
                                    $new_l += $this->GetStringWidth(mb_substr($s,$q,1,'UTF-8'));
                                }else{
                                    $new_l += $cw[$s[$q]]*$this->FontSize/1000;
                                }
                            }
                            //print($new_l . ' - ' . $wmax);
                            if($new_l>$wmax){
                                $f--;
                            }else{
                                break;
                            }
                        }elseif($char=="\n" || $char==">"){
                            $f++;
                            break;
                        }
                    }

                    for($e = $f; $e > 0; $e--){
                        if($s[$e]=='<'){
                            break;
                        }
                    }
                    $nl--;

                    if ($this->unifontSubset) {
                        $string = mb_substr($s, 0, $f,'UTF-8');
                    }else{
                        $string = substr($s, 0, $f);
                    }
                    $tagings = array_reverse($tags);
                    foreach($tagings as $tag){
                        $string .= '</'.$tag.'>';
                    }
                    break;
                }
            }
        }

        //If the string is blank then it fits in the gap, else build two strings
        if($string == '' && $string != "0" && $string != 0){
            $string = $s;
            $string2 = '';
        }else{

            $string2 = substr($s, $f, $nb);

            if ($this->unifontSubset) {
                $c = mb_substr($s,$f,1,'UTF-8');
            }else{
                $c=$s[$f];
            }

            //Remove first char if it's a space or line return
            if($c == " " || $c == "\n"){
                if ($this->unifontSubset) {
                    $string2 = mb_substr($string2,1,strlen($string2),'UTF-8');
                }else{
                    $string2 = substr($string2, 1, strlen($string2));
                }
            }
            $is_tag = false;

            //If first char is open tag, check if there is a tag at the beginning
            if($c=='<'){
                if ($this->unifontSubset) {
                    $tempStr = mb_substr($s, $f, $nb,'UTF-8');
                }else{
                    $tempStr = substr($s, $f, $nb);
                }
                if(strpos($tempStr, '>') < 50){
                    $is_tag = true;
                }
            }

            $start = 0;
            $end = 0;

            //If there is no tag at the start and type is html, add the last tag
            if($is_tag == false && $type == "html"){
                //If the last tag is an li, find the parent tag and add that.
                if(strtolower($taglist[count($taglist) -1]) == "li"){
                    $tag = '';
                    for($i=count($taglist)-1;$i>=0;$i--){
                        if(strtolower($taglist[$i]) != 'li'){
                            $tag = $taglist[$i];
                            break;
                        }
                    }

                    $tempStr = $string2;

                    $tag_name = substr($tag, 0, 2);
                    $close_tag_pos = strpos($tempStr, $tag_name);

                    if ($this->unifontSubset) {
                        $tempStr = mb_substr($tempStr, 0, $close_tag_pos, 'UTF-8');
                    }else{
                        $tempStr = substr($tempStr, 0, $close_tag_pos);
                    }

                    $no_li = substr_count(strtoupper($tempStr), '<LI>');
                    $start = (count($tags) - $i) - 2;
                    $end = $no_li + $start - 1;
                }
            }

            $tagings = array_reverse($tags);

            foreach($tagings as $tag){
                $string2 = '<'.$tag.'>'.$string2;
            }
            $s_array = explode("\r\n", $string2);

            $string2 = '';
            for($i=0;$i<count($s_array);$i++){
                if($s_array[$i] != ''){
                    $string2 .=	$s_array[$i] . "\r\n";
                }
            }
            $string2 = str_replace("\r\n\r\n", "\r\n",substr($string2, 0, -1));
        }

        $data_array = Array();

        array_push($data_array, $string);
        array_push($data_array, $string2);
        array_push($data_array, $start);
        array_push($data_array, $end);

        return $data_array;
    }

    function get_list_width($s, $i, $last_tag){
        $data = new data();
        $tag = $last_tag;
        if ($this->unifontSubset) {
            $tempStr = mb_substr($s, $i, strlen($s), 'UTF-8');
        }else{
            $tempStr = substr($s, $i, strlen($s));
        }

        if(strpos(strtolower($last_tag), 'ol') !== false){
            $last_tag = 'ol';
        }else{
            $last_tag = 'ul';
        }

        $no_li = 1;

        if($last_tag == 'ol'){
            $close_tag_pos = strpos($tempStr, $last_tag);

            if ($this->unifontSubset) {
                $tempStr = mb_substr($tempStr, 0, $close_tag_pos, 'UTF-8');
            }else{
                $tempStr = substr($tempStr, 0, $close_tag_pos);
            }

            $no_li = substr_count(strtoupper($tempStr), '<LI>') - 1;
            $style = '';
        }else{
            $style = 'disc';
        }

        if(strpos(strtolower($tag), "list-style-type:") !== false){
            $start = strpos(strtolower($tag), "list-style-type:") + 16;
            if(strpos(strtolower($tag), ";", $start) !== false){
                $end = strpos(strtolower($tag), ";", $start);
            }else{
                $end = strpos(strtolower($tag), '"', $start);
            }
            if ($this->unifontSubset) {
                $style = mb_substr($tag, $start, $end-$start, 'UTF-8');
            }else{
                $style = substr($tag, $start, $end-$start);
            }
        }

        if($style == 'disc'){
            $blt = "•";
        }elseif($style == 'circle'){
            $blt = "◦";
        }elseif($style == 'square'){
            $blt = "▪";
        }elseif($style==''){
            $blt = $no_li . ".";
        }elseif($style=="upper-roman"){
            $blt = strtoupper($data->numberToRoman($no_li) . ".");
        }elseif($style=="lower-roman"){
            $blt = strtolower($data->numberToRoman($no_li) . ".");
        }elseif($style=="lower-alpha" || $style=="lower-latin" || $style=="upper-alpha" || $style=="upper-latin"){
            $b = ceil($no_li / 26);
            $c = $no_li % 26;
            if($b == 0){
                $b = 1;
            }
            if($c === 0 && $no_li != 0){
                $b++;
            }
            $the_char = chr((($no_li - (26 * ($b-1))) + 1) + 96);
            $blt = '';
            for($k=0;$k<$b;$k++){
                $blt .= $the_char;
            }
            $blt .= '.';
            if($style=="upper-alpha" || $style=="upper-latin"){
                $blt = strtoupper($blt);
            }
        }

        return $this->GetStringWidth($blt)+$this->cMargin*2;
    }

    function calc_width($data, $options, $no){
        $nb = 0;

        for($i=0;$i<count($data);$i++) {
            //Check if standard text or arrayed details
            if(is_array($data[$i])){
                $value = $data[$i]['value'];
                $type = $data[$i]['type'];
            }else{
                $type = "text";
                $value = $data[$i];
            }

            //Set font before checking
            if (isset($options['style'][$i])) {
                $this->SetFont('', $options['style'][$i]);
            }

            $_w = $this->widths[$i];

            //Check for padding to take into consideration
            if(!is_null($options['padding_size'][$i]) && !is_null($options['padding'][$i])){

                if($options['padding'][$i] == 1){
                    $_w -= ($options['padding_size'][$i] * 2);
                }
                if(strstr($options['padding'][$i], 'L')!==false){
                    $_w -= $options['padding_size'][$i];
                }
                if(strstr($options['padding'][$i], 'R')!==false){
                    $_w -= $options['padding_size'][$i];
                }
            }

            if($type=="bullet"){
                //if type is bullet draw bullets
                $_w -= $this->GetStringWidth($data[$i]['options']['bullet'])+$this->cMargin*2;
            }elseif($type=="numbering"){
                //if type is numbering draw numbering
                $start = 0;
                if(!is_null($data[$i]['options']['start'])){
                    $start = $data[$i]['options']['start'];
                }

                $increment = $start + 1;

                $oldwidth = 0;

                for($a=0;$a < count($value);$a++){
                    $blt = $a + $increment . ".";
                    if($type=="upper-roman"){
                        $blt = strtoupper($data->numberToRoman($a + $increment) . ".");
                    }elseif($type=="lower-roman"){
                        $blt = strtolower($data->numberToRoman($a + $increment) . ".");
                    }elseif($type=="lower-alpha" || $type=="lower-latin" || $type=="upper-alpha" || $type=="upper-latin"){
                        $b = ceil($a / 26);
                        $c = $a % 26;
                        if($b == 0){
                            $b = 1;
                        }
                        if($c === 0 && $a != 0){
                            $b++;
                        }
                        $the_char = chr((($a - (26 * ($b-1))) + $increment) + 96);
                        $blt = '';
                        for($k=0;$k<$b;$k++){
                            $blt .= $the_char;
                        }
                        $blt .= '.';
                        if($type=="upper-alpha" || $type=="upper-latin"){
                            $blt = strtoupper($blt);
                        }
                    }

                    $newwidth = $this->GetStringWidth($blt);

                    if($newwidth > $oldwidth){
                        $maxbul = $blt;
                    }

                    $oldwidth = $newwidth;
                }

                $_w -= $maxbul+$this->cMargin*2;
            }
            if($no == $i){
                break;
            }
        }
        return $_w;
    }

    function calc_height($data, $options){
        $nb = 0;

        for($i=0;$i<count($data);$i++) {
            //Check if standard text or arrayed details
            if(is_array($data[$i])){
                $value = $data[$i]['value'];
                $type = $data[$i]['type'];
            }else{
                $type = "text";
                $value = $data[$i];
            }

            //Set font before checking
            if (isset($options['style'][$i])) {
                $this->SetFont('', $options['style'][$i]);
            }

            $_w = $this->widths[$i];

            //Check for padding to take into consideration
            if(!is_null($options['padding_size'][$i]) && !is_null($options['padding'][$i])){

                if($options['padding'][$i] == 1){
                    $_w -= ($options['padding_size'][$i] * 2);
                }
                if(strstr($options['padding'][$i], 'L')!==false){
                    $_w -= $options['padding_size'][$i];
                }
                if(strstr($options['padding'][$i], 'R')!==false){
                    $_w -= $options['padding_size'][$i];
                }
            }

            if($type=="text"){
                //if type is text get height
                $nb = max($nb, $this->NbLines($_w, $value));
            }elseif($type=="bullet"){
                //if type is bullet draw bullets
                $_w = $this->calc_width($data, $options, $i);
                $newval = '';
                for($b=0;$b<count($value);$b++){
                    $newval .= $value[$b] . "\r\n";
                }
                $nb = max($nb, $this->NbLines($_w, $newval));
            }elseif($type=="numbering"){
                $_w = $this->calc_width($data, $options, $i);

                $newval = '';

                for($b=0;$b<count($value);$b++){
                    $newval .= $value[$b] . "\r\n";
                }
                $nb = max($nb, $this->NbLines($_w, $newval));
            }elseif($type=="html"){
                //If type is html get height
                $nb = max($nb, $this->NbLines($_w, $value, "html"));
            }
        }
        return $nb;
    }

    function build_borders($data, $options=Array(), $i, $h){
        $x = $this->GetX();
        $y = $this->GetY();
        // width
        $w = $this->widths[$i];
        // padding size
        $ps = isset($options['padding_size'][$i]) ? $options['padding_size'][$i] : '0';
        // padding
        $p = isset($options['padding'][$i]) ? $options['padding'][$i] : '1';
        // fill
        $f = isset($options['fill'][$i]) ? $options['fill'][$i] : '0';
        // fill colour
        $fc = isset($options['fill_color'][$i]) ? $options['fill_color'][$i] : $this->colours['white'];
        //Gradient colour
        $gc = isset($options['gradient_color'][$i]) ? $options['gradient_color'][$i] : array();

        $est_border_width = 0.1;

        //Border
        if (is_numeric($options['border'][$i])) {
            //If border surrounds cell
            if(is_numeric($p)){
                //if padding surrounds cell
                $h+=($ps*2);
                if($options['border'][$i] > 0){
                    if($f == 1){

                        $this->SetFillColor($fc[0], $fc[1], $fc[2]);
                        $this->Rect($x, $y, $w+($ps*2), $h, 'DF');

                        if(!empty($gc)){
                            //set the coordinates x1, y1, x2, y2 of the gradient (see linear_gradient_coords.jpg)
                            $coords=array(0, 1, 0, 0);
                            //paint a linear gradient
                            $this->LinearGradient($x+$est_border_width, $y+$est_border_width,  $w+($ps*2) - ($est_border_width * 2), $h - ($est_border_width * 2), $fc, $gc, $coords);
                        }
                        $this->SetFillColor(0,0,0);
                    }else{
                        $this->Rect($x, $y, $w+($ps*2), $h);
                    }
                }
                $this->SetXY($x + $ps, $y + $ps);
            }else{
                //if padding surrounds part of cell
                $_p = strtoupper($p);
                $_w = $w;
                $_h = $h;
                $_x = $x;
                $_y = $y;

                if (strstr($_p, 'L')!==false) {
                    $_w += $ps;
                }
                if (strstr($_p, 'R')!==false) {
                    $_w += $ps;
                }
                if (strstr($_p, 'T')!==false) {
                    $_h += $ps;
                    $_y += $ps;
                }
                if (strstr($_p, 'B')!==false) {
                    $_h += $ps;
                }
                if($options['border'][$i] > 0){
                    if($f == 1){
                        $this->SetFillColor($fc[0], $fc[1], $fc[2]);
                        $this->Rect($x, $y, $_w, $_h, 'DF');

                        if(!empty($gc)){
                            //set the coordinates x1, y1, x2, y2 of the gradient (see linear_gradient_coords.jpg)
                            $coords=array(0, 1, 0, 0);
                            //paint a linear gradient
                            $this->LinearGradient($x + $est_border_width, $y + $est_border_width,  $_w - ($est_border_width * 2), $_h - ($est_border_width * 2), $fc, $gc, $coords);
                        }
                        $this->SetFillColor(0,0,0);
                    }else{
                        $this->Rect($x, $_y, $_w, $_h);
                    }
                }
                if (strstr($_p, 'T')!==false) {
                    $this->SetXY($x, $_y + $ps);
                }
                if (strstr($_p, 'L')!==false) {
                    $this->SetX($x + $ps);
                }
            }
        }else {
            //If border surrounds part of cell
            $_border = strtoupper($options['border'][$i]);
            if(is_numeric($p)){
                //if padding surrounds cell
                if (strstr($_border, 'L')!==false) {
                    $this->Line($x, $y, $x, $y+$h+($ps*2));
                }
                if (strstr($_border, 'R')!==false) {
                    $this->Line($x+$w+($ps*2), $y, $x+$w+($ps*2), $y+$h+($ps*2));
                }
                if (strstr($_border, 'T')!==false) {
                    $this->Line($x, $y, $x+$w+($ps*2), $y);
                }
                if (strstr($_border, 'B')!==false) {
                    $this->Line($x, $y+$h+($ps*2), $x+$w+($ps*2), $y+$h+($ps*2));
                }
                if($f == 1){

                    $this->SetFillColor($fc[0], $fc[1], $fc[2]);
                    $this->SetDrawColor($fc[0], $fc[1], $fc[2]);
                    $this->Rect($x, $y, $_w, $_h, 'DF');

                    if(!empty($gc)){
                        //set the coordinates x1, y1, x2, y2 of the gradient (see linear_gradient_coords.jpg)
                        $coords=array(0, 1, 0, 0);
                        //paint a linear gradient
                        $this->LinearGradient($x + $est_border_width, $y + $est_border_width,  $_w - ($est_border_width * 2), $_h - ($est_border_width * 2), $fc, $gc, $coords);
                    }
                    $this->SetFillColor(0,0,0);
                    $this->SetDrawColor(0,0,0);
                }

                $this->SetXY($x + $ps, $y + $ps);
            }else{
                //if padding surrounds part of cell
                $_p = strtoupper($p);
                $_w = $w;
                $_h = $h;
                $_x = $x;
                $_y = $y;

                if (strstr($_p, 'L')!==false) {
                    $_w += $ps;
                }
                if (strstr($_p, 'R')!==false) {
                    $_w += $ps;
                }
                if (strstr($_p, 'T')!==false) {
                    $_h += $ps;
                    $_y += $ps;
                }
                if (strstr($_p, 'B')!==false) {
                    $_h += $ps;
                }

                if($f == 1){

                    $this->SetFillColor($fc[0], $fc[1], $fc[2]);
                    $this->SetDrawColor($fc[0], $fc[1], $fc[2]);
                    $this->Rect($x, $y, $_w, $_h, 'DF');

                    if(!empty($gc)){
                        //set the coordinates x1, y1, x2, y2 of the gradient (see linear_gradient_coords.jpg)
                        $coords=array(0, 1, 0, 0);
                        //paint a linear gradient
                        $this->LinearGradient($x + $est_border_width, $y + $est_border_width,  $_w - ($est_border_width * 2), $_h - ($est_border_width * 2), $fc, $gc, $coords);
                    }
                    $this->SetFillColor(0,0,0);
                    $this->SetDrawColor(0,0,0);
                }

                if (strstr($_border, 'L')!==false) {
                    $this->Line($_x, $_y, $_x, $_y+$_h);
                }

                if (strstr($_border, 'R')!==false) {
                    $this->Line($_x+$_w, $_y, $_x+$_w, $_y+$_h);
                }
                if (strstr($_border, 'T')!==false) {
                    $this->Line($_x, $_y, $_x+$_w, $_y);
                }
                if (strstr($_border, 'B')!==false) {
                    $this->Line($_x, $_y+$_h, $_x+$_w, $_y+$_h);
                }
                if (strstr($_p, 'L')!==false) {
                    $this->SetX($_x + $ps);
                }
                if (strstr($_p, 'T')!==false) {
                    $this->SetXY($_x, $_y + $ps);
                }
            }
        }
        return $h;
    }

    /**
     * Create a row header
     *
     * @param		Array	$data			Array of text for each column
     * @param		Array	$options		Array of Options
     * @arrayitem	Array	border			Array of characters/integers for each column, 1 for border, 0 for none or a combination of L for left, T for top, R for right, B for bottom
     * @arrayitem	Array	align			Array of characters for each column, L for Left, R for Right, C for Centre
     * @arrayitem	Array	style			Array of characters for each column, B for Bold, I for Italic, U for underline
     * @arrayitem	Array	maxline			Array of integers for each column, Overrides auto-calculated number of lines
     * @arrayitem	Array	padding_size	Array of integers for each column, sets amount of padding
     * @arrayitem	Array	padding			Array of characters/integers for each column, 1 for padding, 0 for none or a combination of L for left, T for top, R for right, B for bottom
     * @arrayitem	Array	fill			Array of integers for each column, 1 for fill, 0 for not
     * @arrayitem	Array	fill_color		Array of integers for RGB
     * @arrayitem	Array	font_color		Array of integers for RGB
     * @arrayitem	Integer	line_height		Integer to set the line height in mm
     * @param		Boolean	$print_header	Boolean to set whether the header row should be printed now as well as setting for repeats
     */
    function add_header($data, $options=Array(), $print_header=true){
        $this->fr_data = $data;
        $this->fr_border = $options['border'];
        $this->fr_align = $options['align'];
        $this->fr_style = $options['style'];
        $this->fr_maxline = $options['maxline'];
        $this->fr_padding_size = $options['padding_size'];
        $this->fr_padding = $options['padding'];
        $this->fr_fill = $options['fill'];
        $this->fr_fill_color = $options['fill_color'];
        $this->fr_gradient_color = $options['gradient_color'];
        $this->fr_font_color = $options['font_color'];
        $this->fr_line_height = $options['line_height'];

        //calculate number of lines
        $nb = $this->calc_height($data, $options);

        if (count($options['maxline'])) {
            $_maxline = max($options['maxline']);
            if ($nb > $_maxline) {
                $nb = $_maxline;
            }
        }

        if(!is_null($options['padding_size'][0]) && !is_null($options['padding'][0])){
            $h = ($options['line_height']*$nb)+($options['padding_size'][0]*2);
        }else{
            $h = ($options['line_height']*$nb);
        }
        //Issue a page break first if needed
        $page_break = $this->CheckPageBreak($h+5, false);

        if($print_header){
            if(!$page_break){
                $this->print_header();
            }else{
                $this->add_page($this->CurOrientation);
                $this->print_header();
            }
        }
    }

    function print_header(){
        $aRow = Array();
        $aRow['border'] = $this->fr_border;
        $aRow['align'] = $this->fr_align;
        $aRow['style'] = $this->fr_style;
        $aRow['maxline'] = $this->fr_maxline;
        $aRow['padding_size'] = $this->fr_padding_size;
        $aRow['padding'] = $this->fr_padding;
        $aRow['fill'] = $this->fr_fill;
        $aRow['fill_color'] = $this->fr_fill_color;
        $aRow['gradient_color'] = $this->fr_gradient_color;
        $aRow['font_color'] = $this->fr_font_color;
        $aRow['line_height'] = $this->fr_line_height;

        $this->add_row($this->fr_data, $aRow, false);
    }

    /**
     * Create a Row
     *
     * @param		Array	$data			Array of text for each column
     * @param		Array	$options		Array of Options
     * @arrayitem	Array	border			Array of characters/integers for each column, 1 for border, 0 for none or a combination of L for left, T for top, R for right, B for bottom
     * @arrayitem	Array	align			Array of characters for each column, L for Left, R for Right, C for Centre
     * @arrayitem	Array	style			Array of characters for each column, B for Bold, I for Italic, U for underline
     * @arrayitem	Array	maxline			Array of integers for each column, Overrides auto-calculated number of lines
     * @arrayitem	Array	padding_size	Array of integers for each column, sets amount of padding
     * @arrayitem	Array	padding			Array of characters/integers for each column, 1 for padding, 0 for none or a combination of L for left, T for top, R for right, B for bottom
     * @arrayitem	Array	margin			Array of integers for each column, height in pixels
     * @arrayitem	Array	fill			Array of integers for each column, 1 for fill, 0 for not
     * @arrayitem	Array	fill_color		Array of integers for RGB
     * @arrayitem	Array	font_color		Array of integers for RGB
     * @arrayitem	Integer	line_height		Integer to set the line height in mm
     * @arrayitem	Boolean	$repeat_header	Boolean to set whether the header row should be repeated when breaking over pages
     * @return 								Prints row
     */
    function add_row($data, $options=Array(), $repeat_header=false, $extras=Array()){
        //Set Line Height
        if(!isset($options['line_height']) || $options['line_height'] == ''){
            $options['line_height'] = $this->line_height;
        }

        //Previous Split
        $previous_split = isset($extras['previous_split']) ? $extras['previous_split'] : false;
        //Indent
        $indent = isset($extras['indent']) ? $extras['indent'] : false;
        //Start
        $start = isset($extras['start']) ? $extras['start'] : false;
        //End
        $end = isset($extras['end']) ? $extras['end'] : false;

        $new_extras = $extras;

        //Calculate number of lines
        $nb = $this->calc_height($data, $options);

        //Override number of lines
        if (count($options['maxline'])) {
            $_maxline = max($options['maxline']);
            if ($nb > $_maxline) {
                $nb = $_maxline;
            }
        }

        //Set height to take padding into consideration for page break calculation
        if(!is_null($options['padding_size'][0]) && !is_null($options['padding'][0])){
            $h = ($options['line_height']*$nb)+($options['padding_size'][0]*2);
        }else{
            $h = ($options['line_height']*$nb);
        }

        //If there is not enough room to fit one line on the page then add a new page
        if($this->GetY()+$options['line_height']>$this->PageBreakTrigger){
            $this->add_page($this->CurOrientation);

            //Add header variable is set add table header.
            if($repeat_header == true){
                $this->print_header();
            }
        }

        $new_row = array();

        //Draw the cells of the row
        for($i=0;$i<count($data);$i++) {
            //Set font colour
            if(count($options['font_color'][$i]) == 3){
                $this->SetTextColor($options['font_color'][$i][0], $options['font_color'][$i][1], $options['font_color'][$i][2]);
            }
            //Width
            $w = $this->widths[$i];
            //Padding size
            $ps = isset($options['padding_size'][$i]) ? $options['padding_size'][$i] : '0';
            //Padding
            $p = isset($options['padding'][$i]) ? $options['padding'][$i] : '1';
            //Fill
            $f = isset($options['fill'][$i]) ? $options['fill'][$i] : '0';
            //Fill colour
            $fc = isset($options['fill_color'][$i]) ? $options['fill_color'][$i] : $this->colours['white'];
            //Gradient colour
            $gc = isset($options['gradient_color'][$i]) ? $options['gradient_color'][$i] : array();
            //Alignment
            $a = isset($options['align'][$i]) ? $options['align'][$i] : 'L';
            //Maxline
            $m = isset($options['maxline'][$i]) ? $options['maxline'][$i] : false;
            //Margin
            $ma = isset($options['margin'][$i]) ? $options['margin'][$i] : array();
            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();
            //Setting Style
            if (isset($options['style'][$i])) {
                $this->SetFont('', $options['style'][$i]);
            }

            //Set height to number of lines times the height of one line
            $h = ($options['line_height'])*$nb;

            if(is_array($data[$i])){
                $value = $data[$i]['value'];
                $type = $data[$i]['type'];
            }else{
                $type = "text";
                $value = $data[$i];
            }

            if($type == "numbering" || $type == "bullet"){
                if(count($ma) == count($value)){
                    for($margins=0;$margins<count($ma);$margins++){
                        $h += $this->convert($ma[$margins]);
                    }
                }
            }

            $h2 = 0;
            $h3 = $h;

            if($p == 1){
                $h3 += ($ps * 2);
            }
            if(strstr($p, 'T')!==false){
                $h3 += $ps;
            }
            if(strstr($p, 'B')!==false){
                $h3 += $ps;
            }

            //If height is greater than the remainder of page height
            if(($h3 + $y) > $this->PageBreakTrigger){
                $h2 = 0;
                //Get remaining page height
                $h2 = $this->PageBreakTrigger - $this->GetY();

                if($p == 1){
                    $h2 -= ($ps * 2);
                }
                if(strstr($p, 'T')!==false){
                    $h2 -= $ps;
                }
                if(strstr($p, 'B')!==false){
                    $h2 -= $ps;
                }

                $horig = $h;
                $htemp = $h - $h2;

                //Set height to remaining page height
                $h = $h2;
            }

            if($h2 > $options['line_height'] || $h2 == 0){
                //Create borders and return actual height
                $h = $this->build_borders($data, $options, $i, $h);
            }

            //Split tall text
            if($h2 != 0){
                //if it was calculated that the height is taller than the page height
                if($h2 > $options['line_height']){
                    $_h = $h;

                    if($p == 1){
                        $_h -= ($ps * 2);
                        $_w -= ($ps * 2);
                    }
                    if(strstr($p, 'T')!==false){
                        $_h -= $ps;
                    }
                    if(strstr($p, 'B')!==false){
                        $_h -= $ps;
                    }

                    $_w = $this->calc_width($data, $options, $i);
                    $darray = $this->cell_contents_split($_h, $_w, $value, $type, $options);

                    $the_data = $darray[0];
                    $new_row[$i]['value'] = $darray[1];
                    $new_row[$i]['type'] = $type;
                    $newextras['start'] = $darray[2];
                    $newextras['end'] = $darray[3];
                    $newextras['previous_split'] = $darray[4];
                }else{
                    $the_data = '';
                    $new_row[$i]['value'] = $value;
                    $new_row[$i]['type'] = $type;
                }
            }else{
                $the_data = $value;
                $new_row[$i]['value'] = '';
                $new_row[$i]['type'] = $type;
            }

            if($the_data != ''){
                if($type=="text"){
                    //If type is text draw cell
                    $this->MultiCell($w, ($options['line_height']), html_entity_decode($the_data), 0, $a, 0, $m);
                }elseif($type=="bullet"){
                    //If type is bullet draw bullets
                    $this->add_bullets($the_data, $data[$i]['options']['bullet'], $indent, $w, $previous_split, $ma);
                }elseif($type=="numbering"){
                    //If type is numbering draw numbering
                    $this->add_numbering($the_data, $data[$i]['options']['type'], $start, $indent, $w, $previous_split, $end, $ma);
                }elseif($type=="html"){
                    //If type is html parse html
                    //print(str_replace("<", "&lt;", $the_data) . ' - ' . $h . '<br />');
                    $this->parse_html($the_data, $w, $a, $extras);
                }
            }

            //Put the position to the right of the cell
            if(is_numeric($p)){
                $_border = strtoupper($options['border'][$i]);
                if(is_numeric($_border)){
                    $this->SetXY($x+$w+($ps*2), $y);
                }else{
                    $_x = $w+$x;
                    $_y = $y;
                    //if padding surrounds cell
                    if (strstr($_border, 'L')!==false) {
                        $_x += $ps;
                    }
                    if (strstr($_border, 'R')!==false) {
                        $_x += $ps;
                    }
                    if (strstr($_border, 'T')!==false && $i == count($data)-1) {
                        $_y += $ps*2;
                    }
                    if (strstr($_border, 'B')!==false && $i == count($data)-1) {
                        $_y += $ps*2;
                    }
                    $this->SetXY($_x, $_y);
                }
            }else{
                $_p = strtoupper($p);
                $_w = $w;
                $_h = $h;
                $_x = $x;
                $_y = $y;

                if (strstr($_p, 'L')!==false) {
                    $_w += $ps;
                }
                if (strstr($_p, 'R')!==false) {
                    $_w += $ps;
                }
                if (strstr($_p, 'T')!==false) {
                    $_h += $ps;
                    if(count($data) == 1){
                        $_y += $ps;
                    }
                }
                if (strstr($_p, 'B')!==false) {
                    $_h += $ps;
                    if(count($data) == 1){
                        $_y += $ps;
                    }
                }
                $this->SetXY($_x+$_w, $_y);
            }
            $this->SetTextColor(0,0,0);
        }
        //Go to the next line
        $this->Ln($h);

        //If the text was split across pages add the second half
        for($i=0;$i<count($new_row);$i++){
            if($new_row[$i]['value'] != ''){

                $this->add_page($this->CurOrientation);

                //Print header on new page if variable is true
                if($repeat_header == true){
                    $this->print_header();
                }
                $this->add_row($new_row, $options, $repeat_header, $newextras);
                break;
            }
        }
    }

    function add_para($str="", $double=true, $w=0, $a='L'){
        $str = str_replace("Â", "", utf8_encode(html_entity_decode($str)));
        $str = preg_replace("/<br\W*?\/>/i", "\r\n", $str);
        $nl = $this->MultiCell($w, $this->line_height, $str, 0, $a, 0);
        if($double == true){
            $this->Ln();
            $nl++;
        }
        return $nl;
    }

    function add_bullets($bullet_array, $blt="•", $indent=0, $width=0, $ignore_first=false, $margins=array()){
        if(is_null($blt)){
            $blt = "•";
        }

        if(is_null($indent)){
            $indent = 0;
        }

        for($a=0;$a < count($bullet_array);$a++){
            if($bullet_array[$a] != ''){
                $str = str_replace("Â", "", utf8_encode(html_entity_decode($bullet_array[$a])));
                $str = preg_replace("/<br\W*?\/>/i", "\r\n", $str);

                if($ignore_first == true && $a==0){
                    $this->MultiCellBlt($width, $this->line_height, " ", $str, $indent);
                }else{
                    $this->MultiCellBlt($width, $this->line_height, $blt, $str, $indent);
                }
                $height = 0;
                if(count($margins) == count($bullet_array)){
                    if($margins[$a] != ''){
                        $height += $this->convert($margins[$a]);
                        $this->MultiCellBlt($width, $height, " ", " ", $indent);
                    }
                }
            }
        }
        $this->Ln();
    }

    function add_numbering($number_array, $type="decimal", $start=0, $indent=0, $width=0, $ignore_first=false, $end=0, $margins=array()){
        $data= new data;

        $oldwidth = 0;

        if($end == 0){
            $end = count($number_array);
        }

        $increment = $start + 1;

        for($a=0;$a < $end;$a++){
            if($number_array[$a] != ''){
                $blt = $a + $increment . ".";
                if($type=="upper-roman"){
                    $blt = strtoupper($data->numberToRoman($a + $increment) . ".");
                }elseif($type=="lower-roman"){
                    $blt = strtolower($data->numberToRoman($a + $increment) . ".");
                }elseif($type=="lower-alpha" || $type=="lower-latin" || $type=="upper-alpha" || $type=="upper-latin"){
                    $b = ceil($a / 26);
                    $c = $a % 26;
                    if($b == 0){
                        $b = 1;
                    }
                    if($c === 0 && $a != 0){
                        $b++;
                    }
                    $the_char = chr((($a - (26 * ($b-1))) + $increment) + 96);
                    $blt = '';
                    for($k=0;$k<$b;$k++){
                        $blt .= $the_char;
                    }
                    $blt .= '.';
                    if($type=="upper-alpha" || $type=="upper-latin"){
                        $blt = strtoupper($blt);
                    }
                }

                $newwidth = $this->GetStringWidth($blt);

                if($newwidth > $oldwidth){
                    $maxbul = $blt;
                }

                $oldwidth = $newwidth;
            }
        }

        for($a=0;$a < count($number_array);$a++){
            if($number_array[$a] != ''){
                $blt = $a + $increment . ".";
                if($type=="upper-roman"){
                    $blt = strtoupper($data->numberToRoman($a + $increment) . ".");
                }elseif($type=="lower-roman"){
                    $blt = strtolower($data->numberToRoman($a + $increment) . ".");
                }elseif($type=="lower-alpha" || $type=="lower-latin" || $type=="upper-alpha" || $type=="upper-latin"){
                    $b = ceil($a / 26);
                    $c = $a % 26;
                    if($b == 0){
                        $b = 1;
                    }
                    if($c === 0 && $a != 0){
                        $b++;
                    }
                    $the_char = chr((($a - (26 * ($b-1))) + $increment) + 96);
                    $blt = '';
                    for($k=0;$k<$b;$k++){
                        $blt .= $the_char;
                    }
                    $blt .= '.';
                    if($type=="upper-alpha" || $type=="upper-latin"){
                        $blt = strtoupper($blt);
                    }
                }
                $str = str_replace("Â", "", utf8_encode(html_entity_decode($number_array[$a])));
                $str = preg_replace("/<br\W*?\/>/i", "\r\n", $str);

                if($ignore_first == true && $a==0){
                    $this->MultiCellBlt($width, $this->line_height, " ", $str, $indent, $maxbul);
                }else{
                    $this->MultiCellBlt($width, $this->line_height, $blt, $str, $indent, $maxbul);
                }
                $height = 0;
                if(count($margins) == count($number_array)){
                    if($margins[$a] != ''){
                        $height += $this->convert($margins[$a]);
                        $this->MultiCellBlt($width, $height, " ", " ", $indent, $maxbul);
                    }
                }
            }
        }
        $this->Ln();
    }

    function parse_html($string, $w=0, $a='L', $extras=Array()){
        try{
            $string = str_replace("\t", "", $string);
            $string = str_replace("\n", "", $string);
            $string = str_replace("\r", "", $string);
            if($string != ''){
                $dom = new DOMDocument();
                $dom->registerNodeClass('DOMElement', 'extraElement');

                $string = str_replace("&", "&amp;", $string);

                $dom->loadHTML($string);

                //Previous Split
                $previous_split = isset($extras['previous_split']) ? $extras['previous_split'] : false;
                //Indent
                $indent = isset($extras['indent']) ? $extras['indent'] : 0;
                //Start
                $start = isset($extras['start']) ? $extras['start'] : 0;
                //Start
                $end = isset($extras['end']) ? $extras['end'] : 0;

                $nodes = $dom->childNodes;
                $nodes = $nodes->item(1)->childNodes->item(0)->childNodes;
                for($i=0;$i<$nodes->length;$i++) {

                    $item = $nodes->item($i);
                    $type = $item->tagName;
                    $value = $item->innerHTML;

                    if(strtoupper($type) == "P"){
                        $text_align = $a;
                        $font_size = $this->font_size;
                        foreach($item->attributes as $attr) {
                            if(strtoupper($attr->nodeName) == 'STYLE'){
                                $attr_value = $attr->nodeValue;
                                $style_array = explode(';',$attr_value);
                                for($k=0;$k<count($style_array);$k++){
                                    $styles = explode(':',$style_array[$k]);
                                    if(strtolower($styles[0]) == 'text-align'){
                                        $text_align = strtoupper(substr($styles[1], 0, 1));
                                    }
                                    if(strtolower($styles[0]) == 'font-size'){
                                        $font_size = strtoupper(substr($styles[1], 0, 1));
                                        if(strpos($font_size, 'px') !== false){
                                            $font_size = str_replace("PX", "", $font_size);
                                        }elseif(strpos($font_size, 'em') !== false){
                                            $font_size = str_replace("EM", "", $font_size) * $this->font_size;
                                        }
                                    }
                                }
                            }
                        }

                        $value = preg_replace('/<a.*?>/i', '', $value);
                        $value = preg_replace('/<\/a>/i', '', $value);

                        $v_array = preg_split("/<\s*[Bb][Rr]\s*\/*>/", $value);
                        foreach($v_array as $value){
                            //if($value != ''){
                            $x = $this->GetX();
                            $this->add_para(utf8_decode($value), false, $w, $text_align);
                            $this->SetXY($x, $this->GetY());
                            //}
                        }
                    }elseif(strtoupper($type) == "UL"){
                        $blt = 'disc';
                        $margins = array();

                        foreach($item->attributes as $attr) {
                            if(strtoupper($attr->nodeName) == 'STYLE'){
                                $value = $attr->nodeValue;
                                $style_array = explode(';',$value);
                                for($k=0;$k<count($style_array);$k++){
                                    $styles = explode(':',$style_array[$k]);
                                    if(strtolower($styles[0]) == 'list-style-type'){
                                        $blt = $styles[1];
                                    }
                                }
                            }
                        }
                        $children = $item->childNodes;
                        $child_array = array();
                        for($j=0;$j<$children->length;$j++) {
                            $child = $children->item($j);
                            $tag = $child->tagName;
                            if(strtoupper($tag) == "LI"){
                                $margin = '';
                                foreach($child->attributes as $attr) {
                                    if(strtoupper($attr->nodeName) == 'STYLE'){
                                        $value = $attr->nodeValue;
                                        $style_array = explode(';',$value);
                                        for($k=0;$k<count($style_array);$k++){
                                            $styles = explode(':',$style_array[$k]);
                                            if(strtolower($styles[0]) == 'margin-bottom'){
                                                $margin = str_replace('px', '', $styles[1]);
                                            }
                                        }
                                    }
                                }
                                array_push($child_array, utf8_decode(html_entity_decode($child->innerHTML)));
                                array_push($margins, $margin);
                            }
                        }
                        if($blt == 'disc'){
                            $symbol = "•";
                        }elseif($blt == 'circle'){
                            $symbol = "◦";
                        }elseif($blt == 'square'){
                            $symbol = "▪";
                        }
                        $x = $this->GetX();
                        $this->add_bullets($child_array, $symbol, $indent, $w, $previous_split, $margins);
                        $this->SetXY($x, $this->GetY());
                    }elseif(strtoupper($type) == "OL"){
                        $blt = 'decimal';
                        $margins = array();

                        foreach($item->attributes as $attr) {
                            if(strtoupper($attr->nodeName) == 'STYLE'){
                                $value = $attr->nodeValue;
                                $style_array = explode(';',$value);
                                for($k=0;$k<count($style_array);$k++){
                                    $styles = explode(':',$style_array[$k]);
                                    if(strtolower($styles[0]) == 'list-style-type'){
                                        $blt = $styles[1];
                                    }
                                }
                            }
                        }
                        $children = $item->childNodes;
                        $child_array = array();
                        for($j=0;$j<$children->length;$j++) {
                            $child = $children->item($j);
                            $tag = $child->tagName;
                            if(strtoupper($tag) == "LI"){
                                $margin = '';
                                foreach($child->attributes as $attr) {
                                    if(strtoupper($attr->nodeName) == 'STYLE'){
                                        $value = $attr->nodeValue;
                                        $style_array = explode(';',$value);
                                        for($k=0;$k<count($style_array);$k++){
                                            $styles = explode(':',$style_array[$k]);
                                            if(strtolower($styles[0]) == 'margin-bottom'){
                                                $margin = str_replace('px', '', $styles[1]);
                                            }
                                        }
                                    }
                                }
                                array_push($child_array, utf8_decode(html_entity_decode($child->innerHTML)));
                                array_push($margins, $margin);
                            }
                        }
                        $x = $this->GetX();
                        $this->add_numbering($child_array, $blt, $start, $indent, $w, $previous_split, $end, $margins);
                        $this->SetXY($x, $this->GetY());
                    }elseif(strtoupper($type) == "TABLE"){
                        $table_width = 0;
                        $table_padding_size = 0;
                        $table_border_size = 0;
                        foreach($item->attributes as $attr) {
                            if(strtoupper($attr->nodeName) == 'STYLE'){
                                $value = $attr->nodeValue;
                                $style_array = explode(';',$value);
                                for($k=0;$k<count($style_array);$k++){
                                    $styles = explode(':',$style_array[$k]);
                                    if(strtoupper($styles[0]) == 'WIDTH'){
                                        $table_width = str_replace('px', '', $styles[1]);
                                    }
                                }
                            }
                            if(strtoupper($attr->nodeName) == 'CELLPADDING'){
                                $value = $attr->nodeValue;
                                $table_padding_size = $value;
                            }
                            if(strtoupper($attr->nodeName) == 'BORDER'){
                                $value = $attr->nodeValue;
                                $table_border_size = $value;
                            }
                        }
                        if(strtolower($table_border_size) == "none"){
                            $table_border_size = 0;
                        }

                        $children = $item->childNodes;
                        $child_array = array();
                        for($j=0;$j<$children->length;$j++) {
                            $child = $children->item($j);
                            $tag = $child->tagName;
                            if(strtoupper($tag) == "THEAD"){
                                $rows = $child->childNodes;
                                $rows_array = array();
                                for($k=0;$k<$rows->length;$k++) {
                                    $row = $rows->item($k);
                                    $tag = $row->tagName;
                                    if(strtoupper($tag) == "TR"){
                                        $no_cells = 0;
                                        $cells = $row->childNodes;
                                        $cells_array = array();
                                        for($l=0;$l<$cells->length;$l++) {
                                            $cell = $cells->item($l);
                                            $tag = $cell->tagName;
                                            if(strtoupper($tag) == "TH"){
                                                foreach($cell->attributes as $attr) {
                                                    if(strtoupper($attr->nodeName) == 'STYLE'){
                                                        $value = $attr->nodeValue;
                                                        $style_array = explode(';',$value);
                                                        for($m=0;$m<count($style_array);$m++){
                                                            $styles = explode(':',$style_array[$m]);
                                                            if(strtoupper($styles[0]) == 'WIDTH'){
                                                                $header[$no_cells]['width'] = str_replace('px', '', $styles[1]);
                                                            }
                                                            if(strtoupper($styles[0]) == 'TEXT-ALIGN'){
                                                                $header[$no_cells]['align'] = substr(strtoupper($styles[1]), 0, 1);
                                                            }
                                                            if(strtoupper($styles[0]) == 'BORDER'){
                                                                $border = explode(' ',$styles[1]);
                                                                $header[$no_cells]['border_width'] = str_replace('px', '', $border[0]);
                                                                $header[$no_cells]['border_color'] = $border[2];
                                                            }
                                                            if(strtoupper($styles[0]) == 'BORDER-COLOR'){
                                                                $header[$no_cells]['border_color'] = $styles[1];
                                                            }
                                                            if(strtoupper($styles[0]) == 'BORDER-WIDTH'){
                                                                $header[$no_cells]['border_width'] = str_replace('px', '', $styles[1]);
                                                            }
                                                            if(strtoupper($styles[0]) == 'FONT-WEIGHT'){
                                                                if(strtoupper($styles[1]) == 'BOLD'){
                                                                    $header[$no_cells]['bold'] = 'B';
                                                                }
                                                            }
                                                            if(strtoupper($styles[0]) == 'LINE-HEIGHT'){
                                                                $header[$no_cells]['line_height'] = $styles[1];
                                                            }
                                                            if(strtoupper($styles[0]) == 'FONT-STYLE'){
                                                                if(strtoupper($styles[1]) == 'ITALIC'){
                                                                    $header[$no_cells]['italic'] = 'I';
                                                                }
                                                            }
                                                            if(strtoupper($styles[0]) == 'TEXT-DECORATION'){
                                                                if(strtoupper($styles[1]) == 'UNDERLINE'){
                                                                    $header[$no_cells]['underline'] = 'U';
                                                                }
                                                            }
                                                            if(strtoupper($styles[0]) == 'COLOR'){
                                                                $color_array = $this->hex2RGB($styles[1]);
                                                                $header[$no_cells]['font_color'] = array($color_array['red'], $color_array['green'], $color_array['blue']);
                                                            }
                                                            if(strtoupper($styles[0]) == 'BACKGROUND-COLOR'){
                                                                $color_array = $this->hex2RGB($styles[1]);
                                                                $header[$no_cells]['fill_color'] = array($color_array['red'], $color_array['green'], $color_array['blue']);
                                                            }
                                                        }
                                                    }
                                                }
                                                $header[$no_cells]['content'] = $cell->innerHTML;
                                                $no_cells++;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        $body = Array();

                        $no_rows = 0;
                        $children = $item->childNodes;
                        for($j=0;$j<$children->length;$j++) {
                            $child = $children->item($j);
                            $tag = $child->tagName;
                            if(strtoupper($tag) == "TBODY"){
                                $rows = $child->childNodes;
                                for($k=0;$k<$rows->length;$k++) {
                                    $row = $rows->item($k);
                                    $tag = $row->tagName;
                                    if(strtoupper($tag) == "TR"){
                                        $no_cells = 0;
                                        $cells = $row->childNodes;
                                        for($l=0;$l<$cells->length;$l++) {
                                            $cell = $cells->item($l);
                                            $tag = $cell->tagName;
                                            if(strtoupper($tag) == "TD"){
                                                foreach($cell->attributes as $attr) {
                                                    if(strtoupper($attr->nodeName) == 'STYLE'){
                                                        $value = $attr->nodeValue;
                                                        $style_array = explode(';',$value);
                                                        for($m=0;$m<count($style_array);$m++){
                                                            $styles = explode(':',$style_array[$m]);
                                                            if(strtoupper($styles[0]) == 'WIDTH'){
                                                                $body[$no_rows][$no_cells]['width'] = str_replace('px', '', $styles[1]);
                                                            }
                                                            if(strtoupper($styles[0]) == 'TEXT-ALIGN'){
                                                                $body[$no_rows][$no_cells]['align'] = substr(strtoupper($styles[1]), 0, 1);
                                                            }
                                                            if(strtoupper($styles[0]) == 'BORDER'){
                                                                $border = explode(' ',$styles[1]);
                                                                $body[$no_rows][$no_cells]['border_width'] = str_replace('px', '', $border[0]);
                                                                $body[$no_rows][$no_cells]['border_color'] = $border[2];
                                                            }
                                                            if(strtoupper($styles[0]) == 'BORDER-COLOR'){
                                                                $body[$no_rows][$no_cells]['border_color'] = $styles[1];
                                                            }
                                                            if(strtoupper($styles[0]) == 'LINE-HEIGHT'){
                                                                $body[$no_rows][$no_cells]['line_height'] = $styles[1];
                                                            }
                                                            if(strtoupper($styles[0]) == 'BORDER-WIDTH'){
                                                                $body[$no_rows][$no_cells]['border_width'] = str_replace('px', '', $styles[1]);
                                                            }
                                                            if(strtoupper($styles[0]) == 'FONT-WEIGHT'){
                                                                if(strtoupper($styles[1]) == 'BOLD'){
                                                                    $body[$no_rows][$no_cells]['bold'] = 'B';
                                                                }
                                                            }
                                                            if(strtoupper($styles[0]) == 'FONT-STYLE'){
                                                                if(strtoupper($styles[1]) == 'ITALIC'){
                                                                    $body[$no_rows][$no_cells]['italic'] = 'I';
                                                                }
                                                            }
                                                            if(strtoupper($styles[0]) == 'TEXT-DECORATION'){
                                                                if(strtoupper($styles[1]) == 'UNDERLINE'){
                                                                    $body[$no_rows][$no_cells]['underline'] = 'U';
                                                                }
                                                            }
                                                            if(strtoupper($styles[0]) == 'COLOR'){
                                                                $color_array = $this->hex2RGB($styles[1]);
                                                                $body[$no_rows][$no_cells]['font_color'] = array($color_array['red'], $color_array['green'], $color_array['blue']);
                                                            }
                                                            if(strtoupper($styles[0]) == 'BACKGROUND-COLOR'){
                                                                $color_array = $this->hex2RGB($styles[1]);
                                                                $body[$no_rows][$no_cells]['fill_color'] = array($color_array['red'], $color_array['green'], $color_array['blue']);
                                                            }
                                                        }
                                                    }
                                                }
                                                $body[$no_rows][$no_cells]['content'] = $cell->innerHTML;
                                                $no_cells++;
                                            }
                                        }
                                        $no_rows++;
                                    }
                                }
                            }

                            if(strtoupper($tag) == "TR"){
                                $no_cells = 0;
                                $cells = $child->childNodes;
                                for($l=0;$l<$cells->length;$l++) {
                                    $cell = $cells->item($l);
                                    $tag = $cell->tagName;
                                    if(strtoupper($tag) == "TD"){
                                        foreach($cell->attributes as $attr) {
                                            if(strtoupper($attr->nodeName) == 'STYLE'){
                                                $value = $attr->nodeValue;
                                                $style_array = explode(';',$value);
                                                for($m=0;$m<count($style_array);$m++){
                                                    $styles = explode(':',$style_array[$m]);
                                                    if(strtoupper($styles[0]) == 'WIDTH'){
                                                        $body[$no_rows][$no_cells]['width'] = str_replace('px', '', $styles[1]);
                                                    }
                                                    if(strtoupper($styles[0]) == 'TEXT-ALIGN'){
                                                        $body[$no_rows][$no_cells]['align'] = substr(strtoupper($styles[1]), 0, 1);
                                                    }
                                                    if(strtoupper($styles[0]) == 'BORDER'){
                                                        $border = explode(' ',$styles[1]);
                                                        $body[$no_rows][$no_cells]['border_width'] = str_replace('px', '', $border[0]);
                                                        $body[$no_rows][$no_cells]['border_color'] = $border[2];
                                                    }
                                                    if(strtoupper($styles[0]) == 'BORDER-COLOR'){
                                                        $body[$no_rows][$no_cells]['border_color'] = $styles[1];
                                                    }
                                                    if(strtoupper($styles[0]) == 'LINE-HEIGHT'){
                                                        $body[$no_rows][$no_cells]['line_height'] = $styles[1];
                                                    }
                                                    if(strtoupper($styles[0]) == 'BORDER-WIDTH'){
                                                        $body[$no_rows][$no_cells]['border_width'] = str_replace('px', '', $styles[1]);
                                                    }
                                                    if(strtoupper($styles[0]) == 'FONT-WEIGHT'){
                                                        if(strtoupper($styles[1]) == 'BOLD'){
                                                            $body[$no_rows][$no_cells]['bold'] = 'B';
                                                        }
                                                    }
                                                    if(strtoupper($styles[0]) == 'FONT-STYLE'){
                                                        if(strtoupper($styles[1]) == 'ITALIC'){
                                                            $body[$no_rows][$no_cells]['italic'] = 'I';
                                                        }
                                                    }
                                                    if(strtoupper($styles[0]) == 'TEXT-DECORATION'){
                                                        if(strtoupper($styles[1]) == 'UNDERLINE'){
                                                            $body[$no_rows][$no_cells]['underline'] = 'U';
                                                        }
                                                    }
                                                    if(strtoupper($styles[0]) == 'COLOR'){
                                                        $color_array = $this->hex2RGB($styles[1]);
                                                        $body[$no_rows][$no_cells]['font_color'] = array($color_array['red'], $color_array['green'], $color_array['blue']);
                                                    }
                                                    if(strtoupper($styles[0]) == 'BACKGROUND-COLOR'){
                                                        $color_array = $this->hex2RGB($styles[1]);
                                                        $body[$no_rows][$no_cells]['fill_color'] = array($color_array['red'], $color_array['green'], $color_array['blue']);
                                                    }
                                                }
                                            }
                                        }
                                        $body[$no_rows][$no_cells]['content'] = $cell->innerHTML;
                                        $no_cells++;
                                    }
                                }
                                $no_rows++;
                            }
                        }
                        $total_width = 0;
                        $widths = Array();

                        if(count($header) > 0){
                            $widths = $this->calc_col_widths($header, $body, $table_width, $table_padding_size, $table_border_size);
                            $this->SetWidths($widths);
                            $aRow = Array();
                            $aRow['border'] = array();
                            $aRow['padding_size'] = array();
                            $aRow['padding'] = array();
                            $aRow['align'] = array();
                            $aRow['style'] = array();
                            $aRow['fill'] = array();
                            $aRow['fill_color'] = array();
                            $aRow['font_color'] = array();
                            $aRow['line_height'] = '';
                            $caption = array();

                            for($k=0;$k<count($header);$k++){
                                $aRow['border'][$k] = $header[$k]['border'];
                                $aRow['border'][$k] = $table_border_size;
                                $aRow['padding_size'][$k] = $table_padding_size;
                                $aRow['padding'][$k] = 1;

                                $align = 'L';
                                if(isset($header[$k]['align'])){
                                    $align = $header[$k]['align'];
                                }
                                $aRow['align'][$k] = $align;

                                $style = '';
                                if(isset($header[$k]['bold'])){
                                    $style .= $header[$k]['bold'];
                                }
                                if(isset($header[$k]['italic'])){
                                    $style .= $header[$k]['italic'];
                                }
                                if(isset($header[$k]['underline'])){
                                    $style .= $header[$k]['underline'];
                                }
                                $aRow['style'][$k] = $style;

                                $fill = '0';
                                $fill_color = array();
                                if(isset($header[$k]['fill_color'])){
                                    $fill = '1';
                                    $fill_color = $header[$k]['fill_color'];
                                }
                                $aRow['fill'][$k] = $fill;
                                $aRow['fill_color'][$k] = $fill_color;

                                $font_color = array();
                                if(isset($header[$k]['font_color'])){
                                    $font_color = $header[$k]['font_color'];
                                }
                                $aRow['font_color'][$k] = $font_color;

                                $caption[$k]['type'] = "html";
                                $caption[$k]['value'] = $header[$k]['content'];
                            }
                            $this->add_header($caption, $aRow);
                        }

                        for($j=0;$j<count($body);$j++){
                            $widths = $this->calc_col_widths($header, $body, $table_width, $table_padding_size, $table_border_size);
                            $this->SetWidths($widths);
                            $aRow = Array();
                            $aRow['border'] = array();
                            $aRow['padding_size'] = array();
                            $aRow['padding'] = array();
                            $aRow['align'] = array();
                            $aRow['style'] = array();
                            $aRow['fill'] = array();
                            $aRow['fill_color'] = array();
                            $aRow['font_color'] = array();
                            $aRow['line_height'] = '';
                            $caption = array();

                            for($k=0;$k<count($body[$j]);$k++){
                                $aRow['border'][$k] = $body[$j][$k]['border'];
                                $aRow['border'][$k] = $table_border_size;
                                $aRow['padding_size'][$k] = $table_padding_size;
                                $aRow['padding'][$k] = 1;

                                $align = 'L';
                                if(isset($body[$j][$k]['align'])){
                                    $align = $body[$j][$k]['align'];
                                }
                                $aRow['align'][$k] = $align;

                                $style = '';
                                if(isset($body[$j][$k]['bold'])){
                                    $style .= $body[$j][$k]['bold'];
                                }
                                if(isset($body[$j][$k]['italic'])){
                                    $style .= $body[$j][$k]['italic'];
                                }
                                if(isset($body[$j][$k]['underline'])){
                                    $style .= $body[$j][$k]['underline'];
                                }
                                $aRow['style'][$k] = $style;

                                $fill = '0';
                                $fill_color = array();
                                if(isset($body[$j][$k]['fill_color'])){
                                    $fill = '1';
                                    $fill_color = $body[$j][$k]['fill_color'];
                                }
                                $aRow['fill'][$k] = $fill;
                                $aRow['fill_color'][$k] = $fill_color;

                                $font_color = array();
                                if(isset($body[$j][$k]['font_color'])){
                                    $font_color = $body[$j][$k]['font_color'];
                                }
                                $aRow['font_color'][$k] = $font_color;

                                $caption[$k]['type'] = "html";
                                $caption[$k]['value'] = $body[$j][$k]['content'];
                            }
                            $this->add_row($caption, $aRow, $extras);
                        }
                    }else{
                        $this->parse_html('<p>'.$string.'</p>', $w, $a, $extras);
                    }
                }
            }
        }catch(Exception $e){

        }
    }

    function calc_col_widths($header, $body, $table_width = 0, $table_padding_size = 0, $table_border_size = 0){

        $widths = array();

        $j = 0;

        $table_border_size = 1;

        $total_width = 0;
        if($table_width == 0){
            $table_width = $this->convert($this->w-$this->rMargin-$this->lMargin, 'mm', 'pixels');
        }

        $set_widths = array();

        for($k=0;$k<count($header);$k++){
            if(isset($header[$k]['width'])){
                array_push($set_widths, $header[$k]['width']);
            }else{
                array_push($set_widths, 0);
            }
        }
        if(count($widths) == 0){
            for($k=0;$k<count($body[$j]);$k++){
                if(isset($body[$j][$k]['width'])){
                    array_push($set_widths, $body[$j][$k]['width']);
                }else{
                    array_push($set_widths, 0);
                }
            }
        }

        $padding = $this->convert($table_padding_size, 'mm', 'pixels');

        $num_cols = count($set_widths);
        $num_no_width = 0;
        $width_used = 0;

        foreach($set_widths as $width){
            if($width == 0){
                $num_no_width++;
            }else{
                $width_used += ($width + ($padding*2));
            }
        }

        if($num_no_width > 0){
            $col_width = $this->convert((($table_width - $width_used) / $num_no_width) - $padding*2);
        }

        for($k=0;$k<count($header);$k++){
            if(isset($header[$k]['width'])){
                array_push($widths, $this->convert($header[$k]['width']));
            }else{
                array_push($widths, $col_width);
            }
        }
        if(count($widths) == 0){
            for($k=0;$k<count($body[$j]);$k++){
                if(isset($body[$j][$k]['width'])){
                    array_push($widths, $this->convert($body[$j][$k]['width']));
                }else{
                    array_push($widths, $col_width);
                }
            }
        }

        return $widths;
    }

    function split_by_tags($string){
        $dom = new DOMDocument();
        $dom->registerNodeClass('DOMElement', 'extraElement');
        @$dom->loadHTML('<p>'.$string.'</p>');
        $result_array = array();
        $data_array = array();

        $nodes = $dom->childNodes;
        $nodes = $nodes->item(1)->childNodes->item(0)->childNodes;
        for($i=0;$i<$nodes->length;$i++) {

            $item = $nodes->item($i);
            $type = $item->tagName;
            $value = $item->innerHTML;

            if(strtoupper($type) == "P"){

                $children = $item->childNodes;
                for($j=0;$j<$children->length;$j++) {
                    $child = $children->item($j);
                    $tag = $child->tagName;
                    if($tag != ''){
                        array_push($data_array, array($tag, $child->innerHTML));
                    }
                }
            }
        }
        $temp_string = $string;
        for($i=0;$i<count($data_array);$i++){
            $pos = strpos($temp_string, "<".$data_array[$i][0].">");
            if($pos > 0){
                array_push($result_array, array(substr($temp_string, 0, $pos), ''));
            }
            $temp_string =  substr($temp_string, $pos + strlen("<".$data_array[$i][0].">"));
            $pos = strpos($temp_string, "</".$data_array[$i][0].">");
            array_push($result_array, array(substr($temp_string, 0, $pos),$data_array[$i][0]));
            $temp_string =  substr($temp_string, ($pos + strlen("</".$data_array[$i][0].">")));
        }
        if(count($data_array) == 0){
            array_push($result_array, array($string,''));
        }else{
            array_push($result_array, array($temp_string,''));
        }
        return $result_array;
    }

    function hex2RGB($hexStr, $returnAsString = false, $seperator = ',') {
        $hexStr = preg_replace("/[^0-9A-Fa-f]/", '', $hexStr); // Gets a proper hex string
        $rgbArray = array();
        if (strlen($hexStr) == 6) { //If a proper hex code, convert using bitwise operation. No overhead... faster
            $colorVal = hexdec($hexStr);
            $rgbArray['red'] = 0xFF & ($colorVal >> 0x10);
            $rgbArray['green'] = 0xFF & ($colorVal >> 0x8);
            $rgbArray['blue'] = 0xFF & $colorVal;
        } elseif (strlen($hexStr) == 3) { //if shorthand notation, need some string manipulations
            $rgbArray['red'] = hexdec(str_repeat(substr($hexStr, 0, 1), 2));
            $rgbArray['green'] = hexdec(str_repeat(substr($hexStr, 1, 1), 2));
            $rgbArray['blue'] = hexdec(str_repeat(substr($hexStr, 2, 1), 2));
        } else {
            return false; //Invalid hex color code
        }
        return $returnAsString ? implode($seperator, $rgbArray) : $rgbArray; // returns the rgb string or the associative array
    }

    function convert($no, $from='pixels', $to='mm'){
        $output = $no;

        if($from == 'pixels' && $to == 'mm'){
            $output = (($no * 25.4) / 72);
        }elseif($from == 'mm' && $to == 'pixels'){
            $output = (($no * 72) / 25.4);
        }

        return $output;
    }

    function AddPage($orientation='', $format='') {
        parent::AddPage($orientation, $format);
        if($this->_numbering)
            $this->_numPageNum++;
    }

    function startPageNums() {
        $this->_numbering=true;
        $this->_numberingFooter=true;
    }

    function stopPageNums() {
        $this->_numbering=false;
    }

    function numPageNo() {
        return $this->_numPageNum;
    }

    function TOC_Entry($txt, $level=0) {
        $this->_toc[]=array('t'=>$txt, 'l'=>$level, 'p'=>$this->numPageNo());
    }

    function insertTOC($location=1, $labelSize=20, $entrySize=10, $label='Table of Contents') {
        //make toc at end
        $this->add_page('P');
        $this->stopPageNums();
        $tocstart=$this->page;

        $tocfont = $this->font_name;

        $this->heading($label, 20, 'L', true, false);

        $max_str_size = 0;
        foreach($this->_toc as $t) {
            $strsize=$this->GetStringWidth($t['p'])+2;
            if($strsize > $max_str_size){
                $max_str_size = $strsize;
            }
        }

        foreach($this->_toc as $t) {
            //Offset
            $level=$t['l'];
            if($level>0)
                $this->Cell($level*8);
            $weight='';
            if($level==0)
                $weight='B';
            $str=$t['t'];
            $this->SetFont($tocfont, $weight, $entrySize);
            $strsize=$this->GetStringWidth($str);
            $this->Cell($strsize+2, $this->FontSize+2, $str);

            //Filling dots
            $PageCellSize=$max_str_size+4;
            $w=$this->w-($this->lMargin+$this->rMargin);
            $w=$w-($PageCellSize+(($level*8)+($strsize+2)));

            $this->SetFont($tocfont, '', $entrySize);

            $nb=floor($w/$this->GetStringWidth('.'));
            $dots=str_repeat('.', $nb);
            $this->Cell(floor($w), $this->FontSize+2, $dots, 0, 0, 'R');

            //Page number
            $y = $this->y;
            $this->MultiCell($PageCellSize, $this->FontSize+2, $t['p'], 0, 'R');
            $this->SetY($y + $this->FontSize+2);
        }

        //grab it and move to selected location
        $n=$this->page;
        $n_toc = $n - $tocstart + 1;
        $last = array();

        $pages = $this->pages;
        $sizes = $this->PageSizes;

        //store toc pages
        for($i = $tocstart;$i <= $n;$i++)
            $last['pages'][]=$this->pages[$i];
        $last['sizes'][]=$this->PageSizes[$i];

        //move pages
        for($i=$tocstart - 1;$i>=$location-1;$i--){
            $this->pages[$i+$n_toc] = $pages[$i];
            $this->PageSizes[$i+$n_toc] = $sizes[$i];
        }

        //Put toc pages at insert point
        for($i = 0;$i < $n_toc;$i++){
            $this->pages[$location + $i]=$last['pages'][$i];
            if(isset($last['sizes'][$i])){
                $this->PageSizes[$location + $i]=$last['sizes'][$i];
            }else{
                unset($this->PageSizes[$location + $i]);
            }
        }
    }

    /**
     * Add Figure Table
     *
     * @param	Array		$header			Headers/Widths
     * @param	Array		$data			Row/Column Data
     * @param	string		$title			Figure Title
     * @param	string		$pre_text		Text before figure, displayed as 'Figure x.y {Your text}'
     */
    function add_figure_table($header, $data, $title="", $pre_text="", $total=array(), $title_pos="bottom"){

        // Calculate Figure No.
        $this->figure_table_no++;

        $figure_no = $this->figure_table_no;

        if($pre_text != ""){
            if( $title_pos == "top" ){
                $this->add_para("The below table " . $pre_text);
            }else{
                $this->add_para("Table " .$figure_no . " " . $pre_text);
            }
        }

        if($title != "" && $title_pos = "top"){
            $this->subheading(ucwords($title), 11, 'L', false, false);
        }

        $widths = array();
        $headers = array();
        $types = array();

        foreach($header as $item){
            $widths[] = $item['width'];
            $headers[] = $item['text'];
            $types[] = $item['type'];
        }

        $this->SetWidths($widths);

        unset($widths);

        $aRow = Array();
        $aRow['border'] = array();
        $aRow['padding_size'] = array();
        $aRow['padding'] = array();
        $aRow['align'] = array();
        $aRow['style'] = array();
        $aRow['fill'] = array();
        $aRow['fill_color'] = array();
        $aRow['font_color'] = array();

        $caption = array();

        foreach($headers as $header){
            $aRow['border'][] = 1;
            $aRow['padding_size'][] = 1;
            $aRow['padding'][] = '1';
            $aRow['align'][] = 'L';
            $aRow['style'][] = 'B';
            $aRow['fill'][] = 1;
            $aRow['fill_color'][] = $this->colours['light_blue_header'];
            $aRow['gradient_color'][] = $this->colours['rmg_blue1'];
            $aRow['font_color'][] = $this->colours['white'];

            $caption[] = $header;
        }

        $this->add_header($caption, $aRow);
        unset($caption);

        $bRow = $aRow;
        $bRow['style'] = array();
        $bRow['fill_color'] = array();
        $bRow['gradient_color'] = array();
        $bRow['padding'] = array();

        unset($bRow['font_color']);
        unset($bRow['line_height']);

        $i = 0;
        foreach($headers as $header){
            if($i == 0){
                $bRow['style'][] = 'B';
            }else{
                $bRow['style'][] = '';
            }
            $bRow['padding'][] = 'LR';
            $i++;
        }

        $totals = array();

        $i = 0;
        foreach($data as $row){

            $caption = array();

            $j = 0;
            foreach($row as $item){
                if($types[$j] == 'percentage'){
                    if(is_numeric($item)){
                        $caption[] = $item . '%';
                    }else{
                        $caption[] = $item;
                    }
                }else{
                    $caption[] = $item;
                }

                if(!isset($totals[$j])){
                    if($types[$j] != 'text'){
                        $totals[$j] = $item;
                    }else{
                        $totals[$j] = "Total";
                    }
                }else{
                    if($types[$j] != 'text'){
                        $totals[$j] += $item;
                    }
                }

                if($i % 2){
                    $bRow['fill_color'][$j] = $this->colours['white'];
                }else{
                    $bRow['fill_color'][$j] = $this->colours['light_blue'];
                }

                $j++;
            }

            $this->add_row($caption, $bRow, true);
            unset($caption);
            $i ++;
        }
        unset($bRow);
        $caption = array();

        if(!empty($total)){
            $caption = $total;
        }else{

            if(is_array($totals) && !empty($totals)){
                $i = 0;
                foreach($totals as $total){
                    if($types[$i] == 'percentage'){
                        $caption[] = number_format(($total/count($data)), 1) . '%';
                    }else{
                        if(is_numeric($total) && $total == 0){
                            $total = "0";
                        }

                        $caption[] = $total;
                    }
                    $i++;
                }
            }else{

                if(is_numeric($total) && $totals == 0){
                    $totals = "0";
                }

                $caption = $totals;
            }
        }

        unset($total, $data);

        $this->add_row($caption, $aRow, true);

        unset($caption, $aRow);

        if($title != "" && $title_pos == "bottom"){
            $title = "Table " .$figure_no . " - " . $title;
            $this->subheading($title, 11, 'L', true, false);
        }else{
            $this->ln();
        }
    }

    /**
     *
     * @param	string		$title			TOC Title
     * @param	array		$format			Array of options
     * @param	int			$x				X position
     * @param	int			$y				Y postion
     */
    function graph($title, $format, $x=null, $y=null, $pre_text="", $title_pos="bottom"){
        Global $UTILS_URL_BASE;

        // Calculate Figure No.
        $this->figure_no++;

        $figure_no = $this->figure_no;

        $nl = 0;
        if($pre_text != ""){
            if( $title_pos == "top" ){
                $nl = $this->add_para("The below figure " . $pre_text);
            }else{
                $nl = $this->add_para("Figure " .$figure_no . " " . $pre_text);
            }
        }

        if($title != "" && $title_pos = "top"){
            $this->subheading(ucwords($title), 11, 'L', false, false);
            $y = $this->y;
        }

        if($y != null && $nl != 0){
            $y += ($this->line_height * $nl);
            $y = $this->y;
        }

        //print($figure_no . ' - ' . $UTILS_URL_BASE ."show_graph.php?".http_build_query($format)."#.png\r\n");

        $file_g = new graph($format);
        $file = $file_g->output();

        $info = $this->Image_Info($file, $x, $y);
        // Put image at 300 dpi
        $w = -300;
        $h = -300;

        // Automatic width and height calculation
        $w = -(($info[0]*72)/$w)/$this->k;
        $h = -(($info[1]*72)/$h)/$this->k;

        $this->GDImage($file, $x, $y, $w, $h);

        $this->SetXY($this->x, $y + $h);

        //print($y."\r\n");

        if($title != "" && $title_pos == "bottom"){
            $title = "Figure " .$figure_no . " - " . $title;
            $this->subheading($title, 11, 'L', true, false);
        }else{
            $this->ln();
        }
    }
    function setBackgroundPDF($source){

        $pagecount = $this->setSourceFile($source);


        if($pagecount !== false){
            if($pagecount==1){

                $was_true = false;

                if($this->include_header == true){
                    $was_true = true;
                }

                $_w =  $this->w;
                $_h =  $this->h;

                $this->include_header = false;

                $tplidx = $this->importPage(1, '/MediaBox');

                $size = $this->getTemplateSize($tplidx, 0, 0);

                if($size['w'] < $size['h']){
                    $this->DefOrientation = 'P';
                }else{
                    $this->DefOrientation = 'L';
                }

                //$this->add_page($this->DefOrientation);
                $this->useTemplate($tplidx, null, null, 0, 0, true);

                $this->w=$_w;
                $this->h=$_h;

                if($this->w < $this->h){
                    $this->DefOrientation = 'P';
                }else{
                    $this->DefOrientation = 'L';
                }

                $this->CurOrientation = $this->DefOrientation;

                if($was_true == true){
                    $this->include_header = true;
                }

                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
}
?>