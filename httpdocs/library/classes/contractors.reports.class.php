<?
ini_set("max_execution_time","120");
require_once($UTILS_CLASS_PATH."security.class.php");
require_once($UTILS_CLASS_PATH."data.class.php");


class contractor_report {
	
	function contractor_report(){
		
	}
	
	function purchase_order($contractor_qube_ref, $request){
		$data = new data;
		
		$sql = "SELECT 
		p.*, 
		r.rmc_name,
		j.cpm_po_job_complete,
		(
			SELECT COUNT(*)
			FROM cpm_po_job
			WHERE cpm_po_job_po_id = p.cpm_po_id
			AND cpm_po_job_complete = 'True'
		) as no_complete,
		(
			SELECT COUNT(*)
			FROM cpm_po_job
			WHERE cpm_po_job_po_id = p.cpm_po_id
			AND cpm_po_job_complete = 'False'
		) as no_incomplete
		FROM cpm_po p
		INNER JOIN cpm_lookup_rmcs l ON l.rmc_ref = p.cpm_po_rmc_id 
		INNER JOIN cpm_rmcs r ON r.rmc_num = l.rmc_lookup 
		INNER JOIN cpm_po_job j ON j.cpm_po_job_po_id = p.cpm_po_id 
		WHERE 1=1";
		
		if($contractor_qube_ref <> ''){
			$sql .= "
			AND p.cpm_po_contractors_ref = '".$contractor_qube_ref."'";
		} 
				
		if($request['date_from'] != ''){
			$sql .= "
			AND p.cpm_po_date_raised >= '".$data->date_to_ymd($request['date_from'])."' ";
		}
		
		if($request['date_to'] != ''){
			$sql .= "
			AND p.cpm_po_date_raised <= '".$data->date_to_ymd($request['date_to'])."' ";
		}
		
		if ($request['contract'] != 2){
			$sql .= "
			AND p.cpm_po_is_contract = '".$request['contract']."' ";
		}
		
		if($request['outcome'] == '0'){
			$sql .= "
			AND j.cpm_po_job_reason_id <> '' ";	
		}elseif($request['outcome'] == '1'){
			$sql .= "
			AND j.cpm_po_job_reason_id = '' ";	
		}
		
		if($request['is_completed'] == 'True'){
			$sql .= "
			AND j.cpm_po_job_complete = 'True' ";	
		}elseif($request['is_completed'] == 'False'){
			$sql .= "
			AND j.cpm_po_job_complete = 'False' ";	
		}
		
		$sql .= "
		AND (UCASE(CONVERT(p.cpm_po_description using utf8)) LIKE '%".strtoupper($request['search_term'])."%'
		OR p.cpm_po_number LIKE '%".$request['search_term']."%'
		OR p.cpm_po_rmc_id LIKE '%".$request['search_term']."%'
		OR r.rmc_name LIKE '%".$request['search_term']."%'
		OR j.cpm_po_job_no LIKE '%".$request['search_term']."%')";
		
		$result = @mysql_query($sql);
		
		return $result;		
	}
	
	function job($po_no, $request){
		$data = new data;
				
		$sql_job = "SELECT j.*,
		(
			SELECT c.cpm_contractors_name
			FROM cpm_contractors c
			INNER JOIN cpm_po p ON p.cpm_po_contractors_ref = c.cpm_contractors_qube_id
			WHERE p.cpm_po_id = j.cpm_po_job_po_id
		) as contractor_name,
		(
			SELECT p.cpm_po_number
			FROM cpm_po p
			WHERE p.cpm_po_id = j.cpm_po_job_po_id
		) as po_num,
		(SELECT cpm_contractors_user_name FROM cpm_contractors_user WHERE cpm_contractors_user_ref = j.cpm_po_job_user_ref) as user_name,
		(SELECT cpm_po_reason_text FROM cpm_po_reason WHERE cpm_po_reason_id = j.cpm_po_job_reason_id) as the_reason
		FROM cpm_po_job j
		WHERE ";

		if($request['is_completed'] != ''){
			$sql_job .= "
			j.cpm_po_job_complete = '".$request['is_completed']."' AND ";
		}
		
		$sql_job .= "
		j.cpm_po_job_po_id = '".$po_no."'
		AND j.cpm_po_job_no LIKE '%".$request['search_term']."%'";
		
		$result_job = @mysql_query($sql_job);
		$num_result_jobs = @mysql_num_rows($result_job);
		if($num_result_jobs == 0){
			$sql_job = "
			SELECT j.*,
			(SELECT cpm_contractors_user_name FROM cpm_contractors_user WHERE cpm_contractors_user_ref = j.cpm_po_job_user_ref) as user_name
			FROM cpm_po_job j
			WHERE ";

			if($request['is_completed'] != ''){
				$sql_job .= "
				j.cpm_po_job_complete = '".$request['is_completed']."' AND ";
			}
			
			if($request['date_from'] != ''){
				$sql_job .= "
				j.cpm_po_job_ts >= '".$data->date_to_ymd($request['date_from'])."' AND ";
			}
			
			if($request['date_to'] != ''){
				$sql_job .= "
				j.cpm_po_job_ts <= '".$data->date_to_ymd($request['date_to'])."' AND ";
			}
			
			$sql_job .= "
			j.cpm_po_job_po_id = '".$po_no."'";
		}
		
		$result_job = @mysql_query($sql_job);
		
		
		return $result_job;
	}
	
	function user_audit($user_ref){
		$sql = "
		SELECT t.*, u.cpm_contractors_user_name 
		FROM cpm_contractors_user_trail t 
		INNER JOIN cpm_contractors_user u
		ON t.cpm_contractors_user_trail_user_ref = u.cpm_contractors_user_ref
		WHERE
		u.cpm_contractors_user_ref = '".$user_ref."'
		ORDER BY t.cpm_contractors_user_trail_login DESC";
		
		$output = '';
		
		$result = @mysql_query($sql);
		$num_results = @mysql_num_rows($result);
		$per_page = 12;
		$pages = ceil($num_results/$per_page);
		$page_count = 1;
		$i = 0;
		if($num_results > 0){
			
			$output .= '<div class="header_row" style="width:470px">';
			$output .= '<div class="form_label border" style="width:150px;">Name</div>';
			$output .= '<div class="form_label border" style="width:170px;">Login Date/Time</div>';
			$output .= '<div class="form_label" style="width:110px;">Login Location</div>';
			$output .= '</div>';
			$output .= '<ul class="paging" id="audit_pages"><li id="page-1">';
			
			while($row = @mysql_fetch_array($result)){
				
				$i += 1; 
				
				$output .= '<div class="form_row" style="width:470px">';
				$output .= '<div class="form_label border" style="width:150px;">'.$row['cpm_contractors_user_name'].'</div>';
				$output .= '<div class="form_label border" style="width:170px;">'.$this->sort_date_time($row['cpm_contractors_user_trail_login']).'</div>';
				$output .= '<div class="form_label" style="width:110px;">'.$row['cpm_contractors_user_trail_ip'].'</div>';
				$output .= '</div>';	
				
				if($i == $per_page){
					$i = 0;
					$page_count += 1;
					$output .= '</li><li id="page-'.$page_count.'">';
				}
			}
			
			$output .= '</li></ul>';
			$output .= '<script language="javascript">$(document).ready(function(){ $("#audit_pages").paging({pageName : "pagination"}); });</script>';
		}
		
		$result_array['results'] = $output;
		
		$output = '<ul class="pagination" id="pagination">';
		
		for($i=1; $i<=$pages; $i++){
			$output .= '<li id="'.$i.'"';
			if($i == 1){
				$output .= ' class="active"';
			}
			$output .= ' onclick="javascript:$('."'#audit_pages'".').setPage('."'".$i."'".');">'.$i.'</li>';
		}
		$output .= '</ul>';
		
		$result_array['paging'] = $output;
		
		return $result_array;
	}
	
	function audit_trail(){
		$sql = "
		SELECT t.*, 
		u.cpm_contractors_user_name,
		c.cpm_contractors_name
		FROM cpm_contractors_user_trail t 
		INNER JOIN cpm_contractors_user u ON t.cpm_contractors_user_trail_user_ref = u.cpm_contractors_user_ref
		INNER JOIN cpm_contractors c ON c.cpm_contractors_qube_id = u.cpm_contractors_user_qube_ref
		GROUP BY u.cpm_contractors_user_ref
		ORDER BY t.cpm_contractors_user_trail_login DESC";
		
		$output = '';
		
		$result = @mysql_query($sql);
		
		return $result;
	}
		
	function user_accounts($contractor_qube_ref, $request){
		$sql = "
		SELECT u.*, 
		IFNULL((SELECT cpm_contractors_user_trail_login 
			FROM cpm_contractors_user_trail 
			WHERE 
			cpm_contractors_user_trail_user_ref = u.cpm_contractors_user_ref 
			ORDER BY cpm_contractors_user_trail_id DESC 
			LIMIT 1
		), 'Never Logged In') as last_login, 
		(SELECT COUNT(cpm_contractors_user_trail_login) 
			FROM cpm_contractors_user_trail 
			WHERE 
			cpm_contractors_user_trail_user_ref = u.cpm_contractors_user_ref 
			ORDER BY cpm_contractors_user_trail_id DESC 
		) as login_count
		FROM cpm_contractors_user  u
		WHERE
		(
			u.cpm_contractors_user_ref LIKE '%".$request['search_term']."%'
			OR u.cpm_contractors_user_name LIKE '%".$request['search_term']."%'
			OR u.cpm_contractors_user_email LIKE '%".$request['search_term']."%'
		)
		AND u.cpm_contractors_user_qube_ref = '".$contractor_qube_ref."'
		ORDER BY u.cpm_contractors_user_ref ASC";
		
		$output = '';
		
		$result = @mysql_query($sql);
		$num_results = @mysql_num_rows($result);
		if($num_results > 0){
			
			$output .= '<div class="header_row">';
			$output .= '<div class="form_label border" style="width:95px;">Username</div>';
			$output .= '<div class="form_label border" style="width:115px;">Name</div>';
			$output .= '<div class="form_label border" style="width:180px;">Email Address</div>';
			$output .= '<div class="form_label border" style="width:58px;">Disabled</div>';
			$output .= '<div class="form_label border" style="width:78px;">No. Logins</div>';
			$output .= '<div class="form_label" style="width:130px;">Last Login</div>';
			$output .= '</div>';
			
			while($row = @mysql_fetch_array($result)){
				
				$output .= '<a class="form_row" href="javascript:do_audit('."'".$row['cpm_contractors_user_ref']."'".');">';
				$output .= '<div class="form_label border" style="width:95px;">'.$row['cpm_contractors_user_ref'].'</div>';
				$output .= '<div class="form_label border" style="width:115px;">'.$row['cpm_contractors_user_name'].'</div>';
				$output .= '<div class="form_label border" style="width:180px;">'.$row['cpm_contractors_user_email'].'</div>';
				$output .= '<div class="form_label border" style="width:58px;text-align:center;padding-top:6px;padding-bottom:6px;"><img src="/images/contractors/'.$row['cpm_contractors_user_disabled'].'.png" /></div>';
				$output .= '<div class="form_label border" style="width:78px;">'.$row['login_count'].'</div>';
				$output .= '<div class="form_label" style="width:130px;">'.$this->sort_date_time($row['last_login']).'</div>';
				$output .= '</a>';	
			}
		}
		
		return $output;
	}
	
	function sort_date_time($datetime, $format="d/m/Y H:i:s"){
		if( $datetime != ""){
			if (strpos($datetime, "-") !== false){
				$array = explode("-", $datetime);
				
				$year = $array[0];
				$month = $array[1];
				$day = $array[2];
				$hour = $array[3];
				$min = $array[4];
				$sec = $array[5];
				
				$datetime = new DateTime();
				$datetime->setDate($year, $month, $day);
				$datetime->setTime($hour, $min, $sec); 
				
				return $datetime->format($format);
			}else{
				return $datetime;
			}
		}else{
			return "&nbsp;";	
		}
	}
	
	function is_blank($str, $replace="&nbsp;"){
		if($str == ""){
			return $replace;
		}else{
			return $str;	
		}
	}

	function http_parse_query( $array = NULL, $convention = '%s' ){
			
		if( count( $array ) == 0 ){
			return '';
		} else {
			if( function_exists( 'http_build_query' ) ){
				$query = http_build_query( $array );
			} else {
				
				$query = '';
					
				foreach( $array as $key => $value ){
					if( is_array( $value ) ){
						$new_convention = sprintf( $convention, $key ) . '[%s]';
						$query .= http_parse_query( $value, $new_convention );
							
					} else {
						$key = urlencode( $key );
						$value = urlencode( $value );
							
						$query .= sprintf( $convention, $key ) . "=$value&";
					}
				}
			}
			
			return $query;
		}	
	} 
}

?>
