<?php

require_once('PHPMailer/PHPMailerAutoload.php' );

class emailer extends PHPMailer {

	/**
	 * Create a message and send it.
	 * Uses the sending method specified by $Mailer.
	 * @throws phpmailerException
	 * @return boolean false on error - See the ErrorInfo property for details of the error.
	 */
	public function send()
	{
		$sent = parent::send();

		$sql = "
		INSERT INTO cpm_mailer SET
		mail_type = 'Paperless',
		mail_to = '" . $this->multiImplode( $this->getToAddresses() ) . "',
		mail_from = '" . $this->From . "',
		mail_subject = '" . $this->Subject . "',
		mail_message = '" . $this->Body . "',
		mail_html = '" . $this->Body . "',
		mail_headers = '',
		mail_images = '',
		mail_attachment = '" . $this->attachmentArray( $this->getAttachments() ) . "',
		mail_del_attach = 'N',
		mail_sent = '" . $sent . "'";
		@mysql_query($sql);

		return $sent;
	}

	/**
	 * @param    array   $input
	 * @return    string
	 */
	function multiImplode( $input )
	{
		$r = '';

		foreach ( $input as $email ) {
			$r .= $email[1] . ' ['.$email[0] .'], ';
		}

		if( $r != '' ) {
			$r = substr( $r, 0, -2 );
		}

		return $r;
	}

	/**
	 * @param    array   $input
	 * @return    string
	 */
	function attachmentArray( $input )
	{
		$r = '';

		foreach ( $input as $attachment ) {
			$r .= $attachment[1] . ' ['.$attachment[6] .'], ';
		}

		if( $r != '' ) {
			$r = substr( $r, 0, -2 );
		}

		return $r;
	}

}