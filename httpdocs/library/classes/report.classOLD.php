<?
require_once($UTILS_CLASS_PATH."security.class.php");
require_once($UTILS_CLASS_PATH."xml.class.php");


class report {
	

	function get_aged_debtors_report($rmc_ref, $group, $period_type, $suppress_zero="no"){
	
		$xml = new xml;
		$security = new security();
		$output_html = "";
		$rowcounter = 0;
		
		$session_id = $security->gen_serial(16);
		$ch = curl_init();
	
		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		$host = "Host: hodd1svapp1";
		$content_type = "Content-Type: text/xml; charset=utf-8";
		
		$data = '<parameters>
		<Reference1>'.$rmc_ref.'</Reference1>
		<Reference2>'.$rmc_ref.'</Reference2>
		<periods>'.$period_type.'</periods>
		<suppresszero>'.$suppress_zero.'</suppresszero>
		<noofperiods>4</noofperiods>
		<excludeprevious>No</excludeprevious>
		</parameters>';
		
		$qube_process_request = '<?xml version="1.0" encoding="utf-8"?>
		<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
		<soap:Body>
		<QubeProcess-3 xmlns="http://qube.qubeglobal.com/ns/webservice/">
		<ClientSessionKey>'.$session_id.'</ClientSessionKey>
		<QubeProcessName>rmgweb:ageddebt1</QubeProcessName>
		<Data>'.$data.'</Data>
		<UserName>rmgweb.servacc</UserName>
		<Password>p4ssw0rd</Password>
		<Group>'.$group.'</Group>
		<Application>QGS Property Management</Application>
		</QubeProcess-3>
		</soap:Body>
		</soap:Envelope>';
		
		$process_headers[] = $host;
		$process_headers[] = $content_type;
		$process_headers[] = "Content-Length : " . strlen ($qube_process_request);
		$process_headers[] = "SOAPAction: \"http://qube.qubeglobal.com/ns/webservice/QubeProcess-3\"";
		
		curl_setopt($ch, CURLOPT_URL, "https://81.106.219.24/qubews/qubews.asmx");
		curl_setopt($ch, CURLOPT_HTTPHEADER, $process_headers);
		curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
		curl_setopt($ch, CURLOPT_HEADER, 0);                			// tells curl to include headers in response
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);        			// return into a variable
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);              			// times out after 30 secs
		curl_setopt($ch, CURLOPT_POSTFIELDS, $qube_process_request);  	// adding POST data
		curl_setopt($ch, CURLOPT_POST, 1); 								// data sent as POST
		curl_setopt($ch, CURLOPT_PORT , 443);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		
		$output = curl_exec($ch);
		//return str_replace("\r\n","",$output);
		$info = curl_getinfo($ch);
		
		if($output === false || $info['http_code'] != 200){
	
			$output = "No cURL data returned - [". $info['http_code']. "]";
			if(curl_error($ch)){
				$output .= "\n". curl_error($ch);
			}
			
			$output_html .= '<tr class="report_msg_row"><td colspan="6" style="border:none;">This service is currently unavailable. Please try again later. (1)</td></tr>';
		}
		else {
					
			//Put XML result into array
			$xml_result = $xml->xml2array($output);
			$return_code = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['tenant-debt-values']['code'];
		
			if($return_code != "00"){

				$output_html .= '<tr class="report_msg_row"><td colspan="6" style="border:none;">This service is currently unavailable. Please try again later.</td></tr>';				
			}
			else{
			
				$num_trans = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['tenant-debt-values']['recordcount'];
				if($num_trans == 0 || $num_trans == ""){
					
					$output_html .= '<tr class="report_msg_row"><td colspan="6" style="border:none;">&nbsp;There are no details available.</td></tr>';
				}
				else{
					
					// Firstly, create the table headers
					$column_headings_array = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['tenant-debt-values']['period'];
					$num_debt_columns = count($column_headings_array);
					$debt_column_html = "";
					$debt_column_id_array = array();
					$debt_column_total_array = array();
					for($h=0;$h < $num_debt_columns;$h++){
						
						$debt_column_id_array[] = $column_headings_array[$h]['id'];
						$debt_column_total_array[] = $column_headings_array[$h]['amount'];
						
						$start_date = "";
						if(!is_array($column_headings_array[$h]['startdate']) && $column_headings_array[$h]['startdate'] != ""){
							$start_date_parts = explode("-", $column_headings_array[$h]['startdate']);
							$start_date = $start_date_parts[2]."/".$start_date_parts[1]."/".$start_date_parts[0];
						}
						
						$end_date = "";
						if(!is_array($column_headings_array[$h]['end-date']) && $column_headings_array[$h]['end-date'] != ""){
							$end_date_parts = explode("-", $column_headings_array[$h]['end-date']);
							$end_date = $end_date_parts[2]."/".$end_date_parts[1]."/".$end_date_parts[0];
						}
						
						$column_desc = $column_headings_array[$h]['description'];
						
						$column_label = $column_desc;
						
						$debt_column_html .= '<th style="text-align:right;" nowrap="nowrap">'.$column_label.'</th>';					
					}
					
					// Stitch together, column headings 
					$debt_column_html = '
					<tr class="report_header_row">
						<th class="first">Name/Address</th>
						'.$debt_column_html.'
						<th class="total_col" style="text-align:right;" nowrap="nowrap">Balance</th>
					</tr>
					';
					$output_html .= $debt_column_html;
					
					
					
					// Now list the tenant data
					if($num_trans == "1"){ // For some reason, the xml2array process does not put only one transaction into an array, so we do this manually
						unset($transact_array);
						$transact_array[0] = array();
						$transact_array[0] = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['tenant-debt-values']['tenant'];
					}
					else{
						$transact_array = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['tenant-debt-values']['tenant'];
					}
					$num_tenants = count($transact_array);
					for($t=0;$t < $num_tenants;$t++){
					
						$row_class = "row_off";
						if($rowcounter % 2 == 0){
							$row_class = "row_on";
						}
						
						$tenant_ref = $transact_array[$t]['reference'];
						$tenant_name = $transact_array[$t]['name'];
						if($transact_array[$t]['unit'] != ""){
							$tenant_name .= "<br />".$transact_array[$t]['unit'];
						}
						$tenant_balance = $transact_array[$t]['balancetoday'];
						
						$output_html .= '
						<tr class="report_data_row">
							<td class="first '.$row_class.'">'.$tenant_name.'</td>
							';
							for($d=0;$d < $num_debt_columns;$d++){
								
								$tenant_debt = "0.00";
								for($i=0;$i < count($debt_column_id_array);$i++){
								
									if($transact_array[$t]['debt'][$d]['id'] == $debt_column_id_array[$i]){
										$tenant_debt = $transact_array[$t]['debt'][$d]['amount'];
										break;
									}
								}
								$output_html .= '<td class="'.$row_class.'" nowrap="nowrap" style="text-align:right;">&nbsp;'.number_format($tenant_debt,2,".",",").'</td>';
							}
							$output_html .= '
							<td class="total_col '.$row_class.'" nowrap="nowrap" style="text-align:right;">&nbsp;'.number_format($tenant_balance,2,".",",").'</td>
						</tr>
						';
						
						$rowcounter++;
					}
					
					// Now generate column totals
					$output_html .= '
					<tr class="report_data_row">
						<td style="border:none;" colspan="'.($num_debt_columns+2).'">&nbsp;</td>
					</tr>
					<tr class="report_data_row">
						<td style="border-right:1px solid #036;color:#036; font-weight:bold;border-left:none;border-bottom:none;">Totals</td>
						';
						$grand_total = 0;
						for($d=0;$d < $num_debt_columns;$d++){
							$grand_total += $debt_column_total_array[$d];
							$output_html .= '<td nowrap="nowrap" style="text-align:right;border-right:1px solid #036;border-top:1px solid #036;border-bottom:1px solid #036;font-weight:bold;color:#036;">&nbsp;'.number_format($debt_column_total_array[$d],2,".",",").'</td>';
						}
						$output_html .= '
						<td nowrap="nowrap" style="text-align:right;border:1px solid #036;font-weight:bold;color:#036;">&nbsp;'.number_format($grand_total,2,".",",").'</td>
					</tr>
					';
					
				}
			}				
		}

		curl_close($ch);
		
		return str_replace("\r\n","",$output_html);
		
	}
	
	
	
	
	
	function get_aged_creditors_report($rmc_ref, $group, $period_type){
	
		$xml = new xml;
		$security = new security();
		$output_html = "";
		$rowcounter = 0;
		
		$session_id = $security->gen_serial(16);
		$ch = curl_init();
	
		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		$host = "Host: hodd1svapp1";
		$content_type = "Content-Type: text/xml; charset=utf-8";
		
		$data = '<parameters>
		<Reference1>'.$rmc_ref.'</Reference1>
		<Reference2>'.$rmc_ref.'</Reference2>
		<periods>'.$period_type.'</periods>
		<noofperiods>4</noofperiods>
		</parameters>';
		
		$qube_process_request = '<?xml version="1.0" encoding="utf-8"?>
		<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
		<soap:Body>
		<QubeProcess-3 xmlns="http://qube.qubeglobal.com/ns/webservice/">
		<ClientSessionKey>'.$session_id.'</ClientSessionKey>
		<QubeProcessName>rmgweb:agedcred</QubeProcessName>
		<Data>'.$data.'</Data>
		<UserName>rmgweb.servacc</UserName>
		<Password>p4ssw0rd</Password>
		<Group>'.$group.'</Group>
		<Application>QGS Property Management</Application>
		</QubeProcess-3>
		</soap:Body>
		</soap:Envelope>';
		
		$process_headers[] = $host;
		$process_headers[] = $content_type;
		$process_headers[] = "Content-Length : " . strlen ($qube_process_request);
		$process_headers[] = "SOAPAction: \"http://qube.qubeglobal.com/ns/webservice/QubeProcess-3\"";
		
		curl_setopt($ch, CURLOPT_URL, "https://81.106.219.24/qubews/qubews.asmx");
		curl_setopt($ch, CURLOPT_HTTPHEADER, $process_headers);
		curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
		curl_setopt($ch, CURLOPT_HEADER, 0);                			// tells curl to include headers in response
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);        			// return into a variable
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);              			// times out after 30 secs
		curl_setopt($ch, CURLOPT_POSTFIELDS, $qube_process_request);  	// adding POST data
		curl_setopt($ch, CURLOPT_POST, 1); 								// data sent as POST
		curl_setopt($ch, CURLOPT_PORT , 443);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		
		$output = curl_exec($ch);
		//return str_replace("\r\n","",$output);
		$info = curl_getinfo($ch);
		
		if($output === false || $info['http_code'] != 200){
	
			$output = "No cURL data returned - [". $info['http_code']. "]";
			if(curl_error($ch)){
				$output .= "\n". curl_error($ch);
			}
			
			$output_html .= '<tr class="report_msg_row"><td colspan="6" style="border:none;">This service is currently unavailable. Please try again later. (1)</td></tr>';
			
		}
		else {
					
			//Put XML result into array
			$xml_result = $xml->xml2array($output);
			$return_code = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['creditor-values']['code'];
		
			if($return_code != "00"){
				$output_html .= '<tr class="report_msg_row"><td colspan="6" style="border:none;">This service is currently unavailable. Please try again later.</td></tr>';				
			}
			else{
			
				$num_trans = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['creditor-values']['recordcount'];
				if($num_trans == 0 || $num_trans == ""){
					
					$output_html .= '<tr class="report_msg_row"><td colspan="6" style="border:none;">&nbsp;There are no details available.</td></tr>';
				}
				else{
					
					// Firstly, create the table headers
					$column_headings_array = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['creditor-values']['period'];
					$num_credit_columns = count($column_headings_array);
					$credit_column_html = "";
					$credit_column_id_array = array();
					$credit_column_total_array = array();
					for($h=0;$h < $num_credit_columns;$h++){
						
						$credit_column_id_array[] = $column_headings_array[$h]['id'];
						$credit_column_total_array[] = $column_headings_array[$h]['amount'];
						$column_desc = $column_headings_array[$h]['description'];
						$column_label = $column_desc;
						
						$credit_column_html .= '<th style="text-align:right;" nowrap="nowrap">'.$column_label.'</th>';					
					}
					
					// Stitch together, column headings 
					$credit_column_html = '
					<tr class="report_header_row">
						<th class="first">Supplier name</th>
						'.$credit_column_html.'
						<th class="total_col" style="text-align:right;" nowrap="nowrap">Balance</th>
					</tr>
					';
					$output_html .= $credit_column_html;
					
					
					
					// Now list the supplier data
					if($num_trans == "1"){ // For some reason, the xml2array process does not put only one transaction into an array, so we do this manually
						unset($transact_array);
						$transact_array[0] = array();
						$transact_array[0] = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['creditor-values']['supplier'];
					}
					else{
						$transact_array = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['creditor-values']['supplier'];
					}
					
					$num_suppliers = count($transact_array);
					for($t=0;$t < $num_suppliers;$t++){
					
						$row_class = "row_off";
						if($rowcounter % 2 == 0){
							$row_class = "row_on";
						}
						
						$supplier_name = $transact_array[$t]['name'];
						$supplier_balance = $transact_array[$t]['total-credit'];
						
						$output_html .= '
						<tr class="report_data_row">
							<td class="first '.$row_class.'">'.$supplier_name.'</td>
							';
							for($d=0;$d < $num_credit_columns;$d++){
								
								$supplier_credit = "0.00";
								for($i=0;$i < count($credit_column_id_array);$i++){
								
									if($transact_array[$t]['credit'][$d]['id'] == $credit_column_id_array[$i]){
										$supplier_credit = $transact_array[$t]['credit'][$d]['amount'];
										break;
									}
								}
								$output_html .= '<td class="'.$row_class.'" nowrap="nowrap" style="text-align:right;">&nbsp;'.number_format($supplier_credit,2,".",",").'</td>';
							}
							$output_html .= '
							<td class="total_col '.$row_class.'" nowrap="nowrap" style="text-align:right;">&nbsp;'.number_format($supplier_balance,2,".",",").'</td>
						</tr>
						';
						
						$rowcounter++;
					}
					
					// Now generate column totals
					$output_html .= '
					<tr class="report_data_row">
						<td style="border:none;" colspan="'.($num_credit_columns+2).'">&nbsp;</td>
					</tr>
					<tr class="report_data_row">
						<td style="border-right:1px solid #036;color:#036; font-weight:bold;border-left:none;border-bottom:none;">Totals</td>
						';
						$grand_total = 0;
						for($d=0;$d < $num_credit_columns;$d++){
							$grand_total += $credit_column_total_array[$d];
							$output_html .= '<td nowrap="nowrap" style="text-align:right;border-right:1px solid #036;border-top:1px solid #036;border-bottom:1px solid #036;font-weight:bold;color:#036;">&nbsp;'.number_format($credit_column_total_array[$d],2,".",",").'</td>';
						}
						$output_html .= '
						<td nowrap="nowrap" style="text-align:right;border:1px solid #036;font-weight:bold;color:#036;">&nbsp;'.number_format($grand_total,2,".",",").'</td>
					</tr>
					';
					
				}
			}				
		}

		curl_close($ch);
		
		return str_replace("\r\n","",$output_html);
		
	}
	
	
	
	
	
	
	function get_transaction_status_report($rmc_ref, $group, $period_type){
	
		$xml = new xml;
		$security = new security();
		$output_html = "";
		$rowcounter = 0;
		
		$session_id = $security->gen_serial(16);
		$ch = curl_init();
	
		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		$host = "Host: hodd1svapp1";
		$content_type = "Content-Type: text/xml; charset=utf-8";
		
		$data = '<parameters>
		<Reference1>'.$rmc_ref.'</Reference1>
		<Reference2>'.$rmc_ref.'</Reference2>
		<periods>'.$period_type.'</periods>
		<noofperiods>4</noofperiods>
		</parameters>';
		
		$qube_process_request = '<?xml version="1.0" encoding="utf-8"?>
		<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
		<soap:Body>
		<QubeProcess-3 xmlns="http://qube.qubeglobal.com/ns/webservice/">
		<ClientSessionKey>'.$session_id.'</ClientSessionKey>
		<QubeProcessName>rmgweb:transtat</QubeProcessName>
		<Data>'.$data.'</Data>
		<UserName>rmgweb.servacc</UserName>
		<Password>p4ssw0rd</Password>
		<Group>'.$group.'</Group>
		<Application>QGS Property Management</Application>
		</QubeProcess-3>
		</soap:Body>
		</soap:Envelope>';
		
		$process_headers[] = $host;
		$process_headers[] = $content_type;
		$process_headers[] = "Content-Length : " . strlen ($qube_process_request);
		$process_headers[] = "SOAPAction: \"http://qube.qubeglobal.com/ns/webservice/QubeProcess-3\"";
		
		curl_setopt($ch, CURLOPT_URL, "https://81.106.219.24/qubews/qubews.asmx");
		curl_setopt($ch, CURLOPT_HTTPHEADER, $process_headers);
		curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
		curl_setopt($ch, CURLOPT_HEADER, 0);                			// tells curl to include headers in response
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);        			// return into a variable
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);              			// times out after 30 secs
		curl_setopt($ch, CURLOPT_POSTFIELDS, $qube_process_request);  	// adding POST data
		curl_setopt($ch, CURLOPT_POST, 1); 								// data sent as POST
		curl_setopt($ch, CURLOPT_PORT , 443);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		
		$output = curl_exec($ch);
		//print str_replace("\r\n","",$output);
		//echo $output;
		//exit;
		//return str_replace("\r\n","",$output);
		$info = curl_getinfo($ch);
		
		if($output === false || $info['http_code'] != 200){
	
			$output = "No cURL data returned - [". $info['http_code']. "]";
			if(curl_error($ch)){
				$output .= "\n". curl_error($ch);
			}
			
			$output_html .= '<tr class="report_msg_row"><td colspan="6" style="border:none;">This service is currently unavailable. Please try again later. (1)</td></tr>';
			
		}
		else {
					
			//Put XML result into array
			$xml_result = $xml->xml2array($output);
			$return_code = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['transactionvalues']['code'];
		
			if($return_code != "00"){
				$output_html .= '<tr class="report_msg_row"><td colspan="6" style="border:none;">This service is currently unavailable. Please try again later.</td></tr>';				
			}
			else{
			
				$num_trans = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['transactionvalues']['recordcount'];
				if($num_trans == 0 || $num_trans == ""){
					
					$output_html .= '<tr class="report_msg_row"><td colspan="6" style="border:none;">&nbsp;There are no details available.</td></tr>';
				}
				else{
					
					// Firstly, create the table headers
					$column_headings_array = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['transactionvalues']['period'];
					$num_credit_columns = count($column_headings_array);
					$credit_column_html = "";
					$credit_column_id_array = array();
					$credit_column_total_array = array();
					for($h=0;$h < $num_credit_columns;$h++){
						
						$credit_column_id_array[] = $column_headings_array[$h]['id'];
						$credit_column_total_array[] = $column_headings_array[$h]['amount'];
						$column_desc = $column_headings_array[$h]['description'];
						$column_label = $column_desc;
						
						$credit_column_html .= '<th style="text-align:right;" nowrap="nowrap">'.$column_label.'</th>';					
					}
					
					// Stitch together, column headings 
					$credit_column_html = '
					<tr class="report_header_row">
						<th class="first">Description</th>
						'.$credit_column_html.'
						<th class="total_col" style="text-align:right;" nowrap="nowrap">Balance</th>
					</tr>
					';
					$output_html .= $credit_column_html;
					
					
					
					// Now list the status data
					if($num_trans == "1"){ // For some reason, the xml2array process does not put only one transaction into an array, so we do this manually
						unset($transact_array);
						$transact_array[0] = array();
						$transact_array[0] = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['transactionvalues']['status'];
					}
					else{
						$transact_array = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['transactionvalues']['status'];
					}
					$num_status = count($transact_array);
					
					
					
					// List contents
					for($t=0;$t < $num_status;$t++){
					
						$row_class = "row_off";
						if($rowcounter % 2 == 0){
							$row_class = "row_on";
						}
						
						$status_name = $transact_array[$t]['description'];
						$status_balance = $transact_array[$t]['totaldebt'];
						
						$output_html .= '
						<tr class="report_data_row">
							<td class="first '.$row_class.'">'.$status_name.'</td>
							';
							for($d=0;$d < $num_credit_columns;$d++){
								
								$status_credit = "0.00";
								for($i=0;$i < count($credit_column_id_array);$i++){
								
									if($transact_array[$t]['period'][$d]['id'] == $credit_column_id_array[$i]){
										$status_credit = $transact_array[$t]['period'][$d]['amount'];
										break;
									}
								}
								$output_html .= '<td class="'.$row_class.'" nowrap="nowrap" style="text-align:right;">&nbsp;'.number_format($status_credit,2,".",",").'</td>';
							}
							$output_html .= '
							<td class="total_col '.$row_class.'" nowrap="nowrap" style="text-align:right;">&nbsp;'.number_format($status_balance,2,".",",").'</td>
						</tr>
						';
						
						$rowcounter++;
					}
					
					// Now generate column totals
					$output_html .= '
					<tr class="report_data_row">
						<td style="border:none;" colspan="'.($num_credit_columns+2).'">&nbsp;</td>
					</tr>
					<tr class="report_data_row">
						<td style="border-right:1px solid #036;color:#036; font-weight:bold;border-left:none;border-bottom:none;">Totals</td>
						';
						$grand_total = 0;
						for($d=0;$d < $num_credit_columns;$d++){
							$grand_total += $credit_column_total_array[$d];
							$output_html .= '<td nowrap="nowrap" style="text-align:right;border-right:1px solid #036;border-top:1px solid #036;border-bottom:1px solid #036;font-weight:bold;color:#036;">&nbsp;'.number_format($credit_column_total_array[$d],2,".",",").'</td>';
						}
						$output_html .= '
						<td nowrap="nowrap" style="text-align:right;border:1px solid #036;font-weight:bold;color:#036;">&nbsp;'.number_format($grand_total,2,".",",").'</td>
					</tr>
					';
					
				}
			}				
		}

		curl_close($ch);
		
		return str_replace("\r\n","",$output_html);
		
	}
	
	
	
	
	
	
	
	function get_kpi_report($rmc_ref, $group, $fund_types){
	
		$xml = new xml;
		$security = new security();
		$output_html = "";
		$rowcounter = 0;
		
		$session_id = $security->gen_serial(16);
		$ch = curl_init();
	
		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		$host = "Host: hodd1svapp1";
		$content_type = "Content-Type: text/xml; charset=utf-8";
		
		$data = '<parameters>
		<Reference1>'.$rmc_ref.'</Reference1>
		<Reference2>'.$rmc_ref.'</Reference2>
		<fundType>'.$fund_types.'</fundType>
		</parameters>';
		
		$qube_process_request = '<?xml version="1.0" encoding="utf-8"?>
		<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
		<soap:Body>
		<QubeProcess-3 xmlns="http://qube.qubeglobal.com/ns/webservice/">
		<ClientSessionKey>'.$session_id.'</ClientSessionKey>
		<QubeProcessName>rmgweb:kpi1</QubeProcessName>
		<Data>'.$data.'</Data>
		<UserName>rmgweb.servacc</UserName>
		<Password>p4ssw0rd</Password>
		<Group>'.$group.'</Group>
		<Application>QGS Property Management</Application>
		</QubeProcess-3>
		</soap:Body>
		</soap:Envelope>';
		
		$process_headers[] = $host;
		$process_headers[] = $content_type;
		$process_headers[] = "Content-Length : " . strlen ($qube_process_request);
		$process_headers[] = "SOAPAction: \"http://qube.qubeglobal.com/ns/webservice/QubeProcess-3\"";
		
		curl_setopt($ch, CURLOPT_URL, "https://81.106.219.24/qubews/qubews.asmx");
		curl_setopt($ch, CURLOPT_HTTPHEADER, $process_headers);
		curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
		curl_setopt($ch, CURLOPT_HEADER, 0);                			// tells curl to include headers in response
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);        			// return into a variable
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);              			// times out after 30 secs
		curl_setopt($ch, CURLOPT_POSTFIELDS, $qube_process_request);  	// adding POST data
		curl_setopt($ch, CURLOPT_POST, 1); 								// data sent as POST
		curl_setopt($ch, CURLOPT_PORT , 443);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		
		$output = curl_exec($ch);
		//print str_replace("\r\n","",$output);
		//exit;
		//return str_replace("\r\n","",$output);
		$info = curl_getinfo($ch);
		
		if($output === false || $info['http_code'] != 200){
	
			$output = "No cURL data returned - [". $info['http_code']. "]";
			if(curl_error($ch)){
				$output .= "\n". curl_error($ch);
			}
			
			$output_html .= '<tr class="report_msg_row"><td colspan="5" style="border:none;">This service is currently unavailable. Please try again later. (1)</td></tr>';
			
		}
		else {
					
			//Put XML result into array
			$xml_result = $xml->xml2array($output);
			$return_code = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['kpivalues']['code'];
		
			if($return_code != "00"){
				$output_html .= '<tr class="report_msg_row"><td colspan="5" style="border:none;">This service is currently unavailable. Please try again later.</td></tr>';				
			}
			else{
			
				$num_properties = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['kpivalues']['recordcount'];
				if($num_properties == 0 || $num_properties == ""){
					
					$output_html .= '<tr class="report_msg_row"><td colspan="5" style="border:none;">&nbsp;There are no details available.</td></tr>';
				}
				else{
					
					// Stitch together, column headings 
					$output_html .= '
					<tr class="report_header_row">
						<th class="first">Description</th>
						<th style="text-align:right;" nowrap="nowrap">Bank</th>
						<th style="text-align:right;" nowrap="nowrap">Debtors</th>
						<th style="text-align:right;" nowrap="nowrap">Credit Notes</th>
						<th style="text-align:right;" nowrap="nowrap">Creditors</th>
					</tr>
					';
					
					
					// List all properties...
					for($p=0;$p < $num_properties;$p++){
					
						// Now list the fund data
						$num_funds = count($xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['kpivalues']['property']['fund']);

						for($t=0;$t < $num_funds;$t++){
						
							$row_class = "row_off";
							if($rowcounter % 2 == 0){
								$row_class = "row_on";
							}
							
							$fund_name = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['kpivalues']['property']['fund'][$t]['description'];
							$fund_bank = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['kpivalues']['property']['fund'][$t]['bank'];
							$fund_debtors = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['kpivalues']['property']['fund'][$t]['debtors'];
							$fund_crnotes = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['kpivalues']['property']['fund'][$t]['crnotes'];
							$fund_creditors = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['kpivalues']['property']['fund'][$t]['creditors'];
							
							$output_html .= '
							<tr class="report_data_row">
								<td class="first '.$row_class.'">'.$fund_name.'</td>
								<td class="'.$row_class.'" nowrap="nowrap" style="text-align:right;">&nbsp;'.number_format($fund_bank,2,".",",").'</td>
								<td class="'.$row_class.'" nowrap="nowrap" style="text-align:right;">&nbsp;'.number_format($fund_debtors,2,".",",").'</td>
								<td class="'.$row_class.'" nowrap="nowrap" style="text-align:right;">&nbsp;'.number_format($fund_crnotes,2,".",",").'</td>
								<td class="'.$row_class.'" nowrap="nowrap" style="text-align:right;">&nbsp;'.number_format($fund_creditors,2,".",",").'</td>
							</tr>
							';
							
							$rowcounter++;
						}
					}
					
					
					// Now generate column totals
					if($xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['kpivalues']['property']['solvency'] >= 0){
						$solvency_colour = "#693";
					}
					else{
						$solvency_colour = "#f00";	
					}
					$output_html .= '
					<tr class="report_data_row">
						<td style="border:none;" colspan="5">&nbsp;</td>
					</tr>
					<tr class="report_data_row">
						<td style="border-right:1px solid #036;color:#036; font-weight:bold;border-left:none;border-bottom:none;">Totals</td>
						<td nowrap="nowrap" style="text-align:right;border-right:1px solid #036;border-top:1px solid #036;border-bottom:1px solid #036;font-weight:bold;color:#036;">&nbsp;'.number_format($xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['kpivalues']['property']['bank'],2,".",",").'</td>
						<td nowrap="nowrap" style="text-align:right;border-right:1px solid #036;border-top:1px solid #036;border-bottom:1px solid #036;font-weight:bold;color:#036;">&nbsp;'.number_format($xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['kpivalues']['property']['debtors'],2,".",",").'</td>
						<td nowrap="nowrap" style="text-align:right;border-right:1px solid #036;border-top:1px solid #036;border-bottom:1px solid #036;font-weight:bold;color:#036;">&nbsp;'.number_format($xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['kpivalues']['property']['crnotes'],2,".",",").'</td>
						<td nowrap="nowrap" style="text-align:right;border-right:1px solid #036;border-top:1px solid #036;border-bottom:1px solid #036;font-weight:bold;color:#036;">&nbsp;'.number_format($xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['kpivalues']['property']['creditors'],2,".",",").'</td>
					</tr>
					<!--
					<tr class="report_data_row">
						<td style="border:none;" colspan="5">&nbsp;</td>
					</tr>
					<tr class="report_data_row">
						<td style="color:#036; font-weight:bold;border:none;">Solvency</td>
						<td colspan="3" style="border:none;">&nbsp;</td>
						<td nowrap="nowrap" style="text-align:right;border:1px solid #036;font-weight:bold;color:'.$solvency_colour.';">&nbsp;'.number_format($xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['kpivalues']['property']['solvency'],2,".",",").'</td>
					</tr>
					-->
					';
					
				}
			}				
		}

		curl_close($ch);
		
		return str_replace("\r\n","",$output_html);
		
	}
	
}


?>