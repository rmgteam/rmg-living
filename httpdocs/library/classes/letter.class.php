<?
//======================================================
// Class to build and manipulate HTML letter templates
//======================================================
require("utils.php");
require_once($UTILS_CLASS_PATH."resident.class.php");
require_once($UTILS_CLASS_PATH."letters/letters.class.php");
require_once($UTILS_CLASS_PATH."contractors.class.php");
require_once($UTILS_CLASS_PATH."security.class.php");
require_once($UTILS_CLASS_PATH."encryption.class.php");

class letter {

	var $signature_array = array();
	var $your_ref = "";
	var $recipient_name;
	var $recipient_address_1;
	var $recipient_address_2;
	var $recipient_address_3;
	var $recipient_address_4;
	var $recipient_address_city;
	var $recipient_address_county;
	var $recipient_address_postcode;
	//var $recipient_address_country;
	
	function set_recipient($resident_obj, $type = 1){
		if($type == 1){
			if($resident_obj){
				
				$this->recipient_name = $resident_obj->resident_name;
				$this->recipient_address_1 = $resident_obj->resident_address_1;
				$this->recipient_address_2 = $resident_obj->resident_address_2;
				$this->recipient_address_3 = $resident_obj->resident_address_3;
				$this->recipient_address_4 = $resident_obj->resident_address_4;
				$this->recipient_address_city = $resident_obj->resident_address_city;
				$this->recipient_address_county = $resident_obj->resident_address_county;
				$this->recipient_address_postcode = $resident_obj->resident_address_postcode;
			}
			else{
				return false;
			}
		}
		
		//$this->set_your_ref($resident_obj, $type);
		$this->set_opening($this->recipient_name, 3);
	}
	
	function set_contractor($contractor_obj, $type = 1){
		if($type == 1){
			if($contractor_obj){
				
				$this->recipient_name = $contractor_obj->contractor_name;
				$this->recipient_address_1 = trim( preg_replace( '/\n\r|\r\n/', '<br />', $contractor_obj->contractor_address));
				$this->recipient_address_2 = "";
				$this->recipient_address_3 = "";
				$this->recipient_address_4 = "";
				$this->recipient_address_city = "";
				$this->recipient_address_county = "";
				$this->recipient_address_postcode = "";
			}
			else{
				return false;
			}
		}
		
		//$this->set_your_ref($resident_obj, $type);
		$this->set_opening($this->recipient_name, 3);
	}

	

	//==========================================================================================
	// Letter reference function
	//==========================================================================================
	
	// Sets the recipients reference for the letter
	/*
	function set_your_ref($resident_obj, $type = 1){
		
		if($resident_obj){
			
			// Type 1 = reference relating to resident
			if($type == 1){
				
				$this->your_ref = "Your ref: ".$resident_obj->."-".$resident_obj->resident_ref;
				return true;
			}
			
		}
		else{
			return false;
		}
		
	}
	*/
	
	// Gets the recipients reference for the letter
	/*
	function get_your_ref(){
		return $this->your_ref;
	}
	*/
	
	
	//===================================================================================================
	// Recipient address function(s)
	//===================================================================================================
	
	function get_address($format = 1){
	
		switch ($format){
			case 1:		$start_enclosure = "";
						$finish_enclosure = "";
						$begin_enclosure = "";
						$end_enclosure = "<br>";
						break;
			case 2:		$start_enclosure = "<table>";
						$finish_enclosure = "</table>";
						$begin_enclosure = "<tr><td nowrap>";
						$end_enclosure = "</td></tr>";
						break;
		}
	

		if($this->recipient_name != ""){$formatted_address .= $begin_enclosure.$this->recipient_name.$end_enclosure;}
		if($this->recipient_address_1 != ""){$formatted_address .= $begin_enclosure.$this->recipient_address_1.$end_enclosure;}
		if($this->recipient_address_2 != ""){$formatted_address .= $begin_enclosure.$this->recipient_address_2.$end_enclosure;}
		if($this->recipient_address_3 != ""){$formatted_address .= $begin_enclosure.$this->recipient_address_3.$end_enclosure;}
		if($this->recipient_address_4 != ""){$formatted_address .= $begin_enclosure.$this->recipient_address_4.$end_enclosure;}
		if($this->recipient_address_city != ""){$formatted_address .= $begin_enclosure.$this->recipient_address_city.$end_enclosure;}
		if($this->recipient_address_county != ""){$formatted_address .= $begin_enclosure.$this->recipient_address_county.$end_enclosure;}
		if($this->recipient_address_postcode != ""){$formatted_address .= $begin_enclosure.$this->recipient_address_postcode.$end_enclosure;}
		//if($this->recipient_address_country != ""){$formatted_address .= $begin_enclosure.$this->recipient_address_country.$end_enclosure;}
		$formatted_address = $start_enclosure.$formatted_address.$finish_enclosure;
		
		return $formatted_address;
		
	}
	
	
	//===========================================
	// 'Opening' functions
	//===========================================

	// Sets opening greeting
	function set_opening($name, $type = 1){
		if($type == 1){$this->opening = "Dear Sir/Madam,";}
		if($type == 2){$this->opening = "To whom it may concern,";}
		if($type == 3){$this->opening = "Dear $name,";}
	}
	
	function get_opening(){
		return $this->opening;
	}
	
	
	//==========================================================
	// Functions to set main body of letter (uses files in 'templates' folder)
	//==========================================================
	function set_body($template){
	
		global $UTILS_TEMPLATE_PATH;
	
		if($template != ""){
			$this->body = $UTILS_TEMPLATE_PATH.$template;
		}
		else{
			return false;
		}
	}

	function get_body(){
		return $this->body;
	}
	
	//new functions added while creating pdf letters
	function set_contractors($contractor_obj, $type = 1){
		if($type == 1){
			if($contractor_obj){
	
				$this->recipient_name = '<p>'.$contractor_obj->contractor_name.'</p>';
				$this->recipient_address_1 = '<p>'.trim( preg_replace( '</p><p>', '<br />', $contractor_obj->contractor_address)).'</p>';
				$this->recipient_address_2 = "";
				$this->recipient_address_3 = "";
				$this->recipient_address_4 = "";
				$this->recipient_address_city = "";
				$this->recipient_address_county = "";
				$this->recipient_address_postcode = "";
			}
			else{
				return false;
			}
		}
	
		//$this->set_your_ref($resident_obj, $type);
		$this->set_openings($this->recipient_name, 3);
	}
	function set_openings($name, $type = 1){
		if($type == 1){$this->opening = "<p>Dear Sir/Madam,</p>";}
		if($type == 2){$this->opening = "<p>To whom it may concern,</p>";}
		if($type == 3){$this->opening = "<p>Dear".$name.",</p>";}
	}
	
	
	
	function contractorLetter($resident_nums,$resident_passwords)
	{
		$security = new security;
		$crypt = new encryption_class;

		$resident = new resident();
		$letter_pdf = new letters('P', 'mm', 'A4');
		$letter_pdf->SetDefaultFont('Arial', '', 10);
		$letter_pdf->setStyle("b","Arial","B");
		$letter_pdf->setStyle("i","Arial","I");
		$letter_pdf->bMargin = 15;
		$letter_pdf->header_template = "headed_paper";
		$letter_pdf->line_height = 4.5;
		$letter_pdf->header_email = $UTILS_INFO_EMAIL;
		$letter_pdf->header_phone = $UTILS_TEL_MAIN_TEL;
		
		for($i=0;$i < count($resident_nums);$i++){
				
			$contractors = new contractors($resident_nums[$i]);			
				
			$sql = "UPDATE cpm_contractors SET
			cpm_contractors_letter_sent = 'Y'
			WHERE cpm_contractors_qube_id = '".$resident_nums[$i]."'";
			@mysql_query($sql);
				
			$the_password = $resident_passwords[$i];
				
			if(strtoupper($contractors->contractor_pcm) != 'POST'){
				$the_password = $password;
			}
				
			$sql = "SELECT *
			FROM cpm_contractors_user
			WHERE cpm_contractors_user_ref = '".$_POST['contractor_id']."'";
			$result = @mysql_query($sql);
			$num_rows = @mysql_num_rows($result);
		
			if($num_rows == 0){
				$sql = "INSERT INTO cpm_contractors_user SET
				cpm_contractors_user_ref = '".$resident_nums[$i]."',
				cpm_contractors_user_qube_ref = '".$resident_nums[$i]."',
				cpm_contractors_user_disabled = 'False',
				cpm_contractors_user_parent = '0', ";
			}else{
				$sql = "UPDATE cpm_contractors_user SET ";
			}
				
			$sql .= "cpm_contractors_user_password = '".$security->clean_query($crypt->encrypt($UTILS_DB_ENCODE, $the_password))."'";
				
			if($num_rows > 0){
				$sql .= "WHERE cpm_contractors_user_ref = '".$_POST['contractor_id']."'";
			}
				
			@mysql_query($sql);
				
			$date = new DateTime();
				
			$sql = "
			INSERT INTO cpm_print_contractors SET
			contractor_id = '".$resident_nums[$i]."',
			user_name = '".$_SESSION['user_id']."',
			print_job_time = '".$date->format('Y-m-d H:i:s')."'";
			@mysql_query($sql);
			
			if(strtoupper($contractors->contractor_pcm) != 'POST'){
				$password = $resident->get_rand_id(8, 'ALPHA', 'UPPER');
			
				$letter_pdf->include_header = true;
				$letter_pdf->SetMargins(12.7, 0, 12.7);
				$letter_pdf->add_page();
			
				$set_contractors = $this->set_contractor($contractors);
			
				$letter_pdf->parse_html($set_contractors);
			
			
				$this->set_body("letter_cpm_living_welcome_contractors.php");
			
				$get_body = $this->get_body();
			
				$letter_pdf->parse_html($get_body);
			
				// Make letter reference
				$your_ref = stripslashes($resident_nums[$i]);
				
				if($contractors->contractor_email != '')
				{
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$headers .= 'From: customerservice@rmguk.com' . "\r\n";
					$headers .= 'Reply-To: customerservice@rmguk.com' . "\r\n";
					$headers .= 'X-Mailer: PHP/' . phpversion();
						
								
					$sql = "INSERT INTO cpm_mailer SET
					mail_to = '".$contractors->contractor_email."',
					mail_from = 'customerservice@rmguk.com',
					mail_subject = 'Welcome to RMG Living',
					mail_headers = '".$headers."',
					mail_message = '".$get_body."'";
				
					@mysql_query($sql);
				}
				//Use the appropriate letter format
				return	$letter_pdf->Output();
			}
				
		}
	}
	
	
	//===========================================
	// 'Closing' functions
	//===========================================

	// Sets up signature components based in username provided
	/*
	function set_signature($username = "cwoods"){
		$sql = "SELECT * FROM cpm_signatures WHERE signature_username = '".$username."'";
		$result = mysql_query($sql);
		$this->signature = mysql_fetch_array($result);	
	}
	*/
	
	// Sets closing (Yours sincerely, etc.)
	/*
	function set_closing($closing = "Yours sincerely,"){
		$this->signature['signature_closing'] = $closing;
	}
	*/
	
	// Returns signature
	/*
	function get_signature($sig_image=1, $img_per=100){
	
		if(is_array($this->signature)){
		
			global $UTILS_FILE_PATH;
			global $UTILS_WEBROOT;
		
			// Tests for image and returns new width and height
			unset($img_size);
			$img_size = $this->set_signature_image_size($UTILS_FILE_PATH.$this->signature['signature_image'], $img_per);
			
			// Sets closing if not already called
			if($this->signature['signature_closing'] == ""){$this->set_closing();}
			unset($this->signature_array);
			$this->signature_array[] = $this->signature['signature_closing'];
			
			// Sets up signature components into array
			if($sig_image && is_array($img_size)){
				$this->signature_array[] = "<img src=\"".$UTILS_WEBROOT.$this->signature['signature_image']."\" width=\"".$img_size[0]."\" height=\"".$img_size[1]."\">";
			}
			else{
				$this->signature_array[] = "<br>";
			}
			if($this->signature['signature_name'] != ""){$this->signature_array[] = $this->signature['signature_name'];}
			if($this->signature['signature_job_title'] != ""){$this->signature_array[] = $this->signature['signature_job_title'];}
			if($this->signature['signature_division'] != ""){$this->signature_array[] = $this->signature['signature_division'];}
			
			// Compiles signature components into signature
			for($s=0;$s<(count($this->signature_array)-1);$s++){
				$final_signature .= $this->signature_array[$s]."<br>";
			}
			$final_signature .= $this->signature_array[$s];
			
			return $final_signature;
		}
		else{
			return false;
		}
	}
	*/
	
	
	// Adjusts signature size (signatures are usually stored as high res.)
	/*
	function set_signature_image_size($src, $per=100){
		if($src != ""){
		
			// Sets image size to display
			$img_size = getimagesize($src);
			$img_size[0] = ceil($img_size[0] * ($per/100));
			$img_size[1] = ceil($img_size[1] * ($per/100));
			return $img_size;
		}
		else{
			return false;
		}
	}
	*/
	
	

	
	

	
	
	
	
	

}

?>