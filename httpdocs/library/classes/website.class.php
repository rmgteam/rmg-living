<?

class website {

	// Check if they are able to access 'your community' section
	function allow_community_access(){
		if(	(!isset($_SESSION['login'])) || $_SESSION['login'] == ""){
			session_destroy();
			header("Location: ".$GLOBALS['UTILS_HTTP_ADDRESS']."index.php");
			exit;
		}
	}
	
	// Check if they are able to access content management system section
	function allow_cms_access(){
		if(	(!isset($_SESSION['cms_login'])) || $_SESSION['cms_login'] == ""){
			session_destroy();
			header("Location: ".$GLOBALS['UTILS_HTTP_ADDRESS']."index.php");
			exit;
		}
	}
	
	// Check if they are able to access content management system section
	function allow_contractor_access(){
		if(	(!isset($_SESSION['contractors_qube_id'])) || $_SESSION['contractors_qube_id'] == ""){
			session_destroy();
			header("Location: ".$GLOBALS['UTILS_HTTP_ADDRESS']."index.php");
			exit;
		}
	}


}


?>