<?
//======================================================
// Class to extract and manipulate resident data
//======================================================
require_once($UTILS_CLASS_PATH."encryption.class.php");
require_once($UTILS_CLASS_PATH."security.class.php");
require_once($UTILS_CLASS_PATH."owner.class.php");
require_once($UTILS_CLASS_PATH."rmc.class.php");


class resident {

	var $resident_id;
	var $resident_ref;
	var $resident_serial;
	var $resident_qube_db_id;
	var $subsidiary_id;
	var $subsidiary_name;
	var $rmc_num;
	var $resident_num;
	var $voyager_resident_num;
	var $resident_name;
	var $resident_address_1;
	var $resident_address_2;
	var $resident_address_3;
	var $resident_address_4;
	var $resident_address_city;
	var $resident_address_county;
	var $resident_address_postcode;
	var $resident_address_country;
	var $weblinks_username;
	var $weblinks_password;
	var $is_resident_director;
	var $director_assoc_resident_num;
	var $date_last_imported;
	var $resident_status;
	var $resident_is_active;
	var $resident_is_developer_dummy;
	var $is_subtenant_account;
	var $subtenant_related_lessee_resident_num;
	var $tel;
	var $mobile;
	var $question_id_1;
	var $question_id_2;
	var $answer_1;
	var $answer_2;
	var $email;
	var $resident_email;
	var $password;
	var $optout_marketing;
	var $allow_password_reset;
	var $is_first_login;
	var $password_to_be_sent;
	var $is_demo_resident_account;
	var $login_expiry_YMD;
	var $notes;
	var $is_developer;
	var $developer_id;
	var $is_linked_to_master_account;
	var $announce_optin;
	var $survey_optout;
	var $hide_letter;
	var $date_created;
	var $subsidiary_code;
	
	
	function resident($ref="", $ref_type="id"){
		
		$security = new security;
		
		if($ref_type == "ref"){
			$ref_clause = " lres.resident_ref = '".$security->clean_query($ref)."' AND ";
		}
		else{
			$ref_clause = " res.resident_num = ".$security->clean_query($ref)." AND ";
		}
			
		$sql = "
		SELECT * 
		FROM cpm_lookup_residents lres, cpm_residents_extra rex, cpm_residents res LEFT JOIN cpm_subsidiary s ON (res.subsidiary_id = s.subsidiary_id) 
		WHERE 
		$ref_clause 
		res.resident_num = lres.resident_lookup AND 
		lres.resident_lookup = rex.resident_num";
		//print $sql;
		$result = @mysql_query($sql);
		$row = @mysql_fetch_array($result);
		
		$this->resident_id = $row['resident_id'];
		$this->resident_ref = $row['resident_ref'];
		$this->resident_serial = $row['resident_serial'];
		$this->resident_qube_db_id = $row['resident_qube_db_id'];
		$this->subsidiary_id = $row['subsidiary_id'];
		$this->subsidiary_name = $row['subsidiary_name'];
		$this->rmc_num = $row['rmc_num'];
		$this->resident_num = $row['resident_num'];
		$this->voyager_resident_num = $row['voyager_resident_num'];
		$this->resident_name = $row['resident_name'];
		$this->resident_address_1 = $row['resident_address_1'];
		$this->resident_address_2 = $row['resident_address_2'];
		$this->resident_address_3 = $row['resident_address_3'];
		$this->resident_address_4 = $row['resident_address_4'];
		$this->resident_address_city = $row['resident_address_city'];
		$this->resident_address_county = $row['resident_address_county'];
		$this->resident_address_postcode = $row['resident_address_postcode'];
		$this->resident_address_country = $row['resident_address_country'];
		$this->weblinks_username = $row['weblinks_username'];
		$this->weblinks_password = $row['weblinks_password'];
		$this->is_resident_director = $row['is_resident_director'];
		$this->director_assoc_resident_num = $row['director_assoc_resident_num'];
		$this->date_last_imported = $row['date_last_imported'];
		$this->resident_status = $row['resident_status'];
		$this->resident_is_active = $row['resident_is_active'];
		$this->resident_is_developer_dummy = $row['resident_is_developer_dummy'];
		$this->is_subtenant_account = $row['is_subtenant_account'];
		$this->subtenant_related_lessee_resident_num = $row['subtenant_related_lessee_resident_num'];
		$this->tel = $row['tel'];
		$this->mobile = $row['mobile'];
		$this->question_id_1 = $row['question_id_1'];
		$this->question_id_2 = $row['question_id_2'];
		$this->answer_1 = $row['answer_1'];
		$this->answer_2 = $row['answer_2'];
		$this->email = $row['email'];
		$this->resident_email = $row['resident_email'];
		$this->password = $row['password'];
		$this->optout_marketing = $row['optout_marketing'];
		$this->allow_password_reset = $row['allow_password_reset'];
		$this->is_first_login = $row['is_first_login'];
		$this->password_to_be_sent = $row['password_to_be_sent'];
		$this->is_demo_resident_account = $row['is_demo_resident_account'];
		$this->login_expiry_YMD = $row['login_expiry_YMD'];
		$this->notes = $row['notes'];
		$this->is_developer = $row['is_developer'];
		$this->developer_id = $row['developer_id'];
		$this->announce_optin = $row['announce_optin'];
		$this->survey_optout = $row['survey_optout'];
		$this->hide_letter = $row['hide_letter'];
		$this->is_linked_to_master_account = $row['is_linked_to_master_account'];
		$this->is_subtenant_account = $row['is_subtenant_account'];
		$this->date_created = $row['date_created'];
		$this->subsidiary_code = $row['subsidiary_code'];
	}
	
	
	function is_email_unique($email){
		
		$security = new security;
		
		if($this->resident_id != ""){
			$id_clause = " resident_id <> ".$this->resident_id." AND ";
		}
		
		$sql = "
		SELECT count(*) 
		FROM cpm_residents_extra 
		WHERE 
		$id_clause 
		email = '".$security->clean_query($email)."'
		";
		$result = @mysql_query($sql);
		$row = @mysql_fetch_row($result);
		if($row[0] > 0){
			return false;
		}
		else{
			return true;
		}
	}
	
	
	function get_password($user_id = ""){
	
		if($user_id == ""){
			$user_id = $this->password;
		}
	
		$sql = "SELECT password FROM cpm_residents res, cpm_residents_extra rex WHERE res.resident_num=rex.resident_num AND res.resident_num = ".$user_id;
		$result = mysql_query($sql);
		$row = mysql_fetch_row($result);
	
		global $UTILS_DB_ENCODE;
		$crypt = new encryption_class;
		
		return $crypt->decrypt($UTILS_DB_ENCODE, stripslashes($row[0]));
	}
	
	// Gen password for resident
	function gen_password($res_num){
		
		global $UTILS_DB_ENCODE;
		
		$crypt = new encryption_class;
		$password_unique = "N";
		while($password_unique == "N"){
			$password = $this->get_rand_id(8, 'ALPHA', 'UPPER');
			$sql = "SELECT password FROM cpm_residents_extra WHERE password = '".addslashes($crypt->encrypt($UTILS_DB_ENCODE, $password))."'";
			$result = mysql_query($sql);
			$password_exists = mysql_num_rows($result);
			if($password_exists < 1){
				$password_unique = "Y";
			}
		}
		$sql2 = "
		UPDATE cpm_residents_extra SET
		is_first_login = 'Y',
		password = '".$crypt->encrypt($UTILS_DB_ENCODE, $password)."'
		WHERE resident_num = ".$res_num;
		if( @mysql_query($sql2) ){
		
			return $password;
		}
		else{
			return false;
		}
	}
	
	
	function get_rand_id($length, $type, $case){

		 $rand_alpha_value[] = "a";
		 $rand_alpha_value[] = "b";
		 $rand_alpha_value[] = "c";
		 $rand_alpha_value[] = "d";
		 $rand_alpha_value[] = "e";
		 $rand_alpha_value[] = "f";
		 $rand_alpha_value[] = "g";
		 $rand_alpha_value[] = "h";
		 $rand_alpha_value[] = "j";
		 $rand_alpha_value[] = "k";
		 $rand_alpha_value[] = "m";
		 $rand_alpha_value[] = "n";
		 $rand_alpha_value[] = "p";
		 $rand_alpha_value[] = "q";
		 $rand_alpha_value[] = "r";
		 $rand_alpha_value[] = "t";
		 $rand_alpha_value[] = "u";
		 $rand_alpha_value[] = "v";
		 $rand_alpha_value[] = "w";
		 $rand_alpha_value[] = "x";
		 $rand_alpha_value[] = "y";
		 $rand_numeric_value[] = "2";
		 $rand_numeric_value[] = "3";
		 $rand_numeric_value[] = "4";
		 $rand_numeric_value[] = "6";
		 $rand_numeric_value[] = "7";
		 $rand_numeric_value[] = "8";
		 $rand_numeric_value[] = "9";
		 
		 if($type == "ALPHA"){
			$rand_value = $rand_alpha_value;
		 }
		 elseif($type == "NUMERIC"){
			$rand_value = $rand_numeric_value;
		 }
		 else{
			$rand_value = array_merge ($rand_alpha_value, $rand_numeric_value);
		 }
	
		if($length>0){ 
			$rand_id="";
			for($i=1; $i<=$length; $i++){
				mt_srand((double)microtime() * 1000000);
				$num = mt_rand(1, count($rand_value) );
				$rand_id .= $rand_value[$num-1];
			}
		}
		
		if($case == "UPPER"){
			return strtoupper($rand_id);
		}
		elseif($case == "LOWER"){
			return strtolower($rand_id);
		}
		else{
			return $rand_id;
		}
	}
	
	
	function link_to_master_account(){
		
		$sql = "
		UPDATE cpm_residents_extra SET
			is_linked_to_master_account = 'Y'
		WHERE resident_num = ".$this->resident_num."
		";
		if( @mysql_query($sql) ){
			return true;
		}
		return false;
	}
	
	
	function get_master_account_id(){
		
		$sql = "
		SELECT master_account_id 
		FROM cpm_master_account_resident_assoc 
		WHERE resident_num = ".$this->resident_num."
		";
		//print $sql;
		$result = @mysql_query($sql);
		$row = @mysql_fetch_row($result);
		return $row[0];
	}
	
	
	function get_num_director_ins_sched(){
		
		global $UTILS_INTRANET_DB_LINK;
		
		// Get owner ref that this director account is associated to
		$rmc = new rmc($this->rmc_num);
		$owner = new owner($rmc->owner_id);
		
		// Get intranet-based subsidairy_id
		$sql_s = "
		SELECT subsidiary_id 
		FROM subsidiary 
		WHERE 
		subsidiary_code <> '' AND 
		subsidiary_code IS NOT NULL AND 
		subsidiary_code = '".$this->subsidiary_code."'";
		$result_s = @mysql_query($sql_s, $UTILS_INTRANET_DB_LINK);
		$row_s = @mysql_fetch_row($result_s);
		$int_subsidiary_id = $row_s[0];
		
		// Get intranet-based owner_id
		$sql_o = "
		SELECT owner_id 
		FROM owner 
		WHERE 
		owner_ref <> '' AND 
		owner_ref IS NOT NULL AND 
		subsidiary_id = ".$int_subsidiary_id." AND 
		owner_ref = '".$owner->owner_ref."'";
		$result_o = @mysql_query($sql_o, $UTILS_INTRANET_DB_LINK);
		$row_o = @mysql_fetch_row($result_o);
		$int_owner_id = $row_o[0];
		
		// Check for number of schedules that this Director can see (search Intranet db)
		$sql = "
		SELECT count(*) 
		FROM ins_sched i LEFT JOIN owner o ON (i.owner_id = o.owner_id) LEFT JOIN rmcs r ON (i.rmc_num = r.rmc_num) 
		WHERE 
		(i.owner_id = ".$int_owner_id." OR r.owner_id = ".$int_owner_id.") AND 
		i.ins_sched_reviewed = 'N' AND 
		i.ins_sched_discon = 'N' AND 
		i.ins_sched_is_visible = 'Y' AND 
		i.ins_sched_filename <> '' AND 
		i.ins_sched_filename IS NOT NULL 
		";
		$result = @mysql_query($sql, $UTILS_INTRANET_DB_LINK);
		$row = @mysql_fetch_row($result);
		return $row[0];
	}
	
	
	function get_director_ins_sched(){
		
		global $UTILS_INTRANET_DB_LINK;
		
		// Get owner ref that this director account is associated to
		$rmc = new rmc($this->rmc_num);
		$owner = new owner($rmc->owner_id);
		
		// Get intranet-based subsidairy_id
		$sql_s = "
		SELECT subsidiary_id 
		FROM subsidiary 
		WHERE 
		subsidiary_code <> '' AND 
		subsidiary_code IS NOT NULL AND 
		subsidiary_code = '".$this->subsidiary_code."'";
		$result_s = @mysql_query($sql_s, $UTILS_INTRANET_DB_LINK);
		$row_s = @mysql_fetch_row($result_s);
		$int_subsidiary_id = $row_s[0];
		
		// Get intranet-based owner_id
		$sql_o = "
		SELECT owner_id 
		FROM owner 
		WHERE 
		owner_ref <> '' AND 
		owner_ref IS NOT NULL AND 
		subsidiary_id = ".$int_subsidiary_id." AND 
		owner_ref = '".$owner->owner_ref."'";
		$result_o = @mysql_query($sql_o, $UTILS_INTRANET_DB_LINK);
		$row_o = @mysql_fetch_row($result_o);
		$int_owner_id = $row_o[0];
		
		// Get intranet-based director_id
		$sql_d = "
		SELECT director_id 
		FROM directors 
		WHERE 
		director_ref <> '' AND 
		director_ref IS NOT NULL AND 
		director_owner_id = ".$int_owner_id." AND 
		director_ref = '".$this->resident_ref."'";
		$result_d = @mysql_query($sql_d, $UTILS_INTRANET_DB_LINK);
		$row_d = @mysql_fetch_row($result_d);
		$int_director_id = $row_d[0];
		
		
		// Check for schedules that this Director can see (search Intranet db)
		$sql = "
		SELECT *, '".$int_director_id."' AS int_director_id, i.owner_id AS i_oid, r.owner_id AS r_oid  
		FROM ins_sched_types t, ins_sched i LEFT JOIN owner o ON (i.owner_id = o.owner_id) LEFT JOIN rmcs r ON (i.rmc_num = r.rmc_num) LEFT JOIN freeholders f ON (i.ins_sched_freeholder_id = f.freeholder_id )  
		WHERE 
		i.ins_sched_type_id = t.ins_sched_type_id AND 
		(i.owner_id = ".$int_owner_id." OR r.owner_id = ".$int_owner_id.") AND 
		i.ins_sched_reviewed = 'N' AND 
		i.ins_sched_discon = 'N' AND 
		i.ins_sched_is_visible = 'Y' AND 
		i.ins_sched_filename <> '' AND 
		i.ins_sched_filename IS NOT NULL 
		";		
		$result = @mysql_query($sql, $UTILS_INTRANET_DB_LINK);
		return $result;
	}

	
	// Gets the number of entries in the download table
	function get_num_sched_downloads($owner_id, $rmc_num, $ins_sched_id, $director_id){
		
		global $UTILS_INTRANET_DB_LINK;
		
		$sql = "
		SELECT count(*)  
		FROM ins_sched_download_log 
		WHERE 
		owner_id = ".$owner_id." AND 
		rmc_num = ".$rmc_num." AND 
		ins_sched_id = ".$ins_sched_id." AND 
		director_id = ".$director_id;
		$result = @mysql_query($sql, $UTILS_INTRANET_DB_LINK);
		$row = @mysql_fetch_row($result);
		return $row[0];
	}

	
	// Checks to see if a schedule has any outstanding credit (including any schedules that have already been downloaded but are still within the 48hr allowable download window)
	function get_available_sched_downloads($owner_id, $rmc_num, $ins_sched_id, $director_id){
		
		global $UTILS_INTRANET_DB_LINK;
		$download_window = time() - 172800;
		
		$sql = "
		SELECT * 
		FROM ins_sched_download_log 
		WHERE 
		owner_id = ".$owner_id." AND 
		rmc_num = ".$rmc_num." AND 
		ins_sched_id = ".$ins_sched_id." AND 
		director_id = ".$director_id." AND 
		(credit_type <> 'remove' OR credit_type IS NULL)AND 
		(
			(
				order_ref <> '' AND 
				(ins_sched_download_ts IS NULL OR ins_sched_download_ts = '' OR order_ts > ".$download_window." ) 
			)
			OR
			(
				(order_ref = '' OR order_ref IS NULL) AND 
				ins_sched_download_ts IS NOT NULL AND 
				ins_sched_download_ts <> '' AND 
				ins_sched_download_ts > ".$download_window." 
			)
		)";
		$result = @mysql_query($sql, $UTILS_INTRANET_DB_LINK);
		return $result;
	}
    function get_address($type="string"){
        $address = '';
        $address_arr[0] = $this->resident_address_1;
        $address_arr[1] = $this->resident_address_2;
        $address_arr[2] = $this->resident_address_3;
        $address_arr[3] = $this->resident_address_4;
        $address_arr[4] = $this->resident_address_city;
        $address_arr[5] = $this->resident_address_county;
        $address_arr[6] = $this->resident_address_postcode;
        $address_arr_final = array();
        foreach($address_arr as $address_item){
            $address_item = trim($address_item);
            if(!is_null($address_item)){
                if($address_item != ''){
                    $address .= stripslashes($address_item) . "<br/>";
                    $address_arr_final[] = stripslashes($address_item);
                }
            }
        }

        $address = substr($address, 0, - 5);

        if($type == "string"){
            return $address;
        }else{
            return $address_arr_final;
        }
    }
	

}

?>