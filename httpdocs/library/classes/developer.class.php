<?php
require_once($UTILS_CLASS_PATH."security.class.php");



class developer {
	
	var $developer_id;
	var $developer_name;
	var $developer_url;
	var $developer_dummy_rmc_num;
	var $developer_header_small_image_ext;
	var $rmc_name;

	function developer($ref=""){
		
		$sql = "SELECT *, d.developer_id 
		FROM 
		cpm_developers d 
		LEFT JOIN cpm_rmcs r ON (d.developer_dummy_rmc_num = r.rmc_num) 
		LEFT JOIN cpm_rmcs_extra rex ON (d.developer_dummy_rmc_num = rex.rmc_num) 
		LEFT JOIN cpm_lookup_rmcs lr ON (d.developer_dummy_rmc_num = lr.rmc_lookup) 
		WHERE  
		d.developer_id = ".$ref."
		";
		//print $sql;
		$result = @mysql_query($sql);
		$row = @mysql_fetch_array($result);
		
		$this->developer_id = $row['developer_id'];
		$this->developer_name = $row['developer_name'];
		$this->developer_url = $row['developer_url'];
		$this->developer_dummy_rmc_num = $row['developer_dummy_rmc_num'];
		$this->developer_header_small_image_ext = $row['developer_header_small_image_ext'];
		$this->rmc_name = $row['rmc_name'];
	}

	
	function check_fields($request){
		
		if( $request['developer_name'] == "" ){return "Please complete all required fields marked with (*).";}
		//if( $request['developer_dummy_rmc_name'] == "" ){return "Please complete all required fields marked with (*).";}
		
		return true;
	}
	
	
	function save($request, $files=""){
		
		global $UTILS_FILE_PATH;
		global $UTILS_SITE_VISITS_PATH;
		global $UTILS_MEETING_NOTES_PATH;
		$security = new security;
		
		// Insert record
		if( $this->developer_id == "" ){
			
			$sql = "
			INSERT INTO cpm_developers (
				developer_name,
				developer_url
			)
			VALUES(
				'".$security->clean_query($request['developer_name'])."',
				'".$request['developer_url']."'
			)
			";
			if( @mysql_query($sql) !== false ){
				
				// Get insert id
				$sql = "SELECT LAST_INSERT_ID() FROM cpm_developers";
				$result = @mysql_query($sql);
				$row = @mysql_fetch_row($result);
				$developer_id = $row[0];
				
				// Get the counter to be used in the rmc ref
				$sql = "SELECT * FROM cpm_system";
				$result = @mysql_query($sql);
				$row = @mysql_fetch_array($result);
				$rmc_counter = $row['developer_dummy_rmc_counter'];
				$new_rmc_counter = $row['developer_dummy_rmc_counter'] + 1;
				$sql = "UPDATE cpm_system SET developer_dummy_rmc_counter = ".$new_rmc_counter;
				@mysql_query($sql);
				
				// Insert lookup
				$sql = "
				INSERT INTO cpm_lookup_rmcs (
					subsidiary_id,
					rmc_ref,
					date_created
				)VALUES(
					1,
					'demo-".$rmc_counter."',
					'".date("H:i d/m/Y")."'
				)
				";
				if( @mysql_query($sql) !== false ){
					
					// Get insert id
					$sql = "SELECT LAST_INSERT_ID() FROM cpm_lookup_rmcs";
					$result = @mysql_query($sql);
					$row = @mysql_fetch_row($result);
					$lookup_id = $row[0];
		
					// Insert rmc record
					$sql = "
					INSERT INTO cpm_rmcs (
						subsidiary_id,
						rmc_num,
						rmc_name,
						rmc_status,
						rmc_is_active,
						rmc_address_1,
						rmc_address_city,
						rmc_address_postcode,
						property_manager,
						rmc_op_director_name,
						rmc_is_developer_dummy
					)VALUES(
						1,
						".$lookup_id.",
						'Acme Management Company Ltd.',
						'Active',
						'1',
						'Ridge Close',
						'London',
						'W1 7RJ',
						'Joe Privett',
						'Justin Herbert',
						'Y'
					)
					";
					@mysql_query($sql);
					
					$sql = "
					INSERT INTO cpm_rmcs_extra (
						rmc_num,
						pmt_id,
						member_id,
						developer_id,
						dev_description,
						is_demo_account,
						prevent_printing_of_letters
					)VALUES(
						".$lookup_id.",
						1,
						1,
						".$developer_id.",
						'',
						'Y',
						'Y'
					)
					";
					if( @mysql_query($sql) !== false ){
					
						
						// Add in a site visit, plus copy dummy file over
						$filename = "site_visit_".$lookup_id."_1332756000.pdf";
						copy($UTILS_FILE_PATH."demo/site_visit.pdf", $UTILS_SITE_VISITS_PATH.$filename);
						$sqls = "
						INSERT INTO cpm_site_visits (
							rmc_num,
							site_visit_file,
							site_visit_file_stamp,
							site_visit_date
						)VALUES(
							".$lookup_id.",
							'".$filename."',
							'1332756000',
							'20120326'
						)
						";
						@mysql_query($sqls);
						
						// Add in a meeting notes file, plus copy dummy file over
						$filename = "meeting_notes_".$lookup_id."_1332756000.pdf";
						copy($UTILS_FILE_PATH."demo/meeting_notes.pdf", $UTILS_MEETING_NOTES_PATH.$filename);
						$sqlm = "
						INSERT INTO cpm_agm (
							rmc_num,
							agm_file,
							agm_file_stamp,
							agm_date,
							agm_year,
							meeting_type_id
						)VALUES(
							".$lookup_id.",
							'".$filename."',
							'1332756000',
							'20120326',
							'2012',
							2
						)
						";
						@mysql_query($sqlm);
				
				
						$sql = "
						UPDATE cpm_developers SET
						developer_dummy_rmc_num = ".$lookup_id."
						WHERE developer_id = ".$developer_id."
						";
						@mysql_query($sql);
						
						// re-populate this object
						$this->developer($developer_id);
						
						// Create images directory
						$img_dir = $UTILS_FILE_PATH."images/developers/".$developer_id;
						@mkdir($img_dir);
						
						// Image file extensions
						$file_ext[] = "jpg";
						$file_ext[] = "gif";
						$file_ext[] = "png";
						$file_ext_str = "";
						
						// Save image
						if($files['developer_header_image']['name'] != ""){
							if(check_file_extension($files['developer_header_image']['name'], $file_ext)){				
								
								if(preg_match("/\.jpg/i", $files['developer_header_image']['name']) === 1){
									$header_image_path = $UTILS_FILE_PATH."images/developers/".$developer->developer_id."/header_small_".$developer->developer_id.".jpg";
									$file_ext_str = "jpg";
								}
								elseif(preg_match("/\.jpeg/i", $files['developer_header_image']['name']) === 1){
									$header_image_path = $UTILS_FILE_PATH."images/developers/".$developer->developer_id."/header_small_".$developer->developer_id.".jpeg";
									$file_ext_str = "jpeg";
								}
								elseif(preg_match("/\.gif/i", $files['developer_header_image']['name']) === 1){
									$header_image_path = $UTILS_FILE_PATH."images/developers/".$developer->developer_id."/header_small_".$developer->developer_id.".gif";
									$file_ext_str = "gif";
								}
								copy($files['developer_header_image']['tmp_name'], $header_image_path);
							}
							else{
								return "Technical fault - some image could not be uploaded (1).";
							}
						}
						
						// If 'general' image(s) being uploaded, check file extensions
						if($files['developer_general_image']['name'] != ""){
							if(check_file_extension($files['developer_general_image']['name'], $file_ext)){
								$general_image_path = $UTILS_FILE_PATH."images/developers/".$developer_id."/general_".$developer_id.".jpg";
								copy($files['developer_general_image']['tmp_name'], $general_image_path);
							}
							else{
								return "Technical fault - some image could not be uploaded (2).";
							}
						}
						
						$sql = "
						UPDATE cpm_developers SET
						developer_header_small_image_ext = '".$file_ext_str."'
						WHERE developer_id = ".$developer_id."
						";
						@mysql_query($sql);
						
						return true;
					}
				}
		
			}
			
		}
		
		// Update record
		else{
			
			// If 'header' image(s) being uploaded, check file extensions
			$swf_ext[] = "swf";
			if($files['developer_header_swf']['name'] != ""){
				if(check_file_extension($files['developer_header_swf']['name'], $swf_ext)){
					$header_swf_path = $UTILS_FILE_PATH."images/developers/".$this->developer_id."/header_".$this->developer_id.".swf";
					copy($files['developer_header_swf']['tmp_name'], $header_swf_path);
				}
				else{
					return "Technical fault - some image could not be uploaded (3).";
				}
			}
			
			// Image extensions allowed
			$file_ext[] = "jpg";
			$file_ext[] = "gif";
			$file_ext[] = "png";
			$file_ext_str = "";
			
			// If 'header' image(s) being uploaded, check file extensions
			if($files['developer_header_image']['name'] != ""){
				if(check_file_extension($files['developer_header_image']['name'], $file_ext)){
					
					@mkdir($UTILS_FILE_PATH."images/developers/".$this->developer_id);
					if(preg_match("/\.jpg/i", $files['developer_header_image']['name']) === 1){
						$header_image_path = $UTILS_FILE_PATH."images/developers/".$this->developer_id."/header_small_".$this->developer_id.".jpg";
						$file_ext_str = "jpg";
					}
					elseif(preg_match("/\.jpeg/i", $files['developer_header_image']['name']) === 1){
						$header_image_path = $UTILS_FILE_PATH."images/developers/".$this->developer_id."/header_small_".$this->developer_id.".jpeg";
						$file_ext_str = "jpeg";
					}
					elseif(preg_match("/\.gif/i", $files['developer_header_image']['name']) === 1){
						$header_image_path = $UTILS_FILE_PATH."images/developers/".$this->developer_id."/header_small_".$this->developer_id.".gif";
						$file_ext_str = "gif";
					}
					copy($files['developer_header_image']['tmp_name'], $header_image_path);
				}
				else{
					return "Technical fault - some image could not be uploaded (4).";
				}
			}
			
			// If 'general' image(s) being uploaded, check file extensions
			if($files['developer_general_image']['name'] != ""){
				if(check_file_extension($files['developer_general_image']['name'], $file_ext)){
					$general_image_path = $UTILS_FILE_PATH."images/developers/".$this->developer_id."/general_".$this->developer_id.".jpg";
					copy($files['developer_general_image']['tmp_name'], $general_image_path);
				}
				else{
					return "Technical fault - some image could not be uploaded (5).";
				}
			}
			
			
			// Set-up dummy Man. Co.
			if($request['developer_dummy_rmc_name'] != ""){
				
				if($this->developer_dummy_rmc_num == ""){
					
					$sql_system = "SELECT * FROM cpm_system";
					$result_system = @mysql_query($sql_system);
					$row_system = @mysql_fetch_array($result_system);
					
					$rmc_counter = $row_system['developer_dummy_rmc_counter'];
					$new_rmc_counter = $row_system['developer_dummy_rmc_counter'] + 1;
					
					$sql_system = "UPDATE cpm_system SET developer_dummy_rmc_counter = ".$new_rmc_counter;
					$result_system = @mysql_query($sql_system);
					
					$sql_lrmc = "
					INSERT INTO cpm_lookup_rmcs (
						subsidiary_id,
						rmc_ref,
						date_created
					)VALUES(
						1,
						'demo-".$rmc_counter."',
						'".date("H:i d/m/Y")."'
					)
					";
					@mysql_query($sql_lrmc);
					
					$sql_id = "SELECT LAST_INSERT_ID() FROM cpm_lookup_rmcs";
					$result_id = @mysql_query($sql_id);
					$row_id = @mysql_fetch_row($result_id);
					$lookup_id = $row_id[0];
					
					$sql_rmc = "
					INSERT INTO cpm_rmcs (
						subsidiary_id,
						rmc_num,
						rmc_name,
						rmc_status,
						rmc_is_active,
						rmc_address_1,
						rmc_address_city,
						rmc_address_postcode,
						property_manager,
						rmc_op_director_name,
						rmc_is_developer_dummy
					)VALUES(
						1,
						".$lookup_id.",
						'Acme Management Company Ltd.',
						'Active',
						'1',
						'Ridge Close',
						'London',
						'W1 7RJ',
						'Joe Privett',
						'Justin Herbert',
						'Y'
					)
					";
					@mysql_query($sql_rmc);
					
					$sql_rmcx = "
					INSERT INTO cpm_rmcs_extra (
						rmc_num,
						pmt_id,
						member_id,
						developer_id,
						dev_description,
						carpark_file,
						carpark_file_stamp,
						is_demo_account
					)VALUES(
						".$lookup_id.",
						1,
						1,
						".$this->developer_id.",
						'This prestigious development consists of 122 split-level flats situated on basement, ground and 3 upper floors with underground parking. Constructed on behalf of ".$security->clean_query($request['developer_name'])." and completed in 1980. Located on the banks of the River Thames the development is within walking distnace bus, rail and tube links.',
						'carpark_9999999_1138019653.pdf',
						'1138019653',
						'Y'
					)
					";
					@mysql_query($sql_rmcx);
					
				}
				else{
					
					$sql = "
					UPDATE cpm_lookup_rmcs SET
						rmc_ref = '".$security->clean_query($request['developer_dummy_rmc_name'])."'
					WHERE rmc_lookup = ".$this->developer_dummy_rmc_num."
					";
					@mysql_query($sql);
				}
			}
			
			$sql = "
			UPDATE cpm_developers SET
			developer_name = '".$security->clean_query($request['developer_name'])."',
			developer_url = '".$security->clean_query($request['developer_url'])."',
			developer_header_small_image_ext = '".$file_ext_str."'
			WHERE developer_id = ".$this->developer_id;
			@mysql_query($sql);
			
			return true;
			
		}
	
		return "Technical fault - this developer could not be saved.";
	}
	
	
	function delete(){
			
		global $UTILS_FILE_PATH;
		
		$img_dir = $UTILS_FILE_PATH."images/developers/".$this->developer_id;
	
		$sql = "SELECT resident_num FROM cpm_residents_extra WHERE developer_id = ".$this->developer_id;
		$result = @mysql_query($sql);
		while($row = @mysql_fetch_row($result)){
		
			$sql = "DELETE FROM cpm_residents WHERE resident_num = ".$row[0];
			mysql_query($sql);
			
			$sql = "DELETE FROM cpm_residents_extra WHERE is_developer='Y' AND resident_num = ".$row[0];
			mysql_query($sql);
			
			$sql = "DELETE FROM cpm_units WHERE resident_num = ".$row[0];
			mysql_query($sql);
			
			$sql = "DELETE FROM cpm_stats WHERE resident_num = ".$row[0];
			mysql_query($sql);
			
			$sql = "DELETE FROM cpm_lookup_residents WHERE resident_lookup = ".$row[0];
			mysql_query($sql);
	
		}
		
		$sql_sv = "SELECT site_visit_file FROM cpm_site_visits WHERE rmc_num = ".$this->developer_dummy_rmc_num;
		$result_sv = @mysql_query($sql_sv);
		$row_sv = @mysql_fetch_row($result_sv);
		@unlink($UTILS_SITE_VISITS_PATH.$row_sv[0]);
		$sql = "DELETE FROM cpm_site_visits WHERE rmc_num = ".$this->developer_dummy_rmc_num;
		@mysql_query($sql);
		
		$sql = "DELETE FROM cpm_rmcs_extra WHERE rmc_num = ".$this->developer_dummy_rmc_num;
		mysql_query($sql);
		
		$sql = "DELETE FROM cpm_rmcs WHERE rmc_num = ".$this->developer_dummy_rmc_num;
		mysql_query($sql);
		
		$sql = "DELETE FROM cpm_lookup_rmcs WHERE rmc_lookup = ".$this->developer_dummy_rmc_num;
		mysql_query($sql);
		
		$sql = "DELETE FROM cpm_developers WHERE developer_id = ".$this->developer_id;
		if( @mysql_query($sql) === false ){
			return "Technical fault - this developer could not be deleted.";
		}
		
		$this->remove_directory($UTILS_FILE_PATH."images/developers/".$this->developer_id);
		$this->developer("");
		
		return true;
	}
		
	
	function remove_directory($dir) {
					
		if( $handle = @opendir("$dir") ){
			
			while( false !== ($item = readdir($handle)) ){
				
				if($item != "." && $item != ".."){
					
					if (is_dir($dir."/".$item)) {
						$this->remove_directory($dir."/".$item);
					}
					else{
						@unlink($dir."/".$item);
					}
				}
			}
			closedir($handle);
			@rmdir($dir);
		}
	}
	
	
	
}


?>