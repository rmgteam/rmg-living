<script language="javascript">
glossary_id = new Array();
glossary_term = new Array();
glossary_text = new Array();

function glossary_showhide(term, showhide, e){
	for(i=0;i<glossary_term.length;i++){
		if(glossary_term[i] == term){
			if(showhide == "show"){
				show('glossary_layer_' + glossary_id[i], e);
			}
			else{
				hide('glossary_layer_' + glossary_id[i]);
			}
			return true;
		}
	}
	return false;
}

function show(object,e) {
    if (e != '') {
        if (document.all) {
            x = e.clientX;
            y = e.clientY;
        }
        if (document.layers) {
            x = e.pageX;
            y = e.pageY;
        }
    }

    if (document.layers && document.layers[object] != null) {
        document.layers[object].left = x;
        document.layers[object].top = y;
    }
    else if (document.all) {
        document.all[object].style.posLeft = x;
        document.all[object].style.posTop = y;
    }

    if (document.layers && document.layers[object] != null)
        document.layers[object].visibility = 'visible';
    else if (document.all)
        document.all[object].style.visibility = 'visible';
}

function hide(object) {
    if (document.layers && document.layers[object] != null)
        document.layers[object].visibility = 'hidden';
    else if (document.all)
        document.all[object].style.visibility = 'hidden';
}
</script>
<?
// Gets all terms to do with 'Finance'
$sql = "SELECT * FROM cpm_glossary WHERE glossary_term = 'Bank & Cash' AND glossary_term_category = 'Finance'";
$result = @mysql_query($sql);
$arr_count = 0;
while($row = @mysql_fetch_array($result)){?>
<script language="javascript">
glossary_id[<?=$arr_count?>] = "<?=$row['glossary_id']?>";
glossary_term[<?=$arr_count?>] = "<?=addslashes(trim($row['glossary_term']))?>";
</script>
<span id="glossary_layer_<?=$row['glossary_id']?>" style="position:absolute; visibility: hidden;" onMouseOver="this.style.visibility='visible'" onMouseOut="this.style.visibility='hidden'"><table width="400" bgcolor="#ffffcc" cellpadding="10" style="border:1px solid #666666;"><tr><td><a onClick="document.getElementById('glossary_layer_<?=$row['glossary_id']?>').style.visibility='hidden';" class="link036" style="cursor:hand;">Close Window</a></td></tr><tr><td valign="top"><b><span class="subt036"><?=$row['glossary_term']?></span></b><br><?=nl2br(trim($row['glossary_text']))?></td></tr></table></span>
<?
$arr_count++;
}
?>


