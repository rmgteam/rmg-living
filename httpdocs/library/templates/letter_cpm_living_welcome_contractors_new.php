<p><b>Make life easier through RMG Suppliers!</b><br/></p>
	<p>Welcome to RMG Suppiers, an online portal. RMG is always looking for ways to improve the service we offer to our customers and would like to take this opportunity to introduce the contractors portal on RMG Suppliers.<br/></p>
	<p>From today, you can use RMG Suppliers to:</p>
	<ul>
		<li>Manage multiple users to use your account</li>
		<li>Manage your Purchase Orders</li>
	</ul>
	<p>The following login details are specific for the master account of your company. To access your account visit <b>www.rmgsuppliers.co.uk</b> and use the details below:
        <br/></p>
	<table cellpadding="0" cellspacing="0" border="0">
		<tr nowrap class="letter">
			<td><p><b>Login Id:</b>     <?=$contractors->contractor_qube_id?>          <b>Password:</b>     <?=$password?></p></td>
		</tr>
	</table>
	<p><br/>The first time you login you will asked to set your own password. If you forget your password you can use the password reminder facility on the homepage of the website. Please retain this letter as a reference of your Contractor ID.</p>
	<p><br/>This is the first of many steps which forms part of our continued drive to improve the service we deliver to all of our customers. We hope you enjoy the current features and would be very interested to hear of other features that you think may be of benefit to you. Please forward any ideas or general feedback on RMG Suppliers to <b>customerservice@rmguk.com</b></p>
