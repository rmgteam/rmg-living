<div style="margin:0; padding:0;" class="letter">
	<p style="font-weight:bold;text-decoration:underline;">Make life easier through RMG Suppliers!</p>
	<p>Welcome to RMG Suppiers, an online portal. RMG is always looking for ways to improve the service we offer to our customers and would like to take this opportunity to introduce the contractors portal on RMG Suppliers.</p>
	<p>From today, you can use RMG Suppliers to:</p>
	<ul>
		<li><span>Manage multiple users to use your account</span></li>
		<li><span>Manage your Purchase Orders</span></li>
	</ul>
	<p>The following login details are specific for the master account of your company. To access your account visit www.rmgsuppliers.co.uk and use the details below:</p>
	<table cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td nowrap class="letter"><strong>Login Id:</strong></td>
			<td class="letter"><?=$contractors->contractor_qube_id?></td>
		</tr>
		<tr>
			<td class="letter"><strong>Password:</strong></td>
			<td class="letter">
			<?=$password?>
			</td>
		</tr>
	</table>
	<p>The first time you login you will asked to set your own password. If you forget your password you can use the password reminder facility on the homepage of the website. Please retain this letter as a reference of your Contractor ID.</p>
	<p>This is the first of many steps which forms part of our continued drive to improve the service we deliver to all of our customers. We hope you enjoy the current features and would be very interested to hear of other features that you think may be of benefit to you. Please forward any ideas or general feedback on RMG Suppliers to customerservice@rmguk.com</p>
</div>