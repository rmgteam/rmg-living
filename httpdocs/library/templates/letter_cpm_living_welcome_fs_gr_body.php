<p><b>Please Quote <?=$resident->resident_ref?> on future ground rent correspondence</b></p>
<p>I would like to take this opportunity to advise you of a change to your account.<br/></p>
<p>The ground rent charge has been separated from your service charge account and now has its own reference number. This is a necessary requirement to allow us to improve our efficiency and the service we provide to our customers.<br/></p>
<p>To access your online account, RMG Living, you will need to refer to the attached letter that provides you with your log in details. The site also allows you to raise an issue, view your account information and to make secure payments online.<br/></p>
<p>Alternatively, payment can be made by calling our offices on 023 8022 6686 but please be aware that card charges may apply if paying by credit card. You are also able to make payment via cheque, quoting your tenant reference number (above) and the description of your property '<?=$unit_label?>' on the back.<br/></p>
<p>Alternatively, you can pay by bank transfer using the details below:<br/></p>
<ul>
	<li>Account Name: F &amp; S Property Management Ltd </li>
	<li>Sort Code: 12-27-25</li>
	<li>Account Number: 10495768</li>
</ul>
<p>Payment reference:  <b><?=$resident->resident_ref?></b> - This must be used as without this reference we will not be able to allocate your payment and you may incur further charges.</p>
