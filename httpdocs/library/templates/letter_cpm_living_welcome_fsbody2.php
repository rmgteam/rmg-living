<p><b>Payment Options:</b><br/></p>
<ul>
	<li><b>Direct Debit: </b>Please contact us on 023 8022 6686 to set up a direct debit and have your new customer reference number ready.</li>
	<li><b>On line payment: </b> Payment can be made through the RMG Living website www.rmgliving.co.uk. You can find your login details in the attached letter.</li>
	<li><b>Credit or debit card:: </b> Please contact us on 023 8022 6686, quoting your new customer reference number to pay via debit or credit card. Please note that charges apply if using a credit card.</li>
	<li><b>Cheque: </b> Make your cheques payable to 'F&S Property Management Ltd' and send to:<br/>F&S Property Management Ltd,<br/>9 Carlton Crescent,<br/>Southampton,<br/>SO15<br/>Please ensure that you quote your new customer reference on the reverse of your cheque.</li>
	<li><b>Standing Order: </b> You will need to set this up with your bank directly. If you currently have a standing order set up, you will need to amend the details as given below. The account details to provide are:<br/>Sort Code: 12-27-25<br/>Account Number 10495768<br/>Your new customer reference number should be provided as the standing order reference to appear on our bank statement so that your payment can be correctly allocated to your account.</li>
	<li><b>Electronic Transfer: </b> The account details you will require are as follows:<br/></li></ul>
<table cellpadding="0" cellspacing="0" border="0">
	<tr nowrap class="letter">
		<td><p><b>Sort Code:</b>      12-27-25              <b>Account Number:</b>     10495768</p></td>
	</tr>
</table>
<p><br/>Your new customer reference number should be provided as the transfer reference to appear on our bank statement so that your payment can be correctly allocated to your account.<br/></p>