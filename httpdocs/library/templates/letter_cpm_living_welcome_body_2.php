<p>Please find below your login details for the RMG Living website. To access your account for:</p>
<p><? if($unit_label != ""){print "<b>Property:</b> ".$unit_label;}else{print "<b>Property:</b> ".$unit_label;}?><br><?="<b>Management Company:</b> ".$rmc->rmc['rmc_name'];?></p>
<p> visit www.rmgliving.co.uk and enter the following details as they appear in this letter:</p>
<table>
	<tr>
	<td nowrap class="letter">Login ID:</td>
	<td class="letter"><strong><?=$resident->resident_ref?></strong></td>
	</tr>
	<tr>
	<td class="letter">Password:</td>
	<td class="letter"><strong><? if($_REQUEST['is_print_history'] == "Y" && $resident->is_first_login == "N"){print "********";}else{print $resident->get_password($resident->resident_num);}?></strong></td>
	</tr>
</table>
<p>When you log in you will be asked to reset your password. If you forget your password you will can use the password reminder facility on the home page of the website. It is important that you do not disclose your password to anyone else.</p>
<p>We are continually looking for ways to expand the RMG Living website to include features that will benefit our customers and welcome your feedback, either through letter or by email, about the site and any additions to the service that you would find useful.</p>