<p style="font-weight:bold;text-decoration:underline;"><b>Make life easier through RMG Living!</b><br/></p>
<p>Welcome to RMG Living, your online portal to account management, development news and much more. <? if( $rmc->region != 'FAS' ) {?>RMG<?}else{?>F&S Property Management Ltd<?}?> is always looking for ways to improve the service we offer to our customers and would like to take this opportunity to introduce you to RMG Living.<br/></p>
<p>RMG Living is an online service which allows you to manage your account at the touch of a button. From today, you can use your online portal to:<br/></p>
	<ul>
		<li>View account statement real-time, fully itemised with payment history.</li>
		<li>Make Payments - for Service Charge and Ground Rent (if applicable).</li>
		<li>Report Issues - e.g. Repairs and Maintenance.</li>
		<li>Real-time Online Communication, 'Chat Now'.</li>
		<li>See Announcements about your Development.</li>
		<li>Download Insurance Schedules.</li>
		<li>Access Advice and Frequently Asked Questions.</li>
		<li>Update your contact details.</li>
	</ul>
	<p>The following login details are specific to the property '<?=$unit_label?>'. To access your account please visit <b>www.rmgliving.co.uk</b> and use the details below:<br/></p>
	<table cellpadding="0" cellspacing="0" border="0">
		<tr nowrap class="letter">
			<td><p><b>Login Id:</b>      <?=$resident->resident_ref?>              <b>Password:</b>     <? if($_REQUEST['is_print_history'] == "Y" && $resident->is_first_login == "N"){print "********";}else{print $resident->get_password($resident->resident_num);}?></p></td>
		</tr>
	</table>
	<p><br/>The first time you login you will asked to set your own password. If you forget your password you can use the password reminder facility on the homepage of the website. Please retain this letter as a reference of your Lessee ID.<br/></p>
	<p><br/><br/>Over the following months the service will be developed with new and exciting features:<br/></p>
	<ul>
		<li>Site Visit Reports - Access to inspections and assessments carried out by RMG.</li>
		<li>Newsletters - Keeping you up-to-date with what's happening at your development.</li>
		<li>Meeting Information &amp; Minutes, Budgets and much more...</li>
	</ul>
	<p>This is the first of many steps which forms part of our continued drive to improve the service we deliver to our customers. We hope you enjoy the current features and would be very interested to hear of other features that you think may be of benefit to you. Please forward any ideas or general feedback on RMG Living to <b><? if( $rmc->region == 'FAS' ) {?>customerservice@fspropertymanagement.co.uk<? }else{?>customerservice@rmguk.com<?}?></b></p>
