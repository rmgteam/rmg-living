<div style="margin:0; padding:0;" class="letter">

	<p style="font-weight:bold;text-decoration:underline;">Make life easier through RMG Living!</p>
	<p>In recent Focus Newsletters from our Managing Director we have spoken about keeping you informed of changes within our company and our commitment to improve communication between us.</p>
	<p>RMG are pleased to announce that RMG Living 'Financial Reporting' is now live!</p>
	<p>We hope you will find that this service will provide you with an improved awareness of the financial position of your Management Company. The Financial reports available to you will provide details of current service charge debtors, creditors and the cash position, at any point in time, rather than waiting for hard copy financial reports.</p>
	<p>Your access details can be found below and should remain confidential to you in order to avoid any breach of Data Protection legislation. Should you resign as a Director in the future all access rights will be removed.</p>
	<table cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td nowrap class="letter"><strong>Login Id:</strong></td>
			<td class="letter"><?=$resident->resident_ref?></td>
		</tr>
		<tr>
			<td class="letter"><strong>Password:</strong></td>
			<td class="letter"><? if($_REQUEST['is_print_history'] == "Y" && $resident->is_first_login == "N"){print "********";}else{print $resident->get_password($resident->resident_num);}?></td>
		</tr>
	</table>
	<p>Clearly, we would be more than happy to provide cash flow analysis in addition to this snap shot financial picture and would kindly ask you to contact your Property Manager directly to request any additional financial information that you may require.</p>
	<p>We trust that you will find this new tool of benefit in providing added transparency to the services provided by RMG.</p>
	<p>Should you experience difficulties with gaining access to the reports please do not hesitate to contact your Property Manager who will escalate the matter for resolution.</p>

</div>