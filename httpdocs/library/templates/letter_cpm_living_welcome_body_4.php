<div style="margin:0; padding:0;" class="letter">
	<p style="font-weight:bold;text-decoration:underline;">Make life easier through RMG Living!</p>
	<p>Welcome to RMG Living, an online portal to account management, development news and much more. RMG is always looking for ways to improve the service we offer to our customers and would like to take this opportunity to introduce the first phase of RMG Living.</p>
	<p>RMG Living is an online service which allows the owner of the property to manage their account at the touch of a button. Working closely with the developer of your building, Urban Splash, RMG are now happy to extend some elements of this service to tenants.</p>
	<p>From today, you can use RMG Living to:</p>
	<ul>
		<li><span>Report an Issue - e.g. Repairs and Maintenance.</span></li>
		<li><span>Access Advice and Frequently Asked Questions.</span></li>
		<li><span>Update your contact details.</span></li>
	</ul>
	<p>The following login details are specific to the property '<?=$unit_label?>'. To access your account visit www.rmgliving.co.uk and use the details below:</p>
	<table cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td nowrap class="letter"><strong>Login Id:</strong></td>
			<td class="letter"><?=$resident->resident_ref?></td>
		</tr>
		<tr>
			<td class="letter"><strong>Password:</strong></td>
			<td class="letter"><? if($_REQUEST['is_print_history'] == "Y" && $resident->is_first_login == "N"){print "********";}else{print $resident->get_password($resident->resident_num);}?></td>
		</tr>
	</table>
	<p>The first time you login you will asked to set your own password. If you forget your password you can use the password reminder facility on the homepage of the website. Please retain this letter as a reference of your Tenant ID.</p>
	<p>Over the following months the service will be developed with new and exciting features:</p>
	<ul>
		<li><span>RMG Rewards - Residents Benefit Scheme offering you great deals and discounts.</span></li>
		<li><span>Newsletters - Keeping you up-to-date with what's happening at your development.</span></li>
		<li><span>Announcements, Meeting Information &amp; Minutes, Budgets and much more...</span></li>
	</ul>
	<p>This is the first of many steps which forms part of our continued drive to improve the service we deliver to all of our customers. We hope you enjoy the current features and would be very interested to hear of other features that you think may be of benefit to you. Please forward any ideas or general feedback on RMG Living to customerservice@rmguk.com</p>
</div>