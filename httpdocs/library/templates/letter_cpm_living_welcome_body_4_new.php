<p style="font-weight:bold;text-decoration:underline;"><b>Make life easier through RMG Living!</b><br/></p>
	<p>Welcome to RMG Living, an online portal to account management, development news and much more. RMG is always looking for ways to improve the service we offer to our customers and would like to take this opportunity to introduce the first phase of RMG Living.<br/></p>
	<p>RMG Living is an online service which allows the owner of the property to manage their account at the touch of a button. Working closely with the developer of your building, Urban Splash, RMG are now happy to extend some elements of this service to tenants.<br/></p>
	<p>From today, you can use RMG Living to:<br/></p>
	<ul>
		<li>Report an Issue - e.g. Repairs and Maintenance.</li>
		<li>Access Advice and Frequently Asked Questions.</li>
		<li>Update your contact details.</li>
	</ul>
	<p>The following login details are specific to the property '<?=$unit_label?>'. To access your account visit <b>www.rmgliving.co.uk</b> and use the details below:<br/></p>
	<table cellpadding="0" cellspacing="0" border="0">
        <tr nowrap class="letter">
            <td><p><b>Login Id:</b>      <?=$resident->resident_ref?>              <b>Password:</b>     <? if($_REQUEST['is_print_history'] == "Y" && $resident->is_first_login == "N"){print "********";}else{print $resident->get_password($resident->resident_num);}?></p></td>
        </tr>
	</table>
	<p><br/>The first time you login you will asked to set your own password. If you forget your password you can use the password reminder facility on the homepage of the website. Please retain this letter as a reference of your Tenant ID.<br/></p>
	<p><br/><br/><br/><br/><br/>Over the following months the service will be developed with new and exciting features:<br/></p>
	<ul>
		<li>Newsletters - Keeping you up-to-date with what's happening at your development.</li>
		<li>Announcements, Meeting Information &amp; Minutes, Budgets and much more...</li>
	</ul>
	<p>This is the first of many steps which forms part of our continued drive to improve the service we deliver to all of our customers. We hope you enjoy the current features and would be very interested to hear of other features that you think may be of benefit to you. Please forward any ideas or general feedback on RMG Living to <b>customerservice@rmguk.com</b></p>
