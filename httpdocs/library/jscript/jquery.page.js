(function($) {
	$.paging = {
		defaults: {
			objName: '',
			pageName: ''
		}
	};

	$.extend($.fn, {
		paging:function(options) {
			
			//use defaults or properties supplied by user
			var config = $.extend({}, $.paging.defaults, options);
			
			config.objName = $(this).attr("id");
			
			$('#' + config.objName).find('li').each(function() {
				$(this).hide();
			});	
			
			$('#' + config.objName).find('li:first').show();
			
			$.fn.paging.options = config;
		},
		setPage:function(id){
			$('#' + $.fn.paging.options.objName).find('li').each(function() {
				$(this).hide();
			});	
			
			$('#' + $.fn.paging.options.objName).find('li').each(function() {
				if ($(this).attr("id") == "page-" + id){
					$(this).show();
				}
			});	
			
			if($.fn.paging.options.pageName != ""){
				$('#' + $.fn.paging.options.pageName).find('li').each(function() {
					$(this).removeClass("active");
				});
				
				$('#' + $.fn.paging.options.pageName).find('li').each(function() {
					if ($(this).attr("id") == id){
						$(this).addClass("active");
					}
				});
			}
		}
	});
})(jQuery);