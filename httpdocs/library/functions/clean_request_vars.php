<?
// This function is used to clean the Request, Get or Post global arrays
function clean_request_vars($input_value){
	
	if(is_array($input_value)){
		foreach( $input_value as $key=>$val ){
			if( is_array($input_value[$key]) ){
				foreach( $val as $key2=>$val2 ){
					$input_value[$key][$key2] = clean_query($val2);
				}
			}
			else{
				$input_value[$key] = clean_query($val);
			}
		}
	}
	else{
		$input_value = clean_query($input_value);
	}
	return $input_value;
}


// Actually removes necessary characters / keywords from input variable
function clean_query($string){

	if(get_magic_quotes_gpc()){
		$string = stripslashes($string);
	}
	if (phpversion() >= '4.3.0'){
		$string = mysql_real_escape_string($string);
	}
	else{
		$string = mysql_escape_string($string);
	}
	
	$string = str_replace(";", "", $string);
	
	//$bad_words = "/(delete)|(update)|(union)|(insert)|(drop)|(http)|(--)/";
	//$bad_words = "/(union)|(insert)|(drop)|(http)|(--)/";
	//$string = preg_replace($bad_words, "", $string);
	
	return $string;
}


$_REQUEST = clean_request_vars($_REQUEST);
$_GET = clean_request_vars($_GET);
$_POST = clean_request_vars($_POST);

?>