<?
// Returns size of given file with suffix notation
function get_file_size($file_path, $suffix=true){
	if(!file_exists($file_path)){
		return false;
	}
	else{
		$file_size = ceil(filesize($file_path)/1000);
		if($file_size > 1000){
			$file_size = $file_size/1000;
			$file_size = number_format($file_size,2);
			if($suffix){
				return $file_size." Mb";
			}else{
				return $file_size;
			}
		}
		else{
			if($suffix){
				return $file_size." Kb";
			}else{
				return $file_size;
			}
		}
	}
}	

?>