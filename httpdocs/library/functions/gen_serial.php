<?

function gen_serial($length, $type="", $case=""){

	 $rand_alpha_value[] = "a";
	 $rand_alpha_value[] = "b";
	 $rand_alpha_value[] = "c";
	 $rand_alpha_value[] = "d";
	 $rand_alpha_value[] = "e";
	 $rand_alpha_value[] = "f";
	 $rand_alpha_value[] = "g";
	 $rand_alpha_value[] = "h";
	 $rand_alpha_value[] = "j";
	 $rand_alpha_value[] = "k";
	 $rand_alpha_value[] = "m";
	 $rand_alpha_value[] = "n";
	 $rand_alpha_value[] = "p";
	 $rand_alpha_value[] = "q";
	 $rand_alpha_value[] = "r";
	 $rand_alpha_value[] = "t";
	 $rand_alpha_value[] = "u";
	 $rand_alpha_value[] = "v";
	 $rand_alpha_value[] = "w";
	 $rand_alpha_value[] = "x";
	 $rand_alpha_value[] = "y";
	 $rand_numeric_value[] = "2";
	 $rand_numeric_value[] = "3";
	 $rand_numeric_value[] = "4";
	 $rand_numeric_value[] = "6";
	 $rand_numeric_value[] = "7";
	 $rand_numeric_value[] = "8";
	 $rand_numeric_value[] = "9";
	 
	 if($type == "ALPHA"){
	 	$rand_value = $rand_alpha_value;
	 }
	 elseif($type == "NUMERIC"){
	 	$rand_value = $rand_numeric_value;
	 }
	 else{
	 	$rand_value = array_merge ($rand_alpha_value, $rand_numeric_value);
	 }

  	if($length>0){ 
		$rand_id="";
		for($i=1; $i<=$length; $i++){
			mt_srand((double)microtime() * 1000000);
			$num = mt_rand(1, count($rand_value) );
			$rand_id .= $rand_value[$num-1];
		}
  	}
	
	if($case == "UPPER"){
		return strtoupper($rand_id);
	}
	elseif($case == "LOWER"){
		return strtolower($rand_id);
	}
	else{
		return $rand_id;
	}
}

?>