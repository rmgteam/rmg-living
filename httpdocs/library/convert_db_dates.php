<?
// Coverts database dates from YYYYMMDD to DD/MM/YYYY format
function convert_date_from_db_1($date){
	$new_date = substr($date, 6, 2)."/".substr($date, 4, 2)."/".substr($date, 0, 4);
	return $new_date;
}

// Coverts database dates from YYYYMMDD to DD/MM/YY format
function convert_date_from_db_2($date){
	$new_date = substr($date, 6, 2)."/".substr($date, 4, 2)."/".substr($date, 2, 2);
	return $new_date;
}
?>