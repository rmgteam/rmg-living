<?
require("utils.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_FILE_PATH."management/gen_password.php");
require_once($UTILS_CLASS_PATH."encryption.class.php");
require_once($UTILS_CLASS_PATH."field.class.php");
require_once($UTILS_CLASS_PATH."contractors.class.php");
$website = new website;
$crypt = new encryption_class;
$field = new field;
$contractors = new contractors($_SESSION['contractors_qube_id']);

// Determine if allowed access into content management system
$website->allow_contractor_access();


#===================================
# Get users
#===================================

if($_REQUEST['which_action'] == "edit"){
	$user_id = $_REQUEST['user_id'];
	
	$result = $contractors->get_user($user_id);
	$num_rows = @mysql_num_rows($result);

	if($num_rows > 0){
		$result_array['success'] = 'Y';
		while($row = @mysql_fetch_array($result)){
			$result_array['name'] = $row['cpm_contractors_user_name'];
			$result_array['email'] = $row['cpm_contractors_user_email'];
			$result_array['disabled'] = strtolower($row['cpm_contractors_user_disabled']);
		}
	}else{
		$result_array['success'] = 'N';	
	}
	
	echo json_encode($result_array);
	exit;
}

#===================================
# Get users
#===================================

if($_REQUEST['which_action'] == "get"){
	
	$result = $contractors->get_users($_SESSION['contractors_qube_id']);
	$num_rows = @mysql_num_rows($result);
	$output = "";

	if($num_rows > 0){
		$result_array['success'] = 'Y';
		while($row = @mysql_fetch_array($result)){
			$output .= '<a class="form_row" href="javascript:edit_user('."'".$row['cpm_contractors_user_ref']."'".');">';
			$output .= '<div class="form_label border" style="width:110px;">'.$row['cpm_contractors_user_ref'].'</div>';
			$output .= '<div class="form_label border" style="width:285px;">'.$row['cpm_contractors_user_name'].'</div>';
			$output .= '<div class="form_label" style="width:285px;">'.$row['cpm_contractors_user_email'].'</div>';
			$output .= '</a>';
		}
	}else{
		$result_array['success'] = 'N';	
	}
	
	$result_array['results'] = $output;
	
	echo json_encode($result_array);
	exit;
}

#===================================
# Check new user
#===================================

if($_REQUEST['which_action'] == "check"){
	
	$result_array = $contractors->check_user($_SESSION['contractors_qube_id'], $_REQUEST);
	
	echo json_encode($result_array);
	exit;
}

#===================================
# Reset Password
#===================================

if($_REQUEST['which_action'] == "reset"){
	
	$result_array = $contractors->reset_password($_REQUEST);
	
	echo json_encode($result_array);
	exit;
}

#===================================
# Save
#===================================

if($_REQUEST['which_action'] == "save"){
	
	$result_array = $contractors->save($_SESSION['contractors_qube_id'], $_REQUEST);
	
	echo json_encode($result_array);
	exit;
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>RMG Living - Users</title>
	<link href="/css/reset.css" rel="stylesheet" type="text/css" />
	<link href="/css/common.css" rel="stylesheet" type="text/css" />
	<link href="/css/extra.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="/css/custom-theme/jquery-ui-1.8.16.custom.css"/>
	<script type="text/javascript" language="JavaScript" src="/library/jscript/jquery-1.6.2.min.js"></script>
	<script type="text/javascript" language="JavaScript" src="/library/jscript/jquery-ui-1.8.16.custom.min.js"></script>
	<!--[if lte IE 8]> 
	<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<!--[if lte IE 7]> 
	<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<script language="JavaScript" type="text/JavaScript">
	
		$(document).ready(function(){
			
			$("#processing_dialog").dialog({  
				modal: true,
				open: function(event, ui) { $(".ui-dialog-titlebar-close").hide();},
				autoOpen: false
			});	
			
			$("#error_dialog").dialog({
				modal: true,
				open: function(event, ui) { $(".ui-dialog-titlebar-close").show();},
				buttons: {
					Ok: function() {
						$( this ).dialog( "close" );
					}
				},
				autoOpen: false
			});
			
			do_get();
		});
		
		function save(){
			$("#processing_dialog").dialog('open');
			$('#which_action').val('check');
			
			$.post("users.php", 
			$("#form1").serialize(), 
			function(data){	
				$("#processing_dialog").dialog('close');
				
				if (data["results"] != ""){
					err_message = "<p><strong>Please correct the following errors:</strong></p><p>" + data["results"] + "</p>"
					$('#error_message').html(err_message);
					$("#error_dialog").dialog("option", "title", "Errors!");
					$("#error_dialog").dialog('open');
				}else{
					$("#add_dialog").dialog('close');
					do_save()
				}
			}, 
			"json");
		}
		
		function do_save(){
			$("#processing_dialog").dialog('open');
			$('#which_action').val('save');
				
			$.post("users.php", 
			$("#form1").serialize(), 
			function(data){	
				$("#processing_dialog").dialog('close');
				if(data["success"] == "Y"){
					$('#error_message').html("Successfully Saved!");
					$("#error_dialog").dialog("option", "title", "Saved!");
				}else{
					$('#error_message').html("Apologies. We seem to have a technical fault.");
					$("#error_dialog").dialog("option", "title", "Failed!");
				}
				$("#error_dialog").dialog("open");
				do_get();
			}, 
			"json");
		}
		
		function add_user(){
			var ad = $("#add_dialog").dialog({  
				modal: true,
				width: "500px",
				open: function(event, ui) { $(".ui-dialog-titlebar-close").show();},
				buttons: {
					Ok: function() {
						save();
					}
				},
				autoOpen: false
			});	
			
			ad.parent().appendTo('form:first');	
			
			$('#name').val("");
			$('#email').val("");
			$('#disabled_row').hide();
			$("#add_dialog").dialog("option", "title", "Add User");
			$('#add_dialog').dialog('open');
		}
		
		function edit_user(id){
			
			if (id == '<?=$_SESSION['contractors_qube_id'];?>'){
				window.location = "change_details.php";
			}else{
				$("#processing_dialog").dialog('open');
				
				var ad = $("#add_dialog").dialog({  
					modal: true,
					width: "500px",
					open: function(event, ui) { $(".ui-dialog-titlebar-close").show();},
					buttons: {
						Ok: function() {
							save();
						},
						"Reset Password": function() {
							reset_password();
						}
					},
					autoOpen: false
				});	
				
				ad.parent().appendTo('form:first');	
				
				$('#user_id').val(id);
				$('#which_action').val('edit');
				
				$('#name').val("");
				$('#email').val("");
				$('#disabled_row').show();
				
				$.post("users.php", 
				$("#form1").serialize(),
				function(data){	
					$("#processing_dialog").dialog('close');
					if(data['success'] == 'Y'){
						$('#name').val(data['name']);
						$('#email').val(data['email']);
						if(data['disabled'] == 'true'){
							$("#disabled").prop("checked", true);
						}else{
							$("#disabled").prop("checked", false);
						}
						$("#add_dialog").dialog("option", "title", "Edit User");
						$('#add_dialog').dialog('open');
					}
				}, 
				"json");
			}
		}
		
		function reset_password(){
			$("#add_dialog").dialog('close');
			$("#processing_dialog").dialog('open');
			$('#which_action').val('reset');
			
			$.post("users.php", 
			$("#form1").serialize(),
			function(data){	
				$("#processing_dialog").dialog('close');
			}, 
			"json");
		}
		
		function do_get(){
			$("#processing_dialog").dialog('open');
			$('#which_action').val('get');
			
			$.post("users.php", 
			$("#form1").serialize(),
			function(data){	
				$("#processing_dialog").dialog('close');
				if(data['success'] == 'Y'){
					$('#results').html(data['results']);
				}
			}, 
			"json");
		}
	</script>
</head>
<body>
	<div id="processing_dialog" title="Processing" style="display:none;">
		<p style="text-align:center;display:none;margin-bottom:10px;" id="processing_dialog_message"></p>
		<p style="text-align:center;"><img src="/images/ajax-loader.gif" /></p>
	</div>
	<div id="error_dialog" title="Errors" style="display:none;">
		<span id="error_message"></span>
	</div>
	<div id="wrapper">
		<form id="form1" method="post">
			<div id="add_dialog" title="Add User" style="display:none;">
				<table cellspacing="0" width="400">
					<tr>
						<td width="100">
							Name
						</td>
						<td width="300">
							<input type="text" id="name" name="name" />
						</td>
					</tr>
					<tr>
						<td width="200">
							Email
						</td>
						<td width="300">
							<input type="text" id="email" name="email" />
						</td>
					</tr>
					<tr id="disabled_row">
						<td width="200">
							Disabled
						</td>
						<td width="300">
							<input type="checkbox" id="disabled" name="disabled" value="True" />
						</td>
					</tr>
				</table>
			</div>
			<? require_once($UTILS_FILE_PATH."includes/header.php");?>
			<div id="content">
				<table width="760" cellspacing="0">
					<tr>
						<td>Contractors</td>
						<td style="text-align:right;" nowrap="nowrap"><? if(!empty($_SESSION['contractor_session'])){?><a href="/index.php?logoff=Y" class="crumbs">Log Off</a><? }?></td>
					</tr>
				</table>
				<table width="760" cellspacing="0" style="clear:both; margin-top:13px;">
					<tr>
						<td width="15"><img src="/images/dblue_box_top_left_corner.jpg" width="15" height="33" /></td>
						<td width="730" style="background-color:#426B9F;border-top:1px solid #003366; vertical-align:middle"><img src="/images/your_community/your_community_symbol.jpg" width="22" height="22" style="vertical-align:middle;margin-right:8px;" /><span class="box_title">Your Details</span></td>
						<td width="15" style="text-align:right;" align="right"><img src="/images/dblue_box_top_right_corner.jpg" width="15" height="33" /></td>
					</tr>
				</table>			
				<div class="content_box_1" style="padding:0;width:758px;background-color:#F5F7FB;">
					<div class="clearfix" style="padding:15px; background-color:#fff; width:728px;">
						<p><a href="javascript:add_user();">Add User</a></p>
						<div class="results">
							<div class="header_row">
								<div class="form_label border" style="width:110px;">
									User Name
								</div>
								<div class="form_label border" style="width:285px;">
									Name
								</div>
								<div class="form_label" style="width:285px;">
									Email Address
								</div>
							</div>
							<div id="results">
							
							</div>
						</div>
						<p>&nbsp;<br /><a href="contractors.php">&lt;&lt; Back to Dash Board</a></p>
					</div>
				</div>
			</div>
			<input type="hidden" name="which_action" id="which_action" value="" />
			<input type="hidden" name="user_id" id="user_id" value="<?=$_REQUEST['user_id'];?>" />
		</form>
		<? require_once($UTILS_FILE_PATH."includes/footer.php");?>
	</div>
</body>
</html>
