<?
require_once("utils.php");
require_once($UTILS_CLASS_PATH."data.class.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."contractors.reports.class.php");

$website = new website;

// Determine if allowed access into contractors section
$website->allow_contractor_access();

$contractor_report = new contractor_report();

require_once($UTILS_CLASS_PATH."po.class.php");

if ($_REQUEST['whichaction'] == 'order'){
	$data = new data;
	
	$result = $contractor_report->purchase_order($_SESSION['contractors_qube_id'], $_REQUEST);
	$num_results = @mysql_num_rows($result);
	if($num_results > 0){
		
		$output = '<div class="header_row">';
		$output .= '<div class="form_label border" style="width:95px;">PO Number</div>';
		$output .= '<div class="form_label border" style="width:295px;">Property</div>';
		$output .= '<div class="form_label border" style="width:185px;">Lines Open</div>';
		$output .= '<div class="form_label" style="width:90px;">Date</div>';
		$output .= '</div>';
		
		$querystring = $contractor_report->http_parse_query($_REQUEST);
		
		while($row = @mysql_fetch_array($result)){
			
			$lines = $row['no_complete'] + $row['no_incomplete'];
			
			$output .= '<a class="form_row" onclick="javascript:do_job('."'".$row['cpm_po_id']."', '".$querystring."'".');">';
			$output .= '<div class="form_label border" style="width:95px;">'.$row['cpm_po_number'].'</div>';
			$output .= '<div class="form_label border" style="width:295px;">'.$row['rmc_name'].'</div>';
			$output .= '<div class="form_label border" style="width:185px;">'.$row['no_incomplete']."/".$lines.'</div>';
			$output .= '<div class="form_label" style="width:90px;">'.$data->ymd_to_date($row['cpm_po_date_raised']).'</div>';
			$output .= '</a>';
			
		}
	}
	
	$result_array['results'] = $output;
	
	echo json_encode($result_array);
	exit;
}

if ($_REQUEST['whichaction'] == 'user'){
	$output = $contractor_report->user_accounts($_SESSION['contractors_qube_id'], $_REQUEST);
	$result_array['results'] = $output;
	
	echo json_encode($result_array);
	exit;
}

if ($_REQUEST['whichaction'] == 'audit'){
	$output = $contractor_report->user_audit($_REQUEST['user_id']);
	$result_array = $output;
	
	echo json_encode($result_array);
	exit;
}

if ($_REQUEST['whichaction'] == 'job'){
	$data = new data;
	
	$result_job = $contractor_report->job($_REQUEST['po_no'], $_REQUEST);
	
	$num_result_jobs = @mysql_num_rows($result_job);
	$per_page = 12;
	$pages = ceil($num_result_jobs/$per_page);
	$page_count = 1;
	$i = 0;
	if($num_result_jobs > 0){
		$output .= '<div class="header_row" style="width:565px;text-align:left;">';
		$output .= '<div class="form_label border" style="width:85px;">Job Number</div>';
		$output .= '<div class="form_label border" style="width:155px;">Expected Completion</div>';
		$output .= '<div class="form_label border" style="width:50px;">Quote</div>';
		$output .= '<div class="form_label border" style="width:120px;">Closed By</div>';
		$output .= '<div class="form_label" style="width:50px;">On</div>';
		$output .= '</div>';
			
		$output .= '<ul class="paging" id="job_pages"><li id="page-1">';
		
		while($row_job = @mysql_fetch_array($result_job)){
			
			$i += 1; 
			
			$output .= '<div class="form_row" style="width:565px;">';
			$output .= '<div class="form_label border" style="width:85px;">'.$row_job['cpm_po_job_no'].'</div>';
			$output .= '<div class="form_label border" style="width:155px;">'.$data->ymd_to_date($row_job['cpm_po_job_completion_date']).'</div>';
			$output .= '<div class="form_label border" style="width:50px;">'.$row_job['cpm_po_job_amount'].'</div>';
			$output .= '<div class="form_label border" style="width:120px;">'.$contractor_report->is_blank($row_job['user_name'], $contractor_report->is_blank($row_job['cpm_po_job_user_ref'],"Not Complete")).'</div>';
			$output .= '<div class="form_label" style="width:50px;">'.$contractor_report->is_blank($data->ymd_to_date($row_job['cpm_po_job_ts'])).'</div>';
			$output .= '</div>';
			
			if($row_job['cpm_po_job_reason_id'] != ""){
				$output .= '<div class="sub_header_row" style="width:565px;text-align:left;">';
				$output .= '<div class="form_label" style="width:555px;">Reason for Incompletion</div>';
				$output .= '</div><div class="form_row" style="width:565px;text-align:left;">';
				$output .= '<div class="form_label" style="width:555px;"><strong>'.$row_job['the_reason'].":</strong><br />".$row_job['cpm_po_job_reason'].'</div>';
				$output .= '</div>';	
			}
			
			if($row_job['cpm_po_job_advice'] != ""){
				$output .= '<div class="sub_header_row" style="width:565px;text-align:left;">';
				$output .= '<div class="form_label" style="width:555px;">Advice</div>';
				$output .= '</div><div class="form_row" style="width:565px;text-align:left;">';
				$output .= '<div class="form_label" style="width:555px;">'.$row_job['cpm_po_job_advice'].'</div>';
				$output .= '</div>';	
			}
			
			if($i == $per_page){
				$i = 0;
				$page_count += 1;
				$output .= '</li><li id="page-'.$page_count.'">';
			}
		}	
		
		$output .= '</li></ul>';
	}else{
		$output .= $sql_job;	
	}
	
	$output .= '</div>';
	
	$result_array['results'] = $output;
	
	$output = '<ul class="pagination" id="jobination">';
	
	for($i=1; $i<=$pages; $i++){
		$output .= '<li id="'.$i.'"';
		if($i == 1){
			$output .= ' class="active"';
		}
		$output .= ' onclick="javascript:$('."'#job_pages'".').setPage('."'".$i."'".');">'.$i.'</li>';
	}
	$output .= '</ul>';
	
	$result_array['paging'] = $output;
	
	echo json_encode($result_array);
	exit;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>RMG Living - Contractor Reports</title>
	<link href="/css/reset.css" rel="stylesheet" type="text/css" />
	<link href="/css/common.css" rel="stylesheet" type="text/css" />
	<link href="/css/extra.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="/css/custom-theme/jquery-ui-1.8.16.custom.css"/>
	<script type="text/javascript" language="JavaScript" src="/library/jscript/jquery-1.6.2.min.js"></script>
	<script type="text/javascript" language="JavaScript" src="/library/jscript/jquery-ui-1.8.16.custom.min.js"></script>
	<script type="text/javascript" language="JavaScript" src="/library/jscript/jquery.page.js"></script>
	<!--[if lte IE 8]> 
	<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<!--[if lte IE 7]> 
	<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<script type="text/javascript">
	$(document).ready(function(){
		$("#processing_dialog").dialog({  
			modal: true,
			open: function(event, ui) { $(".ui-dialog-titlebar-close").hide();},
			autoOpen: false
		});			
		
		$(".date").datepicker({ dateFormat: 'dd/mm/yy', changeMonth: true, changeYear: true });
	});
	
	function show_dialog(the_width){
		var ad = $("#dialog_audit").dialog({  
			modal: true,
			width: the_width,
			open: function(event, ui) { $(".ui-dialog-titlebar-close").show();},
			buttons: {
				Ok: function() {
					$( this ).dialog( "close" );
				}
			}
		});
		
		ad.parent().appendTo('form:first');		
	}
	
	function do_search() {
		$("#processing_dialog").dialog('open');
		toggle_fields();
		
		$.post("reports.php", 
		$("#form1").serialize(), 
		function(data){	
			$('#results').html(data['results']);
			$("#processing_dialog").dialog('close');
		}, 
		"json");
	}
	
	function do_audit(id) {
		$("#processing_dialog").dialog('open');
		$('#user_id').val(id);
		$('#whichaction').val('audit');
		
		$.post("reports.php", 
		$("#form1").serialize(), 
		function(data){	
			$('#audit_results').html(data['results']);
			$('#audit_paging').html(data['paging']);
			$('#audit_results').css('width', '470px');
			$('#audit_paging').css('width', '470px');
			$("#processing_dialog").dialog('close');
			$("#dialog_audit").dialog("option", "title", "Audit Trail");
			show_dialog("500px");
			toggle_fields();
		}, 
		"json");
	}
	
	function do_job(id, request) {
		$("#processing_dialog").dialog('open');
		$('#user_id').val(id);
		
		var new_request = request.replace("whichaction=order&", "");
		
		$.post("reports.php", 
		new_request.replace("amp;", "") + "&whichaction=job&po_no=" + id, 
		function(data){	
			$('#audit_results').html(data['results']);
			$('#audit_paging').html(data['paging']);
			$('#audit_results').css('width', '565px');
			$('#audit_paging').css('width', '565px');
			$("#processing_dialog").dialog('close');
			$("#dialog_audit").dialog("option", "title", "Jobs Within PO");
			show_dialog("600px");
			toggle_fields();
			$("#job_pages").paging({pageName : "jobination"});
		}, 
		"json");
	}
	
	function toggle_fields(){
		
		$('#purchase_row_1').hide();
		
		if( $('#search_type').val() == '1'){
			$('#whichaction').val('order');
			$('#purchase_row_1').show();
		}else{
			$('#whichaction').val('user');
			$('#date_from').val('');
			$('#date_to').val('');	
		}
		
		toggle_complete()
	}
	
	function toggle_complete(){
		
		$('#purchase_row_2').hide();
		
		if( $('#is_completed').val() == 'True'){
			$('#purchase_row_2').show();
		}else{
			$('#date_from').val('');
			$('#date_to').val('');
		}
	}

</script>
</head>
<body>
	<div id="processing_dialog" title="Processing" style="display:none;">
		<p style="text-align:center;display:none;margin-bottom:10px;" id="processing_dialog_message"></p>
		<p style="text-align:center;"><img src="/images/ajax-loader.gif" /></p>
	</div>
	<div id="dialog_audit" title="Audit Trail" style="display:none;">
		<div class="results" style="width:470px;" id="audit_results">
		</div>
		<div class="results" style="border:none; width:470px;" id="audit_paging">
		</div>
	</div>
	<div id="wrapper">
			<form id="form1" method="post">
			<? require_once($UTILS_FILE_PATH."includes/header.php");?>
			<div id="content">
				<table width="760" cellspacing="0">
					<tr>
						<td>Contractors</td>
						<td style="text-align:right;" nowrap="nowrap"><? if(!empty($_SESSION['contractor_session'])){?><a href="/index.php?logoff=Y" class="crumbs">Log Off</a><? }?></td>
					</tr>
				</table>
	
	
				<table width="760" cellspacing="0" style="clear:both; margin-top:13px;">
					<tr>
						<td width="15"><img src="/images/dblue_box_top_left_corner.jpg" width="15" height="33" /></td>
						<td width="730" style="background-color:#426B9F;border-top:1px solid #003366; vertical-align:middle"><img src="/images/your_community/your_community_symbol.jpg" width="22" height="22" style="vertical-align:middle;margin-right:8px;" /><span class="box_title">Reports</span></td>
						<td width="15" style="text-align:right;" align="right"><img src="/images/dblue_box_top_right_corner.jpg" width="15" height="33" /></td>
					</tr>
				</table>
				
				<div class="content_box_1" style="padding:0;width:758px;background-color:#F5F7FB;">
					<div class="clearfix" style="padding:15px; background-color:#fff; width:728px;">
						<div class="search_box">
							<div class="form_row">
								<div class="form_label">
									Type of Report
								</div>
								<div class="form_item">
									<select id="search_type" name="search_type" onchange="toggle_fields();">
										<option value="1">Purchase Orders</option>
										<option value="2">User Accounts</option>
									</select>
								</div>
								<div class="form_label">
									Search Term
								</div>
								<div class="form_item">
									<input type="text" id="search_term" name="search_term" />
								</div>
							</div>
							<div class="form_row" id="purchase_row_1">
								<div class="form_label">
									Closed
								</div>
								<div class="form_item">
									<select id="is_completed" name="is_completed" onchange="toggle_complete()">
										<option value="True">Yes</option>
										<option value="False">No</option>
										<option value="">Either</option>
									</select>
								</div>
								<div class="form_label">
									Type of PO
								</div>
								<div class="form_item">
									<select id="contract" name="contract">
										<option value="2">All</option>
										<option value="1">Contract</option>
										<option value="0">Reactive</option>
									</select>
								</div>
							</div>
							<div class="form_row" id="purchase_row_2">
								<div class="form_label">
									Date From
								</div>
								<div class="form_item">
									<input class="date" type="text" id="date_from" name="date_from" />
								</div>
								<div class="form_label">
									Date To
								</div>
								<div class="form_item">
									<input class="date" type="text" id="date_to" name="date_to" />
								</div>
							</div>
							<div class="form_row">
								<div class="form_label">
									<input type="button" id="search_button" name="search_button" value="Search" onclick="do_search()" />
								</div>
							</div>
						</div>
						<div class="results" id="results">
						</div>
						<p>&nbsp;<br /><a href="contractors.php">&lt;&lt; Back To Dash Board</a></p>
					</div>
				</div>
			</div>
			<input type="hidden" id="whichaction" name="whichaction" value="" />
			<input type="hidden" id="to_complete" name="to_complete" value="" />
			<input type="hidden" id="user_id" name="user_id" value="" />
		</form>
		<? require_once($UTILS_FILE_PATH."includes/footer.php");?>
	</div>
</body>
</html>