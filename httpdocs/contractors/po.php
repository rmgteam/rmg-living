<?
ini_set("max_execution_time","7200");
require_once("utils.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."po.class.php");
$website = new website;

// Determine if allowed access into contractors section
$website->allow_contractor_access();

require_once($UTILS_CLASS_PATH."po.class.php");

function load_reasons(){
	$sql = "SELECT 
	*
	FROM cpm_po_reason
	ORDER BY cpm_po_reason_text";
	
	$options = "";
	$result = mysql_query($sql);
	$num_rows = @mysql_num_rows($result);
	if($num_rows > 0){
		$options .= '<option value=""> - Please Select - </option>';
		while($row = @mysql_fetch_array($result)){
			$options .= '<option value="'.$row['cpm_po_reason_id'].'">'.$row['cpm_po_reason_text'].'</option>';
		}
	}
	echo $options;
}

if ($_REQUEST['whichaction'] == 'complete_po'){
	
	$has_error = "N";
	
	if ($_REQUEST['reason_id'] == ""){
		if($_REQUEST['completed'] == "No"){
			$has_error = "Please select a reason";
		}
	}elseif ($_REQUEST['reason_id'] == "4"){
		if($_REQUEST['reason'] == ""){
			$has_error = "Please enter a reason";
		}
	}
	
	if( $has_error == "N"){
		$datetime = new DateTime(); 
		
		$sql = "UPDATE cpm_po_job SET cpm_po_job_complete = 'True', 
		cpm_po_job_ts = '".$datetime->format('Ymd')."',
		cpm_po_job_reason = '".$_REQUEST['reason']."',
		cpm_po_job_advice = '".preg_replace("/[^A-Za-z0-9 ]/","",$_REQUEST['advice'])."',
		cpm_po_job_reason_id = '".$_REQUEST['reason_id']."',
		cpm_po_job_user_ref = '".$_SESSION['contractors_username']."'
		WHERE cpm_po_job_id = '".$_REQUEST['to_complete']."'";
		mysql_query($sql) or $has_error = $sql;
		
		$description = '';
		$property = '';
		$po_num = '';
		$job = '';
		$reason = '';
		$reason_detail = '';
		$advice = '';
		
		$sql = "SELECT * 
		FROM cpm_po_job j
		INNER JOIN cpm_po p
		ON j.cpm_po_job_po_id = p.cpm_po_id 
		INNER JOIN cpm_lookup_rmcs l 
		ON l.rmc_ref = p.cpm_po_rmc_id 
		INNER JOIN cpm_rmcs r 
		ON r.rmc_num = l.rmc_lookup 
		LEFT JOIN cpm_po_reason f
		ON f.cpm_po_reason_id = j.cpm_po_job_reason_id
		WHERE cpm_po_job_id = '".$_REQUEST['to_complete']."'";
		$result = mysql_query($sql);
		$num_rows = @mysql_num_rows($result);
		if($num_rows > 0){
			$row = @mysql_fetch_array($result);
			$description = $row['cpm_po_description'];
			$property = $row['rmc_name'];
			$po_num = $row['cpm_po_number'];
			$job = $row['cpm_po_job_no'];
			$reason = $row['cpm_po_reason_text'];
			$reason_detail = $row['cpm_po_job_reason'];
			$advice = $row['cpm_po_job_advice'];
		}
		
		$po = new po;
		$email = '';
		
		$result_array = $po->complete_job($_REQUEST['to_complete']);
		$email = $result_array['result'];
		
		if($_REQUEST['reason'] != '' || $_REQUEST['advice'] != ''){
			if(strpos($email, 'Error code') === false && strpos($email, 'XML is invalid') === false){
				$sql = "SELECT personnel_email
				FROM personnel
				WHERE personnel_username = '".$email."'";
				$result = mysql_query($sql, $UTILS_INTRANET_DB_LINK);
				$num_rows = @mysql_num_rows($result);
				if($num_rows > 0){
					$row = @mysql_fetch_array($result);
					
					if($_REQUEST['reason'] != ''){
						$mail_message = 'Property: ' . $property . '
						PO Number: ' . $po_num . '
						PO Description: ' . $description . '
						Job No: ' . $job . '
						Cancellation Reason: ' . $reason . '
						Detail: ' . $reason_detail;
					}
					
					if($_REQUEST['advice'] != ''){
						$mail_message = 'Property: ' . $property . '
						PO Number: ' . $po_num . '
						PO Description: ' . $description . '
						Job No: ' . $job . '
						Completed Successfully with Advice: ' . $advice;
					}
						
					$sql = "INSERT INTO cpm_mailer SET
					mail_to = '".$row['personnel_email']."',
					mail_from = 'customerservice@rmguk.com',
					mail_subject = 'Purchase Order - Job closed with notification',
					mail_message = '".$mail_message."'";
					@mysql_query($sql);
					
					$sql = "INSERT INTO cpm_mailer SET
					mail_to = 'customservice@rmguk.com',
					mail_from = 'customerservice@rmguk.com',
					mail_subject = 'Purchase Order - Job closed with notification',
					mail_message = '".$mail_message."'";
					
					//@mysql_query($sql);
				}
			}
		}
	}
	
	$result_array['results'] = $has_error;
	$result_array['email'] = $email;
	
	echo json_encode($result_array);
	exit;
}

if ($_REQUEST['whichaction'] == 'search'){
	$po = new po();
	$output = $po->get_list($_SESSION['contractors_qube_id'], $_REQUEST['search_term'], $_REQUEST['closed_type']);
	$result_array['results'] = $output;
	
	echo json_encode($result_array);
	exit;
}

if ($_REQUEST['whichaction'] == 'load'){
	$po = new po();
	$output = $po->retieve_po($_SESSION['contractors_qube_id']);
	$result_array['success'] = $output;
	
	echo json_encode($result_array);
	exit;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>RMG Living - Contractors</title>
	<link href="/css/reset.css" rel="stylesheet" type="text/css" />
	<link href="/css/common.css" rel="stylesheet" type="text/css" />
	<link href="/css/extra.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="/css/custom-theme/jquery-ui-1.8.16.custom.css"/>
	<script type="text/javascript" language="JavaScript" src="/library/jscript/jquery-1.6.2.min.js"></script>
	<script type="text/javascript" language="JavaScript" src="/library/jscript/jquery-ui-1.8.16.custom.min.js"></script>
	<!--[if lte IE 8]> 
	<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<!--[if lte IE 7]> 
	<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<script type="text/javascript">
	$(document).ready(function(){
		$("#processing_dialog").dialog({  
			modal: true,
			open: function(event, ui) { $(".ui-dialog-titlebar-close").hide();},
			autoOpen: false
		});		
		
		var ad = $("#dialog_alert").dialog({  
			modal: true,
			width: "500px",
			open: function(event, ui) { $(".ui-dialog-titlebar-close").show();},
			buttons: {
				Cancel: function() {
					$( this ).dialog( "close" );
				},
				Ok: function() {
					do_complete_job();
				}
			},
			autoOpen: false
		});
		
		ad.parent().appendTo('form:first');	
		
		load_pos();
	});
	
	function expand(id){
		if ($('#extra_row_' + id).css('display') == "none"){
			$('#extra_row_' + id).show();
			$('#expand_icon_' + id).attr("src", "/images/expand.gif");
		}else{
			$('#extra_row_' + id).hide();
			$('#expand_icon_' + id).attr("src", "/images/expand_down.gif");
		}
	}
	
	function complete_job(id) {
		$('#to_complete').val(id);
		$("#reason_id").get(0).selectedIndex = 0;
		$("#completed").get(0).selectedIndex = 0;
		$('#advice').val('');
		$('#reason').val('');
		is_complete();
		$('#dialog_alert').dialog('open');
	}
	
	function load_pos() {
		$('#processing_dialog_message').html('Retrieving latest purchase orders.');
		$("#processing_dialog").dialog('open');
		$("#processing_dialog_message").show();
		
		$('#whichaction').val('load');
		
		$.post("po.php", 
		$("#form1").serialize(), 
		function(data){	
			$("#processing_dialog_message").hide();
			$("#processing_dialog").dialog('close');
			
			do_search();
		}, 
		"json");
	}
	
	function do_complete_job() {
		$("#processing_dialog").dialog('open');
		
		$('#whichaction').val('complete_po');
		
		$.post("po.php", 
		$("#form1").serialize(), 
		function(data){	
			$("#processing_dialog").dialog('close');
			if(data['results'] == "N"){
				$("#dialog_alert").dialog('close');
				do_search();
			}else{
				alert(data['results']);
			}
			
		}, 
		"json");
	}
	
	function is_complete(){
		if($("#completed").val() == "Yes"){
			$('#complete_advice').show();
			$('#complete_row_1').hide();
			$('#complete_row_2').hide();
		}else{
			$('#complete_advice').hide();
			$('#complete_row_1').show();
		}
	}
	
	function is_misc(){
		if($("#reason_id").val() != "5"){
			$('#complete_row_2').hide();
		}else{
			$('#complete_row_2').show();
		}
	}
	
	function do_search() {
		$("#processing_dialog").dialog('open');
		$('#whichaction').val('search');
		
		$.post("po.php", 
		$("#form1").serialize(), 
		function(data){	
			$('#results').html(data['results']);
			$("#processing_dialog").dialog('close');
		}, 
		"json");
	}

</script></head></html>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
</head>
<body>
	<div id="processing_dialog" title="Processing" style="display:none;">
		<p style="text-align:center;display:none;margin-bottom:10px;" id="processing_dialog_message"></p>
		<p style="text-align:center;"><img src="/images/ajax-loader.gif" /></p>
	</div>
	<div id="dialog_alert" title="Complete Job" style="display:none;">
		<table cellspacing="0" width="400">
			<tr>
				<td width="100">
					Completed
				</td>
				<td width="300">
					<select id="completed" name="completed" onchange="is_complete()">
						<option value="Yes">Yes</option>
						<option value="No">No</option>
					</select>
				</td>
			</tr>
			<tr id="complete_advice">
				<td width="200">
					Advice
				</td>
				<td width="300">
					<input type="text" id="advice" name="advice" />
				</td>
			</tr>
			<tr id="complete_row_1">
				<td width="200">
					Reason
				</td>
				<td width="300">
					<select id="reason_id" name="reason_id" onchange="is_misc()">
					<?php load_reasons(); ?>
					</select>
				</td>
			</tr>
			<tr id="complete_row_2">
				<td width="200">
					Reason
				</td>
				<td width="300">
					<input type="text" id="reason" name="reason" />
				</td>
			</tr>
		</table>
	</div>
	<div id="wrapper">
			<form id="form1" method="post">
			<? require_once($UTILS_FILE_PATH."includes/header.php");?>
			<div id="content">
				<table width="760" cellspacing="0">
					<tr>
						<td>Contractors</td>
						<td style="text-align:right;" nowrap="nowrap"><? if(!empty($_SESSION['contractor_session'])){?><a href="/index.php?logoff=Y" class="crumbs">Log Off</a><? }?></td>
					</tr>
				</table>
	
	
				<table width="760" cellspacing="0" style="clear:both; margin-top:13px;">
					<tr>
						<td width="15"><img src="/images/dblue_box_top_left_corner.jpg" width="15" height="33" /></td>
						<td width="730" style="background-color:#426B9F;border-top:1px solid #003366; vertical-align:middle"><img src="/images/your_community/your_community_symbol.jpg" width="22" height="22" style="vertical-align:middle;margin-right:8px;" /><span class="box_title">Purchase Orders</span></td>
						<td width="15" style="text-align:right;" align="right"><img src="/images/dblue_box_top_right_corner.jpg" width="15" height="33" /></td>
					</tr>
				</table>
				
				<div class="content_box_1" style="padding:0;width:758px;background-color:#F5F7FB;">
					<div class="clearfix" style="padding:15px; background-color:#fff; width:728px;">
						<div class="search_box">
							<div class="form_row">
								<div class="form_label">
									Search Term
								</div>
								<div class="form_item">
									<input type="text" id="search_term" name="search_term" />
								</div>
								<div class="form_label">
									Closed
								</div>
								<div class="form_item">
									<select id="closed_type" name="closed_type">
										<option value="False">No</option>
										<option value="True">Yes</option>
										<option value="">Either</option>
									</select>
								</div>
							</div>
							<div class="form_row">
								<div class="form_label">
									<input type="button" id="search_button" name="search_button" value="Search" onclick="do_search()" />
								</div>
							</div>
						</div>
						<div class="results">
							<div class="header_row">
								<div class="form_label border" style="width:90px;">
									PO Number
								</div>
								<div class="form_label border" style="width:235px;">
									Property
								</div>
								<div class="form_label border" style="width:285px;">
									Description
								</div>
								<div class="form_label" style="width:60px;">
									Date
								</div>
							</div>
							<div id="results">
							
							</div>
						</div>
						<p>&nbsp;<br /><a href="contractors.php">&lt;&lt; Back To Dash Board</a></p>
					</div>
				</div>
			</div>
			<input type="hidden" id="whichaction" name="whichaction" value="" />
			<input type="hidden" id="to_complete" name="to_complete" value="" />
		</form>
		<? require_once($UTILS_FILE_PATH."includes/footer.php");?>
	</div>
</body>
</html>