<?
require("utils.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."encryption.class.php");
require_once($UTILS_CLASS_PATH."field.class.php");
require_once($UTILS_CLASS_PATH."security.class.php");
$website = new website;
$crypt = new encryption_class;
$field = new field;
$security = new security;

// Determine if allowed access into content management system
$website->allow_contractor_access();
$err_message = "";
//print $crypt->encrypt($UTILS_DB_ENCODE, trim("rah"));

#===================================
# Check new details
#===================================

if($_REQUEST['which_action'] == "check"){
	
	$user_id = $_SESSION['contractors_username'];
	$err_message = "";
	$contractor_email = "";
	
	$result_array['email_match'] = "true";	
	
	if($_SESSION['contractors_qube_id'] == $user_id){
	
		$sql = "SELECT * 
		FROM cpm_contractors 
		WHERE cpm_contractors_qube_id = '".$_SESSION['contractors_qube_id']."'";
		$result = @mysql_query($sql);
		$password_exists = @mysql_num_rows($result);
	
		if($password_exists > 0){
			while($row = @mysql_fetch_array($result)){
				$contractor_email = $row['cpm_contractors_email'];
			}
		}
		
		$result_array['cemail'] = $contractor_email;
		
		if(strtoupper($contractor_email) != strtoupper($_REQUEST['email'])){
			$result_array['email_match'] = "false";		
		}
	}
	
	$sql = "SELECT * 
	FROM cpm_contractors_user 
	WHERE cpm_contractors_user_ref = '".$user_id."'
	AND cpm_contractors_user_password = '".$crypt->encrypt($UTILS_DB_ENCODE, trim($_REQUEST['old_password']))."'";
	
	$result = @mysql_query($sql);
	$password_exists = @mysql_num_rows($result);

	if($password_exists > 0){
		while($row = @mysql_fetch_array($result)){
			
			$parent = $row['cpm_contractors_user_parent'];
			
			$is_valid = true;
			$parent_domain = "notadomain";
			
			if ($field->is_valid_email(trim($_REQUEST['email'])) == false){
				$err_message .= "&bull; Email address is not a valid email address<br />";
			}
			
			if($parent != 0){
				$sql2 = "SELECT * 
				FROM cpm_contractors_user
				WHERE cpm_contractors_user_ref='".$parent."'";	
				$result2 = @mysql_query($sql2);
				$num_rows = @mysql_num_rows($result2);
				if($num_rows > 0){
					while($row2 = @mysql_fetch_array($result2)){
						$parent_email = explode("@", $row2['cpm_contractors_user_email']);
						$parent_domain = $parent_email[1];
					}
				}
				
				if(strpos($_REQUEST['email'], $parent_domain) === false){
					$err_message .= "&bull; Email address must have the same domain as the parent account: ".$parent_domain."<br />";
				}
			}
		}
	}else{
		$err_message .= "&bull; Your current password is incorrect.<br />";	
	}
	
	$result_array['results'] = $err_message;
	
	echo json_encode($result_array);
	exit;
}

#===================================
# Save new details
#===================================

if($_REQUEST['which_action'] == "save"){

	$pass_error = "N";
	
	$user_id = $_SESSION['contractors_username'];
	
	$do_loop = false;
	
	while($do_loop == false){
	
		$guid = str_replace("}", "", str_replace("{", "", $security->guid()));
		
		$sql = "SELECT * 
		FROM cpm_contractors_user 
		WHERE cpm_contractors_user_guid = '".$guid."'";
		
		$result = @mysql_query($sql);
		$num_rows = @mysql_num_rows($result);
		if($num_rows < 1){
			$do_loop = true;
		}
	}
	
	// Update user record
	$sql = "UPDATE cpm_contractors_user 
	SET ";
	if ($_REQUEST['old_password'] != $_REQUEST['new_password']){
		$sql .= "cpm_contractors_user_password_temp = '".$crypt->encrypt($UTILS_DB_ENCODE, trim($_REQUEST['new_password']))."', 
		cpm_contractors_user_guid = '".$guid."', ";
	}
	$sql .= "cpm_contractors_user_security_q_1 = '".trim($_REQUEST['new_security_q_1'])."', 
	cpm_contractors_user_security_q_2 = '".trim($_REQUEST['new_security_q_2'])."', 
	cpm_contractors_user_security_a_1 = '".trim($_REQUEST['new_security_a_1'])."', 
	cpm_contractors_user_security_a_2 = '".trim($_REQUEST['new_security_a_2'])."',
	cpm_contractors_user_name = '".trim($_REQUEST['name'])."', 
	cpm_contractors_user_email = '".trim($_REQUEST['email'])."'
	WHERE cpm_contractors_user_ref = '".$user_id."'";
	@mysql_query($sql) or $pass_error = "Y";
	
	if($pass_error == "N"){
		
		$datetime = new DateTime(); 
		
		$sql_insert = "INSERT INTO cpm_contractors_user_trail SET
		cpm_contractors_user_trail_login = '".$datetime->format('Y-m-d-H-i-s')."',
		cpm_contractors_user_trail_user_ref = '".$_SESSION['contractors_username']."',
		cpm_contractors_user_trail_ip = '".$_SERVER["REMOTE_ADDR"]."'";
		mysql_query($sql_insert) or $has_error = $sql_insert;
		
		if ($_REQUEST['old_password'] != $_REQUEST['new_password']){
			$mail_message = 'Dear '.$_REQUEST['name'].',
				
			Update from RMG Living Contractors Portal.
				
			Your password has been changed to: '.$_REQUEST['new_password'].'
			
			Please visit http://www.rmgliving.co.uk/validate.php?id='.$guid.' to approve.
				
				
			Kind Regards,
			
			Technical Support Team
			Residential Management Group Ltd
			
			www.rmgliving.co.uk
			Tel. '.$UTILS_TEL_MAIN;
				
			$sql = "INSERT INTO cpm_mailer SET
			mail_to = '".$_REQUEST['email']."',
			mail_from = 'customerservice@rmguk.com',
			mail_subject = 'New RMG Living Password',
			mail_message = '".$mail_message."'";
			
			@mysql_query($sql);
		}
		
		$_SESSION['contractors_last_login'] = $datetime->format('Y-m-d-H-i-s');
	}
	
	$result_array['results'] = $pass_error;
	
	echo json_encode($result_array);
	exit;
}

#===================================
# Get details
#===================================

if($_REQUEST['which_action'] == "get"){
	
	$user_id = $_SESSION['contractors_username'];
	
	$sql = "SELECT * 
	FROM cpm_contractors_user 
	WHERE cpm_contractors_user_ref = '".$user_id."'";
	
	$result = @mysql_query($sql);
	$num_rows = @mysql_num_rows($result);

	if($num_rows > 0){
		$result_array['success'] = 'Y';
		while($row = @mysql_fetch_array($result)){
			$result_array['name'] = $row['cpm_contractors_user_name'];
			$result_array['email'] = $row['cpm_contractors_user_email'];
			$result_array['security_q_1'] = $row['cpm_contractors_user_security_q_1'];
			$result_array['security_q_2'] = $row['cpm_contractors_user_security_q_2'];
			$result_array['security_a_1'] = $row['cpm_contractors_user_security_a_1'];
			$result_array['security_a_2'] = $row['cpm_contractors_user_security_a_2'];
		}
	}else{
		$result_array['success'] = 'N';	
	}
	
	echo json_encode($result_array);
	exit;
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>RMG Living - Change Details</title>
	<link href="/css/reset.css" rel="stylesheet" type="text/css" />
	<link href="/css/common.css" rel="stylesheet" type="text/css" />
	<link href="/css/extra.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="/css/custom-theme/jquery-ui-1.8.16.custom.css"/>
	<script type="text/javascript" language="JavaScript" src="/library/jscript/jquery-1.6.2.min.js"></script>
	<script type="text/javascript" language="JavaScript" src="/library/jscript/jquery-ui-1.8.16.custom.min.js"></script>
	<!--[if lte IE 8]> 
	<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<!--[if lte IE 7]> 
	<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<script language="JavaScript" type="text/JavaScript">
	
		$(document).ready(function(){			
			$("#processing_dialog").dialog({  
				modal: true,
				open: function(event, ui) { $(".ui-dialog-titlebar-close").hide();},
				autoOpen: false
			});		
			
			do_get();
		});
		
		function save(){
			
			var err_message = "";
			
			if($('#old_password').val() == ""){
				err_message = err_message + "&bull; Enter your old password in the field provided.<br />";
			}
			if($('#new_password').val() == ""){
				err_message = err_message + "&bull; Enter your new password in the field provided.<br />";
			}
			if($('#new_password2').val() == ""){
				err_message = err_message + "&bull; Confirm your new password in the field provided.<br />";
			}
			if($('#new_password').val().length < 8){
				err_message = err_message + "&bull; Your password needs to be at least 8 characters.<br />";
			}
			if($('#new_password2').val() != $('#new_password').val()){
				err_message = err_message + "&bull; The new passwords you have entered do not match, please re-type them and try again.<br />";
			}
			
			if (err_message == ""){
				$("#processing_dialog").dialog('open');
				$('#which_action').val('check');
				
				$.post("change_details.php", 
				$("#form1").serialize(), 
				function(data){	
					$("#processing_dialog").dialog('close');
					
					if (data["results"] != ""){
						err_message = "<p><strong>Please correct the following errors:</strong></p><p>" + data["results"] + "</p>"
						$('#error_message').html(err_message);
						
						$("#error_dialog").dialog({
							modal: true,
							open: function(event, ui) { $(".ui-dialog-titlebar-close").show();},
							buttons: {
								Ok: function() {
									if ($('#error_message').html() == "Successfully Saved!"){
										<? if($_SESSION['contractors_parent'] != "0"){?>
										window.location = 'contractors.php';
										<? }else{ ?>
										window.location = 'users.php';
										<? } ?>							
									}
									$( this ).dialog( "close" );
								}
							},
							autoOpen: false
						});
						
						$("#error_dialog").dialog("option", "title", "Errors!");
						$("#error_dialog").dialog('open');
					}else{
						if (data["email_match"] == "false"){
							$("#error_dialog").dialog({
								modal: true,
								open: function(event, ui) { $(".ui-dialog-titlebar-close").show();},
								buttons: {
									Yes: function() {
										$( this ).dialog( "close" );
										do_save();
									},
									No: function() {
										$( this ).dialog( "close" );
									}
								},
								autoOpen: false
							});
							$('#error_message').html("The email address you have entered does not match our records. Do you wish to use the one you have entered?");
							$("#error_dialog").dialog("option", "title", "Email Question!");
							$("#error_dialog").dialog('open');
						}else{
							do_save();
						}
					}
				}, 
				"json");
			}else{
				err_message = "<p><strong>Please correct the following errors:</strong></p><p>" + err_message + "</p>"
				$('#error_message').html(err_message);
				
				$("#error_dialog").dialog({
					modal: true,
					open: function(event, ui) { $(".ui-dialog-titlebar-close").show();},
					buttons: {
						Ok: function() {
							if ($('#error_message').html() == "Successfully Saved!"){
								<? if($_SESSION['contractors_parent'] != "0"){?>
								window.location = 'contractors.php';
								<? }else{ ?>
								window.location = 'users.php';
								<? } ?>							
							}
							$( this ).dialog( "close" );
						}
					},
					autoOpen: false
				});
				
				$("#error_dialog").dialog("option", "title", "Errors!");
				$("#error_dialog").dialog('open');
			}
		}
		
		function do_save(){
			$("#processing_dialog").dialog('open');
			$('#which_action').val('save');
				
			$.post("change_details.php", 
			$("#form1").serialize(), 
			function(data){	
				$("#processing_dialog").dialog('close');
				
				$("#error_dialog").dialog({
					modal: true,
					open: function(event, ui) { $(".ui-dialog-titlebar-close").show();},
					buttons: {
						Ok: function() {
							if ($('#error_message').html() == "Successfully Saved!"){
								<? if($_SESSION['contractors_parent'] != "0"){?>
								window.location = 'contractors.php';
								<? }else{ ?>
								window.location = 'users.php';
								<? } ?>							
							}
							$( this ).dialog( "close" );
						}
					},
					autoOpen: false
				});
				
				if(data["results"] == "N"){
					$('#error_message').html("Successfully Saved!");
					$("#error_dialog").dialog("option", "title", "Saved!");
				}else{
					$('#error_message').html("Apologies. We seem to have a technical fault.");
					$("#error_dialog").dialog("option", "title", "Failed!");
				}
				$("#error_dialog").dialog("open");
			}, 
			"json");
		}
		
		function do_get(){
			$("#processing_dialog").dialog('open');
			$('#which_action').val('get');
			
			$.post("change_details.php", 
			$("#form1").serialize(),
			function(data){	
				$("#processing_dialog").dialog('close');
				if(data['success'] == 'Y'){
					$('#name').val(data['name']);
					$('#email').val(data['email']);
					$('#new_security_q_1').val(data['security_q_1']);
					$('#new_security_a_1').val(data['security_a_1']);
					$('#new_security_q_2').val(data['security_q_2']);
					$('#new_security_a_2').val(data['security_a_2']);
				}
			}, 
			"json");
		}
	</script>
</head>
<body>
	<div id="processing_dialog" title="Processing" style="display:none;">
		<p style="text-align:center;display:none;margin-bottom:10px;" id="processing_dialog_message"></p>
		<p style="text-align:center;"><img src="/images/ajax-loader.gif" /></p>
	</div>
	<div id="error_dialog" title="Errors" style="display:none;">
		<span id="error_message"></span>
	</div>
	<div id="wrapper">
		<form id="form1" method="post">
			<? require_once($UTILS_FILE_PATH."includes/header.php");?>
			<div id="content">
				<table width="760" cellspacing="0">
					<tr>
						<td>Contractors</td>
						<td style="text-align:right;" nowrap="nowrap"><? if(!empty($_SESSION['contractor_session'])){?><a href="/index.php?logoff=Y" class="crumbs">Log Off</a><? }?></td>
					</tr>
				</table>
				<table width="760" cellspacing="0" style="clear:both; margin-top:13px;">
					<tr>
						<td width="15"><img src="/images/dblue_box_top_left_corner.jpg" width="15" height="33" /></td>
						<td width="730" style="background-color:#426B9F;border-top:1px solid #003366; vertical-align:middle"><img src="/images/your_community/your_community_symbol.jpg" width="22" height="22" style="vertical-align:middle;margin-right:8px;" /><span class="box_title">Your Details</span></td>
						<td width="15" style="text-align:right;" align="right"><img src="/images/dblue_box_top_right_corner.jpg" width="15" height="33" /></td>
					</tr>
				</table>			
				<div class="content_box_1" style="padding:0;width:758px;background-color:#F5F7FB;">
					<div class="clearfix" style="padding:15px; background-color:#fff; width:728px;">
						<table width="722" cellspacing="5">
							<tr>
								<td colspan="2">Passwords needs to be at least 8 characters long. </td>
							</tr>
							<tr>
								<td width="300">&nbsp;</td>
								<td width="422">&nbsp;</td>
							</tr>
							<tr>
								<td>Name: </td>
								<td><input name="name" type="text" id="name" /></td>
							</tr>
							<tr>
								<td>Email: </td>
								<td><input name="email" type="text" id="email" /></td>
							</tr>
							<tr>
								<td>Current password: </td>
								<td><input name="old_password" type="password" id="old_password" /></td>
							</tr>
							<tr>
								<td>New password: </td>
								<td><input name="new_password" type="password" id="new_password" /></td>
							</tr>
							<tr>
								<td>Confirm new password: </td>
								<td><input name="new_password2" type="password" id="new_password2" /></td>
							</tr>
							<tr>
								<td>First security question: </td>
								<td><input name="new_security_q_1" type="text" id="new_security_q_1" /></td>
							</tr>
							<tr>
								<td>First security answer: </td>
								<td><input name="new_security_a_1" type="password" id="new_security_a_1" /></td>
							</tr>
							<tr>
								<td>Second security question: </td>
								<td><input name="new_security_q_2" type="text" id="new_security_q_2" /></td>
							</tr>
							<tr>
								<td>Second security answer: </td>
								<td><input name="new_security_a_2" type="password" id="new_security_a_2" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td colspan="2"><input type="button" value=" Save " onclick="save()" /></td>
							</tr>
						</table>
						<? if($_SESSION['contractors_last_login'] != ""){?>
							<? if($_SESSION['contractors_parent'] != "0"){?>
							<br /><a href="contractors.php">&lt;&lt; Back to Dash Board</a>
							<? }else{ ?>
							<br /><a href="users.php">&lt;&lt; Back to Users</a>
							<? } ?>
						<? } ?>
					</div>
				</div>
			</div>
			<input type="hidden" name="which_action" id="which_action" value="" />
		</form>
		<? require_once($UTILS_FILE_PATH."includes/footer.php");?>
	</div>
</body>
</html>
