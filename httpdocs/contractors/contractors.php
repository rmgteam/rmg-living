<?
require("utils.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."encryption.class.php");
require_once($UTILS_CLASS_PATH."field.class.php");
$website = new website;
$crypt = new encryption_class;
$field = new field;

// Determine if allowed access into content management system
$website->allow_contractor_access();
$err_message = "";

if($_SESSION['contractors_last_login'] == ''){
	header("Location: ".$UTILS_HTTPS_ADDRESS."contractors/change_details.php");
}

#===================================
# Check new details
#===================================

if($_REQUEST['which_action'] == "check"){
	$sql = "SELECT * 
	FROM cpm_contractors_user 
	WHERE cpm_contractors_user_ref = '".$_SESSION['contractors_username']." '
	AND cpm_contractors_user_password = '".$crypt->encrypt($UTILS_DB_ENCODE, trim($_REQUEST['old_password']))."'";
	
	$result = @mysql_query($sql);
	$password_exists = @mysql_num_rows($result);

	if($password_exists > 0){
		while($row = @mysql_fetch_array($result)){
			
			$parent = $row['cpm_contractors_user_parent'];
			
			$is_valid = true;
			$parent_domain = "notadomain";
			
			if ($field->is_valid_email(trim($_REQUEST['email'])) == false){
				$is_valid = false;
				$err_message = "&bull; Email address is not a valid email address<br />";
			}
			
			if($parent != 0){
				$sql2 = "SELECT * 
				FROM cpm_contractors_user
				WHERE cpm_contractors_user_ref='".$parent."'";	
				$result2 = @mysql_query($sql2);
				$num_rows = @mysql_num_rows($result2);
				if($num_rows > 0){
					while($row2 = @mysql_fetch_array($result2)){
						$parent_email = explode("@", $row2['cpm_contractors_user_email']);
						$parent_domain = $parent_email[1];
					}
				}
				
				if(strpos($_REQUEST['email'], $parent_domain) === false){
					$is_valid = false;
					$err_message = "&bull; Email address must have the same domain as the parent account: ".$parent_domain."<br />";
				}
			}
		}
	}else{
		$err_message = "&bull; Your current password is incorrect.<br />";	
	}
	
	$result_array['results'] = $err_message;
	
	echo json_encode($result_array);
	exit;
}

#===================================
# Save new details
#===================================

if($_REQUEST['which_action'] == "save"){

	$pass_error = "N";
	
	// Update user record
	$sql = "UPDATE cpm_contractors_user 
	SET 
	cpm_contractors_user_password = '".$crypt->encrypt($UTILS_DB_ENCODE, trim($_REQUEST['new_password']))."', 
	cpm_contractors_user_security_q_1 = '".trim($_REQUEST['new_security_q_1'])."', 
	cpm_contractors_user_security_q_2 = '".trim($_REQUEST['new_security_q_2'])."', 
	cpm_contractors_user_security_a_1 = '".trim($_REQUEST['new_security_a_1'])."', 
	cpm_contractors_user_security_a_2 = '".trim($_REQUEST['new_security_a_2'])."',
	cpm_contractors_user_name = '".trim($_REQUEST['name'])."', 
	cpm_contractors_user_email = '".trim($_REQUEST['email'])."'
	WHERE cpm_contractors_user_ref = ".$_SESSION['contractors_username'];
	@mysql_query($sql) or $pass_error = "Y";
	
	$result_array['results'] = $pass_error;
	
	echo json_encode($result_array);
	exit;
}

#===================================
# Get details
#===================================

if($_REQUEST['which_action'] == "get"){

	$sql = "SELECT * 
	FROM cpm_contractors_user 
	WHERE cpm_contractors_user_ref = '".$_SESSION['contractors_username']."'";
	
	$result = @mysql_query($sql);
	$num_rows = @mysql_num_rows($result);

	if($num_rows > 0){
		while($row = @mysql_fetch_array($result)){
			$result_array['name'] = $row['cpm_contractors_user_name'];
			$result_array['email'] = $row['cpm_contractors_user_email'];
			$result_array['security_q_1'] = $row['cpm_contractors_user_security_q_1'];
			$result_array['security_q_2'] = $row['cpm_contractors_user_security_q_2'];
			$result_array['security_a_1'] = $row['cpm_contractors_user_security_a_1'];
			$result_array['security_a_2'] = $row['cpm_contractors_user_security_a_2'];
		}
	}
	
	echo json_encode($result_array);
	exit;
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>RMG Living - Contractors</title>
	<link href="/css/reset.css" rel="stylesheet" type="text/css" />
	<link href="/css/common.css" rel="stylesheet" type="text/css" />
	<link href="/css/extra.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="/css/custom-theme/jquery-ui-1.8.16.custom.css"/>
	<script type="text/javascript" language="JavaScript" src="/library/jscript/jquery-1.6.2.min.js"></script>
	<script type="text/javascript" language="JavaScript" src="/library/jscript/jquery-ui-1.8.16.custom.min.js"></script>
	<!--[if lte IE 8]> 
	<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<!--[if lte IE 7]> 
	<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
	<![endif]-->
</head>
<body>
	<div id="wrapper">
		<form id="form1" method="post">
			<? require_once($UTILS_FILE_PATH."includes/header.php");?>
			<div id="content">
				<table width="760" cellspacing="0">
					<tr>
						<td>Contractors</td>
						<td style="text-align:right;" nowrap="nowrap"><? if(!empty($_SESSION['contractor_session'])){?><a href="/index.php?logoff=Y" class="crumbs">Log Off</a><? }?></td>
					</tr>
				</table>
				<table width="760" cellspacing="0" style="clear:both; margin-top:13px;">
					<tr>
						<td width="15"><img src="/images/dblue_box_top_left_corner.jpg" width="15" height="33" /></td>
						<td width="730" style="background-color:#426B9F;border-top:1px solid #003366; vertical-align:middle"><img src="/images/your_community/your_community_symbol.jpg" width="22" height="22" style="vertical-align:middle;margin-right:8px;" /><span class="box_title">Dash Board</span></td>
						<td width="15" style="text-align:right;" align="right"><img src="/images/dblue_box_top_right_corner.jpg" width="15" height="33" /></td>
					</tr>
				</table>			
				<div class="content_box_1" style="padding:0;width:758px;background-color:#F5F7FB;">
					<div class="clearfix" style="padding:15px; background-color:#fff; width:728px;">
					
						<? if($_SESSION['contractors_parent'] != "0"){?>
						<a href="change_details.php">
						<? }else{ ?>
						<a href="users.php">
						<? } ?>
							<div class="icon" id="details">
								<div class="text">
									User Details
								</div>
							</div>
						</a>
						<a href="po.php">
							<div class="icon" id="orders">
								<div class="text">
									Purchase Orders
								</div>
							</div>
						</a>
						<a href="reports.php">
							<div class="icon" id="reports">
								<div class="text">
									PO Reports
								</div>
							</div>
						</a>
					</div>
					
					<!--<p>Due to technical difficulties the Contractors Portal is unavailable at this time.. There will be a communique sent to all when the issue has been resolved.</p>-->
				</div>
			</div>
			<input type="hidden" name="which_action" id="which_action" value="" />
		</form>
		<? require_once($UTILS_FILE_PATH."includes/footer.php");?>
	</div>
</body>
</html>
