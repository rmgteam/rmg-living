<?
require("utils.php");
require($UTILS_CLASS_PATH."rmc.class.php");
require($UTILS_CLASS_PATH."resident.class.php");
require($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."statement.class.php");
require_once($UTILS_CLASS_PATH."subsidiary.class.php");
require_once($UTILS_CLASS_PATH."order.class.php");
$ct_order = new order;
$statement = new statement();
$website = new website;
$resident = new resident($_SESSION['resident_ref'],"ref");
$rmc = new rmc;

// Determine if allowed access into 'your community' section
$website->allow_community_access();

// Get name of RMC
$rmc->set_rmc($_SESSION['rmc_num']);
$subsidiary = new subsidiary($rmc->rmc['subsidiary_id']);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><!-- InstanceBegin template="/Templates/main.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- InstanceBeginEditable name="doctitle" -->
<title>RMG Living - Make a Payment</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<? if($resident->resident_status != "Debt Collect"){?>
<script language="javascript" src="library/jscript/functions/pay_online.js"></script>
<? }?>
<!-- InstanceEndEditable -->
<link href="styles.css" rel="stylesheet" type="text/css">
<!--[if lte IE 8]> 
<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if lte IE 7]> 
<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
<![endif]-->
    <script type='text/javascript' src="<?=$UTILS_HTTPS_ADDRESS?>library/jscript/jquery-1.6.2.min.js"></script>

</head>
<body>
<!-- chat now  -->
<? require_once($UTILS_FILE_PATH."includes/chat.php");?>
<? if($_SESSION['is_demo_account'] == "Y" && $_SESSION['is_developer'] != "Y"){?>
<form method="post" action="/building_details.php" style="margin:0; padding:0;">
<input type="hidden" name="a" value="cl">
<input type="hidden" name="s" value="gkfcbk45cgtf5kngn7kns5cg7snk5g7nsv5ng">
<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" style="margin:0; padding:0;">
	<tr>
		<td style="padding:4px;">Change Account:&nbsp;<input type="text" name="change_lessee" value="<?=$_SESSION['resident_ref']?>" size="20"> <input type="submit" name="submit_button" value="Change"></td>
	</tr>
</table>
</form>
<? }?>
<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#E9EEF4" style="border-bottom:1px solid #c1d1e1;"><div align="center"><img src="images/templates/header_bar_grad2.jpg" alt=""></div></td>
  </tr>
  <tr>
    <td style="border-top:1px solid #ffffff;border-bottom:1px solid #ffffff;">
	<table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="180"><a href="<? print $UTILS_HTTPS_ADDRESS;if( isset($_SESSION['login']) && $_SESSION['login'] != ""){print "/building_details.php";}else{print "/index.php";}?>"><img src="/images/templates/rmg_living_logo.jpg" style="margin-top:10px;" width="180" alt="RMG Living" border="0"></a></td>
        <td width="580" align="right">
		<?
		// User logged in is a 'developer', show any developer specific logos, or default to normal ones...
		if($_SESSION['is_developer'] == "Y"){
			
			$sql = "SELECT developer_id FROM cpm_residents_extra WHERE resident_num = ".$_SESSION['resident_num'];
			$result = @mysql_query($sql);
			$row = @mysql_fetch_row($result);
			if($row[0] != "" && file_exists($UTILS_FILE_PATH.'/images/developers/'.$row[0].'/header_'.$row[0].'.jpg') ){
				
				print '<img src="/images/developers/'.$row[0].'/header_'.$row[0].'.jpg" alt="RMG Living" width="580" height="86" BORDER=0>';
			}
			else{
				
				print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
			}
		}
		else{
		
			// Check to see if the Man. Co. has a developer association, if so, set developer specific images, or default to normal images...
			if( isset($_SESSION['login']) && $_SESSION['login'] != "" && $_SESSION['rmc_num'] != ""){
			
				$sql = "SELECT developer_id FROM cpm_rmcs_extra WHERE rmc_num = ".$_SESSION['rmc_num'];
				$result = @mysql_query($sql);
				$row = @mysql_fetch_row($result);
				if( $row[0] != "" && file_exists($UTILS_FILE_PATH.'/images/developers/'.$row[0].'/header_'.$row[0].'.jpg') ){
					
					print '<img src="/images/developers/'.$row[0].'/header_'.$row[0].'.jpg" alt="RMG Living" width="580" height="86" BORDER=0>';
				}
				else{
					
					print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
				}
			}
			else{
				
				print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
			}
		}
		?> 
		
		<?
		/*
		if( isset($_SESSION['login']) && $_SESSION['login'] != "" && $_SESSION['subsidiary_id'] != ""){
			$sql = "SELECT subsidiary_header_image FROM cpm_subsidiary WHERE subsidiary_id = ".$_SESSION['subsidiary_id'];
			$result = @mysql_query($sql);
			$row = @mysql_fetch_row($result);
			if($row[0] != "" ){ 
				?>
				<img src="/images/templates/header_image_w_logo.jpg" width="580" height="86" border="0" alt="RMG Living">
				<img src="/images/logos/<?=$row[0]?>" style="position:absolute; left:50%; margin-left:245px; margin-top:23px; z-index:10;" border="0" >
				<?
			}
			else{
				?>
				<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">
				<?
			}
		}
		else{
			?>
			<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">
			<?
		}
		*/
		?>
		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
  <td>
  <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
  <tr>
    <td height="10" align="center" valign="bottom" bgcolor="#E9EEF4" style="border-bottom:1px solid #c1d1e1;border-top:1px solid #c1d1e1;"><img src="images/templates/header_bar_grad2.jpg" alt=""></td>
  </tr>
  <tr>
  <td height="8" align="center" valign="top" style="border-top:1px solid #ffffff;"><img src="images/templates/header_bar_grad3.jpg" alt=""></td>
  </tr>
  </table>
  </td>
  </tr>
 
  <tr>
    <td height="4"></td>
  </tr>
  <tr>
    <td><table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="28" valign="top">
	
	<table width="760" cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td>
	<!-- InstanceBeginEditable name="breadcrumbs" --><a href="/building_details.php" class="crumbs">Your Community</a>&nbsp;>&nbsp;Make a Payment<!-- InstanceEndEditable -->
	</td>
	<td style="text-align:right;" nowrap>
	<? if(!empty($_SESSION['resident_session'])){?><a href="index.php?logoff=Y" class="crumbs">Log Off</a><? }?>
	</td>
	</tr>
	</table>
	
	</td>
  </tr>
</table>
</td>
  </tr>
  <tr>
    <td><!-- InstanceBeginEditable name="body" -->
      <table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td style="border-bottom:1px solid #003366;"><table width="760" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="15"><img src="images/dblue_box_top_left_corner.jpg" width="15" height="33"></td>
              <td width="730" bgcolor="#416CA0" style="border-top:1px solid #003366; vertical-align:middle"><img src="images/make_a_payment/make_a_payment_symbol.jpg" width="22" height="22" style="vertical-align:middle;">&nbsp;&nbsp;<span class="box_title">Make a Payment</span></td>
              <td width="15" align="right"><img src="images/dblue_box_top_right_corner.jpg" width="15" height="33"></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td><table width="760" border="0" cellspacing="0" cellpadding="15" style="border-left:1px solid #7ea0bd;border-right:1px solid #7ea0bd;border-top:1px solid #7ea0bd;border-bottom:1px solid #cccccc;">
            <tr>
              
<td width="378" valign="top">
<table width="348" border="0" cellspacing="0" cellpadding="0">
                
<tr>
                  
<td><span class="subt036"><b>Online Payment Facility</b></span></td>
</tr>
<? if($resident->resident_status == "Debt Collect"){?>
<tr>
  <td valign="top">&nbsp;</td>
</tr>
<tr>
  <td valign="top"><p style="color:#CC0000;">Online payment is not available at this time. Please contact Customer Services on <?=$UTILS_TEL_MAIN?>.</p></td>
</tr>
<? }else{?>                
<tr>
  <td valign="top">&nbsp;</td>
</tr>
<tr>
  <td valign="top"><img src="images/make_a_payment/cards.jpg" alt="Cards accepted - Visa, Mastercard, Maestro"></td>
</tr>
<tr>
                  
<td valign="top">&nbsp;</td>
</tr>
                
<tr>
                  
<td valign="top">We have teamed up with SecPay to create this Online Payment facility. This is the quickest method of making a payment to your service charge account. The following handling fees, levied by the banks, are charged:</td>
</tr>
                
<tr>
                  
<td valign="top">&nbsp;</td>
</tr>
                
<tr>
                  
<td valign="top">

	<table width="348" border="0" cellspacing="0" cellpadding="0">
		<?
		$result_ct_order = $ct_order->get_order_card_type_list();
		while( $row_ct_order = @mysql_fetch_array($result_ct_order) ){
			
			?>        
			<tr>                  
				<td style="vertical-align:top; font-weight:bold;"><?=stripslashes( $row_ct_order['order_card_type_label'] )?></td>
				<td width="100" style="vertical-align:top"><?=$ct_order->get_card_type_charge_words($row_ct_order['order_card_type_id'])?></td>
			</tr>
			<?
		}
		?>
	</table>
				  
</td>
</tr>
<? }?>

<tr>
                  
<td valign="top">&nbsp;</td>
</tr>
                
<tr>
                  
<td valign="top">

<? if($resident->resident_status != "Debt Collect"){
	
	if($_SESSION['is_developer'] == "Y" || $_SESSION['is_demo_account'] == "Y"){
	
		$resident_ref = "100201101101";	
		$group = "RMG Live";
	}
	else{
		
		if( $_SESSION['is_resident_director'] == "Y" ){
			$resident_ref = $resident->director_assoc_resident_num;
		}
		else{
			$resident_ref = $_SESSION['resident_ref'];
		}
		$group = $subsidiary->subsidiary_ecs_login_group;
	}
	
	
	$grent_scharge = array(0,0);
	$grent_scharge_array = $statement->get_grent_scharge_only("account", $resident_ref, $group, date("d/m/Y"));
	if($grent_scharge_array !== false){
		$grent_scharge = $grent_scharge_array;
	}
	
	?>
<a href="#" onClick="pay_online('<?=$grent_scharge[0]?>','<?=$grent_scharge[1]?>','<?=$rmc->rmc['payment_plan']?>');return false;"><img src="images/make_a_payment/make_a_payment_button.jpg" alt="Click to Make a Payment" width="120" height="21" border="0"></a>
<!--<span class="style1 style2 style3">This service is currently unavailable and, if all maintenance work has gone to plan, will resume on the 14th December 2006. We apologize for the inconvenience this may cause. Please see below for alternative methods to make Service Charge and Ground Rent payments.</span>--></td>
<? }?>

</tr>
                
<tr>
                  
<td valign="top">&nbsp;</td>
</tr>
                
<tr>
  <td valign="top">&nbsp;</td>
</tr>
<tr>
  <td valign="top"><span class="subt036"><b>Paying by Phone</b></span></td>
</tr>
<tr>
  <td valign="top">&nbsp;</td>
</tr>
<tr>
  <td valign="top">To make a payment over the phone using either of the above mentioned cards, please call our switchboard on <strong><?=$UTILS_TEL_MAIN?></strong> and ask to speak to our Customer Services department.</td>
</tr>
<tr>
  <td valign="top">&nbsp;</td>
</tr>
<tr>              
  <td valign="top">&nbsp;</td>
</tr>
  
  
<tr>
  <td valign="top"><span class="subt036"><b>Paying by Bank Transfer</b></span></td>
</tr>
<tr> 
  <td valign="top">					

                    <? if( $rmc->rmc['rmc_ref'] == "779-INC" && $rmc->rmc['subsidiary_id'] == "6" ){?>
                    <p style="margin-top:8px;">To make a <span style="font-weight:bold;">Ground Rent Payments ONLY</span> use the following:</p>
                    <table width="308" border="0" cellspacing="0" cellpadding="0" style="margin-top:7px;margin-bottom:12px;">
                    	<tr>
                        	<td style="font-weight:bold;width:100px;">Account No.</td>
                            <td>02549399</td>
                        </tr>
                        <tr>
                        	<td style="font-weight:bold;">Sort Code</td>
                            <td>23-83-97</td>
                        </tr>
                    </table>
                    <p style="margin-top:12px;">To make a <span style="font-weight:bold;">Service Charge Payments ONLY</span> use the following:</p>
                    <table width="308" border="0" cellspacing="0" cellpadding="0" style="margin-top:7px;">
                    	<tr>
                        	<td style="font-weight:bold;width:100px;">Account No.</td>
                            <td>02549043</td>
                        </tr>
                        <tr>
                        	<td style="font-weight:bold;">Sort Code</td>
                            <td>23-83-97</td>
                        </tr>
                    </table>
                    <? }else{?>
                    <p>To make a payment via bank transfer, please use the following details:</p>
                    <table width="308" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
                    	<tr>
                        	<td style="font-weight:bold;width:100px;">Account No.</td>
                            <td><?=$subsidiary->subsidiary_cra_acc_num?></td>
                        </tr>
                        <tr>
                        	<td style="font-weight:bold;">Sort Code</td>
                            <td><?=$subsidiary->subsidiary_cra_sort_code?></td>
                        </tr>
                    </table>
                    <? }?>
                    
  </td>
</tr>
<tr>
  <td valign="top">&nbsp;</td>
</tr>
<tr>              
  <td valign="top">&nbsp;</td>
</tr>		            
<tr>              
  <td valign="top"><span class="subt036"><b>Paying by Cheque </b></span></td>
</tr>
                
<tr>                
<td valign="top">&nbsp;</td>
</tr>
                
<tr>
                  
<td valign="top">
<? 
// Work out who cheques are payable to
if( preg_match('/^8/', $rmc->rmc_ref) > 0 && trim($rmc->property_manager) == "Ground Rent Only" && trim($rmc->rmc_freeholder_name) != ""){
	$payable_to = "<strong>".stripslashes($rmc->rmc_freeholder_name)."</strong>";
}
else{
	$payable_to = "<strong>".stripslashes($rmc->rmc_name)."</strong> or <strong>RMG</strong>";
}
?>
Cheques must be made payable to <?=$payable_to?>.<br>If not already printed on the cheque, please write 'A/C Payee Only'. Please also write your name and address of the property on the back of the cheque and send your payment along with the tear-off remittance slip (from your Service Charge Demand) to the  address below. </td>
</tr>
                
<tr>
                  
<td valign="top">&nbsp;</td>
</tr>
                
<tr>
                  
<td valign="top">Send cheques to the following address: </td>
</tr>
                
<tr>
                  
<td valign="top">
Accounts Receivable Dept.<br>
Residential Management Group Ltd<br>
RMG House<br>
Essex Road<br>
Hoddesdon<br>
Herts<br>
EN11 0DR</td>
</tr>
<tr>
  <td>&nbsp;</td>
</tr>
<tr>
  <td>&nbsp;</td>
</tr>
<tr>
  <td><b><span class="subt036">Your Service Charge Demand Explained</span></b></td>
</tr>
<tr>
  <td>&nbsp;</td>
</tr>
<tr>
  <td>If you need help with understanding your service charge demand, please click the link below to open a handy guide.</td>
</tr>
<tr>
  <td>&nbsp;</td>
</tr>
<tr><td><table width="348" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="207"><strong><a href="guides/service_charge_demand_explained.pdf" target="_blank" class="link416CA0" style="text-decoration:underline;">Download guide</a></strong></td>
    <td width="141" rowspan="5" align="right" valign="top"><a href="guides/service_charge_demand_explained.pdf" target="_blank"><img src="images/make_a_payment/service_charge_demand_guide.jpg" alt="Click to view an explanation of your Service Charge demand." width="141" height="93" border="0"></a></td>
  </tr>
  <tr>
    <td height="5"></td>
  </tr>
  <tr>
    <td><font color="#666666" size="1">Adobe Acrobat Format (85 Kb)</font></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><a href="<?=$UTILS_ADOBE_URL?>" target="_blank"><img src="images/getacrobat.gif" alt="Download Adobe Reader" width="88" height="31" border="0"></a></td>
  </tr>
</table></td></tr>
              </table>
</td>
              
<td width="378" valign="top" background="images/make_a_payment/woman_with_card.jpg" bgcolor="#F5F7FB" style="border-left:1px solid #eaeaea;background-repeat:no-repeat;">
  <p><img src="images/spacer.gif" alt="Paying your Service Charge" width="348" height="310"></p>
  <table width="348" border="0" cellspacing="0" cellpadding="0">
<tr>
                  
<td><b><span class="subt036">Frequently Asked Questions</span></b></td>
</tr>
                
<tr>
                  
<td>&nbsp;</td>
</tr>
                
<tr>
                  
<td><strong>Q: When and why must I pay my service charge?</strong></td>
</tr>
                
<tr>
                  
<td>A: Payment is due as indicated by the due date shown on your Service Charge Demand. This is to cover the provision of services which are detailed in the terms of the Lease/Transfer of part. Typically, work includes window cleaning, gardening, communal electricity, minor repairs, buildings insurance, agent management and accountancy fees. Our role as agents is to collect your service charge on behalf of your Management Company; you are not paying this amount to RMG.</td>
</tr>
                
<tr>
                  
<td>&nbsp;</td>
</tr>
                
<tr>
                  
<td><strong>Q: What happens if I do not settle my account or withhold payment?</strong></td>
</tr>
                
<tr>
                  
<td>A: If you do not pay charges on time which are allocated to you under the terms of your Lease, then the following may occur:<br>
					<ul>
						<li style="padding-bottom:6px;">The Management Company may instruct the Managing Agent (RMG) to take steps to legally recover the outstanding debt.</li>
                    	<li>Services or planned works to the property may be suspended pending receipt of outstanding or due monies.
                    </ul>
                    
                    
                      
                      If you have a query or payment difficulties, please contact us at the earliest opportunity. We will always endeavour to assist with additional information or advice. However, we do not have the authority to reduce the individual charges that apply.</td>
</tr>
                
<tr>
                  
<td>&nbsp;</td>
</tr>
                
<tr>
                  
<td><strong>Q: Why do I pay Ground Rent?</strong></td>
</tr>
                
<tr>
                  
<td>A: Leaseholders may be obliged under the terms of their Lease to pay a rental charge to the Freehold owner of their property for the land on which the property is built, and or the rights of access across the communal gardens and surrounding areas. Where applicable, ground rent is usually collected separately on behalf of the Freehold owner and not through the Management Company account.</td>
</tr>
                
<tr>
                  
<td>&nbsp;</td>
</tr>
                
<tr>
                  
<td><strong>Q: Why do I pay Buildings Insurance in my charges?</strong></td>
</tr>
                
<tr>
                  
<td>A: Under the terms of your Lease, it is likely that you are responsible for contributing to the cost of ensuring the property has adequate Buildings Insurance. If your Buildings Insurance is paid through your Management Company, please note that you do not need to arrange separate Buildings Insurance yourself.</td>
</tr>
                
<tr>
                  
<td>&nbsp;</td>
</tr>
                
<tr>
                  
<td><strong>Q: Why do I pay extra for major works such as external redecoration?</strong></td>
</tr>
                
<tr>
                  
<td>A: The Service Charge budget is primarily designed for the day-to-day costs of managing your property. Often a sum of money is allocated to a  'reserve' which is used to fund unexpected costs or major works.<br>
                    Even when such a provision exists, it may not be enough to cover all eventualities. On these occasions, the costs of major works will need to be apportioned and charged as a separate item.</td>
</tr>
              </table>
</td>
              </tr>
          </table></td>
        </tr>
		<tr><td height="5" bgcolor="#416CA0" style="border:1px solid #003366;"><img src="images/spacer.gif" alt=""></td>
		</tr>
      </table>
    <!-- InstanceEndEditable --></td>
  </tr>
  <tr>
    <td height="25"></td>
  </tr>
  <tr>
    <td><table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="445" height="14"><a href="about_us.php" class="link416CA0">About RMG Living</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="contact_us.php" class="link416CA0">Contact Us</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="privacy.php" class="link416CA0">Legal Information</a></td>
        <td width="315" align="right">Copyright <?=date("Y", time())?> &copy; <a href="http://www.rmgltd.co.uk">Residential Management Group Ltd</a></td>
      </tr>
	  <tr><td colspan="2">&nbsp;</td></tr>
	  <tr><td colspan="2" style="text-align:left;"><p>&nbsp;</p>
	    </td>
	  </tr>
    </table></td>
  </tr> 
  <tr><td>&nbsp;</td></tr>
</table>
</body>
<!-- InstanceEnd --></html>
