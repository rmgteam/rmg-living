<?
require("utils.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."master_account.class.php");
$website = new website;
$master_account = new master_account($_SESSION['master_account_serial']);

// Determine if allowed access into 'your community' section
$website->allow_community_access();


// Add property
if($_REQUEST['a'] == "s"){
	
	$save_result = $master_account->add_property($_REQUEST);
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>RMG Living - Add a Property</title>
	<link href="/css/reset.css" rel="stylesheet" type="text/css" />
	<!--<link href="styles.css" rel="stylesheet" type="text/css">-->
	<link href="/css/common.css" rel="stylesheet" type="text/css" />
	<!--[if lte IE 8]> 
	<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<!--[if lte IE 7]> 
	<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
	<![endif]-->
	
	<script type="text/JavaScript">
	<!--
	function do_save(){
		
		document.form1.submit();
	}	
	//-->
	</script>

	<script type='text/javascript' src="<?=$UTILS_HTTPS_ADDRESS?>library/jscript/jquery-1.6.2.min.js"></script>

	<? require_once($UTILS_FILE_PATH."includes/analytics.php");?>
</head>
<body>

	<div id="wrapper">
		
		<? require_once($UTILS_FILE_PATH."includes/header.php");?>
		
		<div id="content">
			
			<table width="760" cellspacing="0">
				<tr>
					<td><a href="/building_details.php" class="crumbs">Your Community</a>&nbsp;&gt;&nbsp;My Account&nbsp;&gt;&nbsp;<a href="my_properties.php" class="crumbs">My Properties</a>&nbsp;&gt;&nbsp;Add a Property</td>
					<td style="text-align:right;" nowrap="nowrap"><? if(!empty($_SESSION['resident_session'])){?><a href="index.php?logoff=Y" class="crumbs">Log Off</a><? }?></td>
				</tr>
			</table>
						
			<table width="760" cellspacing="0" style="clear:both; margin-top:13px;" id="content_box_1_top">
				<tr>
					<td width="15"><img src="/images/dblue_box_top_left_corner.jpg" width="15" height="33" /></td>
					<td width="730" style="border-top:1px solid #003366; background-color:#416CA0; vertical-align:middle"><img src="/images/person_icon.png" width="22" height="22" style="vertical-align:middle;" />&nbsp;&nbsp;<span class="box_title">Add a Property</span></td>
					<td width="15" style="text-align:right;"><img src="/images/dblue_box_top_right_corner.jpg" width="15" height="33" /></td>
				</tr>
			</table>	
			
			<div class="content_box_1" style="min-height:300px; background-image:url(images/my_properties/houses.jpg); background-position:top right; background-repeat:no-repeat;">
			
				<form action="<?=$UTILS_HTTPS_ADDRESS?>ma_add_property.php" name="form1" id="form1" method="post">
					<input type="hidden" name="a" id="a" value="s">
				
					<h4>Add a Property</h4>
					<p style="width:370px;">To add one of your properties to your Master Account, enter the relevant details below. Your lessee id and password should have been sent to you in a letter detailing your login details for RMG Living. If you have already received this letter, logged in and updated your login details, then please use these new details.</p>
						
					<?
					if($_REQUEST['a'] == "s" && $save_result[0] !== true){
						?>
						<p class="msg_fail" style="margin-top:20px;width:350px;"><?=$save_result[1]?></p>
						<?
					}
					if($_REQUEST['a'] == "s" && $save_result[0] === true){
						
						?>
						<p class="msg_success" style="margin-top:20px;width:350px;">Congratulations! You have added <? if($save_result[1] == ""){?>this property<? }else{?>the property '<?=$save_result[1]?>'<? }?> to your Master Account.</p>
						<br />
						<a href="my_properties.php">Go to My Properties</a><br />
						<a href="ma_add_property.php">Add another Property to my Master Account</a>
						<?
					}
					?>
				
					<? if($_REQUEST['a'] != "s" || ($_REQUEST['a'] == "s" && $save_result[0] !== true)){?>
					<div style="width:270px; padding:15px; background-color:#f3f3f3; border:1px solid #999;margin-top:20px;margin-bottom:20px;">
					<table class="table_3" width="270" cellspacing="0">
						<tr>
							<td style="width:80px;">Lessee Id:</td>
							<td><input type="text" name="lessee_id" id="lessee_id" value="<?=$_REQUEST['lessee_id']?>" size="18" maxlength="30" autocomplete="off" /></td>
						</tr>
						<tr>
							<td>Password</td>
							<td><input name="password" type="password" id="password" size="18" maxlength="16" autocomplete="off" /></td>
						</tr>
					</table>
					</div>
					
					<a href="Javascript:;" onClick="do_save();return false;"><img src="images/your_details/save_button.jpg" border="0"></a>
					<? }?>
					
				</form>
			
			</div>			
			
		</div>
		
		<? require_once($UTILS_FILE_PATH."includes/footer.php");?>
		
	</div>
		
</body>
</html>
