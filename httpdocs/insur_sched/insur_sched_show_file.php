<?
require_once("utils.php");
require_once($UTILS_CLASS_PATH."security.class.php");
$security = new security;


// Function to get MIME type
function getMime($file){
	$mimetype = array( 
		'doc'=>'application/msword',
		'eps'=>'application/postscript',
		'htm'=>'text/html',
		'html'=>'text/html',
		'jpg'=>'image/jpeg',
		'pdf'=>'application/pdf',
		'txt'=>'text/plain',
		'xls'=>'application/vnd.ms-excel'
	);
	$p = explode('.', $file);
	$pc = count($p);

	// send headers 
	if($pc > 1 AND isset($mimetype[$p[$pc - 1]])){
		header("Content-Type: " . $mimetype[$p[$pc - 1]] . "\n");
	} 
	else{
		header("Content-Type: application/octet-stream\n");
		header("Content-Disposition: attachment; filename=\"$file\"\n");
	}
}


$file = $UTILS_INS_SCHED_PATH.$_REQUEST['s'].".pdf";  
if(!is_file($file) || !file_exists($file)){print "File does not exists";exit;}

// get file
$fp = fopen($file, "r") or die(); 
while(!feof($fp)){ 
	$data .= fread($fp, 1024); 
} 
fclose($fp);

// Get MIME type
$file = $UTILS_INS_SCHED_PATH."Insurance_Schedule.pdf";
getMime($file);

// send file contents 
header("Content-Transfer-Encoding: binary\n"); 
header("Content-length: " . strlen($data) . "\n");
print($data);


?>