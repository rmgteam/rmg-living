<?
ini_set("max_execution_time", "30");
require_once("utils.php");
require_once($UTILS_CLASS_PATH."security.class.php");
$security = new security;
$result_array = array();
$result_array['save_result'] = "fail";

if($_REQUEST['ud'] == "u"){
	
	// If this schedule has not been downloaded at all, insert record.
	$sql_num = "
	SELECT count(*) 
	FROM ins_sched_download_log 
	WHERE 
	ins_sched_id = ".$security->clean_query($_REQUEST['id'])." AND 
	unit_num = ".$security->clean_query($_REQUEST['lid'])." AND 
	resident_num = ".$security->clean_query($_REQUEST['rid'])." 
	";
	$result_num = @mysql_query($sql_num, $UTILS_INTRANET_DB_LINK);
	$row_num = @mysql_fetch_row($result_num);
	if($row_num[0] == 0){
	
		$sql = "
		INSERT INTO ins_sched_download_log (
			ins_sched_id,
			owner_id,
			rmc_num,
			unit_num,
			resident_num,
			ins_sched_download_ts 
		)VALUES(
			".$security->clean_query($_REQUEST['id']).",
			".$security->clean_query($_REQUEST['owner_id']).",
			".$security->clean_query($_REQUEST['rmc_num']).",
			".$security->clean_query($_REQUEST['lid']).",
			".$security->clean_query($_REQUEST['rid']).",
			'".time()."'
		)
		";
		//print $sql;exit;
		if( @mysql_query($sql, $UTILS_INTRANET_DB_LINK) !== false ){
		
			$result_array['save_result'] = "success";
		}
	}
	else{
		
		// If user has downloaded one already, then there should be one (or more) record available downloads - usually 
		// as a result of adding a credit to their account. 
		// i.e. where it has not been timestamped, therefore we can try to update it.
		$sql = "
		UPDATE ins_sched_download_log SET 
			ins_sched_download_ts = '".time()."' 
		WHERE
			(ins_sched_download_ts = '' OR ins_sched_download_ts IS NULL) AND 
			ins_sched_id = ".$security->clean_query($_REQUEST['id'])." AND 
			owner_id = ".$security->clean_query($_REQUEST['owner_id'])." AND 
			rmc_num = ".$security->clean_query($_REQUEST['rmc_num'])." AND 
			unit_num = ".$security->clean_query($_REQUEST['lid'])." AND 
			resident_num = ".$security->clean_query($_REQUEST['rid'])." AND 
			order_ref = '".$security->clean_query($_REQUEST['oid'])."' 
		";
		if( @mysql_query($sql, $UTILS_INTRANET_DB_LINK) !== false ){
		
			$result_array['save_result'] = "success";
		}	
	}
}
elseif($_REQUEST['ud'] == "d"){
	
	// If this schedule has not been downloaded at all, insert record.
	$sql_num = "
	SELECT count(*) 
	FROM ins_sched_download_log 
	WHERE 
	ins_sched_id = ".$security->clean_query($_REQUEST['id'])." AND 
	director_id = ".$security->clean_query($_REQUEST['lid'])." 
	";
	$result_num = @mysql_query($sql_num, $UTILS_INTRANET_DB_LINK);
	$row_num = @mysql_fetch_row($result_num);
	if($row_num[0] == 0){
	
		$sql = "
		INSERT INTO ins_sched_download_log (
			ins_sched_id,
			owner_id,
			director_id,
			rmc_num,
			ins_sched_download_ts 
		)VALUES(
			".$security->clean_query($_REQUEST['id']).",
			".$security->clean_query($_REQUEST['owner_id']).",
			".$security->clean_query($_REQUEST['lid']).",
			".$security->clean_query($_REQUEST['rmc_num']).",
			'".time()."'
		)
		";
		if( @mysql_query($sql, $UTILS_INTRANET_DB_LINK) !== false ){
		
			$result_array['save_result'] = "success";
		}
	}
	else{
		
		// If user has downloaded one, then there should be one (or more) available downloads. 
		// i.e. where it has not been timestamped, therefore we can try to update it.
		$sql = "
		UPDATE ins_sched_download_log SET 
			ins_sched_download_ts = '".time()."' 
		WHERE
			(ins_sched_download_ts = '' OR ins_sched_download_ts IS NULL) AND 
			ins_sched_id = ".$security->clean_query($_REQUEST['id'])." AND 
			owner_id = ".$security->clean_query($_REQUEST['owner_id'])." AND 
			rmc_num = ".$security->clean_query($_REQUEST['rmc_num'])." AND 
			director_id = ".$security->clean_query($_REQUEST['lid'])." AND 
			order_ref = '".$security->clean_query($_REQUEST['oid'])."'
		";
		//print $sql;
		if( @mysql_query($sql, $UTILS_INTRANET_DB_LINK) !== false ){
		
			$result_array['save_result'] = "success";
		}	
	}
}

echo json_encode($result_array);
exit;
?>