<?
$msg = "
This email is confirmation that you have created a Master Account on RMG Living. Now you can merge any additional properties you have, that are currently being managed by RMG, into this one account. To add properties to your Master Account, login to www.rmgliving.co.uk and proceed to the 'My Properties' area. 
Below is a reminder of your username. If you forget either your username or password, please use the 'Forgotten Details' facility on the home page.

Login Id: ".$star_username."


If you have not created this Master Account please alert our Customer Contact Centre. If you experience any issues with the above, please do not hesitate to contact our Customer Contact Centre on '.$UTILS_TEL_MAIN.'. Alternatively, you can contact by email to customerservice@rmguk.com.

Yours sincerely,

Customer Service
http://www.rmgliving.co.uk
";
?>