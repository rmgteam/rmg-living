<?
$msg = "
Below is your password for the RMG Living website - www.rmgliving.co.uk
This will give you access for your property '".$unit->get_unit_desc()."':

".$password."

Once logged in, you can change you password to something more memorable in the 'My Account' section. If you experience problems accessing the RMG Living website, please contact our Customer Services Team via customerservice@rmguk.com

Kind Regards,
RMG Living Support
	
(This email was generated automatically.)
";
?>