<?
require_once($UTILS_CLASS_PATH."order.class.php");
$ct_order = new order;
?>
<div id="card_type_dialog" title="" class="dialog_1">

	<p>Select which card type you will be paying with. This will be used to calculate the charges for this transaction, as follows:</p>
	
	<table cellspacing="0" style="margin:12px 0 15px 0;">
		<?
		$result_ct_order = $ct_order->get_order_card_type_list();
		while( $row_ct_order = @mysql_fetch_array($result_ct_order) ){
		
			?>
			<tr><td style="padding-right:20px;" nowrap><?=stripslashes( $row_ct_order['order_card_type_label'] )?></td><td><?=$ct_order->get_card_type_charge_words($row_ct_order['order_card_type_id'])?></li></tr>
			<?
		}
		?>
	</table>
	
	
	<select name="card_type" id="card_type">
		<option value="" selected="selected">-- Select a Card Type ---</option>
		<?
		@mysql_data_seek($result_ct_order, 0);
		while( $row_ct_order = @mysql_fetch_array($result_ct_order) ){
		
			?>
			<option value="<?=$row_ct_order['order_card_type_id']?>"><?=stripslashes( $row_ct_order['order_card_type_label'] )?></option>
			<?
		}
		?>
	</select>
	
</div>