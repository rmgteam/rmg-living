<?
require_once("../utils.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."rmc.class.php");
require_once($UTILS_CLASS_PATH."resident.class.php");
$website = new website;
$rmc = new rmc;
$resident = new resident;

// Determine if allowed access into 'your community' section
$website->allow_community_access();

// Set resident
$resident->set_resident($_SESSION['resident_num']);

// Set rmc
$rmc->set_rmc($_SESSION['rmc_num']);

// Check if they are able to access the financial reports
if( ($resident->resident['is_resident_director'] != "Y" && $resident->resident['resident_is_steering_member'] != "Y") || ($rmc->rmc['show_content'] != "dir" && $rmc->rmc['show_content'] != "all") || $rmc->rmc['rmc_status'] != "Active" || $rmc->rmc['rmc_is_active'] != "1"){
	header("Location: ".$UTILS_HTTPS_ADDRESS."building_details.php");
}

if($rmc->rmc['is_demo_account'] == "N"){
	header("Location: https://www.yardiaspnl9.com/81635mountstreet/weblinks/forms/wbl_login.asp?semail=".trim($resident->resident['weblinks_username'])."&blogin=-1&spassword=".trim($resident->resident['weblinks_password'])."&surl=wbl_run_report.asp&sQueryString=ReportName=ro_MSH_fin_GL.txt");
}
else{
	header("Location: ../demo/account_ledger.pdf");
}
?>
