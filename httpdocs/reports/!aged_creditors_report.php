<?
ini_set("max_execution_time","120");
require("../utils.php");
require_once($UTILS_CLASS_PATH."rmc.class.php");
require_once($UTILS_CLASS_PATH."resident.class.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."security.class.php");
require_once($UTILS_CLASS_PATH."subsidiary.class.php");
$resident = new resident($_SESSION['resident_ref'],"ref");
$security = new security();
$website = new website;
$rmc = new rmc;

Global $UTILS_TEL_MAIN;
// Only allow them to view this page if they are a resident director...
if($resident->is_resident_director != "Y"){
	header("Location: ../building_details.php");
	exit;
}


// Only allow them to view this page if they are a resident director...
if($resident->is_resident_director != "Y"){
	header("Location: ../building_details.php");
	exit;
}



// Determine if allowed access into 'your community' section
$website->allow_community_access();
$rmc->set_rmc($_SESSION['rmc_num']);
$subsidiary = new subsidiary($rmc->rmc['subsidiary_id']);


// Statement to show for Developer/Demo accounts...
if($_SESSION['is_developer'] == "Y" || $_SESSION['is_demo_account'] == "Y"){
	$rmc_ref = "106501";	
	$group = "RMG Live";
}
else{
	$rmc_ref = $_SESSION['rmc_ref'];
	$group = $subsidiary->subsidiary_ecs_login_group;
}


// Test data (uncomment a row below to test)
//$rmc_ref = "104901";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>RMG Living - Aged Creditors Report</title>
<link href="/css/reset.css" rel="stylesheet" type="text/css" />
<link href="/css/common.css" rel="stylesheet" type="text/css" />
<link href="/css/statement_print.css" rel="stylesheet" type="text/css" media="print" />
<!--[if lte IE 8]> 
<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if lte IE 7]> 
<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
<![endif]-->
<? if($resident->resident_status != "Debt Collect"){?>
<script src="/library/jscript/functions/pay_online.js" type="text/javascript"></script>
<? }?>
<script type="text/javascript" src="/library/jscript/jquery-1.4.2.min.js"></script>
<script type="text/javascript">
var selected_period_type = "quarterly";

function get_report(){

	$("#stat_table").hide();
	
	$(".report_header_row").remove();
	$(".report_data_row").remove();
	$(".report_msg_row").remove();
	$("#strap_aged_creditors_period_type").html(selected_period_type);
	
	if('<?=$resident->resident_status?>' == "Debt Collect"){
		$("#debt_collect_msg").show();
		$("#statement_loader").hide();
		return false;	
	}
	else{
		$("#debt_collect_msg").hide();
		$("#statement_loader").fadeIn();
	}
	
	if( $("#suppress_zero").is(":checked") === true ){
		var suppress_zero = "yes";
	}
	else{
		var suppress_zero = "no";
	}
	
	$.post("get_report.php?type=aged_creditors&suppress_zero=" + suppress_zero + "&period_type=" + selected_period_type + "&rmc_ref=<?=$rmc_ref?>&group=<?=$group?>", 
		
		function(data){
			
			if(data){
				
				// Thiis is a 'just in case' section (because requests are asynchronous)
				$(".report_header_row").remove();
				$(".report_data_row").remove();
				$(".report_msg_row").remove();
	
				$("#debt_collect_msg").before(data);				
				
			}
			$("#statement_loader").hide();
			$("#stat_table").fadeIn('slow');
		},
		"html"
	);
}

function filter_report(){
	get_report();
}

$(document).ready(function() {
	get_report();
});
</script>
</head>
<body>
	
	<div id="wrapper">
	
		<? require_once($UTILS_FILE_PATH."includes/header.php");?>
		
		<div id="content">
		
			<table width="760" cellspacing="0" id="breadcrumb_table">
				<tr>
					<td nowrap="nowrap"><a href="/building_details.php">Your Community</a> &gt; <a href="../financial_reports.php">Financial Reports</a> &gt; Aged Creditors Report</td>
					<td style="text-align:right;"><? if(!empty($_SESSION['resident_session'])){?><a href="/index.php?logoff=Y" class="crumbs">Log Off</a><? }?></td>
				</tr>
			</table>

			<table width="760" cellspacing="0" style="clear:both; margin-top:13px;" id="content_box_1_top">
				<tr>
					<td width="15"><img src="/images/dblue_box_top_left_corner.jpg" width="15" height="33" /></td>
					<td width="730" style="border-top:1px solid #003366; background-color:#416CA0; vertical-align:middle"><img src="/images/financial_reports/financial_reports_symbol.jpg" width="22" height="22" style="vertical-align:middle;" />&nbsp;&nbsp;<span class="box_title">Aged Creditors Report</span></td>
					<td width="15" style="text-align:right;"><img src="/images/dblue_box_top_right_corner.jpg" width="15" height="33" /></td>
				</tr>
			</table>
			
    			
			<div class="content_box_1">
    			
    			<a href="../financial_reports.php" class="back_to_reports">&lt;&lt;&nbsp;back to Reports</a>
    							
				<h3 class="report_title">Aged Creditors</h3>
    			<p>Below is the <span id="strap_aged_creditors_period_type"></span> Aged Creditors report for <?=stripslashes($rmc->rmc['rmc_name'])?></p>
    			
				<div id="statement_loader">
					<br /><br /><br /><br /><p style="font-size:14px; color:#333;text-align:center;">Retrieving report details... please wait</p><br />
					<img src="/images/ajax-loader.gif" border="0" />
				</div>
				
				<table width="728" border="0" cellspacing="0" cellpadding="0" id="stat_table">
										
					<tr id="debt_collect_msg">
						<td style="color:#CC0000;border-left:1px solid #999;border-right:1px solid #999;" colspan="6">Online reports are not available at this time. Please contact Customer Services on <?=$UTILS_TEL_MAIN?> .</td>
					</tr>
										
				</table>
				
			</div>

		</div>


	<? require_once($UTILS_FILE_PATH."includes/footer.php");?>

	</div>
	

</body>
</html>
