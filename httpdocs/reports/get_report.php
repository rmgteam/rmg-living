<?
ini_set("max_execution_time","120");
require("utils.php");
require_once($UTILS_CLASS_PATH."security.class.php");
require_once($UTILS_CLASS_PATH."report.class.php");
$security = new security();
$report = new report();

if($_REQUEST['type'] == "aged_debtors"){
	$report_html = $report->get_aged_debtors_report($_REQUEST['rmc_ref'], $_REQUEST['group'], $_REQUEST['period_type'], $_REQUEST['suppress_zero']);
}
elseif($_REQUEST['type'] == "aged_creditors"){
	$report_html = $report->get_aged_creditors_report($_REQUEST['rmc_ref'], $_REQUEST['group'], $_REQUEST['period_type']);
}
elseif($_REQUEST['type'] == "transaction_status"){
	$report_html = $report->get_transaction_status_report($_REQUEST['rmc_ref'], $_REQUEST['group'], $_REQUEST['period_type']);
}
elseif($_REQUEST['type'] == "kpi"){
	$report_html = $report->get_kpi_report($_REQUEST['rmc_ref'], $_REQUEST['group'], $_REQUEST['fund_types']);
}

echo $report_html;

exit;
?>