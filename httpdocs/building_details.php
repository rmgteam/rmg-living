<?
require_once("utils.php");
//========================================================
// Change resident feature - DEMO account feature only
//========================================================
if($_REQUEST['a'] == "cl" && $_REQUEST['s'] == "gkfcbk45cgtf5kngn7kns5cg7snk5g7nsv5ng"){
	$sql_resident = "
	SELECT re.rmc_num, rex.allow_password_reset, rex.is_first_login, re.resident_name, rex.is_demo_resident_account, rex.login_expiry_YMD, lrmc.rmc_ref, lre.resident_ref, re.resident_num, rex.is_developer, re.paperless
	FROM cpm_residents re, cpm_lookup_residents lre, cpm_residents_extra rex, cpm_rmcs rmc, cpm_lookup_rmcs lrmc 
	WHERE 
	lrmc.rmc_lookup=rmc.rmc_num AND 
	lre.resident_lookup=re.resident_num AND 
	re.rmc_num=rmc.rmc_num AND 
	re.resident_num=rex.resident_num AND 
	(rmc.rmc_status = 'Active' OR rmc.rmc_is_active = '1') AND 
	(re.resident_status = 'Current' OR re.resident_is_active = '1') AND 
	((lre.resident_ref='".strtolower($_POST['change_lessee'])."' AND lre.resident_ref <> '') OR (re.voyager_resident_num='".strtolower($_POST['change_lessee'])."' AND re.voyager_resident_num <> ''))
	";
	$result_resident = @mysql_query($sql_resident);
	$num_valid_users = @mysql_num_rows($result_resident);
	if($num_valid_users > 0){ 
	
		$row_resident = @mysql_fetch_array($result_resident);
	
		$_SESSION['resident_session'] = session_id();
		$_SESSION['rmc_num'] = $row_resident[0];
		$_SESSION['rmc_ref'] = $row_resident[6];
		$_SESSION['is_developer'] = $row_resident[9];
		$_SESSION['paperless'] = $row_resident['paperless'];

		// Get certain RMC info
		$sql_rmc = "SELECT * FROM cpm_rmcs r, cpm_rmcs_extra rex WHERE r.rmc_num=rex.rmc_num AND r.rmc_num = ".$row_resident[0];
		$result_rmc = @mysql_query($sql_rmc);
		$row_rmc = @mysql_fetch_array($result_rmc);
		$_SESSION['is_demo_account'] = "Y";
		$_SESSION['rmc_name'] = $row_rmc['rmc_name'];
		$_SESSION['dev_description'] = $row_rmc['dev_description'];
		$_SESSION['member_id'] = $row_rmc['member_id'];		
		$_SESSION['resident_num'] = strtolower($_POST['change_lessee']);
		$_SESSION['resident_ref'] = $row_resident[7];
		$_SESSION['resident_name'] = $row_resident[3];
		$_SESSION['resident_num'] = $row_resident[8];
		$_SESSION['login'] = session_id();
		$_SESSION['master_account_serial'] = "";
	}
}


include($UTILS_FILE_PATH."library/functions/get_office_tel.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."rmc.class.php");
require_once($UTILS_CLASS_PATH."resident.class.php");
require_once($UTILS_CLASS_PATH."unit.class.php");
$website = new website;
$rmc = new rmc;
$resident = new resident($_SESSION['resident_num']);
$unit = new unit;

// Determine if allowed access into 'your community' section
$website->allow_community_access();

// Set RMC
$rmc->set_rmc($_SESSION['rmc_num']);

if($rmc->subsidiary_code == "sco"){
	$phone_num = '0345 002 4499';
}else if( $rmc->region == 'FAS'){
	$phone_num = '023 8022 6686';
}else if( $rmc->region == 'CSJ'){
	$phone_num = '020 7598 1660';
}else if( $rmc->region == 'LON'){
	$phone_num = '020 7598 1600';
}else{
	$phone_num = $UTILS_TEL_MAIN;
}


// Set unit
$unit->set_unit($_SESSION['resident_num']);

// Work out 12 month cut-off date
$today = $thistime - (86400 * 366);
$cut_off_day = date("Ymd", $today);

// Get any announcements
$sql_announce = "
SELECT * 
FROM cpm_announcements a, cpm_announcements_read ar 
WHERE 
a.announce_approved = 'Y' AND 
a.announce_id=ar.announce_id AND 
a.rmc_num = ".$_SESSION['rmc_num']." AND 
a.announce_date > '$cut_off_day' AND 
ar.resident_num = ".$_SESSION['resident_num']." 
ORDER BY a.announce_date DESC LIMIT 20";
$result_announce = @mysql_query($sql_announce);
$num_announce = @mysql_num_rows($result_announce);

//foreach($_SERVER as $key=>$val){
//	print $key." = ".$val."<br />";
//}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>RMG Living - Your Community</title>
	<link href="/css/reset.css" rel="stylesheet" type="text/css" />
	<link href="/css/common.css" rel="stylesheet" type="text/css" />
	<!--[if lte IE 8]> 
	<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<!--[if lte IE 7]> 
	<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<script type='text/javascript' src="<?=$UTILS_HTTPS_ADDRESS?>library/jscript/jquery-1.6.2.min.js"></script>
	<? require_once($UTILS_FILE_PATH."includes/analytics.php");?>
</head>
<body>

	<div id="wrapper">

		<? require_once($UTILS_FILE_PATH."includes/header.php");?>
		
		<div id="content">

			<table width="760" cellspacing="0">
				<tr>
					<td>Your Community </td>
					<td style="text-align:right;" nowrap="nowrap"><? if(!empty($_SESSION['resident_session'])){?><a href="index.php?logoff=Y" class="crumbs">Log Off</a><? }?></td>
				</tr>
			</table>

			
			<ul class="your_comm_tabs">
				<li class="tab_yc tab_yc_active"><a href="/building_details.php">Your Community</a></li>
				<li class="divider divider_active_l_inactive_r"></li>
				<li class="tab_advice tab_advice_inactive"><a href="/advice.php">Advice/FAQ</a></li>
				<li class="divider divider_inactive_l_inactive_r_a"></li>
				<li class="tab_gi tab_gi_inactive"><a href="/general_info.php">General Info.</a></li>
				<li class="divider divider_inactive_l_inactive_r_a"></li>
				<li class="tab_end"></li>
			</ul>
			
				
			<div class="content_box_1" style="padding:0;width:758px;background-color:#F5F7FB;">
				
				<div class="clearfix" style="padding:15px; background-color:#fff; width:728px;">
					<?
					if(is_file("building_images/".$_SESSION['rmc_num'].".jpg")){
					
						$imgwh = getimagesize("building_images/".$_SESSION['rmc_num'].".jpg");
						$height = $imgwh[1];
						$width = $imgwh[0];
						if($imgwh[1] > 100){
							$scale = 100/$imgwh[1];
							$height = floor($imgwh[1] * $scale);
							$width = floor($imgwh[0] * $scale); 
						}
						?>
						<img src='building_images/<?=$_SESSION['rmc_num']?>.jpg' style="border:1px solid #9bb6cc; float:left; margin-right:15px;" border='0' width="<?=$width?>" height="<?=$height?>" alt="Your Development" />
						<?
					}
					?>
					
					<?
					if($unit->unit['unit_description'] != ""){
						$unit_name = "(".stripslashes($unit->unit['unit_description']).")";
					}
					elseif($unit->unit['unit_address_1'] != ""){
						$unit_name = "(".stripslashes($unit->unit['unit_address_1']).")";
					}
					?>
					
					<span class="major_title036"><?=$rmc->rmc['rmc_name']?></span><br /><span class="subt036"><?=$resident->resident_name?>&nbsp;<?=$unit_name?></span>
					<p style="margin-top:4px;"><? if(trim($rmc->rmc['dev_description']) != ""){print stripslashes(trim($rmc->rmc['dev_description']));}?></p>
					
				</div>
						
				<div class="clearfix" style="clear:both;vertical-align:top; padding:15px; border-top:1px solid #e5e5e5;">
			
					<table width="212" cellspacing="0" style="float:left;">
						<tr>
							<td class="subt036"><strong>Your Property Manager is</strong></td>
						</tr>
						<tr>
							<td nowrap="nowrap" style="vertical-align:top; padding:5px 0 10px 0;"><b><?=$rmc->rmc['property_manager']?></b><br /><?=$phone_num?></td>
						</tr>
						<tr>
							<td>To contact your Property Manager, please use the <a href="raise_an_issue.php" class="link036">Raise an Issue</a> section.<br />
								<br />
								Otherwise, to contact RMG about technical issues ONLY, please <a href="contact_us.php" class="link036">click here</a>
							</td>
						</tr>
					</table>
						
					<? 
					$total_sections = 1;
					$button_order[] = "raise_an_issue";
					$button_img_alt[] = "Raise an Issue";
					
					if($resident->is_subtenant_account != "Y"){
						$total_sections += 2;
						$button_order[] = "your_statement";
						$button_order[] = "make_a_payment";
						$button_img_alt[] = "View Your Statement";
						$button_img_alt[] = "Make a Payment";
					}
					
					// Determine if Useful Documents present
					$num_useful_documents = 0;
					$num_useful_documents += $rmc->get_num_letters_of_interest();
					$num_useful_documents += $rmc->get_num_client_documents();
					$num_useful_documents += $rmc->get_num_car_park_plans();
					if( $resident->is_resident_director == "Y" && $resident->is_subtenant_account != "Y" ){$num_useful_documents += $rmc->get_num_slas();}
					$num_useful_documents += $rmc->get_num_ssm();
					$num_useful_documents += $rmc->get_num_newsletters();
					if($num_useful_documents > 0){
						$total_sections ++;
						$button_order[] = "useful_documents";
						$button_img_alt[] = "Useful Documents";
					}
					
					// Determine if budgets available
					$num_budgets = 0;
					$num_budgets = $rmc->get_num_budgets();
					if($num_budgets > 0 && $resident->is_subtenant_account != "Y"){
						$total_sections ++;
						$button_order[] = "budgets";
						$button_img_alt[] = "Budgets";
					}
					
					// Determine if meeting notes available
					$num_meetings = 0;
					$num_site_visits = 0;
					$num_site_visits = $rmc->get_num_site_visits();
					$num_meetings = $rmc->get_num_meeting_notes();
					if($num_meetings > 0 || $num_site_visits > 0){
						$total_sections ++;
						$button_order[] = "meetings";
						$button_img_alt[] = "Meeting Notes";
					}
					
					// Determine if insurance schedules available (do not allow for Ground Rent ONLY properties)
					if( $rmc->is_ground_rent_only() === false && $resident->is_subtenant_account == "N"){
						
						$num_ins_sched = 0;
						if($resident->is_resident_director == "Y"){
							$num_ins_sched = $resident->get_num_director_ins_sched();
						}
						else{
							$num_ins_sched = $unit->get_num_unit_ins_sched();
						}						
						if($num_ins_sched > 0){
							$total_sections ++;
							$button_order[] = "ins_sched";
							$button_img_alt[] = "Insurance Schedule(s)";
						}
					}
					
					// Determine if able to show Financial Reports
					//print "HERE:".$resident->is_resident_director."<br>";
					if( $resident->is_resident_director == "Y" && $resident->is_subtenant_account != "Y" ){
						$total_sections ++;
						$button_order[] = "financial_reports";
						$button_img_alt[] = "Financial Reports";
					}
					
					// Set full compliment as starting point
					for($s=0;$s < count($button_order);$s++){
					
						$space_img[$button_order[$s]] 		= "images/your_community/".$button_order[$s]."_button.jpg";
						$space_colspan[$button_order[$s]]	= 1;
						$space_width[$button_order[$s]]		= 100;
					}
					
					// Adjust settings as per content availability
					if($total_sections == 7){
						$space_img[$button_order[0]]		= "images/your_community/".$button_order[0]."_button_large.jpg";
						$space_colspan[$button_order[0]]	= 2;
						$space_width[$button_order[0]]		= 210;
					}
					else if($total_sections == 6){
						$space_img[$button_order[0]]		= "images/your_community/".$button_order[0]."_button_large.jpg";
						$space_colspan[$button_order[0]]	= 2;
						$space_width[$button_order[0]]		= 210;
						$space_img[$button_order[1]]		= "images/your_community/".$button_order[1]."_button_large.jpg";
						$space_colspan[$button_order[1]]	= 2;
						$space_width[$button_order[1]]		= 210;
					}
					else if($total_sections == 5){
						$space_img[$button_order[0]]		= "images/your_community/".$button_order[0]."_button_large.jpg";
						$space_colspan[$button_order[0]]	= 2;
						$space_width[$button_order[0]]		= 210;
						$space_img[$button_order[1]]		= "images/your_community/".$button_order[1]."_button_large.jpg";
						$space_colspan[$button_order[1]]	= 2;
						$space_width[$button_order[1]]		= 210;
						$space_img[$button_order[2]]		= "images/your_community/".$button_order[2]."_button_large.jpg";
						$space_colspan[$button_order[2]]	= 2;
						$space_width[$button_order[2]]		= 210;
					}
					else if($total_sections == 4){
						$space_img[$button_order[0]]		= "images/your_community/".$button_order[0]."_button_large.jpg";
						$space_colspan[$button_order[0]]	= 2;
						$space_width[$button_order[0]]		= 210;
						$space_img[$button_order[1]]		= "images/your_community/".$button_order[1]."_button_large.jpg";
						$space_colspan[$button_order[1]]	= 2;
						$space_width[$button_order[1]]		= 210;
						$space_img[$button_order[2]]		= "images/your_community/".$button_order[2]."_button_large.jpg";
						$space_colspan[$button_order[2]]	= 2;
						$space_width[$button_order[2]]		= 210;
						$space_img[$button_order[3]]		= "images/your_community/".$button_order[3]."_button_large.jpg";
						$space_colspan[$button_order[3]]	= 2;
						$space_width[$button_order[3]]		= 210;
					}
					else if($total_sections == 3){
						$space_img[$button_order[0]]		= "images/your_community/".$button_order[0]."_button_large.jpg";
						$space_colspan[$button_order[0]]	= 2;
						$space_width[$button_order[0]]		= 210;
					}
					else if($total_sections == 2){
						$space_img[$button_order[0]]		= "images/your_community/".$button_order[0]."_button_large.jpg";
						$space_colspan[$button_order[0]]	= 2;
						$space_width[$button_order[0]]		= 210;
						$space_img[$button_order[1]]		= "images/your_community/".$button_order[1]."_button_large.jpg";
						$space_colspan[$button_order[1]]	= 2;
						$space_width[$button_order[1]]		= 210;
					}
					
					// Print out buttons
					?>
					<div class="clearfix" style="width:500px; text-align:right; float:right;">
						<?
						for($s=0;$s < count($button_order);$s++){
							
							print "<div style=\"float:right; margin:0 0 10px 10px; width:".$space_width[$button_order[$s]]."px;\"><a href=\"".$button_order[$s].".php\"><img src=\"".$space_img[$button_order[$s]]."\" border=\"0\" alt=\"".$button_img_alt[$s]."\" /></a></div>";									
						}
						?>
					</div>
					
				</div>
			</div>
				

			<div class="clearfix" style="margin:0 auto; width:760px; clear:both;">

				<div style="float:left; width:530px;">
		
					<table width="530" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="15"><img src="images/lblue_box_top_left_corner.jpg" width="15" height="33" /></td>
							<td width="250" style="background-color:#669ACC;border-top:1px solid #6699cc; vertical-align:middle"><img src="images/your_community/announcements_symbol.jpg" width="22" height="22" style="vertical-align:middle;" />&nbsp;&nbsp;<span class="box_title">Announcements</span></td>
							<td width="250" class="box_title" style="text-align:right;background-color:#669ACC;border-top:1px solid #6699cc;"><a href="announcements.php" class="linkbox">View All</a></td>
							<td width="15" style="text-align:right;verticl-align:top;"><img src="images/lblue_box_top_right_corner.jpg" width="15" height="33" /></td>
						</tr>
					</table>
					
					<div class="yc_announcements">
						<table width="528" cellspacing="0">
							<?
							if($num_announce > 0){
				
								$row_counter = 1;
								// display announcements
								while($row_announce = @mysql_fetch_array($result_announce)){
									?>
									<tr>
										<td width="82" nowrap="nowrap"><?=substr($row_announce['announce_date'],6,2)."/".substr($row_announce['announce_date'],4,2)."/".substr($row_announce['announce_date'],0,4)?>&nbsp;</td>
										<td class="text"><a href="announcements.php?id=<?=$row_announce['announce_id']?>&amp;whichaction=view" class="link416CA0" style="text-decoration:underline;" <? if($row_announce['announce_read'] == 'N'){?>style="font-weight:bold;"<? }?>><? print substr($row_announce['announce_title'],0,50);if(strlen($row_announce['announce_title']) > 50){print "...";}?></a>&nbsp;</td>
									</tr>
									<?
									$row_counter++;
								}
							}
							else{
								?>
								<tr><td colspan="2">There are no announcements</td></tr>
								<?
							}
							?>
						</table>
					</div>
				</div>

				<div style="float:right; width:215px;">

					<table width="215" cellspacing="0">
						<tr>
							<td width="15"><img src="images/lblue_box_top_left_corner.jpg" width="15" height="33" /></td>
							<td width="185" style="vertical-align:middle; background-color:#669ACC;"><img src="images/person_icon.png" width="22" height="22" style="vertical-align:middle;margin-right:8px;" /><span class="box_title">My Account</span> </td>
							<td width="15" style="text-align:right;"><img src="images/lblue_box_top_right_corner.jpg" width="15" height="33" /></td>
						</tr>
					</table>
					<div style="width:183px; height:105px;padding:15px;border-left:1px solid #7ea0bd;border-right:1px solid #7ea0bd;border-top:1px solid #7ea0bd;border-bottom:2px solid #003366;">
						<table width="183" cellspacing="0">
							<tr>
								<td style="padding:0 0 15px 0;">
									<? if($_SESSION['master_account_serial'] != ""){?>
									Change your personal details or manage your properties.
									<? }else{?>
									Change your personal details or, if you own multiple properties, create a single account to manage them with.
									<? }?>
									</td>
							</tr>
							<tr>
								<td valign="bottom">
									<a href="<? if($_SESSION['master_account_serial'] != ""){?>ma_change_details.php<? }else{?>change_details.php<? }?>" title="Manage your account"><img src="images/your_community/<? if($_SESSION['master_account_serial'] != ""){?>my_details_button.jpg<? }else{?>manage_account_button.jpg<? }?>" name="change_button" border="0" id="change_button" /></a>
									<? if($_SESSION['master_account_serial'] != ""){?><a href="my_properties.php" title="Manage your Properties"><img src="images/your_community/my_properties_button.jpg" name="properties_button" border="0" id="properties_button" style="margin-top:10px;" /></a><? }?>
								</td>
							</tr>
						</table>
					</div>
			
				</div>
				
			</div>
			<a class="paperless" href="paperless/index.php">
				<? if($_SESSION['paperless'] == 'N') { ?>
					Have you signed up for paperless?
				<? } else { ?>
					Don't like the paperless service? Opt Out
				<? } ?>
			</a>

		</div>
		
		<? require_once($UTILS_FILE_PATH."includes/footer.php");?>
	
	</div>

</body>
</html>
