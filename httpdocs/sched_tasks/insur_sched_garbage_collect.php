<?php
// This file deletes insurance schedules from this server if they have been marked as discontinued and have been for 30 days or more...

ini_set("max_execution_time", "1440000");
require_once("utils.php");

$thistime = time();
$day_30_limit = $thistime - (86400 * 30);

$sql = "
SELECT * 
FROM ins_sched i 
WHERE 
i.ins_sched_discon = 'Y' AND 
(i.ins_sched_garbage_ts = '' OR i.ins_sched_garbage_ts IS NULL ) AND 
i.ins_sched_discon_ts < ".$day_30_limit."  
";
$result = mysql_query($sql, $UTILS_INTRANET_DB_LINK);
while($row = mysql_fetch_array($result)){
		
	// Delete file
	@unlink($UTILS_INS_SCHED_PATH.$row['ins_sched_filename']);
		
	// Update record
	$sql2 = "
	UPDATE ins_sched SET
		ins_sched_garbage_ts = '".$thistime."' 
	WHERE ins_sched_id = ".$row['ins_sched_id']." 
	";
	mysql_query($sql2, $UTILS_INTRANET_DB_LINK);

}
?>