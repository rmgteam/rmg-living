<?
//==================
// Mail script
//==================

Global $UTILS_ADMIN_EMAIL, $UTILS_FILE_PATH;

ini_set("max_execution_time", "180");
require($UTILS_FILE_PATH."utils.php");
include("Mail.php");
include("Mail/mime.php");
require_once($UTILS_FILE_PATH."library/classes/mime.class.php");

$thistime = time();
$smime = new mime();

$sql = "SELECT * FROM cpm_mailer WHERE mail_sent = 'N'";
$result = @mysql_query($sql);
while($row = @mysql_fetch_array($result)){

	$mail_error = "N";
	/*if(!mail($row['mail_to'],$row['mail_subject'],stripslashes($row['mail_message']),$row['mail_headers'])){
		$mail_error = "Y";
	}*/
	
	$hdrs = array(
		'From' => $row['mail_from'],
		'Subject' => $row['mail_subject']
	);
	
	$headers = explode(PHP_EOL, $row['mail_headers']);
	
	$to = str_replace(' ', '', $row['mail_to']);
	
	foreach ($headers as &$value){
		$header_item = explode(":", $value);
		
		if($header_item[0] != ''){
		
			$hdrs[trim($header_item[0])] = trim($header_item[1]);
			
			if(strtoupper(trim($header_item[0])) == "BCC" OR strtoupper(trim($header_item[0])) == "CC"){
				$to .= ", " . trim($header_item[1]);
			}
		}
	}
	
	$hdrs['To'] = $row['mail_to'];
	$recipients = $to;
	$crlf = "\n";
	
	$mime = new Mail_mime(array('eol' => $crlf));
	
	$images = explode(";", $row['mail_images']);
	
	foreach ($images as &$value){
		
		if($value != ''){
			$image = $value;
			
			if(strpos($value, '|') !== false){
				$vars = explode("|", $value);
				$image = $vars[0];
				$cid = $vars[1];
			}else{
				$cid = Null;				}
			
			print("Embed - " . $image . ' as ' . $cid . "\n");
		
			if($image != ""){
				$added_image = $mime->addHTMLImage($image, $smime->get_mime($value), '', true, $cid);
				print($added_image . "\n");	
			}
		}
		
	}
	
	$mime->setTXTBody($row['mail_message']);
	$mime->setHTMLBody($row['mail_html']);
	
	//max attachment filename length is 69 including extension
	
	if($row['mail_attachment'] != ''){
		if(strpos($row['mail_attachment'], ';') !== false){
			$attachments = explode(";",$row['mail_attachment']);
			foreach($attachments as $value){
				if($value != ''){
					$mime->addAttachment($value, $smime->get_mime($value));
				}
			}
		}else{
			$mime->addAttachment($row['mail_attachment'], $smime->get_mime($row['mail_attachment']));
		}
	}
	
	$body = $mime->get();
	$hdrs = $mime->headers($hdrs);
	
	/* SMTP server name, port, user/passwd */
	$smtpinfo["host"] = "94.250.233.132";
	$smtpinfo["port"] = "25";
	$smtpinfo["auth"] = false;
	/* Create the mail object using the Mail::factory method */
	$mail_object =& Mail::factory("smtp", $smtpinfo);
	/* Ok send mail */
	
	$_mail = $mail_object->send($recipients, $hdrs, $body);
		
	if (PEAR::isError($_mail)) {
		$mail_error = "Y";
		echo date( " Y-m-d H:i:s  ->  " ) . "email to {$recipients} failed, details : " . $_mail->getMessage() . "\n\n";
	}else{
		echo("mail sent \n");
	}
	
	if($mail_error == "N"){
		$sql_up = "
		UPDATE cpm_mailer SET
		mail_sent_ts = '".$thistime."',
		mail_sent = 'Y'
		WHERE mail_id = ".$row['mail_id'];
		@mysql_query($sql_up);
		
		if($row['mail_del_attach'] == 'Y'){
			if($row['mail_attachment'] != ''){
				if(strpos($row['mail_attachment'], ';') !== false){
					$attachments = explode(";",$row['mail_attachment']);
					foreach($attachments as $value){
						if($value != ''){
							unlink($value);
						}
					}
				}else{
					unlink($row['mail_attachment']);
				}
			}
			
		}
	}
	else{
		mail($UTILS_ADMIN_EMAIL,"INTRANET MAILER ERROR","Mail ID ".$row['mail_id']." failed to send.");
	}
}
?>
