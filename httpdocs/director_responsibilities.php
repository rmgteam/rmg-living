<?
require("utils.php");
require($UTILS_CLASS_PATH."website.class.php");
$website = new website;

// Determine if allowed access into 'your community' section
$website->allow_community_access();

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><!-- InstanceBegin template="/Templates/main.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- InstanceBeginEditable name="doctitle" -->
<title>RMG Living - Director Responsibilities</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<link href="styles.css" rel="stylesheet" type="text/css">
<!--[if lte IE 8]> 
<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if lte IE 7]> 
<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
<![endif]-->
</head>
<body>
<? if($_SESSION['is_demo_account'] == "Y" && $_SESSION['is_developer'] != "Y"){?>
<form method="post" action="/building_details.php" style="margin:0; padding:0;">
<input type="hidden" name="a" value="cl">
<input type="hidden" name="s" value="gkfcbk45cgtf5kngn7kns5cg7snk5g7nsv5ng">
<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" style="margin:0; padding:0;">
	<tr>
		<td style="padding:4px;">Change Account:&nbsp;<input type="text" name="change_lessee" value="<?=$_SESSION['resident_ref']?>" size="20"> <input type="submit" name="submit_button" value="Change"></td>
	</tr>
</table>
</form>
<? }?>
<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#E9EEF4" style="border-bottom:1px solid #c1d1e1;"><div align="center"><img src="images/templates/header_bar_grad2.jpg" alt=""></div></td>
  </tr>
  <tr>
    <td style="border-top:1px solid #ffffff;border-bottom:1px solid #ffffff;">
	<table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="180"><a href="<? print $UTILS_HTTPS_ADDRESS;if( isset($_SESSION['login']) && $_SESSION['login'] != ""){print "/building_details.php";}else{print "/index.php";}?>"><img src="/images/templates/rmg_living_logo.jpg" style="margin-top:10px;" width="180" alt="RMG Living" border="0"></a></td>
        <td width="580" align="right">
		<?
		// User logged in is a 'developer', show any developer specific logos, or default to normal ones...
		if($_SESSION['is_developer'] == "Y"){
			
			$sql = "SELECT developer_id FROM cpm_residents_extra WHERE resident_num = ".$_SESSION['resident_num'];
			$result = @mysql_query($sql);
			$row = @mysql_fetch_row($result);
			if($row[0] != "" && file_exists($UTILS_FILE_PATH.'/images/developers/'.$row[0].'/header_'.$row[0].'.jpg') ){
				
				print '<img src="/images/developers/'.$row[0].'/header_'.$row[0].'.jpg" alt="RMG Living" width="580" height="86" BORDER=0>';
			}
			else{
				
				print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
			}
		}
		else{
		
			// Check to see if the Man. Co. has a developer association, if so, set developer specific images, or default to normal images...
			if( isset($_SESSION['login']) && $_SESSION['login'] != "" && $_SESSION['rmc_num'] != ""){
			
				$sql = "SELECT developer_id FROM cpm_rmcs_extra WHERE rmc_num = ".$_SESSION['rmc_num'];
				$result = @mysql_query($sql);
				$row = @mysql_fetch_row($result);
				if( $row[0] != "" && file_exists($UTILS_FILE_PATH.'/images/developers/'.$row[0].'/header_'.$row[0].'.jpg') ){
					
					print '<img src="/images/developers/'.$row[0].'/header_'.$row[0].'.jpg" alt="RMG Living" width="580" height="86" BORDER=0>';
				}
				else{
					
					print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
				}
			}
			else{
				
				print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
			}
		}
		?> 
		
		<?
		/*
		if( isset($_SESSION['login']) && $_SESSION['login'] != "" && $_SESSION['subsidiary_id'] != ""){
			$sql = "SELECT subsidiary_header_image FROM cpm_subsidiary WHERE subsidiary_id = ".$_SESSION['subsidiary_id'];
			$result = @mysql_query($sql);
			$row = @mysql_fetch_row($result);
			if($row[0] != "" ){ 
				?>
				<img src="/images/templates/header_image_w_logo.jpg" width="580" height="86" border="0" alt="RMG Living">
				<img src="/images/logos/<?=$row[0]?>" style="position:absolute; left:50%; margin-left:245px; margin-top:23px; z-index:10;" border="0" >
				<?
			}
			else{
				?>
				<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">
				<?
			}
		}
		else{
			?>
			<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">
			<?
		}
		*/
		?>
		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
  <td>
  <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
  <tr>
    <td height="10" align="center" valign="bottom" bgcolor="#E9EEF4" style="border-bottom:1px solid #c1d1e1;border-top:1px solid #c1d1e1;"><img src="images/templates/header_bar_grad2.jpg" alt=""></td>
  </tr>
  <tr>
  <td height="8" align="center" valign="top" style="border-top:1px solid #ffffff;"><img src="images/templates/header_bar_grad3.jpg" alt=""></td>
  </tr>
  </table>
  </td>
  </tr>
 
  <tr>
    <td height="4"></td>
  </tr>
  <tr>
    <td><table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="28" valign="top">
	
	<table width="760" cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td>
	<!-- InstanceBeginEditable name="breadcrumbs" --><a href="/building_details.php" class="crumbs">Your Community</a>&nbsp;>&nbsp;<a href="general_info.php" class="crumbs">General Info</a>&nbsp;>&nbsp;Director Responsibilities<!-- InstanceEndEditable -->
	</td>
	<td style="text-align:right;" nowrap>
	<? if(!empty($_SESSION['resident_session'])){?><a href="index.php?logoff=Y" class="crumbs">Log Off</a><? }?>
	</td>
	</tr>
	</table>
	
	</td>
  </tr>
</table>
</td>
  </tr>
  <tr>
    <td><!-- InstanceBeginEditable name="body" -->
      <table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td style="border-bottom:1px solid #003366;"><table width="760" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="15"><img src="images/dblue_box_top_left_corner.jpg" width="15" height="33"></td>
              <td width="730" bgcolor="#416CA0" style="border-top:1px solid #003366; vertical-align:middle"><img src="images/general_info/general_info_symbol.jpg" width="22" height="22" style="vertical-align:middle;">&nbsp;&nbsp;<span class="box_title">Director Responsibilities</span></td>
              <td width="15" align="right"><img src="images/dblue_box_top_right_corner.jpg" width="15" height="33"></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td><table width="758" border="0" cellspacing="0" cellpadding="15" style="border-left:1px solid #7ea0bd;border-right:1px solid #7ea0bd;border-top:1px solid #7ea0bd;border-bottom:1px solid #cccccc;">
         
			<tr>
              <td width="380" valign="top">
<table width="348" border="0" cellspacing="0" cellpadding="0">
                
<tr>
                  
<td width="260" height="1"></td>
<td width="88"></td>
</tr>
<tr valign="top">
                  
<td colspan="2"><p>RMG  are frequently asked what are the responsibilities and obligations of a resident director of a Management Company.  Please find below some general guidance in order to give a greater degree of understanding.</p>
  <p>
  <ul>
  <li><a href="#Accountability" class="link036">Accountability</a></li>
  <li><a href="#Auditors" class="link036">Auditors</a></li>
  <li><a href="#Company Continuity" class="link036">Company Continuity</a></li>
  <li><a href="#Company Law and Penalties" class="link036">Company Law and Penalties</a></li>
  <li><a href="#Continuity" class="link036">Continuity</a></li>
  <li><a href="#Contract" class="link036">Contract</a></li>
  <li><a href="#Control" class="link036">Control</a></li>
  <li><a href="#Dissolution" class="link036">Dissolution</a></li>
  <li><a href="#Health & Safety" class="link036">Health & Safety</a></li>
  <li><a href="#Individual Responsibility" class="link036">Individual Responsibility</a></li>
  <li><a href="#Insurance" class="link036">Insurance</a></li>
  <li><a href="#Lease Compliance" class="link036">Lease Compliance</a></li>
  <li><a href="#Lease Terms" class="link036">Lease Terms</a></li>
  <li><a href="#Limited Liability" class="link036">Limited Liability</a></li>
  <li><a href="#Maintenance Charges" class="link036">Maintenance Charges</a></li>
  <li><a href="#Managing Agents" class="link036">Managing Agents</a></li>
  <li><a href="#Records" class="link036">Records</a></li>
  <li><a href="#Registered Office" class="link036">Registered Office</a></li>
  <li><a href="#Reserve Funding" class="link036">Reserve Funding</a></li>
  <li><a href="#Shared Responsibility" class="link036">Shared Responsibility</a></li>
  <li><a href="#Solicitors Enquiries" class="link036">Solicitors Enquiries</a></li>
  <li><a href="#Statutes" class="link036">Statutes</a></li>
  </ul>
  </td>
</tr>
              </table>
			  
			  </td>
              <td valign="top" background="images/general_info/knowing_directors.jpg" style="background-repeat:no-repeat;"><p><img src="images/spacer.gif" alt="Knowning the responsibilities of your Directors" width="348" height="310">
                </p>
              </td>
              </tr>
			  <tr><td colspan="2"><table width="726" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="260" height="1"></td>
                  <td width="88"></td>
                </tr>
                <tr>
                  <td colspan="2"><p><span class="text036" id="Accountability"><strong>Accountability</strong></span><br>
        A resident Director may be held accountable for the adverse consequences arising from the activity, or inactivity, of fellow Directors.
	                    <div align="right"><a href="#" class="link416CA0" style="text-decoration:underline;">Back to top</a></div>
	                  <p></p>
	                  <p><span class="text036" id="Auditors"><strong>Auditors</strong></span><br>
        The Company&rsquo;s Auditors are usually initially appointed prior to the Company&rsquo;s release/transfer to residential control. It should be noted that the Auditors are not subject to dismissal or replacement by the Resident Directors, but may only be subject to replacement at a General Meeting of the Company.
	                    <div align="right"><a href="#" class="link416CA0" style="text-decoration:underline;">Back to top</a></div>
	                  <p></p>
	                  <p><span class="text036" id="Company Continuity"><strong>Company Continuity</strong></span><br>
        The Resident Directors are responsible for ensuring the continuity of the Company. Whilst the Directors have the right to resign from office whenever they wish, steps must always be taken to ensure that the Company in terms of its members or shareholders are advised, where possible, of the number of Directors reducing below the permitted minimum, as set out in the Memorandum & Articles of Association. If such an event transpires, the calling of a General Meeting should be implemented for the purpose of clarification to the flat owners whilst enabling new Directors to be formally appointed. Failure to ensure adequate procedures for continuity could leave those outgoing Directors being responsible for any future collapse of the Company, which may result in the incurring of costs, Companies House fines and Court penalties.
	                    <div align="right"><a href="#" class="link416CA0" style="text-decoration:underline;">Back to top</a></div>
	                  <p></p>
	                  <p><span class="text036" id="Company Law and Penalties"><strong>Company Law and Penalties</strong></span><br>
	                  	The Directors are responsible for ensuring that the Company complies with its statutory obligations under Company Law ie. submission of Annual Returns within 28 days of the due date, filing Annual Accounts within nine months of the end of the financial year etc. The Management Company would be fined on a fixed penalty system ranging from &pound;150 to &pound;1,500 for late submissions and the Resident Directors may be liable to further penalties imposed by the Courts. If accounts are filed late in consecutive 
	                    years the fines are doubled.
	                  <div align="right"><a href="#" class="link416CA0" style="text-decoration:underline;">Back to top</a></div>
	                  <p></p>
	                  <p><span class="text036" id="Continuity"><strong>Continuity</strong></span><br>
        Directors can and will change with a steady process of new appointments and retirements, although the functions of the Company continues regardless. In this respect, contracts entered into by Directors at any given time on behalf of the Company will continue to be binding upon the Company and subsequent Directors for the term of such contracts. The fact that the current Directors at any time were not a party to the original contract has no bearing upon the validity of the contract with the Management Company.
	                    <div align="right"><a href="#" class="link416CA0" style="text-decoration:underline;">Back to top</a></div>
	                  <p></p>
	                  <p><span class="text036" id="Contract"><strong>Contract</strong></span><br>
        There should be an understanding and awareness of the contractual responsibilities placed open the Management Company by the terms of the Lease, together with the understanding that the terms of the Lease are in fact a contract. The terms of the contract may not be varied, except insofar as any contract may be varied by the agreement of all the parties to that contract. In practice, this means that for a Lease to be varied requires the active co-operation of ALL of the residents, with a Deed of Variation being entered into by each flat owner, together with the requisite approval of the freeholder.
	                    <div align="right"><a href="#" class="link416CA0" style="text-decoration:underline;">Back to top</a></div>
	                  <p></p>
	                  <p><span class="text036" id="Control"><strong>Control</strong></span><br>
        The Directors are not a committee. They are the Directors of a Limited Company with all the authority, control and responsibility arising. They are only REQUIRED to report to or consult with Residents (in their capacity as Members or Shareholders) at the General Meetings (normally the Annual General Meeting) of the Company).
	                    <div align="right"><a href="#" class="link416CA0" style="text-decoration:underline;">Back to top</a></div>
	                  <p></p>
	                  <p><span class="text036" id="Dissolution"><strong>Dissolution</strong></span><br>
        A Management Company may be struck off and dissolved by Companies house for not complying with its statutory obligations. In the event of such an occurrence, the Resident Directors may be held personally responsible for all the consequences and for the costs involved in reinstatement. Apart from reinstatement costs, the penalties for late filing of Accounts/Annual Returns may still be applicable.
	                    <div align="right"><a href="#" class="link416CA0" style="text-decoration:underline;">Back to top</a></div>
	                  <p></p>
	                  <p><span class="text036" id="Health & Safety"><strong>Health & Safety</strong></span><br>
        The Management Company is responsible for ensuring risk assessments are undertaken to the communal areas, as these areas are deemed &ldquo;places of work&rdquo; under the Health & Safety at Work Act, with criminal sanctions imposed for non-compliance.
                      <div align="right"><a href="#" class="link416CA0" style="text-decoration:underline;">Back to top</a></div>
                      <p></p>
                      <p><strong class="text036" id="Individual Responsibility">Individual Responsibility</strong><br>
        Individual Resident Directors must take care not to grant approval or authority to a resident or any other person in the name of the Management Company, without the overall approval or a majority of the Resident Directors. If any such approval is given, the resident concerned would probably be entitled to rely on that approval or permission. If the Resident Directors do not collectively approve, or if the approval/permission exceeded the authority of the Management Company, that individual Director may find they are personally responsible for any consequential loss or damage suffered (by either the Management Company), the resident, or other individual or Company involved). 
                      <div align="right"><a href="#" class="link416CA0" style="text-decoration:underline;">Back to top</a></div>
                      <p></p>
                      <p><span class="text036" id="Insurance"><strong>Insurance</strong></span><br>
        The Directors of have a responsibility to ensure that the premises are adequately insured, both in terms of the sum insured and the quality of the insurance policy. Where any change is made in the insurance cover it is prudent to ensure that any savings which might be achieved are not made at the expense of quality cover.
	                    <div align="right"><a href="#" class="link416CA0" style="text-decoration:underline;">Back to top</a></div>
	                  <p></p>
	                  <p><span class="text036" id="Lease Compliance"><strong>Lease Compliance</strong></span><br>
        The Directors must ensure that the Company complies with its contractual requirements as identified within the Lease. However, it is also a pre-emptive condition that flat owners must themselves be providing the necessary funds to enable the Management Company to in turn discharge its obligations. The Resident Directors do not have the authority to either diminish or increase the Company&rsquo;s contractual obligations. Again, they may be held financially responsible in the event of any such variation, and for the financial consequences which may arise.
				        <div align="right"><a href="#" class="link416CA0" style="text-decoration:underline;">Back to top</a></div>
					      <p></p>
					      <p><span class="text036" id="Lease Terms"><strong>Lease Terms</strong></span><br>
        The Resident Director should prudently read, understand and become acquainted with the terms of the Lease and with the rights and responsibilities imposed upon the Management Company itself.
                        <div align="right"><a href="#" class="link416CA0" style="text-decoration:underline;">Back to top</a></div>
	                      <p></p>
	                      <p><span class="text036" id="Limited Liability"><strong>Limited Liability</strong></span><br>
        Whilst the company enjoys the status and authority of being a Limited Company this does not leave the Resident Directors in a position of having no personal responsibility for any adverse consequences arising from their period of office. In the event of it being shown that the Directors acted either improperly or where there has been negligence, neglect, or default then those Directors may be held personally responsible for the consequences. However, since the Finance Act 1989, it has been possible to obtain Directors and Officers Insurance. RMG have themselves arranged for a policy to be available specifically for the purpose of providing protection for the Resident Directors or Management Companies. Advice in respect of probable cost is available upon request.
                        <div align="right"><a href="#" class="link416CA0" style="text-decoration:underline;">Back to top</a></div>
	                      <p></p>
	                      <p><strong class="text036" id="Maintenance Charges">Maintenance Charges</strong><br>
        It is the Directors who will determine the level of Maintenance Charge which is to be set from year to year and for ensuring adequate financial provision is made for the Management Company&rsquo;s obligations. This may require the approval of the Company or Residents at large at an AGM or they may themselves have complete authority to set the Charge; this provision varies according to the Lease provision for each Company. However, the Resident Directors are also responsible for ensuring that Residents are duly advised of the Company&rsquo;s actual expenses after the year end with a certified statement, at which time any under &ndash;funding or over-funding is either collected from or credited to the flat owners concerned.
                        <div align="right"><a href="#" class="link416CA0" style="text-decoration:underline;">Back to top</a></div>
	                      <p></p>
	                      <p><span class="text036" id="Managing Agents"><strong>Managing Agents</strong></span><br>
        The Resident Directors have the authority to appoint Managing Agents to undertake the management of the administration of the Company&rsquo;s affairs under the direction of the Resident Directors. Not only should the Agents be able to properly advise the Resident Directors with regard to the presence of the various Acts and ensure that the Management Company complies with the statutory implications, but they should also draw the attention of the Resident Directors to those occasions where they may be exceeding their authority under the terms of the Lease, to the risk of both themselves and the Management Company.
			            <div align="right"><a href="#" class="link416CA0" style="text-decoration:underline;">Back to top</a></div>
						        <p></p>
						        <p><span class="text036" id="Records"><strong>Records</strong></span><br>
        The Resident Directors should ensure that a record of their Meetings is maintained. RMG will keep notes of such Meetings themselves when they are in attendance, but the Resident Directors may wish to prepare their own Minutes instead, or on other occasions. Copies of such Minutes should be forwarded through to the Managing Agent for retention with the Company&rsquo;s other records.
                                <div align="right"><a href="#" class="link416CA0" style="text-decoration:underline;">Back to top</a></div>
	                            <p></p>
	                            <p><strong class="text036" id="Registered Office">Registered Office</strong><br>
        The Directors must always have an awareness of the location of the Registered Office of their Company. All formal documentation etc is likely to be sent to the Registered Office ie. Companies House correspondence, enquiries from the Inland Revenue etc. The Registered Office is the formal legal address and any individual or organization which writes to that address is deemed to be in communication. If the Directors are not aware of what is being received at that address and no action is being taken with regard to such communications, the Directors may be held to be negligent and sued for any adverse consequences which may then arise.
                        <div align="right"><a href="#" class="link416CA0" style="text-decoration:underline;">Back to top</a></div>
	                            <p></p>
	                            <p><span class="text036" id="Reserve Funding"><strong>Reserve Funding</strong></span><br>
        This can be an area of contention between Resident Directors and Residents. It is a term within your Lease or Transfer, normally subject to some discretion by the Directors, that the Company may ser aside such sum from year to year as may be considered necessary to meet longer terms responsibilities of repair and decoration. It must be borne in mind how the level of Reserve provision may affect the funding of work when it is needed (or whether I can actually be undertaken). Allowance needs to be given for unexpected costs which may throw a budget off course and cash availability may be affected by arrears of Maintenance Charges. Additionally, the level of Reserve funding may affect the 'saleability' of a flat. The Accounts for your Company should be examined by your prospective purchasers&rsquo; professional advisers. A recommendation may be made for a retention of part of a mortgage advance equal to the probable cost of works if the Building Society is not satisfied sufficient reserves are present.
			                    <div align="right"><a href="#" class="link416CA0" style="text-decoration:underline;">Back to top</a></div>
						        <p></p>
						        <p><strong class="text036" id="Shared Responsibility">Shared Responsibility</strong><br>
        All Directors are held to be equally responsible for the actions (and any inaction) on the part of the Company. If an individual appointed as a Director, remains in office but does not participate in the decision-making process, they would still be as much responsible for the consequence of such decisions, as those who were actually involved. It is not therefore prudent for those who cannot afford the time or are otherwise regularly unavailable, to continue in office.
                        <div align="right"><a href="#" class="link416CA0" style="text-decoration:underline;">Back to top</a></div>
	                            <p></p>
	                            <p><strong class="text036" id="Solicitors Enquiries">Solicitors Enquiries</strong><br>
        At the time of sale, solicitors will (should) make an enquiry of the Management Company on the matter of outstanding Maintenance Charges etc. It is essential replies are forthcoming. However, the solicitors may also raise request for approval of matters over which the Directors either do not have the authority or would otherwise be most unwise to grant without seeking appropriate advice. Caution needs to be exercised, particularly if the solicitors are seeking any change in documentation.
                        <div align="right"><a href="#" class="link416CA0" style="text-decoration:underline;">Back to top</a></div>
	                            <p></p>
	                            <p><strong class="text036" id="Statutes">Statutes</strong><br>
        The Resident Directors need to act in accordance with the obligations and requirements placed upon them not only by Company Law, but also the various Housing Acts and Landlord and Tenant Acts. (The professional advisers to the Company should have the necessary awareness of the Various Acts to be able to assist and advise the Resident Directors).
                        <div align="right"><a href="#" class="link416CA0" style="text-decoration:underline;">Back to top</a></div>
	                            <p></p></td>
                </tr>
              </table></td></tr>
          </table></td>
        </tr>
		<tr><td height="5" bgcolor="#416CA0" style="border:1px solid #003366;"><img src="images/spacer.gif" alt=""></td>
		</tr>
      </table>
    <!-- InstanceEndEditable --></td>
  </tr>
  <tr>
    <td height="25"></td>
  </tr>
  <tr>
    <td><table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="445" height="14"><a href="about_us.php" class="link416CA0">About RMG Living</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="contact_us.php" class="link416CA0">Contact Us</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="privacy.php" class="link416CA0">Legal Information</a></td>
        <td width="315" align="right">Copyright <?=date("Y", time())?> &copy; <a href="http://www.rmgltd.co.uk">Residential Management Group Ltd</a></td>
      </tr>
	  <tr><td colspan="2">&nbsp;</td></tr>
	  <tr><td colspan="2" style="text-align:left;"><p>&nbsp;</p>
	    </td>
	  </tr>
    </table></td>
  </tr> 
  <tr><td>&nbsp;</td></tr>
</table>
</body>
<!-- InstanceEnd --></html>
