<?
//print "HERE";
//exit;
require("utils.php");
require_once($UTILS_CLASS_PATH."encryption.class.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."rmc.class.php");
require_once($UTILS_CLASS_PATH."resident.class.php");
require_once($UTILS_CLASS_PATH."security.class.php");
$security = new security;
$website = new website;
$crypt = new encryption_class;
$rmc = new rmc;
$resident = new resident($_SESSION['resident_num']);


// Determine if allowed access into 'your community' section
$website->allow_community_access();

// Set RMC
$rmc->set_rmc($_SESSION['rmc_num']);


// Get value of 'is_first_login' from residents_extra table
$sql_resident = "SELECT e.*, l.resident_ref, s.subsidiary_name, r.resident_email
FROM cpm_residents_extra e
INNER JOIN cpm_lookup_residents l ON l.resident_lookup = e.resident_num
INNER JOIN cpm_subsidiary s ON l.subsidiary_id = s.subsidiary_id
INNER JOIN cpm_residents r on r.resident_num = e.resident_num
WHERE e.resident_num = '".$security->clean_query($_SESSION['resident_num'])."'";
$result_resident = @mysql_query($sql_resident);
$row_resident = @mysql_fetch_array($result_resident);


if($_REQUEST['whichaction'] == "save_info"){
	
	$password_error = "N";
	
	if( preg_match("/[^-a-z0-9_]/i", $_REQUEST['new_password']) ){
		$password_error = "Y";	
	}
	
	if($password_error == "N"){
	
		if($_REQUEST['optout_marketing'] != "Y"){$_REQUEST['optout_marketing'] = "N";}
	
		$save_error = "N";
	
		// Check email is unique
		if( $resident->is_email_unique($_REQUEST['email']) === true ){
			
			if($resident->email != $_REQUEST['email'] || $resident->tel != $_REQUEST['tel'] || $resident->mobile != $_REQUEST['mobile']){
				
				$detail = "";
				
				if($resident->email != $_REQUEST['email']){
					$detail .= "
					Email from: ".$resident->email." to ".$_REQUEST['email'] . "
					Qube email is: " . $resident->resident_email;
				}
				
				if($resident->tel != $_REQUEST['tel']){
					$detail .= "
					Telephone from: ".$resident->tel." to ".$_REQUEST['tel'];
				}
				
				if($resident->mobile != $_REQUEST['mobile']){
					$detail .= "
					Mobile from: ".$resident->mobile." to ".$_REQUEST['mobile'];
				}
				
				$msg = "
				Some details for ".$resident->resident_ref." on " . $resident->subsidiary_name ." have changed:
				$detail	
				
				(This email was generated automatically.)
				";
				
				$sql = "INSERT INTO cpm_mailer SET
					mail_type = 'RMG Living Details Update',
					mail_to = 'customerservice@rmguk.com',
			
					mail_from = 'customerservice@rmguk.com',
					mail_subject = 'RMG Living Details Update',
					mail_message = '".$msg."'";
					
				@mysql_query($sql);
			}
	
			$sql = "
			UPDATE cpm_residents_extra SET
			tel = '".$security->clean_query($_REQUEST['tel'])."',
			mobile = '".$security->clean_query($_REQUEST['mobile'])."',
			email = '".$security->clean_query($_REQUEST['email'])."',
			question_id_1 = ".$security->clean_query($_REQUEST['security_question_1']).",
			question_id_2 = ".$security->clean_query($_REQUEST['security_question_2']).",
			answer_1 = '".$security->clean_query($_REQUEST['security_answer_1'])."',
			answer_2 = '".$security->clean_query($_REQUEST['security_answer_2'])."',
			optout_marketing = '".$security->clean_query($_REQUEST['optout_marketing'])."',
			";
			if(preg_match("/127.0.0.1/", $_SERVER['SERVER_NAME']) !== 1 && $_REQUEST['new_password'] != ""){
			$sql .= "password = '".$crypt->encrypt($UTILS_DB_ENCODE, trim($_REQUEST['new_password']))."',";
			}
			$sql .= "
			allow_password_reset = 'N',
			password_to_be_sent = 'N',
			is_first_login = 'N',
			announce_optin = '".$security->clean_query($_REQUEST['announce_optin'])."',
			survey_optout = '".$security->clean_query($_REQUEST['optout_surveys'])."'
			WHERE resident_num = ".$security->clean_query($_SESSION['resident_num']);
			@mysql_query($sql) or $save_error = "Y";
		}
		else{
			$save_error = "Y";
		}
	}
	
}

// If first logon, re-direct to your community page
if($row_resident['is_first_login'] == "Y"){
	
	$starttime = time();
	
	$sql = "UPDATE cpm_residents_extra SET
	first_login = '".date("H:i d/m/Y", $starttime)."'
	WHERE resident_num = '".$security->clean_query($_SESSION['resident_num'])."'";
	@mysql_query($sql) or $save_error = "Y";
	
	if($_REQUEST['whichaction'] == "save_info" && $save_error == "N"){
		print "<script language='javascript'>location.replace('".$UTILS_HTTPS_ADDRESS."building_details.php');</script>";
		exit;
	}
}

if($_REQUEST['whichaction'] != "save_info" || ($_REQUEST['whichaction'] == "save_info" && $save_error == "N")){
	$_REQUEST['security_question_1'] = $row_resident['question_id_1'];
	$_REQUEST['security_answer_1'] = $row_resident['answer_1'];
	$_REQUEST['security_question_2'] = $row_resident['question_id_2'];
	$_REQUEST['security_answer_2'] = $row_resident['answer_2'];
	$_REQUEST['tel'] = $row_resident['tel'];
	$_REQUEST['mobile'] = $row_resident['mobile'];
	$_REQUEST['email'] = $row_resident['email'];
	
	if($resident->allow_password_reset == "Y"){
		$_REQUEST['new_password'] = "";
		$_REQUEST['new_password_confirm'] = "";
	}
	else{
		$_REQUEST['new_password'] = $crypt->decrypt($UTILS_DB_ENCODE, stripslashes($row_resident['password']));
		$_REQUEST['new_password_confirm'] = $crypt->decrypt($UTILS_DB_ENCODE, stripslashes($row_resident['password']));
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>RMG Living - Change Details</title>
	<link href="/css/reset.css" rel="stylesheet" type="text/css" />
	<!--<link href="styles.css" rel="stylesheet" type="text/css">-->
	<link href="/css/common.css" rel="stylesheet" type="text/css" />
	<!--[if lte IE 8]> 
	<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<!--[if lte IE 7]> 
	<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
	<![endif]-->
	
	<script src="library/jscript/functions/valid_email_check.js"></script>

	<script type="text/JavaScript">
	<!--
	function save_info(){
		
		var bad="N";
		
		if(document.getElementById('new_password').value == ""){bad="Y";}
		if(document.getElementById('new_password_confirm').value == ""){bad="Y";}
		
		// Check password length is suitable
		if(document.getElementById('new_password').value.length < 8 || document.getElementById('new_password').value.length > 16){
			alert("The new password you specify must be between 8-16 characters long.");
			document.getElementById('new_password').value = "";
			document.getElementById('new_password_confirm').value = "";
			return false;
		}
		
		// Check that both passwords match
		if(document.getElementById('new_password').value != document.getElementById('new_password_confirm').value){
			alert("The new password you specified in the 'New Password' box does not match\nthe one you typed in the 'Confirm New Password' box.");
			document.getElementById('new_password').value = "";
			document.getElementById('new_password_confirm').value = "";
			return false;
		}
		
		// Check password contains one number
		if(document.getElementById('new_password').value.match(/[0-9]/) == null){
			alert("Your new password must contain at least one number.");
			return false;
		}
		
		// Make sure the two questions aren't the same
		if(document.getElementById('security_question_1').value == document.getElementById('security_question_2').value){
			alert("Please choose two different security questions.");
			return false;
		}
		
		// Check for question type and do appropriate action
		if(document.getElementById('security_question_1').value == "4"){
			if(!check_dob(document.getElementById('security_answer_1').value)){return false;}
		}
		else{
			if(document.getElementById('security_question_1').value == "-"){bad="Y";}
		}
		if(document.getElementById('security_question_2').value == "4"){
			if(!check_dob(document.getElementById('security_answer_2').value)){return false;}
		}
		else{
			if(document.getElementById('security_question_2').value == "-"){bad="Y";}
		}
		
		// Do remainder of fields checks
		if(document.getElementById('security_answer_1').value == ""){bad="Y";}
		if(document.getElementById('security_answer_2').value == ""){bad="Y";}
		if(document.getElementById('tel').value == ""){bad="Y";}
		if(document.getElementById('email').value == ""){bad="Y";}
		if(bad == "Y"){
			alert("Please fill in all fields marked with *");
			return false;
		}
		
		if(!valid_email_check(document.getElementById('email').value)){
			alert("Please enter a valid email address.");
			return false;
		}
		
		return true;
	}
	
	function check_dob(val){
		if(!val.match("../../....")){
			alert("Your date of birth needs to be in the format dd/mm/yyyy");
			return false;
		}
		return true;
	}
	
	//-->
	</script>

	<script type='text/javascript' src="<?=$UTILS_HTTPS_ADDRESS?>library/jscript/jquery-1.6.2.min.js"></script>
	<? require_once($UTILS_FILE_PATH."includes/analytics.php");?>
</head>
<body>

	<div id="wrapper">
		
		<? require_once($UTILS_FILE_PATH."includes/header.php");?>
		
		<div id="content">
			
			<table width="760" cellspacing="0">
				<tr>
					<td>
						<? if($row_resident['is_first_login'] == "N"){?><a href="building_details.php" class="crumbs">Your Community</a>&nbsp;>&nbsp;My Account&nbsp;>&nbsp;My Details<? }?>
					</td>
					<td style="text-align:right;" nowrap="nowrap">
						<? if(!empty($_SESSION['resident_session'])){?><a href="index.php?logoff=Y" class="crumbs">Log Off</a><? }?>
					</td>
				</tr>
			</table>
						
			<?
			
			// Prevent seeing the 'My Properties' tabe is Director/Sub-tenant account
			if( $resident->is_resident_director == "Y" || $resident->is_subtenant_account == "Y" ){
			?>
			<table width="760" cellspacing="0" style="clear:both; margin-top:13px;" id="content_box_1_top">
				<tr>
					<td width="15"><img src="/images/dblue_box_top_left_corner.jpg" width="15" height="33" /></td>
					<td width="730" style="border-top:1px solid #003366; background-color:#416CA0; vertical-align:middle"><img src="/images/person_icon.png" width="22" height="22" style="vertical-align:middle;" />&nbsp;&nbsp;<span class="box_title">My Details</span></td>
					<td width="15" style="text-align:right;"><img src="/images/dblue_box_top_right_corner.jpg" width="15" height="33" /></td>
				</tr>
			</table>
			<? }else{?>
			<table width="760" cellspacing="0" style="clear:both; margin-top:13px;">
    			<tr>
    				<td width="15"><img src="images/dblue_box_top_left_corner.jpg" width="15" height="33" /></td>
    				<td width="348" style="background-color:#426B9F;border-top:1px solid #003366; vertical-align:middle"><img src="images/person_icon.png" width="22" height="22" style="vertical-align:middle;margin-right:8px;" /><span class="box_title">My Details</span></td>
    				<td width="34"><img src="images/your_community/tab_join.jpg" width="34" height="33" /></td>
    				<td width="348" style="background-color:#669ACC;border-top:1px solid #6699cc; vertical-align:middle"><img src="images/property_icon.png" width="22" height="22" style="vertical-align:middle;margin-right:8px;" /><a href="my_properties.php" class="box_title linkbox">My Properties</a></td>
    				<td width="15" style="text-align:right;" align="right"><img src="images/lblue_box_top_right_corner.jpg" width="15" height="33" /></td>
    			</tr>
    		</table>
			<? }?>
	
			
			<div class="content_box_1">
			
				<form action="<?=$UTILS_HTTPS_ADDRESS?>change_details.php" name="form1" id="form1" method="post">
				
					<table class="table_1" width="727" cellspacing="0">
						<?
						if($row_resident['is_first_login'] == "Y"){
							?>
							<tr>	
								<td colspan="2">As this is the first time you have logged in under this account, please complete the form below. You will only have to fill in these details once for this account and you can ammend them at any point from the 'My Account' section.</td>
							</tr>
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
							<?
						}
						?>
						
						<?
						if($_REQUEST['whichaction'] == "save_info" && $password_error == "Y"){
							?>
							<tr>
								<td colspan="2" style="vertical-align:middle;"><img src="images/del_16.gif" style="vertical-align:middle;">&nbsp;&nbsp;<span class="msg_fail"><b>Your password can only be made up of numbers, letters, underscores and hyphens.</b></span></td>
							</tr>	
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
							<?
						}
						elseif($_REQUEST['whichaction'] == "save_info" && $save_error == "Y"){
							?>
							<tr>
								<td colspan="2" style="vertical-align:middle;"><img src="images/del_16.gif" style="vertical-align:middle;">&nbsp;&nbsp;<span class="msg_fail"><b>There was a problem saving your details.  Please contact <a href="mailto:customerservice@rmguk.com"><span class="msg_fail" style="text-decoration:underline;">customerservice@rmguk.com</span></a> .</b></span></td>
							</tr>
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
							<?
						}
						elseif($_REQUEST['whichaction'] == "save_info" && $save_error == "N"){
							?>
							<tr>
								<td colspan="2" style="vertical-align:middle;"><img src="images/tick_20.gif" width="20" height="20" style="vertical-align:middle;">&nbsp;&nbsp;<span class="msg_success"><b>Your details have been saved. <a href="/building_details.php">Click here to proceed to the 'Your Community' page.</a></b></span></td>
							</tr>
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
							<?
						}
						?>
						<tr>
							<td colspan="2"><span class="subt036"><b>Your Password</b></span></td>
						</tr>
						<tr>
							<td colspan="2" height="5"></td>
						</tr>
						<tr>
							<td colspan="2"><? if($row_resident['is_first_login'] == "Y"){?>Please set yourself a new password of your choice. <? }?>Your new password must be between 8-16 characters in length and contain at least one number. Your password can also only be made up of numbers, letters, underscores and hyphens.</td>
						</tr>
						<tr>
							<td width="155">&nbsp;</td>
							<td width="572">&nbsp;</td>
						</tr>
						<tr>
							<td>New password * </td>
							<td><input name="new_password" type="password" id="new_password" value="<?=$_REQUEST['new_password']?>" size="18" maxlength="16" autocomplete="off"></td>
						</tr>
						<tr>
							<td nowrap>Confirm new password&nbsp;*</td>
							<td><input name="new_password_confirm" type="password" id="new_password_confirm" value="<?=$_REQUEST['new_password_confirm']?>" size="18" maxlength="16" autocomplete="off"></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td><span class="subt036"><b>Security Questions</b></span></td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2" height="5"></td>
						</tr>
						<tr>
							<td colspan="2">Please answer  the security questions below. These may be used to verify you against our records if you call our Customer Services department, or if you forget your password.</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>Question 1 * </td>
							<td>
								<select name="security_question_1" id="security_question_1">
								<option value="-" selected>-</option>
								<? 
								$sql_sq = "SELECT * FROM cpm_security_questions";
								$result_sq = @mysql_query($sql_sq);
								while($row_sq = @mysql_fetch_array($result_sq)){
									?>
									<option value="<?=$row_sq['question_id']?>" <? if($_REQUEST['security_question_1'] == $row_sq['question_id']){print "selected";}?>><?=$row_sq['question']?></option>
									<?
								}
								?>
								</select>
							</td>
						</tr>
						<tr>
							<td>Answer 1 * </td>
							<td><input name="security_answer_1" type="text" id="security_answer_1" value="<?=$_REQUEST['security_answer_1']?>"></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>Question 2 * </td>
							<td><select name="security_question_2" id="security_question_2">
								<option value="-" selected>-</option>
								<? 
								$sql_sq = "SELECT * FROM cpm_security_questions";
								$result_sq = @mysql_query($sql_sq);
								while($row_sq = @mysql_fetch_array($result_sq)){
									?>
									<option value="<?=$row_sq['question_id']?>" <? if($_REQUEST['security_question_2'] == $row_sq['question_id']){print "selected";}?>>
									<?=$row_sq['question']?>
									</option>
									<?
								}
								?>
								</select>
							</td>
						</tr>
						<tr>
							<td>Answer 2 * </td>
							<td><input name="security_answer_2" type="text" id="security_answer_2" value="<?=$_REQUEST['security_answer_2']?>"></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td><span class="subt036"><b>Contact Details</b></span></td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2" height="5"></td>
						</tr>
						<tr>
							<td>Home telephone * </td>
							<td><input name="tel" type="text" id="tel" value="<?=$_REQUEST['tel']?>"></td>
						</tr>
						<tr>
							<td>Mobile telephone</td>
							<td><input name="mobile" type="text" id="mobile" value="<?=$_REQUEST['mobile']?>"></td>
						</tr>
						<tr>
							<td>Email address * </td>
							<td><input name="email" type="text" id="email" size="40" value="<?=$_REQUEST['email']?>"></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td><span class="subt036"><strong>Announcements</strong></span></td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2" height="5"></td>
						</tr>
						<tr>
							<td colspan="2"><input type="checkbox" name="announce_optin" id="announce_optin" value="Y" <? if($row_resident['announce_optin'] == "Y"){?>checked<? }?> /> 
								Send me announcements regarding <?=$rmc->rmc['rmc_name']?>, for example, notification about on-site maintenance.</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2">We may contact you from time to time to inform you about offers we think may be of interest to you as a home owner. If you do not wish to receive this information, please tick the box.
								<input type="checkbox" name="optout_marketing" id="optout_marketing" value="Y" <? if($row_resident['optout_marketing'] == "Y"){?>checked<? }?> /></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2">We may send you a survey from time to time to get your opinion on how we are doing at managing your property. If you do not wish to receive this information, please tick the box.
								<input type="checkbox" name="optout_surveys" id="optout_surveys" value="Y" <? if($row_resident['survey_optout'] == "Y"){?>checked<? }?> /></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td><a href="Javascript:;" onClick="if(save_info()){document.form1.submit();};return false;"><img name="save_button" id="save_button" src="images/your_details/save_button.jpg" width="50" height="20" border="0" /></a></td>
							<td>&nbsp;</td>
						</tr>
					</table>
					<input type="hidden" name="whichaction" id="whichaction" value="save_info" alt="Click to confirm changes" />
				
				</form>
			
			</div>			
			
		</div>
		
		<? require_once($UTILS_FILE_PATH."includes/footer.php");?>
		
	</div>
		
</body>
</html>
