<?
require_once("utils.php");
require_once($UTILS_CLASS_PATH."template/paperlessTemplate.php");

if( isset( $_SESSION['lastName'] ) ) {
	$template = new \template\paperlessTemplate();
	$template->set('scriptExtra', 'addRow( "' . $_SESSION['unit'] . '", "' . $_SESSION['postcode'] . '");');
	$template->set('paperless', $_SESSION['optIn'] );
	$template->set('name', $_SESSION['title'] . ' ' . $_SESSION['lastName'] );
	$template->setPage( 'paperless', 'Paperless Service' );
	$template->getFile( $UTILS_FILE_PATH . 'paperless/designs/modal.tpl', 'modal' );
	echo $template->fetch();
} else {
	header('Location: /paperless/index.php');
}

