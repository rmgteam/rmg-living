<div class="modal modal-rmg fade pg-show-modal" id="modal1" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form role="form" id="modalForm">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
						<i class="fa fa-remove"></i>
					</button>
					<h4 class="modal-title">Add Unit</h4>
				</div>
				<div class="modal-body">
					<div class="form-group has-no-valid">
						<div class="input-group">
							<input type="text" class="form-control rounded" id="verifyTenantRefInput" name="verifyTenantRefInput" placeholder="Enter your tenant reference" required>
							<span class="input-group-addon"><i class="glyphicon glyphicon-warning-sign"
															   data-toggle="popover" data-placement="top"
															   data-content="This is not a required field"
															   data-title="Requirement"></i></span>
						</div>
					</div>
					<div class="form-group has-no-valid">
						<div class="input-group">
							<input type="text" class="form-control postcode" id="verifyPostcodeInput" name="verifyPostcodeInput" placeholder="Enter your Postcode" required>
							<a id="checkPostcode" class="input-group-addon btn btn-sm btn-rmg btn-addon">Find</a>
							<span class="input-group-addon"><i class="glyphicon glyphicon-warning-sign"
															   data-toggle="popover" data-placement="top"
															   data-content="This is not a required field"
															   data-title="Requirement"></i></span>
						</div>
					</div>
					<div class="panel panel-primary" id="unitSelectPnl" style="display: none;">
						<div class="panel-heading"><h2 class="panel-title">Please Select Your Unit</h2></div>
						<div class="panel-body">
							<div class="form-group has-no-valid">
								<div class="input-group">
									<select class="form-control rounded" id="verifyUnitInput" name="verifyUnitInput" placeholder="Select a Unit" required>
										<option value="">Please Select a Unit</option>
									</select>
									<span class="input-group-addon"><i class="glyphicon glyphicon-warning-sign"
																	   data-toggle="popover" data-placement="top"
																	   data-content="This is not a required field"
																	   data-title="Requirement"></i></span>
								</div>
							</div>
						</div>
					</div>
					<a class="basic" href="fail.php" id="verifyFail" style="display: none;">
						<div class="alert alert-danger">
							<strong>Having Trouble Finding your Unit?</strong> Please retry as our records do not match your selection.<br />
							In the event of difficulties please use the online chat facility below to speak to one of our agents. Alternatively click this box for one of our agents to call you back.
						</div>
					</a>
				</div>
				<div class="modal-footer">
					<button type="button" id="cancelBtn" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-primary" id="modalAddBtn">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>