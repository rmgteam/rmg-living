<div class="container-fluid svg-container-other">
</div>
<div class="container">
	<form role="form" id="paperlessForm" class="other-page">
		<h1>Paperless Service</h1>
		<input type="hidden" id="paperless" value="<?=$paperless;?>" />
		<div class="alert alert-info">
			Hello <?=$name;?>
			<p id="newUserRow" style="display:none" >
				Our records show you are not currently signed up to RMG's Paperless service.
			</p>
			<p id="oldUserRow" style="display:none">
				Our records show you and this unit are currently signed up to RMG's Paperless service. You can opt out, or you can sign up additional units to the RMG Paperless Service below.
			</p>
		</div>
		<div id="optTextRow" style="display:none" class="alert alert-info">
			<strong>Opting Out?</strong> All previously opted in units will be opted out
		</div>
		<div class="row" id="optRow">
			<div class="col-md-12">
				<div class="form-group has-no-valid">
					<div class="input-group">
						<div class="form-control rounded">
							<label class="control-label" for="group1">If you wish to <span id="optVar"></span> the RMG Paperless service please select the alternative option below</label><br />
							<label class="control-label">
								<input type="radio" name="group1" value="no" id="optout" checked="" />
								Opt Out
							</label>
							<label class="control-label">
								<input type="radio" name="group1" id="optin" value="yes" />
								Opt In
							</label>
						</div>
						<span class="input-group-addon"><i class="glyphicon glyphicon-warning-sign"
														   data-toggle="popover" data-placement="top"
														   data-content="This is not a required field"
														   data-title="Requirement"></i></span>
					</div>
				</div>
			</div>
		</div>
		<div class="row" id="unitRevertRow">
			<div class="col-md-12">
				<div class="form-group has-no-valid">
					<div class="input-group">
						<div class="form-control rounded">
							<label class="control-label" for="group2">Would you like to revert all mail to be delivered to your unit address?</label><br />
							<label class="control-label">
								<input type="radio" name="group2" value="yes" id="revertYes" checked="" />
								Yes
							</label>
							<label class="control-label">
								<input type="radio" name="group2" id="revertNo" value="no" />
								No
							</label>
						</div>
						<span class="input-group-addon"><i class="glyphicon glyphicon-warning-sign"
														   data-toggle="popover" data-placement="top"
														   data-content="This is not a required field"
														   data-title="Requirement"></i></span>
					</div>
				</div>
			</div>
		</div>
		<div class="row" id="altAddressRow">
			<div class="col-md-12">
				<div class="form-group">
					<div class="form-control rounded">
						<label class="control-label" for="formLookupInput">Alternative Address</label>
						<div class="form-group has-no-valid">
							<div class="input-group">
								<input class="form-control rounded address-lookup" id="formLookupInput" name="formLookupInput"
								   type="text" placeholder="Postcode"
								   data-address-line1="#formLine1Input"
								   data-address-line2="#formLine2Input"
								   data-address-line3="#formLine3Input"
								   data-address-county="#formCountyInput"
								   data-address-country="#formCountryIdInput"
								   data-address-state="#formStateInput"
								   data-address-town="#formCityInput"
								   data-address-postcode="#formPostcodeInput"
								   data-address-block="#Address"
									/>
								<span class="input-group-addon"><i class="glyphicon glyphicon-warning-sign"
															   data-toggle="popover" data-placement="top"
															   data-content="This is not a required field"
															   data-title="Requirement"></i></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div id="Address">
				<div class="col-sm-12 col-md-6">
					<div class="form-group has-no-valid">
						<div class="input-group">
							<input class="form-control rounded" id="formLine1Input" name="formLine1Input" type="text" placeholder="Address 1" />
							<span class="input-group-addon"><i class="glyphicon glyphicon-warning-sign"
															   data-toggle="popover" data-placement="top"
															   data-content="This is not a required field"
															   data-title="Requirement"></i></span>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-6">
					<div class="form-group has-no-valid">
						<div class="input-group">
							<input class="form-control rounded" id="formLine2Input" name="formLine2Input" type="text" placeholder="Address 2" />
							<span class="input-group-addon"><i class="glyphicon glyphicon-warning-sign"
															   data-toggle="popover" data-placement="top"
															   data-content="This is not a required field"
															   data-title="Requirement"></i></span>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-6">
					<div class="form-group has-no-valid">
						<div class="input-group">
							<input class="form-control rounded" id="formLine3Input" name="formLine3Input" type="text" placeholder="Address 3" />
							<span class="input-group-addon"><i class="glyphicon glyphicon-warning-sign"
															   data-toggle="popover" data-placement="top"
															   data-content="This is not a required field"
															   data-title="Requirement"></i></span>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-6">
					<div class="form-group has-no-valid">
						<div class="input-group">
							<input class="form-control rounded" id="formCityInput" name="formCityInput" type="text" placeholder="City" />
							<span class="input-group-addon"><i class="glyphicon glyphicon-warning-sign"
															   data-toggle="popover" data-placement="top"
															   data-content="This is not a required field"
															   data-title="Requirement"></i></span>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-6">
					<div class="form-group has-no-valid">
						<div class="input-group">
							<input class="form-control rounded" id="formCountyInput" name="formCountyInput" type="text" placeholder="County" />
							<span class="input-group-addon"><i class="glyphicon glyphicon-warning-sign"
															   data-toggle="popover" data-placement="top"
															   data-content="This is not a required field"
															   data-title="Requirement"></i></span>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-6">
					<div class="form-group has-no-valid">
						<div class="input-group">
							<input class="form-control rounded" id="formStateInput" name="formStateInput" type="text" placeholder="State" />
							<span class="input-group-addon"><i class="glyphicon glyphicon-warning-sign"
															   data-toggle="popover" data-placement="top"
															   data-content="This is not a required field"
															   data-title="Requirement"></i></span>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-6">
					<div class="form-group has-no-valid">
						<div class="input-group">
							<input class="form-control rounded" id="formPostcodeInput" name="formPostcodeInput" type="text" placeholder="Postal Code" />
							<span class="input-group-addon"><i class="glyphicon glyphicon-warning-sign"
															   data-toggle="popover" data-placement="top"
															   data-content="This is not a required field"
															   data-title="Requirement"></i></span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group has-no-valid" id="cPolicyRow">
					<div class="form-group">
						<label class="control-label">Cookie &amp; Privacy Policy</label>
						<button type="button" id="cookieViewBtn" class="btn btn-warning pull-right">View</button>
					</div>
					<div class="form-group" id="cookiePolicy">
						<object data="/paperless/cookie-policy.pdf" type="application/pdf" width="100%" height="400" style="z-index:-1;"></object>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group has-no-valid" id="agreeRow">
					<div class="input-group">
						<div class="form-control rounded">
							<input type="checkbox" id="chkCookie" name="chkCookie" value="yes" />
							<label class="control-label" for="chkCookie">Please select to confirm you have read, understood and agree to our Cookie &amp; Privacy Policy</label>
						</div>
						<span class="input-group-addon"><i class="glyphicon glyphicon-warning-sign"
														   data-toggle="popover" data-placement="top"
														   data-content="This is not a required field"
														   data-title="Requirement"></i></span>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group has-no-valid" id="tPolicyRow">
					<div class="form-group">
						<label class="control-label">Terms &amp; Conditions</label>
						<button type="button" id="termsViewBtn" class="btn btn-warning pull-right">View</button>
					</div>
					<div class="form-group" id="termsPolicy">
						<object data="/paperless/terms-conditions.pdf" type="application/pdf" width="100%" height="400" style="z-index:-1;"></object>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group has-no-valid" id="agreeTRow">
					<div class="input-group">
						<div class="form-control rounded">
							<input type="checkbox" id="chkTerms" name="chkTerms" value="yes" />
							<label class="control-label" for="chkTerms">Please select to confirm you have read, understood and agree to the RMG terms and conditions</label>
						</div>
						<span class="input-group-addon"><i class="glyphicon glyphicon-warning-sign"
														   data-toggle="popover" data-placement="top"
														   data-content="This is not a required field"
														   data-title="Requirement"></i></span>
					</div>
				</div>
			</div>
		</div>
		<div class="panel panel-primary" id="unitRow">
			<div class="panel-heading">
				<div class="row">
					<div class="col-xs-6 text-left">
						<h2 class="panel-title-mini">My Units</h2>
					</div>
					<div class="col-xs-6 text-right">
						<button type="button" id="unitAddBtn" class="btn btn-warning" data-toggle="modal" data-target="#modal1">Add Unit</button>
					</div>
				</div>
			</div>
			<div class="panel-body">
				<div class="form-group has-no-valid">
					<table class="table table-condensed table-striped" id="unitTable">
						<thead>
						<tr style="display: table-row;">
							<th>Address Line</th>
							<th>Postcode</th>
						</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="alert alert-info" id="emailTextRow">
			The email address entered will be used for all future correspondence. This will not change the email address used when setting up your security details on your RMG Living Account.
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group has-no-valid" id="emailRow">
					<div class="input-group">
						<input type="email" class="form-control rounded" id="formEmailInput" name="formEmailInput" placeholder="Enter your email address">
						<span class="input-group-addon"><i class="glyphicon glyphicon-warning-sign"
														   data-toggle="popover" data-placement="top"
														   data-content="This is not a required field"
														   data-title="Requirement"></i></span>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group has-no-valid" id="confirmRow">
					<div class="input-group">
						<input type="email" class="form-control rounded" id="formConfirmEmailInput" name="formConfirmEmailInput" placeholder="Confirm your email address" />
						<span class="input-group-addon"><i class="glyphicon glyphicon-warning-sign"
														   data-toggle="popover" data-placement="top"
														   data-content="This is not a required field"
														   data-title="Requirement"></i></span>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group has-no-valid" id="phoneRow">
					<div class="input-group">
						<input type="tel" class="form-control rounded" id="formTelInput" name="formTelInput" placeholder="Enter your telephone number" />
						<span class="input-group-addon"><i class="glyphicon glyphicon-warning-sign"
														   data-toggle="popover" data-placement="top"
														   data-content="This is not a required field"
														   data-title="Requirement"></i></span>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group has-no-valid" id="mobileRow">
					<div class="input-group">
						<input type="tel" class="form-control rounded" id="formMobileInput" name="formMobileInput" placeholder="Enter your mobile number" />
						<span class="input-group-addon"><i class="glyphicon glyphicon-warning-sign"
														   data-toggle="popover" data-placement="top"
														   data-content="This is not a required field"
														   data-title="Requirement"></i></span>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 text-center">
				<button type="submit" id="successBtn" class="btn btn-rmg">Submit</button>
			</div>
		</div>
	</form>
	<div class="modal fade modal-rmg" id="emailAlertDialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-remove"></i></button>
					<h4 class="modal-title">No Changes Detected</h4>
				</div>
				<div class="modal-body">
					<p id="messageDialogMessage"></p>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

</div>