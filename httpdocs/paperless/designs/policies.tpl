<div class="container-fluid svg-container-other">
</div>
<div class="container">
	<h1>Policies</h1>
	<form role="form" id="paperlessForm" class="other-page">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group has-no-valid" id="cPolicyRow">
					<div class="form-group">
						<label class="control-label">Cookie &amp; Privacy Policy</label>
						<button type="button" id="cookieViewBtn" class="btn btn-warning pull-right">View</button>
					</div>
					<div class="form-group" id="cookiePolicy">
						<object data="/paperless/cookie-policy.pdf" type="application/pdf" width="100%" height="400" style="z-index:-1;"></object>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group has-no-valid" id="tPolicyRow">
					<div class="form-group">
						<label class="control-label">Terms &amp; Conditions</label>
						<button type="button" id="termsViewBtn" class="btn btn-warning pull-right">View</button>
					</div>
					<div class="form-group" id="termsPolicy">
						<object data="/paperless/terms-conditions.pdf" type="application/pdf" width="100%" height="400" style="z-index:-1;"></object>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>