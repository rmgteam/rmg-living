<div class="container-fluid svg-container-other">
</div>
<div class="container">
	<form role="form" id="failForm" class="other-page">
		<h1>Thank You</h1>
		<p>We are sorry we couldn't verify your details at this time. Your information has now been passed to our Customer Contact Centre. One of our agents will contact you within 24 hours.</p>
	</form>
</div>