<div class="container-fluid svg-container-other">
</div>
<div class="container">
	<form role="form" id="failForm" class="other-page">
		<h1>Have One Of Our Customer Contact Centre Agents Contact You</h1>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group has-no-valid">
					<div class="input-group">
						<input type="text" class="form-control rounded" id="formFirstInput" name="formFirstInput" placeholder="Enter your first name" required>
						<span class="input-group-addon"><i class="glyphicon glyphicon-warning-sign"
														   data-toggle="popover" data-placement="top"
														   data-content="This is not a required field"
														   data-title="Requirement"></i></span>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group has-no-valid">
					<div class="input-group">
						<input type="text" class="form-control rounded" id="formLastInput" name="formLastInput" placeholder="Enter your last name" required>
						<span class="input-group-addon"><i class="glyphicon glyphicon-warning-sign"
														   data-toggle="popover" data-placement="top"
														   data-content="This is not a required field"
														   data-title="Requirement"></i></span>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group has-no-valid">
					<div class="input-group">
						<div class="form-control rounded">
							<label class="control-label" for="verifyGroup1Input">What is your preferred contact method?</label><br />
							<label class="control-label">
								<input type="radio" name="group1" value="email" id="chkEmail" checked="">
								Email
							</label>
							<label class="control-label">
								<input type="radio" name="group1" id="chkPhone" value="phone">
								Phone
							</label>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row" id="emailRow">
			<div class="col-md-6">
				<div class="form-group has-no-valid">
					<div class="input-group">
						<input type="email" class="form-control rounded" id="formEmailInput" name="formEmailInput" placeholder="Enter your email address">
						<span class="input-group-addon"><i class="glyphicon glyphicon-warning-sign"
														   data-toggle="popover" data-placement="top"
														   data-content="This is not a required field"
														   data-title="Requirement"></i></span>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group has-no-valid">
					<div class="input-group">
						<input type="email" class="form-control rounded" id="formConfirmEmailInput" name="formConfirmEmailInput" placeholder="Confirm your email address">
						<span class="input-group-addon"><i class="glyphicon glyphicon-warning-sign"
														   data-toggle="popover" data-placement="top"
														   data-content="This is not a required field"
														   data-title="Requirement"></i></span>
					</div>
				</div>
			</div>
		</div>
		<div class="row" id="numberRow">
			<div class="col-md-6">
				<div class="form-group has-no-valid">
					<div class="input-group">
						<input type="tel" class="form-control phone-group" id="formTelInput" name="formTelInput" placeholder="Enter your telephone number">
						<span class="input-group-addon"><i class="glyphicon glyphicon-warning-sign"
														   data-toggle="popover" data-placement="top"
														   data-content="This is not a required field"
														   data-title="Requirement"></i></span>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group has-no-valid">
					<div class="input-group">
						<input type="tel" class="form-control phone-group" id="formMobileInput" name="formMobileInput" placeholder="Enter your mobile number">
						<span class="input-group-addon"><i class="glyphicon glyphicon-warning-sign"
														   data-toggle="popover" data-placement="top"
														   data-content="This is not a required field"
														   data-title="Requirement"></i></span>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 text-center">
				<button type="submit" id="successBtn" class="btn btn-rmg">Submit</button>
			</div>
		</div>
	</form>
</div>