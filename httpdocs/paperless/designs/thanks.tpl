<div class="container-fluid svg-container-other">
</div>
<div class="container">
	<form role="form" id="failForm" class="other-page">
		<h1>Thank You</h1>
		<?=$blurb;?>
		<p>If you have any queries you can chat with our agents by selecting online chat through any of the RMG websites. Alternatively you can email to <a href="mailto:paperless@rmguk.com">paperless@rmguk.com</a> or call 0345 002 4444</p>
	</form>
</div>