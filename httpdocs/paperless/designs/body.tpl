<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>RMG Living - <?=$title?></title>
	<!-- Bootstrap core CSS -->
	<link href="/paperless/css/gothambold.css" rel="stylesheet">
	<link href="/paperless/css/bootstrap.css" rel="stylesheet">
	<link href="/paperless/css/addressLookup.css" rel="stylesheet">
	<link href="/paperless/css/advancedSelect.css" rel="stylesheet">
	<link href="/paperless/css/intlTelInput.css" rel="stylesheet">
	<link href="/paperless/css/font-awesome.min.css" rel="stylesheet">
	<link href="/paperless/css/style.css" rel="stylesheet">
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
</head>
<body>
<nav class="navbar navbar-rmg navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="/paperless/index.php"><img src="img/rmg.png" /></a>
		</div>
		<div class="navbar-collapse collapse">
		</div>
		<!--/.navbar-collapse -->
	</div>
</nav>
<?=$content?>
<?=$modal;?>
<div class="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-4 copyright">
				<img src="img/rmg.png" />
				<p>&copy; 2015 Residential Management Group Ltd.</p>
			</div>
			<div class="col-md-4 contact">
				<a href="mailto:paperless@rmguk.com">paperless@rmguk.com</a>
				<h2>0345 002 4444</h2>
			</div>
			<div class="col-md-4 icons">
				<a data-toggle="popover" id="new_whoson" data-class="rmg" data-container="body" data-title="Need Some Help?" data-content="Please click the circle below to chat to one of our agents" data-placement="top" class="circle">
					<i class="fa fa-comments"></i>
				</a>
				<a data-toggle="popover" data-content="Follow Us on Twitter" data-placement="left" class="circle" href="http://twitter.com/rmgltd" target="_blank">
					<i class="fa fa-twitter"></i>
				</a>
			</div>
		</div>
	</div>
</div>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<script src="/paperless/js/jquery.min.js"></script>
<script src="/paperless/js/jquery-ui.min.js"></script>
<script src="/paperless/js/bootstrap.min.js"></script>
<script src="/paperless/js/jquery.validate.min.js"></script>
<script src="/paperless/js/jquery.ajaxExtend.js"></script>
<script src="/paperless/js/jQuery.postcode.js"></script>
<script src="/paperless/js/jquery.advancedSelect.js"></script>
<script src="/paperless/js/jQuery.addressLookup.js"></script>
<script src="/paperless/js/intlTel.utils.js"></script>
<script src="/paperless/js/intlTelInput.min.js"></script>
<script src="/paperless/js/main.js"></script>
<script src="/paperless/js/<?=$script;?>"></script>
<script>
	<?=$scriptExtra;?>
</script>
<?=$chat;?>
<script>
	$(document).on('ready', function(){
		$('#new_whoson').on('click', function(e){
			$('#whoson_chat_link').click();
			e.stopPropagation();
		});
		$('#chatonId').hide();

		$(window ).on('resize', function(){
			reFooter();
		});

		reFooter();
	});

	function reFooter(){
		var height = $('.footer' ).height();
		$('body' ).css('margin-bottom', height);
	}
</script>
</body>
</html>
