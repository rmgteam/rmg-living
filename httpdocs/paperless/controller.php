<?
require_once("utils.php");
require_once( $UTILS_CLASS_PATH."emailer.php" );
require_once( $UTILS_CLASS_PATH."webservice.class.php" );

//ALTER TABLE `cpm_residents`
//ADD COLUMN `paperless`  char(1) NOT NULL DEFAULT 'N' AFTER `resident_email`;

/// Get json and convert to array
$body = file_get_contents( "php://input" );
if($body == '' ) {
	$request = $_REQUEST;
}else{
	$request = json_decode( $body, true );
	if(!isset($request['a'])) {
		$request['a'] = $_REQUEST['a'];
	}
}

$GLOBALS['email']["host"] = "94.250.233.132";
$GLOBALS['email']["port"] = "25";
$GLOBALS['email']['auth'] = false;
$GLOBALS['email']['from'] = 'paperless@rmguk.com';
//$GLOBALS['email']['from'] = 'marc.evans@rmguk.com';
$GLOBALS['email']['name'] = 'RMG Paperless Service';
$GLOBALS['email']['templates'] = $UTILS_FILE_PATH . 'paperless/emails/';

if( $request['a'] == 'contactMe' ) {
	$resultArray = array();
	$refs = array();

	$resultArray['result'] = true;

	//Create a new PHPMailer instance
	$mail = new \emailer();
	//Tell PHPMailer to use SMTP
	$mail->isSMTP();
	$mail->SMTPDebug = 0;
	//Ask for HTML-friendly debug output
	$mail->Debugoutput = 'html';
	//Set the hostname of the mail server
	$mail->Host = $GLOBALS['email']['host'];
	//Set the SMTP port number - likely to be 25, 465 or 587
	$mail->Port = $GLOBALS['email']['port'];
	//Whether to use SMTP authentication
	$mail->SMTPAuth = $GLOBALS['email']['auth'];
	//Set who the message is to be sent from
	$mail->setFrom( $GLOBALS['email']['from'], $GLOBALS['email']['name'] );
	//Set an alternative reply-to address
	$mail->addReplyTo( $GLOBALS['email']['from'], $GLOBALS['email']['name'] );
	//Set who the message is to be sent to
	$mail->addAddress( $GLOBALS['email']['from'], $GLOBALS['email']['name'] );
	//Set the subject line
	$mail->Subject = 'Paperless Verification Failed - Contact Required';
	//Read an HTML message body from an external file, convert referenced images to embedded,
	//convert HTML into a basic plain-text alternative body
	$html = file_get_contents( $GLOBALS['email']['templates'] . 'contact/html.tpl' );
	$plain = file_get_contents( $GLOBALS['email']['templates'] . 'contact/plain.tpl' );

	$value = $request;
	foreach ( $value as $key => $val ) {
		$html = str_replace( '{{' . $key . '}}', $val, $html );
		$plain = str_replace( '{{' . $key . '}}', $val, $plain );
	}

	$mail->msgHTML( $html, $GLOBALS['email']['templates'] . 'contact/' );
	//Replace the plain text body with one created manually
	$mail->AltBody = $plain;

	$mail->send();

	output( $resultArray );
} elseif( $request['a'] == 'updatePaperless' ) {
	$resultArray = array();
	$update = '';
	$where = '';

	if( isset( $_SESSION['refs'] ) ) {
		foreach( $_SESSION['refs'] as $ref ) {
			$where .= "resident_num = '" . $ref . "' OR ";
		}
	}

	if($where != ''){
		$where = " WHERE " . substr($where, 0, -3);
	}

	if( $request['email'] != '' ) {
		$update .= "resident_email = '" . $request['email'] . "', ";
	}

	if( $request['telephone'] != '' ) {
		$update .= "tel = '" . $request['telephone'] . "', ";
	}

	$resultArray['result'] = true;

	if($request['opt'] == 'Y') {
		$_SESSION['text'] = "<p>The units you have selected have now been opted into our Paperless Service. A confirmation email has been issued to the email address you registered during the signing up process.</p>";
	}else{
		$_SESSION['text'] = "<p>You have now opted out of our Paperless Service. You will receive a confirmation email shortly and then you will no longer receive RMG's Paperless Service for your previously selected units.</p>";
	}

	foreach ( $_SESSION['refs'] as $ref ) {
		$sqlRef = "SELECT lr.resident_ref,
			r.resident_email,
			e.paperless
			FROM cpm_lookup_residents lr
			INNER JOIN cpm_residents r on r.resident_num = lr.resident_lookup
			INNER JOIN cpm_residents_extra e ON e.resident_num = r.resident_num
			WHERE resident_lookup = '" . $ref . "'";
		$result = @mysql_query($sqlRef);
		$row = @mysql_fetch_array($result);
		$xml .= '<reference>' . $row['resident_ref'] . '</reference>';
		$refs[] = $row['resident_ref'];
		if($i == 0){
			$emailAddress = $row['resident_email'];
			$currentPaperless = $row['paperless'];
			$currentPaperlessDate = $row['paperless_date'];
		}
		$i++;
	}

	if( $request['email'] == $emailAddress && count( $_SESSION['refs'] ) == 1 && $request['opt'] == 'Y' && $currentPaperless == 'Y' ){
		$resultArray['result'] = 'email';
		output( $resultArray );
		exit;
	}

	if( $request['email'] != '' ) {
		$emailAddress =  $request['email'];
	}

	if( isset( $_SESSION['refs'] ) ) {

		$resultArray['subsidiary'] = array();
		$resultArray['output'] = array();
		$resultArray['reference'] = array();
		$resultArray['xml'] = array();
		$resultArray['review'] = array();

		//@todo update qube. - create loop and check for all brands
		$sql = "SELECT * FROM cpm_subsidiary";
		$result = @mysql_query($sql);
		while($row = @mysql_fetch_array($result)) {
			$resultArray['subsidiary'][] = $row['subsidiary_id'];

			$xmlFound = false;
			// diarytype eCorrespond/none
			// force-diary 1/{blank} - 1 to force diary when nothing changed/ {blank} does not force
			$xml = '<dispatch>' . ( $request['opt'] == 'Y' ? 2 : 1 ) . '</dispatch>
			<diarytype></diarytype>
			<force-diary></force-diary>';
			//$xml .= '<unit-address>' . $request['unit-revert'] . "</unit-address>";
			if ( $request['email'] != '' ) {
				$xml .= '<email>' . $request['email'] . '</email>';
			}
			if ( $request['telephone'] != '' ) {
				$xml .= "<telephone>" . $request['telephone'] . "</telephone>";
			}
			if ( $request['mobile'] != '' ) {
				$xml .= "<mobile>" . $request['mobile'] . "</mobile>";
			}
			if( $request['unit-revert'] == 'N' ) {
				$xml .= "<alt-address>";
				$xml .= "<line1>" . htmlentities($request['line1']) . "</line1>";
				$xml .= "<line2>" . htmlentities($request['line2']) . "</line2>";
				$xml .= "<line3>" . htmlentities($request['line3']) . "</line3>";
				$xml .= "<town>" . htmlentities($request['town']) . "</town>";
				if( $request['country'] == 15 ) {
					$xml .= "<county>" . htmlentities($request['county']) . "</county>";
				}else{
					$xml .= "<state>" . htmlentities($request['state']) . "</state>";
					$countryName = '';
					$SQL = "SELECT * FROM country WHERE id='" . $request['country'] . "'";
					$result_country = mysql_query($SQL);
					if ( mysql_num_rows($result_country) > 0 ) {
						while($row_country = mysql_fetch_array($result_country)){
							$countryName = $row_country['name'];
						}
					}
					$xml .= "<country>" . htmlentities($countryName) . "</country>";
				}
				$xml .= "<postcode>" . $request['postcode'] . "</postcode>";
				$xml .= "</alt-address>";
			}

			if ( isset( $_SESSION['refs'] ) ) {
				$xml .= '<tenants>';
				foreach ( $_SESSION['refs'] as $ref ) {
					$sqlRef = "SELECT lr.resident_ref,
					r.resident_email,
					u.unit_address_1,
					u.unit_address_2
					FROM cpm_lookup_residents lr
					INNER JOIN cpm_residents r on r.resident_num = lr.resident_lookup
					INNER JOIN cpm_units u on r.resident_num = u.resident_num
					WHERE resident_lookup = '" . $ref . "'
					AND r.subsidiary_id = '" . $row['subsidiary_id'] . "'";
					$result1 = @mysql_query( $sqlRef );
					while($row1 = @mysql_fetch_array( $result1 )) {
						$xml .= '<reference>' . $row1['resident_ref'] . '</reference>';
						$resultArray['reference'][] = $row1['resident_ref'];
						$resultArray['lookup'][] = $ref;
						$xmlFound = true;
					}
				}
				$xml .= '</tenants>';

				if($xmlFound){
					$resultArray['xml'][] = $xml;
					$return_code = '';
					$data = "<parameters>
					" . $xml . "
					</parameters>";

					$webservice = new webservice;

					$output = $webservice->qube_execute(2, 'puttenant', $data, $row['subsidiary_ecs_login_group']);
					$resultArray['output'][] = $output;

					if ( !is_null( $output['code'] ) ) {
						$resultArray['reason'][] = $row['subsidiary_ecs_login_group'] . " - XML is invalid/Curl is unavailable. Http response: " . $output['code'] . " XML = " . $output['desc'];
						$resultArray['result'] = 'error';
					} else {

						$return_code = $output['soap:Envelope']['soap:Body'][0]['QubeProcess-3Response'][0]['QubeProcess-3Result'][0]['data'][0]['results'][0]['code'];
						$review_code = $output['soap:Envelope']['soap:Body'][0]['QubeProcess-3Response'][0]['QubeProcess-3Result'][0]['data'][0]['results'][0]['review-code'];

						if ( $return_code == "00" ) {
							$resultArray['result'] = true;
							$resultArray['xml'][] =  $data;
							if( $review_code != '') {
								$resultArray['review'][] = $row['subsidiary_name'] . ' - ' . $review_code;
							}
						}else{
							$resultArray['result'] = 'error';
							$resultArray['reason'][] = $row['subsidiary_ecs_login_group'] . " - Error code: " . $return_code;
						}
					}
				}else{
					$resultArray['reason'][] = $row['subsidiary_ecs_login_group'] . " - no tenant found ";
				}
			}
		}

		if( $resultArray['result'] == true && !empty($where) ) {

			$sqlUpdate = "UPDATE cpm_residents SET
			".$update . $where;
			$result = @mysql_query($sqlUpdate);

			$paperlessDate = $currentPaperlessDate;
			if( $request['opt'] == 'Y' && $currentPaperlessDate == ''){
				$paperlessDate = date( 'Ymd' );
			}

			$sqlUpdate = "UPDATE cpm_residents_extra SET
			paperless = '".$request['opt']."',
			paperless_date = '".$paperlessDate."'" . $where;
			$result = @mysql_query($sqlUpdate);

			//Create a new PHPMailer instance
			$mail = new \emailer();
			//Tell PHPMailer to use SMTP
			$mail->isSMTP();
			$mail->SMTPDebug = 0;
			//Ask for HTML-friendly debug output
			$mail->Debugoutput = 'html';
			//Set the hostname of the mail server
			$mail->Host = $GLOBALS['email']['host'];
			//Set the SMTP port number - likely to be 25, 465 or 587
			$mail->Port = $GLOBALS['email']['port'];
			//Whether to use SMTP authentication
			$mail->SMTPAuth = $GLOBALS['email']['auth'];
			//Set who the message is to be sent from
			$mail->setFrom( $GLOBALS['email']['from'], $GLOBALS['email']['name'] );
			//Set an alternative reply-to address
			$mail->addReplyTo( $GLOBALS['email']['from'], $GLOBALS['email']['name'] );
			//Set who the message is to be sent to
			$mail->addAddress( $emailAddress );
			//$mail->addAddress( $GLOBALS['email']['from'] );
			//Set the subject line
			$mail->Subject = 'RMG Paperless Service';

			if( $request['opt'] == 'Y' ) {
				$folder = 'optin';
			}else{
				$folder = 'optout';
			}
			$_SESSION['optIn'] = $request['opt'];

			//Read an HTML message body from an external file, convert referenced images to embedded,
			//convert HTML into a basic plain-text alternative body
			$html = file_get_contents( $GLOBALS['email']['templates'] . $folder . '/html.tpl' );
			$plain = file_get_contents( $GLOBALS['email']['templates'] . $folder . '/plain.tpl' );

			$value = array();
			$value['policyUrl'] = $UTILS_HTTPS_ADDRESS . '/paperless/policies.php';
			$value['url'] = $UTILS_HTTPS_ADDRESS . '/paperless/index.php';
			if( is_array( $_SESSION['unit_refs'] ) ) {
				$value['unitList'] = implode( '<br />', $_SESSION['unit_refs'] );
				$value['plainUnitList'] = implode( "\r\n", $_SESSION['unit_refs'] );
			}
			foreach ( $value as $key => $val ) {
				$html = str_replace( '{{' . $key . '}}', $val, $html );
				$plain = str_replace( '{{' . $key . '}}', $val, $plain );
			}

			$mail->msgHTML( $html, $GLOBALS['email']['templates'] . $folder );
			//Replace the plain text body with one created manually
			$mail->AltBody = $plain;

			$mail->AddEmbeddedImage( $UTILS_FILE_PATH . 'paperless/img/rmg_logo.png', 'rmg_logo' );
			$mail->AddEmbeddedImage( $UTILS_FILE_PATH . 'paperless/img/Hugh.png', 'hugh_image' );

			if( $emailAddress != '' ) {
				$mail->send();
			}else{
				//@todo We have no email address for this user.
				$resultArray['result'] = false;
			}

			if( count( $_SESSION['refs'] ) > 1 ) {

				//Create a new PHPMailer instance
				$mail = new \emailer();
				//Tell PHPMailer to use SMTP
				$mail->isSMTP();
				$mail->SMTPDebug = 0;
				//Ask for HTML-friendly debug output
				$mail->Debugoutput = 'html';
				//Set the hostname of the mail server
				$mail->Host = $GLOBALS['email']['host'];
				//Set the SMTP port number - likely to be 25, 465 or 587
				$mail->Port = $GLOBALS['email']['port'];
				//Whether to use SMTP authentication
				$mail->SMTPAuth = $GLOBALS['email']['auth'];
				//Set who the message is to be sent from
				$mail->setFrom( $GLOBALS['email']['from'], $GLOBALS['email']['name'] );
				//Set an alternative reply-to address
				$mail->addReplyTo( $GLOBALS['email']['from'], $GLOBALS['email']['name'] );
				//Set who the message is to be sent to
				$mail->addAddress( $GLOBALS['email']['from'], $GLOBALS['email']['name'] );
				//Set the subject line
				$mail->Subject = 'RMG Paperless Service - Non-Resident Linkage';
				//Read an HTML message body from an external file, convert referenced images to embedded,
				//convert HTML into a basic plain-text alternative body
				$html = file_get_contents( $GLOBALS['email']['templates'] . 'optinccc/html.tpl' );
				$plain = file_get_contents( $GLOBALS['email']['templates'] . 'optinccc/plain.tpl' );

				$value = array();
				$value['last'] = $_SESSION['lastName'];
				$value['email'] = $request['email'];
				$value['telephone'] = $request['telephone'];
				$value['mobile'] = $request['mobile'];
				$value['refs'] = implode( ', ', $refs );
				$value['review'] = implode( ', ', $resultArray['review'] );

				foreach ( $value as $key => $val ) {
					$html = str_replace( '{{' . $key . '}}', $val, $html );
					$plain = str_replace( '{{' . $key . '}}', $val, $plain );
				}

				$mail->msgHTML( $html, $GLOBALS['email']['templates'] . 'optinccc/' );
				//Replace the plain text body with one created manually
				$mail->AltBody = $plain;

				$mail->send();

				//Create a new PHPMailer instance
				$mail = new \emailer();
				//Tell PHPMailer to use SMTP
				$mail->isSMTP();
				$mail->SMTPDebug = 0;
				//Ask for HTML-friendly debug output
				$mail->Debugoutput = 'html';
				//Set the hostname of the mail server
				$mail->Host = $GLOBALS['email']['host'];
				//Set the SMTP port number - likely to be 25, 465 or 587
				$mail->Port = $GLOBALS['email']['port'];
				//Whether to use SMTP authentication
				$mail->SMTPAuth = $GLOBALS['email']['auth'];
				//Set who the message is to be sent from
				$mail->setFrom( $GLOBALS['email']['from'], $GLOBALS['email']['name'] );
				//Set an alternative reply-to address
				$mail->addReplyTo( $GLOBALS['email']['from'], $GLOBALS['email']['name'] );
				//Set who the message is to be sent to
				$mail->addAddress( 'marc.evans@rmgltd.co.uk', 'Web Team' );
				//Set the subject line
				$mail->Subject = 'RMG Paperless Service - Non-Resident Linkage';
				//Read an HTML message body from an external file, convert referenced images to embedded,
				//convert HTML into a basic plain-text alternative body
				$html = file_get_contents( $GLOBALS['email']['templates'] . 'xmldev/html.tpl' );
				$plain = file_get_contents( $GLOBALS['email']['templates'] . 'xmldev/plain.tpl' );

				$value = array();
				$value['lookup'] = implode( ',', $_SESSION['refs'] );
				$value['refs'] = implode( ',', $refs );
				$value['lookup2'] = implode( ',', $resultArray['lookup']);
				$value['refs2'] = implode( ',', $resultArray['reference']);
				$value['xml'] = implode( ',',$resultArray['xml']);

				foreach ( $value as $key => $val ) {
					$html = str_replace( '{{' . $key . '}}', $val, $html );
					$plain = str_replace( '{{' . $key . '}}', $val, $plain );
				}

				$mail->msgHTML( $html, $GLOBALS['email']['templates'] . 'xmldev/' );
				//Replace the plain text body with one created manually
				$mail->AltBody = $plain;
				//$mail->send();
			}elseif( $request['opt'] == 'N' ) {

				//Create a new PHPMailer instance
				$mail = new \emailer();
				//Tell PHPMailer to use SMTP
				$mail->isSMTP();
				$mail->SMTPDebug = 0;
				//Ask for HTML-friendly debug output
				$mail->Debugoutput = 'html';
				//Set the hostname of the mail server
				$mail->Host = $GLOBALS['email']['host'];
				//Set the SMTP port number - likely to be 25, 465 or 587
				$mail->Port = $GLOBALS['email']['port'];
				//Whether to use SMTP authentication
				$mail->SMTPAuth = $GLOBALS['email']['auth'];
				//Set who the message is to be sent from
				$mail->setFrom( $GLOBALS['email']['from'], $GLOBALS['email']['name'] );
				//Set an alternative reply-to address
				$mail->addReplyTo( $GLOBALS['email']['from'], $GLOBALS['email']['name'] );
				//Set who the message is to be sent to
				$mail->addAddress( $GLOBALS['email']['from'], $GLOBALS['email']['name'] );
				//Set the subject line
				$mail->Subject = 'RMG Paperless Service - Non-Resident Linkage';
				//Read an HTML message body from an external file, convert referenced images to embedded,
				//convert HTML into a basic plain-text alternative body
				$html = file_get_contents( $GLOBALS['email']['templates'] . 'optoutccc/html.tpl' );
				$plain = file_get_contents( $GLOBALS['email']['templates'] . 'optoutccc/plain.tpl' );

				$value = array();
				$value['unit-revert'] = $request['unit-revert'] == 'N' ? 'No' : 'Yes';
				$value['refs'] = implode( ',', $refs );
				$value['last'] = $_SESSION['lastName'];
				$value['line1'] = $request['line1'];
				$value['line2'] = $request['line2'];
				$value['line3'] = $request['line3'];
				$value['town'] = $request['town'];
				if( $request['country'] == 15 ) {
					$value['county'] = $request['county'];
					$value['country'] = 'United Kingdom';
				}else{
					$value['county'] = $request['state'];
					$countryName = '';
					$SQL = "SELECT * FROM country WHERE id='" . $request['country'] . "'";
					$result_country = mysql_query($SQL);
					if ( mysql_num_rows($result_country) > 0 ) {
						while($row_country = mysql_fetch_array($result_country)){
							$countryName = $row_country['name'];
						}
					}
					$value['country'] = $countryName;
				}
				$value['postcode'] = $request['postcode'];

				foreach ( $value as $key => $val ) {
					$html = str_replace( '{{' . $key . '}}', $val, $html );
					$plain = str_replace( '{{' . $key . '}}', $val, $plain );
				}

				$mail->msgHTML( $html, $GLOBALS['email']['templates'] . 'optoutccc/' );
				//Replace the plain text body with one created manually
				$mail->AltBody = $plain;
				$mail->send();
			}
		}

	} else {
		$resultArray['result'] = 'error';
	}

	output( $resultArray );
}elseif( $request['a'] == 'unitVerify' ) {
	$resultArray = array();
	$home = true;

	if( !isset( $request['name'] ) ) {
		$request['name'] = $_SESSION['lastName'];
		$home = false;
	}

	$sqlVerify = "SELECT unit_address_1,
	unit_address_2,
	r.resident_num,
	e.paperless
	FROM cpm_residents r
	INNER JOIN cpm_lookup_residents lr ON lr.resident_lookup = r.resident_num
	INNER JOIN cpm_units u ON u.resident_num = r.resident_num
	INNER JOIN cpm_residents_extra e ON e.resident_num = r.resident_num
	WHERE r.resident_name LIKE '%" . addslashes($request['name']) . "%'
	AND lr.resident_ref = '" . $request['ref'] . "'
	AND u.unit_postcode = '" . $request['postcode'] . "'
	AND u.unit_num = '" . $request['unit'] . "'";
	$result = @mysql_query($sqlVerify);
	$num = @mysql_num_rows($result);

	if( $num > 0 ) {
		$resultArray['result'] = true;
		$row = mysql_fetch_array( $result );

		$unitAddress = $row['unit_address_1'] . ', ' . $row['unit_address_2'];

		if ( !isset( $_SESSION['unit'] ) || $_SESSION['unit'] == '' ) {
			$_SESSION['unit'] = $unitAddress;
			$_SESSION['lastName'] = ucfirst( strtolower( $request['name'] ) );
			$_SESSION['title'] = $request['title'];
			$_SESSION['postcode'] = $request['postcode'];
			$_SESSION['optIn'] = $row['paperless'];
		}

		if ( !isset( $_SESSION['refs'] ) ) {
			$_SESSION['refs'] = array();
			if ( $home == false ) {
				$resultArray['result'] = 'error';
			}
		}

		if ( !isset( $_SESSION['unit_refs'] ) ) {
			$_SESSION['unit_refs'] = array();
		}

		if ( !in_array( $row['resident_num'], $_SESSION['refs'] ) ) {
			$_SESSION['refs'][] = $row['resident_num'];
		}

		if ( !in_array( $unitAddress, $_SESSION['unit_refs'] ) ) {
			$_SESSION['unit_refs'][] = $unitAddress;
		}
	} else {
		$resultArray['result'] = false;
	}

	if( $resultArray['result'] == 'error'){
		unset($_SESSION);
	}

	output( $resultArray );
}elseif( $request['a'] == 'verify' ) {
	$resultArray = array();

	$sqlVerify = "SELECT r.resident_num,
	r.rmc_num
	FROM cpm_residents r
	INNER JOIN cpm_lookup_residents lr ON lr.resident_lookup = r.resident_num
	INNER JOIN cpm_units u ON u.resident_num = r.resident_num
	WHERE resident_name LIKE '%" . addslashes($request['name']) . "%'
	AND resident_ref = '" . $request['ref'] . "'
	AND unit_postcode = '" . $request['postcode'] . "'";
	$result = @mysql_query($sqlVerify);
	$num = @mysql_num_rows($result);

	if( $num > 0 ) {
		$resultArray['result'] = true;
		$resultArray['units'] = array();
		$row = @mysql_fetch_array( $result );

		$sqlUnits = "SELECT unit_address_1,
		unit_address_2,
		unit_num
		FROM cpm_units
		WHERE unit_postcode = '" . $request['postcode'] . "'
		AND rmc_num = '" . $row['rmc_num'] . "'
		ORDER BY unit_address_1, unit_address_2";
		$result = @mysql_query($sqlUnits);
		$num = @mysql_num_rows($result);

		if( $num > 0 ) {
			while( $row = @mysql_fetch_array( $result ) ) {
				$resultArray['units'][] = array('value' => $row['unit_num'], 'name' => $row['unit_address_1'] . ', ' . $row['unit_address_2'] );
			}
		}

	} else {
		$resultArray['result'] = false;
	}

	output( $resultArray );
}elseif( $request['a'] == 'lookupUp' ) {
	$resultArray = array();
	if ( $request['country'] == 15 ) {
		$resultArray['results'] = ukLookup( $request['search'] );
	} else {
		$resultArray['results'] = internationalLookup( $request['country'], $request['search'] );
	}
	output( $resultArray );
}elseif( $request['a'] == 'countries' ) {
	$SQL = "SELECT * FROM country ORDER BY name ASC";
	$result = mysql_query($SQL);
	$data = array();
	$i = 0;
	if ( mysql_num_rows($result) > 0 ) {
		$data['results'][$i]['name'] = " - Please Select - ";
		$data['results'][$i]['value'] = '';
		$i++;

		while($row = mysql_fetch_array($result)){
			$data['results'][$i]['name'] = $row['name'];
			$data['results'][$i]['value'] = $row['id'];
			$data['results'][$i]['code'] = $row['code'];
			$i++;
		}
	}

	output( $data );
} else {
	output( array( 'result' => false ) );
}

function output($array) {
	$protocol = ( isset( $_SERVER['SERVER_PROTOCOL'] ) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0' );

	header( $protocol . ' 200 OK' );
	header( 'Content-Type: application/json; charset=utf8' );
	echo json_encode($array);
}

function ukLookup( $postcode )
{
	$addressInfo = array();
	try {
		$account = "2976";
		$password = "qif20ljn";
		$url = "http://ws1.postcodesoftware.co.uk/lookup.asmx/getAddress?account=" . $account . "&password=" . $password . "&postcode=" . $postcode;
		$xml = simplexml_load_file( str_replace( ' ', '', $url ) );

		if( $xml->ErrorNumber <> 0 ) {
			/// If an error has occured log message
			throw new Exception( $xml->ErrorMessage );
		} else {
			/// Split up premise data
			$chunks = explode( ";", $xml->PremiseData );
			$id = 1;
			foreach( $chunks as $premise ) {
				if( $premise <> "" ) {
					$addressName = '';

					/// Splits premise into organisation, building and number
					list( $organisation, $building, $number ) = explode( '|', $premise );

					if( $organisation <> "" ) {
						$addressName .= $organisation . ", ";
					}

					if( $building <> "" ) {
						$addressName .= str_replace( "/", ", ", $building ) . ", ";
					}

					if($number <> "") {
						$addressName .= $number . ' ';
					}

					$addressName .= $xml->Address1;

					$tempArray = array();
					$tempArray['name'] = $addressName;
					$tempArray['value'] = $id;
					$addressInfo['addresses'][] = $tempArray;
					$id ++;
				}
			}

			if( $xml->Address2 <> "" ) {
				$addressInfo['address2'] = (string) $xml->Address2;
			}

			if( $xml->Address3 <> "" ) {
				$addressInfo['address3'] = (string) $xml->Address3;
			}

			if( $xml->Address4 <> "" ) {
				$addressInfo['address4'] = (string) $xml->Address4;
			}

			$addressInfo['town'] = (string) $xml->Town;
			$addressInfo['county'] = (string) $xml->County;
			$addressInfo['postcode'] = (string) $xml->Postcode;
		}
		return $addressInfo;
	} catch ( Exception $e ) {

	}
}

function internationalLookup( $countryId, $string )
{
	try {
		$SQL = "SELECT * FROM country WHERE id='" . $countryId . "'";
		$result = mysql_query($SQL);
		if ( mysql_num_rows($result) > 0 ) {
			while($row = mysql_fetch_array($result)){
				$countryCode = $row['three_code'];
			}
		}

		$addressArray = explode( ",", $string );
		$strings = '';
		foreach( $addressArray as $addressString ) {
			$strings .= "<string>" . trim( $addressString ) . "</string>";
		}

		$xml = '<?xml version="1.0" encoding="utf-8"?>
			<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
				<soap:Body>
					<Process xmlns="http://validator5.AddressDoctor.com/Webservice5/v2">
						<login>162133</login>
						<password>1oVny%02</password>
						<parameters>
							<ProcessMode>INTERACTIVE</ProcessMode>
							<ValidationParameters>
								<FormatType>ALL</FormatType>
								<FormatDelimiter>COMMA</FormatDelimiter>
								<DefaultCountryISO3>' . $countryCode .'</DefaultCountryISO3>
								<StreetWithNumber>true</StreetWithNumber>
          						<FormatWithCountry>false</FormatWithCountry>
          						<ElementAbbreviation>false</ElementAbbreviation>
								<PreferredScript>LATIN</PreferredScript>
								<PreferredLanguage>ENGLISH</PreferredLanguage>
								<AliasStreet>OFFICIAL</AliasStreet>
								<AliasLocality>OFFICIAL</AliasLocality>
								<GlobalCasing>MIXED</GlobalCasing>
								<GlobalPreferredDescriptor>DATABASE</GlobalPreferredDescriptor>
								<MatchingScope>ALL</MatchingScope>
								<MaxResultCount>100</MaxResultCount>
								<DualAddressPriority>POSTAL_ADMIN</DualAddressPriority>
								<StandardizeInvalidAddresses>true</StandardizeInvalidAddresses>
								<RangesToExpand>ALL</RangesToExpand>
								<FlexibleRangeExpansion>true</FlexibleRangeExpansion>
								<MatchingAlternatives>ALL</MatchingAlternatives>
								<MatchingExtendedArchive>false</MatchingExtendedArchive>
								<FormatMaxLines>19</FormatMaxLines>
							</ValidationParameters>
						</parameters>
						<addresses>
							<Address>
								<FormattedAddress>
									' . $strings . '
									<string>' . $countryCode . '</string>
								</FormattedAddress>
							</Address>
						</addresses>
					</Process>
				</soap:Body>
			</soap:Envelope>';

		$webservice = new webservice();
		$webservice->hostaddress = 'http://validator5.AddressDoctor.com/Webservice5/v2/AddressValidation.asmx';
		$webservice->xml = $xml;
		$webservice->process_headers[] = 'Host: validator5.addressdoctor.com';
		$webservice->process_headers[] = 'Content-Type: text/xml; charset=utf-8';
		$webservice->process_headers[] = "Content-Length : " . strlen ($xml);
		$webservice->process_headers[] = 'SOAPAction: "http://validator5.AddressDoctor.com/Webservice5/v2/Process"';

		$response = $webservice->post('CURL_HTTP_VERSION_1_1');

		$addressInfo['output'] = $response['output'];

		$output = simplexml_load_string( str_replace( '<soap:Body>', '', str_replace( '</soap:Body>', '', str_replace( '</soap:Envelope>', '', str_replace('<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">', '', $response['output'] ) ) ) ) );
		if( $output->ProcessResult->StatusCode <> 100 ) {
			/// If an error has occured log message
			throw new Exception( $output->ProcessResult->StatusMessage );
		}else{
			$resultArray = $output->ProcessResult->Results->Result->ResultDataSet->ResultData;

			if ( isset( $resultArray[0]->Address->Building ) ) {
				$addressInfo['address1'] = formatAddressDoctorLine( $resultArray[0]->Address->Building );
			}

			if ( isset( $resultArray[0]->Address->SubBuilding ) ) {
				if ( isset( $addressInfo['address1'] ) ) {
					$addressInfo['address2'] = formatAddressDoctorLine( $resultArray[0]->Address->SubBuilding );
				} else {
					$addressInfo['address1'] = formatAddressDoctorLine( $resultArray[0]->Address->SubBuilding );
				}
			}

			$id = 1;
			foreach( $resultArray as $resultAddress ) {
				$addressResult = $resultAddress->Address;
				$addressName = formatAddressDoctorLine( $addressResult->Street );
				if( isset( $addressResult->Building ) ) {
					$building = formatAddressDoctorLine( $addressResult->Building );
					if ( $building <> "" ) {
						$addressName = str_replace( "/", ", ", $building ) . ", " . $addressName;
					}
				}

				$tempArray = array();
				$tempArray['name'] = $addressName;
				$tempArray['value'] = $id;
				$addressInfo['addresses'][] = $tempArray;
				$id++;
			}

			if ( isset( $resultArray[0]->Address->Locality ) ) {
				$locality = formatAddressDoctorLine( $resultArray[0]->Address->Locality );
				if( is_array( $locality ) ){
					$addressInfo['address3'] = $locality[ 1 ];
					$addressInfo['town'] = $locality[0];
				}else{
					$addressInfo['town'] = $locality;
				}
			}

			if ( isset( $resultArray[0]->Address->Province ) ) {
				$addressInfo['state'] = formatAddressDoctorLine( $resultArray[0]->Address->Province )[0];
			}

			if ( isset( $resultArray[0]->Address->PostalCode ) ) {
				$addressInfo['postcode'] = formatAddressDoctorLine( $resultArray[0]->Address->PostalCode );
			}
		}
		return $addressInfo;
	} catch ( Exception $e ) {
		var_dump( $output );
	}
}

function formatAddressDoctorLine( $line )
{
	if( count($line->string) > 1 ) {
		$output = array();
		foreach( $line->string as $string ){
			$output[] = (string)$string;
		}
		return $output;
	}else {
		return (string)$line->string;
	}
}