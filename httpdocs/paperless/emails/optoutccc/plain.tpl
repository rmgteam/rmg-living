The below lessee has opted out.\r\n
Last Name: {{last}}\r\n
Tenant Reference: {{refs}}\r\n
Revert to Unit {{unit-revert}}\r\n\r\n
Alternative Address\r\n
Line 1: {{line1}}\r\n
Line 2: {{line2}}\r\n
Line 3: {{line3}}\r\n
City: {{town}}\r\n
County: {{county}}\r\n
Postal Code: {{postcode}}\r\n
Country: {{country}}