<font face="Verdana">
	<p>The below lessee has opted out.</p>
	<p><strong>Last Name: </strong> {{last}}</p>
	<p><strong>Tenant Reference: </strong> {{refs}}</p>
	<p><strong>Revert to Unit</strong> {{unit-revert}}</p>
	<p>Alternative Address</p>
	<p><strong>Line 1: </strong> {{line1}}</p>
	<p><strong>Line 2: </strong> {{line2}}</p>
	<p><strong>Line 3: </strong> {{line3}}</p>
	<p><strong>City: </strong> {{town}}</p>
	<p><strong>County: </strong> {{county}}</p>
	<p><strong>Postal Code: </strong> {{postcode}}</p>
	<p><strong>Country: </strong> {{country}}</p>
</font>