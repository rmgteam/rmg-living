Please can you contact this customer with the information below who has tried to sign up to paperless but their details are incorrect. The customer has requested to be contacted using the details below.
First Name: {{first}}
Last Name: {{last}}
Method: {{method}}
Email: {{email}}
Telephone: {{telephone}}
Mobile: {{mobile}}
