<font face="Verdana">
	<p>Please can you contact this customer with the information below who has tried to sign up to paperless but their details are incorrect. The customer has requested to be contacted using the details below.</p>
	<p><strong>First Name: </strong> {{first}}</p>
	<p><strong>Last Name: </strong> {{last}}</p>
	<p><strong>Contact Method: </strong> {{method}}</p>
	<p><strong>Email: </strong> {{email}}</p>
	<p><strong>Telephone: </strong> {{telephone}}</p>
	<p><strong>Mobile: </strong> {{mobile}}</p>

</font>