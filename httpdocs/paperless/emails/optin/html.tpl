<font face="Verdana">
	<p style="text-align: center;"><img src='cid:rmg_logo' /></p>
	<p><font color="#0096da"><strong>Welcome to RMG's Paperless Service</strong></font></p>
	<p>This email is confirmation that you have opted to receive your correspondence via our Paperless Service. This scheme is our latest innovation to reduce our impact on the environment and improve customer access to our services.</p>
	<p>As from today you will receive Invoices, Payment Reminders and any other correspondence from RMG straight to this email address.</p>
	<p>Please note that whilst you are opted into this service all correspondence will contain the unit property address details.</p>
	<p>Please <a href="{{policyUrl}}">click here</a> to see the policies and terms and conditions you accepted by joining.</p>
	<p>You have signed up the following units: <br />{{unitList}}</p>
	<p>Thank you for your continued support and for joining our paperless scheme.</p>
	<p><img src='cid:hugh_image' /></p>
	<p>Hugh McGeever<br />
	<strong>Managing Director</strong><br/>
	<strong>Residential Management Group Ltd.</strong></p>
	<p style='font-size:7.5pt'>In the event you have received this email by mistake or would like to opt out of RMG's Paperless Service you can do so via our <a href="{{url}}">website</a>, contacting our customer contact centre on 0345 002 4444 or alternatively through your leaseholder account at RMG Living account at <a href="https://www.rmgliving.co.uk">www.rmgliving.co.uk</a>.</p>
	<p style='font-size:7.5pt'><strong>If you have a question in relation to the RMG Paperless Policies then please send an email to <a href="mailto:policies@rmguk.com">policies@rmguk.com</a> and a response will be sent to you within 5 working days.</strong></p>
	<p style='font-size:7.5pt'><strong>Please do not respond to this email address as your email will not be read, thank you.</strong></p>
	<p style='font-size:7.5pt'>This message is intended only for the use of the addressee and may contain information that is PRIVILEGED and/or CONFIDENTIAL. If you are not the intended recipient, you are hereby notified that any dissemination of this communication is strictly prohibited. If you have received this communication in error, please erase all copies of the message and its attachments and notify us immediately on <a href="mailto:itservicedesk@rmguk.com">itservicedesk@rmguk.com</a>. Whilst attachments are checked, neither the sender nor the Residential Management Group accepts any liability in respect of any virus that has not been detected.</p>
	<p style='font-size:7.5pt'>Please think before you print!</p>
	<p style='font-size:7.5pt'>Registered Office: RMG House, Essex Road, Hoddesdon, Hertfordshire, EN11 0DR.<br>Registered in England No. 01513643</p>
</font>
