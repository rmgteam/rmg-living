Welcome to RMG's Paperless Service\r\n\r\n
This email is confirmation that you have opted to receive your correspondence via our Paperless Service. This scheme is our latest innovation to reduce our impact on the environment and improve customer access to our services.\r\n\r\n
As from today you will receive Invoices, Payment Reminders and any other correspondence from RMG straight to this email address.\r\n\r\n
Please note that whilst you are opted into this service all correspondence correspondence will contain the unit property address details.\r\n\r\n
Please visit {{policyUrl}} to see the policies and terms and conditions you accepted by joining.\r\n\r\n
You have signed up the following units: \r\n{{plainUnitList}}\r\n\r\n
Thank you for your continued support and for joining our paperless scheme.\r\n\r\n
Hugh Mcgeever\r\n
Managing Director\r\n
Residential Management Group\r\n\r\n

In the event you have received this email by mistake or would like to opt out of RMG's Paperless Service you can do so via our website at {{url}}, contacting our customer contact centre on 0345 002 4444 or alternatively through your leaseholder account at RMG Living account at www.rmgliving.co.uk.\r\n\r\n
If you have a question in relation to the RMG Paperless Policies then please send an email to policies@rmguk.com and a response will be sent to you within 5 working days.\r\n\r\n
Please do not respond to this email address as your email will not be read, thank you.\r\n\r\n
This message is intended only for the use of the addressee and may contain information that is PRIVILEGED and/or CONFIDENTIAL. If you are not the intended recipient, you are hereby notified that any dissemination of this communication is strictly prohibited. If you have received this communication in error, please erase all copies of the message and its attachments and notify us immediately on itservicedesk@rmguk.com . Whilst attachments are checked, neither the sender nor the Residential Management Group accepts any liability in respect of any virus that has not been detected.\r\n\r\n

Please think before you print!\r\n\r\n

Registered Office: RMG House, Essex Road, Hoddesdon, Hertfordshire, EN11 0DR.\r\n
Registered in England No. 01513643
