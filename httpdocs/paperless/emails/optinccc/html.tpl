<font face="Verdana">
	<p>The below lessee has signed up the following units that require linking to a Non-Resident tenant Id.</p>
	<p><strong>Last Name: </strong> {{last}}</p>
	<p><strong>Email: </strong> {{email}}</p>
	<p><strong>Telephone: </strong> {{telephone}}</p>
	<p><strong>Mobile: </strong> {{mobile}}</p>
	<p><strong>Tenant References: </strong> {{refs}}</p>
	<p><strong>To Review: </strong> {{review}}</p>
</font>