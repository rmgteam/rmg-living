$(document).on('ready', function(){

	$('#failForm').on('submit', function(e) {
		e.preventDefault();
	}).validate( {
		ignore: [],
		rules: [],
		onkeyup: false,
		success: function ( element ) {
			successFunc( element );
		},
		errorPlacement: function ( error, element ) {
			errorFunc( error, element );
		},
		submitHandler: function ( form ) {

			var telCountryData = $('#formTelInput' ).intlTelInput("getSelectedCountryData");
			var mobCountryData = $('#formMobileInput' ).intlTelInput("getSelectedCountryData");

			var tel = $('#formTelInput' ).intlTelInput( "getNumber", intlTelInputUtils.numberFormat.NATIONAL ).replace(/ /g, '');
			var mob = $('#formMobileInput' ).intlTelInput( "getNumber", intlTelInputUtils.numberFormat.NATIONAL ).replace(/ /g, '');

			if( telCountryData['dialCode'] != '44' ){
				tel = $('#formTelInput' ).intlTelInput( "getNumber", intlTelInputUtils.numberFormat.E164 ).replace('+', '00');
			}

			if( mobCountryData['dialCode'] != '44' ){
				mob = $('#formMobileInput' ).intlTelInput ("getNumber", intlTelInputUtils.numberFormat.E164 ).replace('+', '00');
			}

			var data = {
				'a': 'contactMe',
				'method': $('input[name="group1"]:checked').val(),
				'email': $('#formEmailInput').val(),
				'telephone': tel,
				'mobile': mob,
				'first': $('#formFirstInput' ).val(),
				'last': $('#formLastInput' ).val()
			};

			$(document ).ajaxExtend('setExecute', {
				"url": '/paperless/controller.php',
				"type": "POST",
				"data": data,
				"key": "send",
				"text": "Sending Your Details",
				"success": function ( data, textStatus, xhr ) {
					window.location = 'contact.php';
				}
			});
		}
	});

	initFields();
	$('#formEmailInput').rules('add', {
		required: true
	});
	$('#formConfirmEmailInput').rules('add', {
		equalTo: '#formEmailInput'
	});

	$('input[name="group1"]').on('change', function(){
		if( $(this).val() == 'email' ) {
			$('#formEmailInput').rules('add', {
				required: true
			});
			$('#formConfirmEmailInput').rules('add', {
				equalTo: '#formEmailInput'
			});
			$('#formTelInput').rules('remove', 'required');
			$('#formMobileInput').rules('remove', 'required');
			$('#numberRow' ).hide();
			$('#emailRow' ).show();
		}else{
			$('#formConfirmEmailInput').rules('remove', 'equalTo');
			$('#formEmailInput').rules('remove', 'required');
			successFunc('#formEmailInput');
			$('#formTelInput').rules('add', {
				require_from_group: [1, '.phone-group']
			});
			$('#formMobileInput').rules('add', {
				require_from_group: [1, '.phone-group']
			});
			$('#numberRow' ).show();
			$('#emailRow' ).hide();
		}
		//$('#failForm' ).valid();
	});

	$('#formEmailInput').rules('add', {
		required: true
	});
	$('#formConfirmEmailInput').rules('add', {
		equalTo: '#formEmailInput'
	});
	$('#formTelInput').rules('remove', 'required');
	$('#formMobileInput').rules('remove', 'required');
	$('#numberRow' ).hide();
	$('#emailRow' ).show();

});