/**
 * Created by Marc.Evans on 26/11/2014.
 */


(function( $, undefined ) {

	$.widget( "ui.ajaxExtend", {

		options: {
			"beforeSend": function(x){},													/// Execute before ajax call
			"complete": function(x){},														/// Execute on ajax call complete
			"success": function(data, textStatus, xhr){},									/// Execute on ajax call success
			"abortCallback": function(){},													/// Execute on ajax abort call
			"error": function (xhr, ajaxOptions, thrownError) {},							/// Execute on ajax call error
			"uploadProgress": function(event, i, position, total, percentComplete) {		/// Execute on upload progress
				var bar = $('#processing-progress_' + i + " .progress-bar");
				var percentVal = percentComplete + '%';
				bar.width(percentVal);
			},
			"url": '',																		/// URL of ajax request
			"type": 'GET',																	/// Type of ajax request
			"cache": false,																	/// Boolean on whether to cache
			"fileUpload": false,															/// Type of ajax request
			"form": $(),																	/// Form element
			"dataType": 'json',																/// data format
			"contentType": "application/json; charset=utf-8",								/// data content type
			"processData": true,															/// convert data to querystring
			"processing": true,																/// whether to show processing bar
			"data": "",																		/// data
			"authorisation": true,															/// Send authorisation request
			"apiKey": "",																	/// API key
			"token": "",																	/// Token
			"domain": "",																	/// Current Domain
			"domainVar": "",																/// Variable name to hold domain
			"abort": false,																	/// Boolean on whether to enable aborts
			"ajaxCalls": {},																/// store for AJAX calls
			"ajaxCallBacks": {},															/// store for AJAX call, callback event
			"key": '',																		/// Processing key
			"text": '',																		/// Processing text
			"customCall": {},																/// Config for request to be run
			"defaultPage": 'login.html',													/// Page to redirect to if authorisation fails
			"log": {},																		/// Log of requests
			"timeout": 1
		},

		_create: function(){
			var self = this;
			var options = self.options;

			// Create Processing Dialog
			var modal = $(document.createElement('div'))
				.addClass("modal fade")
				.attr('id', 'processing_modal');

			var dialog = $(document.createElement('div'))
				.addClass("modal-dialog");

			var content = $(document.createElement('div'))
				.addClass("modal-content");

			var header = $(document.createElement('div'))
				.addClass("modal-header");

			var button = $(document.createElement('button'))
				.attr({
					'type': 'button'
				})
				.on('click', {'self': self}, function(e){
					var self = e.data['self'];
					self.abortAll();
					self.hide();
				})
				.addClass("close");

			var span = $(document.createElement('span'))
				.attr("aria-hidden", 'true')
				.html('<i class="fa fa-remove"></i>')
				.appendTo(button);

			button.appendTo(header);

			var heading = $(document.createElement('h4'))
				.addClass("modal-title")
				.html('Processing')
				.appendTo(header);

			header.appendTo(content);

			var body = $(document.createElement('div'))
				.addClass("modal-body")
				.appendTo(content);

			content.appendTo(dialog);

			dialog.appendTo(modal);

			$('body').append(modal);

			var backdrop = $(document.createElement('div'))
				.addClass("modal-backdrop fade")
				.attr('id', 'processing-backdrop')
				.css({
					'opacity': 0.7,
					'background-color': '#000',
					position: 'fixed',
					height: '100% !important'
				})
				.hide()
				.appendTo($('body'));

			self.hide();

			$(document).ajaxStop(function() {
				options.timeout = setTimeout(function(){
					self.hide();
				}, 500);
			});

			self._trigger('_create');

		},

		hide: function() {
			var self = this;
			var options = self.options;

			$('#processing_modal')
				.removeClass('in')
				.attr('aria-hidden', true)
				.hide();

			$('#processing-backdrop')
				.removeClass('in')
				.attr('aria-hidden', true)
				.hide();

			self._trigger('hide');
		},

		show: function() {
			var self = this;
			var options = self.options;

			$('#processing-backdrop')
				.addClass('in')
				.attr('aria-hidden', false)
				.show();

			$('#processing_modal')
				.addClass('in')
				.attr('aria-hidden', false)
				.show();

			self._trigger('show');
		},

		/// Settings for current call
		set: function(object){
			var self = this;
			var options = self.options;

			options.customCall = $.extend(true, {}, options);
			options.customCall = $.extend(true, options.customCall, object);

			self._trigger('set');

		},

		supportFormData: function () {
			return !! window.FormData;
		},

		/// Execute
		execute: function () {
			var self = this;
			var options = self.options.customCall;
			self.doExecute(options);
		},

		doExecute: function(options) {
			var self = this;
			var mainOptions = self.options;
			var parseData = options.data;

			if( typeof options.data != FormData){
				options.data = JSON.stringify(options.data)
			}

			if ( options.fileUpload == true ) {

				if(self.supportFormData()){

					var queryString = "";

					if( options.form instanceof jQuery ) {
						var formData = new FormData(options.form.get(0));
						queryString = "?formName=" + options.form.attr('id');
					}else{
						var formData = options.form;
					}

					$.each(parseData, function (key, value) {
						if( queryString == '' ) {
							queryString += '?';
						}else{
							queryString += '&';
						}
						queryString += key + '=' + value;
					});

					$.ajax({
						url: options.url + queryString,  //Server script to process data
						type: 'POST',
						beforeSend: function(x) {

							var i = 0;

							var key = options.key;
							/// Get unique key
							while(mainOptions.ajaxCalls[key] !== undefined){
								key = options.key + i.toString();
								i++;
							}

							options.key = key;

							x['id'] = options.key;

							if ($.isFunction(options.beforeSend)) {
								options.beforeSend(x);
							}

							if(options.key.indexOf('sendError') < 0 ) {
								mainOptions.log[options.key] = {};
								mainOptions.log[options.key]['requestType'] = options.type;
								mainOptions.log[options.key]['requestUrl'] = options.url + queryString;
								mainOptions.log[options.key]['requestData'] = formData;
							}

							mainOptions.ajaxCalls[options.key] = x;

							if (options.authorisation) {

								var token = options.token;

								/// Get string if token is element
								if (typeof token != 'string') {
									token = token.val();
								}
								/// Do authorisation request
								x.setRequestHeader('Authorization', 'TRUEREST token=' + token + '&apikey=' + options.apiKey + "&" + options.domainVar + "=" + options.domain);
							}

							var text = options.text;

							if (text == '') {
								text = options.key;
							}

							/// Start processing
							if(options.processing) {
								self.start( text, options.key, options.abort );
							}

						},
						"xhr": function() {
							var xhr = new window.XMLHttpRequest();

							/// Upload progress
							xhr.upload.addEventListener("progress", function(event){
								var percent = 0;
								var position = event.loaded || event.position; /*event.position is deprecated*/
								var total = event.total;
								if (event.lengthComputable) {
									percent = Math.ceil(position / total * 100);
								}
								options.uploadProgress(event, options.key, position, total, percent);
							}, false);
							return xhr;
						},
						"success": function (data, textStatus, xhr) {
							if(options.key.indexOf('sendError') < 0 ) {
								mainOptions.log[options.key]['responseData'] = data;
								mainOptions.log[options.key]['responseStatus'] = xhr.status;
								//mainOptions.log[options.key]['responseText'] = xhr.responseText;
							}

							if ($.isFunction(options.success)) {
								options.success(data, textStatus, xhr)
							}

							self._trigger('execute');
						},
						"complete": function (x) {

							if ($.isFunction(options.complete)) {
								options.complete(x);
							}

							/// Find correct call and remove corresponding processing bar
							$.each(mainOptions.ajaxCalls, function (key, value) {
								if(value['id'] !== undefined) {
									if (value['id'] == x['id']) {
										self.end(key);
									}
								}
							});
						},
						"error": function(xhr){
							if(options.key.indexOf('sendError') < 0 ) {
								mainOptions.log[options.key]['responseStatus'] = xhr.status;
								mainOptions.log[options.key]['responseText'] = xhr.responseText;
							}
						},
						// Form data
						data: formData,
						//Options to tell jQuery not to process data or worry about content-type.
						cache: false,
						contentType: false,
						processData: false
					});

				}else{

					var formData = $(":text", $(options.prefix + 'Form')).serializeArray();

					if(!$.isEmptyObject(options.data)){
						$.each(options.data, function(index, value){
							formData.push({'name': index, 'value':value});
						});
					}

					if(!$.isEmptyObject(options.data)){
						$.each(options.data, function(index, value){
							if(typeof(value) == "string") {
								formData.push({'name': index, 'value':value});
							}else{
								formData.push({'name': index, 'value':value.val()});
							}
						});
					}

					var fileData = options.fileArray.serializeArray();

					if(options.key.indexOf('sendError') < 0 ) {
						mainOptions.log[options.key] = {};
						mainOptions.log[options.key]['requestType'] = options.type;
						mainOptions.log[options.key]['requestUrl'] = options.url;
						mainOptions.log[options.key]['requestData'] = formData;
					}

					$.ajax(options.url, {
						data: formData,
						files: fileData,
						iframe: true,
						processData: false
					}).complete(function(data) {
						options.completeCallback();
						self.close();
					}).success(function(data, textStatus, xhr){
						if(options.key.indexOf('sendError') < 0 ) {
							mainOptions.log[options.key]['responseData'] = data;
							mainOptions.log[options.key]['responseStatus'] = xhr.status;
							//mainOptions.log[options.key]['responseText'] = xhr.responseText;
						}
					}).error(function(xhr){
						if(options.key.indexOf('sendError') < 0 ) {
							mainOptions.log[options.key]['responseStatus'] = xhr.status;
							mainOptions.log[options.key]['responseText'] = xhr.responseText;
						}
					});
				}
			}else {

				$.ajax({
					"url": options.url,
					"dataType": options.dataType,
					"processData": options.processData,
					"cache": options.cache,
					"type": options.type,
					"xhr": function() {
						var xhr = new window.XMLHttpRequest();

						/// Upload progress
						xhr.upload.addEventListener("progress", function(event){
							var percent = 0;
							var position = event.loaded || event.position; /*event.position is deprecated*/
							var total = event.total;
							if (event.lengthComputable) {
								percent = Math.ceil(position / total * 100);
							}
							options.uploadProgress(event, options.key, position, total, percent);
						}, false);

						/// Download progress
						xhr.addEventListener("progress", function(event){
							var percent = 0;
							var position = event.loaded || event.position; /*event.position is deprecated*/
							var total = event.total;
							if (event.lengthComputable) {
								percent = Math.ceil(position / total * 100);
							}
							options.uploadProgress(event, options.key, position, total, percent);
						}, false);
						return xhr;
					},
					"beforeSend": function (x) {

						var i = 0;

						var key = options.key;
						/// Get unique key
						while(mainOptions.ajaxCalls[key] !== undefined){
							key = options.key + i.toString();
							i++;
						}

						options.key = key;

						x['id'] = options.key;

						if ($.isFunction(options.beforeSend)) {
							options.beforeSend(x);
						}

						if(options.key.indexOf('sendError') < 0 ) {
							mainOptions.log[options.key] = {};
							mainOptions.log[options.key]['requestType'] = options.type;
							mainOptions.log[options.key]['requestUrl'] = options.url;
							mainOptions.log[options.key]['requestData'] = options.data;
						}

						mainOptions.ajaxCalls[options.key] = x;
						mainOptions.ajaxCallBacks[options.key] = options.abortCallback;

						if (options.authorisation) {

							var token = options.token;

							/// Get string if token is element
							if (typeof token != 'string') {
								token = token.val();
							}

							/// Do authorisation request
							x.setRequestHeader('Authorization', 'TRUEREST token=' + token + '&apikey=' + options.apiKey + "&" + options.domainVar + "=" + options.domain);
						}

						var text = options.text;

						if (text == '') {
							text = options.key;
						}

						/// Start processing bar
						if(options.processing) {
							self.start( text, options.key, options.abort );
						}
					},
					"contentType": options.contentType,
					"data": options.data,
					"success": function (data, textStatus, xhr) {
						if(options.key.indexOf('sendError') < 0 ) {
							mainOptions.log[options.key]['responseData'] = data;
							mainOptions.log[options.key]['responseStatus'] = xhr.status;
							//mainOptions.log[options.key]['responseText'] = xhr.responseText;
						}

						if ($.isFunction(options.success)) {
							options.success(data, textStatus, xhr);
						}

						self._trigger('execute');
					},
					"complete": function (x) {
						if ($.isFunction(options.complete)) {
							options.complete(x);
						}

						/// Find correct call and remove corresponding processing bar
						$.each(mainOptions.ajaxCalls, function (key, value) {
							if(value['id'] !== undefined) {
								if (value['id'] == x['id']) {
									self.end(key);
								}
							}
						});
					},
					"error": function (xhr, ajaxOptions, thrownError) {
						if(options.key.indexOf('sendError') < 0 ) {
							mainOptions.log[options.key]['responseStatus'] = xhr.status;
							mainOptions.log[options.key]['responseText'] = xhr.responseText;
						}

						if( xhr.status == 401 && options.authorisation == true && location.pathname.indexOf('login') < 0 ){
							if (!Modernizr.history) {
								var location_url = window.location.toString();

								var location_array = location_url.split('#');

								/// Get page name, part after hash
								location_url = location_array[1];

								if(location_url === undefined){
									location_url = options.defaultPage;
								}
							} else {
								location_url = location.pathname;
							}
							location_url = options.defaultPage;

							var token = options.token;

							/// Get string if token is element
							if (typeof token != 'string') {
								token.val('');
								$(document).storage('set','token', '', 'local');
							}

							loggedOutMessage();
							/// Redirect page if authentication fails
							$(document).history('swapContent', location_url);
						}

						options.error(xhr, ajaxOptions, thrownError);
					}
				});
			}
		},

		/// Settings for current call
		setExecute: function(object){
			var self = this;
			var options = self.options;

			var customCall = $.extend(true, {}, options);
			customCall = $.extend(true, customCall, object);

			var mainOptions = self.options;
			var options = customCall;

			self.doExecute( options );
		},

		/// Build select options
		buildOptions: function(array, element) {
			var self = this;
			var options = self.options.customCall;

			if($.isArray(array)){

				/// For each array item
				for (var i = 0; i < array.length; i++) {
					var item = array[i];

					var type = 'option';
					var parentId = 0;

					if (item['type'] !== undefined) {
						type = item['type'];
					}

					if (item['parent'] !== undefined) {
						parentId = item['parent'];
					}

					/// Create option / optgroup
					var e = $(document.createElement(type));
					if (type == 'option') {
						e.attr('value', item['value']);
						e.html(item['name']);
					} else {
						e.attr({
							'data-value': item['value'],
							'label': item['name']
						});
					}

					if (type == 'option') {
						if (parentId == 0) {
							element.append(e);
						} else {
							element.find('optgroup').each(function () {
								if ($(this).attr('data-value') == parentId) {
									$(this).append(e);
								}
							});
						}
					} else {
						element.append(e);
					}
				}
			}
		},

		/// List
		list: function (element) {
			var self = this;
			var mainOptions = self.options;
			var options = self.options.customCall;

			$.ajax({
				"url": options.url,
				"dataType": options.dataType,
				"processData": options.processData,
				"cache": options.cache,
				"type": options.type,
				"xhr": function() {
					var xhr = new window.XMLHttpRequest();

					/// Upload progress
					xhr.upload.addEventListener("progress", function(event){
						var percent = 0;
						var position = event.loaded || event.position; /*event.position is deprecated*/
						var total = event.total;
						if (event.lengthComputable) {
							percent = Math.ceil(position / total * 100);
						}
						options.uploadProgress(event, options.key, position, total, percent);
					}, false);

					/// Download progress
					xhr.addEventListener("progress", function(event){
						var percent = 0;
						var position = event.loaded || event.position; /*event.position is deprecated*/
						var total = event.total;
						if (event.lengthComputable) {
							percent = Math.ceil(position / total * 100);
						}
						options.uploadProgress(event, options.key, position, total, percent);
					}, false);
					return xhr;
				},
				"beforeSend": function (x) {

					var i = 0;

					var key = options.key;
					/// Get unique key
					while(mainOptions.ajaxCalls[key] !== undefined){
						key = options.key + i.toString();
						i++;
					}

					options.key = key;

					x['id'] = options.key;

					if ($.isFunction(options.beforeSend)) {
						options.beforeSend(x);
					}

					if(options.key.indexOf('sendError') < 0 ) {
						mainOptions.log[options.key] = {};
						mainOptions.log[options.key]['requestType'] = options.type;
						mainOptions.log[options.key]['requestUrl'] = options.url;
						mainOptions.log[options.key]['requestData'] = options.data;
					}

					mainOptions.ajaxCalls[options.key] = x;
					mainOptions.ajaxCallBacks[options.key] = options.abortCallback;

					if (options.authorisation) {

						var token = options.token;

						/// Get string if token is element
						if (typeof token != 'string') {
							token = token.val();
						}

						/// Do authorisation request
						x.setRequestHeader('Authorization', 'TRUEREST token=' + token + '&apikey=' + options.apiKey + "&" + options.domainVar + "=" + options.domain);
					}

					var text = options.text;

					if (text == '') {
						text = options.key;
					}

					/// Start processing bar
					self.start(text, options.key, options.abort);
				},
				"uploadProgress": function(event, i, position, total, percentComplete) {
					if ($.isFunction(options.uploadProgress)) {
						options.uploadProgress(event, i, position, total, percentComplete);
					}
				},
				"contentType": options.contentType,
				"data": options.data,
				"success": function (data, textStatus, xhr) {
					if(options.key.indexOf('sendError') < 0 ) {
						mainOptions.log[options.key]['responseData'] = data;
						mainOptions.log[options.key]['responseStatus'] = xhr.status;
						//mainOptions.log[options.key]['responseText'] = xhr.responseText;
					}

					if(typeof element == "object") {
						/// Remove all options / optgroups
						element.find('option').remove();
						element.find('optgroup').remove();

						if( xhr.status == 200 ) {
							self.buildOptions(data['results'], element);
						} else if( xhr.status == 204 ) {
							self.buildOptions([{"name":"No Results Found", "value": ""}], element);
						}
					}

					if ($.isFunction(options.success)) {
						options.success(data, textStatus, xhr)
					}

					self._trigger('execute');
				},
				"complete": function (x) {
					if ($.isFunction(options.complete)) {
						options.complete(x);
					}

					/// Find correct call and remove corresponding processing bar
					$.each(mainOptions.ajaxCalls, function (key, value) {
						if(value['id'] !== undefined) {
							if (value['id'] == x['id']) {
								self.end(key);
							}
						}
					})
				},
				"error": function (xhr, ajaxOptions, thrownError) {
					if(options.key.indexOf('sendError') < 0 ) {
						mainOptions.log[options.key]['responseStatus'] = xhr.status;
						mainOptions.log[options.key]['responseText'] = xhr.responseText;
					}
					if( xhr.status == 401 && options.authorisation == true && location.pathname.indexOf('login') < 0 ){
						if (!Modernizr.history) {
							var location_url = window.location.toString();

							var location_array = location_url.split('#');

							/// Get page name, part after hash
							location_url = location_array[1];

							if(location_url === undefined){
								location_url = options.defaultPage;
							}
						} else {
							location_url = location.pathname;
						}

						var token = options.token;

						/// Get string if token is element
						if (typeof token != 'string') {
							token.val('');
							//$(document).storage('set','token', '', 'session');
							$(document).storage('set','token', '', 'local');
						}

						loggedOutMessage();
						/// Redirect page if authentication fails
						$(document).history('swapContent', location_url);
					}

					options.error(xhr, ajaxOptions, thrownError);
				}
			});
		},

		listExecute: function (object, element) {
			var self = this;
			var options = self.options;

			var customCall = $.extend(true, {}, options);
			customCall = $.extend(true, customCall, object);

			var mainOptions = self.options;
			var options = customCall;

			$.ajax({
				"url": options.url,
				"dataType": options.dataType,
				"processData": options.processData,
				"cache": options.cache,
				"type": options.type,
				"xhr": function() {
					var xhr = new window.XMLHttpRequest();

					/// Upload progress
					xhr.upload.addEventListener("progress", function(event){
						var percent = 0;
						var position = event.loaded || event.position; /*event.position is deprecated*/
						var total = event.total;
						if (event.lengthComputable) {
							percent = Math.ceil(position / total * 100);
						}
						options.uploadProgress(event, options.key, position, total, percent);
					}, false);

					/// Download progress
					xhr.addEventListener("progress", function(event){
						var percent = 0;
						var position = event.loaded || event.position; /*event.position is deprecated*/
						var total = event.total;
						if (event.lengthComputable) {
							percent = Math.ceil(position / total * 100);
						}
						options.uploadProgress(event, options.key, position, total, percent);
					}, false);
					return xhr;
				},
				"beforeSend": function (x) {

					var i = 0;

					var key = options.key;
					/// Get unique key
					while(mainOptions.ajaxCalls[key] !== undefined){
						key = options.key + i.toString();
						i++;
					}

					options.key = key;

					x['id'] = options.key;

					if ($.isFunction(options.beforeSend)) {
						options.beforeSend(x);
					}

					if(options.key.indexOf('sendError') < 0 ) {
						mainOptions.log[options.key] = {};
						mainOptions.log[options.key]['requestType'] = options.type;
						mainOptions.log[options.key]['requestUrl'] = options.url;
						mainOptions.log[options.key]['requestData'] = options.data;
					}

					mainOptions.ajaxCalls[options.key] = x;
					mainOptions.ajaxCallBacks[options.key] = options.abortCallback;

					if (options.authorisation) {

						var token = options.token;

						/// Get string if token is element
						if (typeof token != 'string') {
							token = token.val();
						}

						/// Do authorisation request
						x.setRequestHeader('Authorization', 'TRUEREST token=' + token + '&apikey=' + options.apiKey + "&" + options.domainVar + "=" + options.domain);
					}

					var text = options.text;

					if (text == '') {
						text = options.key;
					}

					/// Start processing bar
					self.start(text, options.key, options.abort);
				},
				"uploadProgress": function(event, i, position, total, percentComplete) {
					if ($.isFunction(options.uploadProgress)) {
						options.uploadProgress(event, i, position, total, percentComplete);
					}
				},
				"contentType": options.contentType,
				"data": options.data,
				"success": function (data, textStatus, xhr) {
					if(options.key.indexOf('sendError') < 0 ) {
						mainOptions.log[options.key]['responseData'] = data;
						mainOptions.log[options.key]['responseStatus'] = xhr.status;
						//mainOptions.log[options.key]['responseText'] = xhr.responseText;
					}

					if(typeof element == "object") {
						/// Remove all options / optgroups
						element.find('option').remove();
						element.find('optgroup').remove();

						if( xhr.status == 200 ) {
							self.buildOptions(data['results'], element);
						} else if( xhr.status == 204 ) {
							self.buildOptions([{"name":"No Results Found", "value": ""}], element);
						}
					}

					if ($.isFunction(options.success)) {
						options.success(data, textStatus, xhr)
					}

					self._trigger('execute');
				},
				"complete": function (x) {
					if ($.isFunction(options.complete)) {
						options.complete(x);
					}

					/// Find correct call and remove corresponding processing bar
					$.each(mainOptions.ajaxCalls, function (key, value) {
						if(value['id'] !== undefined) {
							if (value['id'] == x['id']) {
								self.end(key);
							}
						}
					})
				},
				"error": function (xhr, ajaxOptions, thrownError) {
					if(options.key.indexOf('sendError') < 0 ) {
						mainOptions.log[options.key]['responseStatus'] = xhr.status;
						mainOptions.log[options.key]['responseText'] = xhr.responseText;
					}
					if( xhr.status == 401 && options.authorisation == true && location.pathname.indexOf('login') < 0 ){
						if (!Modernizr.history) {
							var location_url = window.location.toString();

							var location_array = location_url.split('#');

							/// Get page name, part after hash
							location_url = location_array[1];

							if(location_url === undefined){
								location_url = options.defaultPage;
							}
						} else {
							location_url = location.pathname;
						}

						var token = options.token;

						/// Get string if token is element
						if (typeof token != 'string') {
							token.val('');
							//$(document).storage('set','token', '', 'session');
							$(document).storage('set','token', '', 'local');
						}

						loggedOutMessage();
						/// Redirect page if authentication fails
						$(document).history('swapContent', location_url);
					}

					options.error(xhr, ajaxOptions, thrownError);
				}
			});
		},

		/// Abort Call
		abort: function (key) {
			var self = this;
			var options = self.options;

			if(key in options.ajaxCalls){
				options.ajaxCalls[key].abort();
				var callback = options.ajaxCallBacks[key];

				if ($.isFunction(callback)) {
					callback();
				}

				self.end(key);
			}

			self._trigger('abort');
		},

		/// Abort All Ajax calls
		abortAll: function( ) {
			var self = this;
			var options = self.options;

			$.each(options.ajaxCalls, function(key, value){
				self.abort(key);
			});
			self._trigger('abortAll');
		},

		/// Start Processing
		start: function(text, val, abort){
			var self = this;
			var options = this.options;

			clearTimeout( options.timeout );

			if(typeof text === 'undefined'){
				text = '';
			}

			/// Create processing bar
			var processing_dialog_inner = $( document.createElement('div') )
				.attr({
					'id' :		'processing-progress_' + val
				})
				.addClass('progress progress-striped active');

			var processing_bar = $( document.createElement('div') )
				.addClass('progress-bar')
				.css({
					'width' : '100%'
				})
				.html(text);

			if(abort) {
				/// Add cancel button if can abort
				var closeDiv = $(document.createElement('div'))
					.addClass('fa fa-times-circle pull-right hand')
					.css({
						'margin': '5px 5px 0 0'
					})
					.on('click', {"self": self, "val": val}, function (e) {
						var self = e.data['self'];
						var val = e.data['val'];
						self.abort(val);
					})
					.appendTo(processing_bar);
			}

			processing_bar.appendTo(processing_dialog_inner);

			$('#processing_modal .modal-body').append(processing_dialog_inner);
			self.show();

			self._trigger('start');
		},

		/// Remove processing request
		end: function(i){
			var self = this;
			var options = this.options;

			/// Remove call from calls object
			delete options.ajaxCalls[i];

			self.complete(i);
			self._trigger('end');
		},

		/// Remove processing bar
		complete: function(i){
			var self = this;
			var options = this.options;

			/// Remove processing bar
			if($('#processing-progress_' + i ).length > 0) {
				$( '#processing-progress_' + i ).remove();
			}

			self._trigger('complete');
		},

		destroy: function() {
			var options = this.options;

			$('#processing_modal').remove();
			$('#processing_backdrop').remove();

			this._trigger('destroy');

			return $.Widget.prototype.destroy.call( this );
		},

		serializeObject: function(element)	{
			var o = {};
			var disabledArray = [];

			element.find(':disabled').each(function(){
				disabledArray.push($(this));
				$(this).removeAttr('disabled');
			});

			var a = element.serializeArray();
			$.each(a, function() {
				if (o[this.name] !== undefined) {
					if (!o[this.name].push) {
						o[this.name] = [o[this.name]];
					}
					o[this.name].push(this.value || '');
				} else {
					o[this.name] = this.value || '';
				}
			});

			$.each(disabledArray, function(key, value){
				value.attr('disabled', 'disabled');
			});

			return o;

			this._trigger('serializeObject');
		},

		getLog: function() {
			var self = this;
			var options = this.options;

			return options.log;
			this._trigger('getLog');
		}
	});
}( jQuery ) );