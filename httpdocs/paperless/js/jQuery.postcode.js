/*
checkPostCode Function:  	John Gardner
Plugin:						Marc Evans
*/

(function ($, undefined) {

	$.widget("ui.postcode", {

		options: {
			element: $(),														/// element plugin being applied to
			allowedChars: {
				alpha1: "[abcdefghijklmnoprstuwyz]",                       		/// Character 1
				alpha2: "[abcdefghklmnopqrstuvwxy]",                      		/// Character 2
				alpha3: "[abcdefghjkpmnrstuvwxy]",                         		/// Character 3
				alpha4: "[abehmnprvwxy]",                                  		/// Character 4
				alpha5: "[abdefghjlnpqrstuwxyz]",                          		/// Character 5
				BFPOa5: "[abdefghjlnpqrst]",                               		/// BFPO alpha5
				BFPOa6: "[abdefghjlnpqrstuwzyz]"                          		/// BFPO alpha6
			},
			language: {
				notValid: 'Please enter a valid postcode'
			}
		},

		_create: function () {
			var self = this;
			var options = this.options;

			options.element = this.element;

			/// Add data for val check
			options.element.data( 'plugin-name', 'postcode' );

			options.element.on('change', {'self': self }, function(e){
				var self = e.data['self'];
				var newPostCode = self.checkPostCode( $.trim( $( this ).val() ) );
				if (newPostCode) {
					$( this ).val( newPostCode );
					$( this ).attr('data-postcode', 'valid');

				} else {
					$( this ).attr('data-postcode', 'invalid');
				}
				self._trigger('update');
			});

			if(options.element.val() != ''){
				options.element.trigger('change');
			}

			self._trigger('_create');
		},

		checkPostCode: function( toCheck ) {
			var self = this;
			var options = this.options;

			// Permitted letters depend upon their position in the postcode.
			var alpha1 = options.allowedChars.alpha1;
			var alpha2 = options.allowedChars.alpha2;
			var alpha3 = options.allowedChars.alpha3;
			var alpha4 = options.allowedChars.alpha4;
			var alpha5 = options.allowedChars.alpha5;
			var BFPOa5 = options.allowedChars.BFPOa5;
			var BFPOa6 = options.allowedChars.BFPOa6;

			// Array holds the regular expressions for the valid postcodes
			var pcexp = new Array ();

			// BFPO postcodes
			pcexp.push (new RegExp ("^(bf1)(\\s*)([0-6]{1}" + BFPOa5 + "{1}" + BFPOa6 + "{1})$","i"));

			// Expression for postcodes: AN NAA, ANN NAA, AAN NAA, and AANN NAA
			pcexp.push (new RegExp ("^(" + alpha1 + "{1}" + alpha2 + "?[0-9]{1,2})(\\s*)([0-9]{1}" + alpha5 + "{2})$","i"));

			// Expression for postcodes: ANA NAA
			pcexp.push (new RegExp ("^(" + alpha1 + "{1}[0-9]{1}" + alpha3 + "{1})(\\s*)([0-9]{1}" + alpha5 + "{2})$","i"));

			// Expression for postcodes: AANA  NAA
			pcexp.push (new RegExp ("^(" + alpha1 + "{1}" + alpha2 + "{1}" + "?[0-9]{1}" + alpha4 +"{1})(\\s*)([0-9]{1}" + alpha5 + "{2})$","i"));

			// Exception for the special postcode GIR 0AA
			pcexp.push (/^(GIR)(\s*)(0AA)$/i);

			// Standard BFPO numbers
			pcexp.push (/^(bfpo)(\s*)([0-9]{1,4})$/i);

			// c/o BFPO numbers
			pcexp.push (/^(bfpo)(\s*)(c\/o\s*[0-9]{1,3})$/i);

			// Overseas Territories
			pcexp.push (/^([A-Z]{4})(\s*)(1ZZ)$/i);

			// Anguilla
			pcexp.push (/^(ai-2640)$/i);

			// Load up the string to check
			var postCode = toCheck;

			// Assume we're not going to find a valid postcode
			var valid = false;

			// Check the string against the types of post codes
			for ( var i=0; i<pcexp.length; i++) {

				if (pcexp[i].test(postCode)) {

					// The post code is valid - split the post code into component parts
					pcexp[i].exec(postCode);

					// Copy it back into the original string, converting it to uppercase and inserting a space
					// between the inward and outward codes
					postCode = RegExp.$1.toUpperCase() + " " + RegExp.$3.toUpperCase();

					// If it is a BFPO c/o type postcode, tidy up the "c/o" part
					postCode = postCode.replace (/C\/O\s*/,"c/o ");

					// If it is the Anguilla overseas territory postcode, we need to treat it specially
					if (toCheck.toUpperCase() == 'AI-2640') {postCode = 'AI-2640'};

					// Load new postcode back into the form element
					valid = true;

					// Remember that we have found that the code is valid and break from loop
					break;
				}
			}

			self._trigger( 'checkPostCode' );
			// Return with either the reformatted valid postcode or the original invalid postcode
			if (valid) {return postCode;} else return false;
		},

		destroy: function () {
			var self = this;
			var options = this.options;

			options.element.removeData('plugin-name');

			$.Widget.prototype.destroy.call(this);
		},

		setOption: function (key, value) {
			var self = this;
			var options = this.options;

			$.Widget.prototype._setOption.apply(this, arguments);
		},

		setOptions: function(newOptions){
			var self = this;
			var options = this.options;

			this.options = $.extend(true, options, newOptions);

			self._trigger('setOptions');
		}

	});
}(jQuery));
