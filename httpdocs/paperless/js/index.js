$(document).on('ready', function() {

	$( '#successBtn' ).hide();

	$( '#verifyPostcodeInput' ).on( 'change', function () {
		if ( $( '#verifyPostcodeInput' ).val() == '' ) {
			$( '#unitSelectPnl' ).hide();
		}
	} );
	$( '#checkPostcode' ).on( 'click', function ( e ) {
		e.preventDefault();
		$( '#unitSelectPnl' ).show();
		$( '#verifyFail' ).hide();
		$( '#verifyUnitInput' ).find( 'option' ).remove();
		if ( $( '#verifyPostcodeInput' ).val() == '' ) {
			$( '#verifyUnitInput' ).append( $( '<option value="">Please Select a Unit</option>' ) );
			$( '#unitSelectPnl' ).hide();
		} else {
			var data = {
				'a': 'verify',
				'postcode': $( '#verifyPostcodeInput' ).val(),
				'name': $( '#verifyNameInput' ).val(),
				'ref': $( '#verifyTenantRefInput' ).val()
			};

			$( document ).ajaxExtend( 'setExecute', {
				"url": '/paperless/controller.php',
				"type": "POST",
				"data": data,
				"key": "verify",
				"text": "Verifying Details",
				"success": function ( data, textStatus, xhr ) {
					if ( data['result'] != true ) {
						$( '#verifyFail' ).show();
						$( '#new_whoson' ).popover( 'show' );
						$( '#verifyUnitInput' ).append( $( '<option value="">Cannot Find Any Matches</option>' ) );
					} else {
						$( '#verifyUnitInput' ).append( $( '<option value="">Please Select a Unit</option>' ) );
						$.each( data['units'], function ( k, v ) {
							$( '#verifyUnitInput' ).append( $( '<option value="' + v['value'] + '">' + v['name'] + '</option>' ) );
						} );
					}
				}
			} );
		}
		$( '#verifyForm' ).valid();
	} );

	$('#verifyUnitInput').on('change', function(e) {
		if( $(this ).val() != '' )
			$( '#successBtn' ).show();
		else
			$( '#successBtn' ).hide();

	});

	$('#verifyForm').on('submit', function(e) {
		e.preventDefault();
	}).validate( {
		ignore: [],
		rules: [],
		onkeyup: false,
		success: function ( element ) {
			successFunc( element );
		},
		showErrors: function(errorMap, errorList) {
			$('#findFail' ).hide();
			if(errorList.length == 1){
				if($(errorList[0]['element'] ).attr('id') == 'verifyUnitInput' && $(errorList[0]['element'] ).is(':hidden')){
					$('#findFail' ).show();
				}
			}
			this.defaultShowErrors();
		},
		errorPlacement: function ( error, element ) {
			errorFunc( error, element );
		},
		submitHandler: function ( form ) {
			var data = {
				'a': 'unitVerify',
				'postcode': $('#verifyPostcodeInput').val(),
				'name': $('#verifyNameInput' ).val(),
				'ref': $('#verifyTenantRefInput' ).val(),
				'unit': $('#verifyUnitInput' ).val(),
				'title': $('#verifyTitleInput' ).val()
			};

			$(document ).ajaxExtend('setExecute', {
				"url": '/paperless/controller.php',
				"type": "POST",
				"data": data,
				"key": "unitVerify",
				"text": "Checking Unit Details",
				"success": function ( data, textStatus, xhr ) {
					if(data['result'] != true){
						$( '#verifyFail' ).show();
						$( '#new_whoson' ).popover('show');
					}else{

						window.location = 'paperless.php';
					}
				}
			});
		}
	});
	initFields();

	$('#verifyTitleInput' ).focus();
});