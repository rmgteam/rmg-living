/*
Plugin:						Marc Evans
*/

(function ($, undefined) {

	$.widget("ui.telephone", {

		options: {
			element: $(),														/// element plugin being applied to
			formats: [
				{number:'02', format: [3,4,4]},
				{number:'03', format: [4,3,4]},
				{number:'05', format: [5,6]},
				{number:'0500', format: [4,6]},
				{number:'07', format: [5,6]},
				{number:'08', format: [4,3,4]},
				{number:'01', format: [5,6]},
				{number:'011', format: [4,3,4]},
				{number:'0121', format: [4,3,4]},
				{number:'0131', format: [4,3,4]},
				{number:'0141', format: [4,3,4]},
				{number:'0151', format: [4,3,4]},
				{number:'0161', format: [4,3,4]},
				{number:'0191', format: [4,3,4]},
				{number:'013873', format: [6,5]},
				{number:'015242', format: [6,5]},
				{number:'015394', format: [6,5]},
				{number:'015395', format: [6,5]},
				{number:'015396', format: [6,5]},
				{number:'016973', format: [6,5]},
				{number:'016974', format: [6,5]},
				{number:'017683', format: [6,5]},
				{number:'017684', format: [6,5]},
				{number:'017687', format: [6,5]},
				{number:'019467', format: [6,5]},
				{number:'0169737', format: [5,6]},
				{number:'0169772', format: [6,4]},
				{number:'0169773', format: [6,4]},
				{number:'0169774', format: [6,4]}
			],
			language: {
				notValid: 'Please enter a valid telephone'
			}
		},

		_create: function () {
			var self = this;
			var options = this.options;

			options.element = this.element;

			/// Add data for val check
			options.element.data( 'plugin-name', 'telephone' );

			self.enable();

			if(options.element.val() != ''){
				options.element.trigger('change');
			}

			self._trigger('_create');
		},

		format: function (number) {
			var self = this;
			var options = self.options;

			// Change the international number format and remove any non-number character
			if( $.isNumeric(number)){
				// Get format array
				var matched = self.getFormat(number);
				if (matched != false){
					// Turn number into array based on Telephone Format
					var numberArray = self.splitNumber(number, matched);

					if(numberArray != false) {
						// Convert array back into string, split by spaces
						var nonFormattedNumber = numberArray.join( "" );
						var origNumber = number.replace(/ /g, '');
						var formattedNumber = numberArray.join( " " );

						if( origNumber !== nonFormattedNumber ) {
							return false;
						}

						return formattedNumber;
					}else{
						return false;
					}
				}else{
					return false;
				}
			}else{
				return false;
			}
		},

		getFormat: function(number) {
			var self = this;
			var options = self.options;
			var telephoneFormat = options.formats;

			// Sorts into longest number first
			telephoneFormat.sort(function(a, b) {
				var a1 = a.number.length, b1 = b.number.length;
				if (a1 == b1) return 0;
				return a1 < b1 ? 1 : -1;
			});

			var found = false;

			$.each(telephoneFormat, function(key,value) {
				if (number.substr(0,value.number.length) == value.number) {
					found = value.format;
					return false;
				}
			});

			return found;
		},

		splitNumber: function( number, split ) {
			var start = 0;
			var array = [];
			$.each(split, function( key, value ) {
				array.push( number.substr( start, value ) );
				start = start + value;
			});
			if(number.length < start) {
				return false;
			}else {
				return array;
			}
		},

		enable: function() {
			var self = this;
			var options = self.options;

			options.element.on('change', {'self': self }, function(e){
				var self = e.data['self'];
				var current = $( this ).val().replace( / /g, '' );
				var newtelephone = self.format( current );
				if (newtelephone) {
					$( this ).val( newtelephone );
					$( this ).attr('data-telephone', 'valid');

				} else {
					$( this ).val( current );
					$( this ).attr('data-telephone', 'invalid');
				}
				self._trigger('update');
			});

			self._trigger('enable');

		},

		disable: function() {
			var self = this;
			var options = self.options;

			options.element.off('change');

			self._trigger('disable');

		},

		destroy: function () {
			var self = this;
			var options = this.options;

			options.element.removeData('plugin-name');

			$.Widget.prototype.destroy.call(this);
		},

		setOption: function (key, value) {
			var self = this;
			var options = this.options;

			$.Widget.prototype._setOption.apply(this, arguments);
		},

		setOptions: function(newOptions){
			var self = this;
			var options = this.options;

			this.options = $.extend(true, options, newOptions);

			self._trigger('setOptions');
		}

	});
}(jQuery));
