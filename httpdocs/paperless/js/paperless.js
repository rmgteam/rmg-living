var isMobile = false; //initiate as false
// device detection
if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
	|| /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;

$(document).on('ready', function(){

	if(!isMobile){
		isMobile = iOS();
	}

	$('#emailAlertDialog' ).modal({
		show: false
	});

	$('#verifyPostcodeInput' ).on('change', function(){
		if($('#verifyPostcodeInput').val() == ''){
			$('#verifyUnitInput').append($('<option value="">Please Select a Unit</option>'));
			$('#unitSelectPnl').hide();
		}
	});
	$('#checkPostcode').on('click', function(e){
		e.preventDefault();
		$('#unitSelectPnl').show();
		$('#verifyFail').hide();
		$('#verifyUnitInput').find('option').remove();
		if($('#verifyPostcodeInput').val() == ''){
			$('#verifyUnitInput').append($('<option value="">Please Select a Unit</option>'));
			$('#unitSelectPnl').hide();
		}else {
			var data = {
				'a': 'verify',
				'postcode': $('#verifyPostcodeInput').val(),
				'ref': $('#verifyTenantRefInput' ).val()
			};

			$(document ).ajaxExtend('setExecute', {
				"url": '/paperless/controller.php',
				"type": "POST",
				"data": data,
				"key": "verify",
				"text": "Verifying Details",
				"success": function ( data, textStatus, xhr ) {
					if(data['result'] != true){
						$( '#verifyFail' ).show();
						$( '#new_whoson' ).popover('show');
						$( '#verifyUnitInput' ).append( $( '<option value="">Cannot Find Any Matches</option>' ) );
					}else{
						$( '#verifyUnitInput' ).append( $( '<option value="">Please Select a Unit</option>' ) );
						$.each(data['units'], function(k,v){
							$( '#verifyUnitInput' ).append( $( '<option value="' + v['value'] + '">' + v['name'] + '</option>' ) );
						});
					}
				}
			});
		}
		$('#modalForm' ).valid();
	});

	$.validator.addMethod("notEqualTo", function (value, element, param) {
		var target = $(param);
		if (value) return value != target.val();
		else return this.optional(element);
	}, "Must not match");

	$.validator.addMethod('notMobile', function(phone_number, element) {
		var countryData = $(element).intlTelInput("getSelectedCountryData");
		if ( countryData['iso2'] != 'us' && countryData['iso2'] != 'ca' ) {
			if ( $( element ).intlTelInput( "getNumberType" ) != 1 || this.optional( element ) ) {
				return true;
			} else {
				return false;
			}
		}else{
			return true;
		}
	}, 'Please specify a valid land line number');

	$.validator.addMethod('mobile', function(phone_number, element) {
		var countryData = $(element).intlTelInput("getSelectedCountryData");
		if ( countryData['iso2'] != 'us' && countryData['iso2'] != 'ca' ) {
			if ( $( element ).intlTelInput( "getNumberType" ) == 1 || this.optional( element ) ) {
				return true;
			} else {
				return false;
			}
		}else{
			return true;
		}
	}, 'Please specify a valid mobile number');

	$('#paperlessForm').on('submit', function(e) {
		e.preventDefault();
	}).validate( {
		ignore: [],
		rules: {
			'formTelInput': {
				'notMobile': true,
				'notEqualTo': '#formMobileInput'
			},
			'formMobileInput': {
				'mobile': true,
				'notEqualTo': '#formTelInput'
			}
		},
		onkeyup: false,
		success: function ( element ) {
			successFunc( element );
		},
		errorPlacement: function ( error, element ) {
			errorFunc( error, element );
		},
		submitHandler: function ( form ) {

			var telCountryData = $('#formTelInput' ).intlTelInput("getSelectedCountryData");
			var mobCountryData = $('#formMobileInput' ).intlTelInput("getSelectedCountryData");

			var tel = $('#formTelInput' ).intlTelInput( "getNumber", intlTelInputUtils.numberFormat.NATIONAL ).replace(/ /g, '');
			var mob = $('#formMobileInput' ).intlTelInput( "getNumber", intlTelInputUtils.numberFormat.NATIONAL ).replace(/ /g, '');

			if( telCountryData['dialCode'] != '44' ){
				tel = $('#formTelInput' ).intlTelInput( "getNumber", intlTelInputUtils.numberFormat.E164 ).replace('+', '00');
			}

			if( mobCountryData['dialCode'] != '44' ){
				mob = $('#formMobileInput' ).intlTelInput ("getNumber", intlTelInputUtils.numberFormat.E164 ).replace('+', '00');
			}

			var data = {
				'a': 'updatePaperless',
				'email': $('#formEmailInput').val(),
				'telephone': tel,
				'mobile': mob,
				'opt': $('input[name=group1]:checked' ).val() == 'yes' ? 'Y' : 'N',
				'unit-revert': $('input[name=group2]:checked' ).val() == 'yes' ? 'Y' : 'N',
				'line1': $("#formLine1Input" ).val(),
				'line2': $("#formLine2Input" ).val(),
				'line3': $("#formLine3Input" ).val(),
				'county': $("#formCountyInput" ).val(),
				'country': $("#formCountryIdInput" ).val(),
				'state': $("#formStateInput" ).val(),
				'town': $("#formCityInput" ).val(),
				'postcode': $("#formPostcodeInput" ).val()
			};

			$(document ).ajaxExtend('setExecute', {
				"url": '/paperless/controller.php',
				"type": "POST",
				"data": data,
				"key": "update",
				"text": "Updating Your Details",
				"success": function ( data, textStatus, xhr ) {
					if(data['result'] == false){
						$( '#verifyFail' ).show();
					}else if(data['result'] == true){
						window.location = 'thanks.php';
					}else if(data['result'] == 'email'){
						$('#messageDialogMessage' ).html('It appears that none of the data you have supplied has changed. If this is correct please leave this page, otherwise please add further units or amend your email address.');
						$('#emailAlertDialog' ).modal('show');
					}else{
						window.location = 'index.php';
					}
				}
			});
		}
	});

	$('#modalForm').on('submit', function(e) {
		e.preventDefault();
	}).validate( {
		ignore: [],
		rules: [],
		success: function ( element ) {
			successFunc( element );
		},
		errorPlacement: function ( error, element ) {
			errorFunc( error, element );
		},
		submitHandler: function ( form ) {
			var data = {
				'a': 'unitVerify',
				'postcode': $('#verifyPostcodeInput').val(),
				'ref': $('#verifyTenantRefInput' ).val(),
				'unit': $('#verifyUnitInput' ).val()
			};

			$(document ).ajaxExtend('setExecute', {
				"url": '/paperless/controller.php',
				"type": "POST",
				"data": data,
				"key": "unitVerify",
				"text": "Checking Unit Details",
				"success": function ( data, textStatus, xhr ) {
					if(data['result'] == false){
						$( '#verifyFail' ).show();
					}else if(data['result'] == true){
						var address = $('#verifyUnitInput option:selected').text();
						var postcode = $('#verifyPostcodeInput').val();

						$('#verifyTenantRefInput').val('');
						$('#verifyPostcodeInput').val('').trigger('change');
						$('#modal1' ).modal('hide');
						addRow( address, postcode );
					}else{
						window.location = 'index.php';
					}
				}
			});
		}
	});

	initFields();

	$('#cookiePolicy').hide();
	$('#cookieViewBtn').on('click', function(){
		if(isMobile){
			downloadFile('http://stage.rmgliving.co.uk/paperless/cookie-policy.pdf', 'cookie-policy.pdf');
		}else {
			if ( $( this ).html() == 'View' ) {
				$( '#cookiePolicy' ).show();
				$( this ).html( 'Hide' );
			} else {
				$( '#cookiePolicy' ).hide();
				$( this ).html( 'View' );
			}
		}
	});

	$('#termsPolicy').hide();
	$('#termsViewBtn').on('click', function(){
		if(isMobile){
			downloadFile('http://stage.rmgliving.co.uk/paperless/terms-conditions.pdf', 'terms-conditions.pdf');
		}else {
			if ( $( this ).html() == 'View' ) {
				$( '#termsPolicy' ).show();
				$( this ).html( 'Hide' );
			} else {
				$( '#termsPolicy' ).hide();
				$( this ).html( 'View' );
			}
		}
	});

	$('#cPolicyRow').hide();
	$('#policyTextRow' ).hide();
	$('#agreeRow').hide();
	$('#tPolicyRow').hide();
	$('#agreeTRow').hide();
	$('#emailTextRow' ).hide();
	$('#emailRow').hide();
	$('#confirmRow').hide();
	$('#phoneRow').hide();
	$('#mobileRow').hide();
	$('#unitRow').hide();
	$('#unitRevertRow').hide();
	$('#altAddressRow').hide();
	$('#Address').hide();

	$('input[name="group1"]').on('change', function(){
		if( $('input[name=group1]:checked', '#paperlessForm').val() == 'yes' ) {
			$('#cPolicyRow').show();
			$('#policyTextRow' ).show();
			$('#agreeRow').show();
			$('#tPolicyRow').show();
			$('#agreeTRow').show();
			$('#unitRow').show();
			$('#optTextRow').hide();
			$('#optInTextRow').hide();
			$('#unitRevertRow').hide();
			$('#altAddressRow').hide();
			if($('#paperless' ).val() == 'N') {
				$('#optInTextRow').show();
			}
			$('#chkCookie').rules('add', {
				'agree': true
			});
			$('#chkTerms').rules('add', {
				'agree': true
			});
		}else if( $('input[name=group1]:checked', '#paperlessForm').val() == 'no' ) {
			$('#cPolicyRow').hide();
			$('#policyTextRow' ).hide();
			$('#agreeRow').hide();
			$('#tPolicyRow').hide();
			$('#agreeTRow').hide();
			$('#unitRow').hide();
			$('#unitRevertRow').show();
			$('#altAddressRow').hide();
			if($('#paperless' ).val() == 'Y') {
				$( '#optTextRow' ).show();
			}
			$('#chkCookie').rules('remove');
			$('#chkTerms').rules('remove');
		}else{
			$('#cPolicyRow').hide();
			$('#policyTextRow' ).hide();
			$('#agreeRow').hide();
			$('#tPolicyRow').hide();
			$('#agreeTRow').hide();
			$('#unitRow').hide();
			//$('#unitRevertRow').show();
			$('#unitRevertRow').hide();
			$('#altAddressRow').hide();
			if($('#paperless' ).val() == 'Y') {
				$( '#optTextRow' ).show();
			}else{
				console.log('unitRevertRow');
				$('#unitRevertRow').show();
			}
			$('#chkCookie').rules('remove');
			$('#chkTerms').rules('remove');
		}
	});

	$('input[name="group2"]').on('change', function(){
		if( $(this).val() == 'yes' ) {
			$('#altAddressRow').hide();
		}else{
			$('#altAddressRow').show();
		}
		$('#formLookupInput' ).addressLookup('clear');
	});

	$('#chkCookie, #chkTerms').on('change', function(){
		if($('#chkCookie:checked').val() == 'yes' && $('#chkTerms:checked').val() == 'yes'){
			$('#emailTextRow' ).show();
			$('#emailRow').show();
			$('#confirmRow').show();
			$('#phoneRow').show();
			$('#mobileRow').show();
			$('#formEmailInput').attr('required', 'required');
			$('#formConfirmEmailInput').attr('required', 'required');
			$('#formConfirmEmailInput').rules('add', {
				equalTo: '#formEmailInput'
			});
		}else{
			$('#emailTextRow' ).hide();
			$('#emailRow').hide();
			$('#confirmRow').hide();
			$('#phoneRow').hide();
			$('#mobileRow').hide();
			$('#formEmailInput').removeAttr('required');
			$('#formConfirmEmailInput').removeAttr('required');
			$('#formConfirmEmailInput').rules('remove', 'equalTo');
		}

		if($('#chkCookie:checked').val() == 'yes'){
			$( '#cookiePolicy' ).hide();
			$( '#cookieViewBtn' ).html( 'View' );
		}

		if($('#chkTerms:checked').val() == 'yes'){
			$( '#termsPolicy' ).hide();
			$( '#termsViewBtn' ).html( 'View' );
		}
	});

	$('#cancelBtn').on('click', function(){
		if($( '#new_whoson' ).attr( 'aria-describedby' ) !== undefined){
			setTimeout( function() {
				$( '#new_whoson' ).popover( 'show' );
			}, 500);
		}
		$('#verifyFail' ).hide();
		$('#verifyTenantRefInput').val('');
		$('#verifyPostcodeInput').val('').trigger('change');
		$('#verifyUnitInput').find('option').remove();
		$('#verifyUnitInput').append($('<option value="">Please Select a Unit</option>'));
	});

	if( $('#paperless' ).val() == 'Y' ) {
		$('#optRow').show();
		$('#oldUserRow').show();
		$('#optVar' ).html('Opt Out of');
	}else{
		$('#optRow').hide();
		$('#newUserRow').show();
		$('#optVar' ).html('Opt In to');
	}
	$('input[name="group1"][value="yes"]').prop('checked', true).trigger('change');
	$('input.address-lookup' ).addressLookup();
});

function addRow( address, postcode ) {
	var row = $(document.createElement( 'tr' ));
	var addressTd = $(document.createElement( 'td' ))
		.html(address)
		.appendTo( row );
	var postcodeTd = $(document.createElement( 'td' ))
		.html(postcode)
		.appendTo( row );
	$('#unitTable tbody').append(row);
}

function downloadFile( url, filename ) {

	var a = document.createElement( "a" );

	if( iOS() ){
		window.open(url, '_blank');
	}else {
		if ( 'download' in a ) { //html5 A[download]
			a.href = url;
			a.setAttribute( "download", filename );
			a.innerHTML = "downloading...";
			document.body.appendChild( a );
			setTimeout( function () {
				a.click();
				document.body.removeChild( a );
			}, 66 );
		}
	}
}