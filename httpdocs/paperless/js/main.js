/**
 * Created by marc.evans on 15/07/2015.
 */

var thisUnavailable = false;

$(document ).on('ready', function(){
	if( thisUnavailable ) {
		location.href = "unavailable.php";
	}
});

$.validator.addMethod("agree", function(value, element){
	if( value != 'yes' ) {
		return false;
	}else{
		return true;
	}
}, "Please agree to the terms");

$.validator.addMethod("postcode", function(value, element){
	var valid = false;
	if( $(element ).attr('required') ) {
		if( $(element).attr('data-postcode') == 'valid' ) {
			valid = true;
		}
	}else{
		if( $(element).attr('data-postcode') != 'valid' ) {
			if( $(element ).val() == '' ){
				valid = true;
			}
		} else {
			valid = true;
		}
	}
	return valid;
}, "Please enter a valid postcode");

$.validator.addMethod('telephone', function(value, element) {
	var valid = false;
	if( $(element ).attr('required') ) {
		if( $(element).attr('data-telephone') == 'valid' ) {
			valid = true;
		}
	}else{
		if( $(element).attr('data-telephone') != 'valid' ) {
			if( $(element ).val() == '' ){
				valid = true;
			}
		} else {
			valid = true;
		}
	}
	return valid;

}, 'Please specify a valid phone number');

//ensures a given number of fields in a group a required
$.validator.addMethod("require_from_group", function (value, element, options) {
	var validator = this;
	var minRequired = options[0];
	var selector = options[1];
	var validOrNot = $(selector, element.form).filter(function () {
			return validator.elementValue(this);
		}).length >= minRequired;

	return validOrNot;
}, $.validator.format("Please fill at least {0} of these fields."));

jQuery.extend(jQuery.validator.messages, {
	equalTo: "Both email addresses must match."
});

function initFields(){

	$('[data-toggle="popover"]').popover();
	$('[data-toggle="popover"]')
		.on('mouseover', function(){
			$('[data-toggle="popover"]' ).not($(this)).popover('hide');
			$(this).popover('show');
		})
		.on('shown.bs.popover', function(){
			if( $(this ).data('class') !== undefined ) {
				var id = $( this ).attr( 'aria-describedby' );
				if ( !$( '#' + id ).hasClass( $( this ).data( 'class' ) ) ) {
					$( '#' + id ).addClass( $( this ).data( 'class' ) );
					$( this ).popover( 'show' );
				}
			}
		});

	$('input[type="tel"]').each(function() {
		$(this).rules('add', {
			telephone: true
		});
		$(this)
			.on('change', function() {
				if ($.trim($(this).val())) {
					if ($(this).intlTelInput("isValidNumber")) {
						$( this ).attr('data-telephone', 'valid');
					} else {
						$( this ).attr('data-telephone', 'invalid');
					}
					$( this ).closest('form' ).valid();
				}
			})
			.intlTelInput({
				'utilsScript': '/paperless/js/intlTel.utils.js',
				'preferredCountries': ['GB']
			});
	});

	$('input.postcode').postcode();

}

function iOS() {

	var iDevices = [
		'iPad Simulator',
		'iPhone Simulator',
		'iPod Simulator',
		'iPad',
		'iPhone',
		'iPod'
	];

	while (iDevices.length) {
		if (navigator.platform === iDevices.pop()){ return true; }
	}

	return false;
}

$(document).on('ready', function(){
	/// Click anywhere to close popovers, except elements that open popovers
	$(document).click(function(e) {
		if (!$(e.target).is('.popover') && !$(e.target).is('[data-toggle="popover"]')) {
			$('.popover').popover('hide');
		}
	});

	$(document ).ajaxExtend();

	if(iOS()){
		$('.svg-container-other .row' ).css({
			'background-image': 'url(/paperless/img/header_sm.png)'
		});
	}
});

function successFunc( element ){
	var id = '#' + $( element ).attr( 'id' ).replace( "-error", '' );
	if( $(id ).length <= 0 ){
		id = 'input[name="' + $( element ).attr( 'id' ).replace( "-error", '' ) + '"]';
	}

	$( id ).addClass( 'valid' );
	var formGroup = $( id ).closest( '.form-group' );
	formGroup
		.removeClass( 'has-error' )
		.addClass( 'has-no-valid' );


	var iGroup = $(id ).closest('.input-group');

	iGroup.find( 'span.input-group-addon' ).hide();

	if(iGroup.children().length > 2) {
		iGroup.find('.input-group-addon:not(span)' ).addClass('btn-addon');
		iGroup.find('.intl-tel' ).addClass('right-rounded');
	}else{
		if( $( id ).hasClass('form-control') ) {
			$( id ).addClass( 'rounded' );
		} else {
			if( $( id ).parent().hasClass('form-control') ) {
				$( id ).parent().addClass( 'rounded' );
			}
		}
	}
}

function errorFunc( error, element ){
	var formGroup = element.closest( '.form-group' );
	formGroup.addClass( 'has-error' ).removeClass( 'has-no-valid' );

	if( element.hasClass('rounded') ) {
		element.removeClass( 'rounded' );
	}else{
		if( element.parent().hasClass('rounded') ) {
			element.parent().removeClass( 'rounded' );
		}
	}
	var iGroup = element.closest('.input-group');
	element.removeClass('right-rounded');
	var span = iGroup.find('span.input-group-addon' );

	var i = $( document.createElement( 'i' ) )
		.addClass( 'glyphicon glyphicon-remove' )
		.attr( {
			'data-toggle': 'popover',
			'data-placement': 'top',
			'data-content': error.html()
		} );

	span
		.html( i )
		.css('display', 'table-cell');


	element.parent().find('.btn-addon' ).removeClass('btn-addon');

	$( '[data-toggle="popover"]' ).popover();
	formGroup.find( "span.input-group-addon i" ).popover( 'show' );
}