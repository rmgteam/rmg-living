(function ($, undefined) {
	$.widget("ui.addressLookup", {
		options: {
			element: $(),														/// element plugin being applied to
			divId: $(),
			ajax: {
				data: [],														/// data returned from search
				resultsUrl : '/paperless/controller.php?a=lookupUp',			/// url for search
				countryUrl : '/paperless/controller.php?a=countries',			/// url for countries
				key: 'addressLookup',											/// ajaxExtend processing key
				text: "Retrieving Search Results"								/// ajaxExtend processing string
			},
			language:{
				findText: 'Find',												/// text for Find button
				clearText: 'Clear',												/// text for clear button
				dialogTitle: 'Please select an Address',							/// text for advancedSelect title
				manual: 'Manually Enter Address'
			},
			classes: {
				flagButton: 'flag-dropdown input-group-addon',						/// class for flag button
				clearButton: 'btn btn-rmg clear',								/// class for clear button
				findButton: 'input-group-addon btn btn-sm btn-rmg btn-addon',										/// class for find button
				manualButton: 'btn btn-rmg',									/// class for maunal button
				surround: 'surround',											/// class for surround
				inlineGroup: 'intl-tel-input btn-group'							/// class for group
			},
			fields: {
				block: '',
				address1: '',
				address2: '',
				address3: '',
				county: '',
				country: '',
				town: '',
				state: '',
				postcode: ''
			},
			data: {
				addresses: [],
				address2: '',
				address3: '',
				county: '',
				town: '',
				state: '',
				postcode: ''
			},
			disabled: false,													/// boolean for if disabled
			defaultCountry: 15
		},

		_create: function () {

			var self = this;
			var options = self.options;

			options.element = this.element;
			var cssClasses = options.element.attr('class');

			if(cssClasses == undefined){
				cssClasses = '';
			}

			/// Add data for val check
			options.element.data('plugin-name', 'addressLookup');

			if(options.element.attr('disabled') == 'disabled'){
				options.disabled = true;
			}

			if(options.element.attr('title') != null){
				options.language.dialogTitle = options.element.attr('title');
			}

			if(options.element.data('title') != null){
				options.language.dialogTitle = options.element.data('title');
			}

			if(options.element.data('ajax-resultsurl') != null){
				options.ajax.resultsUrl = options.element.data('ajax-resultsurl');
			}

			if(options.element.data('address-block') != null){
				options.fields.block = options.element.data('address-block');
			}

			if(options.element.data('address-line1') != null){
				options.fields.address1 = options.element.data('address-line1');
			}

			if(options.element.data('address-line2') != null){
				options.fields.address2 = options.element.data('address-line2');
			}

			if(options.element.data('address-line3') != null){
				options.fields.address3 = options.element.data('address-line3');
			}

			if(options.element.data('address-county') != null){
				options.fields.county = options.element.data('address-county');
			}

			if(options.element.data('address-country') != null){
				options.fields.country = options.element.data('address-country');
			}

			if(options.element.data('address-town') != null){
				options.fields.town = options.element.data('address-town');
			}

			if(options.element.data('address-state') != null){
				options.fields.state = options.element.data('address-state');
			}

			if(options.element.data('address-postcode') != null){
				options.fields.postcode = options.element.data('address-postcode');
			}

			/// End check element attributes for settings
			/// Build inline structure
			self.buildInline().then(function(){
				var countryName = options.fields.country;
				if( options.disabled ) {
					countryName = options.fields.country.replace('Input', 'View');
				}
				$( countryName ).advancedSelect('set', options.defaultCountry);
				self.clear();

				if(options.disabled) {
					self.disable();
				}
			});

			self._trigger('_create');
		},

		buildInline: function() {
			var self = this;
			var options = self.options;

			var defer = $.Deferred();
			var promise = defer.promise();

			promise.progress(function() {
				var countryName = options.fields.country.replace( '#', '' );
				if( options.disabled ) {
					countryName = options.fields.country.replace('#', '').replace('Input', 'View');
				}

				/// Build button group
				var buttonGroup = options.element.parent();
				buttonGroup.addClass('address-lookup');

				/// Build open button
				var flag = $( document.createElement( 'div' ) )
					.addClass( options.classes.flagButton )
					.attr( {
						'id': options.element.attr( 'id' ) + 'Flag'
					} )
					.on( 'click', {'self': self}, function ( e ) {
						var self = e.data['self'];
						var options = self.options;
						$('#' + countryName ).advancedSelect( 'open' );
					} );

				/// Build open button
				var flagCont = $( document.createElement( 'div' ) )
					.addClass( 'selected-flag' )
					.attr( {
						'id': options.element.attr( 'id' ) + 'FlagCont'
					} );

				$( document.createElement( 'div' ) )
					.attr( 'id', options.element.attr( 'id' ) + 'FlagIcon' )
					.appendTo( flagCont );

				$( document.createElement( 'div' ) )
					.addClass( 'arrow' )
					.appendTo( flagCont );

				flagCont.appendTo(flag);
				buttonGroup.prepend( flag );

				/// Build clear button
				$( document.createElement( 'a' ) )
					.attr( {
						'id': options.element.attr( 'id' ) + 'ClearButton'
					} )
					.html( options.language.clearText )
					.addClass( options.classes.clearButton )
					.on( 'click', {'self': self}, function ( e ) {
						var self = e.data['self'];
						var options = self.options;
						self.clear();
					} )
					.hide()
					.insertAfter( options.element );

				/// Build clear button
				var button = $( document.createElement( 'a' ) )
					.attr( {
						'id': options.element.attr( 'id' ) + 'FindButton'
					} )
					.html( options.language.findText )
					.addClass( options.classes.findButton )
					.on( 'click', {'self': self}, function ( e ) {
						var self = e.data['self'];
						self.search();
					} )
					.insertAfter( options.element );

				/// Build country select
				$( document.createElement( 'select' ) )
					.attr( {
						'id': countryName
					} )
					.appendTo( buttonGroup );

				/// Build address select
				$( document.createElement( 'select' ) )
					.attr( {
						'id': options.element.attr( 'id' ) + 'AddressInput'
					} )
					.appendTo( buttonGroup );

				$( document.createElement( 'button' ) )
					.attr({
						'type': 'button',
						'id': options.element.attr( 'id' ) + 'ManualButton'
					})
					.html( options.language.manual )
					.addClass( options.classes.manualButton )
					.on('click', {'self': self}, function(e){
						var self = e.data['self'];
						self.doShow();
					})
					.insertAfter(buttonGroup);

				$( '#' + countryName ).on( 'advancedselectset', {'self': self}, function ( e, self ) {
					var selfie = e.data['self'];
					var value = $( this ).val();
					var found = false;
					if( $.isNumeric( value ) ){
						value = parseInt( value );
					}

					if( self.options.ajax.data['results'] !== undefined ) {
						$.each( self.options.ajax.data['results'], function ( k, v ) {
							var checkValue = v['value'];
							if( $.isNumeric( checkValue ) ){
								checkValue = parseInt( checkValue );
							}
							if ( checkValue == value ) {
								selfie.setFlag( v['code'] );
								found = true;
								return false;
							}
						} );
					}

					if( found == false && value !== null ) {
						$( this ).advancedSelect( 'set', selfie.options.defaultCountry );
					}else if($(options.fields['address1'] ).length > 0 ) {
						if( $(options.fields['address1'] ).val() != '' ) {
							selfie.doShow();
						}
					}
					if( value == 15 ){
						selfie.element.attr('placeholder', 'Postcode');
					}else{
						selfie.element.attr('placeholder', 'Comma separated address lines');
					}
					found = null;

					$( options.fields.state ).parents('[class^="col-"]').show();
					$( options.fields.county ).parents('[class^="col-"]').hide();

					try {
						$( options.fields.postcode ).rules( 'remove', 'required' );
						$( options.fields.postcode ).postcode( 'disable' );
						if ( value == 15 || value == null  ) {
							$( options.fields.postcode ).rules( 'add', {'required': true} );
							$( options.fields.postcode ).postcode( 'enable' );
						}
					}catch (err){

					}
					if ( value == 15 || value == null  ) {
						$( options.fields.state ).parents('[class^="col-"]').hide();
						$( options.fields.county ).parents('[class^="col-"]').show();
					}

				} );

				try {
					$( options.fields.postcode ).rules( 'add', {'required': true} );
					$( options.fields.postcode ).postcode( 'enable' );
				}catch (err){

				}
				$( options.fields.state ).parents('[class^="col-"]').hide();

				$( '#' + countryName ).on( 'advancedselectbuild', {'self': self}, function ( e, self ) {
					$( this ).off( 'advancedselectbuild');
					defer.resolve();
				});

				$( '#' + options.element.attr( 'id' ) + 'AddressInput' ).on( 'advancedselectset', {'self': self}, function ( e ) {
					var self = e.data['self'];
					var options = self.options;
					if( $(this ).val() != '' && $(this ).val() !== null ) {
						self.choose( $(this ).val() );
					}
				} );

				$( '#' + countryName ).advancedSelect( {
					ajax: {
						listUrl: options.ajax.countryUrl
					},
					autoBuild: true
				} );

				$( '#' + options.element.attr( 'id' ) + 'AddressInput' ).advancedSelect( {
					autoBuild: false
				} );
				$( '#' + countryName ).advancedSelect( 'hide' );
				$( '#' + options.element.attr( 'id' ) + 'AddressInput' ).advancedSelect( 'hide' );
				buttonGroup = null;

				self._trigger( 'buildInline' );
			});

			defer.notify();
			return promise;
		},

		doShow: function(){
			var self = this;
			var options = self.options;

			if( $(options.fields['block'] ).length > 0 ) {
				$( options.fields['block'] ).show();
			}

			$('#' + options.element.attr( 'id' ) + 'ManualButton' ).hide();

			$( '#' + options.element.attr( 'id' ) + 'ClearButton' ).show();
			$( '#' + options.element.attr( 'id' ) + 'FindButton' ).hide();
			options.element.hide();
		},

		choose: function(){
			var self = this;
			var options = self.options;

			self.doShow();

			if($(options.fields['address1'] ).length > 0 ) {
				var chosen = parseInt( $( '#' + options.element.attr( 'id' ) + 'AddressInput' ).val() );
				$.each(options.data['addresses'], function(k, v){
					if( $.isNumeric( v['value'] ) ) {
						v['value'] = parseInt( v['value'] );
					}
					if( v['value'] == chosen ) {
						$( options.fields['address1'] ).val( v['name'] );
					}
				});

			}
			if(options.data['address2'] !== null && $(options.fields['address2'] ).length > 0 ) {
				$( options.fields['address2'] ).val( options.data['address2'] );
			}
			if(options.data['address3'] !== null && $(options.fields['address3'] ).length > 0 ) {
				$( options.fields['address3'] ).val( options.data['address3'] );
			}
			if(options.data['town'] !== null && $(options.fields['town'] ).length > 0 ) {
				$( options.fields['town'] ).val( options.data['town'] );
			}
			if(options.data['state'] !== null && $(options.fields['state'] ).length > 0 ) {
				$( options.fields['state'] ).val( options.data['state'] );
			}
			if(options.data['county'] !== null && $(options.fields['county'] ).length > 0 ) {
				$( options.fields['county'] ).val( options.data['county'] );
			}
			if(options.data['postcode'] !== null && $(options.fields['postcode'] ).length > 0 ) {
				$( options.fields['postcode'] ).val( options.data['postcode'] );
			}

		},

		setFlag: function(code){
			var self = this;
			var options = self.options;

			$('#' + options.element.attr('id') + 'FlagIcon' ).removeAttr('class');
			$('#' + options.element.attr('id') + 'FlagIcon' ).addClass( "iti-flag " + code.toLowerCase() );

			self._trigger('setFlag');
		},

		destroy: function () {
			var self = this;
			var options = self.options;

			options.element.removeData('plugin-name');

			options.element.removeClass("addressLookup");

			options.element.html('');

			$.Widget.prototype.destroy.call(this);
		},

		setOption: function (key, value) {
			var self = this;
			var options = self.options;

			$.Widget.prototype._setOption.apply(this, arguments);
		},

		setOptions: function(newOptions){
			var self = this;
			var options = self.options;

			this.options = $.extend(true, options, newOptions);

			self._trigger('setOptions');
		},

		/// Make selection
		set: function (id) {
			var self = this;
			var options = self.options;

			self._trigger('set');
		},

		search: function () {
			var self = this;
			var options = self.options;

			var defer = $.Deferred();
			var promise = defer.promise();

			promise.progress(function() {

				/// Gather parameters
				var theData = {
					'country': $(options.fields.country).val(),
					'search': options.element.val()
				};

				$(document).ajaxExtend('setExecute', {
					"url": options.ajax.resultsUrl,
					"type": "POST",
					"data": theData,
					"key": options.ajax.key,
					"text": options.ajax.text,
					"success": function (data) {
						var newData = {
							"results": data['results']['addresses']
						};

						$('#' + options.element.attr('id') + 'AddressInput' ).find('option' ).remove();

						$('#' + options.element.attr('id') + 'AddressInput' ).advancedSelect('setOptions', {'ajax':{ 'data': newData } } );
						$(document ).ajaxExtend('buildOptions', data['results']['addresses'], $('#' + options.element.attr('id') + 'AddressInput' ));
						$('#' + options.element.attr('id') + 'AddressInput' ).advancedSelect('build');
						$('#' + options.element.attr('id') + 'AddressInput' ).advancedSelect('open');
						newData = null;

						options.data['addresses'] = data['results']['addresses'];
						options.data['address2'] = data['results']['address2'];
						options.data['address3'] = data['results']['address3'];
						options.data['town'] = data['results']['town'];
						options.data['county'] = data['results']['county'];
						options.data['state'] = data['results']['state'];
						options.data['postcode'] = data['results']['postcode'];

						defer.resolve();
						self._trigger('search');
					}
				});
				theData = null;
			});

			defer.notify();
			return promise;
		},

		clear: function () {
			var self = this;
			var options = self.options;

			options.element.val('');
			$('#' + options.element.attr('id') + 'ClearButton').hide();
			$('#' + options.element.attr('id') + 'FindButton').show();

			$('#' + options.element.attr('id') + 'ManualButton').show();

			if( $(options.fields['block'] ).length > 0 ) {
				$( options.fields['block'] ).hide();
			}

			options.element.show();

			if( $(options.fields['address1'] ).length > 0 ) {
				$( options.fields['address1'] ).val( '' );
			}
			if( $(options.fields['address2'] ).length > 0 ) {
				$( options.fields['address2'] ).val( '' );
			}
			if( $(options.fields['address3'] ).length > 0 ) {
				$( options.fields['address3'] ).val( '' );
			}
			if( $(options.fields['town'] ).length > 0 ) {
				$( options.fields['town'] ).val( '' );
			}
			if( $(options.fields['state'] ).length > 0 ) {
				$( options.fields['state'] ).val( '' );
			}
			if( $(options.fields['county'] ).length > 0 ) {
				$( options.fields['county'] ).val( '' );
			}
			if( $(options.fields['postcode'] ).length > 0 ) {
				$( options.fields['postcode'] ).val( '' );
			}

			self._trigger('clear');
		},

		disable: function () {
			var self = this;
			var options = self.options;

			$('#' + options.element.attr('id') + 'FindButton').hide();
			$('#' + options.element.attr('id') + 'ClearButton').hide();
			$('#' + options.element.attr('id') + 'ManualButton').hide();
			$('#' + options.element.attr('id') + 'Flag').attr('disabled', 'disabled');

			options.disabled = true;
			self._trigger('disable');
		},

		enable: function () {
			var self = this;
			var options = self.options;

			$('#' + options.element.attr('id') + 'FindButton').show();
			$('#' + options.element.attr('id') + 'ClearButton').show();
			$('#' + options.element.attr('id') + 'Flag').removeAttr('disabled');

			options.disabled = false;
			self._trigger('enable');
		},

		show: function(){
			var self = this;
			var options = this.options;
			options.element.parent().show();

			self._trigger('show');
		},

		hide: function(){
			var self = this;
			var options = this.options;
			options.element.parent().hide();

			self._trigger('hide');
		},

		focus: function() {
			var self = this;
			var options = this.options;
			options.element.focus();

			self._trigger('focus');
		}

	});

	/*var originalVal = $.fn.val;
	$.fn.val = function (value) {

		if (typeof value == 'undefined') {
			return originalVal.call(this);
		} else {

			var output = originalVal.call(this, value);

			if ($(this[0]).data('plugin-name') == 'addressLookup') {
				$(this[0]).addressLookup('set', value);
			}

			return output;
		}
	};*/
}(jQuery));
