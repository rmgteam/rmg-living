<?
require_once( "utils.php" );
require_once( $UTILS_CLASS_PATH."template/paperlessTemplate.php" );
require_once($UTILS_CLASS_PATH."resident.class.php");
require_once($UTILS_CLASS_PATH."unit.class.php");

unset( $_SESSION['unit'] );
unset( $_SESSION['lastName'] );
unset( $_SESSION['postcode'] );
unset( $_SESSION['refs'] );
unset( $_SESSION['optIn'] );
unset( $_SESSION['text'] );

if( isset( $_SESSION['resident_num'] ) ) {
	$resident = new resident($_SESSION['resident_num']);
	$unit = new unit();
	$unit->set_unit($_SESSION['resident_num']);

	$_SESSION['lastName'] = $resident->resident_name;
	$_SESSION['refs'] = array();
	$_SESSION['unit_refs'] = array();
	$_SESSION['refs'][] = $_SESSION['resident_num'];
	$_SESSION['postcode'] = $unit->unit_postcode;
	$_SESSION['unit'] = $unit->unit_address_1 . ', ' . $unit->unit_address_2;
	header('Location: paperless.php');
}else{
	$_SESSION['refs'] = array();
	$_SESSION['unit_refs'] = array();
	$_SESSION['unit'] = '';
}

$template = new \template\paperlessTemplate();
$template->setPage( 'index', 'Please Verify Your Details');
echo $template->fetch();

