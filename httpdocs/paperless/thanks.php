<?
require_once("utils.php");
require_once($UTILS_CLASS_PATH."template/paperlessTemplate.php");

$template = new \template\paperlessTemplate();
$template->set( 'blurb', $_SESSION['text']);
$template->setPage( 'thanks', 'Thank You' );
echo $template->fetch();

