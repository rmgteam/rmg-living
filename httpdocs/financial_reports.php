<?
require("utils.php");
require_once($UTILS_CLASS_PATH."rmc.class.php");
require_once($UTILS_CLASS_PATH."resident.class.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."security.class.php");
require_once($UTILS_CLASS_PATH."subsidiary.class.php");
$resident = new resident($_SESSION['resident_ref'],"ref");
$security = new security();
$website = new website;
$rmc = new rmc;


// Determine if allowed access into 'your community' section
//$website->allow_community_access();
$rmc->set_rmc($_SESSION['rmc_num']);
$subsidiary = new subsidiary($rmc->rmc['subsidiary_id']);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>RMG Living - Financial Reports</title>
<link href="/css/reset.css" rel="stylesheet" type="text/css" />
<link href="/css/common.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 8]> 
<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if lte IE 7]> 
<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
<![endif]-->
<script type="text/javascript" src="/library/jscript/jquery-1.4.2.min.js"></script>
<? require_once($UTILS_FILE_PATH."includes/analytics.php");?>
</head>
<body>
	
	<div id="wrapper">
	
		<? require_once($UTILS_FILE_PATH."includes/header.php");?>
		
		<div id="content">
		
			<table width="760" cellspacing="0" id="breadcrumb_table">
				<tr>
					<td nowrap="nowrap"><a href="/building_details.php">Your Community</a> &gt; Financial Reports</td>
					<td style="text-align:right;"><? if(!empty($_SESSION['resident_session'])){?><a href="index.php?logoff=Y" class="crumbs">Log Off</a><? }?></td>
				</tr>
			</table>

			<table width="760" cellspacing="0" style="clear:both; margin-top:13px;" id="content_box_1_top">
				<tr>
					<td width="15"><img src="/images/dblue_box_top_left_corner.jpg" width="15" height="33" /></td>
					<td width="730" style="border-top:1px solid #003366; background-color:#416CA0; vertical-align:middle"><img src="/images/financial_reports/financial_reports_symbol.jpg" width="22" height="22" style="vertical-align:middle;" />&nbsp;&nbsp;<span class="box_title">Financial Reports</span></td>
					<td width="15" style="text-align:right;"><img src="/images/dblue_box_top_right_corner.jpg" width="15" height="33" /></td>
				</tr>
			</table>
			
    			
			<div class="clearfix content_box_1" style="padding:0;width:758px;">
    							
				<div style="width:302px; padding:0 15px 15px 15px; float:left;">
					<h3 class="report_title">Financial Reports</h3>
    				<p>Below is a list of Financial Reports pertinent to your Management Company. All reports show 'live' data. Access to these reports is restricted to Directors (and selected Steering Committee members, if applicable) of <?=stripslashes($rmc->rmc['rmc_name'])?>.</p>
    				
					<h6>Aged Debtors Report</h6>
					<p>This report lists all residents that have an outstanding debt to your Management Company and ages the debt according to the length that it has remained outstanding.</p>
					<a href="/reports/aged_debtors_report.php" style="margin-top:6px; display:block;">View Report &gt;&gt;</a>
					
					<h6>Aged Creditors Report</h6>
					<p>This report lists invoices that are yet to be paid by your Management Company, along with how long the invoices have remained unpaid.</p>
					<a href="/reports/aged_creditors_report.php" style="margin-top:6px; display:block;">View Report &gt;&gt;</a>
					
					<h6>Transactional Status Report</h6>
					<p>This report is a summary of the outstanding debt of your Management Company by status.</p>
					<a href="/reports/transaction_status_report.php" style="margin-top:6px; display:block;">View Report &gt;&gt;</a>
					
					<h6>Key Performance Index (KPI) Report</h6>
					<p>This report summarizes the overall financial performance of your Management Company.</p>
					<a href="/reports/kpi_report.php" style="margin-top:6px; display:block;">View Report &gt;&gt;</a>
				</div>
				
				<div style="width:378px; float:right; height:480px; vertical-align:top; background-image:url(/images/financial_reports/chart.jpg); background-position:top; background-repeat:no-repeat;">
			    	&nbsp;
				</div>
				
			</div>

		</div>


	<? require_once($UTILS_FILE_PATH."includes/footer.php");?>

	</div>
	

</body>
</html>
