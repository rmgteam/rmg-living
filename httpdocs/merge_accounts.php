<?
require("utils.php");
require($UTILS_CLASS_PATH."website.class.php");
require($UTILS_CLASS_PATH."rmc.class.php");
require($UTILS_CLASS_PATH."encryption.class.php");
$crypt = new encryption_class;
$website = new website;
$rmc = new rmc;

// Determine if allowed access into 'your community' section
$website->allow_community_access();

// Set RMC
$rmc->set_rmc($_SESSION['rmc_num']);

if($_REQUEST['whichaction'] == "save_info"){

	if($_REQUEST['optout_marketing'] != "Y"){$_REQUEST['optout_marketing'] = "N";}
	
	$save_error = "N";
	
	$sql = "
	UPDATE cpm_residents_extra SET
	tel = '".$_REQUEST['tel']."',
	mobile = '".$_REQUEST['mobile']."',
	email = '".$_REQUEST['email']."',
	question_id_1 = ".$_REQUEST['security_question_1'].",
	question_id_2 = ".$_REQUEST['security_question_2'].",
	answer_1 = '".$_REQUEST['security_answer_1']."',
	answer_2 = '".$_REQUEST['security_answer_2']."',
	optout_marketing = '".$_REQUEST['optout_marketing']."',
	";
	if(preg_match("/127.0.0.1/", $_SERVER['SERVER_NAME']) !== 1){
	$sql .= "password = '".$crypt->encrypt($UTILS_DB_ENCODE, trim($_REQUEST['new_password']))."',";
	}
	$sql .= "
	allow_password_reset = 'N',
	password_to_be_sent = 'N',
	is_first_login = 'N',
	announce_optin = '".$_REQUEST['announce_optin']."',
	survey_optout = '".$_REQUEST['optout_surveys']."'
	WHERE resident_num = ".$_SESSION['resident_num'];
	@mysql_query($sql) or $save_error = "Y";
	
}


// Get value of 'is_first_login' from residents_extra table
$sql_resident = "SELECT * FROM cpm_residents_extra WHERE resident_num = ".$_SESSION['resident_num'];
$result_resident = @mysql_query($sql_resident);
$row_resident = @mysql_fetch_array($result_resident);

// Get 'password' from residents_extra table
$sql_password = "SELECT DECODE(password, '".$UTILS_DB_ENCODE."') FROM cpm_residents_extra WHERE resident_num = ".$_SESSION['resident_num'];
$result_password = @mysql_query($sql_password);
$row_password = @mysql_fetch_row($result_password);

// If first logon, re-direct to your community page
if($row_resident['is_first_login'] == "Y"){
	if($_REQUEST['whichaction'] == "save_info" && $save_error == "N"){
		print "<script language='javascript'>location.replace('".$UTILS_HTTPS_ADDRESS."building_details.php');</script>";
		exit;
	}
}

if($_REQUEST['whichaction'] != "save_info" || ($_REQUEST['whichaction'] == "save_info" && $save_error == "N")){
	$_REQUEST['security_question_1'] = $row_resident['question_id_1'];
	$_REQUEST['security_answer_1'] = $row_resident['answer_1'];
	$_REQUEST['security_question_2'] = $row_resident['question_id_2'];
	$_REQUEST['security_answer_2'] = $row_resident['answer_2'];
	$_REQUEST['tel'] = $row_resident['tel'];
	$_REQUEST['mobile'] = $row_resident['mobile'];
	$_REQUEST['email'] = $row_resident['email'];
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><!-- InstanceBegin template="/Templates/main.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- InstanceBeginEditable name="doctitle" -->
<title>RMG Living - Change Details</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<script language="javascript" src="library/jscript/functions/valid_email_check.js"></script>
<script language="JavaScript" type="text/JavaScript">
<!--
function save_info(){
	
	var bad="N";
	
	if(document.forms[0].new_password.value == ""){bad="Y";}
	if(document.forms[0].new_password_confirm.value == ""){bad="Y";}
	
	// Check password length is suitable
	if(document.forms[0].new_password.value.length < 8 || document.forms[0].new_password.value.length > 16){
		alert("The new password you specify must be between 8-16 characters long.");
		document.forms[0].new_password.value = "";
		document.forms[0].new_password_confirm.value = "";
		return false;
	}
	
	// Check that both passwords match
	if(document.forms[0].new_password.value != document.forms[0].new_password_confirm.value){
		alert("The new password you specified in the 'New Password' box does not match\nthe one you typed in the 'Confirm New Password' box.");
		document.forms[0].new_password.value = "";
		document.forms[0].new_password_confirm.value = "";
		return false;
	}
	
	// Check password contains one number
	if(document.forms[0].new_password.value.match(/[0-9]/) == null){
		alert("Your new password must contain at least one number.");
		return false;
	}
	
	// Make sure the two questions aren't the same
	if(document.forms[0].security_question_1.value == document.forms[0].security_question_2.value){
		alert("Please choose two different security questions.");
		return false;
	}
	
	// Check for question type and do appropriate action
	if(document.forms[0].security_question_1.value == "4"){
		if(!check_dob(document.forms[0].security_answer_1.value)){return false;}
	}
	else{
		if(document.forms[0].security_question_1.value == "-"){bad="Y";}
	}
	if(document.forms[0].security_question_2.value == "4"){
		if(!check_dob(document.forms[0].security_answer_2.value)){return false;}
	}
	else{
		if(document.forms[0].security_question_2.value == "-"){bad="Y";}
	}
	
	// Do remainder of fields checks
	if(document.forms[0].security_answer_1.value == ""){bad="Y";}
	if(document.forms[0].security_answer_2.value == ""){bad="Y";}
	if(document.forms[0].tel.value == ""){bad="Y";}
	if(document.forms[0].email.value == ""){bad="Y";}
	if(bad == "Y"){
		alert("Please fill in all fields marked with *");
		return false;
	}
	
	if(!valid_email_check(document.forms[0].email.value)){
		alert("Please enter a valid email address.");
		return false;
	}
	
	
	
	return true;
	
}

function check_dob(val){
	if(!val.match("../../....")){
		alert("Your date of birth needs to be in the format dd/mm/yyyy");
		return false;
	}
	return true;
}

//-->
<? require_once($UTILS_FILE_PATH."includes/analytics.php");?>
</script>
<!-- InstanceEndEditable -->
<link href="styles.css" rel="stylesheet" type="text/css">
<!--[if lte IE 8]> 
<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if lte IE 7]> 
<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
<![endif]-->
</head>
<body>
<? if($_SESSION['is_demo_account'] == "Y" && $_SESSION['is_developer'] != "Y"){?>
<form method="post" action="/building_details.php" style="margin:0; padding:0;">
<input type="hidden" name="a" value="cl">
<input type="hidden" name="s" value="gkfcbk45cgtf5kngn7kns5cg7snk5g7nsv5ng">
<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" style="margin:0; padding:0;">
	<tr>
		<td style="padding:4px;">Change Account:&nbsp;<input type="text" name="change_lessee" value="<?=$_SESSION['resident_ref']?>" size="20"> <input type="submit" name="submit_button" value="Change"></td>
	</tr>
</table>
</form>
<? }?>
<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#E9EEF4" style="border-bottom:1px solid #c1d1e1;"><div align="center"><img src="images/templates/header_bar_grad2.jpg" alt=""></div></td>
  </tr>
  <tr>
    <td style="border-top:1px solid #ffffff;border-bottom:1px solid #ffffff;">
	<table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="180"><a href="<? print $UTILS_HTTPS_ADDRESS;if( isset($_SESSION['login']) && $_SESSION['login'] != ""){print "/building_details.php";}else{print "/index.php";}?>"><img src="/images/templates/rmg_living_logo.jpg" style="margin-top:10px;" width="180" alt="RMG Living" border="0"></a></td>
        <td width="580" align="right">
		<?
		// User logged in is a 'developer', show any developer specific logos, or default to normal ones...
		if($_SESSION['is_developer'] == "Y"){
			
			$sql = "SELECT developer_id FROM cpm_residents_extra WHERE resident_num = ".$_SESSION['resident_num'];
			$result = @mysql_query($sql);
			$row = @mysql_fetch_row($result);
			if($row[0] != "" && file_exists($UTILS_FILE_PATH.'/images/developers/'.$row[0].'/header_'.$row[0].'.jpg') ){
				
				print '<img src="/images/developers/'.$row[0].'/header_'.$row[0].'.jpg" alt="RMG Living" width="580" height="86" BORDER=0>';
			}
			else{
				
				print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
			}
		}
		else{
		
			// Check to see if the Man. Co. has a developer association, if so, set developer specific images, or default to normal images...
			if( isset($_SESSION['login']) && $_SESSION['login'] != "" && $_SESSION['rmc_num'] != ""){
			
				$sql = "SELECT developer_id FROM cpm_rmcs_extra WHERE rmc_num = ".$_SESSION['rmc_num'];
				$result = @mysql_query($sql);
				$row = @mysql_fetch_row($result);
				if( $row[0] != "" && file_exists($UTILS_FILE_PATH.'/images/developers/'.$row[0].'/header_'.$row[0].'.jpg') ){
					
					print '<img src="/images/developers/'.$row[0].'/header_'.$row[0].'.jpg" alt="RMG Living" width="580" height="86" BORDER=0>';
				}
				else{
					
					print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
				}
			}
			else{
				
				print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
			}
		}
		?> 
		
		<?
		/*
		if( isset($_SESSION['login']) && $_SESSION['login'] != "" && $_SESSION['subsidiary_id'] != ""){
			$sql = "SELECT subsidiary_header_image FROM cpm_subsidiary WHERE subsidiary_id = ".$_SESSION['subsidiary_id'];
			$result = @mysql_query($sql);
			$row = @mysql_fetch_row($result);
			if($row[0] != "" ){ 
				?>
				<img src="/images/templates/header_image_w_logo.jpg" width="580" height="86" border="0" alt="RMG Living">
				<img src="/images/logos/<?=$row[0]?>" style="position:absolute; left:50%; margin-left:245px; margin-top:23px; z-index:10;" border="0" >
				<?
			}
			else{
				?>
				<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">
				<?
			}
		}
		else{
			?>
			<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">
			<?
		}
		*/
		?>
		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
  <td>
  <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
  <tr>
    <td height="10" align="center" valign="bottom" bgcolor="#E9EEF4" style="border-bottom:1px solid #c1d1e1;border-top:1px solid #c1d1e1;"><img src="images/templates/header_bar_grad2.jpg" alt=""></td>
  </tr>
  <tr>
  <td height="8" align="center" valign="top" style="border-top:1px solid #ffffff;"><img src="images/templates/header_bar_grad3.jpg" alt=""></td>
  </tr>
  </table>
  </td>
  </tr>
 
  <tr>
    <td height="4"></td>
  </tr>
  <tr>
    <td><table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="28" valign="top">
	
	<table width="760" cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td>
	<!-- InstanceBeginEditable name="breadcrumbs" --><a href="/building_details.php" class="crumbs">Your Community</a>&nbsp;>&nbsp;My Details&nbsp;>&nbsp;Merge Accounts<!-- InstanceEndEditable -->
	</td>
	<td style="text-align:right;" nowrap>
	<? if(!empty($_SESSION['resident_session'])){?><a href="index.php?logoff=Y" class="crumbs">Log Off</a><? }?>
	</td>
	</tr>
	</table>
	
	</td>
  </tr>
</table>
</td>
  </tr>
  <tr>
    <td><!-- InstanceBeginEditable name="body" -->
      <table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td style="border-bottom:1px solid #003366;"><table width="760" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="15"><img src="images/dblue_box_top_left_corner.jpg" width="15" height="33"></td>
              <td width="730" bgcolor="#416CA0" style="border-top:1px solid #003366; vertical-align:middle"><img src="images/your_details/your_details_symbol.jpg" width="22" height="22" style="vertical-align:middle;">&nbsp;&nbsp;<span class="box_title">Merge Accounts</span></td>
              <td width="15" align="right"><img src="images/dblue_box_top_right_corner.jpg" width="15" height="33"></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td valign="top"><table width="758" border="0" cellspacing="0" cellpadding="15" style="border-left:1px solid #7ea0bd;border-right:1px solid #7ea0bd;border-top:1px solid #7ea0bd;border-bottom:1px solid #cccccc;">
            <tr>
              <td valign="top" style="border-right:1px solid #eaeaea;">
			  
			  <form action="<?=$UTILS_HTTPS_ADDRESS?>change_details.php" method="post">
			  <table width="727" border="0" cellspacing="0" cellpadding="0">
               <? if($row_resident['is_first_login'] == "Y"){?>
			    <tr>
                  <td colspan="2">As this is the first time you have logged in under this account, please complete the form below. You will only have to fill in these details once for this account and you can ammend them at any point from the 'My Account' section.</td>
                  </tr>
				   <tr>
                  <td colspan="2">&nbsp;</td>
                </tr>
				  <? }?>
				  
				  <? if($_REQUEST['whichaction'] == "save_info" && $save_error == "Y"){?>
                <tr>
                  <td colspan="2" style="vertical-align:middle;"><img src="images/del_16.gif" style="vertical-align:middle;">&nbsp;&nbsp;<span class="msg_fail"><b>There was a problem saving your details.  Please contact <a href="mailto:customerservice@rmguk.com"><span class="msg_fail" style="text-decoration:underline;">customerservice@rmguk.com</span></a> .</b></span></td>
                </tr>
				
                <tr>
                  <td colspan="2">&nbsp;</td>
                  </tr>
				  <? }?>
				  <? if($_REQUEST['whichaction'] == "save_info" && $save_error == "N"){?>
                <tr>
                  <td colspan="2" style="vertical-align:middle;"><img src="images/tick_20.gif" width="20" height="20" style="vertical-align:middle;">&nbsp;&nbsp;<span class="msg_success"><b>Your details have been saved.</b></span></td>
                </tr>
                <tr>
                  <td colspan="2">&nbsp;</td>
                  </tr>
				  <? }?>
                <tr>
                  <td colspan="2"><span class="subt036"><b>Your Password</b></span></td>
                  </tr>
                <tr>
                  <td colspan="2" height="5"></td>
                </tr>
                <tr>
                  <td colspan="2"><? if($row_resident['is_first_login'] == "Y"){?>Please set yourself a new password of your choice. <? }?>Your new password must be between 8-16 characters in length and contain at least one number.</td>
                  </tr>
                <tr>
                  <td width="155">&nbsp;</td>
                  <td width="572">&nbsp;</td>
                </tr>
                <tr>
                  <td>New password * </td>
                  <td><input name="new_password" type="password" id="new_password" value="<?=$row_password[0]?>" <? if(preg_match("/127.0.0.1/", $_SERVER['SERVER_NAME']) === 1){print "disabled";}?>></td>
                </tr>
                <tr>
                  <td nowrap>Confirm new password&nbsp;*</td>
                  <td><input name="new_password_confirm" type="password" id="new_password_confirm" value="<?=$row_password[0]?>" <? if(preg_match("/127.0.0.1/", $_SERVER['SERVER_NAME']) === 1){print "disabled";}?>></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td><span class="subt036"><b>Security Questions</b></span></td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="2" height="5"></td>
                </tr>
                <tr>
                  <td colspan="2">Please answer  the security questions below. These may be used to verify you against our records if you call our Property Management department, or if you forget your password.</td>
                  </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>Question 1 * </td>
                  <td><select name="security_question_1" id="security_question_1">
				  <option value="-" selected>-</option>
				  <? 
				  $sql_sq = "SELECT * FROM cpm_security_questions";
				  $result_sq = @mysql_query($sql_sq);
				  while($row_sq = @mysql_fetch_array($result_sq)){?>
				  <option value="<?=$row_sq['question_id']?>" <? if($_REQUEST['security_question_1'] == $row_sq['question_id']){print "selected";}?>><?=$row_sq['question']?></option>
				  <? }?>
				  </select></td>
                </tr>
                <tr>
                  <td>Answer 1 * </td>
                  <td><input name="security_answer_1" type="text" id="security_answer_1" value="<?=$_REQUEST['security_answer_1']?>"></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>Question 2 * </td>
                  <td><select name="security_question_2" id="security_question_2">
                                            <option value="-" selected>-</option>
                                            <? 
										  $sql_sq = "SELECT * FROM cpm_security_questions";
										  $result_sq = @mysql_query($sql_sq);
										  while($row_sq = @mysql_fetch_array($result_sq)){?>
                                            <option value="<?=$row_sq['question_id']?>" <? if($_REQUEST['security_question_2'] == $row_sq['question_id']){print "selected";}?>>
                                            <?=$row_sq['question']?>
                                            </option>
                                            <? }?>
                                          </select></td>
                </tr>
                <tr>
                  <td>Answer 2 * </td>
                  <td><input name="security_answer_2" type="text" id="security_answer_2" value="<?=$_REQUEST['security_answer_2']?>"></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td><span class="subt036"><b>Contact Details</b></span></td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="2" height="5"></td>
                  </tr>
                <tr>
                  <td>Home telephone * </td>
                  <td><input name="tel" type="text" id="tel" value="<?=$_REQUEST['tel']?>"></td>
                </tr>
                <tr>
                  <td>Mobile telephone</td>
                  <td><input name="mobile" type="text" id="mobile" value="<?=$_REQUEST['mobile']?>"></td>
                </tr>
                <tr>
                  <td>Email address * </td>
                  <td><input name="email" type="text" id="email" size="40" value="<?=$_REQUEST['email']?>"></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td><span class="subt036"><strong>Announcements</strong></span></td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="2" height="5"></td>
                </tr>
                <tr>
                  <td colspan="2"><input type="checkbox" name="announce_optin" id="announce_optin" value="Y" <? if($row_resident['announce_optin'] == "Y"){?>checked<? }?>> 
                    Send me announcements regarding <?=$rmc->rmc['rmc_name']?>, for example, notification about on-site maintenance.</td>
                  </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="2">                    We may contact you from time to time to inform you about offers we think may be of interest to you as a home owner. If you do not wish to receive this information, please tick the box.
                    <input type="checkbox" name="optout_marketing" id="optout_marketing" value="Y" <? if($row_resident['optout_marketing'] == "Y"){?>checked<? }?>></td>
                  </tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">We may send you a survey from time to time to get your opinion on how we are doing at managing your property. If you do not wish to receive this information, please tick the box.
						<input type="checkbox" name="optout_surveys" id="optout_surveys" value="Y" <? if($row_resident['survey_optout'] == "Y"){?>checked<? }?> /></td>
				</tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td><a href="#" onClick="if(save_info()){document.forms[0].submit();}"><img name="save_button" id="save_button" src="images/your_details/save_button.jpg" width="50" height="20" border="0"></a></td>
                  <td>&nbsp;</td>
                </tr>
              </table>
			  <input type="hidden" name="whichaction" id="whichaction" value="save_info" alt="Click to confirm changes">
			</form>
			  </td>
              </tr>
          </table></td>
        </tr>
		<tr><td height="5" bgcolor="#416CA0" style="border:1px solid #003366;"><img src="images/spacer.gif" alt=""></td>
		</tr>
      </table>
    <!-- InstanceEndEditable --></td>
  </tr>
  <tr>
    <td height="25"></td>
  </tr>
  <tr>
    <td><table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="445" height="14"><a href="about_us.php" class="link416CA0">About RMG Living</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="contact_us.php" class="link416CA0">Contact Us</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="privacy.php" class="link416CA0">Legal Information</a></td>
        <td width="315" align="right">Copyright <?=date("Y", time())?> &copy; <a href="http://www.rmgltd.co.uk">Residential Management Group Ltd</a></td>
      </tr>
	  <tr><td colspan="2">&nbsp;</td></tr>
	  <tr><td colspan="2" style="text-align:left;"><p>&nbsp;</p>
	    </td>
	  </tr>
    </table></td>
  </tr> 
  <tr><td>&nbsp;</td></tr>
</table>
</body>
<!-- InstanceEnd --></html>
