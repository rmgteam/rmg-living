<?
#########################################################################################
# This script imports all director data exported from ECS into the RMG Living website
#########################################################################################

ini_set("max_execution_time", "1440000");


require_once("../utils.php");
require_once("../management/gen_password.php");
require("../library/classes/encryption.class.php");
require_once("../library/classes/security.class.php");
$crypt = new encryption_class;
$security = new security;

$starttime = time();
$now_ts = $starttime;
$subsidiary_counter=1;
$total_rows_imported = 0;
$ftp_folder_path = "C:\\Inetpub\\vhosts\\rmgliving.co.uk\\private\\ecs_export\\"; // Path to FTP folder
$this_ymd = date("Ymd");


$incorrect_address_counter = 0;
$incorrect_and_printed_counter = 0;


// Loop for each version of the ECS database
$sql_sub = "SELECT * FROM cpm_subsidiary WHERE subsidiary_ecs_import = 'Y' AND subsidiary_discon = 'N'";

$result_sub = mysql_query($sql_sub);
$num_subsidiaries = mysql_num_rows($result_sub);
while($row_sub = mysql_fetch_array($result_sub)){
	

$msg = "

IMPORTING ".$row_sub['subsidiary_name']."
================================
";
	
	//print $import_file_name."<br>";
	// Check that Export file exists
	$import_file_name = $ftp_folder_path.$row_sub['subsidiary_code']."_ecs_director.csv";
	$skip_lines = 1;
	if(file_exists($import_file_name)){
		$num_records = count(file($import_file_name));
		$num_records = $num_records - $skip_lines;
	}
	else{
		continue;
	}
	
	// Empty tables
	$merr = "N";
	mysql_query("SET AUTOCOMMIT=0") or $merr = "Y";
	mysql_query("START TRANSACTION") or $merr = "Y";
	mysql_query("SET transaction isolation level serializable") or $merr = "Y";
	mysql_query("DELETE FROM cpm_residents WHERE subsidiary_id = ".$row_sub['subsidiary_id']." AND is_resident_director = 'Y' AND resident_name <> 'RMG Test User' AND rmc_num <> 9999999 AND resident_is_developer_dummy <> 'Y'") or $merr = "Y";
	if($merr == "Y"){
		mysql_query("ROLLBACK");
	}
	else{
		mysql_query("COMMIT");
	}
	
	
	// Loop through each line of file and split data
	$import_row_counter = 0;
	$sub_row_counter = 0;
	mysql_query("SET AUTOCOMMIT=1");
	$handle = fopen($import_file_name, "r");
	while($import_fields = fgetcsv($handle, 1000, ",","\"")){
		
		// Skip file header rows if need be
		if($skip_lines > $import_row_counter){
			$import_row_counter++;
			continue;
		}
		
		print $import_row_counter."\n";

		#=====================================================================================
		# ASSIGN FIELDS TO VARIABLES
		#=====================================================================================
		$date_last_imported = date("H:i d/m/Y", $starttime);
		$resident_serial = $security->gen_serial(32);
		$rmc_num = trim(addslashes($import_fields[1]));
		$resident_num = trim(addslashes($import_fields[2]));
		$unit_num = trim(addslashes($import_fields[3]));
		$director_num = trim(addslashes($import_fields[4]));
		$director_name = trim(addslashes($import_fields[5]));
		$start_occupany_date = trim(addslashes($import_fields[6]));
		$end_occupany_date = trim(addslashes($import_fields[7]));
		
		// Determine if record should be active...
		$director_is_active = "0";
		if($start_occupany_date != ""){
			$start_ymd_parts = explode("/", $start_occupany_date);
			$start_ymd = $start_ymd_parts[2].$start_ymd_parts[1].$start_ymd_parts[0];
		}
		if($end_occupany_date != ""){
			$end_ymd_parts = explode("/", $end_occupany_date);
			$end_ymd = $end_ymd_parts[2].$end_ymd_parts[1].$end_ymd_parts[0];
		}
		if($start_ymd <= $this_ymd && ($end_occupany_date == "" || $end_ymd > $this_ymd)){
			$director_is_active = "1";
		}
		
		
		// Get RMC data (of lessee)
		$rmc_lookup_id = "";
		$sql_rmc_lookup = "
		SELECT rmc_lookup 
		FROM cpm_lookup_rmcs 
		WHERE 
		subsidiary_id = ".$row_sub['subsidiary_id']." AND 
		rmc_ref = '".$rmc_num."'";
		$result_rmc_lookup = @mysql_query($sql_rmc_lookup);
		$num_rmc_lookup = @mysql_num_rows($result_rmc_lookup);
		if($num_rmc_lookup > 0){
			
			$row_rmc_lookup = @mysql_fetch_row($result_rmc_lookup);
			$rmc_lookup_id = $row_rmc_lookup[0];
		}
		else{
			continue;	
		}

	
		// Get Lessee data
		$resident_lookup_id = "";
		$sql_resident_lookup = "
		SELECT resident_lookup 
		FROM cpm_lookup_residents 
		WHERE 
		subsidiary_id = ".$row_sub['subsidiary_id']." AND 
		resident_ref = '".$resident_num."'";
		$result_resident_lookup = @mysql_query($sql_resident_lookup);
		$num_resident_lookup = @mysql_num_rows($result_resident_lookup);
		if($num_resident_lookup > 0){
		
			$row_resident_lookup = @mysql_fetch_row($result_resident_lookup);
			$resident_lookup_id = $row_resident_lookup[0];
		}
		else{
			continue;
		}
		
		
		// Handle director assoc resident num
		$dir_assoc_res_num = $resident_lookup_id;
		if($resident_lookup_id == ""){
			$dir_assoc_res_num = "NULL";
		}
		
		
		// Get resident data (of lessee) to insert into director record
		$sql_resident = "
		SELECT * 
		FROM cpm_residents  
		WHERE 
		subsidiary_id = ".$row_sub['subsidiary_id']." AND 
		resident_num = ".$resident_lookup_id." 
		";
		$result_resident = @mysql_query($sql_resident);
		$row_resident = @mysql_fetch_array($result_resident);
		
		
		// Get unit data (of lessee) to insert into director record
		$sql_resident = "
		SELECT * 
		FROM cpm_units  
		WHERE 
		subsidiary_id = ".$row_sub['subsidiary_id']." AND 
		resident_num = ".$resident_lookup_id." 
		";
		$result_unit = @mysql_query($sql_unit);
		$row_unit = @mysql_fetch_array($result_unit);
		
		
		
		// Determine if active, if letter has been sent and if addresses are different
		if($director_is_active == "1"){
			
			if($row_resident['resident_address_1'] != $row_unit['unit_address_1']){
				
				$incorrect_address_counter++;
				
				$sql_pj = "SELECT count(*) FROM cpm_print_jobs WHERE resident_nums LIKE '%".$resident_lookup_id."%'";
				$result_pj = @mysql_query($sql_pj);
				$row_pj = @mysql_fetch_array($result_pj);
				
				if($row_pj[0] > 0){
					$incorrect_and_printed_counter++;
				}
			}
		}
		
	
	
		# Increment file row counter
		$import_row_counter++;
		$sub_row_counter++;
		
	}
	
	// Close import files
	fclose($handle);

	$subsidiary_counter++;
}


print "With different addresses: ".$incorrect_address_counter."<br>";
print "With different addresses + printed: ".$incorrect_and_printed_counter;


exit;
?>