<?
require("../utils.php");
require($UTILS_CLASS_PATH."rmc.class.php");
require($UTILS_CLASS_PATH."website.class.php");
require($UTILS_CLASS_PATH."xml.class.php");
require($UTILS_CLASS_PATH."security.class.php");
require($UTILS_CLASS_PATH."subsidiary.class.php");
$security = new security();
$website = new website;
$rmc = new rmc;
$xml = new xml;


$final_balance = 0;
$session_id = $security->gen_serial(16);
$ch = curl_init();

$user_agent = $_SERVER['HTTP_USER_AGENT'];
$host = "Host: hodd1svapp1";
$content_type = "Content-Type: text/xml; charset=utf-8";

$data = '<parameters>
<Reference1>103901</Reference1>
<Reference2>103901</Reference2>
<periods>quarterly</periods>
<suppresszero>No</suppresszero>
<noofperiods>4</noofperiods>
<excludeprevious>No</excludeprevious>
</parameters>';

$qube_process_request = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
<soap:Body>
<QubeProcess-3 xmlns="http://qube.qubeglobal.com/ns/webservice/">
<ClientSessionKey>'.$session_id.'</ClientSessionKey>
<QubeProcessName>rmgweb:ageddebt1</QubeProcessName>
<Data>'.$data.'</Data>
<UserName>rmgweb.servacc</UserName>
<Password>p4ssw0rd</Password>
<Group>RMG Live</Group>
<Application>QGS Property Management</Application>
</QubeProcess-3>
</soap:Body>
</soap:Envelope>';

$process_headers[] = $host;
$process_headers[] = $content_type;
$process_headers[] = "Content-Length : " . strlen ($qube_process_request);
$process_headers[] = "SOAPAction: \"http://qube.qubeglobal.com/ns/webservice/QubeProcess-3\"";

curl_setopt($ch, CURLOPT_URL, "https://81.106.219.24/qubews/qubews.asmx");
curl_setopt($ch, CURLOPT_HTTPHEADER, $process_headers);
curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
curl_setopt($ch, CURLOPT_HEADER, 0);                			// tells curl to include headers in response
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);        			// return into a variable
curl_setopt($ch, CURLOPT_TIMEOUT, 30);              			// times out after 30 secs
curl_setopt($ch, CURLOPT_PORT , 443);
//curl_setopt($ch, CURLOPT_SSLCERT, "C:/Inetpub/vhosts/rmgliving.co.uk/httpdocs/library/client.crt");
//curl_setopt($ch, CURLOPT_SSLKEY, "C:/Inetpub/vhosts/rmgliving.co.uk/httpdocs/library/keyout.pem");
//curl_setopt($ch, CURLOPT_CAINFO, "C:/Inetpub/vhosts/rmgliving.co.uk/httpdocs/library/ca-bundle.crt"); 
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_POSTFIELDS, $qube_process_request);  	// adding POST data
curl_setopt($ch, CURLOPT_POST, 1); 								// data sent as POST

$output = curl_exec($ch);
print str_replace("\r\n","",$output);

$info = curl_getinfo($ch);
print_r($info);

curl_close($ch);

?>