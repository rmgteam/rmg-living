<?php
// One-off script to update all resident ref records with qube db id
ini_set("max_execution_time", "1440000");
require_once("utils.php");

// Export files
$file_array = array();
$file_array[] = "_ecs_export";
$file_array[] = "_ecs_director";
$file_array[] = "_ecs_subtenant";

// Loop for each version of the ECS database
$sql_sub = "
SELECT * 
FROM cpm_subsidiary 
WHERE 
subsidiary_ecs_import = 'Y' AND 
subsidiary_discon = 'N'
";
$result_sub = @mysql_query($sql_sub);
$num_subsidiaries = @mysql_num_rows($result_sub);
if($num_subsidiaries > 0){
	
	while($row_sub = @mysql_fetch_array($result_sub)){
		
		
		print stripslashes($row_sub['subsidiary_name']).":\n=========================================\n";
		
		
		foreach($file_array as $file_suffix){
		
			// Loop through main export file
			$import_file_name = $UTILS_ECS_EXPORT_PATH.$row_sub['subsidiary_code'].$file_suffix.".csv";
			if(!file_exists($import_file_name)){
				
				print "Import File [$import_file_name] Does Not Exist\n";
				print "FILE (".$row_sub['subsidiary_code'].") DOES NOT EXIST\n";
			}else{
			
				$handle = fopen($import_file_name, "r");
				$import_counter = 0;
				$rows_missed = 0;
				$rows_to_skip = 1;
				while (($data = fgetcsv($handle, 1000, ",", '"')) !== FALSE) {
				
					if($import_counter < $rows_to_skip){
						$import_counter++;continue;
					}
				
					$import_counter++;
					print $row_sub['subsidiary_name'].": Resident Record ".$import_counter."\n";
					
					$subsidiary_id = $row_sub['subsidiary_id'];
					
					if( $file_suffix == "_ecs_export" ){
						$qube_record_id = trim($data[45]);
						$resident_ref = trim(addslashes($data[0]));
					}
					elseif( $file_suffix == "_ecs_director" ){
						$qube_record_id = trim($data[9]);
						$director_ref = trim(addslashes($data[4]));
						$resident_ref = $director_ref;
					}
					elseif( $file_suffix == "_ecs_subtenant" ){
						$qube_record_id = trim($data[8]);
						$subten_ref = trim(addslashes($data[4]));
						$resident_ref = $subten_ref;
					}
										
					
				
					$sql = "
					UPDATE cpm_lookup_residents SET 
						qube_record_id = ".$qube_record_id." 
					WHERE 
					subsidiary_id = ".$subsidiary_id." AND 
					resident_ref = '".$resident_ref."' 
					";
					@mysql_query($sql);	
					
					
				}
			}
		}
	}
}




?>