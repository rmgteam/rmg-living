<?
require("../utils.php");
require($UTILS_CLASS_PATH."rmc.class.php");
require($UTILS_CLASS_PATH."website.class.php");
require($UTILS_CLASS_PATH."xml.class.php");
require($UTILS_CLASS_PATH."security.class.php");
require($UTILS_CLASS_PATH."subsidiary.class.php");
$security = new security();
$website = new website;
$rmc = new rmc;
$xml = new xml;


$final_balance = 0;
$session_id = $security->gen_serial(16);
$ch = curl_init();

$user_agent = $_SERVER['HTTP_USER_AGENT'];
$host = "Host: hodd1svapp1";
$content_type = "Content-Type: text/xml; charset=utf-8";


// PARAMETERS:
// method	- 'reference' (return single supplier data using ref.)
//			- 'analysis' (return based on analysis code)
//			- 'name'
// 			- 'all' (return all suppliers. default)
// filter	- choose from invoice, payment, order, approved, insured (can be concatenated using spaces)
// key		- needed if used with filter in method parameter

$data = '<parameters>
<method>analysis</method>
<filter>invoice</filter>
<key>INS</key>
</parameters>';

					
$qube_process_request = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
<soap:Body>
<QubeProcess-3 xmlns="http://qube.qubeglobal.com/ns/webservice/">
<ClientSessionKey>'.$session_id.'</ClientSessionKey>
<QubeProcessName>rmgweb:getsupp</QubeProcessName>
<Data>'.$data.'</Data>
<UserName>rmgweb.servacc</UserName>
<Password>p4ssw0rd</Password>
<Group>RMG Live</Group>
<Application>QGS Purchase Ledger</Application>
</QubeProcess-3>
</soap:Body>
</soap:Envelope>';

$process_headers[] = $host;
$process_headers[] = $content_type;
$process_headers[] = "Content-Length : " . strlen ($qube_process_request);
$process_headers[] = "SOAPAction: \"http://qube.qubeglobal.com/ns/webservice/QubeProcess-3\"";

curl_setopt($ch, CURLOPT_URL, "http://81.106.219.24/qubews/qubews.asmx");
curl_setopt($ch, CURLOPT_HTTPHEADER, $process_headers);
curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
curl_setopt($ch, CURLOPT_HEADER, 0);                			// tells curl to include headers in response
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);        			// return into a variable
curl_setopt($ch, CURLOPT_TIMEOUT, 30);              			// times out after 30 secs
curl_setopt($ch, CURLOPT_POSTFIELDS, $qube_process_request);  	// adding POST data
curl_setopt($ch, CURLOPT_POST, 1); 								// data sent as POST

$output = curl_exec($ch);
$info = curl_getinfo($ch);

print $output;
exit;

if($output === false || $info['http_code'] != 200){

	$output = "No cURL data returned - [". $info['http_code']. "]";
	if(curl_error($ch)){
		$output .= "\n". curl_error($ch);
	}
	?>
	<tr>
		<td colspan="6">This service is currently unavailable. Please try again later. (1)</td>
	</tr>
	<?
}
else {
						
	//Put XML result into array
	$xml_result = $xml->xml2array($output);
	$return_code = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['suppliervalues']['code'];
	
	if($return_code != "00"){
		$error_responses = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['returnvalue']['details'];
		$err_str = "The following errors occurred:<br />";
		for($r=0;$r < count($error_responses);$r++){
			$err_str .= $error_responses[$r]['reference'].": ".$error_responses[$r]['comment']."<br />";
		}
		?>
		<tr>
			<td colspan="6" style="border-right:1px solid #ccc;border-left:1px solid #ccc;">This service is currently unavailable. Please try again later. (2) <?=$err_str?></td>
			</tr>
		<?
	}
	else{
	
		$num_trans = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['suppliervalues']['lines'];
		if($num_trans == 0 || $num_trans == ""){
			?>
			<tr>
				<td colspan="6" style="border-right:1px solid #ccc;border-left:1px solid #ccc;">&nbsp;There are no transactions available.</td>
			</tr>
			<?
		}
		else{
			
			if($num_trans == "1"){ // For some reason, the xml2array process does not put only one transaction into an array, so we do this manually
				unset($transact_array);
				$transact_array[0] = array();
				$transact_array[0] = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['suppliervalues']['transact'];
			}
			else{
				$transact_array = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['suppliervalues']['transact'];
			}
			
			for($t=0;$t < count($transact_array);$t++){
			
				print_r($transact_array[$t]);
			}
		}
	}				
}

curl_close($ch);

?>