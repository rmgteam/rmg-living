<?php

ini_set("max_execution_time", "1440000");
require("../utils.php");

GLOBAL $UTILS_SERVER_PATH;

$start_row = 1;
$i = 0;

$new_import_file_name = $UTILS_SERVER_PATH."temp/pfp_master.csv";
print($new_import_file_name."<br />");

if (($handle = fopen($new_import_file_name, "r")) !== FALSE) {
	while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		$tenantRef = $data[0];
		$masterUser = $data[1];

		$sql = "SELECT resident_lookup
		FROM cpm_lookup_residents
		WHERE resident_ref = '" . $tenantRef . "'";
		$result = mysql_query($sql);
		if( mysql_num_rows( $result ) > 0 ){
			while( $row = mysql_fetch_array( $result ) ) {
				$residentNum = $row['resident_lookup'];
			}
		}

		$sql = "SELECT master_account_id
		FROM cpm_master_accounts
		WHERE master_account_username = '" . $masterUser . "'";
		$result = mysql_query($sql);
		if( mysql_num_rows( $result ) > 0 ){
			while( $row = mysql_fetch_array( $result ) ) {
				$masterId = $row['master_account_id'];
			}
		}

		$sql = "SELECT master_account_id
		FROM cpm_master_account_resident_assoc
		WHERE master_account_id = '" . $masterId . "'
		AND resident_num = '" . $residentNum . "'";
		$result = mysql_query($sql);
		if( mysql_num_rows( $result ) == 0 ){
			$has_error = false;

			$sql = "INSERT INTO cpm_master_account_resident_assoc SET
			master_account_id = '" . $masterId . "',
			resident_num = '" . $residentNum . "',
			master_account_resident_assoc_announce_optin = 'Y',
			master_account_resident_assoc_default = 'N'";
			mysql_query($sql) or $has_error = true;

			if($has_error){
				print($tenantRef . ' failed to be added to ' . $masterUser);
			}else{
				print($tenantRef . ' has been added to ' . $masterUser);
			}
		}else{
			print($tenantRef . ' has already been added to ' . $masterUser);
		}
	}
}

?>