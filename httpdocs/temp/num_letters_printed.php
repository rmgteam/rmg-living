<?
ini_set("max_execution_time", "1440000");
require("../utils.php");

// Get no. of letters printed to date

$sql = "
SELECT *    
FROM cpm_print_jobs  
WHERE 
resident_nums <> '' AND resident_nums IS NOT NULL 
ORDER BY print_job_time DESC  
";
$result = mysql_query($sql);
$num_rows = mysql_num_rows($result);
$counter = 1;
$total = 0;
$active_residents_sent = 0;
$unique_residents = 0;
$active_resident_array = array();
while($row = mysql_fetch_array($result)){
	
	print $counter." of ".$num_rows." print jobs processed<\n";
	
	$old_resident_nums = explode(",", $row['resident_nums']);
	$total += count($old_resident_nums);
	
	// Check if account is active
	for($r=0;$r<count($old_resident_nums);$r++){
		
		if( !in_array($old_resident_nums[$r], $active_resident_array) ){		
		
			$unique_residents++;
		
			$sql2 = "
			SELECT count(*) 
			FROM cpm_residents res, cpm_residents_extra rex, cpm_rmcs rmc 
			WHERE 
			res.resident_num = rex.resident_num AND 
			res.rmc_num = rmc.rmc_num AND 
			res.resident_is_active = 1 AND 
			rmc.rmc_is_active = 1 AND 
			res.resident_num = ".$old_resident_nums[$r]."
			";
			$result2 = @mysql_query($sql2);
			$row2 = @mysql_fetch_row($result2);
			
			$active_residents_sent += $row2[0];
			$active_resident_array[] = $old_resident_nums[$r];
		
		}
	}
	
	
	$counter++;
}


print "Total letters sent: ".$total."\n";
print "Total letters sent to 'active' residents: ".$active_residents_sent;
?>