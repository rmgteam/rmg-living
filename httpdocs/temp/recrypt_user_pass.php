<?
// This script was used to re-encrypt passwords containing semi-colons (once encrypted) as they cannot be translated properly by MySQL
ini_set("max_execution_time", "1440000");
require("../utils.php");
require("../management/gen_password.php");
require("../library/classes/encryption.class.php");
require("../library/classes/new_encryption.class.php");
$crypt = new encryption_class;
$new_crypt = new new_encryption_class;

$counter = 1;
$blanks = 0;
/*
$sql = "
SELECT rex.id, rex.password, rex.resident_num, rex.email, lres.resident_ref, res.resident_name, res.resident_address_2, res.resident_address_3, res.resident_address_postcode  
FROM cpm_residents_extra rex, cpm_lookup_residents lres, cpm_residents res 
WHERE 
res.resident_num = rex.resident_num AND 
lres.resident_lookup = rex.resident_num AND 
res.resident_is_active = '1'
";
*/
$sql = "
SELECT user_id, password 
FROM cpm_backend 

";
$result = @mysql_query($sql);

print "<table border=1><tr><td>Resident Name</td><td>Resident Address</td><td>Resident Ref</td><td>Orig' Encrypt' Pass'</td><td>Orig' Decrypt' Pass'</td><td>New Encrypt' Pass'</td><td>Test</td><td>Email</td></tr>";

while($row = @mysql_fetch_row($result)){
	
	if($row[1] == ""){
		$blanks++;	
	}
	
	// Decrypt original password
	$orig_password = $crypt->decrypt($UTILS_DB_ENCODE, $row[1]);
	
	// Re-encrypt password
	$re_password = $new_crypt->encrypt($UTILS_DB_ENCODE, $orig_password);
	
	// Test password encryption
	$test_password = $new_crypt->decrypt($UTILS_DB_ENCODE, $re_password);
	
	if($orig_password != $test_password){	
		
		//continue;
	}
	
	print "<tr><td>".$row[5]."</td><td>".$row[6].", ".$row[7].", ".$row[8]."</td><td>".$row[4]."</td><td>".$row[1]."</td><td>".$orig_password."</td><td>".$re_password."</td><td>".$test_password."</td><td>".$row[3]."</td></tr>";
	
	
	$sql2 = "
	UPDATE cpm_backend SET
		password = '".$re_password."'
	WHERE user_id = ".$row[0]."
	";
	//mysql_query($sql2);
	
	$counter++;
}


print "</table><br><br><b>Blanks:".$blanks."</b><br>";
print "<b>Total:".$counter."</b>";

?>