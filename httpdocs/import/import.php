<?
#####################################################################################
# This script imports all data exported from Voyager into the RMG Living website
#####################################################################################

ini_set("max_execution_time", "1440000");

// Mail report function
function mail_report($report){

	$report = $GLOBALS['report'];

	mail("itservicedesk@rmguk.com", "RMG Living Voyager Import Report", $report);
	print $report;
	exit;
}

function add_to_report($err_code, $msg){
	if($err_code != "1062"){
		$report .= $msg;
	}
}

require_once("../utils.php");
require_once("../management/gen_password.php");
require("../library/classes/encryption.class.php");
$crypt = new encryption_class;
$site_locality = "remote";
$starttime = time();
$import_file_name = "C:\\voyager_export\\CPM_Export.csv";
$import_backup_file_name = "C:\\voyager_export\\CPM_Export_Backup_".date("M_jS_Y", $starttime).".csv";
$log_file_name = "C:\\voyager_export\\CPM_Export.log";

# Check that Export file exists
if(!file_exists($import_file_name)){
	$report .= "Import File Does Not Exist\r\n";
	mail_report($report);
}

# Do export integrity check using contents of log file
if(!file_exists($log_file_name)){
	$report .= "Log File Does Not Exist\r\n";
}

# Get Log File contents
$log_contents = file($log_file_name);
$log_file_date = trim(substr($log_contents[0], 0, 10));
$log_file_date_parts = explode("/", $log_file_date);
$log_file_date_ts = mktime(12,30,30,$log_file_date_parts[0],$log_file_date_parts[1],$log_file_date_parts[2]);
$log_file_date_ts = $log_file_date_ts;
$log_file_date = date("d/m/Y", $log_file_date_ts);
$log_file_size = trim(substr($log_contents[0], 23, 38));
$file_stats = stat($import_file_name);
$file_size = $file_stats[7];
$file_date = date("d/m/Y", $file_stats[9]);
if($log_file_date != $file_date){
	$report .= "The Import file date ($file_date) does not match that in the Log file ($log_file_date).<br>\r\n";
}
if($log_file_size != $file_size){
	$report .= "The Import file size ($file_size) does not match that in the Log file ($log_file_size).<br>\r\n";
}
if($file_size == 0){
	$report .= "The Import file (size - $file_size) appears to be empty.<br>\r\n";
	mail_report($report);
}


// Access data form export file
$handle = fopen($import_file_name, "r");


# Empty tables, but keep DEMO accounts
$merr = "N";
mysql_query("SET AUTOCOMMIT=0") or $merr = "Y";
mysql_query("START TRANSACTION") or $merr = "Y";
mysql_query("SET transaction isolation level serializable") or $merr = "Y";
mysql_query("DELETE FROM cpm_residents WHERE rmc_num <> 9999999") or $merr = "Y";
mysql_query("DELETE FROM cpm_residents WHERE resident_num LIKE 't0%'") or $merr = "Y";
mysql_query("DELETE FROM cpm_residents_extra WHERE resident_num LIKE 't0%'") or $merr = "Y";
mysql_query("DELETE FROM cpm_rmcs WHERE rmc_num <> 9999999") or $merr = "Y";
mysql_query("DELETE FROM cpm_units WHERE rmc_num <> 9999999") or $merr = "Y";
if($merr == "Y"){
	mysql_query("ROLLBACK");
	mail_report("Could not empty tables. Script terminated here.");
}
else{
	mysql_query("COMMIT") or mail_report("Could not commit transaction. Script terminated here.");
}


# Loop through each line of file and split data
$import_row_counter = 1;
$skip_lines = 0;
mysql_query("SET AUTOCOMMIT=1");
while($import_fields = fgetcsv($handle, 1000, ",","\"")){

	echo $import_row_counter."\n";
	
	# Skip file header rows if need be
	if($skip_lines >= $import_row_counter){
		$import_row_counter++;
		continue;
	}
	
	
	#=====================================================================================
	# ASSIGN FIELDS TO VARIABLES
	#=====================================================================================
	
	$date_last_imported = date("H:i d/m/Y", $starttime);
	
	$resident_num = trim($import_fields[0]);
	$resident_name = trim(addslashes($import_fields[1]));
	$resident_address_1 = trim(addslashes($import_fields[2]));
	$resident_address_2 = trim(addslashes($import_fields[3]));
	$resident_address_county = trim(addslashes($import_fields[4]));
	$resident_address_city = trim(addslashes($import_fields[5]));
	$resident_address_postcode = trim($import_fields[6]);
	$resident_address_country = trim($import_fields[7]);
	$resident_status = trim($import_fields[8]);
	$rmc_num = trim($import_fields[9]);
	$rmc_name = trim(addslashes($import_fields[10]));
	$is_resident_director = trim($import_fields[11]);
	$property_manager = trim(addslashes($import_fields[12]));
	$unit_num = trim($import_fields[13]);
	$unit_address_1 = trim(addslashes($import_fields[14]));
	$unit_address_2 = trim(addslashes($import_fields[15]));
	$unit_address_3 = trim(addslashes($import_fields[16]));
	$unit_city = trim(addslashes($import_fields[17]));
	$unit_county = trim(addslashes($import_fields[18]));
	$unit_postcode = trim($import_fields[19]);
	$unit_country = trim($import_fields[20]);
	$rmc_status = trim($import_fields[21]);
	$rmc_address_1 = trim(addslashes($import_fields[22]));
	$rmc_address_2 = trim(addslashes($import_fields[23]));
	$rmc_address_county = trim(addslashes($import_fields[24]));
	$rmc_address_city = trim(addslashes($import_fields[25]));
	$rmc_address_postcode = trim(addslashes($import_fields[26]));
	$rmc_address_country = trim(addslashes($import_fields[27]));
	$weblinks_username = trim($import_fields[28]);
	$weblinks_password = trim($import_fields[29]);
	$rmc_service_level = addslashes(trim($import_fields[31]));
	$rmc_managed_office = addslashes(trim($import_fields[32]));
	
	# Skip the procedure if RMC status is inactive
	if($rmc_status != "Active" || $resident_status != "Current"){continue;}
	
	#=====================================================================================
	# IMPORT PROPERTY
	#=====================================================================================
	$rmc_inserted = "Y";
	$sql_ad = "
	INSERT INTO cpm_rmcs(
		rmc_name,
		rmc_status,
		rmc_address_1,
		rmc_address_2,
		rmc_address_city,
		rmc_address_county,
		rmc_address_postcode,
		rmc_address_country,
		property_manager,
		managed_office,
		rmc_service_level,
		rmc_num,
		date_last_imported
	)
	VALUES(
		'".$rmc_name."',
		'".$rmc_status."',
		'".$rmc_address_1."',
		'".$rmc_address_2."',
		'".$rmc_address_city."',
		'".$rmc_address_county."',
		'".$rmc_address_postcode."',
		'".$rmc_address_country."',
		'".$property_manager."',
		'".$rmc_managed_office."',
		'".$rmc_service_level."',
		'".$rmc_num."',
		'".$date_last_imported."'
	)
	";
	mysql_query($sql_ad) or add_to_report(mysql_errno(), "Insert Property (cpm_rmcs) ".$rmc_num." Failed<br>\r\n");
	

		
	# Insert into cpm_rmcs_extra
	$sql_ad = "
	INSERT INTO cpm_rmcs_extra(
		rmc_num,
		member_id
	)
	VALUES(
		'".$rmc_num."',
		1
	)
	";
	mysql_query($sql_ad) or add_to_report(mysql_errno(), "Insert Property (cpm_rmcs_extra) ".$rmc_num." Failed<br>\r\n");


	// IMPORT UNIT
	$sql_unit = "
	INSERT INTO cpm_units(
		unit_num,
		resident_num,
		rmc_num,
		unit_address_1,
		unit_address_2,
		unit_address_3,
		unit_city,
		unit_county,
		unit_postcode,
		unit_country,
		date_last_imported
	)
	VALUES(
		'".$unit_num."',
		'".$resident_num."',
		".$rmc_num.",
		'".$unit_address_1."',
		'".$unit_address_2."',
		'".$unit_address_3."',
		'".$unit_city."',
		'".$unit_county."',
		'".$unit_postcode."',
		'".$unit_country."',
		'".$date_last_imported."'
	)
	";
	mysql_query($sql_unit) or add_to_report(mysql_errno(), "Insert Unit (cpm_units) ".$resident_num." Failed<BR>\r\n");

	
	// IMPORT RESIDENT
	$sql_adr = "
	INSERT INTO cpm_residents(
		resident_num,
		rmc_num,
		resident_name,
		resident_address_1,
		resident_address_2,
		resident_address_city,
		resident_address_county,
		resident_address_postcode,
		resident_address_country,
		is_resident_director,
		weblinks_username,
		weblinks_password,
		date_last_imported,
		resident_status
	)
	VALUES(
		'".$resident_num."',
		'".$rmc_num."',
		'".$resident_name."',
		'".$resident_address_1."',
		'".$resident_address_2."',
		'".$resident_address_city."',
		'".$resident_address_county."',
		'".$resident_address_postcode."',
		'".$resident_address_country."',
		'".$is_resident_director."',
		'".$weblinks_username."',
		'".$weblinks_password."',
		'".$date_last_imported."',
		'".$resident_status."'
	)
	";
	mysql_query($sql_adr) or add_to_report(mysql_errno(), "Insert Resident (cpm_residents) ".$resident_num." Failed<BR>\r\n");

	
	// Generate unique password for this user
	$password = get_rand_id(8, "ALPHA", "UPPER");
		
	
	// Insert extra resident data
	$dir_let_to_b_sent = "N";
	if($is_resident_director == "Y"){$dir_let_to_b_sent = "Y";}
	
	$sql_adr = "
	INSERT INTO cpm_residents_extra(
		resident_num,
		rmc_num,
		password,
		password_to_be_sent,
		director_letter_to_be_sent,
		date_created
	)
	VALUES(
		'".$resident_num."',
		'".$rmc_num."',
		'".addslashes($crypt->encrypt($UTILS_DB_ENCODE, $password))."',
		'Y',
		'$dir_let_to_b_sent',
		'".time()."'
	)
	";
	mysql_query($sql_adr) or add_to_report(mysql_errno(), "Insert Resident (cpm_residents_extra) ".$resident_num." Failed<br>\r\n");
	

	# Increment file row counter
	$import_row_counter++;
	
}


// Get time
$thistime = time();

// Close import files
fclose($handle);


// Send import status report by email
$endtime = time();
$script_duration = ($endtime - $starttime);
$report .= "SCRIPT DURATION $script_duration sec\r\n";
$report .= $import_row_counter." row imported\r\n";
mail_report($report);

# Make copy of export file, in case failure occurs
if(copy($import_file_name, $import_backup_file_name)){
	unlink($import_file_name);
}

exit;
?>