<?
//===================================================================================================
// This script creates all the dummy logins for PM teams to be able to access individual RMC sites.
// This script must run at 4pm on Saturday (after the ECS Import script so that all new Man. Co. 
// records are picked up and after the previous batch of test logins have been removed - see that 
// script for further details.)
//===================================================================================================

ini_set("max_execution_time", "1440000");
require_once("utils.php");

require_once($UTILS_FILE_PATH."management/gen_password.php");
require_once($UTILS_FILE_PATH."library/classes/encryption.class.php");
require_once($UTILS_FILE_PATH."library/classes/security.class.php");
$crypt = new encryption_class;
$security = new security;
$starttime = time();
$date_last_imported = date("H:i d/m/Y", $starttime);


$test_mode = "N";


// Generates unique Resident reference
function gen_unique_resident_ref(){
  
	$do_loop = true;
	$loop_counter = 0;
	$dummy_resident_ref = "";
	while($do_loop === true){
		
		$tnum1 = get_rand_id(16, "NUMERIC", "");
		$dummy_resident_ref = "t0".$tnum1;
		$sql_un = "SELECT count(*) FROM cpm_lookup_residents WHERE resident_ref = '".$dummy_resident_ref."'";
		$result_un = mysql_query($sql_un);
		$row_un = mysql_fetch_row($result_un);
		if($row_un[0] == 0){
			$do_loop = false;
			break;
		}
		$loop_counter++;
		if($loop_counter > 1000){
			$do_loop = false;
		}
	}
	
	return $dummy_resident_ref;
}



// For all Man. Co's...
$report = "Script started at ".date("H:i:s jS F Y").".

";
$rmc_counter = 1;
$sql_tl = "SELECT * FROM cpm_rmcs ORDER BY rmc_num ASC";
$result_tl = mysql_query($sql_tl);
while($row_tl = mysql_fetch_array($result_tl)){

	
	//==================================================================================
	// Standard Lessee Login
	//==================================================================================	

	
	// Get unique Resident reference...
	$dummy_resident_ref = gen_unique_resident_ref();
	$resident_serial = $security->gen_serial(32);
	
	
	// Insert resident lookup record...
	$sql_lres = "
	INSERT INTO cpm_lookup_residents (
		subsidiary_id,
		qube_record_id,
		resident_ref,
		related_to_test_account,
		date_created
	)VALUES(
		".$row_tl['subsidiary_id'].",
		".$rmc_counter.",
		'".$dummy_resident_ref."',
		'Y',
		'".date("H:i d/m/Y")."'
	) 
	";
	mysql_query($sql_lres);
	if($test_mode == "Y"){
		print $sql_lres."<br>";
	}
	
	
	// Get resident lookup id
	$sql_greslook = "SELECT LAST_INSERT_ID() FROM cpm_lookup_residents";
	$result_greslook = mysql_query($sql_greslook);
	$row_greslook = mysql_fetch_row($result_greslook);
	$resident_lookup_id = $row_greslook[0];


	// Insert resident record...
	$sql2 = "
	INSERT INTO cpm_residents (
		resident_serial,
		rmc_num,
		subsidiary_id,
		resident_num,
		resident_name,
		is_resident_director,
		date_last_imported,
		resident_status,
		resident_is_active,
		related_to_test_account
	)
	VALUES(
		'".$resident_serial."',
		".$row_tl['rmc_num'].",
		".$row_tl['subsidiary_id'].",
		".$resident_lookup_id.",
		'RMG Test User',
		'N',
		'".$date_last_imported."',
		'Current',
		'1',
		'Y'
	)
	";
	mysql_query($sql2);
	if($test_mode == "Y"){
		print $sql2."<br>";
	}
	
	
	// Insert resident extra record...
	$sql3 = "
	INSERT INTO cpm_residents_extra (
		resident_num,
		rmc_num,
		question_id_1,
		question_id_2,
		answer_1,
		answer_2,
		email,
		password,
		is_first_login,
		password_to_be_sent,
		announce_optin,
		optout_marketing,
		related_to_test_account
	)
	VALUES(
		".$resident_lookup_id.",
		".$row_tl['rmc_num'].",
		4,
		2,
		'01/01/1980',
		'Somewhere',
		'webmaster@rmgliving.co.uk',
		'".$crypt->encrypt($UTILS_DB_ENCODE, get_rand_id(16, "ALPHA", "UPPER") )."',
		'N',
		'N',
		'N',
		'Y',
		'Y'
	)
	";
	mysql_query($sql3);
	if($test_mode == "Y"){
		print $sql3."<br>";
	}
	
	
	// Insert unit record...
	$sql3a = "
	INSERT INTO cpm_units (
		subsidiary_id,
		unit_num,
		rmc_num,
		resident_num,
		unit_address_1,
		unit_address_2,
		unit_address_3,
		unit_city,
		unit_county,
		unit_postcode,
		unit_country,
		date_last_imported,
		related_to_test_account
	)
	VALUES(
		".$row_tl['subsidiary_id'].",
		'1001',
		".$row_tl['rmc_num'].",
		".$resident_lookup_id.",
		'Test Address 1',
		'Test Address 2',
		'Test Address 3',
		'Test City',
		'Test County',
		'Test Postcode',
		'UK',
		'".$date_last_imported."',
		'Y'
	)
	";
	mysql_query($sql3a);
	if($test_mode == "Y"){
		print $sql3a."<br>";
	}
	
	
	
	
	//==================================================================================
	// Sub-tenant Login
	//==================================================================================	

	
	// Get unique Resident reference...
	$dummy_resident_ref = gen_unique_resident_ref();
	$resident_serial = $security->gen_serial(32);

	
	// Insert resident lookup record...
	$sql_lres = "
	INSERT INTO cpm_lookup_residents (
		subsidiary_id,
		qube_record_id,
		resident_ref,
		date_created,
		related_to_test_account
	)VALUES(
		".$row_tl['subsidiary_id'].",
		".$rmc_counter.",
		'".$dummy_resident_ref."',
		'".date("H:i d/m/Y")."',
		'Y'
	) 
	";
	mysql_query($sql_lres);
	if($test_mode == "Y"){
		print $sql_lres."<br>";
	}
	
	
	// Get resident lookup id
	$sql_greslook = "SELECT LAST_INSERT_ID() FROM cpm_lookup_residents";
	$result_greslook = mysql_query($sql_greslook);
	$row_greslook = mysql_fetch_row($result_greslook);
	$resident_lookup_id = $row_greslook[0];


	// Insert resident record...
	$sql2 = "
	INSERT INTO cpm_residents(
		resident_serial,
		rmc_num,
		subsidiary_id,
		resident_num,
		resident_name,
		is_resident_director,
		date_last_imported,
		resident_status,
		resident_is_active,
		is_subtenant_account,
		related_to_test_account
	)
	VALUES(
		'".$resident_serial."',
		".$row_tl['rmc_num'].",
		".$row_tl['subsidiary_id'].",
		".$resident_lookup_id.",
		'RMG Test User',
		'N',
		'".$date_last_imported."',
		'Current',
		'1',
		'Y',
		'Y'
	)
	";
	mysql_query($sql2);
	if($test_mode == "Y"){
		print $sql2."<br>";
	}
	
	
	// Insert resident extra record...
	$sql3 = "
	INSERT INTO cpm_residents_extra(
		resident_num,
		rmc_num,
		question_id_1,
		question_id_2,
		answer_1,
		answer_2,
		email,
		password,
		is_first_login,
		password_to_be_sent,
		announce_optin,
		optout_marketing,
		is_subtenant_account,
		related_to_test_account
	)
	VALUES(
		".$resident_lookup_id.",
		".$row_tl['rmc_num'].",
		4,
		2,
		'01/01/1980',
		'Somewhere',
		'webmaster@rmgliving.co.uk',
		'".$crypt->encrypt($UTILS_DB_ENCODE, get_rand_id(16, "ALPHA", "UPPER") )."',
		'N',
		'N',
		'N',
		'Y',
		'Y',
		'Y'
	)
	";
	mysql_query($sql3);
	if($test_mode == "Y"){
		print $sql3."<br>";
	}
	
	
	
	
	
	//========================================================================
	// Director login account
	//========================================================================
	
	// Get unique Resident reference...
	$dummy_resident_ref = gen_unique_resident_ref();
	$resident_serial = $security->gen_serial(32);
	
	
	// Insert resident lookup record...
	$sql_lres = "
	INSERT INTO cpm_lookup_residents (
		subsidiary_id,
		qube_record_id,
		resident_ref,
		date_created,
		related_to_test_account
	)VALUES(
		".$row_tl['subsidiary_id'].",
		".$rmc_counter.",
		'".$dummy_resident_ref."',
		'".date("H:i d/m/Y")."',
		'Y'
	) 
	";
	mysql_query($sql_lres);
	if($test_mode == "Y"){
		print $sql_lres."<br>";
	}
	
	// Get resident lookup id
	$sql_greslook = "SELECT LAST_INSERT_ID() FROM cpm_lookup_residents";
	$result_greslook = mysql_query($sql_greslook);
	$row_greslook = mysql_fetch_row($result_greslook);
	$resident_lookup_id = $row_greslook[0];
	
	
	// Insert resident record...
	$sql2 = "
	INSERT INTO cpm_residents(
		resident_serial,
		rmc_num,
		subsidiary_id,
		resident_num,
		resident_name,
		is_resident_director,
		date_last_imported,
		resident_status,
		resident_is_active,
		related_to_test_account
	)
	VALUES(
		'".$resident_serial."',
		".$row_tl['rmc_num'].",
		".$row_tl['subsidiary_id'].",
		".$resident_lookup_id.",
		'RMG Test User',
		'Y',
		'".$date_last_imported."',
		'Current',
		'1',
		'Y'
	)
	";
	mysql_query($sql2);
	if($test_mode == "Y"){
		print $sql2."<br>";
	}
	
	
	// Insert resident extra record...
	$sql3 = "
	INSERT INTO cpm_residents_extra(
		resident_num,
		rmc_num,
		question_id_1,
		question_id_2,
		answer_1,
		answer_2,
		email,
		password,
		is_first_login,
		password_to_be_sent,
		announce_optin,
		optout_marketing,
		related_to_test_account,
		is_resident_director
	)
	VALUES(
		".$resident_lookup_id.",
		".$row_tl['rmc_num'].",
		4,
		2,
		'01/01/1980',
		'Somewhere',
		'webmaster@rmgliving.co.uk',
		'".$crypt->encrypt($UTILS_DB_ENCODE, get_rand_id(16, "ALPHA", "UPPER") )."',
		'N',
		'N',
		'N',
		'Y',
		'Y',
		'Y'
	)
	";
	mysql_query($sql3);
	if($test_mode == "Y"){
		print $sql3."<br>";
	}
	
	
	// Insert unit record...
	$sql3a = "
	INSERT INTO cpm_units(
		subsidiary_id,
		unit_num,
		rmc_num,
		resident_num,
		unit_address_1,
		unit_address_2,
		unit_address_3,
		unit_city,
		unit_county,
		unit_postcode,
		unit_country,
		date_last_imported,
		related_to_test_account
	)
	VALUES(
		".$row_tl['subsidiary_id'].",
		'1002',
		".$row_tl['rmc_num'].",
		".$resident_lookup_id.",
		'Test Address 1',
		'Test Address 2',
		'Test Address 3',
		'Test City',
		'Test County',
		'Test Postcode',
		'UK',
		'".$date_last_imported."',
		'Y'
	)
	";
	mysql_query($sql3a);
	if($test_mode == "Y"){
		print $sql3a."<br>";
	}
	
	$report .= "Added accounts for rmc_num ".$row_tl['rmc_num']." 
	";
	
	print $rmc_counter."\n";
	
	$rmc_counter++;
}

$report .= "Script finished at ".date("H:i:s jS F Y").".

";
$report .= "Created ".$rmc_counter." login records.";

GLOBAL $UTILS_ADMIN_EMAIL;
	
mail($UTILS_ADMIN_EMAIL,"Create Team RMC Logins",$report);

?>