<?
//============================================================================================
// This script creates all the logins for PN teams to be able to access individual RMC sites
//============================================================================================

ini_set("max_execution_time", "1440000");
require_once("../utils.php");
$site_locality = "remote";
require_once("../management/gen_password.php");
require ("../library/classes/encryption.class.php");
$crypt = new encryption_class;
$starttime = time();
$date_last_imported = date("H:i d/m/Y", $starttime);



// Check to see if rmc exists, if not add PM team login account
$rmc_exists = 0;
$sql_tl = "SELECT rmc_num FROM cpm_rmcs ORDER BY rmc_num ASC";
$result_tl = mysql_query($sql_tl);
while($row_tl = mysql_fetch_row($result_tl)){

	print $row_tl[0]."<br>";

	//==================================================================================
	// Standard Lessee
	//==================================================================================
	
	$password1 = get_rand_id(8, "ALPHA", "UPPER");
	$do_loop = 1;
	$loop_counter = 0;
	
	// First check that resident num is unique
	while($do_loop > 0){
		$tnum1 = get_rand_id(6, "NUMERIC", "");
		$sql_un = "SELECT resident_num FROM cpm_residents WHERE resident_num = 't0".$tnum1."'";
		print $sql_un."<br>";
		$result_un = mysql_query($sql_un);
		$do_loop = mysql_num_rows($result_un);
		$loop_counter++;
		if($loop_counter > 1000){
			$do_loop = 0;
		}
	}

	print "Standard Lessee : t0".$tnum1."<br>";

	// Insert login account
	$sql2 = "
	INSERT INTO cpm_residents(
		rmc_num,
		resident_num,
		resident_name,
		is_resident_director,
		date_last_imported,
		resident_status,
		resident_is_active
	)
	VALUES(
		'".$row_tl[0]."',
		't0".$tnum1."',
		'CPM User',
		'N',
		'".$date_last_imported."',
		'Current',
		'1'
	)
	";
	mysql_query($sql2);
	
	$sql3 = "
	INSERT INTO cpm_residents_extra(
		resident_num,
		rmc_num,
		question_id_1,
		question_id_2,
		answer_1,
		answer_2,
		email,
		password,
		is_first_login,
		password_to_be_sent
	)
	VALUES(
		't0".$tnum1."',
		'".$row_tl[0]."',
		4,
		2,
		'01/01/1980',
		'Somewhere',
		'webmaster@cpmliving.com',
		'".addslashes($crypt->encrypt($UTILS_DB_ENCODE, $password1))."',
		'N',
		'N'
	)
	";
	mysql_query($sql3);
	
	$sql3a = "
	INSERT INTO cpm_units(
		unit_num,
		rmc_num,
		resident_num,
		resident_name,
		unit_address_1,
		unit_address_2,
		unit_address_3,
		unit_city,
		unit_county,
		unit_postcode,
		unit_country,
		date_last_imported
	)
	VALUES(
		'1001',
		'".$row_tl[0]."',
		't0".$tnum1."',
		'CPM User',
		'Test',
		'Test',
		'Test',
		'Test',
		'Test',
		'Test',
		'UK',
		'".$date_last_imported."'
	)
	";
	mysql_query($sql3a);
	
	
	//========================================================================
	// Director login account
	//========================================================================
	
	$password2 = get_rand_id(8, "ALPHA", "UPPER");
	$do_loop = 1;
	$loop_counter = 0;
	
	// First check that resident num is unique
	while($do_loop > 0){
		$tnum2 = get_rand_id(6, "NUMERIC", "");
		$sql_un = "SELECT resident_num FROM cpm_residents WHERE resident_num = 't0".$tnum2."'";
		$result_un = mysql_query($sql_un);
		$do_loop = mysql_num_rows($result_un);
		$loop_counter++;
		if($loop_counter > 1000){
			$do_loop = 0;
		}
	}
	
	print "Director : t0".$tnum2."<br>";
	
	// Insert login account
	$sql4 = "
	INSERT INTO cpm_residents(
		rmc_num,
		resident_num,
		resident_name,
		is_resident_director,
		date_last_imported,
		resident_status,
		resident_is_active
	)
	VALUES(
		'".$row_tl[0]."',
		't0".$tnum2."',
		'CPM User',
		'Y',
		'".$date_last_imported."',
		'Current',
		'1'
	)
	";
	mysql_query($sql4);
	
	$sql5 = "
	INSERT INTO cpm_residents_extra(
		resident_num,
		rmc_num,
		question_id_1,
		question_id_2,
		answer_1,
		answer_2,
		email,
		password,
		is_first_login,
		password_to_be_sent
	)
	VALUES(
		't0".$tnum2."',
		'".$row_tl[0]."',
		4,
		2,
		'01/01/1980',
		'Somewhere',
		'webmaster@cpmliving.com',
		'".addslashes($crypt->encrypt($UTILS_DB_ENCODE, $password2))."',
		'N',
		'N'
	)
	";
	mysql_query($sql5);
	
	$sql5a = "
	INSERT INTO cpm_units(
		unit_num,
		rmc_num,
		resident_num,
		resident_name,
		unit_address_1,
		unit_address_2,
		unit_address_3,
		unit_city,
		unit_county,
		unit_postcode,
		unit_country,
		date_last_imported
	)
	VALUES(
		'1001',
		'".$row_tl[0]."',
		't0".$tnum2."',
		'CPM User',
		'Test',
		'Test',
		'Test',
		'Test',
		'Test',
		'Test',
		'UK',
		'".$date_last_imported."'
	)
	";
	mysql_query($sql5a);
	
	
	
	//============================================================================
	// Steerng committee account
	//============================================================================
	
	$password3 = get_rand_id(8, "ALPHA", "UPPER");
	$do_loop = 1;
	$loop_counter = 0;
	
	// First check that resident num is unique
	while($do_loop > 0){
		$tnum3 = get_rand_id(6, "NUMERIC", "");
		$sql_un = "SELECT resident_num FROM cpm_residents WHERE resident_num = 't0".$tnum3."'";
		$result_un = mysql_query($sql_un);
		$do_loop = mysql_num_rows($result_un);
		$loop_counter++;
		if($loop_counter > 1000){
			$do_loop = 0;
		}
	}
	
	print "Steering Member : t0".$tnum3."<br>";
	
	// Insert 'Steering Committee' login account
	$sql6 = "
	INSERT INTO cpm_residents(
		rmc_num,
		resident_num,
		resident_name,
		is_resident_director,
		date_last_imported,
		resident_status,
		resident_is_active
	)
	VALUES(
		'".$row_tl[0]."',
		't0".$tnum3."',
		'CPM User',
		'N',
		'".$date_last_imported."',
		'Current',
		'1'
	)
	";
	mysql_query($sql6);
	
	$sql7 = "
	INSERT INTO cpm_residents_extra(
		resident_num,
		rmc_num,
		question_id_1,
		question_id_2,
		answer_1,
		answer_2,
		email,
		password,
		is_first_login,
		password_to_be_sent,
		resident_is_steering_member
	)
	VALUES(
		't0".$tnum3."',
		'".$row_tl[0]."',
		4,
		2,
		'01/01/1980',
		'Somewhere',
		'webmaster@cpmliving.com',
		'".addslashes($crypt->encrypt($UTILS_DB_ENCODE, $password3))."',
		'N',
		'N',
		'Y'
	)
	";
	mysql_query($sql7);
	
	$sql8 = "
	INSERT INTO cpm_units(
		unit_num,
		rmc_num,
		resident_num,
		resident_name,
		unit_address_1,
		unit_address_2,
		unit_address_3,
		unit_city,
		unit_county,
		unit_postcode,
		unit_country,
		date_last_imported
	)
	VALUES(
		'1001',
		'".$row_tl[0]."',
		't0".$tnum3."',
		'CPM User',
		'Test',
		'Test',
		'Test',
		'Test',
		'Test',
		'Test',
		'UK',
		'".$date_last_imported."'
	)
	";
	mysql_query($sql8);
}


?>