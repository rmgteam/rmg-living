<?
//===================================================================================================
// This script removes all the dummy logins for PM teams to be able to access individual RMC sites.
// This script must run at 9pm on a Friday night (before the 'create_team_rmc_logins.php'
// script runs.) This script runs weekly.
//===================================================================================================

ini_set("max_execution_time", "1440000");
require_once("utils.php");

$starttime = time();

$report .= "Script started at ".date("H:i:s jS F Y").".

";

// Remove all existing 't0' resdient records (increases security)
$del_dummy_counter = 0;
$sql = "
SELECT resident_lookup 
FROM cpm_lookup_residents 
WHERE 
related_to_test_account = 'Y' 
";
$result = mysql_query($sql);
$num_existing_dummy_records = mysql_num_rows($result);
while($row = mysql_fetch_row($result)){
	
	print "Deleting existing dummy records:".$del_dummy_counter." of ".$num_existing_dummy_records."\n";
	
	$sql2 = "DELETE FROM cpm_residents WHERE resident_num = ".$row[0];
	$result2 = mysql_query($sql2);
	
	$sql3 = "DELETE FROM cpm_residents_extra WHERE resident_num = ".$row[0];
	$result3 = mysql_query($sql3);
	
	$sql4 = "DELETE FROM cpm_lookup_residents WHERE resident_lookup = ".$row[0];
	$result4 = mysql_query($sql4);
	
	$del_dummy_counter++;
}

$report .= "Script finished at ".date("H:i:s jS F Y").".

";
$report .= "Removed ".$del_dummy_counter." login records.";

GLOBAL $UTILS_ADMIN_EMAIL;

mail($UTILS_ADMIN_EMAIL,"Remove Team RMC Logins",$report);

?>