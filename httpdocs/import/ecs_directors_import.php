<?
#########################################################################################
# This script imports all director data exported from ECS into the RMG Living website
#########################################################################################

ini_set("max_execution_time", "1440000");

// Mail report function
function mail_report($report){

	GLOBAL $UTILS_ADMIN_EMAIL;
	
	mail($UTILS_ADMIN_EMAIL, "RMG Living ECS Director Import Report", $report);
	exit;
}
function add_to_report($err_code, $msg){
	if($err_code != "1062"){
		$GLOBALS['report'] .= $msg."\n";
	}
}

require_once("utils.php");
require_once($UTILS_FILE_PATH."management/gen_password.php");
require($UTILS_FILE_PATH."library/classes/encryption.class.php");
require_once($UTILS_FILE_PATH."library/classes/security.class.php");
$crypt = new encryption_class;
$security = new security; 

$starttime = time();
$now_ts = $starttime;
$subsidiary_counter=1;
$total_rows_imported = 0;
$ftp_folder_path = $UTILS_ECS_EXPORT_PATH;
$this_ymd = date("Ymd");


// Loop for each version of the ECS database
$sql_sub = "SELECT * FROM cpm_subsidiary WHERE subsidiary_ecs_import = 'Y' AND subsidiary_discon = 'N'";
$result_sub = mysql_query($sql_sub);
$num_subsidiaries = mysql_num_rows($result_sub);
while($row_sub = mysql_fetch_array($result_sub)){
	

$msg = "

IMPORTING ".$row_sub['subsidiary_name']."
================================
";
	add_to_report("", $msg);
	
	// Check that Export file exists
	$import_file_name = $ftp_folder_path.$row_sub['subsidiary_code']."_ecs_director.csv";
	$skip_lines = 1;
	if(file_exists($import_file_name)){
		$num_records = count(file($import_file_name));
		$num_records = $num_records - $skip_lines;
	}
	else{
		continue;
	}
	
	
	// Empty tables
	mysql_query("DELETE FROM cpm_residents WHERE subsidiary_id = ".$row_sub['subsidiary_id']." AND is_resident_director = 'Y' AND resident_name <> 'RMG Test User' AND rmc_num <> 9999999 AND resident_is_developer_dummy <> 'Y'");
	
	
	// Loop through each line of file and split data
	$import_row_counter = 0;
	$sub_row_counter = 0;
	mysql_query("SET AUTOCOMMIT=1");
	$handle = fopen($import_file_name, "r");
	while($import_fields = fgetcsv($handle, 1000, ",","\"")){
		
		// Skip file header rows if need be
		if($skip_lines > $import_row_counter){
			$import_row_counter++;
			continue;
		}
		
		print $import_row_counter."\n";

		#=====================================================================================
		# ASSIGN FIELDS TO VARIABLES
		#=====================================================================================
		$date_last_imported = date("H:i d/m/Y", $starttime);
		$resident_serial = $security->gen_serial(32);
		$rmc_num = trim(addslashes($import_fields[1]));
		$resident_ref = trim(addslashes($import_fields[2]));
		$unit_num = trim(addslashes($import_fields[3]));
		$director_num = trim(addslashes($import_fields[4]));
		$director_name = trim(addslashes($import_fields[5]));
		$start_occupany_date = trim(addslashes($import_fields[6]));
		$end_occupany_date = trim(addslashes($import_fields[7]));
		$qube_record_id = trim($import_fields[9]);
		$resident_qube_record_id = trim($import_fields[10]);
		
		// Determine if record should be active...
		$director_is_active = "0";
		if($start_occupany_date != ""){
			$start_ymd_parts = explode("/", $start_occupany_date);
			$start_ymd = $start_ymd_parts[2].$start_ymd_parts[1].$start_ymd_parts[0];
		}
		if($end_occupany_date != ""){
			$end_ymd_parts = explode("/", $end_occupany_date);
			$end_ymd = $end_ymd_parts[2].$end_ymd_parts[1].$end_ymd_parts[0];
		}
		if($start_ymd <= $this_ymd && ($end_occupany_date == "" || $end_ymd > $this_ymd)){
			$director_is_active = "1";
		}
		
		
		// Get RMC data (of lessee)
		$rmc_lookup_id = "";
		$sql_rmc_lookup = "
		SELECT rmc_lookup 
		FROM cpm_lookup_rmcs 
		WHERE 
		subsidiary_id = ".$row_sub['subsidiary_id']." AND 
		rmc_ref = '".$rmc_num."'";
		//print $sql_rmc_lookup;
		$result_rmc_lookup = @mysql_query($sql_rmc_lookup);
		$num_rmc_lookup = @mysql_num_rows($result_rmc_lookup);
		if($num_rmc_lookup > 0){
			
			$row_rmc_lookup = @mysql_fetch_row($result_rmc_lookup);
			$rmc_lookup_id = $row_rmc_lookup[0];
		}
		else{
			continue;	
		}

	
		// Get Lessee data
		$resident_lookup_id = "";
		$sql_resident_lookup = "
		SELECT resident_lookup 
		FROM cpm_lookup_residents 
		WHERE 
		subsidiary_id = ".$row_sub['subsidiary_id']." AND 
		qube_record_id = ".$resident_qube_record_id." AND 
		resident_ref = '".$resident_ref."'";
		$result_resident_lookup = @mysql_query($sql_resident_lookup);
		$num_resident_lookup = @mysql_num_rows($result_resident_lookup);
		if($num_resident_lookup > 0){
		
			$row_resident_lookup = @mysql_fetch_row($result_resident_lookup);
			$resident_lookup_id = $row_resident_lookup[0];
		}
		else{
			continue;
		}
		
		// Handle director assoc resident num
		$dir_assoc_res_num = $resident_lookup_id;
		if($resident_lookup_id == ""){
			$dir_assoc_res_num = "NULL";
		}
		
		// Get resident data (of lessee) to insert into director record
		$sql_resident = "
		SELECT * 
		FROM cpm_residents  
		WHERE 
		subsidiary_id = ".$row_sub['subsidiary_id']." AND 
		resident_num = ".$resident_lookup_id." 
		";
		$result_resident = @mysql_query($sql_resident);
		$row_resident = @mysql_fetch_array($result_resident);
		
		// Now create director account
		$director_lookup_id = "";
		$sql_subtenant_lookup = "
		SELECT resident_lookup 
		FROM cpm_lookup_residents 
		WHERE 
		subsidiary_id = ".$row_sub['subsidiary_id']." AND 
		qube_record_id = ".$qube_record_id." AND 
		resident_ref = '".$director_num."'";
		$result_subtenant_lookup = @mysql_query($sql_subtenant_lookup);
		$num_director_lookup = @mysql_num_rows($result_subtenant_lookup);
		if($num_director_lookup > 0){
			
			$row_subtenant_lookup = @mysql_fetch_row($result_subtenant_lookup);
			$director_lookup_id = $row_subtenant_lookup[0];
			
			// Insert director record
			$sql_adr = "
			INSERT INTO cpm_residents(
				resident_serial,
				subsidiary_id,
				resident_num,
				rmc_num,
				resident_name,
				resident_address_1,
				resident_address_2,
				resident_address_3,
				resident_address_4,
				resident_address_city,
				resident_address_county,
				resident_address_postcode,
				date_last_imported,
				resident_status,
				resident_is_active,
				is_resident_director,
				director_assoc_resident_num,
				temp
			)
			VALUES(
				'".$resident_serial."',
				'".$row_sub['subsidiary_id']."',
				".$director_lookup_id.",
				".$rmc_lookup_id.",
				'".addslashes($director_name)."',
				'".addslashes($row_resident['resident_address_1'])."',
				'".addslashes($row_resident['resident_address_2'])."',
				'".addslashes($row_resident['resident_address_3'])."',
				'".addslashes($row_resident['resident_address_4'])."',
				'".addslashes($row_resident['resident_address_city'])."',
				'".addslashes($row_resident['resident_address_county'])."',
				'".addslashes($row_resident['resident_address_postcode'])."',
				'".$date_last_imported."',
				'".addslashes($row_resident['resident_status'])."',
				'".$director_is_active."',
				'Y',
				".$dir_assoc_res_num.",
				'director'
			)
			";
			mysql_query($sql_adr) or add_to_report(mysql_errno(), "Insert director into 'cpm_residents' (".$row_sub['subsidiary_name']." - ".$director_num.") failed: ".$sql_adr);
		
		}
		else{

			if($director_is_active === "1"){
						
				$sql_ins_lookup = "
				INSERT INTO cpm_lookup_residents (
					subsidiary_id,
					qube_record_id,
					resident_ref,
					date_created,
					temp
				)VALUES(
					".$row_sub['subsidiary_id'].",
					".$qube_record_id.",
					'".$director_num."',
					'".date("H:i d/m/Y", $starttime)."',
					'director'
				)
				";
				mysql_query($sql_ins_lookup) or add_to_report(mysql_errno(), "Insert director lookup into 'cpm_lookup_residents' (".$row_sub['subsidiary_name']." - ".$director_num.") failed: ".$sql_ins_lookup);
				
				$sql_insid = "SELECT LAST_INSERT_ID() FROM cpm_lookup_residents";
				$result_insid = mysql_query($sql_insid);
				$row_insid = mysql_fetch_row($result_insid);
				$director_lookup_id = $row_insid[0];
				
				// Insert director record
				$sql_adr = "
				INSERT INTO cpm_residents(
					resident_serial,
					subsidiary_id,
					resident_num,
					rmc_num,
					resident_name,
					resident_address_1,
					resident_address_2,
					resident_address_3,
					resident_address_4,
					resident_address_city,
					resident_address_county,
					resident_address_postcode,
					date_last_imported,
					resident_status,
					resident_is_active,
					is_resident_director,
					director_assoc_resident_num,
					temp
				)
				VALUES(
					'".$resident_serial."',
					'".$row_sub['subsidiary_id']."',
					".$director_lookup_id.",
					".$rmc_lookup_id.",
					'".addslashes($director_name)."',
					'".addslashes($row_resident['resident_address_1'])."',
					'".addslashes($row_resident['resident_address_2'])."',
					'".addslashes($row_resident['resident_address_3'])."',
					'".addslashes($row_resident['resident_address_4'])."',
					'".addslashes($row_resident['resident_address_city'])."',
					'".addslashes($row_resident['resident_address_county'])."',
					'".addslashes($row_resident['resident_address_postcode'])."',
					'".$date_last_imported."',
					'".addslashes($row_resident['resident_status'])."',
					'".$director_is_active."',
					'Y',
					".$dir_assoc_res_num.",
					'director'
				)
				";
				mysql_query($sql_adr) or add_to_report(mysql_errno(), "Insert director into 'cpm_residents' (".$row_sub['subsidiary_name']." - ".$director_num.") failed: ".$sql_adr);
			
				// Insert director 'extra' record
				$password = get_rand_id(8, "ALPHA", "UPPER");
				$sql_adr = "
				INSERT INTO cpm_residents_extra(
					resident_num,
					rmc_num,
					password,
					password_to_be_sent,
					is_resident_director,
					date_created,
					temp
				)
				VALUES(
					".$director_lookup_id.",
					".$rmc_lookup_id.",
					'".$crypt->encrypt($UTILS_DB_ENCODE, $password)."',
					'Y',
					'Y',
					'".date("H:i d/m/Y", $starttime)."',
					'director'
				)
				";
				mysql_query($sql_adr) or add_to_report(mysql_errno(), "Insert director into 'cpm_residents_extra' (".$row_sub['subsidiary_name']." - ".$director_num.") failed: ".$sql_adr);
			}
		}
		
		
		
		
	
	
		# Increment file row counter
		$import_row_counter++;
		$sub_row_counter++;
		
	}
	
	
	// Get time
	$thistime = time();
	
	// Close import files
	fclose($handle);

	$report .= "\r\nROWS IMPORTED FOR ".$row_sub['subsidiary_name'].": ".$sub_row_counter."";
	$subsidiary_counter++;
	$total_rows_imported += $sub_row_counter;
}


// Send import status report by email
$endtime = time();
$script_duration = ($endtime - $starttime);
add_to_report("","\n\nSCRIPT DURATION: $script_duration sec");
add_to_report("","\nTOTAL ROWS IMPORTED: $total_rows_imported\n\n");
//print $GLOBALS['report'];
mail_report($GLOBALS['report']);

exit;
?>