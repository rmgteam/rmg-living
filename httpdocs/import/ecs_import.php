<?
#####################################################################################
# This script imports all data exported from ECS into the RMG Living website
#####################################################################################

require_once($UTILS_FILE_PATH."library/classes/mailer.class.php");

ini_set("max_execution_time", "1440000");
ini_set("memory_limit", -1);

// Mail report function
function mail_report($report){

	GLOBAL $UTILS_IMPORT_EMAIL;

	$email = new mailer();
	$email->set_mail_type("ECS to RMG Living Property ONLY Import Report");
	$email->set_mail_to($UTILS_IMPORT_EMAIL);
	$email->set_mail_from("webteam@rmguk.com");
	$email->set_mail_subject("RMG Living ECS Import Report");
	$email->set_mail_message($report);
	$email->set_mail_headers('');
	$email->send();

	print $report;
}

function add_to_report($err_code, $msg){

	if($err_code != "1062"){
		$GLOBALS['report'] .= $msg."\n";
	}
}

require_once("utils.php");
require_once($UTILS_FILE_PATH."management/gen_password.php");
require($UTILS_FILE_PATH."library/classes/encryption.class.php");
require($UTILS_FILE_PATH."library/classes/security.class.php");
$crypt = new encryption_class;
$security = new security;
$starttime = time();
$now_ts = $starttime; 
$subsidiary_counter=1;
$total_rows_imported = 0;
$ftp_folder_path = $UTILS_ECS_EXPORT_PATH;


// Loop for each version of the ECS database
$sql_sub = "SELECT * FROM cpm_subsidiary WHERE subsidiary_ecs_import = 'Y' AND subsidiary_discon = 'N'";
$result_sub = mysql_query($sql_sub);
$num_subsidiaries = mysql_num_rows($result_sub);
while($row_sub = mysql_fetch_array($result_sub)){


$msg = "

IMPORTING ".$row_sub['subsidiary_name']."
================================
";
	add_to_report("", $msg);

	// Check that Export file exists
	$import_file_name = $ftp_folder_path.$row_sub['subsidiary_code']."_ecs_export.csv";
	$skip_lines = 1;
	$num_records = count(file($import_file_name));
	$num_records = $num_records - $skip_lines;
	if(!file_exists($import_file_name)){
		mail_report("Import File Does Not Exist for ".$row_sub['subsidiary_name']);
	}
	
	
	// Empty tables
	mysql_query("DELETE FROM cpm_residents WHERE subsidiary_id = ".$row_sub['subsidiary_id']." AND related_to_test_account <> 'Y' AND rmc_num <> 9999999 AND resident_is_developer_dummy <> 'Y'");
	mysql_query("DELETE FROM cpm_rmcs WHERE subsidiary_id = ".$row_sub['subsidiary_id']." AND related_to_test_account <> 'Y' AND rmc_num <> 9999999 AND rmc_is_developer_dummy <> 'Y'");
	mysql_query("DELETE FROM cpm_units WHERE subsidiary_id = ".$row_sub['subsidiary_id']." AND related_to_test_account <> 'Y' AND rmc_num <> 9999999 AND unit_is_developer_dummy <> 'Y'");



	// Loop through each line of file and split data
	$import_row_counter = 0;
	$sub_row_counter = 0;
	//mysql_query("SET AUTOCOMMIT=1");
	$handle = fopen($import_file_name, "r");
	while($import_fields = fgetcsv($handle, 1000, ",","\"")){
		
		# Skip file header rows if need be
		if($skip_lines > $import_row_counter){
			$import_row_counter++;
			continue;
		}
		
	
		#=====================================================================================
		# ASSIGN FIELDS TO VARIABLES
		#=====================================================================================
		$date_last_imported = date("H:i d/m/Y", $starttime);
		$resident_serial = $security->gen_serial(32);
		
		$resident_num = trim(addslashes($import_fields[0]));
		$voyager_resident_num = trim(addslashes($import_fields[1]));
		$resident_name = $security->clean_query(trim(utf8_encode($import_fields[2])));
		$resident_address_1 = $security->clean_query(trim(utf8_encode($import_fields[3])));
		$resident_address_2 = $security->clean_query(trim(utf8_encode($import_fields[4])));
		$resident_address_3 = $security->clean_query(trim(utf8_encode($import_fields[5])));
		$resident_address_4 = $security->clean_query(trim(utf8_encode($import_fields[6])));
		$resident_address_5 = $security->clean_query(trim(utf8_encode($import_fields[7])));
		$resident_address_6 = $security->clean_query(trim(utf8_encode($import_fields[8])));
		$resident_address_postcode = $security->clean_query(trim(utf8_encode($import_fields[9])));
		$resident_status = trim(addslashes($import_fields[10]));
		$rmc_num = trim(addslashes($import_fields[11]));
		$voyager_rmc_num = trim(addslashes($import_fields[12]));
		$rmc_name = $security->clean_query(trim(utf8_encode($import_fields[13])));
		$property_manager = trim(addslashes($import_fields[14]));
		$unit_num = trim(addslashes($import_fields[15]));
		$voyager_unit_num = trim(addslashes($import_fields[16]));
		$unit_desc = $security->clean_query(trim(utf8_encode($import_fields[17])));
		$unit_address_1 = $security->clean_query(trim(utf8_encode($import_fields[18])));
		$unit_address_2 = $security->clean_query(trim(utf8_encode($import_fields[19])));
		$unit_address_3 = $security->clean_query(trim(utf8_encode($import_fields[20])));
		$unit_address_4 = $security->clean_query(trim(utf8_encode($import_fields[21])));
		$unit_address_5 = $security->clean_query(trim(utf8_encode($import_fields[22])));
		$unit_postcode = trim(addslashes($import_fields[23]));
		$rmc_status = trim(addslashes($import_fields[24]));
		$plot_num = trim(addslashes($import_fields[25]));
		$is_tenant_active = trim(addslashes($import_fields[26]));
		$is_property_active = trim(addslashes($import_fields[27]));
		$brand_name = trim(addslashes($import_fields[28]));
		$regional_manager = trim(addslashes($import_fields[31]));
		$op_director_name = trim(addslashes($import_fields[35]));
		$freeholder_name = $security->clean_query(trim(utf8_encode($import_fields[40])));
		$email_address = $security->clean_query(trim(utf8_encode($import_fields[41])));
		$qube_record_id = trim($import_fields[45]);

		
		# Skip the procedure if RMC status is inactive, tenant status in inactive
		if($is_property_active != "1" || $is_tenant_active != "1"){
			continue;
		}
		
		#=====================================================================================
		# IMPORT PROPERTY
		#=====================================================================================
		$rmc_record_start_ts = microtime(true);
		$rmc_lookup_id = "";
		$sql_rmc_lookup = "
		SELECT rmc_lookup 
		FROM cpm_lookup_rmcs 
		WHERE 
		subsidiary_id = ".$row_sub['subsidiary_id']." AND 
		rmc_ref = '".$rmc_num."'";
		$result_rmc_lookup = mysql_query($sql_rmc_lookup);
		$num_rmc_lookup = mysql_num_rows($result_rmc_lookup);
		if($num_rmc_lookup == 0){
		
			$sql_ins_lookup = "
			INSERT INTO cpm_lookup_rmcs (
				subsidiary_id,
				rmc_ref,
				date_created
			)VALUES(
				".$row_sub['subsidiary_id'].",
				'".$rmc_num."',
				'".date("H:i d/m/Y", $starttime)."'
			)
			";
			mysql_query($sql_ins_lookup) or add_to_report(mysql_errno(), "Insert Man. Co. lookup into 'cpm_lookup_rmcs' (".$row_sub['subsidiary_name']." - ".$rmc_num.") failed: ".mysql_error());
			
			$sql_insid = "SELECT LAST_INSERT_ID() FROM cpm_lookup_rmcs";
			$result_insid = mysql_query($sql_insid);
			$row_insid = mysql_fetch_row($result_insid);
			$rmc_lookup_id = $row_insid[0];
			
			$sql_ad = "
			INSERT INTO cpm_rmcs_extra (
				rmc_num,
				member_id
			)
			VALUES(
				".$rmc_lookup_id.",
				1
			)
			";
			mysql_query($sql_ad) or add_to_report(mysql_errno(), "Insert Man. Co. into 'cpm_rmcs_extra' (".$row_sub['subsidiary_name']." - ".$rmc_num.") failed: ".mysql_error());
		}
		else{
			$row_rmc_lookup = mysql_fetch_row($result_rmc_lookup);
			$rmc_lookup_id = $row_rmc_lookup[0];
		}
		
		// Here we insert the rmc record regardless (because the table is cleared out at the beginning of the script)
		$sql_ad = "
		INSERT INTO cpm_rmcs (
			subsidiary_id,
			rmc_name,
			rmc_status,
			rmc_is_active,
			property_manager,
			regional_manager,
			rmc_num,
			voyager_rmc_num,
			date_last_imported,
			rmc_op_director_name,
			brand_name,
			rmc_freeholder_name
		)
		VALUES(
			'".$row_sub['subsidiary_id']."',
			'".$rmc_name."',
			'".$rmc_status."',
			'".$is_property_active."',
			'".$property_manager."',
			'".$regional_manager."',
			".$rmc_lookup_id.",
			'".$voyager_rmc_num."',
			'".$date_last_imported."',
			'".$op_director_name."',
			'".$brand_name."',
			'".$freeholder_name."'
		)
		";
		mysql_query($sql_ad) or add_to_report(mysql_errno(), "Insert Man. Co. into 'cpm_rmcs' (".$row_sub['subsidiary_name']." - ".$rmc_num.") failed: ".mysql_error());


		// CHECK RMCS_EXTRA row exists
		$sql_rmc_extra = "
		SELECT count(*) 
		FROM cpm_rmcs_extra 
		WHERE 
		rmc_num = ".$rmc_lookup_id;
		$result_rmc_extra = mysql_query($sql_rmc_extra);
		$row_rmc_extra = mysql_fetch_row($result_rmc_extra);
		if($row_rmc_extra[0] == 0){
			
			$sql_ad = "
			INSERT INTO cpm_rmcs_extra (
				rmc_num,
				member_id
			)
			VALUES(
				".$rmc_lookup_id.",
				1
			)
			";
			mysql_query($sql_ad) or add_to_report(mysql_errno(), "Insert Man. Co. into 'cpm_rmcs_extras' (".$row_sub['subsidiary_name']." - ".$rmc_lookup_id.") failed: ".mysql_error());
		
		}



		// IMPORT RESIDENT
		$resident_record_start_ts = microtime(true);
		$resident_lookup_id = "";
		$sql_resident_lookup = "
		SELECT resident_lookup 
		FROM cpm_lookup_residents 
		WHERE 
		subsidiary_id = ".$row_sub['subsidiary_id']." AND 
		qube_record_id = ".$qube_record_id." AND 
		resident_ref = '".$resident_num."'";
		$result_resident_lookup = mysql_query($sql_resident_lookup);
		$num_resident_lookup = mysql_num_rows($result_resident_lookup);
		if($num_resident_lookup == 0){
		
			$sql_ins_lookup = "
			INSERT INTO cpm_lookup_residents (
				subsidiary_id,
				resident_ref,
				qube_record_id,
				date_created
			)VALUES(
				".$row_sub['subsidiary_id'].",
				'".$resident_num."',
				".$qube_record_id.",
				'".date("H:i d/m/Y", $starttime)."'
			)
			";
			mysql_query($sql_ins_lookup) or add_to_report(mysql_errno(), "Insert resident lookup into 'cpm_lookup_residents' (".$row_sub['subsidiary_name']." - ".$resident_num.") failed: ".mysql_error());
			
			$sql_insid = "SELECT LAST_INSERT_ID() FROM cpm_lookup_residents";
			$result_insid = mysql_query($sql_insid);
			$row_insid = mysql_fetch_row($result_insid);
			$resident_lookup_id = $row_insid[0];
			
			$password = get_rand_id(8, "ALPHA", "UPPER");
			$sql_adr = "
			INSERT INTO cpm_residents_extra (
				resident_num,
				rmc_num,
				password,
				password_to_be_sent,
				date_created,
				date_created_ymdhis
			)
			VALUES(
				".$resident_lookup_id.",
				".$rmc_lookup_id.",
				'".$crypt->encrypt($UTILS_DB_ENCODE, $password)."',
				'Y',
				'".date("H:i d/m/Y", $starttime)."',
				'".date("YmdHis", $starttime)."'
			)
			";
			mysql_query($sql_adr) or add_to_report(mysql_errno(), "Insert resident into 'cpm_residents_extra' (".$row_sub['subsidiary_name']." - ".$resident_num.") failed: ".mysql_error());
		}
		else{
			$row_resident_lookup = mysql_fetch_row($result_resident_lookup);
			$resident_lookup_id = $row_resident_lookup[0];
		}
			
		if($voyager_unit_num > 9000){
			$is_dev = 'Y';				
		}else{
			$is_dev = 'N';
		}
		
		$sql_adr = "
		INSERT INTO cpm_residents(
			resident_serial,
			subsidiary_id,
			resident_num,
			voyager_resident_num,
			rmc_num,
			resident_name,
			resident_address_1,
			resident_address_2,
			resident_address_3,
			resident_address_4,
			resident_address_city,
			resident_address_county,
			resident_address_postcode,
			date_last_imported,
			resident_status,
			resident_is_active,
			resident_is_developer,
			resident_email
		)
		VALUES(
			'".$resident_serial."',
			'".$row_sub['subsidiary_id']."',
			".$resident_lookup_id.",
			'".$voyager_resident_num."',
			".$rmc_lookup_id.",
			'".$resident_name."',
			'".$resident_address_1."',
			'".$resident_address_2."',
			'".$resident_address_3."',
			'".$resident_address_4."',
			'".$resident_address_5."',
			'".$resident_address_6."',
			'".$resident_address_postcode."',
			'".$date_last_imported."',
			'".$resident_status."',
			'".$is_tenant_active."',
			'".$is_dev."',
			'".$email_address."'
		)
		";
		mysql_query($sql_adr) or add_to_report(mysql_errno(), "Insert resident into 'cpm_residents' (".$row_sub['subsidiary_name']." - ".$resident_num.") failed: ".mysql_error());
	
		
	
		// IMPORT UNIT
		$unit_record_start_ts = microtime(true);
		$unit_lookup_id = "";
		$sql_unit_lookup = "
		SELECT unit_lookup 
		FROM cpm_lookup_units 
		WHERE 
		subsidiary_id = ".$row_sub['subsidiary_id']." AND 
		rmc_num = ".$rmc_lookup_id." AND 
		unit_ref = '".$unit_num."'";
		$result_unit_lookup = mysql_query($sql_unit_lookup);
		$num_unit_lookup = mysql_num_rows($result_unit_lookup);
		if($num_unit_lookup == 0){
		
			$sql_unit_lookup = "
			INSERT INTO cpm_lookup_units (
				subsidiary_id,
				unit_ref,
				rmc_num,
				date_created
			)VALUES(
				".$row_sub['subsidiary_id'].",
				'".$unit_num."',
				".$rmc_lookup_id.",
				'".date("H:i d/m/Y", $starttime)."'
			)
			";
			mysql_query($sql_unit_lookup) or add_to_report(mysql_errno(), "Insert unit lookup into 'cpm_lookup_units' (".$row_sub['subsidiary_name']." - ".$unit_num.") failed: ".mysql_error());
			
			
			$sql_insid = "SELECT LAST_INSERT_ID() FROM cpm_lookup_units";
			$result_insid = mysql_query($sql_insid);
			$row_insid = mysql_fetch_row($result_insid);
			$unit_lookup_id = $row_insid[0];
		}
		else{
			$row_unit_lookup = @mysql_fetch_row($result_unit_lookup);
			$unit_lookup_id = $row_unit_lookup[0];
		}
		
				
		$sql_unit = "
		INSERT INTO cpm_units(
			subsidiary_id,
			unit_num,
			voyager_unit_num,
			resident_num,
			rmc_num,
			unit_description,
			unit_address_1,
			unit_address_2,
			unit_address_3,
			unit_city,
			unit_county,
			unit_postcode,
			date_last_imported
		)
		VALUES(
			'".$row_sub['subsidiary_id']."',
			".$unit_lookup_id.",
			'".$voyager_unit_num."',
			'".$resident_lookup_id."',
			".$rmc_lookup_id.",
			'".$unit_desc."',
			'".$unit_address_1."',
			'".$unit_address_2."',
			'".$unit_address_3."',
			'".$unit_address_4."',
			'".$unit_address_5."',
			'".$unit_postcode."',
			'".$date_last_imported."'
		)
		";
		mysql_query($sql_unit) or add_to_report(mysql_errno(), "Insert unit into 'cpm_units' (".$row_sub['subsidiary_name']." - ".$unit_num.") failed: ".mysql_error().":".$sql_unit);


		// Increment file row counter
		$import_row_counter++;
		$sub_row_counter++;
		
	}
	
	
	// Get time
	$thistime = time();
	
	// Close import files
	fclose($handle);

	$report .= "\r\nROWS IMPORTED FOR ".$row_sub['subsidiary_name'].": ".$sub_row_counter."";
	$subsidiary_counter++;
	$total_rows_imported += $sub_row_counter;
}


// For any resident accounts that have gone inactive, dis-associate it from all master accounts 
$sql_dis = "
SELECT resident_num 
FROM cpm_residents 
WHERE 
resident_is_active <> '1' 
";
$result_dis = @mysql_query($sql_dis);
while( $row_dis = @mysql_fetch_row($result_dis) ){
	
	$sql_disd = "
	DELETE FROM cpm_master_account_resident_assoc 
	WHERE 
	resident_num = ".$row_dis[0]." 
	";
	@mysql_query($sql_disd);
}


// Send import status report by email
$endtime = time();
$script_duration = ($endtime - $starttime);
add_to_report("","\n\nSCRIPT DURATION: $script_duration sec");
add_to_report("","\nTOTAL ROWS IMPORTED: $total_rows_imported\n\n");
print $GLOBALS['report'];
mail_report($GLOBALS['report']);

exit;
?>