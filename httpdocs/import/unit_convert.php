<?
#####################################################################################
# This script imports all data exported from ECS into the RMG Living website
#####################################################################################

ini_set("max_execution_time", "1440000");

// Mail report function
function mail_report($report){
	GLOBAL $UTILS_IMPORT_EMAIL;
	
	mail($UTILS_IMPORT_EMAIL, "RMG Living ECS Import Report", $report);
	exit;
}

function add_to_report($err_code, $msg){

	if($err_code != "1062"){
		$GLOBALS['report'] .= $msg."\n";
	}
}

require_once("utils.php");
require($UTILS_FILE_PATH."library/classes/encryption.class.php");
require($UTILS_FILE_PATH."library/classes/security.class.php");
$crypt = new encryption_class;
$security = new security;
$starttime = time();
$now_ts = $starttime; 
$subsidiary_counter=1;
$total_rows_imported = 0;
$ftp_folder_path = $UTILS_ECS_EXPORT_PATH;

$import_file_name = $ftp_folder_path."unit_convert.csv";

// Loop through each line of file and split data
$import_row_counter = 0;
$sub_row_counter = 0;
$skip_lines = 1;
$handle = fopen($import_file_name, "r");
while($import_fields = fgetcsv($handle, 1000, ",","\"")){
	
	# Skip file header rows if need be
	if($skip_lines > $import_row_counter){
		$import_row_counter++;
		continue;
	}

	#=====================================================================================
	# ASSIGN FIELDS TO VARIABLES
	#=====================================================================================
	
	$subsidiary_ref = strtolower(trim(addslashes($import_fields[0])));
	$rmc_ref = trim(addslashes($import_fields[1]));
	$unit_ref = trim(addslashes($import_fields[2]));
	
	// Loop for each version of the ECS database
	$sql_sub = "SELECT subsidiary_id 
	FROM cpm_subsidiary 
	WHERE subsidiary_ecs_import = 'Y' 
	AND subsidiary_discon = 'N'
	AND subsidiary_code = '".$subsidiary_ref."'";
	$result_sub = mysql_query($sql_sub);
	$num_subsidiaries = mysql_num_rows($result_sub);
	while($row_sub = mysql_fetch_array($result_sub)){
		$subsidiary_id = $row_sub['subsidiary_id'];
	}
	
	// Loop for each version of the ECS database
	$sql_sub = "SELECT rmc_lookup 
	FROM cpm_lookup_rmcs 
	WHERE rmc_ref = '".$rmc_ref."'
	AND subsidiary_id = '".$subsidiary_id."'";
	$result_sub = mysql_query($sql_sub);
	$num_subsidiaries = mysql_num_rows($result_sub);
	while($row_sub = mysql_fetch_array($result_sub)){
		$rmc_num = $row_sub['rmc_lookup'];
	}
	
	
	// IMPORT UNIT
	$unit_record_start_ts = microtime(true);
	$unit_lookup_id = "";
	$sql_unit_lookup = "
	UPDATE cpm_lookup_units
	SET rmc_num = '".$rmc_num."' 
	WHERE unit_ref = '".$unit_ref."'";
	$result_unit_lookup = mysql_query($sql_unit_lookup);
	
	// Increment file row counter
	$import_row_counter++;
	
}


// Get time
$thistime = time();

// Close import files
fclose($handle);

$report .= "\r\nROWS IMPORTED FOR ".$row_sub['subsidiary_name'].": ".$sub_row_counter."";
$subsidiary_counter++;
$total_rows_imported += $sub_row_counter;



// For any resident accounts that have gone inactive, dis-associate it from all master accounts 
$sql_dis = "
SELECT resident_num 
FROM cpm_residents 
WHERE 
resident_is_active <> '1' 
";
$result_dis = @mysql_query($sql_dis);
while( $row_dis = @mysql_fetch_row($result_dis) ){
	
	$sql_disd = "
	DELETE FROM cpm_master_account_resident_assoc 
	WHERE 
	resident_num = ".$row_dis[0]." 
	";
	@mysql_query($sql_disd);
}


// Send import status report by email
$endtime = time();
$script_duration = ($endtime - $starttime);
add_to_report("","\n\nSCRIPT DURATION: $script_duration sec");
add_to_report("","\nTOTAL ROWS IMPORTED: $total_rows_imported\n\n");
print $GLOBALS['report'];
mail_report($GLOBALS['report']);

exit;
?>