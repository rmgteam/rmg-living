<?
$site_inactive = "N";
if( $site_inactive == "Y" ){
	require_once($UTILS_FILE_PATH."holding_page.php");
	exit;
}
require("utils.php");
require_once($UTILS_CLASS_PATH."encryption.class.php");
require_once($UTILS_CLASS_PATH."security.class.php");
require_once($UTILS_CLASS_PATH."login.class.php");
require_once($UTILS_CLASS_PATH."resident.class.php");
require_once($UTILS_CLASS_PATH."master_account.class.php");
$crypt = new encryption_class;
$security = new security;
$login = new login;
$resident = new resident;
$master_account = new master_account;
$cut_off_day = date("Ymd", (time() - (86400*90)) );



// Check that address is https (secure) because of login
$login_error = "";
$remind_error = "";
$remind_success = "";


//================================================
// Log off routine
//================================================
if($_REQUEST['logoff'] == "Y"){
	
	unset($_SESSION['resident_session']);
	unset($_SESSION['rmc_num']);
	unset($_SESSION['rmc_ref']);
	unset($_SESSION['unit_ref']);
	unset($_SESSION['is_demo_account']);
	unset($_SESSION['is_developer']);
	unset($_SESSION['rmc_name']);
	unset($_SESSION['dev_description']);
	unset($_SESSION['member_id']);
	unset($_SESSION['resident_num']);
	unset($_SESSION['resident_ref']);
	unset($_SESSION['resident_name']);
	unset($_SESSION['login']);
	unset($_SESSION['subsidiary_id']);
	unset($_SESSION['master_account_serial']);
	unset($_SESSION['allow_letters']);
	unset($_SESSION['allow_letters_toggle']);
	unset($_SESSION['allow_letters']);
	unset($_SESSION['allow_contractor']);
	unset($_SESSION['contractor_session']);		
	unset($_SESSION['contractors_username']);
	unset($_SESSION['contractors_qube_id']);
	
	header("Location: ".$UTILS_HTTPS_ADDRESS."index.php");
	exit;
}

//================================================
// Log in routine
//================================================
if($_POST['whichaction'] == "login" && ($site_inactive != "Y" || $_POST['username'] == "demo" || $_POST['username'] == "cpmlivingadmin")){

	if($_POST['username'] == "" || $_POST['password'] == ""){
	
		$login_error = "Please enter your login details";
	}
	else{
		
		// Check for Resident login record...
		$login_error_msg = "";
		$result_array = $login->do_lessee_login($_POST['username'], $_POST['password']);
		
		if( $result_array[0] === true ){
			
			$resident->resident($_SESSION['resident_num']);
			
			// Update 'cpm_stat' table
			require($UTILS_FILE_PATH."library/stat.php");
			add_stat(3);
			
			// Check if first logon
			if($resident->is_first_login == "Y" || $resident->allow_password_reset == "Y"){
				
				header("Location: ".$UTILS_HTTPS_ADDRESS."change_details.php");
				exit;
			}
			else{

				// Check to see if there are any unread announcements and redirect if necessary
				$sql_announce_unread = "
				SELECT * 
				FROM cpm_announcements a, cpm_announcements_read ar 
				WHERE 
				a.announce_id=ar.announce_id AND 
				a.rmc_num = ".$security->clean_query($resident->rmc_num)." AND 
				a.announce_date > '$cut_off_day' AND 
				ar.resident_num = ".$security->clean_query($resident->resident_num)." AND 
				ar.announce_read = 'N' 
				ORDER BY a.announce_date DESC";
				$result_announce_unread = @mysql_query($sql_announce_unread);
				$num_announce_unread = @mysql_num_rows($result_announce_unread);
				if($num_announce_unread > 0){
						
					header("Location: ".$UTILS_HTTPS_ADDRESS."announcements.php?from=index");
					exit;
				}

				header("Location: ".$UTILS_HTTPS_ADDRESS."building_details.php");
				exit;
			}
		}
		else{
		
			if( $result_array[1] != "" ){
				$login_error_msg = $result_array[1];
			}
		
			$result_array = $login->do_master_account_login($_POST['username'], $_POST['password']);
			if( $result_array[0] === true ){
				
				$master_account = new master_account($_SESSION['master_account_serial']);
				$resident->resident($_SESSION['resident_num']);
				
				// Update 'cpm_stat' table
				require($UTILS_FILE_PATH."library/stat.php");
				add_stat(3);
				
				// Check if password needs to be set
				if($master_account->master_account_allow_password_reset == "Y"){
					
					header("Location: ".$UTILS_HTTPS_ADDRESS."ma_change_details.php");
					exit;
				}
				else{
					
					// Check to see if there are any unread announcements and redirect if necessary
					$total_num_announce_unread = 0;
					$result_ma_assoc_res = $master_account->get_master_account_assoc_residents();
					while($row_ma_assoc_res = @mysql_fetch_array($result_ma_assoc_res)){
	
						$sql_announce_unread = "
						SELECT * 
						FROM cpm_announcements a, cpm_announcements_read ar 
						WHERE 
						a.announce_id=ar.announce_id AND 
						a.rmc_num = ".$row_ma_assoc_res['rmc_num']." AND 
						a.announce_date > '$cut_off_day' AND 
						ar.resident_num = ".$row_ma_assoc_res['resident_num']." AND 
						ar.announce_read = 'N' 
						ORDER BY a.announce_date DESC";
						$result_announce_unread = @mysql_query($sql_announce_unread);
						$num_announce_unread = @mysql_num_rows($result_announce_unread);
						$total_num_announce_unread += $num_announce_unread;
					}
											
					if($total_num_announce_unread > 0){
							
						header("Location: ".$UTILS_HTTPS_ADDRESS."announcements.php?from=index");
						exit;
					}
	
					header("Location: ".$UTILS_HTTPS_ADDRESS."building_details.php");
					exit;
				}
			}
			else{
				
				if( $login_error_msg == "" ){
					$login_error_msg = $result_array[1];
				}
				
				//===========================================
				// Check for content management system login
				//===========================================
				$sql_backend = "
				SELECT p.manager, p.first_name, p.last_name, b.f_name, b.l_name, b.user_id, b.is_sys_admin, b.is_first_logon, b.user_type_id, b.email,
				b.allow_pms, b.allow_rmc, b.allow_residents, b.allow_passwords, b.allow_letters, b.allow_news, b.allow_users, b.allow_report, b.allow_developers, b.password,
				allow_letters_module, allow_announcements, allow_contractor, allow_letters_toggle 
				FROM cpm_backend b LEFT JOIN cpm_pmt_members p ON b.member_id=p.member_id
				WHERE LOWER(b.user_name)='".$_POST['username']."' AND b.password <> '' AND b.password = '".$security->clean_query($crypt->encrypt($UTILS_DB_ENCODE, $_POST['password']))."'";
				$result_backend = @mysql_query($sql_backend);
				$num_backed_users = @mysql_num_rows($result_backend);
				
				if($num_backed_users > 0){
				
					$row_backend = @mysql_fetch_row($result_backend);
				
					$_SESSION['manager'] = $row_backend[0];
					$_SESSION['manager_name'] = $row_backend[1]." ".$row_backend[2];
					$_SESSION['f_name'] = $row_backend[3];
					$_SESSION['l_name'] = $row_backend[4];
					$_SESSION['user_id'] = $row_backend[5];
					$_SESSION['is_sys_admin'] = $row_backend[6];
					$_SESSION['user_name'] = $row_backend[3]." ".$row_backend[4];
					$_SESSION['cms_login'] = session_id();
					$_SESSION['is_first_logon'] = $row_backend[7];
					$_SESSION['user_type_id'] = $row_backend[8];
					$_SESSION['user_email'] = $row_backend[9];
					
					$_SESSION['allow_pms'] = $row_backend[10];
					$_SESSION['allow_rmc'] = $row_backend[11];
					$_SESSION['allow_residents'] = $row_backend[12];
					$_SESSION['allow_passwords'] = $row_backend[13];
					$_SESSION['allow_letters'] = $row_backend[14];
					$_SESSION['allow_letters_toggle'] = $row_backend[23];
					$_SESSION['allow_news'] = $row_backend[15];
					$_SESSION['allow_users'] = $row_backend[16];
					$_SESSION['allow_report'] = $row_backend[17];
					$_SESSION['allow_developers'] = $row_backend[18];
					$_SESSION['allow_letters_module'] = $row_backend[20];
					$_SESSION['allow_announcements'] = $row_backend[21];
					$_SESSION['allow_contractor'] = $row_backend[22];
					
					if ($row_backend['allow_report'] == '1'){
						$sql = "SELECT *
						FROM cpm_report";
						$result_report = @mysql_query($sql);
						$num_rows = @mysql_num_rows($result_report);
						if($num_rows > 0){
							while($row = @mysql_fetch_array($result_report)){
								$_SESSION['report_'.$row['cpm_report_id']] = '1';
							}
						}
						
						$options .= '</table>';
						
						return $options;	
					}
					
					if($site_locality == "remote"){
						
						if($_SESSION['is_first_logon'] == "Y"){
							header("Location: ".$UTILS_HTTPS_ADDRESS."management/change_password.php");
							exit;
						}
						else{
							header("Location: ".$UTILS_HTTPS_ADDRESS."management/index.php");
							exit;
						}
					}
					else{
						
						if($_SESSION['is_first_logon'] == "Y"){
							header("Location: ".$UTILS_HTTPS_ADDRESS."management/change_password.php");
							exit;
						}
						else{
							
							header("Location: ".$UTILS_HTTPS_ADDRESS."management/index.php");
							exit;
						}
					}
					
					exit;
				}
				else{
					
					if( $login_error_msg == "" ){
						$login_error_msg = $result_array[1];
					}
				
					$result_array = $login->do_contractor_login($_POST['username'], $_POST['password']);
					if( $result_array[0] === true ){
						if($_SESSION['contractors_last_login'] == ''){
							header("Location: ".$UTILS_HTTPS_ADDRESS."contractors/change_details.php");
						}else{
							$datetime = new DateTime();
							$today = $datetime->format('Ymd');
							if($_SESSION['contractors_next_change'] == ""){
								header("Location: ".$UTILS_HTTPS_ADDRESS."contractors/change_details.php");
							}elseif($_SESSION['contractors_next_change'] <= $today){
								header("Location: ".$UTILS_HTTPS_ADDRESS."contractors/change_details.php");
							}else{
								header("Location: ".$UTILS_HTTPS_ADDRESS."contractors/contractors.php");
							}
						}
						
						exit;						
					}
					else{
						if( $result_array[1] != "" ){
							$login_error_msg = $result_array[1];
						}
						
						$login_error = $login_error_msg;
						if( $login_error == "" ){
							$login_error = "Incorrect login details";
						}
					}
				}
			}
		}
	}
}

// Forgotten password facility...
elseif($_POST['whichaction'] == "remind"){

	
	$sql_remind = "
	SELECT re.resident_num 
	FROM cpm_residents re, cpm_residents_extra rex, cpm_lookup_residents lre 
	WHERE 
	lre.resident_lookup=re.resident_num AND 
	re.resident_num=rex.resident_num AND 
	rex.email <> '' AND 
	lre.resident_ref = '".$security->clean_query($_POST['forgot_login_id'])."' AND 
	rex.email = '".$security->clean_query($_POST['forgot_email'])."'";
	$result_remind = @mysql_query($sql_remind);
	$resident_found = @mysql_num_rows($result_remind);

	if($resident_found > 0){
	
		$row_remind = @mysql_fetch_row($result_remind);
	
		// Gen password
		$password_unique = "N";
		while($password_unique == "N"){
			$password = $resident->get_rand_id(8, 'ALPHA', 'UPPER');
			$sql = "SELECT password FROM cpm_residents_extra WHERE password = '".$security->clean_query($crypt->encrypt($UTILS_DB_ENCODE, $password))."'";
			$result = mysql_query($sql);
			$password_exists = mysql_num_rows($result);
			if($password_exists < 1){
				$password_unique = "Y";
			}
		}
		$sql2 = "
		UPDATE cpm_residents_extra SET
		is_first_login = 'Y',
		password = '".$security->clean_query($crypt->encrypt($UTILS_DB_ENCODE, $password))."'
		WHERE resident_num = ".$row_remind[0];
		@mysql_query($sql2);
		
		
		// Get unit address
		$sql_ua = "SELECT unit_address_1 FROM cpm_units WHERE resident_num = ".$row_remind[0];
		$result_ua = @mysql_query($sql_ua);
		$row_ua = @mysql_fetch_row($result_ua);
		
		// Set email headers
		$headers = "From: RMG Living Support <customerservice@rmguk.com>\n";
		$headers .= "Reply-To: RMG Living Support <customerservice@rmguk.com>";
		
		// Send email password
$body_pw = "
Below is your password for the RMG Living website - www.rmgliving.co.uk
This will give you access for your property '".$row_ua[0]."':

".$password."

Once logged in, you can change you password to something more memorable in the 'My Account' section. If you experience problems accessing the RMG Living website, please contact our Customer Services Team via customerservice@rmguk.com

Kind Regards,
RMG Living Support
	
(This email was generated automatically.)
";
mail($_POST['forgot_email'],"RMG Living",$body_pw,$headers) || $remind_success = "N";
		
		
		if($remind_success == "N"){
			$remind_error = "There was a problem sending your details.";
		}
		else{
			$remind_success = "Y";
		}
	
	}
	else{
		
		// Now check the master accounts...
		$sql_ma = "
		SELECT * 
		FROM cpm_master_accounts ma 
		WHERE 
		ma.master_account_username IS NOT NULL AND 
		ma.master_account_username <> '' AND 
		ma.master_account_username = '".$security->clean_query($_POST['forgot_login_id'])."' AND 
		ma.master_account_email IS NOT NULL AND 
		ma.master_account_email <> '' AND 
		ma.master_account_email = '".$security->clean_query($_POST['forgot_email'])."' 
		";
		$result_ma = @mysql_query($sql_ma);
		$num_ma = @mysql_num_rows($result_ma);
		if($num_ma > 0){
			
			$row_ma = @mysql_fetch_array($result_ma);
			$master_account->master_account($row_ma['master_account_id'],"id");
			if( $master_account->send_forgot_password($_POST['forgot_email']) === true ){
				$remind_success = "Y";
			}
			else{
				$remind_success = "";
				$remind_error = "There was a problem sending your details.";
			}
		}
		else{
			$remind_success = "";
			$remind_error = "Details not recognised.";
		}
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>RMG Living</title>
<meta name="description" content="RMG Living is a portal for lessees to access information about their management company">
<meta name="keywords" content="rmg, residential management group, cpm asset management, management company, property management">
<link href="/css/common.css" rel="stylesheet" type="text/css">
<!--[if lte IE 8]> 
<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if lte IE 7]> 
<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
<![endif]-->

<script type="text/javascript">
function what_is_lessee_id(){
	window.open("what_is_a_lessee_id.htm","whatlesseewin","height=150, width=320, status=no, menubar=no");
}
</script>
<? require_once($UTILS_FILE_PATH."includes/analytics.php");?>
</head>
<body>

	<div id="wrapper">
	
		<? require_once($UTILS_FILE_PATH."includes/header.php");?>
		
		<div id="content" class="clearfix">

			<table width="760" cellspacing="0">
				<tr>
					<td>&nbsp;</td>
					<td style="text-align:right;" nowrap="nowrap"><? if(!empty($_SESSION['resident_session'])){?><a href="index.php?logoff=Y" class="crumbs">Log Off</a><? }?></td>
				</tr>
			</table>
			
			
			<div id="home_left_block">						
				<img src="images/index/welcome_box_title2.jpg" alt="Welcome to RMG Living" />
				<p>RMG Living provides Property Owners with access to their statement of account, information about their development, helpful resources for property management and the ability to make secure, online payments.</p>
			</div>		
			
			
			<div id="home_middle_block">
			
				<table width="240" border="0" cellspacing="0" cellpadding="0" style="margin:7px 15px 7px 15px;">
					<tr>
						<td align="left" valign="middle" style="vertical-align:middle;"><img src="images/index/login_symbol.jpg" width="22" height="32" style="vertical-align:middle;" alt="Login">&nbsp;&nbsp;<span class="box_title">Login</span></td>
					</tr>
				</table>
				
				<div class="clearfix" style="background-color:#f1f1f1; padding:15px; height:153px;">
							
					<?
					if($site_inactive == "Y" && $_POST['whichaction'] == "login"){
						
						?>
						<p>The RMG Living website is currently unavailable.</p>
						<?
			
					}
					else{
					
						?>
						
						<form method="post" name="login_form" action="<?=$UTILS_HTTPS_ADDRESS?>index.php">
							<table width="208" border="0" align="left" cellpadding="0" cellspacing="0">
								<tr>
									<td colspan="2" style="padding:0px 0px 10px 0px;">Enter your login details into the fields below and click 'Login'.</td>
								</tr>
								<?
								// Display login error
								if($login_error != ""){
									
									?>
									<tr>
										<td colspan="2" style="padding:0px 0px 9px 0px;"><span class="msg_fail"><b><?=$login_error?></b></span></td>
									</tr>
									<?
								}
								?>
								<tr>
									<td width="70" nowrap style="padding-bottom:3px; vertical-align:middle;"><span class="text036">Login Id:</span>&nbsp;</td>
									<td width="138" nowrap style="padding-bottom:3px; vertical-align:middle;"><input name="username" type="text" id="username" style="width:93px; padding:2px; float:left;"> <a href="Javascript:;" onClick="what_is_lessee_id();return false;" style="margin:0; padding:0; float:right;" title="What is a Lessee Id?"><img src="images/index/what_is_lessee_id_button.jpg" style="margin:0px 10px 0px 4px; float:right;" border="0"></a></td>
									</tr>
								<tr>
									<td nowrap style=" vertical-align:middle;"><span class="text036">Password:</span>&nbsp;</td>
									<td style=" vertical-align:middle;"><input name="password" type="password" id="password" style="width:120px; padding:2px;"></td>
									</tr>
								<tr>
									<td colspan="2" style="padding-top:10px;"><input name="login_button" type="image" onClick="document.forms['login_form'].submit();" src="images/index/login_button.jpg" alt="Click to login to RMG Living"></td>
								</tr>
							</table>
							<input type="hidden" name="whichaction" value="login">
						</form>
					
						<?
					}
					?>	
					
				</div>
				
				<div class="clearfix" style="background-color:#f1f1f1; padding:0px 15px 10px 15px; height:85px;">	
					<b><span class="subt036">Forgotten your login details ?</span></b>
					<p>If you have lost either your username or password to your account, <a href="forgot_details.php">click here</a>.</p>	
				</div>
				
				<div id="rewards_block">
					<a href="http://rmgltd.co.uk/rewards" target="_blank" title="RMG Rewards..."><img src="images/rewards_home.jpg" border="0" /></a>
				</div>
				
			</div>
			
					
			<div id="home_right_block">
				
				<div id="login_request_block">
					<table width="240" border="0" cellspacing="0" cellpadding="0" style="margin:7px 15px 7px 15px;">
						<tr>
							<td align="left" valign="middle" style="vertical-align:middle;height:25px;"><img src="images/index/registration_symbol.jpg" width="22" height="22" style="vertical-align:middle;" alt="Request a login" />&nbsp;&nbsp;<span class="box_title">Login Request </span></td>
						</tr>
					</table>
					<div class="clearfix" style="background-color:#f1f1f1; padding:5px 15px 15px 15px; height:95px;">
						<p>If RMG is the managing agent for your property and you have not received login details for this site, please click the button below.</p>
						<input name="register_button" type="image" id="register_button" onClick="location.href='<?=$UTILS_HTTPS_ADDRESS?>request.php';" src="images/index/register_button.jpg" alt="Click to request login details to RMG Living" />
					</div>
				</div>
							
				<div id="help_block">
					<table width="240" border="0" cellspacing="0" cellpadding="0" style="margin:7px 15px 7px 15px;">
						<tr>
							<td align="left" valign="middle" style="vertical-align:middle;height:25px;"><img src="images/index/help_symbol.jpg" width="22" height="22" style="vertical-align:middle;" alt="Support" />&nbsp;&nbsp;<span class="box_title">Help</span></td>
						</tr>
					</table>
					<div class="clearfix" style="background-color:#f1f1f1; padding:5px 15px 15px 15px; height:94px;">
						<p>If you are experiencing problems with the website, please click the link below to email our technical support team.</p>
						<a href="contact_us.php" class="link036"><strong>Technical support</strong></a>
					</div>
				</div>
												
				<div id="twitter_block">
					<a href="http://twitter.com/rmgltd" target="_blank" title="Follow RMG on twitter..."><img src="images/twitter_home.jpg" border="0" /></a>
				</div>
			
			</div>
							
		
		</div>
	
		<? require_once($UTILS_FILE_PATH."includes/footer.php");?>

	</div>

</body>
</html>
