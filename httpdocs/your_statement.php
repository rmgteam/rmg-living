<?
require("utils.php");
require_once($UTILS_CLASS_PATH."rmc.class.php");
require_once($UTILS_CLASS_PATH."resident.class.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."security.class.php");
require_once($UTILS_CLASS_PATH."subsidiary.class.php");
require_once($UTILS_CLASS_PATH."order.class.php");
require_once($UTILS_CLASS_PATH."unit.class.php");
$resident = new resident($_SESSION['resident_ref'],"ref");
$ct_order = new order;
$security = new security;
$website = new website;
$rmc = new rmc;
$unit = new unit;
$unit->set_unit($resident->resident_num);
Global $UTILS_TEL_MAIN;

// Determine if allowed access into 'your community' section
$website->allow_community_access();
$rmc->set_rmc($_SESSION['rmc_num']);
$subsidiary = new subsidiary($rmc->rmc['subsidiary_id']);


// Set date for ECS query
if( $_REQUEST['date_from'] == "" || preg_match("/^([0-9]{2})/([0-9]{2})/([0-9]{4})$/", $_REQUEST['date_from']) !== 1 ){
	$date_from_ts = (time()-(86400*365));
	$_REQUEST['date_from'] = date("d/m/Y", $date_from_ts);
}


// Statement to show for Developer accounts...
if($_SESSION['is_developer'] == "Y" || $_SESSION['is_demo_account'] == "Y"){
	
	$resident_ref = "999901100101";	
	$group = "RMG Live";
}
else{
	if($resident->is_resident_director == "Y"){
		$dir_assoc_resident = new resident($resident->director_assoc_resident_num);
		$resident_ref = $dir_assoc_resident->resident_ref;
		$group = $subsidiary->subsidiary_ecs_login_group;
	}
	else{
		$resident_ref = $_SESSION['resident_ref'];
		$group = $subsidiary->subsidiary_ecs_login_group;
	}
}


// Test data (uncomment a row below to test)
// $resident_ref = "111001140801"; // breaks
// $resident_ref = "111001101001"; // good
// $resident_ref = "105301100201"; // lots of transactions

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>RMG Living - Statement</title>
<link href="/css/reset.css" rel="stylesheet" type="text/css" />
<link href="/css/common.css" rel="stylesheet" type="text/css" />
<link href="/css/statement_print.css" rel="stylesheet" type="text/css" media="print" />
<!--[if lte IE 8]> 
<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if lte IE 7]> 
<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
<![endif]-->
<? if(strtolower($resident->resident_status) != "debt collect"){?>
<script src="/library/jscript/functions/pay_online.js" type="text/javascript"></script>
<? }?>
<script type="text/javascript" src="/library/jscript/jquery-1.4.2.min.js"></script>

<script type="text/javascript">
var selected_type = "account";
var grent_due = 0;
var s_charge_due = 0;

function get_statement(type){

	selected_type = type;

	$(".statement_links").css('visibility', 'hidden');
	$("#stat_table").hide();
	$("#statement_date_chooser").hide();
	$("#header_row_account").hide();
	$("#header_row_arrears").hide();
	$("#msg_row_account").hide();
	$("#msg_row_arrears").hide();

	$(".statement_data_row").remove();
	$(".statement_msg_row").remove();
	$("#statement_totals_row").remove();

	if(type == 'arrears'){
		$("#title_arrears").show();
		$("#title_account").hide();
	}
	else{
		$("#title_arrears").hide();
		$("#title_account").show();
	}

	if('<?=strtolower($resident->resident_status)?>' == "debt collect"){
		$("#pay_online_div").hide();
		$("#pay_by_card_div").hide();
		$("#debt_collect_msg").hide();
		$("#statement_loader").fadeIn();
	}
	else{
		$("#debt_collect_msg").hide();
		$("#statement_loader").fadeIn();
	}


	$.post("get_statement.php?date_from=" + $("#date_from").val() + "&type=" + type + "&resident_ref=<?=$resident_ref?>&group=<?=$group?>",

		function(data){

			if(data){

				// Need this here again becasue requests are asynchronous
				$(".statement_data_row").remove();
				$(".statement_msg_row").remove();
				$("#statement_totals_row").remove();

				$("#debt_collect_msg").before(data);

				if(type == 'arrears'){
					$("#header_row_arrears").show();
					$("#header_row_account").hide();
					$("#msg_row_arrears").show();
					$("#msg_row_account").hide();
				}
				else{
					$("#header_row_arrears").hide();
					$("#header_row_account").show();
					$("#msg_row_arrears").hide();
					$("#msg_row_account").show();
				}
				$("#statement_date_chooser").show();
			}
			$("#statement_loader").hide();
			$("#stat_table").fadeIn('slow');
			$(".statement_links").css('visibility', 'visible');
		},
		"html"
	);
}

function filter_statement(){
	get_statement( selected_type );
}

$(document).ready(function() {
	get_statement('account');
});

</script>
</head>
<body>
	
	<div id="wrapper">
	
		<? require_once($UTILS_FILE_PATH."includes/header.php");?>
		
		<div id="content">
		
			<table width="760" cellspacing="0" id="breadcrumb_table">
				<tr>
					<td nowrap="nowrap"><a href="/building_details.php">Your Community</a> &gt; Statement</td>
					<td style="text-align:right;"><? if(!empty($_SESSION['resident_session'])){?><a href="index.php?logoff=Y" class="crumbs">Log Off</a><? }?></td>
				</tr>
			</table>

			<table width="760" cellspacing="0" style="clear:both; margin-top:13px;" id="content_box_1_top">
				<tr>
					<td width="15"><img src="/images/dblue_box_top_left_corner.jpg" width="15" height="33" /></td>
					<td width="730" style="border-top:1px solid #003366; background-color:#416CA0; vertical-align:middle"><img src="/images/make_a_payment/make_a_payment_symbol.jpg" width="22" height="22" style="vertical-align:middle;" />&nbsp;&nbsp;<span class="box_title">Your Statement</span></td>
					<td width="15" style="text-align:right;"><img src="/images/dblue_box_top_right_corner.jpg" width="15" height="33" /></td>
				</tr>
			</table>
			
    			
			<div class="content_box_1">
    			
    			<div class="statement_links"><a href="Javascript:;" onclick="get_statement('account');return false;">Statement of Account</a>&nbsp;|&nbsp;<a href="Javascript:;" onclick="get_statement('arrears');return false;">Arrears Statement</a></div>
    							
				<h3 style="margin:20px 0 0 0; display:none;" id="title_arrears">Arrears Statement</h3>
				<h3 style="margin:20px 0 0 0;" id="title_account">Statement of Account</h3>
				<?
				if($unit->unit['unit_description'] != ""){
					$unit_name = "(".stripslashes($unit->unit['unit_description']).")";
				}
				elseif($unit->unit['unit_address_1'] != ""){
					$unit_name = "(".stripslashes($unit->unit['unit_address_1']).")";
				}
				?>
				<div style="margin-top:7px;float:left; position:relative;width:725px; margin-bottom:15px;">
					<p style="font-size:13px;float:left;width:450px;"><?=$resident->resident_name?>&nbsp;<?=$unit_name?></p>
					<p style="font-size:13px;float:right; width:250px;text-align:right;">Ref:&nbsp;<strong><?=$resident->resident_ref?></strong></p>
				</div>
				
    			<p id="msg_row_account" style="margin-top:10px;">This statement lists all the transactions that have taken place on your account since <?=$_REQUEST['date_from']?>.</p>
    			<p id="msg_row_arrears" style="margin-top:10px;">This statement lists all the unsettled transactions on this account since <?=$_REQUEST['date_from']?>.</p>
    			
				<div id="statement_loader">
					<br /><br /><br /><br /><p style="font-size:14px; color:#333;text-align:center;">Retrieving statement details... please wait</p><br />
					<img src="images/ajax-loader.gif" border="0" />
				</div>
				
				<table width="728" border="0" cellspacing="0" cellpadding="0" id="stat_table">
					
					<tr id="header_row_arrears" style="display:none;">
						<th class="first" style="width:90px;">Date</th>
						<th>Description</th>
						<th style="width:90px;">Due</th>
						<th style="text-align:right; width:75px;" nowrap="nowrap">Charged(&pound;)</th>
						<th style="text-align:right; width:85px;" nowrap="nowrap">Received(&pound;)</th>
						<th style="text-align:right; width:75px;" nowrap="nowrap">Owing(&pound;)</th>
						<th style="text-align:right; width:75px;" nowrap="nowrap">Balance(&pound;)</th>
					</tr>
				
					<tr id="header_row_account">
						<th class="first" style="width:90px;">Date</th>
						<th colspan="2">Description</th>
						<th style="width:90px;">Due</th>
						<th style="text-align:right; width:75px;" nowrap="nowrap">Charged(&pound;)</th>
						<th style="text-align:right; width:85px;" nowrap="nowrap">Received(&pound;)</th>
						<th style="text-align:right; width:75px;" nowrap="nowrap">Balance(&pound;)</th>
					</tr>
					
					<tr id="debt_collect_msg">
						<td class="first" style="color:#CC0000;" colspan="6">Online payment is not available at this time. Please contact Customer Services on <?=$UTILS_TEL_MAIN;?>.</td>
					</tr>
										
				</table>
				<div id="statement_date_chooser" class="clearfix" style="vertical-align:top; width:724px; padding-left:4px;">
					<div style="float:left; vertical-align:top; width:520px; margin-top:10px;">To view  transactions after a certain date, please enter it below (dd/mm/yyyy)<br />
						<form method="post" style="padding:5px 0 0 0; margin:0;"><input type="text" id="date_from" name="date_from" size="11" maxlength="10" style="padding:2px; float:left;" value="<?=$_REQUEST['date_from']?>" /> <a href="Javascript:;" onclick="filter_statement();return false;"><img src="/images/your_statement/view_button.jpg" style="margin-left:6px;" border="0" /></a></form>
					</div>
					<div id="pay_online_div" style="float:right; vertical-align:top; width:200px;">
						<img src="/images/make_a_payment/cards.jpg" alt="Cards accepted - Visa, Mastercard, Maestro" style="float:right; margin-top:15px;" /><a href="#" onclick="pay_online(grent_due, s_charge_due,'<?=$rmc->rmc['payment_plan']?>');return false;" style="float:right;clear:right;margin-top:10px;"><img src="/images/make_a_payment/make_a_payment_button.jpg" alt="Click to Make a Payment" width="120" height="21" border="0" /></a>
					</div>
				</div>
				
    						
    			<p>&nbsp;</p>
				
				<div id="pay_by_card_div">
					<h6>Paying by Card</h6>
					<p>We have teamed up with SecPay to create this Online Payment facility. This is the quickest method of making a payment to your service charge account. The following handling fees are charged:</p>
					<table width="348" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
						<?
						$result_ct_order = $ct_order->get_order_card_type_list();
						while( $row_ct_order = @mysql_fetch_array($result_ct_order) ){
							
							?>        
							<tr>                  
								<td style="vertical-align:top; font-weight:bold;"><?=stripslashes( $row_ct_order['order_card_type_label'] )?></td>
								<td width="100" style="vertical-align:top"><?=$ct_order->get_card_type_charge_words($row_ct_order['order_card_type_id'])?></td>
							</tr>
							<?
						}
						?>
					</table>
					
				</div>
				
				<div id="pay_by_phone_div">
					<br />
					<h6>Paying by Phone</h6>
					<p>To make a payment over the phone using either of the above mentioned cards, please call our switchboard on <br /><strong><?=$UTILS_TEL_MAIN;?></strong> and ask to speak to our Customer Services department.</p>
				</div>
                
                <div id="pay_by_bank_div">
					<br />
					<h6>Paying by Bank Transfer</h6>
                    <? if( $rmc->rmc['rmc_ref'] == "779-INC" && $rmc->rmc['subsidiary_id'] == "6" ){?>
                    <p style="margin-top:8px;">To make a <span style="font-weight:bold;">Ground Rent Payments ONLY</span> use the following:</p>
                    <table width="348" border="0" cellspacing="0" cellpadding="0" style="margin-top:7px;margin-bottom:12px;">
                    	<tr>
                        	<td style="font-weight:bold;width:100px;">Account No.</td>
                            <td>02549399</td>
                        </tr>
                        <tr>
                        	<td style="font-weight:bold;">Sort Code</td>
                            <td>23-83-97</td>
                        </tr>
                    </table>
                    <p style="margin-top:12px;">To make a <span style="font-weight:bold;">Service Charge Payments ONLY</span> use the following:</p>
                    <table width="348" border="0" cellspacing="0" cellpadding="0" style="margin-top:7px;">
                    	<tr>
                        	<td style="font-weight:bold;width:100px;">Account No.</td>
                            <td>02549043</td>
                        </tr>
                        <tr>
                        	<td style="font-weight:bold;">Sort Code</td>
                            <td>23-83-97</td>
                        </tr>
                    </table>
                    <? }else{?>
                    <p>To make a payment via bank transfer, please use the following details:</p>
                    <table width="348" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
                    	<tr>
                        	<td style="font-weight:bold;width:100px;">Account No.</td>
                            <td><?=$subsidiary->subsidiary_cra_acc_num?></td>
                        </tr>
                        <tr>
                        	<td style="font-weight:bold;">Sort Code</td>
                            <td><?=$subsidiary->subsidiary_cra_sort_code?></td>
                        </tr>
                    </table>
                    <? }?>
				</div>
				
				<div id="pay_by_cheque_div">
					<br />
					<h6>Paying by Cheque</h6>
					<p>
					<? 
					// Work out who cheques are payable to
					if( preg_match('/^8/', $rmc->rmc_ref) > 0 && trim($rmc->property_manager) == "Ground Rent Only" && trim($rmc->rmc_freeholder_name) != ""){
						$payable_to = "<strong>".stripslashes($rmc->rmc_freeholder_name)."</strong>";
					}
					else{
						$payable_to = "<strong>".stripslashes($rmc->rmc_name)."</strong> or <strong>RMG</strong>";
					}
					?>
					Cheques must be made payable to <?=$payable_to?>.<br>If not already printed on the cheque, please write 'A/C Payee Only'. Please also write your name and address of the property on the back of the cheque and send your payment along with the tear-off remittance slip (from your Service Charge Demand) to the  address below. 
					</p>		
					<br />
					<p>Send cheques to the following address: </p>
					<p>
						Accounts Receivable Dept.<br />
						Residential Management Group Ltd<br />
						RMG House<br />
						Essex Road<br />
						Hoddesdon<br />
						Herts<br />
						EN11 0DR
					</p>
				</div>
				
			</div>

		</div>


	<? require_once($UTILS_FILE_PATH."includes/footer.php");?>

	</div>


</body>
</html>
