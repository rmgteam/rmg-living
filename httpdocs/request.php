<?
require("utils.php");
require($UTILS_FILE_PATH."library/functions/valid_email_check.php");
Global $UTILS_TEL_MAIN;
// Send details to support team
if($_REQUEST['whichaction'] == "login_request"){

	// Check to see if Company is NLA
	$sql_nla = "SELECT * FROM cpm_rmcs rmc, cpm_residents res WHERE rmc.rmc_num=res.rmc_num AND res.resident_num = ".trim($_REQUEST['lessee_id']);
	$result_nla = @mysql_query($sql_nla);
	$row_nla = @mysql_fetch_array($result_nla);
	if($row_nla['rmc_status'] == "Inactive" || $row_nla['rmc_is_active'] == "0"){
		
		$send = "Y";
		
		// Set email headers
		$headers = "From: RMG Living Support <customerservice@rmguk.com>\n";
		$headers .= "Reply-To: RMG Living Support <customerservice@rmguk.com>";
		
		$body_nla = "
Dear Sir/Madam,

It appears that RMG no longer act on behalf of your Management Company - ".$row_nla['rmc_name'].". If you require further information regarding this, please contact our Customer Services department on '.$UTILS_TEL_MAIN.'. 


Kind Regards,
RMG Living Support
	
(This email was been generated automatically.)
";
		mail($_REQUEST['email'], "RMG Living - No Longer Acting", $body_nla, $headers) or $send = "N";

	}
	
		
	// Otherwise, proceed with login request
	if($login_exists == 0 || ($login_exists > 0 && $remind_success == "N")){
		
			
		$send_error = "Please complete all required fields.";
		$send = "Y";
			
		if($_REQUEST['from'] == "cpm"){
			$sql = "SELECT res.resident_name, res.resident_address_1, res.resident_address_city, res.resident_address_postcode, res.resident_address_country, rex.tel, rex.email, res.resident_address_2 FROM cpm_residents res, cpm_residents_extra rex WHERE res.resident_num = rex.resident_num AND res.resident_num = ".$_REQUEST['lessee_id'];
			$result = mysql_query($sql);
			$row = mysql_fetch_row($result) or $send = "N";
			$_REQUEST['rmc_name'] = "None supplied";
			$_REQUEST['name'] = $row[0];
			$_REQUEST['address_1'] = $row[1];
			$_REQUEST['address_2'] = $row[7];
			$_REQUEST['town'] = $row[2];
			$_REQUEST['postcode'] = $row[3];
			$_REQUEST['country'] = $row[4];
			$_REQUEST['tel'] = $row[5];
			$_REQUEST['email'] = $row[6];
			$requested_by = $_SESSION['f_name']." ".$_SESSION['l_name'];
		}
		else{		
			if($_REQUEST['unit_address_1'] == ""){$send = "N";}
			if($_REQUEST['rmc_name'] == ""){$send = "N";}
			if($_REQUEST['name'] == ""){$send = "N";}
			if($_REQUEST['email'] == ""){$send = "N";}
			if($_REQUEST['address_1'] == ""){$send = "N";}
			if($_REQUEST['town'] == ""){$send = "N";}
			if($_REQUEST['postcode'] == ""){$send = "N";}
			if(valid_email_check($_REQUEST['email']) == false){$send = "N";$send_error = "Please provide a valid email address.";}
			$requested_by = "(Web User)";
		}
		
		// Send email if form details are correct
		if($send == "Y"){
		
$message = "
REQUESTED BY: ".$requested_by."

Login Id: ".$_REQUEST['lessee_id']."
Property: ".$_REQUEST['unit_address_1']."
Man. Co.: ".$_REQUEST['rmc_name']."

Name: ".$_REQUEST['name']."
Address 1: ".$_REQUEST['address_1']."
Address 2: ".$_REQUEST['address_2']."
Town/City: ".$_REQUEST['town']."
Postcode: ".$_REQUEST['postcode']."
Country: ".$_REQUEST['country']."

Tel: ".$_REQUEST['tel']."
Email: ".$_REQUEST['email']."
";
			mail($UTILS_LOGIN_REQUEST_EMAIL,"RMG Living Login Request",$message,"From:".$_REQUEST['email']);
			
		}
			
		if($_REQUEST['from'] == "cpm"){
			print "
			<script language=Javascript>
			alert(\"Your request has been sent to the RMG Living Support Team.\");
			</script>
			";
			exit;
		}
	}
	else{
		if($_REQUEST['from'] == "cpm"){
			print "
			<script language=Javascript>
			alert(\"Your request could NOT been sent to the RMG Living Support Team.\n\nIf this problem persists, pleaase contact them directly on customerservice@rmguk.com\");
			</script>
			";
			exit;
		}
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><!-- InstanceBegin template="/Templates/main.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- InstanceBeginEditable name="doctitle" -->
<title>RMG Living - Login Request</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<link href="styles.css" rel="stylesheet" type="text/css">
<!--[if lte IE 8]> 
<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if lte IE 7]> 
<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
<![endif]-->
</head>
<body>
<? if($_SESSION['is_demo_account'] == "Y" && $_SESSION['is_developer'] != "Y"){?>
<form method="post" action="/building_details.php" style="margin:0; padding:0;">
<input type="hidden" name="a" value="cl">
<input type="hidden" name="s" value="gkfcbk45cgtf5kngn7kns5cg7snk5g7nsv5ng">
<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" style="margin:0; padding:0;">
	<tr>
		<td style="padding:4px;">Change Account:&nbsp;<input type="text" name="change_lessee" value="<?=$_SESSION['resident_ref']?>" size="20"> <input type="submit" name="submit_button" value="Change"></td>
	</tr>
</table>
</form>
<? }?>
<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#E9EEF4" style="border-bottom:1px solid #c1d1e1;"><div align="center"><img src="images/templates/header_bar_grad2.jpg" alt=""></div></td>
  </tr>
  <tr>
    <td style="border-top:1px solid #ffffff;border-bottom:1px solid #ffffff;">
	<table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="180"><a href="<? print $UTILS_HTTPS_ADDRESS;if( isset($_SESSION['login']) && $_SESSION['login'] != ""){print "/building_details.php";}else{print "/index.php";}?>"><img src="/images/templates/rmg_living_logo.jpg" style="margin-top:10px;" width="180" alt="RMG Living" border="0"></a></td>
        <td width="580" align="right">
		<?
		// User logged in is a 'developer', show any developer specific logos, or default to normal ones...
		if($_SESSION['is_developer'] == "Y"){
			
			$sql = "SELECT developer_id FROM cpm_residents_extra WHERE resident_num = ".$_SESSION['resident_num'];
			$result = @mysql_query($sql);
			$row = @mysql_fetch_row($result);
			if($row[0] != "" && file_exists($UTILS_FILE_PATH.'/images/developers/'.$row[0].'/header_'.$row[0].'.jpg') ){
				
				print '<img src="/images/developers/'.$row[0].'/header_'.$row[0].'.jpg" alt="RMG Living" width="580" height="86" BORDER=0>';
			}
			else{
				
				print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
			}
		}
		else{
		
			// Check to see if the Man. Co. has a developer association, if so, set developer specific images, or default to normal images...
			if( isset($_SESSION['login']) && $_SESSION['login'] != "" && $_SESSION['rmc_num'] != ""){
			
				$sql = "SELECT developer_id FROM cpm_rmcs_extra WHERE rmc_num = ".$_SESSION['rmc_num'];
				$result = @mysql_query($sql);
				$row = @mysql_fetch_row($result);
				if( $row[0] != "" && file_exists($UTILS_FILE_PATH.'/images/developers/'.$row[0].'/header_'.$row[0].'.jpg') ){
					
					print '<img src="/images/developers/'.$row[0].'/header_'.$row[0].'.jpg" alt="RMG Living" width="580" height="86" BORDER=0>';
				}
				else{
					
					print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
				}
			}
			else{
				
				print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
			}
		}
		?> 
		
		<?
		/*
		if( isset($_SESSION['login']) && $_SESSION['login'] != "" && $_SESSION['subsidiary_id'] != ""){
			$sql = "SELECT subsidiary_header_image FROM cpm_subsidiary WHERE subsidiary_id = ".$_SESSION['subsidiary_id'];
			$result = @mysql_query($sql);
			$row = @mysql_fetch_row($result);
			if($row[0] != "" ){ 
				?>
				<img src="/images/templates/header_image_w_logo.jpg" width="580" height="86" border="0" alt="RMG Living">
				<img src="/images/logos/<?=$row[0]?>" style="position:absolute; left:50%; margin-left:245px; margin-top:23px; z-index:10;" border="0" >
				<?
			}
			else{
				?>
				<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">
				<?
			}
		}
		else{
			?>
			<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">
			<?
		}
		*/
		?>
		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
  <td>
  <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
  <tr>
    <td height="10" align="center" valign="bottom" bgcolor="#E9EEF4" style="border-bottom:1px solid #c1d1e1;border-top:1px solid #c1d1e1;"><img src="images/templates/header_bar_grad2.jpg" alt=""></td>
  </tr>
  <tr>
  <td height="8" align="center" valign="top" style="border-top:1px solid #ffffff;"><img src="images/templates/header_bar_grad3.jpg" alt=""></td>
  </tr>
  </table>
  </td>
  </tr>
 
  <tr>
    <td height="4"></td>
  </tr>
  <tr>
    <td><table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="28" valign="top">
	
	<table width="760" cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td>
	<!-- InstanceBeginEditable name="breadcrumbs" --><a href="index.php" class="crumbs">Home</a>&nbsp;>&nbsp;Login Request<!-- InstanceEndEditable -->
	</td>
	<td style="text-align:right;" nowrap>
	<? if(!empty($_SESSION['resident_session'])){?><a href="index.php?logoff=Y" class="crumbs">Log Off</a><? }?>
	</td>
	</tr>
	</table>
	
	</td>
  </tr>
</table>
</td>
  </tr>
  <tr>
    <td><!-- InstanceBeginEditable name="body" -->
      <table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td style="border-bottom:1px solid #003366;"><table width="760" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="15"><img src="images/dblue_box_top_left_corner.jpg" width="15" height="33"></td>
              <td width="730" bgcolor="#416CA0" style="border-top:1px solid #003366; vertical-align:middle"><img src="images/register/register_symbol.jpg" width="22" height="22" style="vertical-align:middle;">&nbsp;&nbsp;<span class="box_title">Login Request</span></td>
              <td width="15" align="right"><img src="images/dblue_box_top_right_corner.jpg" width="15" height="33"></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td><table width="760" border="0" cellspacing="0" cellpadding="0" style="border-left:1px solid #7ea0bd;border-right:1px solid #7ea0bd;border-top:1px solid #7ea0bd;border-bottom:1px solid #cccccc;">
            <tr>
              <td style="border-bottom:3px solid #7ea0bd;"><table width="758" border="0" cellspacing="0" cellpadding="15">
                <tr>
                  <td>                    If you are an existing customer of RMG and require a set of login details for your property, please confirm your details below and submit the form. Once your request has been received we will issue your login details by first-class post. If you own more than one property, you will need to submit a login request for each property.</td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td bgcolor="#F5F7FB"><table width="758" border="0" cellspacing="0" cellpadding="15">
                <tr>
                  <td>
				  
				  <form method="post" action="<?=$UTILS_HTTPS_ADDRESS?>request.php">
				  <table width="728" border="0" cellspacing="0" cellpadding="2">
                    <? if($_REQUEST['whichaction'] == "login_request" && $send == "N"){?>
					<tr>
                      <td colspan="5" style="vertical-align:middle;"><img src="images/del_16.gif" width="16" height="16" style="vertical-align:middle;">&nbsp;&nbsp;<span class="msg_fail"><?=$send_error?></span></td>
                    </tr>
                    <tr>
                      <td colspan="5" height="10"></td>
                    </tr>
					<? }?>
					<? if($_REQUEST['whichaction'] == "login_request" && $send == "Y"){?>
					<tr>
					  <td colspan="5"><table width="720" border="0" cellspacing="0" cellpadding="0">
					  <tr>
						<td width="28" style="vertical-align:middle;"><img src="images/tick_20.gif" width="20" height="20" style="vertical-align:middle;"></td>
						<td width="692" style="vertical-align:middle;"><span class="msg_success">Your request has been sent. Please allow 5 working days for your details to arrive by post. To request login details for an additional property, please re-submit the form below with the new lessee id and property address.</span></td>
					  </tr>
					</table>
					</td>
					  </tr>
                    <tr>
                      <td colspan="5" height="10"></td>
                    </tr>
					<? }?>
                    <tr>
                      <td colspan="2">Fields marked with * must be completed. </td>
                      <td>&nbsp;</td>
                      <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="2">&nbsp;</td>
                      <td>&nbsp;</td>
                      <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="2"><b><span class="subt036">Property details</span></b></td>
                      <td width="32">&nbsp;</td>
                      <td colspan="2"><b><span class="subt036">Correspondence address</span></b></td>
                      </tr>
                    <tr>
                      <td colspan="5" height="5"></td>
                      </tr>
                    <tr>
                      <td width="144">Lessee Id</td>
                      <td width="205"><input tabindex="1" name="lessee_id" type="text" id="lessee_id" size="15" value="<?=$_REQUEST['lessee_id']?>"></td>
                      <td>&nbsp;</td>
                      <td width="78">Address * </td>
                      <td width="249"><input tabindex="7" name="address_1" type="text" id="address_1" size="30" value="<?=$_REQUEST['address_1']?>"></td>
                    </tr>
                    <tr>
                      <td>Property * </td>
                      <td><input tabindex="2" name="unit_address_1" type="text" id="unit_address_1" size="30" value="<?=$_REQUEST['unit_address_1']?>"></td>
                      <td>&nbsp;</td>
                      <td>Town/City * </td>
                      <td><input tabindex="8" name="town" type="text" id="town" value="<?=$_REQUEST['town']?>"></td>
                    </tr>
                    <tr>
                      <td>Management company * </td>
                      <td><input tabindex="3" name="rmc_name" type="text" id="rmc_name" size="30" value="<?=$_REQUEST['rmc_name']?>"></td>
                      <td>&nbsp;</td>
                      <td>Postcode * </td>
                      <td><input tabindex="9" name="postcode" type="text" id="postcode" size="10" value="<?=$_REQUEST['postcode']?>"></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>Country</td>
                      <td><input tabindex="10" name="country" type="text" id="country" size="30" value="<?=$_REQUEST['country']?>"></td>
                    </tr>
                    <tr>
                      <td><b><span class="subt036">Contact details</span></b></td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td colspan="5" height="5"></td>
                      </tr>
                    <tr>
                      <td>Your name * </td>
                      <td><input tabindex="4" name="name" type="text" id="name" size="30" value="<?=$_REQUEST['name']?>"></td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>Telephone</td>
                      <td><input tabindex="5" name="tel" type="text" id="tel" size="20" value="<?=$_REQUEST['tel']?>"></td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>Email * </td>
                      <td><input tabindex="6" name="email" type="text" id="email" size="30" value="<?=$_REQUEST['email']?>"></td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td><input name="submit_button" type="image" src="images/register/submit_button.jpg" width="65" height="20" border="0"  onClick="document.forms[0].submit();"></td>
                    </tr>
					<tr>
                      <td colspan="5" height="10"></td>
                      </tr>
                  </table>
				  <input type="hidden" name="whichaction" id="whichaction" value="login_request">
				  </form>
				  
				  </td>
                </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
		<tr><td height="5" bgcolor="#416CA0" style="border:1px solid #003366;"><img src="images/spacer.gif" alt=""></td>
		</tr>
      </table>
    <!-- InstanceEndEditable --></td>
  </tr>
  <tr>
    <td height="25"></td>
  </tr>
  <tr>
    <td><table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="445" height="14"><a href="about_us.php" class="link416CA0">About RMG Living</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="contact_us.php" class="link416CA0">Contact Us</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="privacy.php" class="link416CA0">Legal Information</a></td>
        <td width="315" align="right">Copyright <?=date("Y", time())?> &copy; <a href="http://www.rmgltd.co.uk">Residential Management Group Ltd</a></td>
      </tr>
	  <tr><td colspan="2">&nbsp;</td></tr>
	  <tr><td colspan="2" style="text-align:left;"><p>&nbsp;</p>
	    </td>
	  </tr>
    </table></td>
  </tr> 
  <tr><td>&nbsp;</td></tr>
</table>
</body>
<!-- InstanceEnd --></html>
