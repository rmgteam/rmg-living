<?
require("../utils.php");
require($UTILS_CLASS_PATH."rmc.class.php");
require($UTILS_CLASS_PATH."website.class.php");
require($UTILS_CLASS_PATH."xml.class.php");
require($UTILS_CLASS_PATH."security.class.php");
require($UTILS_CLASS_PATH."subsidiary.class.php");
$security = new security();
$website = new website;
$rmc = new rmc;
$xml = new xml;

// Determine if allowed access into 'your community' section
$website->allow_community_access();
$rmc->set_rmc($_SESSION['rmc_num']);
$subsidiary = new subsidiary($rmc->rmc['subsidiary_id']);


// Set date for ECS query
if($_REQUEST['date_from'] == ""){
	$date_from_ts = (time()-(86400*365));
	$_REQUEST['date_from'] = date("Y-m-d", $date_from_ts);
}
else{
	if(preg_match("/^([0-9]{2})/([0-9]{2})/([0-9]{4})$/", $_REQUEST['date_from']) === 1){
		$date_from_split = explode("/",$_REQUEST['date_from']);
		$date_from_ts = mktime(1,1,1,$date_from_split[1],$date_from_split[0],$date_from_split[2]);
		$_REQUEST['date_from'] = $date_from_split[2]."-".$date_from_split[1]."-".$date_from_split[0];
	}
	else{
		$date_from_ts = (time()-(86400*365));
		$_REQUEST['date_from'] = date("Y-m-d", $date_from_ts);
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>RMG Living - Statement<?=$subsidiary->subsidiary_ecs_login_group?></title>
<script language="javascript" src="/library/jscript/functions/pay_online.js"></script>
<!-- <link href="../styles.css" rel="stylesheet" type="text/css"> -->
<link href="/css/common.css" rel="stylesheet" type="text/css">
<!--[if lte IE 8]> 
<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if lte IE 7]> 
<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
<![endif]-->
</head>
<body>
<? if($_SESSION['is_demo_account'] == "Y" && $_SESSION['is_developer'] != "Y"){?>
<form method="post" action="/building_details.php" style="margin:0; padding:0;">
<input type="hidden" name="a" value="cl">
<input type="hidden" name="s" value="gkfcbk45cgtf5kngn7kns5cg7snk5g7nsv5ng">
<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" style="margin:0; padding:0;">
	<tr>
		<td style="padding:4px;">Change Account:&nbsp;<input type="text" name="change_lessee" value="<?=$_SESSION['resident_ref']?>" size="20"> <input type="submit" name="submit_button" value="Change"></td>
	</tr>
</table>
</form>
<? }?>
<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#E9EEF4" style="border-bottom:1px solid #c1d1e1;"><div align="center"><img src="../images/templates/header_bar_grad2.jpg" alt=""></div></td>
  </tr>
  <tr>
    <td style="border-top:1px solid #ffffff;border-bottom:1px solid #ffffff;">
	<table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="180"><a href="<? print $UTILS_HTTPS_ADDRESS;if( isset($_SESSION['login']) && $_SESSION['login'] != ""){print "/building_details.php";}else{print "index.php";}?>"><img src="/images/templates/rmg_living_logo.jpg" style="margin-top:10px;" width="180" alt="RMG Living" border="0"></a></td>
        <td width="580" align="right">
		<?
		// User logged in is a 'developer', show any developer specific logos, or default to normal ones...
		if($_SESSION['is_developer'] == "Y"){
			
			$sql = "SELECT developer_id FROM cpm_residents_extra WHERE resident_num = ".$_SESSION['resident_num'];
			$result = @mysql_query($sql);
			$row = @mysql_fetch_row($result);
			if($row[0] != "" ){
				
				print '<img src="/images/developers/'.$row[0].'/header_'.$row[0].'.jpg" alt="RMG Living" width="580" height="86" BORDER=0>';
			}
			else{
				
				print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
			}
		}
		else{
		
			// Check to see if the Man. Co. has a developer association, if so, set developer specific images, or default to normal images...
			if( isset($_SESSION['login']) && $_SESSION['login'] != "" && $_SESSION['rmc_num'] != ""){
			
				$sql = "SELECT developer_id FROM cpm_rmcs_extra WHERE rmc_num = ".$_SESSION['rmc_num'];
				$result = @mysql_query($sql);
				$row = @mysql_fetch_row($result);
				if( $row[0] != "" && file_exists($UTILS_FILE_PATH.'/images/developers/'.$row[0].'/header_'.$row[0].'.jpg') ){
					
					print '<img src="/images/developers/'.$row[0].'/header_'.$row[0].'.jpg" alt="RMG Living" width="580" height="86" BORDER=0>';
				}
				else{
					
					print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
				}
			}
			else{
				
				print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
			}
		}
		?> 
		
		<?
		/*
		if( isset($_SESSION['login']) && $_SESSION['login'] != "" && $_SESSION['subsidiary_id'] != ""){
			$sql = "SELECT subsidiary_header_image FROM cpm_subsidiary WHERE subsidiary_id = ".$_SESSION['subsidiary_id'];
			$result = @mysql_query($sql);
			$row = @mysql_fetch_row($result);
			if($row[0] != "" ){ 
				?>
				<img src="/images/templates/header_image_w_logo.jpg" width="580" height="86" border="0" alt="RMG Living">
				<img src="/images/logos/<?=$row[0]?>" style="position:absolute; left:50%; margin-left:245px; margin-top:23px; z-index:10;" border="0" >
				<?
			}
			else{
				?>
				<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">
				<?
			}
		}
		else{
			?>
			<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">
			<?
		}
		*/
		?>
		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
  <td>
  <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
  <tr>
    <td height="10" align="center" valign="bottom" bgcolor="#E9EEF4" style="border-bottom:1px solid #c1d1e1;border-top:1px solid #c1d1e1;"><img src="../images/templates/header_bar_grad2.jpg" alt=""></td>
  </tr>
  <tr>
  <td height="8" align="center" valign="top" style="border-top:1px solid #ffffff;"><img src="../images/templates/header_bar_grad3.jpg" alt=""></td>
  </tr>
  </table>
  </td>
  </tr>
 
  <tr>
    <td height="4"></td>
  </tr>
  <tr>
    <td><table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="28" valign="top">
	
	<table width="760" cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td><a href="../building_details.php" class="crumbs">Your Community</a>&nbsp;>&nbsp;Statement </td>
	<td style="text-align:right;" nowrap>
	<? if(!empty($_SESSION['resident_session'])){?><a href="../index.php?logoff=Y" class="crumbs">Log Off</a><? }?>
	</td>
	</tr>
	</table>
	
	</td>
  </tr>
</table>
</td>
  </tr>
  <tr>
    <td><table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
    	<tr>
    		<td style="border-bottom:1px solid #003366;">
    			<table width="760" border="0" cellspacing="0" cellpadding="0">
    				<tr>
    					<td width="15"><img src="/images/dblue_box_top_left_corner.jpg" width="15" height="33"></td>
    					<td width="730" bgcolor="#416CA0" style="border-top:1px solid #003366; vertical-align:middle"><img src="/images/make_a_payment/make_a_payment_symbol.jpg" width="22" height="22" style="vertical-align:middle;">&nbsp;&nbsp;<span class="box_title">Your Statement</span></td>
    					<td width="15" align="right"><img src="/images/dblue_box_top_right_corner.jpg" width="15" height="33"></td>
    					</tr>
    				</table>
    			</td>
    		</tr>
    	<tr>
    		<td>
    			
    			<table width="760" border="0" cellspacing="0" cellpadding="15" style="border-left:1px solid #7ea0bd;border-right:1px solid #7ea0bd;border-top:1px solid #7ea0bd;border-bottom:1px solid #cccccc;">
    				<tr>
    					<td width="728" valign="top">
    						
    						<a href="trans_statement.php">Statement of Account</a>&nbsp;|&nbsp;<a href="trans_statement.php?type=arrears">Arrears Statement</a>
    						<h3 style="margin:20px 0 0 0; padding:0; font-weight:normal; font-size:24px;"><? if($_REQUEST['type'] == "arrears"){?>Arrears Statement<? }else{?>Statement of Account<? }?></h3>
    						<p style="margin-top:8px;">This statement lists all the transactions that have taken place on your account since <?=date("jS M Y",$date_from_ts)?>.</p>
    						
    						<table width="728" border="0" cellspacing="0" cellpadding="0" id="stat_table">
    							
    							<? if($_REQUEST['type'] == "arrears"){?>
    							<tr>
    								<th style="width:90px;border-left:1px solid #999;">Due Date</th>
    								<th style="width:100px;">Period of Charge</th>
    								<th>Description</th>
    								<th style="text-align:right; width:75px;" nowrap="nowrap">Due(&pound;)</th>
    								<th style="text-align:right; width:75px;" nowrap="nowrap">Paid(&pound;)</th>
    								<th style="text-align:right; width:115px; border-right:1px solid #999;" nowrap="nowrap">Outstanding(&pound;)</th>
    								</tr>
    							<? }else{?>
    							<tr>
    								<th style="width:90px;border-left:1px solid #999;">Due Date</th>
    								<th style="width:100px;">Period of Charge</th>
    								<th colspan="2">Description</th>
    								<th style="text-align:right; width:75px;" nowrap="nowrap">Due(&pound;)</th>
    								<th style="text-align:right; width:75px; border-right:1px solid #999;" nowrap="nowrap">Receipt(&pound;)</th>
    								</tr>
    							<? }?>
    							
    							<?
					$final_balance = 0;
					$session_id = $security->gen_serial(16);
					$ch = curl_init();
	
					$user_agent = $_SERVER['HTTP_USER_AGENT'];
					$host = "Host: hodd1svapp1";
					$content_type = "Content-Type: text/xml; charset=utf-8";
					
					$statement_type = "Full";
					if($_REQUEST['type'] == "arrears"){
						$statement_type = "";
					}
					
					// 111001140801 - breaks
					// 111001101001 - good
					// 105301100201 - lots of transactions
					$data = '<parameters>
					<Reference>105301100201</Reference>
					<order>p</order>
					<statement>'.$statement_type.'</statement>
					<bfdate>'.$_REQUEST['date_from'].'</bfdate>
					</parameters>';

										
					$qube_process_request = '<?xml version="1.0" encoding="utf-8"?>
					<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
					<soap:Body>
					<QubeProcess-3 xmlns="http://qube.qubeglobal.com/ns/webservice/">
					<ClientSessionKey>'.$session_id.'</ClientSessionKey>
					<QubeProcessName>rmgweb:tenstat</QubeProcessName>
					<Data>'.$data.'</Data>
					<UserName>rmgweb.servacc</UserName>
					<Password>p4ssw0rd</Password>
					<Group>'.$subsidiary->subsidiary_ecs_login_group.'</Group>
					<Application>QGS Property Management</Application>
					</QubeProcess-3>
					</soap:Body>
					</soap:Envelope>';
					
					$process_headers[] = $host;
					$process_headers[] = $content_type;
					$process_headers[] = "Content-Length : " . strlen ($qube_process_request);
					$process_headers[] = "SOAPAction: \"http://qube.qubeglobal.com/ns/webservice/QubeProcess-3\"";
					
					curl_setopt($ch, CURLOPT_URL, "http://81.106.219.24/qubews/qubews.asmx");
					curl_setopt($ch, CURLOPT_HTTPHEADER, $process_headers);
					curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
					curl_setopt($ch, CURLOPT_HEADER, 0);                			// tells curl to include headers in response
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);        			// return into a variable
					curl_setopt($ch, CURLOPT_TIMEOUT, 110);              			// times out after 30 secs
					curl_setopt($ch, CURLOPT_POSTFIELDS, $qube_process_request);  	// adding POST data
					curl_setopt($ch, CURLOPT_POST, 1); 								// data sent as POST
					
					$output = curl_exec($ch);
					$info = curl_getinfo($ch);
					
					if($output === false || $info['http_code'] != 200){
	
						$output = "No cURL data returned - [". $info['http_code']. "]";
						if(curl_error($ch)){
							$output .= "\n". curl_error($ch);
						}
						?>
    							<tr>
    								<td colspan="6">This service is currently unavailable. Please try again later. (1)</td>
    								</tr>
    							<?
					}
					else {
											
						//Put XML result into array
						$xml_result = $xml->xml2array($output);
						$return_code = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['tenantvalues']['code'];
					
						if($return_code != "00"){
							$error_responses = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['returnvalue']['details'];
							$err_str = "The following errors occurred:<br />";
							for($r=0;$r < count($error_responses);$r++){
								$err_str .= $error_responses[$r]['reference'].": ".$error_responses[$r]['comment']."<br />";
							}
							?>
    							<tr>
    								<td colspan="6" style="border-right:1px solid #ccc;border-left:1px solid #ccc;">This service is currently unavailable. Please try again later. (2)</td>
    								</tr>
    							<?
						}
						else{
						
							$num_trans = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['tenantvalues']['lines'];
							if($num_trans == 0 || $num_trans == ""){
								?>
    							<tr>
    								<td colspan="6" style="border-right:1px solid #ccc;border-left:1px solid #ccc;">&nbsp;There are no transactions available.</td>
    								</tr>
    							<?
							}
							else{
								
								if($num_trans == "1"){ // For some reason, the xml2array process does not put only one transaction into an array, so we do this manually
									unset($transact_array);
									$transact_array[0] = array();
									$transact_array[0] = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['tenantvalues']['transact'];
								}
								else{
									$transact_array = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['tenantvalues']['transact'];
								}
								
								for($t=0;$t < count($transact_array);$t++){
								
									$this_trans_date_split = explode("-", $transact_array[$t]['date']);
									$this_trans_date = mktime(1, 1, 1, $this_trans_date_split[1], $this_trans_date_split[2], $this_trans_date_split[0]);
									
									$this_from_date = "";
									if($transact_array[$t]['from'] != "" && !is_array($transact_array[$t]['from'])){
										$this_from_date_split = explode("-", $transact_array[$t]['from']);
										$this_from_date = mktime(1, 1, 1, $this_from_date_split[1], $this_from_date_split[2], $this_from_date_split[0]);
									}
									
									$this_to_date = "";
									if($transact_array[$t]['to'] != "" && !is_array($transact_array[$t]['to'])){
										$this_to_date_split = explode("-", $transact_array[$t]['to']);
										$this_to_date = mktime(1, 1, 1, $this_to_date_split[1], $this_to_date_split[2], $this_to_date_split[0]);
									}
									
									$period_date = "";
									if($this_from_date != "" && $this_to_date != ""){
										$period_date = date("jS M Y",$this_from_date)." - ".date("jS M Y",$this_to_date);
									}
									elseif($this_from_date != ""){
										$period_date = date("jS M Y",$this_from_date);
									}
									elseif($this_to_date != ""){
										$period_date = date("jS M Y",$this_to_date);
									}
									else{
										$period_date = "&nbsp;";
									}
									
									if($_REQUEST['type'] == "arrears"){
										?>
    							<tr>
    								<td style="width:90px; border-left:1px solid #999;"><?=date("jS M Y",$this_trans_date)?></td>
    								<td style="width:100px;border-left:1px solid #eaeaea;"><?=$period_date?></td>
    								<td style="background-color:#fafafa;border-right:1px solid #eaeaea;border-left:1px solid #eaeaea;"><?=ucfirst($transact_array[$t]['comment'])?></td>
    								<td style="text-align:right; width:75px; border-right:1px solid #eaeaea;"><? if($transact_array[$t]['charged'] != ""){print $transact_array[$t]['charged'];}else{print "&nbsp;";}?></td>
    								<td style="text-align:right; width:75px; border-right:1px solid #eaeaea;"><? if($transact_array[$t]['received'] != ""){print $transact_array[$t]['received'];}else{print "&nbsp;";}?></td>
    								<td style="text-align:right; width:115px; border-right:1px solid #999;"><? if($transact_array[$t]['outstand'] != ""){print $transact_array[$t]['outstand'];}else{print "&nbsp;";}?></td>
    								</tr>
    							<?
									}
									else{
										?>
    							<tr>
    								<td style="width:90px; border-left:1px solid #999;"><?=date("jS M Y",$this_trans_date)?></td>
    								<td style="width:100px;border-left:1px solid #eaeaea;"><?=$period_date?></td>
    								<td colspan="2" style="background-color:#fafafa;border-right:1px solid #eaeaea;border-left:1px solid #eaeaea;"><?=ucfirst($transact_array[$t]['comment'])?></td>
    								<td style="text-align:right; width:75px; border-right:1px solid #eaeaea;"><? if($transact_array[$t]['charged'] != ""){print $transact_array[$t]['charged'];}else{print "&nbsp;";}?></td>
    								<td style="text-align:right; width:75px; border-right:1px solid #999;"><? if($transact_array[$t]['received'] != ""){print $transact_array[$t]['received'];}else{print "&nbsp;";}?></td>
    								</tr>
    							<?
									}
								}
							}
						}
						
						?>
    							
    							<?					
					}
					
					curl_close($ch);
					
					if($output === true || $info['http_code'] == 200){
					
						$end_balance = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['tenantvalues']['carriedfwd'];
						$grent_due = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['tenantvalues']['cfgrentdebt'];
						$s_charge_due = $xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['tenantvalues']['cfothbalnet'];
					
						?>
    							<tr>
    								<td colspan="4" style="border-bottom:none; padding-top:10px;">Balance brought forward from before <?=date("jS M Y",$date_from_ts)?> was <strong>&pound;<?=$xml_result['soap:Envelope']['soap:Body']['QubeProcess-3Response']['QubeProcess-3Result']['data']['tenantvalues']['broughtfwd']?></strong></td>
    								<td colspan="2" style="text-align:right; font-size:16px; font-weight:bold; padding:10px 5px 10px 0;"><span style="color:<? if($end_balance > 0){?>#CC3300<? }else{?>#336633<? }?>;">&pound; <?=$end_balance?></span>&nbsp;</td>
    								</tr>
    							<?
					}
					?>
    							
    							<tr>
    								<td colspan="3" style="border-bottom:none; vertical-align:top;"><? if($output === true || $info['http_code'] == 200){?><? if($_REQUEST['type'] == ""){?>To view transactions before a certain date, please enter it below (dd/mm/yyyy)&nbsp;<form method="post" style="padding:0; margin:0;"><input type="text" name="date_from" size="11" maxlength="10" style="margin-bottom:3px; padding:2px;" > <input type="hidden" name="type" value="<?=$_REQUEST['type']?>" ><input type="image" src="/images/your_statement/view_button.jpg" ></form><? }?>&nbsp;<? }?></td>
    								<td colspan="3" style="border-bottom:none; vertical-align:top;"><img src="/images/make_a_payment/cards.jpg" alt="Cards accepted - Visa, Mastercard, Maestro" style="float:right; margin-top:15px;" ><a href="#" onClick="pay_online('<?=$grent_due?>','<?=$s_charge_due?>',<?=$rmc->rmc['payment_plan']?>)" style="float:right;clear:right;margin-top:10px;"><img src="/images/make_a_payment/make_a_payment_button.jpg" alt="Click to Make a Payment" width="120" height="21" border="0"></a></td>
    								</tr>
    							
    							</table>
    						
    						
    						
	<table width="728" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td valign="top">&nbsp;</td>
	</tr>
	<tr>
		<td valign="top">&nbsp;</td>
	</tr>
	<tr>
		<td valign="top"><span class="subt036"><b>Paying by Card</b></span></td>
	</tr>
	<tr>
		<td valign="top">&nbsp;</td>
	</tr>   
	<tr>     
	<td valign="top">We have teamed up with SecPay to create this Online Payment facility. This is the quickest method of making a payment to your service charge account. The following handling fees are charged:</td>
	</tr>       
	<tr>    
	<td valign="top">&nbsp;</td>
	</tr>        
	<tr>           
	<td valign="top">
		
	<table width="348" border="0" cellspacing="0" cellpadding="0">      
	<tr>          
	<td width="156" style="vertical-align:top"><strong>Debit Cards</strong></td>
	<td width="192" style="vertical-align:top">35p + 1% of the transaction value</td>
	</tr>      
	<tr>        
	<td><strong>Credit Cards </strong></td>
	<td>2.5% of the transaction value </td>
	</tr>      
	<tr>            
	<td><strong>Company Credit Cards </strong></td>
	<td>3% of the transaction value</td>
	</tr>
	</table></td>
	</tr>
		
	<tr>
		
	<td valign="top">&nbsp;</td>
	</tr>
		
	<tr>
		
	<td valign="top">&nbsp;</td>
	</tr>
		
	<tr>
		<td valign="top">&nbsp;</td>
	</tr>
	<tr>
		<td valign="top"><span class="subt036"><b>Paying by Phone</b></span></td>
	</tr>
	<tr>
		<td valign="top">&nbsp;</td>
	</tr>
	<tr>
		<td valign="top">To make a payment over the phone using either of the above mentioned cards, please call our switchboard on <strong><?=$UTILS_TEL_MAIN?> </strong> and ask to speak to our Customer Services department.</td>
	</tr>
	<tr>
		<td valign="top">&nbsp;</td>
	</tr>
	<tr>
		
	<td valign="top">&nbsp;</td>
	</tr>
		
	<tr>
		
	<td valign="top"><span class="subt036"><b>Paying by Cheque </b></span></td>
	</tr>
		
	<tr>
		
	<td valign="top">&nbsp;</td>
	</tr>
		
	<tr>
		
	<td valign="top">Please make cheques payable to the name of your Management Company - see below (DO NOT make the cheque out to RMG). If not already printed on the cheque, please write 'A/C Payee Only'. Please also write your name and address of the property on the back of the cheque and send your payment along with the tear-off remittance slip (from your Service Charge Demand) to the  address below. </td>
	</tr>
		
	<tr>
		
	<td valign="top">&nbsp;</td>
	</tr>
		
	<tr>
		
	<td valign="top">Cheques must be made payable to your Management Company:<br>
		<strong>
	<?=$rmc->rmc['rmc_name']?>
	</strong></td>
	</tr>
		
	<tr>
		
	<td valign="top">&nbsp;</td>
	</tr>
		
	<tr>
		
	<td valign="top">Send cheques to the following address: </td>
	</tr>
		
	<tr>
		
	<td valign="top">
		Accounts Receivable Dept.<br>
		Residential Management Group Ltd<br>
		RMG House<br>
		Essex Road<br>
		Hoddesdon<br>
		Herts<br>
		EN11 0DR</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
		</table></td>
	</tr>
    				</table></td>
    		</tr>
    	<tr><td height="5" bgcolor="#416CA0" style="border:1px solid #003366;"><img src="/images/spacer.gif" alt=""></td>
    		</tr>
    	</table></td>
  </tr>
  <tr>
    <td height="25"></td>
  </tr>
  <tr>
    <td><table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="445" height="14"><a href="../about_us.php" class="link416CA0">About RMG Living</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="../contact_us.php" class="link416CA0">Contact Us</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="../privacy.php" class="link416CA0">Legal Information</a></td>
        <td width="315" align="right">Copyright <?=date("Y", time())?> &copy; <a href="http://www.rmgltd.co.uk">Residential Management Group Ltd</a></td>
      </tr>
	  <tr><td colspan="2">&nbsp;</td></tr>
	  <tr><td colspan="2" style="text-align:left;"><p>&nbsp;</p>
	    </td>
	  </tr>
    </table></td>
  </tr> 
  <tr><td>&nbsp;</td></tr>
</table>
</body>
</html>
