<?
require("utils.php");
require($UTILS_CLASS_PATH."website.class.php");
require($UTILS_CLASS_PATH."rmc.class.php");
$website = new website;
$rmc = new rmc;

// Determine if allowed access into 'your community' section
$website->allow_community_access();

// Set RMC
$rmc->set_rmc($_SESSION['rmc_num']);

// Mark announcement as read
if($_REQUEST['whichaction'] == "read"){
	$sql = "UPDATE cpm_announcements_read SET announce_read = 'Y' WHERE announce_id = ".$_REQUEST['id']." AND resident_num = ".$_SESSION['resident_num'];
	mysql_query($sql);
}

// Mark announcement as unread
if($_REQUEST['whichaction'] == "unread"){
	$sql = "UPDATE cpm_announcements_read SET announce_read = 'N' WHERE announce_id = ".$_REQUEST['id']." AND resident_num = ".$_SESSION['resident_num'];
	@mysql_query($sql);
}

// Work out 12 month cut-off date
$today = $thistime - (86400 * 366);
$cut_off_day = date("Ymd", $today);

// Get any unread announcements
$sql_announce_unread = "SELECT * FROM cpm_announcements a, cpm_announcements_read ar WHERE a.announce_approved = 'Y' AND a.announce_id=ar.announce_id AND a.rmc_num = ".$_SESSION['rmc_num']." AND a.announce_date > '$cut_off_day' AND ar.resident_num = ".$_SESSION['resident_num']." AND ar.announce_read = 'N' ORDER BY a.announce_date DESC";
$result_announce_unread = @mysql_query($sql_announce_unread);
$num_announce_unread = @mysql_num_rows($result_announce_unread);

// Get all read announcements
$sql_announce_read = "SELECT * FROM cpm_announcements a, cpm_announcements_read ar WHERE a.announce_approved = 'Y' AND a.announce_id=ar.announce_id AND a.rmc_num = ".$_SESSION['rmc_num']." AND a.announce_date > '$cut_off_day' AND ar.resident_num = ".$_SESSION['resident_num']." AND ar.announce_read = 'Y' ORDER BY a.announce_date DESC";
$result_announce_read = @mysql_query($sql_announce_read);
$num_announce_read = @mysql_num_rows($result_announce_read);

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><!-- InstanceBegin template="/Templates/main.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- InstanceBeginEditable name="doctitle" -->
<title>RMG Living - Announcements</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<link href="styles.css" rel="stylesheet" type="text/css">
<!--[if lte IE 8]> 
<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if lte IE 7]> 
<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
<![endif]-->
</head>
<body>
<? if($_SESSION['is_demo_account'] == "Y" && $_SESSION['is_developer'] != "Y"){?>
<form method="post" action="/building_details.php" style="margin:0; padding:0;">
<input type="hidden" name="a" value="cl">
<input type="hidden" name="s" value="gkfcbk45cgtf5kngn7kns5cg7snk5g7nsv5ng">
<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" style="margin:0; padding:0;">
	<tr>
		<td style="padding:4px;">Change Account:&nbsp;<input type="text" name="change_lessee" value="<?=$_SESSION['resident_ref']?>" size="20"> <input type="submit" name="submit_button" value="Change"></td>
	</tr>
</table>
</form>
<? }?>
<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#E9EEF4" style="border-bottom:1px solid #c1d1e1;"><div align="center"><img src="images/templates/header_bar_grad2.jpg" alt=""></div></td>
  </tr>
  <tr>
    <td style="border-top:1px solid #ffffff;border-bottom:1px solid #ffffff;">
	<table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="180"><a href="<? print $UTILS_HTTPS_ADDRESS;if( isset($_SESSION['login']) && $_SESSION['login'] != ""){print "/building_details.php";}else{print "/index.php";}?>"><img src="/images/templates/rmg_living_logo.jpg" style="margin-top:10px;" width="180" alt="RMG Living" border="0"></a></td>
        <td width="580" align="right">
		<?
		// User logged in is a 'developer', show any developer specific logos, or default to normal ones...
		if($_SESSION['is_developer'] == "Y"){
			
			$sql = "SELECT developer_id FROM cpm_residents_extra WHERE resident_num = ".$_SESSION['resident_num'];
			$result = @mysql_query($sql);
			$row = @mysql_fetch_row($result);
			if($row[0] != "" && file_exists($UTILS_FILE_PATH.'/images/developers/'.$row[0].'/header_'.$row[0].'.jpg') ){
				
				print '<img src="/images/developers/'.$row[0].'/header_'.$row[0].'.jpg" alt="RMG Living" width="580" height="86" BORDER=0>';
			}
			else{
				
				print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
			}
		}
		else{
		
			// Check to see if the Man. Co. has a developer association, if so, set developer specific images, or default to normal images...
			if( isset($_SESSION['login']) && $_SESSION['login'] != "" && $_SESSION['rmc_num'] != ""){
			
				$sql = "SELECT developer_id FROM cpm_rmcs_extra WHERE rmc_num = ".$_SESSION['rmc_num'];
				$result = @mysql_query($sql);
				$row = @mysql_fetch_row($result); 
				if( $row[0] != "" && file_exists($UTILS_FILE_PATH.'images/developers/'.$row[0].'/header_'.$row[0].'.jpg') ){
					
					print '<img src="/images/developers/'.$row[0].'/header_'.$row[0].'.jpg" alt="RMG Living" width="580" height="86" BORDER=0>';
				}
				else{
					
					print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
				}
			}
			else{
				
				print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
			}
		}
		?> 
		
		<?
		/*
		if( isset($_SESSION['login']) && $_SESSION['login'] != "" && $_SESSION['subsidiary_id'] != ""){
			$sql = "SELECT subsidiary_header_image FROM cpm_subsidiary WHERE subsidiary_id = ".$_SESSION['subsidiary_id'];
			$result = @mysql_query($sql);
			$row = @mysql_fetch_row($result);
			if($row[0] != "" ){ 
				?>
				<img src="/images/templates/header_image_w_logo.jpg" width="580" height="86" border="0" alt="RMG Living">
				<img src="/images/logos/<?=$row[0]?>" style="position:absolute; left:50%; margin-left:245px; margin-top:23px; z-index:10;" border="0" >
				<?
			}
			else{
				?>
				<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">
				<?
			}
		}
		else{
			?>
			<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">
			<?
		}
		*/
		?>
		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
  <td>
  <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
  <tr>
    <td height="10" align="center" valign="bottom" bgcolor="#E9EEF4" style="border-bottom:1px solid #c1d1e1;border-top:1px solid #c1d1e1;"><img src="images/templates/header_bar_grad2.jpg" alt=""></td>
  </tr>
  <tr>
  <td height="8" align="center" valign="top" style="border-top:1px solid #ffffff;"><img src="images/templates/header_bar_grad3.jpg" alt=""></td>
  </tr>
  </table>
  </td>
  </tr>
 
  <tr>
    <td height="4"></td>
  </tr>
  <tr>
    <td><table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="28" valign="top">
	
	<table width="760" cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td>
	<!-- InstanceBeginEditable name="breadcrumbs" --><? if(!$_REQUEST['from']){?><a href="/building_details.php" class="crumbs">Your Community</a>&nbsp;>&nbsp;Announcements<? }?>&nbsp;<!-- InstanceEndEditable -->
	</td>
	<td style="text-align:right;" nowrap>
	<? if(!empty($_SESSION['resident_session'])){?><a href="index.php?logoff=Y" class="crumbs">Log Off</a><? }?>
	</td>
	</tr>
	</table>
	
	</td>
  </tr>
</table>
</td>
  </tr>
  <tr>
    <td><!-- InstanceBeginEditable name="body" -->
      <table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td style="border-bottom:1px solid #003366;"><table width="760" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="15"><img src="images/dblue_box_top_left_corner.jpg" width="15" height="33"></td>
              <td width="730" bgcolor="#416CA0" style="border-top:1px solid #003366; vertical-align:middle"><img src="images/announcements/announcements_symbol.jpg" width="22" height="22" style="vertical-align:middle;">&nbsp;&nbsp;<span class="box_title">Announcements</span></td>
              <td width="15" align="right"><img src="images/dblue_box_top_right_corner.jpg" width="15" height="33"></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td><table width="760" border="0" cellspacing="0" cellpadding="15" style="border-left:1px solid #7ea0bd;border-right:1px solid #7ea0bd;border-top:1px solid #7ea0bd;border-bottom:1px solid #cccccc;">
            <tr>
              <td width="378" valign="top"><table width="348" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td valign="top"><b><span class="subt036">Latest Announcements</span></b></td>
                </tr>
                <tr>
                  <td valign="top" height="10"></td>
                </tr>
				<?
				// Display unread announcements
				if($num_announce_unread > 0){
					while($row_announce_unread = @mysql_fetch_array($result_announce_unread)){
					?>
					<tr>
					<td valign="top">
					<table width="348" cellpadding="0" cellspacing="0" border="0">
					<tr>
					  <td colspan="2" width="348" valign="top"><b><?=substr($row_announce_unread['announce_date'],6,2)."/".substr($row_announce_unread['announce_date'],4,2)."/".substr($row_announce_unread['announce_date'],0,4)?></b></td>
					</tr>
					<tr>
					  <td colspan="2" height="10"></td>
					  </tr>
					<tr>
					  <td colspan="2"><b><?=stripslashes($row_announce_unread['announce_title'])?></b></td>
					  </tr>
					<tr><td colspan="2" height="2"></td></tr>
					<tr>
					  <td colspan="2"><?=stripslashes($row_announce_unread['announce_text'])?></td>
					  </tr>
					<tr>
					  <td colspan="2" height="10"></td>
					  </tr>
					<tr><td colspan="2"><b><a href="announcements.php?id=<?=$row_announce_unread['announce_id']?>&whichaction=read" class="link669966">Mark as Read</a></b></td></tr>
					<tr>
					  <td colspan="2" height="30"></td>
					  </tr>
					  </table>
					</td>
                	</tr>
					<?
					}
				}
				else{
					?>
					<tr>
                      <td height="30" style="vertical-align:middle;"><img src="images/about_16.gif" width="16" height="16" style="vertical-align:middle;">&nbsp;There are no new announcements</td>
                    </tr>
					<?
				}
				?>
                <tr>
                  <td>&nbsp;</td>
                </tr>
				<? if($_REQUEST['from'] && $_REQUEST['from'] == "index"){?>
				<tr><td>&nbsp;</td></tr>
				<tr>
                  <td><a href="/building_details.php"><img src="images/announcements/go_to_button.jpg" alt="Click to enter Your Community" border="0"></a></td>
                </tr>
                <? }?>
				<tr>
                  <td>&nbsp;</td>
                </tr>
				<? if(!$_REQUEST['from']){?>
                <tr>
                  <td><b><span class="subt036">Past Announcements</span></b></td>
                </tr>
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td>Below is a list of announcements that have been made to lessees of <?=$rmc->rmc['rmc_name']?>, in the past 12 months. Click on an announcement to view it in full</td>
                </tr>
                <tr>
                  <td valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td valign="top"><table width="347" border="0" cellspacing="0" cellpadding="2">
                    <?
	  				if($num_announce_read > 0){
					?>
                    <tr>
                      <td width="77" height="20" bgcolor="#f1f1f1" style="border-bottom:1px solid #cccccc;border-top:1px solid #cccccc;border-left:1px solid #cccccc;">&nbsp;<strong><span class="text036">Date</span></strong></td>
                      <td width="262" bgcolor="#f1f1f1" style="border-bottom:1px solid #cccccc;border-top:1px solid #cccccc;border-right:1px solid #cccccc;"><strong><span class="text036">&nbsp;Title</span></strong></td>
                    </tr>
						<?
						while($row_announce_read = @mysql_fetch_array($result_announce_read)){
						?>
						<tr style="background:#ffffff;">
						  <td height="20" style="border-left:1px solid #cccccc;border-bottom:1px solid #eaeaea;">&nbsp;<?=substr($row_announce_read['announce_date'],6,2)."/".substr($row_announce_read['announce_date'],4,2)."/".substr($row_announce_read['announce_date'],0,4)?></td>
						  <td style="border-right:1px solid #cccccc;border-bottom:1px solid #eaeaea;">&nbsp;<a href="announcements.php?id=<?=$row_announce_read['announce_id']?>&whichaction=view" class="link416CA0" style="text-decoration:underline;"><? print substr($row_announce_read['announce_title'],0,35);if(strlen($row_announce_read['announce_title']) > 35){print "...";}?></a></td>
						</tr>
						<?
						}
					}
	  
	 				else{
	  				?>
                    <tr>
                      <td colspan="2" height="30" style="vertical-align:middle;"><img src="images/about_16.gif" width="16" height="16" style="vertical-align:middle;">&nbsp;There are no past announcements</td>
                    </tr>
                    <?
	  				}
					?>
                  </table></td>
                </tr>
				<?
				// Display selected announcement
				if($_REQUEST['whichaction'] == "view"){
					$sql_announce_chosen = "SELECT * FROM cpm_announcements a, cpm_announcements_read ar WHERE a.announce_approved = 'Y' AND a.announce_id=ar.announce_id AND a.rmc_num = ".$_SESSION['rmc_num']." AND ar.announce_read = 'Y' AND a.announce_id = ".$_REQUEST['id'];
					$result_announce_chosen = @mysql_query($sql_announce_chosen);
					$announce_valid = @mysql_num_rows($result_announce_chosen);
					if($announce_valid > 0){
					$row_announce_chosen = @mysql_fetch_array($result_announce_chosen);
					?>
					<tr>
					  <td>&nbsp;</td>
				    </tr>
					<tr>
					<td>&nbsp;</td>
					</tr>
					<tr>
					<td>
					<table width="348" cellpadding="0" cellspacing="0" border="0">
                      <tr>
                        <td width="90" valign="top">
                          <?=substr($row_announce_chosen['announce_date'],6,2)."/".substr($row_announce_chosen['announce_date'],4,2)."/".substr($row_announce_chosen['announce_date'],0,4)?>
                        </td>
                        <td width="258" valign="top"><b><?=stripslashes($row_announce_chosen['announce_title'])?></b></td>
                      </tr>
                      <tr>
                        <td colspan="2" height="5"></td>
                      </tr>
                      <tr>
                        <td colspan="2"><?=stripslashes($row_announce_chosen['announce_text'])?></td>
                      </tr>
                      <tr>
                        <td colspan="2" height="10"></td>
                      </tr>
					
                    </table>
					</td>
					</tr>
					<? }?>
				<? }
				}?>
              </table>
			  </td>
              <td width="378" background="images/announcements/keeping_informed.jpg" bgcolor="#F5F7FB" style="border-left:1px solid #eaeaea;background-repeat:no-repeat;"><p><img src="images/spacer.gif" alt="Keeping you Informed" width="348" height="310"></p>
                <p>&nbsp;</p>
                <p>&nbsp;</p></td>
              </tr>
          </table></td>
        </tr>
		<tr><td height="5" bgcolor="#416CA0" style="border:1px solid #003366;"><img src="images/spacer.gif" alt=""></td>
		</tr>
      </table>
    <!-- InstanceEndEditable --></td>
  </tr>
  <tr>
    <td height="25"></td>
  </tr>
  <tr>
    <td><table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="445" height="14"><a href="about_us.php" class="link416CA0">About RMG Living</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="contact_us.php" class="link416CA0">Contact Us</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="privacy.php" class="link416CA0">Legal Information</a></td>
        <td width="315" align="right">Copyright <?=date("Y", time())?> &copy; <a href="http://www.rmgltd.co.uk">Residential Management Group Ltd</a></td>
      </tr>
	  <tr><td colspan="2">&nbsp;</td></tr>
	  <tr><td colspan="2" style="text-align:left;"><p>&nbsp;</p>
	    </td>
	  </tr>
    </table></td>
  </tr> 
  <tr><td>&nbsp;</td></tr>
</table>
</body>
<!-- InstanceEnd --></html>
