<?
require("utils.php");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><!-- InstanceBegin template="/Templates/main.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- InstanceBeginEditable name="doctitle" -->
<title>RMG Living - Privacy Policy</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->

<!-- InstanceEndEditable -->
<link href="styles.css" rel="stylesheet" type="text/css">
<!--[if lte IE 8]> 
<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if lte IE 7]> 
<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
<![endif]-->
</head>
<body>
<? if($_SESSION['is_demo_account'] == "Y" && $_SESSION['is_developer'] != "Y"){?>
<form method="post" action="/building_details.php" style="margin:0; padding:0;">
<input type="hidden" name="a" value="cl">
<input type="hidden" name="s" value="gkfcbk45cgtf5kngn7kns5cg7snk5g7nsv5ng">
<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" style="margin:0; padding:0;">
	<tr>
		<td style="padding:4px;">Change Account:&nbsp;<input type="text" name="change_lessee" value="<?=$_SESSION['resident_ref']?>" size="20"> <input type="submit" name="submit_button" value="Change"></td>
	</tr>
</table>
</form>
<? }?>
<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#E9EEF4" style="border-bottom:1px solid #c1d1e1;"><div align="center"><img src="images/templates/header_bar_grad2.jpg" alt=""></div></td>
  </tr>
  <tr>
    <td style="border-top:1px solid #ffffff;border-bottom:1px solid #ffffff;">
	<table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="180"><a href="<? print $UTILS_HTTPS_ADDRESS;if( isset($_SESSION['login']) && $_SESSION['login'] != ""){print "/building_details.php";}else{print "/index.php";}?>"><img src="/images/templates/rmg_living_logo.jpg" style="margin-top:10px;" width="180" alt="RMG Living" border="0"></a></td>
        <td width="580" align="right">
		<?
		// User logged in is a 'developer', show any developer specific logos, or default to normal ones...
		if($_SESSION['is_developer'] == "Y"){
			
			$sql = "SELECT developer_id FROM cpm_residents_extra WHERE resident_num = ".$_SESSION['resident_num'];
			$result = @mysql_query($sql);
			$row = @mysql_fetch_row($result);
			if($row[0] != "" && file_exists($UTILS_FILE_PATH.'/images/developers/'.$row[0].'/header_'.$row[0].'.jpg') ){
				
				print '<img src="/images/developers/'.$row[0].'/header_'.$row[0].'.jpg" alt="RMG Living" width="580" height="86" BORDER=0>';
			}
			else{
				
				print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
			}
		}
		else{
		
			// Check to see if the Man. Co. has a developer association, if so, set developer specific images, or default to normal images...
			if( isset($_SESSION['login']) && $_SESSION['login'] != "" && $_SESSION['rmc_num'] != ""){
			
				$sql = "SELECT developer_id FROM cpm_rmcs_extra WHERE rmc_num = ".$_SESSION['rmc_num'];
				$result = @mysql_query($sql);
				$row = @mysql_fetch_row($result);
				if( $row[0] != "" && file_exists($UTILS_FILE_PATH.'/images/developers/'.$row[0].'/header_'.$row[0].'.jpg') ){
					
					print '<img src="/images/developers/'.$row[0].'/header_'.$row[0].'.jpg" alt="RMG Living" width="580" height="86" BORDER=0>';
				}
				else{
					
					print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
				}
			}
			else{
				
				print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
			}
		}
		?> 
		
		<?
		/*
		if( isset($_SESSION['login']) && $_SESSION['login'] != "" && $_SESSION['subsidiary_id'] != ""){
			$sql = "SELECT subsidiary_header_image FROM cpm_subsidiary WHERE subsidiary_id = ".$_SESSION['subsidiary_id'];
			$result = @mysql_query($sql);
			$row = @mysql_fetch_row($result);
			if($row[0] != "" ){ 
				?>
				<img src="/images/templates/header_image_w_logo.jpg" width="580" height="86" border="0" alt="RMG Living">
				<img src="/images/logos/<?=$row[0]?>" style="position:absolute; left:50%; margin-left:245px; margin-top:23px; z-index:10;" border="0" >
				<?
			}
			else{
				?>
				<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">
				<?
			}
		}
		else{
			?>
			<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">
			<?
		}
		*/
		?>
		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
  <td>
  <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
  <tr>
    <td height="10" align="center" valign="bottom" bgcolor="#E9EEF4" style="border-bottom:1px solid #c1d1e1;border-top:1px solid #c1d1e1;"><img src="images/templates/header_bar_grad2.jpg" alt=""></td>
  </tr>
  <tr>
  <td height="8" align="center" valign="top" style="border-top:1px solid #ffffff;"><img src="images/templates/header_bar_grad3.jpg" alt=""></td>
  </tr>
  </table>
  </td>
  </tr>
 
  <tr>
    <td height="4"></td>
  </tr>
  <tr>
    <td><table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="28" valign="top">
	
	<table width="760" cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td>
	<!-- InstanceBeginEditable name="breadcrumbs" --><? if(isset($_SESSION['resident_num']) && $_SESSION['resident_num'] != ""){?><a href="/building_details.php" class="crumbs">Your Community</a><? }else{?><a href="index.php" class="crumbs">Home</a><? }?>&nbsp;>&nbsp;Privacy Policy<!-- InstanceEndEditable -->
	</td>
	<td style="text-align:right;" nowrap>
	<? if(!empty($_SESSION['resident_session'])){?><a href="index.php?logoff=Y" class="crumbs">Log Off</a><? }?>
	</td>
	</tr>
	</table>
	
	</td>
  </tr>
</table>
</td>
  </tr>
  <tr>
    <td><!-- InstanceBeginEditable name="body" -->
      <table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td style="border-bottom:1px solid #003366;"><table width="760" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="15"><img src="images/dblue_box_top_left_corner.jpg" width="15" height="33"></td>
              <td width="730" bgcolor="#416CA0" style="border-top:1px solid #003366; vertical-align:middle"><img src="images/privacy/privacy_symbol.jpg" width="22" height="22" style="vertical-align:middle;">&nbsp;&nbsp;<span class="box_title">Legal Information</span></td>
              <td width="15" align="right"><img src="images/dblue_box_top_right_corner.jpg" width="15" height="33"></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td><table width="758" border="0" cellspacing="0" cellpadding="15" style="border-left:1px solid #7ea0bd;border-right:1px solid #7ea0bd;border-top:1px solid #7ea0bd;border-bottom:1px solid #cccccc;">
            <tr>
              <td valign="top" style="border-right:1px solid #eaeaea;"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td><ul>
                            <li><a href="#privacy" class="link036">Privacy</a> </li>
                            <li><a href="#accessibility" class="link036">Accessibility</a></li>
                            <li><a href="#t_and_c" class="link036">Terms &amp; Conditions of Website Usage</a></li>
                            <li><a href="#legal" class="link036">Legal Disclaimer</a></li>
                            </ul>
                            <p>&nbsp;</p></td>
                        </tr>
                    </table>
                      <table width="100%"  border="0" cellspacing="5" cellpadding="0">
                        <tr>
                          <td width="97%"><strong><a name="privacy"></a>Privacy</strong></td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td>Residential Management Group Limited registered office is RMG House, Essex Road, Hoddesdon EN11 0DR . We respect your privacy and do not sell, rent or loan any identifiable information collected on this site. Any information that you give us will be treated with the utmost care and security. It will not be used in ways to which you have not consented. From time to time Residential Management Group Limited and its associated companies may use your details to provide you with information about other products and services of Residential Management Group Limited that may be of interest to you.</td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td><p><strong>Use of Information</strong><br /> 
Residential Management Group Limited ("we") will not collect any information about individuals, except where it is specifically and knowingly provided by them. Examples of such information are:</p>
<p>Your name<br>
  Your mobile and work telephone number<br>
  Your email address<br>
  Your occupation</p>
<p>The information collected will be used to send you the information you have requested and to provide information that may be useful to you.</p>
<p> 
    We may share non-personal aggregate statistics (group) data about our site visitors' traffic patterns with partners or other parties. However, we do not sell or share any information about individual users.</p>	</td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td><strong>Your rights<br>
    </strong> In addition to the company's safeguards, your personal data is protected in the UK by the Data Protection Act. This provides amongst other things that the data we hold about you should be processed lawfully and fairly. It should be accurate, relevant and not excessive. The information should be kept up to date, where necessary, and not retained for longer than is necessary. It should be kept securely to prevent unauthorised access by other people. You have the right to see what is held about you and correct any inaccuracies online. You can do this by contacting us on the details provided within this website.</td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td><strong>Policy changes<br>
	</strong>Any changes to this policy will be posted here.</td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td><strong>Security</strong><br>
	  Residential Management Group Limited treats all the data held with the utmost care and security. Any details you give will remain completely confidential.</td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td><div align="right"> <span><a href="#" class="link036"><b>^ Back to top</b></a></span> </div></td>
                        </tr>
                        <tr>
                          <td height="21">&nbsp;</td>
                        </tr>
                        <tr>
                          <td height="21">&nbsp;</td>
                        </tr>
                        <tr>
                          <td><strong><a name="accessibility"></a>Accessibility</strong></td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td>Residential Management Group Limited is committed to taking all reasonable steps to ensure that the material published on this website (http://www.rmgliving.co.uk) is accessible to all users. Some of the documents on this website are published in PDF format and these require a suitable reader such as the free <a href="http://www.adobe.com/products/acrobat/readstep2.html" target="_blank">Adobe Acrobat Reader</a>.</td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td>If you use assistive technology (such as a Braille reader, a screen reader, TTY, etc.) and the format of material on our server obstructs your ability to access the information, please contact our head office to request a Word document of the website on <?=$UTILS_TEL_MAIN;?> or email <a href="mailto:customerservice@rmguk.com">customerservice@rmguk.com</a></td>
                        </tr>
                        <tr>
                          <td>&nbsp</td>
                        </tr>
                        <tr>
                          <td>To enable us to respond in a manner most helpful to you, please indicate the nature of your accessibility problem, the web address of the requested material, and your contact details.</td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                        
                        <tr>
                          <td><div align="right"> <span><a href="#" class="link036"><b>^ Back to top</b></a></span> </div></td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td><strong><a name="t_and_c"></a>Terms &amp; Conditions of Website Usage</strong></td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td>Welcome to our website. If you continue to browse and use this website you are agreeing to comply with and be bound by the following terms and conditions of use, which together with our privacy policy govern Residential Management Group Limited relationship with you in relation to this website.</td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td>The term "Residential Management Group Limited" or "us" or "we" refers to the owner of the website whose registered office is RMG House, Essex Road, Hoddesdon EN11 0DR. The term "you" refers to the user or viewer of our website.</td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td><p>The use of this website is subject to the following terms of use:</p>
                            <ul>
                              <li>The content of the pages of this website is for your general information and use only. It is subject to change without notice.</li>
                              <li>Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information and materials found or offered on this website for any particular purpose. You acknowledge that such information and materials may contain inaccuracies or errors and we expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law.</li>
                              <li>Your use of any information or materials on this website is entirely at your own risk, for which we shall not be liable. It shall be your own responsibility to ensure that any products, services or information available through this website meet your specific requirements.</li>
                              <li>This website contains material which is owned by or licensed to us. This material includes, but is not limited to, the design, layout, look, appearance and graphics. Reproduction is prohibited other than in accordance with the copyright notice, which forms part of these terms and conditions.</li>
                              <li>All trademarks reproduced in this website, which are not the property of, or licensed to the operator, are acknowledged on the website.</li>
                              <li>Unauthorised use of this website may give to a claim for damages and/or be a criminal offence.</li>
                              <li>From time to time this website may also include links to other websites. These links are provided for your convenience to provide further information. They do not signify that we endorse the website(s). We have no responsibility for the content of the linked website(s).</li>
                              <li>You may not create a link to this website from another website or document without prior written consent from Residential Management Group Limited.</li>
                              <li>Your use of this website and any dispute arising out of such use of the website is subject to the laws of England and Wales.</li>
                            </ul></td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td><div align="right"> <span><a href="#" class="link036"><b>^ Back to top</b></a></span> </div> </td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td><strong><a name="legal"></a>Legal Disclaimer</strong></td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td>Please read this legal message carefully, as by accessing the site of Residential Management Group Limited, a site you agree to be bound by the conditions contained in this legal message:</td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td><p>1. All material contained in this web site is the property of Residential Management Group Limited and all rights are reserved.</p>
    <p>2. No part of this web site may be copied, performed in public, broadcast, published or adapted without the prior written permission of Residential Management Group Limited, except solely for your own personal and non-commercial use.</p>
    <p>3. Residential Management Group Limited assumes no responsibility for the contents of any other web site to which this site has links and cannot be held liable for damage suffered or loss or costs incurred howsoever caused, as a result of your use of any other site.</p>
    <p>4. The material contained in this web site is a guide only, and is not intended as an invitation to invest in or enter into a contract with Residential Management Group Limited and should not be relied on in making any financial decisions. If you are in any doubt, please consult a qualified financial advisor.</p>
    <p>5. Whilst we have taken every effort to provide accurate information on this web site, neither the Residential Management Group Limited, its employees, affiliated companies, suppliers or any of their employees accept liability or responsibility for the accuracy or completeness of any information contained on this site. Residential Management Group Limited shall not be liable for any loss or damage which may arise from the use of any information contained on this site.</p>
    <p>6. Residential Management Group Limited reserves the right to revise this web site service or withdraw access to the whole or any part of the web site at any time.</p>
    <p>7. These conditions of use are governed by the laws of England and Wales and you agree that the English courts shall have exclusive jurisdiction in any dispute. </p>     </td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td><div align="right"> <span><a href="#" class="link036"><b>^ Back to top</b></a></span> </div> </td>
                        </tr>
                      </table>
                      </td>
                  </tr>
              </table></td>
              </tr>
          </table></td>
        </tr>
		<tr><td height="5" bgcolor="#416CA0" style="border:1px solid #003366;"><img src="images/spacer.gif" alt=""></td>
		</tr>
      </table>
    <!-- InstanceEndEditable --></td>
  </tr>
  <tr>
    <td height="25"></td>
  </tr>
  <tr>
    <td><table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="445" height="14"><a href="about_us.php" class="link416CA0">About RMG Living</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="contact_us.php" class="link416CA0">Contact Us</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="privacy.php" class="link416CA0">Legal Information</a></td>
        <td width="315" align="right">Copyright <?=date("Y", time())?> &copy; <a href="http://www.rmgltd.co.uk">Residential Management Group Ltd</a></td>
      </tr>
	  <tr><td colspan="2">&nbsp;</td></tr>
	  <tr><td colspan="2" style="text-align:left;"><p>&nbsp;</p>
	    </td>
	  </tr>
    </table></td>
  </tr> 
  <tr><td>&nbsp;</td></tr>
</table>
</body>
<!-- InstanceEnd --></html>
