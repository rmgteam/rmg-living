<?
require_once("utils.php");
require_once($UTILS_SERVER_ROOT."library/functions/get_file_size.php");
require_once($UTILS_CLASS_PATH."website.class.php");
$website = new website;

// Determine if allowed access into 'your community' section
$website->allow_community_access();

$padding_rows = 8;

// Determine if meeting notes available
$sql_meeting = "SELECT * FROM cpm_agm m, cpm_meeting_types mt WHERE m.meeting_type_id=mt.meeting_type_id AND m.rmc_num = ".$_SESSION['rmc_num'];
$result_meeting = @mysql_query($sql_meeting);
$num_meetings = @mysql_num_rows($result_meeting);

// Determine if site visits available
$sql_site_visits = "SELECT * FROM cpm_site_visits WHERE rmc_num = ".$_SESSION['rmc_num'];
$result_site_visits = @mysql_query($sql_site_visits);
$num_site_visits = @mysql_num_rows($result_site_visits);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>RMG Living - Meetings &amp; Site Visits</title>
	<link href="styles.css" rel="stylesheet" type="text/css" />
	<link href="/css/common.css" rel="stylesheet" type="text/css" />
	<!--[if lte IE 8]> 
	<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<!--[if lte IE 7]> 
	<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<script type="text/javascript" src="/library/jscript/jquery-1.4.2.min.js"></script>

	<script type="text/javascript">
	$(document).ready(function() {

		l_height = $("#content_left").height();
		r_height = $("#content_right").height();
		if(l_height < r_height){
			$("#content_left").height(r_height);
		}
		else{
			$("#content_right").height(l_height);
		}
	});
	</script>
	<? require_once($UTILS_FILE_PATH."includes/analytics.php");?>
</head>
<body>

	<div id="wrapper">
	
		<? require_once($UTILS_FILE_PATH."includes/header.php");?>
		
		<div id="content">
		
			<table width="760" cellspacing="0">
				<tr>
					<td><a href="/building_details.php" class="crumbs">Your Community</a>&nbsp;&gt;&nbsp;Meetings &amp; Site Visits</td>
					<td style="text-align:right;" nowrap="nowrap"><? if(!empty($_SESSION['resident_session'])){?><a href="index.php?logoff=Y" class="crumbs">Log Off</a><? }?></td>
				</tr>
			</table>

				
			<table width="760" cellspacing="0" cellpadding="0" style="margin-top:12px;">
				<tr>
					<td width="15"><img src="images/dblue_box_top_left_corner.jpg" width="15" height="33" /></td>
					<td width="730" bgcolor="#416CA0" style="border-top:1px solid #003366; vertical-align:middle"><img src="images/meetings/meetings_symbol.jpg" width="22" height="22" style="vertical-align:middle;">&nbsp;&nbsp;<span class="box_title">Meetings &amp; Site Visits </span></td>
					<td width="15" align="right"><img src="images/dblue_box_top_right_corner.jpg" width="15" height="33" /></td>
				</tr>
			</table>
						
			
			<div class="content_box_1 clearfix" style="padding:0;width:758px;">
								
				<div id="content_left" style="float:left;width:377px;padding:15px;">
				
					<span class="subt036" style="font-weight:bold;">Meetings</span>
					<p>Below is a list of notes from meetings that have taken place between RMG and Residents/ Developers on behalf of your Management Company.</p>
				
					<table width="347" cellspacing="0">
						<?
						if($num_meetings > 0){
							?>
	
							<tr>
								<td width="139" height="20" bgcolor="#f1f1f1" style="border-bottom:1px solid #cccccc;border-top:1px solid #cccccc;border-left:1px solid #cccccc;">&nbsp;<strong><span class="text036">Date of Meeting </span></strong></td>
								<td width="208" bgcolor="#f1f1f1" style="border-bottom:1px solid #cccccc;border-top:1px solid #cccccc;border-right:1px solid #cccccc;"><strong><span class="text036">&nbsp;Meeting  Type</span></strong></td>
							</tr>
	
							<?
							while($row_meeting = @mysql_fetch_array($result_meeting)){
								$padding_rows--;
								?>  
								<tr style="background:#ffffff;">
									<td height="20" style="border-left:1px solid #cccccc;border-bottom:1px solid #eaeaea;">&nbsp;<?=substr($row_meeting['agm_date'],6,2)."/".substr($row_meeting['agm_date'],4,2)."/".substr($row_meeting['agm_date'],2,2)?></td>
									<td style="border-right:1px solid #cccccc;border-bottom:1px solid #eaeaea;">&nbsp;<a href="<?=$UTILS_HTTP_ADDRESS?>show_file.php?type=meeting_notes&id=<?=$row_meeting['agm_file_stamp']?>&rmc_num=<?=$_SESSION['rmc_num']?>" target="_blank" class="link416CA0" style="text-decoration:underline;"><?=$row_meeting['meeting_type']?></a> <? if(file_exists($UTILS_MEETING_NOTES_PATH.$row_meeting['agm_file'])){print "(".get_file_size($UTILS_MEETING_NOTES_PATH.$row_meeting['agm_file']).")";}?></td>
								</tr>
								<?
							}
						}
						else{
							?>
							<tr>
								<td colspan="2" height="30" style="vertical-align:middle;"><img src="images/about_16.gif" width="16" height="16" style="vertical-align:middle;">&nbsp;No meetings found</td>
							</tr>	
							<?
						}
						?>
					</table>
					
					<div class="subt036" style="font-weight:bold;margin-top:30px;">Site Visits</div>
					<p>Below is a list of site visits that have been carried out by your Property Management Team at RMG.</p>
					
					<table width="347" cellspacing="0">
					
						<?
						if($num_site_visits > 0){
						
							?>
							<tr>			
								<td width="139" height="20" bgcolor="#f1f1f1" style="border-bottom:1px solid #cccccc;border-top:1px solid #cccccc;border-left:1px solid #cccccc;">&nbsp;<strong><span class="text036">Date of Site Visit</span></strong></td>
								<td width="208" bgcolor="#f1f1f1" style="border-bottom:1px solid #cccccc;border-top:1px solid #cccccc;border-right:1px solid #cccccc;"><strong><span class="text036">&nbsp;Download</span></strong></td>
							</tr>
							
							<?
							while($row_site_visits = @mysql_fetch_array($result_site_visits)){
								$padding_rows--;
								
								?>
								<tr>
									<td style="border-bottom:1px solid #eaeaea;border-left:1px solid #cccccc;" height="20">&nbsp;<?=substr($row_site_visits['site_visit_date'],6,2)."/".substr($row_site_visits['site_visit_date'],4,2)."/".substr($row_site_visits['site_visit_date'],2,2)?></td>
									<td style="border-bottom:1px solid #eaeaea;border-right:1px solid #cccccc;">&nbsp;<a href="<?=$UTILS_HTTP_ADDRESS?>show_file.php?type=site_visit&id=<?=$row_site_visits['site_visit_file_stamp']?>&rmc_num=<?=$_SESSION['rmc_num']?>" target="_blank" class="link416CA0" style="text-decoration:underline;">Report</a> <?="(".get_file_size($UTILS_SITE_VISITS_PATH.$row_site_visits['site_visit_file']).")"?></td>
								</tr>
								<?
							}
						}
						else{
							?>
							<tr>
								<td colspan="2" height="30" style="vertical-align:middle;"><img src="images/about_16.gif" width="16" height="16" style="vertical-align:middle;">&nbsp;No Site Visits found</td>
							</tr>
							<?
						}
						
						// Add in padding rows
						for($p=$padding_rows;$p>0;$p--){
							?>
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
							<?
						}
						?>
						
					</table>
				</div>
	
				<div id="content_right" style="width:348px;float:right;background-color:#F5F7FB;vertical-align:top;background-image:url(/images/meetings/surveyor.jpg);border-left:1px solid #eaeaea;background-repeat:no-repeat;">
						
					<table cellspacing="0" style="margin-top:70px;margin-right:20px;margin-left:15px;">
						<tr>
							<td style="vertical-align:top;">These documents can be viewed using Adobe Reader.<br /><a href="<?=$UTILS_ADOBE_URL?>" target="_blank">Click to download</a>. </td>
							<td style="width:90px;text-align:right;vertical-align:top;"><a href="<?=$UTILS_ADOBE_URL?>" target="_blank"><img src="images/getacrobat.gif" alt="Get Adobe Reader" width="88" height="31" border="0" style="vertical-align:middle;"></a></td>
						</tr>
					</table>
					<p><img src="images/spacer.gif" alt="Ensuring the well-being of your property" width="348" height="310"></p>
					
				</div>
			
			</div>

		</div>
	
		<? require_once($UTILS_FILE_PATH."includes/footer.php");?>

	</div>

</body>
</html>
