$.validator.addMethod("valueNotEquals", function (value, element, options) {
	var validator = this;
	return options != value;
}, $.validator.format("Value must not equal {0}."));

$.validator.addMethod("maxlength", function(value, element){
	var val = true;

	if($(element).data('val') !== undefined){
		val = ($(element).data('val' ) === 'true');
	}

	var length = $(element).val().length;

	if( !val ) {
		length = $($(element).attr('data-content-handler')).text().length;
	}

	if( length > $(element).attr('maxlength') ) {
		return false;
	}else{
		return true;
	}
}, "Please remove some characters, you have exceeded the character limit");

//ensures a given number of fields in a group a required
$.validator.addMethod("require_from_group", function (value, element, options) {
	var validator = this;
	var minRequired = options[0];
	var selector = options[1];
	var validOrNot = $(selector, element.form).filter(function () {
			return validator.elementValue(this);
		}).length >= minRequired;

	return validOrNot;
}, $.validator.format("Please fill at least {0} of these fields."));

$.validator.addMethod("greaterThan",function(value, element, params) {
	if (!/Invalid|NaN/.test(new Date(value))) {
		if(params instanceof Date) {
			return new Date( value ) > params;
		}else if( params == 'today'){
			return new Date( value ) > new Date( );
		}else{
			return new Date( value ) > new Date( $( params ).val() );
		}
	}

	return isNaN(value) && isNaN($(params).val())
	|| (Number(value) > Number($(params).val()));
},'Must be greater than {0}.');

$.validator.addMethod("passwordSimple",function(value, element, params) {
	var pass =/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{6,})/.test(value);
	return pass;

},'Must contain a lowercase character, uppercase character and number.');

$.validator.addMethod("passwordComplex",function(value, element, params) {
	var pass =/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~])(?=.{6,})/.test(value);
	return pass;

},'Must contain a lowercase character, uppercase character, number and symbol.');

$.validator.addMethod("ageGreaterThan", function(value, element, arg) {

	var dateElement = $(element);
	var age = arg;

	if (!/Invalid|NaN/.test(new Date(dateElement.val()))) {
		var dobDate = new Date(dateElement.val());

		var currDate = new Date();
		currDate.setFullYear(currDate.getFullYear() - age);

		if( currDate > dobDate ){
			return true;
		}else{
			return false;
		}
	}
}, "You must be at least {0} years of age.");

$.validator.addMethod('telephone', function(value, element) {
	var valid = false;
	if( $(element ).attr('required') ) {
		if( $(element).attr('data-telephone') == 'valid' ) {
			valid = true;
		}
	}else{
		if( $(element).attr('data-telephone') != 'valid' ) {
			if( $(element ).val() == '' ){
				valid = true;
			}
		} else {
			valid = true;
		}
	}
	return valid;

}, 'Please specify a valid phone number');