function validateSuccess( element ){
	var id = '#' + $(element).attr('id').replace("-error", '');
	var formGroup = $(id).parent().find('label');
	$(id).removeClass('invalid');
	$(id).addClass('valid');
	formGroup.addClass('active');
}

function validateShowErrors(errorMap, errorList, that){
	for (var i = 0; errorList[i]; i++) {
		var element = that.errorList[i].element;
		that.errorsFor(element).remove();
	}
	that.defaultShowErrors();
}

function validateError( error, element ){
	var formGroup = element.parent().find('label');

	element.addClass('invalid');
	element.removeClass('valid');

	formGroup.attr({
		'data-error': error.html()
	});
}

function validateFocusOut( element ) {
	var rules = $( element ).rules();
	if ( rules['require_from_group'] !== undefined ) {
		$( rules['require_from_group'][1] ).valid();
	} else {
		$( element ).valid();
	}
}

function formSubmitHandle ( form, data ) {
	var def = new $.Deferred();
	var promise = def.promise();
	promise.progress(function(form, data){
		formMap( form ).then(function(postData){
			var formId = $(form ).attr('id');
			var id = $( '#' + formId + '-id-input' ).val();

			if( data !== undefined ){
				$.each(data, function(key, value){
					postData[key] = value;
				});
			}
			def.resolve(postData);
		});
	});
	def.notify(form, data);
	return promise;
}

function formMap( form ) {
	var def = new $.Deferred();
	var promise = def.promise();
	promise.progress(function(form) {
		var postData = serializeObject( $( form ) );

		var formName = $( form ).attr( 'id' ) + '-';
		var regex = RegExp( '-input', 'g' );

		$.each( postData, function ( key, value ) {
			var newKey = key.replace( formName, '' ).replace( regex, '' );
			postData[newKey] = value;
			delete postData[key];
		} );

		$( form ).find( 'input[type="checkbox"]' ).each( function () {
			var id = $( this ).attr( 'id' ).replace( formName, '' ).replace( regex, '' );
			if ( postData[id] != undefined ) {
				postData[id] = 1;
			} else {
				postData[id] = 0;
			}
		} );

		$( form ).find( 'input[type="date"]' ).each( function () {

			var id = $( this ).attr( 'id' ).replace( formName, '' ).replace( regex, '' );

			if ( !Modernizr.inputtypes.date ) {
				var dateString = $( this ).val();
				var dateArray = dateString.split( '/' );
				var dateInput = new Date( dateArray[2], dateArray[1] - 1, dateArray[0], 2, 0, 1, 0 ).toJSON();
			} else {
				var date = new Date( $( this ).val() );
				var day = date.getUTCDate();
				var dateInput = new Date( date.getFullYear(), date.getMonth(), day, 2, 0, 1, 0 ).toJSON();
			}

			postData[id] = dateInput;
		} );

		def.resolve(postData);
	});
	def.notify(form);
	return promise;
}

function serializeObject(element)	{
	var o = {};
	var disabledArray = [];

	element.find(':disabled').each(function(){
		disabledArray.push($(this));
		$(this).removeAttr('disabled');
	});

	var a = element.serializeArray();
	$.each(a, function() {
		if (o[this.name] !== undefined) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});

	$.each(disabledArray, function(key, value){
		value.attr('disabled', 'disabled');
	});

	return o;
}

function initFields() {
	$('input[type="password"]').complexify({}, function (valid, complexity) {
		if(!$(this).hasClass('invalid')) {
			var strength = 'very weak';
			var cssClass = 'red-text';

			if( complexity > 20 ){
				strength = 'weak';
				cssClass = 'orange-text';
			}
			if( complexity > 40 ){
				strength = 'average';
				cssClass = 'blue-text';
			}
			if( complexity > 60 ){
				strength = 'strong';
				cssClass = 'green-text';
			}
			if( complexity > 80 ){
				strength = 'very strong';
				cssClass = 'green-text';
			}

			$(this).parent().find('span.complex').remove();
			$(document.createElement('span'))
				.addClass("complex " + cssClass)
				.html(strength)
				.appendTo($(this ).parent());
		}
	});
	$('input[type="tel"]').each(function() {
		$(this).rules('add', {
			telephone: true
		});

		$( this )
			.on('change', function() {
				if ($.trim($(this).val())) {
					if ($(this).intlTelInput("isValidNumber")) {
						$( this ).attr('data-telephone', 'valid');
					} else {
						$( this ).attr('data-telephone', 'invalid');
					}
					try {
						$( this ).closest('form' ).valid();
					}catch (err){

					}
				}
			})
			.intlTelInput({
				'utilsScript': '/new_design/js/intlTel.utils.js',
				'preferredCountries': ['GB'],
				'nationalMode': false,
				'onCountryChange': function(e){
					var country = e['country'];
					var that = e['this'];
					if( country['iso2'] == 'gb' ){
						that.setNationalMode(true);
					}else{
						that.setNationalMode(false);
					}
				}
			});
	});
	$('input[type="date"]').attr('data-date-format',"dd/mm/yyyy");
	$('input[type="date"]').pickadate({
		selectMonths: true,
		selectYears: 15,
		format: 'dd/mm/yyyy',
		onOpen: function(){
			this.$node.parents('.side-nav' ).css('overflow', 'visible');
		},
		onClose: function(){
			this.$node.parents('.side-nav' ).css('overflow', '');
		},
		onRender: function(){
			var label = this.$node.parent().find('label' );
			this.$node.after(label);
		}
	});

	$('input[type!="submit"], select, textarea' ).each(function(){
		if( $(this ).val() !== ''){
			$(this ).valid();
			$(this ).parent().find('label' ).addClass('active');
		}else{
			$(this ).parent().find('label' ).removeClass('active');
		}
	})
}


function objectToDate( value ) {
	var valueTime = '';
	var valueDate = '';

	if (typeof value === 'object' && value['date'] !== undefined) {
		var valueArray = value['date'].split('.');
		var dateString = valueArray[0];

		var match = dateString.match(/^(\d+)-(\d+)-(\d+) (\d+)\:(\d+)\:(\d+)$/);
		var date = new Date(match[1], match[2] - 1, match[3], match[4], match[5], match[6]);


		var dd = date.getDate();
		var mm = date.getMonth() + 1; //January is 0!

		var yyyy = date.getFullYear();
		if (dd < 10) {
			dd = '0' + dd
		}
		if (mm < 10) {
			mm = '0' + mm
		}

		var h = date.getHours();
		var i = date.getMinutes();
		if (h < 10) {
			h = '0' + h
		}
		if (i < 10) {
			i = '0' + i
		}

		value = dd + '/' + mm + '/' + yyyy;
		valueTime = h + ':' + i;

		if (Modernizr.inputtypes.date) {
			valueDate = date.toISOString().substr(0, 10);
		}
		dd = null;
		mm = null;
		yyyy = null;
		h = null;
		i = null;
	}else if(typeof value === 'string') {
		if ( value.match( /(\d{4})-(\d{2})-(\d{2})T(\d{2})\:(\d{2})\:(\d{2})/ ) !== null ) {
			var date = new Date( value );
			dateString = null;
			match = null;

			var dd = date.getDate();
			var mm = date.getMonth() + 1; //January is 0!
			var yyyy = date.getFullYear();
			var h = date.getHours();
			var i = date.getMinutes();

			if ( dd < 10 ) {
				dd = '0' + dd
			}
			if ( mm < 10 ) {
				mm = '0' + mm
			}
			if ( h < 10 ) {
				h = '0' + h
			}
			if ( i < 10 ) {
				i = '0' + i
			}

			value = dd + '/' + mm + '/' + yyyy;
			var valueTime = h + ':' + i;

			if (Modernizr.inputtypes.date) {
				valueDate = date.toISOString().substr(0, 10);
			}
			date = null;
			dd = null;
			mm = null;
			yyyy = null;
			h = null;
			i = null;
		}
	}

	return [ valueDate, valueTime, value ];
}

function formFormat( data, element, callback ) {

	$( '#' + element )[0].reset();

	for (var key in data) {
		var value = data[key];

		if (value != null) {

			var valueArray = objectToDate( value );
			var valueDate = valueArray[0];
			var valueTime = valueArray[1];
			value = valueArray[2];

			if (key.indexOf('Password') < 0 ) {

				if ( $( '#' + element + '-' + key + '-input' ).attr( 'type' ) == 'time' ) {
					$( '#' + element + '-' + key + '-input' ).val( valueTime );
				} else if ( $( '#' + element + '-' + key + '-input' ).attr( 'type' ) == 'date' ) {
					var picker = $( '#' + element + '-' + key + '-input' ).pickadate('picker');
					picker.set('select', value, { format: 'dd/mm/yyyy' });
					picker = null;
				} else if ( $( '#' + element + '-' + key + '-input' ).attr( 'type' ) == 'tel' ) {
					$( '#' + element + '-' + key + '-input' ).intlTelInput( 'setNumber', value.replace('00', '+') );
					$( '#' + element + '-' + key + '-input' ).trigger('change');
				} else{
					$( '#' + element + '-' + key + '-input' ).val(value);
				}
			}
			
		}
	}

	$( '#' + element ).find('input[type!="submit"], select, textarea').filter(function() {
		if( $(this).val() != '' ) {
			$(this ).valid();
		}
	});

	if( $.isFunction( callback ) ) {
		callback(data);
	}
}


$(document).on('ready', function(){
	$.getScript("/new_design/js/validation.rules.min.js", function(){
		$.getScript("/new_design/js/modernizr.min.js", function() {

		});
	});
});