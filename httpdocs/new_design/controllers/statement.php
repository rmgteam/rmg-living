<?php

if( $_REQUEST['func'] == 'accountOptions' ){
	$resultsArray = array();
	$resultsArray['results'] = array();

	$tempArray = array();
	$tempArray['html'] = "Date";
	$tempArray['colId'] = 0;
	$tempArray['sort'] = true;
	$resultsArray['results'][] = $tempArray;

	$tempArray = array();
	$tempArray['html'] = "Description";
	$tempArray['colId'] = 1;
	$tempArray['sort'] = true;
	$resultsArray['results'][] = $tempArray;

	$tempArray = array();
	$tempArray['html'] = "Due";
	$tempArray['colId'] = 2;
	$tempArray['sort'] = true;
	$resultsArray['results'][] = $tempArray;

	$tempArray = array();
	$tempArray['html'] = "Charged (&pound;)";
	$tempArray['colId'] = 3;
	$tempArray['sort'] = true;
	$resultsArray['results'][] = $tempArray;

	$tempArray = array();
	$tempArray['html'] = "Received (&pound;)";
	$tempArray['colId'] = 4;
	$tempArray['sort'] = true;
	$resultsArray['results'][] = $tempArray;

	$tempArray = array();
	$tempArray['html'] = "Balance (&pound;)";
	$tempArray['colId'] = 5;
	$tempArray['sort'] = true;
	$resultsArray['results'][] = $tempArray;

	echo json_encode( $resultsArray );
	exit;
}

if( $_REQUEST['func'] == 'account' ){
	$requests = json_decode(file_get_contents("php://input"), true);
	$request = $requests['requestObject'];
	$limitArray = array();
	$columnFilter = array();
	$whereArray = array();
	$orderArray = array();
	$indexColumn = 'id';
	$columns = array(array('data'=>'date', 'type'=>'date'), 'description', array('data'=>'due', 'type'=>'date'), array('data'=>'charged', 'type'=>'currency'), array('data'=>'received', 'type'=>'currency'), array('data'=>'balance', 'type'=>'currency'));
	$iColumnCount = count($columns);

	/// Paging
	if (isset($request['iDisplayLength'])){
		if($request['iDisplayLength'] != '-1') {
			$limitArray = array($request['iDisplayStart'], $request['iDisplayLength']);
		}
	}

	/// Get Number of sorted columns
	$iSortingCols = intval( $request['iSortingCols'] );

	/// Global Filtering
	if ( isset($request['sSearch']) && $request['sSearch'] != "" ) {
		for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
			$colData = $columns[$i];
			$type = 'string';
			if( is_array( $colData )) {
				$type = $colData['type'];
				$colData = $colData['data'];
			}
			if( $type == 'date' ) {
				$term = '';
				if( DateTime::createFromFormat('d/m/Y', $request['sSearch']) !== false ){
					$term = DateTime::createFromFormat('d/m/Y H:i:s', $request['sSearch'] . ' 00:00:00')->format('Y-m-d H:i:s');
				}else if( DateTime::createFromFormat('Y-m-d', $request['sSearch']) !== false ){
					$term = $request['sSearch'] . ' 00:00:00';
				}
				if( $term != '' ) {
					$termB = str_replace('00:00:00', '23:59:59', $term);
					$whereArray[] = array( "key" => '(', "operator" => "", "term" => "", "type" => "" );
					$whereArray[] = array( "key" => $colData, "operator" => ">=", "term" => $term, "type" => "AND" );
					$whereArray[] = array( "key" => $colData, "operator" => "<=", "term" => $termB, "type" => "OR" );
					$whereArray[] = array( "key" => ')', "operator" => "", "term" => "", "type" => "OR" );
				}
			}else{
				$whereArray[] = array( "key"=>$colData, "operator"=>"LIKE", "term"=>'%'.$request['sSearch'].'%', "type"=>"OR" );
			}
		}
		$columnFilter =  $request['sSearch'];
	}

	/// Ordering
	/// Loop through column variables
	for ( $i=0 ; $i<$iSortingCols ; $i++ ) {
		/// If sorting on column is true
		if ( $request[ 'bSortable_'.intval($request['iSortCol_'.$i]) ] == 'true' ) {
			$key = $columns[intval( $request['iSortCol_'.$i] )];
			if( is_array( $key ) ){
				$newKey = $key['data'];
				$key = $newKey;
			}
			$orderArray[] = array( "key"=>$key, "direction"=>( $request['sSortDir_'.$i] === 'asc' ? 'ASC' : 'DESC' ) );
		}
	}

	/// Build SELECT
	$sql = "SELECT " . $indexColumn . ", ";
	foreach( $columns as $columnD){
		$column = $columnD;
		if( is_array( $columnD )) {
			$column = $columnD['data'];
		}
		$sql .= $column . ', ';
	}
	$sql = substr( $sql, 0, -2 );
	$sql .= "
	FROM statement_test";
	/// Build WHERE
	$sql_clause = "";
	if( count( $whereArray ) > 0 ) {
		$sql_clause .= "
		WHERE";
		for($i = 0; $i < count( $whereArray ); $i++ ) {
			$whereClause = $whereArray[$i];
			if( $whereClause['key'] == '(' ) {
				$sql_clause .= " (";
			}else if( $whereClause['key'] == ')' ) {
				$sql_clause = substr( $sql_clause, 0, -(strlen( $lastType) ) );
				$sql_clause .= ")
				" . $whereClause['type'];
			}else {
				$sql_clause .= " " . $whereClause['key'] . " " . $whereClause['operator'] . " '" . $whereClause['term'] . "'
			" . $whereClause['type'];
			}
			$lastType = $whereClause['type'];
		}
		/// Remove last type
		$sql_clause = substr( $sql_clause, 0, -(strlen( $lastType) ) );
	}

	/// Build ORDER BY
	$sql_order = "";
	if( count( $orderArray ) > 0 ) {
		$sql_order .= "
		ORDER BY ";
		for($i = 0; $i < count( $orderArray ); $i++ ) {
			$orderClause = $orderArray[$i];
			$sql_order .= $orderClause['key'] . " " . $orderClause['direction'];
		}
	}
	/// Build LIMIT
	$sql_limit = "";
	if( count( $limitArray ) > 0 ) {
		$sql_limit .= "
		LIMIT " . $limitArray[0] . ", " . $limitArray[1];
	}

	$mysql_host = "localhost";
	$mysql_user = "cpmlivingmysql";
	$mysql_password = "cpml1v1n9my5ql";
	$database = "cpm_living";

	$link = mysql_connect( $mysql_host, $mysql_user, $mysql_password ) or die( 'Could not connect: ' . mysql_error() );
	mysql_select_db( $database ) or die( 'Could not select database' );

	$input = array();
	/// Performing SQL query
	$sql = $sql . $sql_clause . $sql_order;
	$result_all = mysql_query( $sql ) or die( 'Query failed: ' . mysql_error() . 'SQL: ' . $sql );
	$result = mysql_query( $sql . $sql_limit ) or die( 'Query failed: ' . mysql_error() . 'SQL: ' . $sql );
	if( mysql_num_rows( $result ) > 0 ) {
		$input['count'] = mysql_num_rows( $result );
		$input['totalCount'] = mysql_num_rows( $result_all );
		$input['data'] = array();
		while( $row = mysql_fetch_array( $result, MYSQL_ASSOC ) ) {
			$input['data'][] = $row;
		}
	}

	/// array format
	$output = array(
		"sEcho"                => intval($request['sEcho']),
		"iTotalRecords"        => $input['count'],
		"iTotalDisplayRecords" => $input['totalCount'],
		"sql"                  => $sql . $sql_limit,
		"aaData"               => array()
	);

	/// Put data into DataTables format.
	if ( count( $input['data'] ) > 0 ){
		foreach ( $input['data'] as $row ) {
			$row["DT_RowId"] = $row[$indexColumn];

			if ( isset($row['class'] ) ) {
				$row["DT_RowClass"] = $row['class'];
			}

			$i = 0;
			foreach( $columns as $columnD ) {
				$column = $columnD;
				$type = "string";
				if( is_array( $columnD )) {
					$column = $columnD['data'];
					$type = $columnD['type'];
				}

				if( $column != $indexColumn ){
					if( !is_null( $row[$column] ) ) {
						if ($type == 'date') {
							$row[$i] = $dateTime = DateTime::createFromFormat('Y-m-d H:i:s', $row[$column])->format('d/m/Y');
						}else if ($type == 'currency') {
							$row[$i] = '&pound;' . number_format($row[$column], 2);
						}else {
							$row[$i] = $row[$column];
						}
					}else{
						$row[$i] = '&nbsp;';
					}
					unset($row[$column]);
					$i++;
				}
			}

			unset($row[$indexColumn]);
			unset($row['class']);
			$output['aaData'][] = $row;
		}
	}else{
		$output['aaData'] = array();
	}

	echo json_encode( $output );
	exit;
}