$(document ).on('ready', function(){
	templateEngine.load('/new_design/templates/sidevert.html', {image: 'images/sidevert.png'}, $('#advert') ).then(function() {
		templateEngine.load( '/new_design/templates/footer.html', {}, $( 'footer' ), true ).then( function () {
			templateEngine.load( '/new_design/templates/sidebar.html', {}, $( 'aside' ), true ).then( function () {
				templateEngine.load( '/new_design/templates/header.html', {title: 'Community'}, $( 'header' ), true ).then( function () {
					templateEngine.load( '/new_design/templates/details.html', {}, $( '#account-details' ), true ).then( function () {

						var propertyData = {
							image: 'http://li.zoocdn.com/42412bfe0c2b0c4576134b3df5adfbdf88ba812f_645_430.jpg',
							property: 'Lawrence Square Management Company (York) Ltd',
							description: 'A modern development of 91 apartments off Lawrence Street offering excellent access to the City Centre. Pleasant surroundings with a communal courtyard and allocated parking.',
							phone: '0345 002 4444',
							propertyManager: 'Ben Johnson'
						};

						templateEngine.load( '/new_design/templates/property-info.html', propertyData, $( '#propertyInformation' ) ).then( function () {

							var floodDesc = "We have been made aware that there is a severe flood warning in place along the River Aire. The purpose of this announcement is to advise you that the environment agency are advising residents, particularly on the ground floor to take reasonable actions to prepare for potential flooding. The environment agency have advised that residents on ground floors should:<br/><br/>"
							floodDesc += "1. Take precautions to protect valuables<br/>";
							floodDesc += "2. Place valuables high up within the property or remove to a safe location<br/>";
							floodDesc += "3. Ensure mobile phones are charged and in the event of an emergency to contact 999.<br/>";
							floodDesc += "4. Do not venture into flood waters<br/>";
							floodDesc += "5. Do not drive unless absolutely necessary<br/><br/>";
							floodDesc += "The environment agency are monitoring the levels. RMG Personnel are liaising with the authorities to ensure that we are kept up to date with progress.";

							var data = [
								{
									'title': 'Flooding on Site',
									'description': 'We have been made aware that there is a severe flood warning in place along the River Aire. The purpose of this announcement is to advise you that the environment agency are advising residents, particularly on the ground floor to take reasonable actions to prepare for potential flooding.',
									'long-description': floodDesc,
									'type': 'announce',
									'id': 1
								},
								{
									'title': 'New Newsletter Available',
									'description': 'January 2016',
									'colour': 'blue',
									'type': 'colour',
									'link': '/new_design/newsletter.pdf',
									'other': 'Newsletter',
									'id': 2
								},
								{
									'title': 'New Budget Available',
									'description': '2016 Budget',
									'colour': 'green',
									'type': 'colour',
									'link': '/new_design/budget.xlsx',
									'other': 'Budget',
									'id': 3
								},
								{
									'title': 'New Site Meeting Available',
									'description': 'AGM Meeting',
									'colour': 'yellow',
									'type': 'colour',
									'link': '/new_design/agm-meeting.pdf',
									'other': 'Site Meeting',
									'id': 4
								}
							];

							var images = [
								{
									link: "http://li.zoocdn.com/42412bfe0c2b0c4576134b3df5adfbdf88ba812f_645_430.jpg",
									height: 120,
									width: 180
								},
								{
									link: "http://imganuncios.mitula.net/lawrence_square_york_yo10_2200052449001295671.jpg",
									height: 120,
									width: 167
								},
								{
									link: "http://li.zoocdn.com/f4a812f3f3a09c927d112a064f24e0403945b3d2_645_430.jpg",
									height: 120,
									width: 180
								},
								{
									link: "http://media.rightmove.co.uk/map/_generate?latitude=53.953699&longitude=-1.064168&zoomLevel=14&width=190&height=222&signature=WxiD4yzSlV66Vuk4W8iZcK77J5o=",
									height: 120,
									width: 103
								},
								{
									link: "http://imganuncios.mitula.net/lawrence_square_york_yo10_2200052449001295671.jpg",
									height: 120,
									width: 167
								},
								{
									link: "http://li.zoocdn.com/f4a812f3f3a09c927d112a064f24e0403945b3d2_645_430.jpg",
									height: 120,
									width: 180
								}
							];

							processAnnouncement( data ).then( function () {
								processGallery( images ).then( function () {
									$( '.image-container' ).rowGrid( {
										itemSelector: ".item",
										minMargin: 10,
										maxMargin: 25,
										firstItemClass: "first-item",
										lastRowClass: 'last-row',
										resize: true
									} );

									$( '#propertyInformation .image-container img' ).on( 'click', function () {
										$( '#propertyInformation .card-image .crop-height img' ).attr( 'src', $( this ).attr( 'src' ) );
									} );
								} );
							} );
						} );
					} );
				} );
			} );
		});
	});
});

var processAnnouncement = function(data) {
	var mainDef = new $.Deferred();
	var promise = mainDef.promise();
	promise.progress(function() {
		var top = "";
		var bottom = "";
		templateEngine.load('/new_design/templates/js/asyncProcessAnnouncement.js.html', {}).then(function(indexTemplate) {
			$.each(data, function (i, v) {
				top += templateEngine.execute(indexTemplate, {
					'obj': 'data',
					'i': i
				});
				bottom += "});"
			});

			var string = top + "mainDef.resolve();" + bottom;
			eval(string);
		});
	});
	mainDef.notify();
	return promise;
};

var processGallery = function(data) {
	var mainDef = new $.Deferred();
	var promise = mainDef.promise();
	promise.progress(function() {
		var top = "";
		var bottom = "";
		templateEngine.load('/new_design/templates/js/asyncProcessGallery.js.html', {}).then(function(indexTemplate) {
			$.each(data, function (i, v) {
				top += templateEngine.execute(indexTemplate, {
					'obj': 'data',
					'i': i
				});
				bottom += "});"
			});

			var string = top + "mainDef.resolve();" + bottom;
			eval(string);
		});
	});
	mainDef.notify();
	return promise;
};