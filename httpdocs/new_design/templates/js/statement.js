$(document ).on('ready', function(){
	templateEngine.load( '/new_design/templates/sidebar.html', {}, $( 'aside' ), true ).then( function () {
		templateEngine.load( '/new_design/templates/footer.html', {}, $( 'footer' ), true ).then( function () {
			templateEngine.load( '/new_design/templates/header.html', {title: 'Statements'}, $( 'header' ), true ).then( function () {
				templateEngine.load( '/new_design/templates/details.html', {}, $( '#account-details' ), true ).then( function () {
					ajaxExtend.options.templateDir = "/new_design/templates/ajaxExtend/";

					$( '#closeAccounts' ).on( 'click', function () {
						$( '#cardAccounts' ).hide();
					} );

					$( '#closeArrears' ).on( 'click', function () {
						$( '#cardArrears' ).hide();
					} );

					$( '#statementAccount' ).dataTemplates( {
						"templateDir": "/new_design/templates/dataTemplates/",
						"ajax": {
							"optionsUrl": "/new_design/controllers/statement.php?func=accountOptions",
							"url": "/new_design/controllers/statement.php?func=account",
							"templateUrl": "/new_design/templates/dataTemplates/materialize/accountRow.html",
							"type": "POST"
						},
						"paging": true,
						"searching": true,
						"sorting": true
					} );

					$( '#statementArrears' ).dataTemplates( {
						"templateDir": "/new_design/templates/dataTemplates/",
						"ajax": {
							"optionsUrl": "/new_design/controllers/statement.php?func=accountOptions",
							"url": "/new_design/controllers/statement.php?func=account",
							"templateUrl": "/new_design/templates/dataTemplates/materialize/accountRow.html",
							"type": "POST"
						},
						"paging": true,
						"searching": true,
						"sorting": true
					} );
				});
			});
		});
	});
});