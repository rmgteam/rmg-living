$(document ).on('ready', function(){
	templateEngine.load('/new_design/templates/sidevert.html', {image: 'images/sidevert.png'}, $('#advert') ).then(function() {
		templateEngine.load( '/new_design/templates/footer.html', {}, $( 'footer' ), true ).then( function () {
			templateEngine.load( '/new_design/templates/sidebar.html', {}, $( 'aside' ), true ).then( function () {
				templateEngine.load( '/new_design/templates/header.html', {title: 'Archive'}, $( 'header' ), true ).then( function () {
					templateEngine.load( '/new_design/templates/details.html', {}, $( '#account-details' ), true ).then( function () {
						templateEngine.load( '/new_design/templates/announcementFilter.html', {}, $( '#announce-filter' ), true ).then( function () {
							var floodDesc = "We have been made aware that there is a severe flood warning in place along the River Aire. The purpose of this announcement is to advise you that the environment agency are advising residents, particularly on the ground floor to take reasonable actions to prepare for potential flooding. The environment agency have advised that residents on ground floors should:<br/><br/>"
							floodDesc += "1. Take precautions to protect valuables<br/>";
							floodDesc += "2. Place valuables high up within the property or remove to a safe location<br/>";
							floodDesc += "3. Ensure mobile phones are charged and in the event of an emergency to contact 999.<br/>";
							floodDesc += "4. Do not venture into flood waters<br/>";
							floodDesc += "5. Do not drive unless absolutely necessary<br/><br/>";
							floodDesc += "The environment agency are monitoring the levels. RMG Personnel are liaising with the authorities to ensure that we are kept up to date with progress.";

							var data = [
								{
									'title': 'Flooding on Site',
									'description': 'We have been made aware that there is a severe flood warning in place along the River Aire. The purpose of this announcement is to advise you that the environment agency are advising residents, particularly on the ground floor to take reasonable actions to prepare for potential flooding.',
									'long-description': floodDesc,
									'type': 'announce',
									'id': 1,
									'date': {
										"date": "2015-11-29 00:00:00.000000",
										"timezone_type": 3,
										"timezone": "Europe\/Dublin"
									}
								},
								{
									'title': 'New Newsletter Available',
									'description': 'January 2016',
									'colour': 'blue',
									'type': 'colour',
									'link': '/new_design/newsletter.pdf',
									'other': 'Newsletter',
									'id': 2,
									'date': {
										"date": "2015-01-01 00:00:00.000000",
										"timezone_type": 3,
										"timezone": "Europe\/Dublin"
									}
								},
								{
									'title': 'New Budget Available',
									'description': '2016 Budget',
									'colour': 'green',
									'type': 'colour',
									'link': '/new_design/budget.xlsx',
									'other': 'Budget',
									'id': 3,
									'date': {
										"date": "2015-12-12 00:00:00.000000",
										"timezone_type": 3,
										"timezone": "Europe\/Dublin"
									}
								},
								{
									'title': 'New Site Meeting Available',
									'description': 'AGM Meeting',
									'colour': 'yellow',
									'type': 'colour',
									'link': '/new_design/agm-meeting.pdf',
									'other': 'Site Meeting',
									'id': 4,
									'date': {
										"date": "2015-11-29 00:00:00.000000",
										"timezone_type": 3,
										"timezone": "Europe\/Dublin"
									}
								}
							];

							processAnnouncement( data ).then( function () {
								$( '#search-form-clear-button' ).on( 'click', function () {
									$( '#search-form-search-input' ).val( '' );
								} )
							} );
						});
					} );
				} );
			} );
		});
	});
});

var processAnnouncement = function(data) {
	var mainDef = new $.Deferred();
	var promise = mainDef.promise();
	promise.progress(function() {
		var top = "";
		var bottom = "";
		templateEngine.load('/new_design/templates/js/asyncProcessAnnouncement.js.html', {}).then(function(indexTemplate) {
			$.each(data, function (i, v) {
				top += templateEngine.execute(indexTemplate, {
					'obj': 'data',
					'i': i
				});
				bottom += "});"
			});

			var string = top + "mainDef.resolve();" + bottom;
			eval(string);
		});
	});
	mainDef.notify();
	return promise;
};

var processGallery = function(data) {
	var mainDef = new $.Deferred();
	var promise = mainDef.promise();
	promise.progress(function() {
		var top = "";
		var bottom = "";
		templateEngine.load('/new_design/templates/js/asyncProcessGallery.js.html', {}).then(function(indexTemplate) {
			$.each(data, function (i, v) {
				top += templateEngine.execute(indexTemplate, {
					'obj': 'data',
					'i': i
				});
				bottom += "});"
			});

			var string = top + "mainDef.resolve();" + bottom;
			eval(string);
		});
	});
	mainDef.notify();
	return promise;
};