$(document ).on('ready', function(){
	templateEngine.load('/new_design/templates/full-header.html', {}, $('header'), true).then(function(){
		templateEngine.load( '/new_design/templates/footer.html', {}, $( 'footer' ), true ).then( function () {
			var data = [
				{
					'image': 'http://li.zoocdn.com/42412bfe0c2b0c4576134b3df5adfbdf88ba812f_645_430.jpg',
					'property': 'Lawrence Square Management Company (York) Ltd',
					'address1': '4 Fountayne House',
					'address2': 'Lawrence Square',
					'address3': '',
					'id': 1
				},
				{
					'image': 'http://li.zoocdn.com/b47a87582fca0a39f818d848ce09018438b2d68d_645_430.jpg',
					'property': 'Northumberland Court, London',
					'address1': 'Flat 10, Northumberland Court',
					'address2': '150 Hanworth Road',
					'address3': '',
					'id': 2
				},
				{
					'image': 'http://li.zoocdn.com/42412bfe0c2b0c4576134b3df5adfbdf88ba812f_645_430.jpg',
					'property': 'Lawrence Square Management Company (York) Ltd',
					'address1': '11 Skipton House',
					'address2': 'Lawrence Square',
					'address3': '',
					'id': 3
				}
			];
			templateEngine.load( '/new_design/templates/property-cards.html', data, $( '#properties' ), true );
		});
	});
});