<?
require_once("utils.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."rmc.class.php");
require_once($UTILS_CLASS_PATH."resident.class.php");
$website = new website;
$resident = new resident($_SESSION['resident_num']);
$rmc = new rmc;

// Determine if allowed access into 'your community' section
$website->allow_community_access();

// Set RMC
$rmc->set_rmc($_SESSION['rmc_num']);

// Work out 12 month cut-off date
$today = $thistime - (86400 * 366);
$cut_off_day = date("Ymd", $today);

// Get any announcements
$sql_announce = "SELECT * FROM cpm_announcements a, cpm_announcements_read ar WHERE a.announce_approved = 'Y' AND a.announce_id=ar.announce_id AND a.rmc_num = ".$_SESSION['rmc_num']." AND a.announce_date > '$cut_off_day' AND ar.resident_num = ".$_SESSION['resident_num']." ORDER BY a.announce_date DESC LIMIT 4";
$result_announce = @mysql_query($sql_announce);
$num_announce = @mysql_num_rows($result_announce);

// Get developer info
if($_SESSION['is_developer'] == "Y"){

	$resident->resident($_SESSION['resident_num']);
	$sql_dev = "SELECT * FROM cpm_developers WHERE developer_id = ".$resident->developer_id;
	$result_dev = @mysql_query($sql_dev);
	$row_dev = @mysql_fetch_array($result_dev);
}
else{
	if($rmc->rmc['developer_id'] != ""){
		$sql_dev = "SELECT * FROM cpm_developers WHERE developer_id = ".$rmc->rmc['developer_id'];
		$result_dev = @mysql_query($sql_dev);
		$row_dev = @mysql_fetch_array($result_dev);
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>RMG Living - General Info</title>
	<link href="/css/reset.css" rel="stylesheet" type="text/css" />
	<!-- <link href="styles.css" rel="stylesheet" type="text/css" /> -->
	<link href="/css/common.css" rel="stylesheet" type="text/css" />
	<!--[if lte IE 8]> 
	<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<!--[if lte IE 7]> 
	<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
	<![endif]-->
</head>
<body>

	<div id="wrapper">
	
		<? require_once($UTILS_FILE_PATH."includes/header.php");?>
		
		<div id="content">			
			
			<table width="760" cellspacing="0">
				<tr>
					<td><a href="building_details.php" class="crumbs">Your Community</a>&nbsp;&gt;&nbsp;General Info </td>
					<td style="text-align:right;" nowrap="nowrap"><? if(!empty($_SESSION['resident_session'])){?><a href="index.php?logoff=Y" class="crumbs">Log Off</a><? }?></td>
				</tr>
			</table>


			<table width="760" cellspacing="0" style="clear:both; margin-top:13px;">
				<tr>
					<td width="15"><img src="images/lblue_box_top_left_corner.jpg" width="15" height="33"></td>
					<td width="348" style="background-color:#669ACC;border-top:1px solid #6699cc; vertical-align:middle"><img src="images/property_icon.png" width="22" height="22" style="vertical-align:middle;margin-right:8px;"><a href="building_details.php" class="linkbox">Your Community</a></td>
					<td width="34"><img src="images/general_info/tab_join2.jpg" width="34" height="33"></td>
					<td width="348" style="background-color:#426B9F;border-top:1px solid #003366; vertical-align:middle"><img src="images/general_info/general_info_symbol.jpg" width="22" height="22" style="vertical-align:middle;margin-right:8px;"><span class="box_title">General Info</span></td>
					<td width="15" style="text-align:right;" align="right"><img src="images/dblue_box_top_right_corner.jpg" width="15" height="33"></td>
				</tr>
			</table>

			
			<div class="content_box_1">
									
				<table class="table_1" width="728" cellspacing="0">
					<tr>
						<td width="346"><b><span class="subt036">Our responsibilities as a Managing Agent</span></b></td>
						<td>&nbsp;</td>
						<td><b><span class="subt036">Glossary of Terms</span></b></td>
					</tr>
					<tr>
						<td valign="top">Learn what your Resident Management Company (<?=$rmc->rmc['rmc_name']?>) is and why it was created.<br>
							<strong><a href="man_co_responsibilities.php" class="link416CA0" style="text-decoration:underline ">Click here</a></strong></td>
						<td valign="top">&nbsp;</td>
						<td valign="top"><p>See our glossary of terms to understand some of the words and phrases used throughout this website.<br>
							<strong><a href="glossary.php" class="link416CA0" style="text-decoration:underline ">Click here</a></strong></p>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><b><span class="subt036">The responsibilities of your Resident Director(s)</span></b></td>
						<td>&nbsp;</td>
						<td><? if($rmc->rmc['developer_id'] != ""){?><b><span class="subt036">Your House Builder</span></b><? }?></td>
					</tr>
					<tr>
						<td valign="top">Learn how your Resident Director takes responsibility in running your Resident Management Company and how we endeavour to help them.<br>
							<strong><a href="director_responsibilities.php" class="link416CA0" style="text-decoration:underline ">Click here</a></strong></td>
						<td>&nbsp;</td>
						<td rowspan="9" valign="top">
							<?
							if($row_dev['developer_id'] != ""){
								?>
								<p>To learn more about your house builder, <?=$row_dev['developer_name']?>, or to contact their customer services department, please click the image below.<br>
								<br>
								<?
								$logo_file = "images/developers/".$row_dev['developer_id']."/general_".$row_dev['developer_id'].".jpg";
								if(file_exists($logo_file)){
									$dev_image = "<img src='".$logo_file."' border='0' alt='Click to visit the website for ".$row_dev['developer_name']."'>";
									if($row_dev['developer_url'] != ""){
										$dev_image = "<a href='".$row_dev['developer_url']."' target='_blank'>".$dev_image."</a>";
									}else{
										$dev_image = "<a href='#'>".$dev_image."</a>";
									}
									print $dev_image;
								}
							}
							?>
							</p>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td><b><span class="subt036">The responsibilities of your Management Company</span></b></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td valign="top"><p>This secton details how you the property owner, as a member of the Resident Management Company, should be involved in ensuring your queries are made to the right person.<br>
							<strong><a href="lessee_responsibilities.php" class="link416CA0" style="text-decoration:underline ">Click here</a></strong></p>
							</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr <? if($resident->is_subtenant_account == "Y"){?>style="visibility:hidden;"<? }?>>
						<td><b><span class="subt036">Your responsibilities as a Property Owner</span></b></td>
						<td>&nbsp;</td>
					</tr>
					<tr <? if($resident->is_subtenant_account == "Y"){?>style="visibility:hidden;"<? }?>>
						<td valign="top"><p>This section details our responsibilities as a Managing Agent acting on behalf of your Resident Management Company.<br><a href="agent_responsibilities.php" class="link416CA0" style="text-decoration:underline "><strong>Click here</strong></a></p></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td width="34">&nbsp;</td>
						<td width="348">&nbsp;</td>
					</tr>
				</table>
										
										
										
			</div>
								
			
			<div class="clearfix" style="margin:0 auto; width:760px; clear:both; margin-bottom:20px;">
								
				<div style="float:left; width:530px;">
		
					<table width="530" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="15"><img src="images/lblue_box_top_left_corner.jpg" width="15" height="33" /></td>
							<td width="250" style="background-color:#669ACC;border-top:1px solid #6699cc; vertical-align:middle"><img src="images/your_community/announcements_symbol.jpg" width="22" height="22" style="vertical-align:middle;" />&nbsp;&nbsp;<span class="box_title">Announcements</span></td>
							<td width="250" class="box_title" style="text-align:right;background-color:#669ACC;border-top:1px solid #6699cc;"><a href="announcements.php" class="linkbox">View All</a></td>
							<td width="15" style="text-align:right;"><img src="images/lblue_box_top_right_corner.jpg" width="15" height="33" /></td>
						</tr>
					</table>
					<div class="yc_announcements">
						<table width="528" cellspacing="0">
							<?
							if($num_announce > 0){
				
								$row_counter = 1;
								// display announcements
								while($row_announce = @mysql_fetch_array($result_announce)){
									?>
									<tr>
										<td width="82" nowrap="nowrap"><?=substr($row_announce['announce_date'],6,2)."/".substr($row_announce['announce_date'],4,2)."/".substr($row_announce['announce_date'],0,4)?>&nbsp;</td>
										<td class="text"><a href="announcements.php?id=<?=$row_announce['announce_id']?>&amp;whichaction=view" class="link416CA0" style="text-decoration:underline;" <? if($row_announce['announce_read'] == 'N'){?>style="font-weight:bold;"<? }?>><? print substr($row_announce['announce_title'],0,50);if(strlen($row_announce['announce_title']) > 50){print "...";}?></a>&nbsp;</td>
									</tr>
									<?
									$row_counter++;
								}
							}
							else{
								?>
								<tr><td colspan="2">There are no announcements</td></tr>
								<?
							}
							?>
						</table>
					</div>
				</div>
									
										
				<div style="float:right; width:215px;">

					<table width="215" cellspacing="0">
						<tr>
							<td width="15"><img src="images/lblue_box_top_left_corner.jpg" width="15" height="33" /></td>
							<td width="185" style="vertical-align:middle; background-color:#669ACC;"><img src="images/person_icon.png" width="22" height="22" style="vertical-align:middle;margin-right:8px;" /><span class="box_title">My Account</span> </td>
							<td width="15" style="text-align:right;"><img src="images/lblue_box_top_right_corner.jpg" width="15" height="33" /></td>
						</tr>
					</table>
					<div style="width:183px; height:105px;padding:15px;border-left:1px solid #7ea0bd;border-right:1px solid #7ea0bd;border-top:1px solid #7ea0bd;border-bottom:2px solid #003366;">
						<table width="183" cellspacing="0">
							<tr>
								<td style="padding:0 0 15px 0;">Change your personal details or, if you own multiple properties, create a single account to manage them with.</td>
							</tr>
							<tr>
								<td valign="bottom"><a href="<? if($_SESSION['master_account_serial'] != ""){?>ma_change_details.php<? }else{?>change_details.php<? }?>" title="Manage your account"><img src="images/your_community/manage_account_button.jpg" name="change_button" width="72" height="20" border="0" id="change_button" /></a></td>
							</tr>
						</table>
					</div>
			
				</div>
			
			</div>					
			
		</div>
		
		<? require_once($UTILS_FILE_PATH."includes/footer.php");?>
		
	</div>
		
</body>
</html>
