<?
require_once("utils.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."rmc.class.php");
require_once($UTILS_CLASS_PATH."resident.class.php");
require_once($UTILS_CLASS_PATH."unit.class.php");
require_once($UTILS_CLASS_PATH."login.class.php");
require_once($UTILS_CLASS_PATH."master_account.class.php");
require_once($UTILS_CLASS_PATH."security.class.php");
$website = new website;
$resident = new resident($_SESSION['resident_num']);
$unit = new unit;
$unit->set_unit($_SESSION['resident_num']);
$rmc = new rmc;
$login = new login;
$master_account = new master_account($_SESSION['master_account_serial']);
$security = new security;

// Determine if allowed access into 'your community' section
$website->allow_community_access();

// Set RMC
$rmc->set_rmc($_SESSION['rmc_num']);

// Do login to Master Account...
if($_REQUEST['a'] == "dl"){
	
	$save_result = $login->do_master_account_login($_REQUEST['username'], $_REQUEST['password'], $_REQUEST['do_auto_merge']);
	if($save_result[0] === true){
		$master_account = new master_account($_SESSION['master_account_serial']);
	}
}

// Switch property
if($_REQUEST['a'] == "sp"){
	
	// Check that the selected property (resident) is part of the Master Account that the user is logged under
	if( $master_account->is_resident_assoc($_REQUEST['s']) === true ){
		
		if( $master_account->switch_property($_REQUEST['s']) === true ){
		
			print "success";
			exit;
		}
	}
}

// Save optin
if($_REQUEST['a'] == "so"){
	
	$sql = "
	UPDATE cpm_residents_extra SET 
		announce_optin = '".$security->clean_query($_REQUEST['v'])."' 
	WHERE resident_num = ".$security->clean_query($_REQUEST['s']);
	if(@mysql_query($sql)){	print "success"; }
	exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<title>RMG Living - My Properties</title>
	<link href="/css/reset.css" rel="stylesheet" type="text/css" />
	<!-- <link href="styles.css" rel="stylesheet" type="text/css" /> -->
	<link href="/css/common.css" rel="stylesheet" type="text/css" />
	<!--[if lte IE 8]> 
	<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<!--[if lte IE 7]> 
	<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
	<![endif]-->
	<script type="text/javascript" src="/library/jscript/jquery-1.4.2.min.js"></script>

	<script type="text/javascript">
	function do_login(){
		document.form1.submit();
	}

	function switch_property(s){

		$.post("my_properties.php?a=sp&s=" + s,

			function(data){

				if(data == "success"){

					location.href = "/building_details.php";
					return false;
				}
			},"html");
	}

	function do_filter(){
		document.filterform1.submit();
	}

	function do_optin(s){

		var val = "N";
		if( $("#optin_" + s).attr("checked") == true ){
			val = "Y";
		}

		$.post("my_properties.php?a=so&v=" + val + "&s=" + s,

			function(data){

			},"html");
	}
	</script>
	<? require_once($UTILS_FILE_PATH."includes/analytics.php");?>
</head>
<body>

	<div id="wrapper">
	
		<? require_once($UTILS_FILE_PATH."includes/header.php");?>
		
		<div id="content">			
			
			<table width="760" cellspacing="0">
				<tr>
					<td><a href="/building_details.php" class="crumbs">Your Community</a>&nbsp;&gt;&nbsp;My Account&nbsp;&gt;&nbsp;My Properties</td>
					<td style="text-align:right;" nowrap="nowrap"><? if(!empty($_SESSION['resident_session'])){?><a href="index.php?logoff=Y" class="crumbs">Log Off</a><? }?></td>
				</tr>
			</table>


			<table width="760" cellspacing="0" style="clear:both; margin-top:13px;">
				<tr>
					<td width="15"><img src="images/lblue_box_top_left_corner.jpg" width="15" height="33"></td>
					<td width="348" style="background-color:#669ACC;border-top:1px solid #6699cc; vertical-align:middle"><img src="images/person_icon.png" width="22" height="22" style="vertical-align:middle;margin-right:8px;"><a href="<? if($_SESSION['master_account_serial'] == ""){?>change_details.php<? }else{?>ma_change_details.php<? }?>" class="box_title linkbox">My Details</a></td>
					<td width="34"><img src="images/general_info/tab_join2.jpg" width="34" height="33"></td>
					<td width="348" style="background-color:#426B9F;border-top:1px solid #003366; vertical-align:middle"><img src="images/property_icon.png" width="22" height="22" style="vertical-align:middle;margin-right:8px;"><span class="box_title">My Properties</span></td>
					<td width="15" style="text-align:right;" align="right"><img src="images/dblue_box_top_right_corner.jpg" width="15" height="33"></td>
				</tr>
			</table>

			
			<div class="content_box_1" style="min-height:400px; background-image:url(images/my_properties/houses.jpg); background-position:top right; background-repeat:no-repeat;">
									
				<?
				// If not logged in with master account...
				if($_SESSION['master_account_serial'] == ""){
					?>
					
					<h4>Access all your properties with one login</h4>
					<p style="width:360px;">If you have more than one property that is managed by RMG you can create a Master Account which will enable you to use just a single login to view their details.</p>
					<a href="create_master_account.php" style="display:block;margin-top:12px;width:190px;height:20px;" title="Create a new Master Account"><img src="images/my_properties/create_master_account_button.jpg" border="0" /></a>
					
					<h5 style="margin-top:35px; font-weight:bold;">Already have a Master Account?</h5>
					<p style="width:360px;">If you already have a Master Account please login below:</p>
					
					<? if($_REQUEST['a'] == "dl" && $save_result[0] !== true){?>
					<div class="msg_fail" style="margin-top:10px;width:360px;"><?=$save_result[1]?></div>
					<? }?>
					
					<form name="form1" id="form1">
						<input type="hidden" name="a" id="a" value="dl" />
						<table class="table_1" width="500" cellspacing="0" style="margin-top:15px;">
							<tr>
								<td style="width:80px;">Login ID:</td>
								<td><input type="text" name="username" id="username" value="<?=$_REQUEST['username']?>" size="30" autocomplete="off" /></td>
							</tr>
							<tr>
								<td>Password:</td>
								<td><input type="password" name="password" id="password" value="<?=$_REQUEST['password']?>" size="30" autocomplete="off" /></td>
							</tr>
							<tr>
								<td colspan="2" style="padding:8px 0px 8px 0px;"><input type="checkbox" name="do_auto_merge" id="do_auto_merge" value="Y" />&nbsp;Automatically add this property <? if($unit->get_unit_desc() != ""){print "'".stripslashes($unit->get_unit_desc())."'";}?> to my Master Account.</td>
							</tr>
							<tr>
								<td colspan="2" style="padding-top:7px;"><a href="Javascript:;" onclick="do_login();return false;" title="Login to your existing Master Account"><img src="images/index/login_button.jpg" border="0" /></a></td>
							</tr>
						</table>
					</form>
					
					<?
				}
				else{
					?>
					
					<h4>My Properties</h4>
					
					<?
					if($_REQUEST['ma_complete'] == "Y"){
						?>
						<h5 style="margin-top:10px;margin-bottom:10px;">Congratulations! Your Master Account is now active.</h5>
						<?
					}
					?>
					
					<p style="width:360px; padding-bottom:10px;">If you have more properties that you would like to manage via your master account, simply click the 'Add a Property' button below.</p>
					<a href="ma_add_property.php" title="Add a Property"><img src="images/my_properties/add_a_property_button.jpg" border="0" /></a>
					<br /><br /><br /><br />
					<p style="width:360px;">Below is a list containing the properties that you have added to your master account. To manage a particular property, e.g. view the statement of account, make a payment etc., simply click on the property name below and you will be re-directed to the relevant page.</p>
					
					<?
					// List properties assoc with master account...
					$result_prop = $master_account->get_master_account_assoc_residents($_REQUEST['man_co_filter'], $_REQUEST['prop_filter']);
					$num_prop = @mysql_num_rows($result_prop);
					
					// If more than 10 properties, show search function
					if($num_prop > 10){
						?>
						<br />
						<form name="filterform1" id="filterform1">
							<input type="hidden" name="filter" id="filter" value="Y" />
							<fieldset class="fieldset_1" style="margin-top:15px;width:703px;">
								<table class="table_3" cellspacing="0">
									<tr>
										<td style="width:110px;">Management Co.</td>
										<td>
											<select name="man_co_filter" id="man_co_filter" style="width:200px;">
												<option value="">(All)</option>
												<?
												$result_man_co = $master_account->get_master_account_assoc_rmcs();
												while($row_man_co = @mysql_fetch_array($result_man_co)){
													?>
													<option value="<?=$row_man_co['rmc_num']?>" <? if($_REQUEST['man_co_filter'] == $row_man_co['rmc_num']){?>selected="selected"<? }?>><?=stripslashes($row_man_co['rmc_name'])?></option>
													<?	
												}
												?>
											</select>
										</td>
										<td style="width:100px; padding-left:30px;">Property name</td>
										<td>
											<input type="text" name="prop_filter" id="prop_filter" value="<?=$_REQUEST['prop_filter']?>" style="width:150px;" />
										</td>
										<td style="padding-left:20px; vertical-align:middle;">
											<a href="Javascript:;" onclick="do_filter();return false;" title="Search your properties" style="margin:0; padding:0;"><img src="/images/search_button.jpg" border="0" /></a>
										</td>
									</tr>
								</table>
							</fieldset>
						</form>
						<?
					}
					?>
					
					<table class="table_2" cellspacing="0" style="margin-top:15px;width:725px;">
						<tr>
							<th style="border-bottom:2px solid #333;">Management Co.</th>
							<th style="border-bottom:2px solid #333;">Property name</th>
							<th style="border-bottom:2px solid #333;width:110px;" class="end">Announcements Opt-in</th>
						</tr>
						
						<?
						if($num_prop > 0){
								
							$this_unit = new unit;
							$this_rmc = new rmc;
							while($row_prop = @mysql_fetch_array($result_prop)){	
								
								$this_rmc->set_rmc($row_prop['rmc_num']);
								$this_unit->set_unit($row_prop['resident_num']);
								?>
								<tr>
									<td><?=stripslashes($this_rmc->rmc_name)?></td>
									<td><a href="Javascript:;" onclick="switch_property('<?=$row_prop['resident_serial']?>');return false;" title="Click to view..."><?=stripslashes($this_unit->get_unit_desc())?></a></td>
									<td class="end"><input type="checkbox" name="optin_<?=$row_prop['resident_num']?>" id="optin_<?=$row_prop['resident_num']?>" <? if($row_prop['announce_optin'] == "Y"){?>checked="checked"<? }?> onclick="do_optin('<?=$row_prop['resident_num']?>')" /></td>
								</tr>
								<?
							}
						}
						else{
							?>
							<tr><td colspan="3" class="end"><? if($_REQUEST['filter'] == "Y"){?>No properties matched your search criteria.<? }else{?>You do not have any properties linked to your Master Account.<? }?></td></tr>
							<?	
						}
						?>
					</table>
					
					<?
				}
				?>
												
			</div>

		</div>
		
		<? require_once($UTILS_FILE_PATH."includes/footer.php");?>
		
	</div>

</body>
</html>
