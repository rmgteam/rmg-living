<?
require("utils.php");
require_once($UTILS_CLASS_PATH."security.class.php");
$security = new security;


// Function to get MIME type
function getMime($file){
	$mimetype = array( 
		'doc'=>'application/msword',
		'eps'=>'application/postscript',
		'htm'=>'text/html',
		'html'=>'text/html',
		'jpg'=>'image/jpeg',
		'pdf'=>'application/pdf',
		'txt'=>'text/plain',
		'xls'=>'application/vnd.ms-excel'
	);
	$p = explode('.', $file);
	$pc = count($p);

	// send headers 
	if($pc > 1 AND isset($mimetype[$p[$pc - 1]])){ 
		header("Content-Type: " . $mimetype[$p[$pc - 1]] . "\n");
	} 
	else{ 
		header("Content-Type: application/octet-stream\n");
		header("Content-Disposition: attachment; filename=\"$file\"\n"); 
	} 
}

//===========================================
// Get Budget file
//===========================================
if($_REQUEST['type'] == "budget"){

	// Check that correct file is being requested
	$sql = "
	SELECT budget_file 
	FROM cpm_budgets 
	WHERE 
	budget_file_stamp = '".$security->clean_query($_REQUEST['id'])."' AND 
	rmc_num = ".$security->clean_query($_REQUEST['rmc_num']);
	$result = mysql_query($sql);
	$row = mysql_fetch_row($result);
	$file = $UTILS_PRIVATE_FILE_PATH."budgets/".$row[0];  
	if(!is_file($file) || !file_exists($file)){exit;}
	
	// get file
	$fp = fopen($file, "rb") or die(); 
	while(!feof($fp)){ 
		$data .= fread($fp, 1024); 
	} 
	fclose($fp);
	
	// Get MIME type
	getMime($file);
	
	// send file contents 
	header("Content-Transfer-Encoding: binary\n"); 
	header("Content-length: " . strlen($data) . "\n");
	print($data);
}

//===========================================
// Get Car Park file
//===========================================
if($_REQUEST['type'] == "carpark"){

	// Check that correct file is being requested
	$sql = "
	SELECT car_park_file 
	FROM cpm_car_park 
	WHERE 
	car_park_file_stamp = '".$security->clean_query($_REQUEST['id'])."' AND 
	rmc_num = ".$security->clean_query($_REQUEST['rmc_num']);
	$result = mysql_query($sql);
	$row = mysql_fetch_row($result);
	$file = $UTILS_PRIVATE_FILE_PATH."car_park_plans/".$row[0];  
	if(!is_file($file) || !file_exists($file)){print "This file does not exist.";exit;}
	
	// get file
	$fp = fopen($file, "rb") or die(); 
	while(!feof($fp)){ 
		$data .= fread($fp, 1024); 
	} 
	fclose($fp);
	
	// Get MIME type
	getMime($file);
	
	// send file contents 
	header("Content-Transfer-Encoding: binary\n"); 
	header("Content-length: " . strlen($data) . "\n");
	print($data);
}


//===========================================
// Get Meeting Notes file
//===========================================
if($_REQUEST['type'] == "meeting_notes"){

	// Check rmc_num against session variable
	if( $_REQUEST['rmc_num'] != $_SESSION['rmc_num'] ){
		exit;
	}
	
	// Check that correct file is being requested
	$sql = "
	SELECT agm_file 
	FROM cpm_agm 
	WHERE 
	agm_file_stamp = '".$security->clean_query($_REQUEST['id'])."' AND 
	rmc_num = ".$security->clean_query($_REQUEST['rmc_num']);
	$result = mysql_query($sql);
	$row = mysql_fetch_row($result);
	$file = $UTILS_MEETING_NOTES_PATH.$row[0];  
	if(!is_file($file) || !file_exists($file)){exit;}
	
	// get file
	$fp = fopen($file, "rb") or die(); 
	while(!feof($fp)){ 
		$data .= fread($fp, 1024); 
	} 
	fclose($fp);
	
	// Get MIME type
	getMime($file);
	
	// send file contents 
	header("Content-Transfer-Encoding: binary\n"); 
	header("Content-length: " . strlen($data) . "\n");
	print($data);
}

//===========================================
// Get Site Visit file
//===========================================
if($_REQUEST['type'] == "site_visit"){

	// Check rmc_num against session variable
	if( $_REQUEST['rmc_num'] != $_SESSION['rmc_num'] ){
		exit;
	}
	
	// Check that correct file is being requested
	$sql = "
	SELECT site_visit_file 
	FROM cpm_site_visits 
	WHERE 
	site_visit_file_stamp = '".$security->clean_query($_REQUEST['id'])."' AND 
	rmc_num = ".$security->clean_query($_REQUEST['rmc_num']);
	$result = mysql_query($sql);
	$row = mysql_fetch_row($result);
	$file = $UTILS_SITE_VISITS_PATH.$row[0];  
	if(!is_file($file) || !file_exists($file)){exit;}
	
	// get file
	$fp = fopen($file, "rb") or die(); 
	while(!feof($fp)){ 
		$data .= fread($fp, 1024); 
	} 
	fclose($fp);
	
	// Get MIME type
	getMime($file);
	
	// send file contents 
	header("Content-Transfer-Encoding: binary\n"); 
	header("Content-length: " . strlen($data) . "\n");
	print($data);
}

//===========================================
// Get Ownership Documents file
//===========================================
if($_REQUEST['type'] == "lease"){

	// Check that correct file is being requested
	$sql = "
	SELECT lease_file 
	FROM cpm_leases 
	WHERE 
	lease_file_stamp = '".$security->clean_query($_REQUEST['id'])."' AND 
	rmc_num = ".$security->clean_query($_REQUEST['rmc_num']);
	$result = mysql_query($sql);
	$row = mysql_fetch_row($result);
	$file = $UTILS_PRIVATE_FILE_PATH."leases/".$row[0];  
	if(!is_file($file) || !file_exists($file)){exit;}
	
	// get file
	$fp = fopen($file, "rb") or die(); 
	while(!feof($fp)){ 
		$data .= fread($fp, 1024); 
	} 
	fclose($fp);
	
	// Get MIME type
	getMime($file);
	
	// send file contents 
	header("Content-Transfer-Encoding: binary\n"); 
	header("Content-length: " . strlen($data) . "\n");
	print($data);
}


//===========================================
// Get SLA file
//===========================================
if($_REQUEST['type'] == "sla"){

	// Check that correct file is being requested
	$sql = "
	SELECT sla_file 
	FROM cpm_sla 
	WHERE 
	sla_file_stamp = '".$security->clean_query($_REQUEST['id'])."' AND 
	rmc_num = ".$security->clean_query($_REQUEST['rmc_num']);
	$result = mysql_query($sql);
	$row = mysql_fetch_row($result);
	$file = $UTILS_PRIVATE_FILE_PATH."sla/".$row[0];  
	if(!is_file($file) || !file_exists($file)){exit;}
	
	// get file
	$fp = fopen($file, "rb") or die(); 
	while(!feof($fp)){ 
		$data .= fread($fp, 1024); 
	} 
	fclose($fp);
	
	// Get MIME type
	getMime($file);
	
	// send file contents 
	header("Content-Transfer-Encoding: binary\n"); 
	header("Content-length: " . strlen($data) . "\n");
	print($data);
}


//===========================================
// Get Site Specific Maintenance file
//===========================================
if($_REQUEST['type'] == "ssm"){

	// Check that correct file is being requested
	$sql = "
	SELECT ssm_file 
	FROM cpm_ssm 
	WHERE 
	ssm_file_stamp = '".$security->clean_query($_REQUEST['id'])."' AND 
	rmc_num = ".$security->clean_query($_REQUEST['rmc_num']);
	$result = mysql_query($sql);
	$row = mysql_fetch_row($result);
	$file = $UTILS_PRIVATE_FILE_PATH."ssm/".$row[0];  
	if(!is_file($file) || !file_exists($file)){exit;}
	
	// get file
	$fp = fopen($file, "rb") or die(); 
	while(!feof($fp)){ 
		$data .= fread($fp, 1024); 
	} 
	fclose($fp);
	
	// Get MIME type
	getMime($file);
	
	// send file contents 
	header("Content-Transfer-Encoding: binary\n"); 
	header("Content-length: " . strlen($data) . "\n");
	print($data);
}


//===========================================
// Get Site Specific Maintenance file
//===========================================
if($_REQUEST['type'] == "client"){

	// Check that correct file is being requested
	$sql = "
	SELECT client_file
	FROM cpm_client
	WHERE
	client_file_stamp = '".$security->clean_query($_REQUEST['id'])."' AND
	rmc_num = ".$security->clean_query($_REQUEST['rmc_num']);
	$result = mysql_query($sql);
	$row = mysql_fetch_row($result);
	$file = $UTILS_PRIVATE_FILE_PATH."client/".$row[0];
	if(!is_file($file) || !file_exists($file)){exit;}

	// get file
	$fp = fopen($file, "rb") or die();
	while(!feof($fp)){
		$data .= fread($fp, 1024);
	}
	fclose($fp);

	// Get MIME type
	getMime($file);

	// send file contents
	header("Content-Transfer-Encoding: binary\n");
	header("Content-length: " . strlen($data) . "\n");
	print($data);
}


//===========================================
// Get Letter file
//===========================================
if($_REQUEST['type'] == "letter"){

	// Check that correct file is being requested
	$sql = "
	SELECT letter_file 
	FROM cpm_letters 
	WHERE 
	letter_file_stamp = '".$security->clean_query($_REQUEST['id'])."' AND 
	rmc_num = ".$security->clean_query($_REQUEST['rmc_num']);
	$result = mysql_query($sql);
	$row = mysql_fetch_row($result);
	$file = $UTILS_PRIVATE_FILE_PATH."letters/".$row[0];  
	if(!is_file($file) || !file_exists($file)){exit;}
	
	// get file
	$fp = fopen($file, "rb") or die(); 
	while(!feof($fp)){ 
		$data .= fread($fp, 1024); 
	} 
	fclose($fp);
	
	// Get MIME type
	getMime($file);
	
	// send file contents 
	header("Content-Transfer-Encoding: binary\n"); 
	header("Content-length: " . strlen($data) . "\n");
	print($data);
}


//===========================================
// Get Newsletter file
//===========================================
if($_REQUEST['type'] == "newsletter"){

	// Check that correct file is being requested
	$sql = "
	SELECT newsletter_file 
	FROM cpm_newsletters 
	WHERE 
	newsletter_file_stamp = '".$security->clean_query($_REQUEST['id'])."' AND 
	rmc_num = ".$security->clean_query($_REQUEST['rmc_num']);
	$result = mysql_query($sql);
	$row = mysql_fetch_row($result);
	$file = $UTILS_PRIVATE_FILE_PATH."newsletters/".$row[0];  
	if(!is_file($file) || !file_exists($file)){exit;}
	
	// get file
	$fp = fopen($file, "rb") or die(); 
	while(!feof($fp)){ 
		$data .= fread($fp, 1024); 
	} 
	fclose($fp);
	
	// Get MIME type
	getMime($file);
	
	// send file contents 
	header("Content-Transfer-Encoding: binary\n"); 
	header("Content-length: " . strlen($data) . "\n");
	print($data);
}


//===========================================
// Get Insurance Schedule
//===========================================
if($_REQUEST['type'] == "ins_sched"){
	
	// Check that correct file is being requested
	if($_REQUEST['ud'] == "d"){
		
		$sql = "
		SELECT i.ins_sched_filename  
		FROM ins_sched i LEFT JOIN rmcs r ON (i.rmc_num = r.rmc_num) LEFT JOIN directors d2 ON (r.owner_id = d2.director_owner_id) LEFT JOIN directors d1 ON (i.owner_id = d1.director_owner_id) 
		WHERE 
		(
			(	
				d1.director_ref <> '' AND 
				d1.director_ref IS NOT NULL AND 
				d1.director_ref = '".$security->clean_query($_SESSION['resident_ref'])."' AND 
				i.owner_id = ".$security->clean_query($_REQUEST['owid'])." 
			)
			OR
			( 
				d2.director_ref <> '' AND 
				d2.director_ref IS NOT NULL AND 
				d2.director_ref = '".$security->clean_query($_SESSION['resident_ref'])."' AND 
				r.owner_id = ".$security->clean_query($_REQUEST['owid'])." 
			)
		) 
		AND 
		i.ins_sched_reviewed = 'N' AND 
		i.ins_sched_discon = 'N' AND 
		i.ins_sched_is_visible = 'Y' AND 
		i.ins_sched_filename <> '' AND 
		i.ins_sched_filename IS NOT NULL AND 
		i.ins_sched_serial = '".$security->clean_query($_REQUEST['s'])."' 
		";
		//print $sql;
	}
	elseif($_REQUEST['ud'] == "u"){
		
		$sql = "
		SELECT i.ins_sched_filename  
		FROM ins_sched i, ins_sched_unit_assoc ua, units u, lookup_unit lu, lookup_rmc lrmc   
		WHERE 
		i.ins_sched_id = ua.ins_sched_id AND 
		ua.unit_num = u.unit_num AND 
		u.unit_num = lu.unit_lookup AND 
		u.rmc_num = lrmc.rmc_lookup AND 
		lu.unit_ref <> '' AND 
		lu.unit_ref IS NOT NULL AND 
		lu.unit_ref = '".$security->clean_query($_SESSION['unit_ref'])."' AND 
		lrmc.rmc_ref <> '' AND 
		lrmc.rmc_ref IS NOT NULL AND 
		lrmc.rmc_ref = '".$security->clean_query($_SESSION['rmc_ref'])."' AND 
		i.ins_sched_reviewed = 'N' AND 
		i.ins_sched_discon = 'N' AND 
		i.ins_sched_is_visible = 'Y' AND 
		i.ins_sched_filename <> '' AND 
		i.ins_sched_filename IS NOT NULL  AND 
		i.ins_sched_serial = '".$security->clean_query($_REQUEST['s'])."' 
		";
	}
	$result = mysql_query($sql, $UTILS_INTRANET_DB_LINK);
	$row = mysql_fetch_row($result);
	$file = $UTILS_INS_SCHED_PATH.$row[0];  
	if(!is_file($file) || !file_exists($file)){exit;}
	
	// get file
	$fp = fopen($file, "rb") or die(); 
	while(!feof($fp)){ 
		$data .= fread($fp, 1024); 
	} 
	fclose($fp);
	
	// Get MIME type
	getMime($file);
	
	// send file contents 
	header("Content-Transfer-Encoding: binary\n"); 
	header("Content-length: " . strlen($data) . "\n");
	print($data);
}

?>