<?
require_once("utils.php");
require_once($UTILS_SERVER_ROOT."library/functions/get_file_size.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."rmc.class.php");
require_once($UTILS_CLASS_PATH."resident.class.php");
$website = new website;
$resident = new resident($_SESSION['resident_num']);
$rmc = new rmc;

// Determine if allowed access into 'your community' section
$website->allow_community_access();

// Set rmc
$rmc->set_rmc($_SESSION['rmc_num']);

$docs_available = 0;
 
// Get letters of interest for this RMC
$num_letters=0;
$sql_letters = "SELECT * FROM cpm_letters l, cpm_letter_types lt WHERE l.letter_type_id=lt.letter_type_id AND l.rmc_num = ".$rmc->rmc['rmc_num']." ORDER BY l.letter_uploaded_ts DESC";
$result_letters = @mysql_query($sql_letters);
$num_letters = @mysql_num_rows($result_letters);
$docs_available += $num_letters;

// Get car park plan(s)
$sql_car_park = "SELECT * FROM cpm_car_park cp, cpm_car_park_types cpt WHERE cp.car_park_type_id=cpt.car_park_type_id AND cp.rmc_num = ".$rmc->rmc['rmc_num'];
$result_car_park = @mysql_query($sql_car_park);
$num_car_park_plans = @mysql_num_rows($result_car_park);
$docs_available += $num_car_park_plans;

// Get SLA
$num_slas = 0;
$num_client = 0;
if( $resident->is_resident_director == "Y" && $resident->is_subtenant_account != "Y" ){
	$sql_sla = "SELECT * FROM cpm_sla WHERE rmc_num = ".$rmc->rmc['rmc_num'];
	$result_sla = @mysql_query($sql_sla);
	$num_slas = @mysql_num_rows($result_sla);

// Get client documents
	$sql_client = "SELECT * FROM cpm_client WHERE rmc_num = ".$rmc->rmc['rmc_num'];
	$result_client = @mysql_query($sql_client);
	$num_client = @mysql_num_rows($result_client);
}
$docs_available += $num_slas;
$docs_available += $num_client;

// Get Site Specific Maintenance document
$sql_ssm = "SELECT * FROM cpm_ssm WHERE rmc_num = ".$rmc->rmc['rmc_num'];
$result_ssm = @mysql_query($sql_ssm);
$num_ssm = @mysql_num_rows($result_ssm);
$docs_available += $num_ssm;

// Get newsletters
$sql_newsletters = "SELECT * FROM cpm_newsletters WHERE rmc_num = ".$rmc->rmc['rmc_num']." ORDER BY newsletter_issue_date DESC";
$result_newsletters = @mysql_query($sql_newsletters);
$num_newsletters = @mysql_num_rows($result_newsletters);
$docs_available += $num_newsletters;

// Display curteous message if no documents exist
if($docs_available == 0){
	$page_intro = "This page provides a list of documents pertinent to your Management Company, such as letters of interest, site maintenance agreements and management agreements.<br><br>Unfortunately there are no documents present as yet, however, new documents may be added to this section over the course of time.";
}
else{
	$page_intro = "Below is a list of documents pertinent to your Management Company. New documents may be added to this section over the course of time.";
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><!-- InstanceBegin template="/Templates/main.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- InstanceBeginEditable name="doctitle" -->
<title>RMG Living - Useful Documents</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<link href="styles.css" rel="stylesheet" type="text/css">
<!--[if lte IE 8]> 
<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if lte IE 7]> 
<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
<![endif]-->
</head>
<body>
<? if($_SESSION['is_demo_account'] == "Y" && $_SESSION['is_developer'] != "Y"){?>
<form method="post" action="/building_details.php" style="margin:0; padding:0;">
<input type="hidden" name="a" value="cl">
<input type="hidden" name="s" value="gkfcbk45cgtf5kngn7kns5cg7snk5g7nsv5ng">
<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" style="margin:0; padding:0;">
	<tr>
		<td style="padding:4px;">Change Account:&nbsp;<input type="text" name="change_lessee" value="<?=$_SESSION['resident_ref']?>" size="20"> <input type="submit" name="submit_button" value="Change"></td>
	</tr>
</table>
</form>
<? }?>
<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#E9EEF4" style="border-bottom:1px solid #c1d1e1;"><div align="center"><img src="images/templates/header_bar_grad2.jpg" alt=""></div></td>
  </tr>
  <tr>
    <td style="border-top:1px solid #ffffff;border-bottom:1px solid #ffffff;">
	<table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="180"><a href="<? print $UTILS_HTTPS_ADDRESS;if( isset($_SESSION['login']) && $_SESSION['login'] != ""){print "/building_details.php";}else{print "/index.php";}?>"><img src="/images/templates/rmg_living_logo.jpg" style="margin-top:10px;" width="180" alt="RMG Living" border="0"></a></td>
        <td width="580" align="right">
		<?
		// User logged in is a 'developer', show any developer specific logos, or default to normal ones...
		if($_SESSION['is_developer'] == "Y"){
			
			$sql = "SELECT developer_id FROM cpm_residents_extra WHERE resident_num = ".$_SESSION['resident_num'];
			$result = @mysql_query($sql);
			$row = @mysql_fetch_row($result);
			if($row[0] != "" && file_exists($UTILS_FILE_PATH.'/images/developers/'.$row[0].'/header_'.$row[0].'.jpg') ){
				
				print '<img src="/images/developers/'.$row[0].'/header_'.$row[0].'.jpg" alt="RMG Living" width="580" height="86" BORDER=0>';
			}
			else{
				
				print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
			}
		}
		else{
		
			// Check to see if the Man. Co. has a developer association, if so, set developer specific images, or default to normal images...
			if( isset($_SESSION['login']) && $_SESSION['login'] != "" && $_SESSION['rmc_num'] != ""){
			
				$sql = "SELECT developer_id FROM cpm_rmcs_extra WHERE rmc_num = ".$_SESSION['rmc_num'];
				$result = @mysql_query($sql);
				$row = @mysql_fetch_row($result);
				if( $row[0] != "" && file_exists($UTILS_FILE_PATH.'/images/developers/'.$row[0].'/header_'.$row[0].'.jpg') ){
					
					print '<img src="/images/developers/'.$row[0].'/header_'.$row[0].'.jpg" alt="RMG Living" width="580" height="86" BORDER=0>';
				}
				else{
					
					print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
				}
			}
			else{
				
				print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
			}
		}
		?> 
		
		<?
		/*
		if( isset($_SESSION['login']) && $_SESSION['login'] != "" && $_SESSION['subsidiary_id'] != ""){
			$sql = "SELECT subsidiary_header_image FROM cpm_subsidiary WHERE subsidiary_id = ".$_SESSION['subsidiary_id'];
			$result = @mysql_query($sql);
			$row = @mysql_fetch_row($result);
			if($row[0] != "" ){ 
				?>
				<img src="/images/templates/header_image_w_logo.jpg" width="580" height="86" border="0" alt="RMG Living">
				<img src="/images/logos/<?=$row[0]?>" style="position:absolute; left:50%; margin-left:245px; margin-top:23px; z-index:10;" border="0" >
				<?
			}
			else{
				?>
				<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">
				<?
			}
		}
		else{
			?>
			<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">
			<?
		}
		*/
		?>
		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
  <td>
  <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
  <tr>
    <td height="10" align="center" valign="bottom" bgcolor="#E9EEF4" style="border-bottom:1px solid #c1d1e1;border-top:1px solid #c1d1e1;"><img src="images/templates/header_bar_grad2.jpg" alt=""></td>
  </tr>
  <tr>
  <td height="8" align="center" valign="top" style="border-top:1px solid #ffffff;"><img src="images/templates/header_bar_grad3.jpg" alt=""></td>
  </tr>
  </table>
  </td>
  </tr>
 
  <tr>
    <td height="4"></td>
  </tr>
  <tr>
    <td><table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="28" valign="top">
	
	<table width="760" cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td>
	<!-- InstanceBeginEditable name="breadcrumbs" --><a href="/building_details.php" class="crumbs">Your Community</a>&nbsp;>&nbsp;Useful Documents<!-- InstanceEndEditable -->
	</td>
	<td style="text-align:right;" nowrap>
	<? if(!empty($_SESSION['resident_session'])){?><a href="index.php?logoff=Y" class="crumbs">Log Off</a><? }?>
	</td>
	</tr>
	</table>
	
	</td>
  </tr>
</table>
</td>
  </tr>
  <tr>
    <td><!-- InstanceBeginEditable name="body" -->
      <table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td style="border-bottom:1px solid #003366;"><table width="760" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="15"><img src="images/dblue_box_top_left_corner.jpg" width="15" height="33"></td>
              <td width="730" bgcolor="#416CA0" style="border-top:1px solid #003366; vertical-align:middle"><img src="images/useful_documents/useful_documents_symbol.jpg" width="22" height="22" style="vertical-align:middle;">&nbsp;&nbsp;<span class="box_title">Useful Documents</span></td>
              <td width="15" align="right"><img src="images/dblue_box_top_right_corner.jpg" width="15" height="33"></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td><table width="760" border="0" cellspacing="0" cellpadding="15" style="border-left:1px solid #7ea0bd;border-right:1px solid #7ea0bd;border-top:1px solid #7ea0bd;border-bottom:1px solid #cccccc;">
            <tr>
              <td width="378" valign="top"><table width="348" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td colspan="2" valign="top"><?=$page_intro?></td>
                  </tr>
                <tr>
                  <td colspan="2"></td>
                </tr>
                <tr>
                  <td colspan="2"></td>
                </tr>
                <tr>
                  <td colspan="2">&nbsp;</td>
                  </tr>
                <tr>
                  <td width="245">&nbsp;</td>
                  <td width="103">&nbsp;</td>
                </tr>
				<? if($num_letters > 0){?>
                <tr>
                  <td colspan="2"><p><strong><span class="text036">Circulated Letters / Letters of Interest</span><br>
                  </strong></p>
                    </td>
                  </tr>
                <tr>
                  <td colspan="2">Below is a list of letters that have been issued to Lessees of <?=$rmc->rmc['rmc_name']?>.</td>
                </tr>
                <tr>
                  <td colspan="2" height="10"></td>
                </tr>
				
				<tr>
                <td colspan="2">
				<table border="0" cellpadding="0" cellspacing="0" width="348">
				<?
				// Display the list of letters for this RMC 
				while($row_letters = @mysql_fetch_array($result_letters)){
				?>
				<tr><td width="20" style="vertical-align:top; padding-top:3px;"><img src="images/pdf.gif" alt="Adobe Acrobat File" width="16" height="16" border="0" style="vertical-align:top;"></td>
				<td style="padding-top:3px;"><?=date("d/m/y", $row_letters['letter_uploaded_ts'])?> <a href="<?=$UTILS_HTTP_ADDRESS?>show_file.php?type=letter&id=<?=$row_letters['letter_file_stamp']?>&rmc_num=<?=$rmc->rmc['rmc_num']?>" target="_blank" class="link416CA0" style="text-decoration:underline "><strong><?=$row_letters['letter_type']?></strong></a> <?="(".get_file_size($UTILS_PRIVATE_FILE_PATH."letters/".$row_letters['letter_file']).")"?></td></tr>
				<? }?>
				</table>
				</td>
                </tr>
				
                <tr>
                  <td colspan="2" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="2" valign="top">&nbsp;</td>
                </tr>
				<? }?>
				
				<? if($num_car_park_plans > 0){?>
                <tr>
                  <td colspan="2" valign="top"><p><strong><span class="text036">Car Park Plan</span></strong></p></td>
                  </tr>
                <tr>
                  <td colspan="2" valign="top">This will show the allocated parking spaces for your development, either in diagramatic or tabular form.</td>
                </tr>
                <tr>
                  <td colspan="2" height="10"></td>
                </tr>
				<?
				// Display the list of car park plans for this RMC
				$car_park_plan_counter=1;
				while($row_car_park = @mysql_fetch_array($result_car_park)){
				?>
                <tr>
                  <td colspan="2" style="vertical-align:middle;"><img src="images/pdf.gif" alt="Adobe Acrobat File" width="16" height="16" border="0" style="vertical-align:middle;">&nbsp;<a href="<?=$UTILS_HTTP_ADDRESS?>show_file.php?type=carpark&id=<?=$row_car_park['car_park_file_stamp']?>&rmc_num=<?=$rmc->rmc['rmc_num']?>" target="_blank" class="link416CA0" style="text-decoration:underline "><strong>Download <?=$row_car_park['car_park_type']?></strong></a> <? if($num_car_park_plans > 1){print "(".$car_park_plan_counter." of ".$num_car_park_plans.")";}?> <?="(".get_file_size($UTILS_PRIVATE_FILE_PATH."car_park_plans/".$row_car_park['car_park_file']).")"?></td>
                </tr>
				<? 
				$car_park_plan_counter++;
				}?>
                <tr>
                  <td colspan="2" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="2" valign="top">&nbsp;</td>
                </tr>
				<? }?>
				
				<? if($num_slas > 0){
					$row_sla = @mysql_fetch_array($result_sla);
				?>
                <tr>
                  <td colspan="2" valign="top"><strong><span class="text036">Management Agreement</span></strong> </td>
                  </tr>
                <tr>
                  <td colspan="2" valign="top">This document outlines the level of service that <?=$rmc->rmc['rmc_name']?> has agreed to accept from RMG between <?=substr($row_sla['sla_from_date'],6,2)."/".substr($row_sla['sla_from_date'],4,2)."/".substr($row_sla['sla_from_date'],2,2)." to ".substr($row_sla['sla_to_date'],6,2)."/".substr($row_sla['sla_to_date'],4,2)."/".substr($row_sla['sla_to_date'],2,2)?>.</td>
                </tr>
                <tr>
                  <td colspan="2" height="10"></td>
                </tr>
                <tr>
                  <td colspan="2" style="vertical-align:middle;"><img src="images/pdf.gif" alt="Adobe Acrobat File" width="16" height="16" border="0" style="vertical-align:middle;">&nbsp;<a href="<?=$UTILS_HTTP_ADDRESS?>show_file.php?type=sla&id=<?=$row_sla['sla_file_stamp']?>&rmc_num=<?=$rmc->rmc['rmc_num']?>" target="_blank" class="link416CA0" style="text-decoration:underline "><strong>Download the agreement</strong></a> <?="(".get_file_size($UTILS_PRIVATE_FILE_PATH."sla/".$row_sla['sla_file']).")"?></td>
                </tr>
                <tr>
                  <td colspan="2" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="2" valign="top">&nbsp;</td>
                </tr>
				<? }?>
				
				<? if($num_ssm > 0){?>
                <tr>
                  <td colspan="2" valign="top"><p><strong><span class="text036">Site Maintenance Document</span></strong></p></td>
                  </tr>
                <tr>
                  <td colspan="2" valign="top"><p>The below documents outline an overview of the general maintenance that is to be carried out on your development</td>
                </tr>
                <tr>
                  <td colspan="2" height="10"></td>
                </tr>
				  <?
				  // Display the list of ssms for this RMC
				  while($row_ssm = @mysql_fetch_array($result_ssm)){
				 ?>
                <tr>
                  <td colspan="2" style="vertical-align:middle;"><img src="images/pdf.gif" alt="Adobe Acrobat File" width="16" height="16" border="0"  style="vertical-align:middle;">&nbsp;<a href="<?=$UTILS_HTTP_ADDRESS?>show_file.php?type=ssm&id=<?=$row_ssm['ssm_file_stamp']?>&rmc_num=<?=$rmc->rmc['rmc_num']?>" target="_blank" class="link416CA0" style="text-decoration:underline "><strong>Download <? print substr($row_ssm['ssm_from_date'],6,2)."/".substr($row_ssm['ssm_from_date'],4,2)."/".substr($row_ssm['ssm_from_date'],0,4)." to ".substr($row_ssm['ssm_to_date'],6,2)."/".substr($row_ssm['ssm_to_date'],4,2)."/".substr($row_ssm['ssm_to_date'],0,4);?></strong></a> <?="(".get_file_size($UTILS_PRIVATE_FILE_PATH."ssm/".$row_ssm['ssm_file']).")"?></td>
                </tr>
				  <? } ?>
                <tr>
                  <td colspan="2" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="2" valign="top">&nbsp;</td>
                </tr>
				<? }?>
			  <? if($num_client > 0){?>
				  <tr>
					  <td colspan="2" valign="top"><p><strong><span class="text036">Client Documents</span></strong></p></td>
				  </tr>
				  <tr>
					  <td colspan="2" valign="top"><p>The below list of documents are general documents for clients</td>
				  </tr>
				  <tr>
					  <td colspan="2" height="10"></td>
				  </tr>
				  <?
				  // Display the list of client documents for this RMC
				  while($row_client = @mysql_fetch_array($result_client)){
					  ?>
					  <tr>
						  <td colspan="2" style="vertical-align:middle;"><img src="images/pdf.gif" alt="Adobe Acrobat File" width="16" height="16" border="0"  style="vertical-align:middle;">&nbsp;<a href="<?=$UTILS_HTTP_ADDRESS?>show_file.php?type=client&id=<?=$row_client['client_file_stamp']?>&rmc_num=<?=$rmc->rmc['rmc_num']?>" target="_blank" class="link416CA0" style="text-decoration:underline "><strong>Download <?=$row_client['client_title'];?> (<?=substr($row_client['client_from_date'],6,2)."/".substr($row_client['client_from_date'],4,2)."/".substr($row_client['client_from_date'],0,4)." to ".substr($row_client['client_to_date'],6,2)."/".substr($row_client['client_to_date'],4,2)."/".substr($row_client['client_to_date'],0,4);?>)</strong></a> <?="(".get_file_size($UTILS_PRIVATE_FILE_PATH."client/".$row_client['client_file']).")"?></td>
					  </tr>
				  <? } ?>
				  <tr>
					  <td colspan="2" valign="top">&nbsp;</td>
				  </tr>
				  <tr>
					  <td colspan="2" valign="top">&nbsp;</td>
				  </tr>
			  <? }?>
				<!--
                <tr>
                  <td colspan="2" valign="top"><strong><span class="text036">Sample Lease(s) </span></strong></td>
                </tr>
                <tr>
                  <td colspan="2" valign="top">We are supplied with sample leases from the development companies who built your property. These are guidlines ONLY  to the types of clauses that may occur in the lease that is specific to your property. We must stress that these are not specific to any one property. Therefore, if you require the exact lease for your property, in order to deal with a dispute, for example, please visit the Land Registry website, where you can pay for your specific lease. </td>
                </tr>
                <tr>
                  <td colspan="2" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="2" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="2" valign="top">&nbsp;</td>
                </tr>
				-->
				
				<? if($num_newsletters > 0){?>
                <tr>
                  <td valign="top"><p><strong><span class="text036">Newsletters</span></strong></p></td>
                  <td align="right" valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="2" valign="top"><p>These newsletters are issued to residents of <?=$rmc->rmc['rmc_name']?> from RMG.</p>
                    </td>
                </tr>
                <tr>
                  <td colspan="2" height="10"></td>
                </tr>
                <tr>
                  <td colspan="2">
				  <table border="0" cellpadding="0" cellspacing="0" width="348">
					<?
					// Display the list of newsletters for this RMC 
					while($row_newsletters = @mysql_fetch_array($result_newsletters)){
					?>
					<tr><td width="20" style="vertical-align:top;"><img src="images/pdf.gif" alt="Adobe Acrobat File" width="16" height="16" border="0" style="vertical-align:top;"></td>
					<td><a href="<?=$UTILS_HTTP_ADDRESS?>show_file.php?type=newsletter&id=<?=$row_newsletters['newsletter_file_stamp']?>&rmc_num=<?=$rmc->rmc['rmc_num']?>" target="_blank" class="link416CA0" style="text-decoration:underline "><strong><?=$row_newsletters['newsletter_title']?></strong></a> <?="(".get_file_size($UTILS_PRIVATE_FILE_PATH."newsletters/".$row_newsletters['newsletter_file']).")"?></td></tr>
					<? }?>
					</table>
				  </td>
                </tr>
				<? }?>
				
              </table></td>
              <td width="378" valign="top" background="images/useful_documents/folders.jpg" bgcolor="#F5F7FB" style="border-left:1px solid #eaeaea;background-repeat:no-repeat;">
			    <p>&nbsp;</p>
			    <p>&nbsp;</p>
			    <table width="348" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="250">These documents can be viewed using Adobe Reader. <a href="<?=$UTILS_ADOBE_URL?>" target="_blank">Click to download</a>. </td>
					<td width="98" style="vertical-align:middle; text-align:right"><a href="<?=$UTILS_ADOBE_URL?>" target="_blank"><img src="images/getacrobat.gif" alt="Get Adobe Reader" width="88" height="31" border="0" style="vertical-align:middle;"></a></td>
                  </tr>
                </table>
			    <p>&nbsp;</p>
			    <p><img src="images/spacer.gif" alt="Information about your development" width="348" height="310"></p></td>
              </tr>
          </table></td>
        </tr>
		<tr><td height="5" bgcolor="#416CA0" style="border:1px solid #003366;"><img src="images/spacer.gif" alt=""></td>
		</tr>
      </table>
    <!-- InstanceEndEditable --></td>
  </tr>
  <tr>
    <td height="25"></td>
  </tr>
  <tr>
    <td><table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="445" height="14"><a href="about_us.php" class="link416CA0">About RMG Living</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="contact_us.php" class="link416CA0">Contact Us</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="privacy.php" class="link416CA0">Legal Information</a></td>
        <td width="315" align="right">Copyright <?=date("Y", time())?> &copy; <a href="http://www.rmgltd.co.uk">Residential Management Group Ltd</a></td>
      </tr>
	  <tr><td colspan="2">&nbsp;</td></tr>
	  <tr><td colspan="2" style="text-align:left;"><p>&nbsp;</p>
	    </td>
	  </tr>
    </table></td>
  </tr> 
  <tr><td>&nbsp;</td></tr>
</table>
</body>
<!-- InstanceEnd --></html>
