<?
$site_inactive = "N";
require("utils.php");
require($UTILS_FILE_PATH."management/gen_password.php");
require($UTILS_FILE_PATH."library/functions/clean_request_vars.php");
require($UTILS_FILE_PATH."library/classes/encryption.class.php");
$crypt = new encryption_class;
//if($_SERVER['HTTPS'] == "off" && $site_locality != "local"){header("Location: ".$UTILS_HTTPS_ADDRESS);}
// Check that address is https (secure) because of login
$login_error = "";
$remind_error = "";
$remind_success = "";

//================================================
// Log off routine
//================================================
if($_REQUEST['logoff'] == "Y"){
	
	unset($_SESSION['resident_session']);
	unset($_SESSION['rmc_num']);
	unset($_SESSION['rmc_ref']);
	unset($_SESSION['is_demo_account']);
	unset($_SESSION['is_developer']);
	unset($_SESSION['rmc_name']);
	unset($_SESSION['dev_description']);
	unset($_SESSION['member_id']);
	unset($_SESSION['resident_num']);
	unset($_SESSION['resident_ref']);
	unset($_SESSION['resident_name']);
	unset($_SESSION['login']);
	unset($_SESSION['subsidiary_id']);
}

//================================================
// Log in routine
//================================================
if($_POST['whichaction'] == "login" && ($site_inactive != "Y" || $_POST['username'] == "demo" || $_POST['username'] == "cpmlivingadmin")){

	if($_POST['username'] == "" || $_POST['password'] == ""){
	
		$login_error = "Please enter your login details";
	}
	else{
	
		//================================
		// Check for resident login
		//================================
		$sql_resident = "
		SELECT re.rmc_num, rex.allow_password_reset, rex.is_first_login, re.resident_name, rex.is_demo_resident_account, rex.login_expiry_YMD, lrmc.rmc_ref, lre.resident_ref, re.resident_num, rex.is_developer 
		FROM cpm_residents re, cpm_lookup_residents lre, cpm_residents_extra rex, cpm_rmcs rmc, cpm_lookup_rmcs lrmc 
		WHERE 
		lrmc.rmc_lookup=rmc.rmc_num AND 
		lre.resident_lookup=re.resident_num AND 
		rex.password = '".addslashes($crypt->encrypt($UTILS_DB_ENCODE, $_POST['password']))."' AND 
		rex.password <> '' AND 
		re.rmc_num=rmc.rmc_num AND 
		re.resident_num=rex.resident_num AND 
		(rmc.rmc_status = 'Active' OR rmc.rmc_is_active = '1') AND 
		(re.resident_status = 'Current' OR re.resident_is_active = '1') AND 
		((lre.resident_ref='".strtolower($_POST['username'])."' AND lre.resident_ref <> '') OR (re.voyager_resident_num='".strtolower($_POST['username'])."' AND re.voyager_resident_num <> ''))
		";
		//print $sql_resident;
		$result_resident = @mysql_query($sql_resident);
		$num_valid_users = @mysql_num_rows($result_resident);
		
		if($num_valid_users > 0){
		
			$row_resident = @mysql_fetch_row($result_resident);

			$_SESSION['resident_session'] = session_id();
			$_SESSION['rmc_num'] = $row_resident[0];
			$_SESSION['rmc_ref'] = $row_resident[6];
			$_SESSION['is_developer'] = $row_resident[9];
			
			// Check login expiry date (for developer logins)
			$thisYMD = date("Ymd", $thistime);
			if($row_resident[5] == "" || $thisYMD < $row_resident[5]){
				
				// Get certain RMC info
				$sql_rmc = "
				SELECT * 
				FROM cpm_rmcs r, cpm_rmcs_extra rex 
				WHERE r.rmc_num=rex.rmc_num AND r.rmc_num = ".$row_resident[0];
				$result_rmc = @mysql_query($sql_rmc);
				$row_rmc = @mysql_fetch_array($result_rmc);
				$_SESSION['is_demo_account'] = $row_rmc['is_demo_account'];
				$_SESSION['rmc_name'] = $row_rmc['rmc_name'];
				$_SESSION['dev_description'] = $row_rmc['dev_description'];
				$_SESSION['member_id'] = $row_rmc['member_id'];		
				$_SESSION['resident_num'] = $row_resident[8];
				$_SESSION['resident_ref'] = $row_resident[7];
				$_SESSION['resident_name'] = $row_resident[3];
				$_SESSION['subsidiary_id'] = $row_rmc['subsidiary_id'];	
				$_SESSION['login'] = session_id();
				
				// Update 'cpm_stat' table
				require($UTILS_FILE_PATH."library/stat.php");
				add_stat(3);
				
				//==============================
				// Determine where to send user
				//==============================
				
				// First check to see if there are any unread announcements
				// Get any unread announcements
				$sql_announce_unread = "SELECT * FROM cpm_announcements a, cpm_announcements_read ar WHERE a.announce_id=ar.announce_id AND a.rmc_num = ".$_SESSION['rmc_num']." AND a.announce_date > '$cut_off_day' AND ar.resident_num = ".$_SESSION['resident_num']." AND ar.announce_read = 'N' ORDER BY a.announce_date DESC";
				$result_announce_unread = @mysql_query($sql_announce_unread);
				$num_announce_unread = @mysql_num_rows($result_announce_unread);

				if($row_resident[2] == "Y" || $row_resident[1] == "Y"){
					if($site_locality == "remote"){
						header("Location: ".$UTILS_HTTPS_ADDRESS."change_details.php");
					}
					else{
						header("Location: change_details.php");
					}
				}
				elseif($num_announce_unread > 0){
					if($site_locality == "remote"){
						header("Location: ".$UTILS_HTTPS_ADDRESS."announcements.php?from=index");
					}
					else{
						header("Location: announcements.php?from=index");
					}
				}
				else{
					if($site_locality == "remote"){
						header("Location: ".$UTILS_HTTPS_ADDRESS."building_details.php");
					}
					else{
						header("Location: building_details.php");
					}
				}
				exit;
			}
			else{
				$login_error = "Your login details have expired";
			}
		}
		else{
			
			//===========================================
			// Check for content management system login
			//===========================================
			$sql_backend = "
			SELECT p.manager, p.first_name, p.last_name, b.f_name, b.l_name, b.user_id, b.is_sys_admin, b.is_first_logon, b.user_type_id, b.email,
			b.allow_pms, b.allow_rmc, b.allow_residents, b.allow_passwords, b.allow_letters, b.allow_news, b.allow_users, b.allow_report, b.allow_developers, b.password,
			allow_letters_module, allow_announcements
			FROM cpm_backend b LEFT JOIN cpm_pmt_members p ON b.member_id=p.member_id
			WHERE LOWER(b.user_name)='".$_POST['username']."' AND b.password <> '' AND b.password = '".addslashes($crypt->encrypt($UTILS_DB_ENCODE, $_POST['password']))."'";
			$result_backend = @mysql_query($sql_backend);
			$num_backed_users = @mysql_num_rows($result_backend);
			
			if($num_backed_users > 0){
			
				$row_backend = @mysql_fetch_row($result_backend);
			
				$_SESSION['manager'] = $row_backend[0];
				$_SESSION['manager_name'] = $row_backend[1]." ".$row_backend[2];
				$_SESSION['f_name'] = $row_backend[3];
				$_SESSION['l_name'] = $row_backend[4];
				$_SESSION['user_id'] = $row_backend[5];
				$_SESSION['is_sys_admin'] = $row_backend[6];
				$_SESSION['user_name'] = $row_backend[3]." ".$row_backend[4];
				$_SESSION['cms_login'] = session_id();
				$_SESSION['is_first_logon'] = $row_backend[7];
				$_SESSION['user_type_id'] = $row_backend[8];
				$_SESSION['user_email'] = $row_backend[9];
				
				$_SESSION['allow_pms'] = $row_backend[10];
				$_SESSION['allow_rmc'] = $row_backend[11];
				$_SESSION['allow_residents'] = $row_backend[12];
				$_SESSION['allow_passwords'] = $row_backend[13];
				$_SESSION['allow_letters'] = $row_backend[14];
				$_SESSION['allow_news'] = $row_backend[15];
				$_SESSION['allow_users'] = $row_backend[16];
				$_SESSION['allow_report'] = $row_backend[17];
				$_SESSION['allow_developers'] = $row_backend[18];
				$_SESSION['allow_letters_module'] = $row_backend[20];
				$_SESSION['allow_announcements'] = $row_backend[21];
				
				if($site_locality == "remote"){
					if($_SESSION['is_first_logon'] == "Y"){
						header("Location: ".$UTILS_HTTPS_ADDRESS."management/change_password.php");
					}
					else{
						header("Location: ".$UTILS_HTTPS_ADDRESS."management/index.php");
					}
				}
				else{
					if($_SESSION['is_first_logon'] == "Y"){
						header("Location: management/change_password.php");
					}
					else{
						header("Location: management/index.php");
					}
				}
				
				exit;
			}
			else{
				$login_error = "Incorrect login details";
			}
		}
	}
}

elseif($_POST['whichaction'] == "remind"){

	$sql_remind = "
	SELECT re.resident_num 
	FROM cpm_residents re, cpm_residents_extra rex, cpm_lookup_residents lre 
	WHERE 
	lre.resident_lookup=re.resident_num AND 
	re.resident_num=rex.resident_num AND 
	rex.email <> '' AND 
	lre.resident_ref = '".trim($_POST['forgot_login_id'])."' AND 
	rex.email = '".trim($_POST['forgot_email'])."'";
	$result_remind = @mysql_query($sql_remind);
	$resident_found = @mysql_num_rows($result_remind);

	if($resident_found > 0){
	
		$row_remind = @mysql_fetch_row($result_remind);
	
		// Gen password
		$password_unique = "N";
		while($password_unique == "N"){
			$password = get_rand_id(8, 'ALPHA', 'UPPER');
			$sql = "SELECT password FROM cpm_residents_extra WHERE password = '".addslashes($crypt->encrypt($UTILS_DB_ENCODE, $password))."'";
			$result = mysql_query($sql);
			$password_exists = mysql_num_rows($result);
			if($password_exists < 1){
				$password_unique = "Y";
			}
		}
		$sql2 = "
		UPDATE cpm_residents_extra SET
		is_first_login = 'Y',
		password = '".addslashes($crypt->encrypt($UTILS_DB_ENCODE, $password))."'
		WHERE resident_num = ".$row_remind[0];
		@mysql_query($sql2);
		
		
		// Get unit address
		$sql_ua = "SELECT unit_address_1 FROM cpm_units WHERE resident_num = ".$row_remind[0];
		$result_ua = @mysql_query($sql_ua);
		$row_ua = @mysql_fetch_row($result_ua);
		
		// Set email headers
		$headers = "From: RMG Living Support <customerservice@rmguk.com>\n";
		$headers .= "Reply-To: RMG Living Support <customerservice@rmguk.com>";
		
		// Send email password
$body_pw = "
Below is your password for the RMG Living website - www.rmgliving.co.uk
This will give you access for your property '".$row_ua[0]."':

".$password."

Once logged in, you can change you password to something more memorable in the 'Your Details' section. If you experience problems accessing the RMG Living website, please contact our Customer Services Team via customerservice@rmguk.com

Kind Regards,
RMG Living Support
	
(This email was been generated automatically.)
";
mail($_POST['forgot_email'],"RMG Living",$body_pw,$headers) || $remind_success = "N";
		
		
		if($remind_success == "N"){
			$remind_error = "There was a problem sending your details.";
		}
		else{
			$remind_success = "Y";
		}
	
	}
	else{
		$remind_success = "";
		$remind_error = "Details not recognised.";
	}
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><!-- InstanceBegin template="/Templates/main.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- InstanceBeginEditable name="doctitle" -->
<title>RMG Living</title>
<meta name="description" content="RMG Living is a portal for lessees to access information about their management company">
<meta name="keywords" content="rmg, residential management group, cpm asset management, management company">
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<script type="text/javascript">
function what_is_lessee_id(){
	window.open("what_is_a_lessee_id.htm","whatlesseewin","height=150, width=320, status=no, menubar=no");
}
</script>

<!-- InstanceEndEditable -->
<link href="styles.css" rel="stylesheet" type="text/css">
<!--[if lte IE 8]> 
<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if lte IE 7]> 
<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
<![endif]-->
</head>
<body>
<? if($_SESSION['is_demo_account'] == "Y" && $_SESSION['is_developer'] != "Y"){?>
<form method="post" action="/building_details.php" style="margin:0; padding:0;">
<input type="hidden" name="a" value="cl">
<input type="hidden" name="s" value="gkfcbk45cgtf5kngn7kns5cg7snk5g7nsv5ng">
<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" style="margin:0; padding:0;">
	<tr>
		<td style="padding:4px;">Change Account:&nbsp;<input type="text" name="change_lessee" value="<?=$_SESSION['resident_ref']?>" size="20"> <input type="submit" name="submit_button" value="Change"></td>
	</tr>
</table>
</form>
<? }?>

<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#E9EEF4" style="border-bottom:1px solid #c1d1e1;"><div align="center"><img src="images/templates/header_bar_grad2.jpg" alt=""></div></td>
  </tr>
  <tr>
    <td style="border-top:1px solid #ffffff;border-bottom:1px solid #ffffff;">
	<table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="180"><a href="<? print $UTILS_HTTPS_ADDRESS;if( isset($_SESSION['login']) && $_SESSION['login'] != ""){print "/building_details.php";}else{print "/index.php";}?>"><img src="/images/templates/rmg_living_logo.jpg" style="margin-top:10px;" width="180" alt="RMG Living" border="0"></a></td>
        <td width="580" align="right">
		<?
		// User logged in is a 'developer', show any developer specific logos, or default to normal ones...
		if($_SESSION['is_developer'] == "Y"){
			
			$sql = "SELECT developer_id FROM cpm_residents_extra WHERE resident_num = ".$_SESSION['resident_num'];
			$result = @mysql_query($sql);
			$row = @mysql_fetch_row($result);
			if($row[0] != "" && file_exists($UTILS_FILE_PATH.'/images/developers/'.$row[0].'/header_'.$row[0].'.jpg') ){
				
				print '<img src="/images/developers/'.$row[0].'/header_'.$row[0].'.jpg" alt="RMG Living" width="580" height="86" BORDER=0>';
			}
			else{
				
				print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
			}
		}
		else{
		
			// Check to see if the Man. Co. has a developer association, if so, set developer specific images, or default to normal images...
			if( isset($_SESSION['login']) && $_SESSION['login'] != "" && $_SESSION['rmc_num'] != ""){
			
				$sql = "SELECT developer_id FROM cpm_rmcs_extra WHERE rmc_num = ".$_SESSION['rmc_num'];
				$result = @mysql_query($sql);
				$row = @mysql_fetch_row($result);
				if( $row[0] != "" && file_exists($UTILS_FILE_PATH.'/images/developers/'.$row[0].'/header_'.$row[0].'.jpg') ){
					
					print '<img src="/images/developers/'.$row[0].'/header_'.$row[0].'.jpg" alt="RMG Living" width="580" height="86" BORDER=0>';
				}
				else{
					
					print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
				}
			}
			else{
				
				print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
			}
		}
		?> 
		
		<?
		/*
		if( isset($_SESSION['login']) && $_SESSION['login'] != "" && $_SESSION['subsidiary_id'] != ""){
			$sql = "SELECT subsidiary_header_image FROM cpm_subsidiary WHERE subsidiary_id = ".$_SESSION['subsidiary_id'];
			$result = @mysql_query($sql);
			$row = @mysql_fetch_row($result);
			if($row[0] != "" ){ 
				?>
				<img src="/images/templates/header_image_w_logo.jpg" width="580" height="86" border="0" alt="RMG Living">
				<img src="/images/logos/<?=$row[0]?>" style="position:absolute; left:50%; margin-left:245px; margin-top:23px; z-index:10;" border="0" >
				<?
			}
			else{
				?>
				<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">
				<?
			}
		}
		else{
			?>
			<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">
			<?
		}
		*/
		?>
		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
  <td>
  <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
  <tr>
    <td height="10" align="center" valign="bottom" bgcolor="#E9EEF4" style="border-bottom:1px solid #c1d1e1;border-top:1px solid #c1d1e1;"><img src="images/templates/header_bar_grad2.jpg" alt=""></td>
  </tr>
  <tr>
  <td height="8" align="center" valign="top" style="border-top:1px solid #ffffff;"><img src="images/templates/header_bar_grad3.jpg" alt=""></td>
  </tr>
  </table>
  </td>
  </tr>
 
  <tr>
    <td height="4"></td>
  </tr>
  <tr>
    <td><table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="28" valign="top">
	
	<table width="760" cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td>
	<!-- InstanceBeginEditable name="breadcrumbs" -->&nbsp;<!-- InstanceEndEditable -->
	</td>
	<td style="text-align:right;" nowrap>
	<? if(!empty($_SESSION['resident_session'])){?><a href="index.php?logoff=Y" class="crumbs">Log Off</a><? }?>
	</td>
	</tr>
	</table>
	
	</td>
  </tr>
</table>
</td>
  </tr>
  <tr>
    <td><!-- InstanceBeginEditable name="body" -->
      <table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td width="240" valign="top"><table width="240" border="0" cellspacing="0" cellpadding="0">
            <tr valign="top">
              <td colspan="3" align="left" width="240"><img src="images/index/welcome_box_title2.jpg" alt="Welcome to RMG Living" width="240" height="30"></td>
              </tr>
            <tr>
              <td bgcolor="#7EA0BD"></td>
              <td style="padding:15px; height:174px;">RMG Living provides Property Owners with access to their statement of account, information about their development, helpful resources for property management and the ability to make secure, online payments.</td>
              <td bgcolor="#7EA0BD"></td>
            </tr>
            <tr>
              <td width="1" bgcolor="#7EA0BD"><img src="images/spacer.gif" alt=""></td>
              <td width="238" valign="bottom" style="border-bottom:1px solid #7EA0BD;" align="left"><img src="images/index/welcome_main_image.jpg" alt="Welcome to RMG Living" width="238" height="217"></td>
              <td width="1" bgcolor="#7EA0BD"><img src="images/spacer.gif" alt=""></td>
            </tr>
          </table></td>
          <td width="20"><img src="images/spacer.gif" width="20" height="1" alt=""></td>
          <td width="240" valign="top"><table width="240" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td valign="bottom" style="vertical-align:bottom;">
			  
			  <table width="240" border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px solid #003366;">
                <tr>
                  <td width="15" valign="bottom"><img src="images/dblue_box_top_left_corner.jpg" width="15" height="33" alt=""></td>
                  <td align="left" valign="middle" bgcolor="#426C9E" style="border-top:1px solid #003366; vertical-align:middle"><img src="images/index/login_symbol.jpg" width="22" height="32" style="vertical-align:middle;" alt="Login">&nbsp;&nbsp;<span class="box_title">Property Owner Login</span></td>
                  <td width="15" align="right" valign="bottom"><img src="images/dblue_box_top_right_corner.jpg" width="15" height="33" alt=""></td>
                </tr>
              </table>
			  
			  
			  </td>
            </tr>
            <tr>
              <td>
			  
			  <table width="240" border="0" cellspacing="0" cellpadding="0" style="border-left:1px solid #7ea0bd;border-right:1px solid #7ea0bd;border-top:1px solid #7ea0bd;border-bottom:1px solid #cccccc; background-color:#f1f4f8;">
                <tr>
                	<td valign="top" style="padding:15px; height:186px; vertical-align:top;">
                		
                		<? if($site_inactive == "Y" && $_POST['whichaction'] == "login"){?>
                		<p>The RMG Living website is currently unavailable.</p>
                		<? }else{?>
                		<form action="<? if($site_locality == "remote"){print $UTILS_HTTPS_ADDRESS;}?>index.php" method="post" name="login_form">
                			<table width="208" border="0" align="left" cellpadding="0" cellspacing="0">
                				<tr>
                					<td colspan="2" style="padding:10px 0px 10px 0px;">Enter your login details into the fields below and click 'Login'.</td>
                					</tr>
                				<?
					// Display login error
					if($login_error != ""){
					?>
                				<tr>
                					<td colspan="2" style="padding:10px 0px 10px 0px;"><span class="msg_fail"><b><?=$login_error?></b></span></td>
                					</tr>
                				<? }?>
                				<tr>
                					<td width="70" nowrap style="padding-bottom:3px;"><span class="text036">Lessee Id:</span>&nbsp;</td>
                					<td width="138" nowrap style="padding-bottom:3px;"><input name="username" type="text" id="username" style="width:93px;">
                						<a href="Javascript:;" onClick="what_is_lessee_id();return false;"><img src="images/index/what_is_lessee_id_button.jpg" align="absmiddle" style="margin-bottom:3px;" border="0"></a></td>
                					</tr>
                				<tr>
                					<td nowrap><span class="text036">Password:</span>&nbsp;</td>
                					<td><input name="password" type="password" id="password" style="width:120px;"></td>
                					</tr>
                				<tr>
                					<td colspan="2" style="padding-top:10px;"><input name="login_button" type="image" onClick="document.forms['login_form'].submit();" src="images/index/login_button.jpg" alt="Click to login to RMG Living"></td>
                					</tr>
                				</table>
                			<input type="hidden" name="whichaction" value="login">
                			</form>
                		
                		<? }?>
                		
                		</td>
                	</tr>
                	</table>
			  
			  </td>
            </tr>
            <tr>
              <td style="border-left:1px solid #7ea0bd;border-right:1px solid #7ea0bd;border-bottom:1px solid #7ea0bd;" bgcolor="#f1f4f8"><table width="238" border="0" cellspacing="0" cellpadding="0">
                <tr>
                	<td valign="top" style="padding:15px;">
                		
                		<form method="post" name="reminder_form" action="<?=$UTILS_HTTPS_ADDRESS?>index.php">
                			<table width="208" border="0" cellpadding="0" cellspacing="0">
                				<tr valign="top">
                					<td colspan="2"><b><span class="subt036">Forgotten your login details ?</span></b></td>
                					</tr>
                				<?
						// Display reminder error
						if($remind_success == "N"){
						?>
                				<tr>
                					<td colspan="2" style="padding:10px 0px 10px 0px;"><span class="msg_fail"><b><?=$remind_error?></b></span></td>
                					</tr>
                				<? }elseif($remind_success == ""){?>
                				<tr>
                					<td colspan="2" style="padding:10px 0px 10px 0px;" >Enter your registered email address, then click submit to be reminded of your details.</td>
                					</tr>
                					<?
					  				if($remind_error != ""){
										?>
										<tr>
											<td colspan="2" style="padding:10px 0px 10px 0px;"><span class="msg_fail"><b><?=$remind_error?></b></span></td>
											</tr>
                				<? }?>
                				<tr>
                					<td width="70" nowrap style="padding-bottom:3px;"><span class="text036">Lessee Id:</span>&nbsp;</td>
                					<td nowrap style="padding-bottom:3px;"><input name="forgot_login_id" type="text" id="forgot_login_id" style="width:93px;">
                						<a href="Javascript:;" onClick="what_is_lessee_id();return false;"><img src="images/index/what_is_lessee_id_button.jpg" align="absmiddle" style="margin-bottom:3px;" border="0"></a></td>
                					</tr>
                				<tr>
                					<td width="70" nowrap><span class="text036">Email:</span>&nbsp;</td>
                					<td width="68"><input name="forgot_email" type="text" id="forgot_email" style="width:120px;"></td>
                					</tr>
                				<tr>
                					<td colspan="2" style="padding-top:10px;"><input name="submit_button" type="image" src="images/index/submit_button.jpg" alt="Click to be reminded of your Password"></td>
                					</tr>
                				<? }elseif($remind_success == "Y"){?>
                				<tr>
                					<td colspan="2" style="padding:10px 0px 10px 0px;"><span class="msg_success">Your password has been sent to the email address you have provided.</span>
									<br /><br />If you do not receive this reminder email, click <a href="index.php" class="link036">here</a> to try again.</td>
                					</tr>
                				<? }?>
                				</table>
                			<input type="hidden" name="whichaction" value="remind">
                			</form>
                		
                		</td>
                	</tr>
                <tr>
                  <td height="10"><img src="images/spacer.gif" width="208" height="10" alt=""></td>
                </tr>
              </table></td>
            </tr>
			<tr><td colspan="3" bgcolor="#416CA0" height="5" style="border:1px solid #003366;"><img src="images/spacer.gif" alt=""></td></tr>
          </table></td>
          <td width="20"><img src="images/spacer.gif" width="20" height="1" alt=""></td>
          <td width="240" valign="top">
		  
		  
		  <table width="240" border="0" cellspacing="0" cellpadding="0">
            <tr>
				<td style="padding-bottom:15px;"><a href="http://twitter.com/rmgltd" target="_blank" title="Follow RMG on twitter..."><img src="images/twitter_home.jpg" border="0" /></a></td>
			</tr>
			<tr>
              <td valign="bottom" style="vertical-align:bottom;">
			  
			  <table width="240" border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px solid #003366;">
                  <tr>
                    <td width="15" valign="bottom"><img src="images/lblue_box_top_left_corner.jpg" width="15" height="33" alt=""></td>
                    <td align="left" valign="middle" style="background-color:#669ACC; border-top:1px solid #5F91C4; vertical-align:middle"><img src="images/index/registration_symbol.jpg" width="22" height="22" style="vertical-align:middle;" alt="Request a login">&nbsp;&nbsp;<span class="box_title">Login Request </span></td>
                    <td width="15" align="right" valign="bottom"><img src="images/lblue_box_top_right_corner.jpg" width="15" height="33" alt=""></td>
                  </tr>
              </table>
			  
			  </td>
            </tr>
            <tr>
            	<td style="padding-bottom:15px;">
            		
            		<table width="240" border="0" cellspacing="0" cellpadding="0" style="border-left:1px solid #7ea0bd;border-right:1px solid #7ea0bd;border-top:1px solid #7ea0bd;border-bottom:1px solid #cccccc; background-color:#f1f4f8"">
            			<tr>
            				<td valign="top" style="padding:15px;">If RMG is the managing agent for your property and you have not received login details for this site, please click the button below.</td>
            				</tr>
            			<tr>
            				<td valign="bottom" style="padding:0px 15px 15px 15px;"><input name="register_button" type="image" id="register_button" onClick="location.href='<?=$UTILS_HTTPS_ADDRESS?>request.php';" src="images/index/register_button.jpg" alt="Click to request login details to RMG Living"></td>
            				</tr>
            			<tr>
            				<td bgcolor="#5F91C4" height="5" colspan="3"><img src="images/spacer.gif" alt=""></td>
            				</tr>
            			</table>
            		
            		
            		</td>
            	</tr>
            <tr>
            	<td valign="bottom" style="vertical-align:bottom;">
				
				<table width="240" border="0" cellspacing="0" cellpadding="0" style="border-bottom:1px solid #003366;">
            		<tr>
            			<td width="15" valign="bottom"><img src="images/lblue_box_top_left_corner.jpg" width="15" height="33" alt=""></td>
            			<td align="left" valign="middle" bgcolor="#669ACC" style="border-top:1px solid #5F91C4; vertical-align:middle"><img src="images/index/help_symbol.jpg" width="22" height="22" style="vertical-align:middle;" alt="Support">&nbsp;&nbsp;<span class="box_title">Help</span></td>
            			<td width="15" align="right" valign="bottom"><img src="images/lblue_box_top_right_corner.jpg" width="15" height="33" alt=""></td>
            			</tr>
            		</table></td>
            	</tr>
            <tr>
              <td>
			  
			  	<table width="240" border="0" cellspacing="0" cellpadding="0" style="border-left:1px solid #7ea0bd;border-right:1px solid #7ea0bd;border-bottom:1px solid #7ea0bd; background-color:#f1f4f8;">
                  <tr>
                  	<td valign="top" style="padding:15px;">If you are experiencing problems with the website, please click the link below to email our technical support team.</td>
                  	</tr>
                  <tr>
                  	<td valign="top" style="padding:0px 15px 15px 15px;"><a href="contact_us.php" class="link036"><strong>Technical support</strong></a></td>
                  	</tr>
                  <tr>
                  	<td bgcolor="#5F91C4" height="5"><img src="images/spacer.gif" alt=""></td>
                  	</tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
      </table>
    <!-- InstanceEndEditable --></td>
  </tr>
  <tr>
    <td height="25"></td>
  </tr>
  <tr>
    <td><table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="445" height="14"><a href="about_us.php" class="link416CA0">About RMG Living</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="contact_us.php" class="link416CA0">Contact Us</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="privacy.php" class="link416CA0">Legal Information</a></td>
        <td width="315" align="right">Copyright <?=date("Y", time())?> &copy; <a href="http://www.rmgltd.co.uk">Residential Management Group Ltd</a></td>
      </tr>
	  <tr><td colspan="2">&nbsp;</td></tr>
	  <tr><td colspan="2" style="text-align:left;"><p>&nbsp;</p>
	    </td>
	  </tr>
    </table></td>
  </tr> 
  <tr><td>&nbsp;</td></tr>
</table>
<!-- Embedded WhosOn: Insert the script below at the point on your page where you want the Click To Chat link to appear -->
<script type='text/javascript' src='//rmg.whoson.com/include.js?domain=www.rmgliving.co.uk'></script>
<script type='text/javascript' >
	if (typeof sWOTrackPage == 'function') sWOTrackPage();
	var c=1;
	var pn=sWOPage;
	var t = setInterval("if(c<40){sWOImageLoaded=function() {}; sWOInvite='';sWOResponse='N';sWOPage=pn+'%3Ftimer%3D'+c++;sWOTrackPage();}", 5000);</script>
</script>
</body>
<!-- InstanceEnd --></html>
