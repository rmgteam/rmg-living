<?
require("utils.php");
$request = $_REQUEST;
GLOBAL $UTILS_SCO_EMAIL;
GLOBAL $UTILS_CCC_EMAIL;
GLOBAL $UTILS_FAS_EMAIL;

require($UTILS_FILE_PATH."library/functions/clean_request_vars.php");
include($UTILS_FILE_PATH."library/functions/get_office_tel.php");
include($UTILS_FILE_PATH."library/functions/get_office_email.php");
require($UTILS_CLASS_PATH."website.class.php");
require($UTILS_CLASS_PATH."rmc.class.php");
require($UTILS_CLASS_PATH."resident.class.php");
$website = new website;
$rmc = new rmc;
$resident = new resident($_SESSION['resident_num']);

// Determine if allowed access into 'your community' section
$website->allow_community_access();

// Set RMC
$rmc->set_rmc($_SESSION['rmc_num']);

// Send message
if($_REQUEST['whichaction'] == "send"){
	
	if($_REQUEST['message'] != "" && $_REQUEST['email'] != "" && $_REQUEST['tel'] != ""){
	
		$send_error = "N";
		
		//$_REQUEST['message'] = "For ".$rmc->rmc['property_manager']." \r\n ".$rmc->rmc['rmc_name'].": \r\n\r\n ".$_REQUEST['message']." \r\n\r\n Resident: ".$resident->resident_name." \r\n Tenant Ref: ".$resident->resident_ref." \r\n Tel: ".$_REQUEST['tel']." \r\n Email: ".$_REQUEST['email'];
		$message_body = $request['message'];
		
		$message = "For ".$rmc->rmc['property_manager']." \r\n";
		$message .= $rmc->rmc['rmc_name'].": \r\n\r\n".$message_body." \r\n\r\n";
		$message .= "Resident: ".$resident->resident_name." \r\n";
		$message .= "Tenant Ref: ".$resident->resident_ref." \r\n";
		$message .= "Tel: ".$_REQUEST['tel']." \r\n";
		$message .= "Email: ".$_REQUEST['email']." \r\n";

		$email = $UTILS_CCC_EMAIL;

		if($rmc->subsidiary_code == 'sco'){
			$email = $UTILS_SCO_EMAIL;
		}elseif($rmc->region == 'FAS') {
			$email = $UTILS_FAS_EMAIL;
		}
		
		// Send email to office
		//if(!@mail("customerservice@rmguk.com", "RMG Living Feedback", addslashes($_REQUEST['message']), "From:".$_REQUEST['email'])){$send_error = "Y";}
		if(!@mail($email, "RMG LIVING Feedback", $message, "From:".$_REQUEST['email'])){
			$send_error = "Y";}
		else{
			
		// Add enquiry to log
		$sql = "
		INSERT INTO cpm_enquiries (
			rmc_num,
			enquiry_pm_name,
			resident_num,
			enquiry_email_from,
			enquiry_msg,
			enquiry_added_ts
		)
		VALUES(
			".$rmc->rmc['rmc_num'].",
			'".$rmc->rmc['property_manager']."',
			".$_SESSION['resident_num'].",
			'".$_REQUEST['email']."',
			'".$message."',
			'".$thistime."'
		)
		";
		@mysql_query($sql);
		
		$_REQUEST['message'] = "";
		}
		
	}
	
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><!-- InstanceBegin template="/Templates/main.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- InstanceBeginEditable name="doctitle" -->
<title>RMG Living - Raise an Issue</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->




<!-- InstanceEndEditable -->
<link href="styles.css" rel="stylesheet" type="text/css">
<!--[if lte IE 8]> 
<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if lte IE 7]> 
<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
<![endif]-->
	<script type='text/javascript' src="<?=$UTILS_HTTPS_ADDRESS?>library/jscript/jquery-1.6.2.min.js"></script>

</head>
<body>
<!-- chat now  -->
<? require_once($UTILS_FILE_PATH."includes/chat.php");?>
<? if($_SESSION['is_demo_account'] == "Y" && $_SESSION['is_developer'] != "Y"){?>
<form method="post" action="/building_details.php" style="margin:0; padding:0;">
<input type="hidden" name="a" value="cl">
<input type="hidden" name="s" value="gkfcbk45cgtf5kngn7kns5cg7snk5g7nsv5ng">
<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" style="margin:0; padding:0;">
	<tr>
		<td style="padding:4px;">Change Account:&nbsp;<input type="text" name="change_lessee" value="<?=$_SESSION['resident_ref']?>" size="20"> <input type="submit" name="submit_button" value="Change"></td>
	</tr>
</table>
</form>
<? }?>
<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#E9EEF4" style="border-bottom:1px solid #c1d1e1;"><div align="center"><img src="images/templates/header_bar_grad2.jpg" alt=""></div></td>
  </tr>
  <tr>
    <td style="border-top:1px solid #ffffff;border-bottom:1px solid #ffffff;">
	<table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="180"><a href="<? print $UTILS_HTTPS_ADDRESS;if( isset($_SESSION['login']) && $_SESSION['login'] != ""){print "/building_details.php";}else{print "/index.php";}?>"><img src="/images/templates/rmg_living_logo.jpg" style="margin-top:10px;" width="180" alt="RMG Living" border="0"></a></td>
        <td width="580" align="right">
		<?
		// User logged in is a 'developer', show any developer specific logos, or default to normal ones...
		if($_SESSION['is_developer'] == "Y"){
			
			$sql = "SELECT developer_id FROM cpm_residents_extra WHERE resident_num = ".$_SESSION['resident_num'];
			$result = @mysql_query($sql);
			$row = @mysql_fetch_row($result);
			if($row[0] != "" && file_exists($UTILS_FILE_PATH.'/images/developers/'.$row[0].'/header_'.$row[0].'.jpg') ){
				
				print '<img src="/images/developers/'.$row[0].'/header_'.$row[0].'.jpg" alt="RMG Living" width="580" height="86" BORDER=0>';
			}
			else{
				
				print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
			}
		}
		else{
		
			// Check to see if the Man. Co. has a developer association, if so, set developer specific images, or default to normal images...
			if( isset($_SESSION['login']) && $_SESSION['login'] != "" && $_SESSION['rmc_num'] != ""){
			
				$sql = "SELECT developer_id FROM cpm_rmcs_extra WHERE rmc_num = ".$_SESSION['rmc_num'];
				$result = @mysql_query($sql);
				$row = @mysql_fetch_row($result);
				if( $row[0] != "" && file_exists($UTILS_FILE_PATH.'/images/developers/'.$row[0].'/header_'.$row[0].'.jpg') ){
					
					print '<img src="/images/developers/'.$row[0].'/header_'.$row[0].'.jpg" alt="RMG Living" width="580" height="86" BORDER=0>';
				}
				else{
					
					print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
				}
			}
			else{
				
				print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
			}
		}
		?> 
		
		<?
		/*
		if( isset($_SESSION['login']) && $_SESSION['login'] != "" && $_SESSION['subsidiary_id'] != ""){
			$sql = "SELECT subsidiary_header_image FROM cpm_subsidiary WHERE subsidiary_id = ".$_SESSION['subsidiary_id'];
			$result = @mysql_query($sql);
			$row = @mysql_fetch_row($result);
			if($row[0] != "" ){ 
				?>
				<img src="/images/templates/header_image_w_logo.jpg" width="580" height="86" border="0" alt="RMG Living">
				<img src="/images/logos/<?=$row[0]?>" style="position:absolute; left:50%; margin-left:245px; margin-top:23px; z-index:10;" border="0" >
				<?
			}
			else{
				?>
				<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">
				<?
			}
		}
		else{
			?>
			<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">
			<?
		}
		*/
		?>
		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
  <td>
  <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
  <tr>
    <td height="10" align="center" valign="bottom" bgcolor="#E9EEF4" style="border-bottom:1px solid #c1d1e1;border-top:1px solid #c1d1e1;"><img src="images/templates/header_bar_grad2.jpg" alt=""></td>
  </tr>
  <tr>
  <td height="8" align="center" valign="top" style="border-top:1px solid #ffffff;"><img src="images/templates/header_bar_grad3.jpg" alt=""></td>
  </tr>
  </table>
  </td>
  </tr>
 
  <tr>
    <td height="4"></td>
  </tr>
  <tr>
    <td><table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="28" valign="top">
	
	<table width="760" cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td>
	<!-- InstanceBeginEditable name="breadcrumbs" --><a href="/building_details.php" class="crumbs">Your Community</a>&nbsp;>&nbsp;Raise an Issue<!-- InstanceEndEditable -->
	</td>
	<td style="text-align:right;" nowrap>
	<? if(!empty($_SESSION['resident_session'])){?><a href="index.php?logoff=Y" class="crumbs">Log Off</a><? }?>
	</td>
	</tr>
	</table>
	
	</td>
  </tr>
</table>
</td>
  </tr>
  <tr>
    <td><!-- InstanceBeginEditable name="body" -->
	<form action="raise_an_issue.php" method="post">
      <table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td style="border-bottom:1px solid #003366;"><table width="760" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="15"><img src="images/dblue_box_top_left_corner.jpg" width="15" height="33"></td>
              <td width="730" bgcolor="#416CA0" style="border-top:1px solid #003366; vertical-align:middle"><img src="images/raise_an_issue/raise_symbol.jpg" width="22" height="22" style="vertical-align:middle;">&nbsp;&nbsp;<span class="box_title">Raise an Issue</span></td>
              <td width="15" align="right"><img src="images/dblue_box_top_right_corner.jpg" width="15" height="33"></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td><table width="758" border="0" cellspacing="0" cellpadding="15" style="border-left:1px solid #7ea0bd;border-right:1px solid #7ea0bd;border-top:1px solid #7ea0bd;border-bottom:1px solid #cccccc;">
            <tr>
              <td width="380" valign="top">
			  <? if($_REQUEST['whichaction'] == "send"){?>
			  <table width="348" border="0" cellspacing="0" cellpadding="0">
			  <? if($send_error == "N"){?>
			  <tr><td width="25" style="vertical-align:middle;"><img src="images/tick_20.gif" border="0" style="vertical-align:middle;"></td>
			  <td class="msg_success" style="vertical-align:middle;">Thank you - your message has been sent to our Customer Services Department.</td>
			  </tr>
			  <? }?>
			  <? if($send_error == "Y"){?>
			  <tr><td width="25" style="vertical-align:middle;"><img src="images/del_16.gif" border="0" style="vertical-align:middle;"></td><td class="msg_fail" style="vertical-align:middle;">There was a problem sending your message. If this problem persists, please contact our Customer Services Team on <a href="mailto:customerservice@rmguk.com;">customerservice@rmguk.com</a></td></tr>
			  <? }?>
			  <tr><td>&nbsp;</td></tr>
			  </table>
			  <? }?>
			  <table width="348" border="0" cellspacing="0" cellpadding="0">
                
<tr>
                  
<td width="260" height="1"></td>
<td width="88"></td>
</tr>
<tr>
<td colspan="2"><p>If you need to bring something to our attention about your property or the development you live on, have a complaint about the service we are providing, or need to inform us of a change of correspondence address, please use the form below.</p>
  <p>For car parking disputes, nuisance neighbours and leaking pipes, please visit our <a href="advice.php">Advice Centre</a> first to determine if we are the right people to contact initially.</p>
  <p>Under the terms of our service, please allow up to 5 working days for us to respond to any newly raised issues. <strong>Please do not use this form for emergencies.</strong></p>
  <p>The form below will email our Customer Services Department.</p></td>
</tr>

<tr>       
<td colspan="2">&nbsp;</td>
</tr>

<? if($_REQUEST['whichaction'] == "send" && ($_REQUEST['email'] == "" || $_REQUEST['tel'] == "")){?>          
<tr>
  <td colspan="2" class="msg_fail"><b>Please complete all boxes below.</b></td>
</tr>
<tr>
  <td colspan="2">&nbsp;</td>
</tr>
<? }?>
<tr>
  <td colspan="2">&nbsp;</td>
</tr>
<tr>
  <td colspan="2" class="text036">Your telephone number </td>
</tr>
<tr>
  <td colspan="2"><input name="tel" type="text" id="tel" size="25" value="<?=$resident->tel?>"></td>
</tr>
<tr>
  <td colspan="2">&nbsp;</td>
</tr>
<tr>
  <td colspan="2" class="text036">Your email Address </td>
</tr>
<tr>
  <td colspan="2"><input name="email" type="text" id="email" size="45" value="<?=$resident->email?>"></td>
</tr>
<tr>
  <td colspan="2">&nbsp;</td>
</tr>              
<tr>
  <td colspan="2" class="text036">Your message</td>
</tr>
<tr>
  <td colspan="2"><textarea name="message" rows="10" id="message" style="width:340px "><?=$_REQUEST['message']?></textarea></td>
</tr>
<tr>
  <td colspan="2">&nbsp;</td>
</tr>
<tr>
  <td colspan="2"><input name="submit_button" type="image" id="submit_button" src="images/raise_an_issue/submit_button.jpg" width="65" height="20" border="0" alt="Click to Send"></td>
</tr>
              </table>
			  
			  </td>
              <td valign="top" background="images/raise_an_issue/keeping_informed.jpg" bgcolor="#F5F7FB" style="border-left:1px solid #eaeaea;background-repeat:no-repeat;"><p><img src="images/spacer.gif" alt="Keeping us Informed" width="348" height="310"></p>
              <p><br>
              We do try to work through all enquiries as soon as possible. If you feel your problem is not being dealt with effectivley please call our Customer Services Department on <?=$UTILS_TEL_MAIN;?> and ask to speak to the Property Manager (<?=$rmc->rmc['property_manager']?>) for 
                <?=$rmc->rmc['rmc_name']?>
                .</p></td>
              </tr>
          </table></td>
        </tr>
		<tr><td height="5" bgcolor="#416CA0" style="border:1px solid #003366;"><img src="images/spacer.gif" alt=""></td>
		</tr>
      </table>
	  <input type="hidden" name="whichaction" value="send">
	  </form>
    <!-- InstanceEndEditable --></td>
  </tr>
  <tr>
    <td height="25"></td>
  </tr>
  <tr>
    <td><table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="445" height="14"><a href="about_us.php" class="link416CA0">About RMG Living</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="contact_us.php" class="link416CA0">Contact Us</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="privacy.php" class="link416CA0">Legal Information</a></td>
        <td width="315" align="right">Copyright <?=date("Y", time())?> &copy; <a href="http://www.rmgltd.co.uk">Residential Management Group Ltd</a></td>
      </tr>
	  <tr><td colspan="2">&nbsp;</td></tr>
	  <tr><td colspan="2" style="text-align:left;"><p>&nbsp;</p>
	    </td>
	  </tr>
    </table></td>
  </tr> 
  <tr><td>&nbsp;</td></tr>
</table>
</body>
<!-- InstanceEnd --></html>
