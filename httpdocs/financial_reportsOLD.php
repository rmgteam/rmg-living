<?
require_once("utils.php");
require_once($UTILS_CLASS_PATH."website.class.php");
require_once($UTILS_CLASS_PATH."rmc.class.php");
require_once($UTILS_CLASS_PATH."resident.class.php");
$website = new website;
$rmc = new rmc;
$resident = new resident;

// Determine if allowed access into 'your community' section
$website->allow_community_access();

// Set resident
$resident->set_resident($_SESSION['resident_num']);

// Set rmc
$rmc->set_rmc($_SESSION['rmc_num']);

// Check if they are able to access the financial reports
if( ($resident->resident['is_resident_director'] != "Y" && $resident->resident['resident_is_steering_member'] != "Y") || ($rmc->rmc['show_content'] != "dir" && $rmc->rmc['show_content'] != "all") || $rmc->rmc['rmc_status'] != "Active" || $rmc->rmc['rmc_is_active'] != "1"){
	header("Location: ".$UTILS_HTTPS_ADDRESS."building_details.php");
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><!-- InstanceBegin template="/Templates/main.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- InstanceBeginEditable name="doctitle" -->
<title>RMG Living - Financial Reports</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<script language="javascript" src="library/jscript/functions/glossary_show.js"></script>
<script language="javascript" src="library/jscript/functions/show_account_ledger.js"></script>
<script language="javascript" src="library/jscript/functions/show_balance_sheet.js"></script>
<script language="javascript" src="library/jscript/functions/show_budget_comparison.js"></script>
<script language="javascript" src="library/jscript/functions/show_payable_aging_detail.js"></script>
<? if($resident->resident['resident_is_steering_member'] == "N"){?>
<script language="javascript" src="library/jscript/functions/show_receivable_aging_summary.js"></script>
<? }?>
<!-- InstanceEndEditable -->
<link href="styles.css" rel="stylesheet" type="text/css">
<!--[if lte IE 8]> 
<link href="/lte-ie8.css" rel="stylesheet" type="text/css">
<![endif]-->
<!--[if lte IE 7]> 
<link href="/lte-ie7.css" rel="stylesheet" type="text/css">
<![endif]-->
</head>
<body>
<? if($_SESSION['is_demo_account'] == "Y" && $_SESSION['is_developer'] != "Y"){?>
<form method="post" action="/building_details.php" style="margin:0; padding:0;">
<input type="hidden" name="a" value="cl">
<input type="hidden" name="s" value="gkfcbk45cgtf5kngn7kns5cg7snk5g7nsv5ng">
<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" style="margin:0; padding:0;">
	<tr>
		<td style="padding:4px;">Change Account:&nbsp;<input type="text" name="change_lessee" value="<?=$_SESSION['resident_ref']?>" size="20"> <input type="submit" name="submit_button" value="Change"></td>
	</tr>
</table>
</form>
<? }?>
<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#E9EEF4" style="border-bottom:1px solid #c1d1e1;"><div align="center"><img src="images/templates/header_bar_grad2.jpg" alt=""></div></td>
  </tr>
  <tr>
    <td style="border-top:1px solid #ffffff;border-bottom:1px solid #ffffff;">
	<table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="180"><a href="<? print $UTILS_HTTPS_ADDRESS;if( isset($_SESSION['login']) && $_SESSION['login'] != ""){print "/building_details.php";}else{print "/index.php";}?>"><img src="/images/templates/rmg_living_logo.jpg" style="margin-top:10px;" width="180" alt="RMG Living" border="0"></a></td>
        <td width="580" align="right">
		<?
		// User logged in is a 'developer', show any developer specific logos, or default to normal ones...
		if($_SESSION['is_developer'] == "Y"){
			
			$sql = "SELECT developer_id FROM cpm_residents_extra WHERE resident_num = ".$_SESSION['resident_num'];
			$result = @mysql_query($sql);
			$row = @mysql_fetch_row($result);
			if($row[0] != "" && file_exists($UTILS_FILE_PATH.'/images/developers/'.$row[0].'/header_'.$row[0].'.jpg') ){
				
				print '<img src="/images/developers/'.$row[0].'/header_'.$row[0].'.jpg" alt="RMG Living" width="580" height="86" BORDER=0>';
			}
			else{
				
				print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
			}
		}
		else{
		
			// Check to see if the Man. Co. has a developer association, if so, set developer specific images, or default to normal images...
			if( isset($_SESSION['login']) && $_SESSION['login'] != "" && $_SESSION['rmc_num'] != ""){
			
				$sql = "SELECT developer_id FROM cpm_rmcs_extra WHERE rmc_num = ".$_SESSION['rmc_num'];
				$result = @mysql_query($sql);
				$row = @mysql_fetch_row($result);
				if( $row[0] != "" && file_exists($UTILS_FILE_PATH.'/images/developers/'.$row[0].'/header_'.$row[0].'.jpg') ){
					
					print '<img src="/images/developers/'.$row[0].'/header_'.$row[0].'.jpg" alt="RMG Living" width="580" height="86" BORDER=0>';
				}
				else{
					
					print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
				}
			}
			else{
				
				print '<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">';
			}
		}
		?> 
		
		<?
		/*
		if( isset($_SESSION['login']) && $_SESSION['login'] != "" && $_SESSION['subsidiary_id'] != ""){
			$sql = "SELECT subsidiary_header_image FROM cpm_subsidiary WHERE subsidiary_id = ".$_SESSION['subsidiary_id'];
			$result = @mysql_query($sql);
			$row = @mysql_fetch_row($result);
			if($row[0] != "" ){ 
				?>
				<img src="/images/templates/header_image_w_logo.jpg" width="580" height="86" border="0" alt="RMG Living">
				<img src="/images/logos/<?=$row[0]?>" style="position:absolute; left:50%; margin-left:245px; margin-top:23px; z-index:10;" border="0" >
				<?
			}
			else{
				?>
				<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">
				<?
			}
		}
		else{
			?>
			<img src="/images/templates/header_image.jpg" width="580" height="86" border="0" alt="RMG Living">
			<?
		}
		*/
		?>
		</td>
      </tr>
    </table></td>
  </tr>
  <tr>
  <td>
  <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
  <tr>
    <td height="10" align="center" valign="bottom" bgcolor="#E9EEF4" style="border-bottom:1px solid #c1d1e1;border-top:1px solid #c1d1e1;"><img src="images/templates/header_bar_grad2.jpg" alt=""></td>
  </tr>
  <tr>
  <td height="8" align="center" valign="top" style="border-top:1px solid #ffffff;"><img src="images/templates/header_bar_grad3.jpg" alt=""></td>
  </tr>
  </table>
  </td>
  </tr>
 
  <tr>
    <td height="4"></td>
  </tr>
  <tr>
    <td><table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="28" valign="top">
	
	<table width="760" cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td>
	<!-- InstanceBeginEditable name="breadcrumbs" --><a href="building_details.php" class="crumbs">Your Community</a>&nbsp;>&nbsp;Financial Reports<!-- InstanceEndEditable -->
	</td>
	<td style="text-align:right;" nowrap>
	<? if(!empty($_SESSION['resident_session'])){?><a href="index.php?logoff=Y" class="crumbs">Log Off</a><? }?>
	</td>
	</tr>
	</table>
	
	</td>
  </tr>
</table>
</td>
  </tr>
  <tr>
    <td><!-- InstanceBeginEditable name="body" -->
      <table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td style="border-bottom:1px solid #003366;"><table width="760" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="15"><img src="images/dblue_box_top_left_corner.jpg" width="15" height="33"></td>
              <td width="730" bgcolor="#416CA0" style="border-top:1px solid #003366; vertical-align:middle"><img src="images/financial_reports/financial_reports_symbol.jpg" width="22" height="22" style="vertical-align:middle;">&nbsp;&nbsp;<span class="box_title">Financial Reports</span></td>
              <td width="15" align="right"><img src="images/dblue_box_top_right_corner.jpg" width="15" height="33"></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td><table width="760" border="0" cellspacing="0" cellpadding="15" style="border-left:1px solid #7ea0bd;border-right:1px solid #7ea0bd;border-top:1px solid #7ea0bd;border-bottom:1px solid #cccccc;">
            <tr>
              <td width="378" valign="top"><table width="348" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td>Below is a list of Financial Reports pertinent to your Management Company. All reports show 'live' data.</td>
</tr>
                <tr>
                  <td></td>
                </tr>
                <tr>
                  <td></td>
                </tr>
                <tr>
                  <td>Access to these reports is restricted to  Directors (and selected Steering Committee members, if applicable) of <?=$rmc->rmc['rmc_name']?>. </td>
</tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
</tr>
				
				<? if($resident->resident['resident_is_steering_member'] == "N"){?>
                <tr>
                  <td><p><strong><span class="text036">Aged Debtors Report</span><br>
                  </strong></p>
                  </td>
</tr>
                <tr>
                  <td>This is an <a onClick="glossary_show(10, 'Finance')" class="link036" style="cursor:hand;">arrears</a> report for your particular development or your Management Company.  It lists all residents that have an outstanding <a onClick="glossary_show(17, 'Finance')" class="link036" style="cursor:hand;">debt</a> to your Management Company and ages the debt according to the length that it has remained outstanding.  The "Total Owed" column shows the total amount owed by each <a onClick="glossary_show(18, 'Finance')" class="link036" style="cursor:hand;">debtor</a>.  It also shows any payments on account, known as <a onClick="glossary_show(23, 'Finance')" class="link036" style="cursor:hand;">pre-payments</a>.</td>
                </tr>
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td style="vertical-align:middle;"><img src="images/pdf.gif" alt="Adobe Acrobat Document" width="16" height="16" border="0" style="vertical-align:middle;">&nbsp;<a href="#" class="link416CA0" style="text-decoration:underline" onclick="show_receivable_aging_summary();"><strong>View report</strong></a></td>
                </tr>
                <tr>
                  <td valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td valign="top">&nbsp;</td>
                </tr>
				<? }?>
				
				<!--
                <tr>
                  <td valign="top"><p><strong><span class="text036">Account Ledger Report</span></strong></p></td>
				</tr>
                <tr>
                  <td valign="top"><p>This report provides a full breakdown of all <a onClick="glossary_show(21, 'Finance')" class="link036" style="cursor:hand;">expenditure</a>; it lists contractors' names, a brief description of what and in some cases when the work was carried out, along with the cost.  The report gives the detail of the amounts that can be seen in the Budget Comparison report (<a onClick="glossary_show(13, 'Finance')" class="link036" style="cursor:hand;">cumulative balances</a>).  These reports are on an <a onClick="glossary_show(7, 'Finance')" class="link036" style="cursor:hand;">accrual basis</a>.</p></td>
                </tr>
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td style="vertical-align:middle;"><img src="images/pdf.gif" alt="Adobe Acrobat Document" width="16" height="16" border="0" style="vertical-align:middle;">&nbsp;<a href="#" class="link416CA0" style="text-decoration:underline" onclick="show_account_ledger();"><strong>View report</strong></a></td>
                </tr>
                <tr>
                  <td valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td valign="top">&nbsp;</td>
                </tr>
				-->
				
				<!--
                <tr>
                  <td valign="top"><strong><span class="text036">Budget Comparison Report</span></strong> </td>
</tr>
                <tr>
                  <td valign="top">This report details your development / management companies performance against <a onClick="glossary_show(12, 'Finance')" class="link036" style="cursor:hand;">budgeted</a> amounts.  It highlights any deviations from the budget, these are known as <a onClick="glossary_show(27, 'Finance')" class="link036" style="cursor:hand;">variances</a>.  It is the <a onClick="glossary_show(14, 'Finance')" class="link036" style="cursor:hand;">current financial year to date</a> position only and does not take into account any <a onClick="glossary_show(26, 'Finance')" class="link036" style="cursor:hand;">surplus</a> / <a onClick="glossary_show(20, 'Finance')" class="link036" style="cursor:hand;">deficits</a> you may have incurred in prior years.<br>
                    These figures are a guideline only and will not take into account any purchase invoices and charges yet to be received / demanded.  In addition they will include invoices that relate to future periods (i.e. no <a onClick="glossary_show(22, 'Finance')" class="link036" style="cursor:hand;">expenditure prepayments</a> are made until the <a onClick="glossary_show(25, 'Finance')" class="link036" style="cursor:hand;">statutory books</a> are prepared). (Column definitions for this report can be viewed by <a href="guides/budget_comparison_report_guide.pdf" target="_blank" class="link416CA0" style="text-decoration:underline;">clicking here</a>).</td>
                </tr>
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td style="vertical-align:middle;"><img src="images/pdf.gif" alt="Adobe Acrobat Document" width="16" height="16" border="0" style="vertical-align:middle;">&nbsp;<a href="#" class="link416CA0" style="text-decoration:underline" onclick="show_budget_comparison();"><strong>View report</strong></a></td>
                </tr>
                <tr>
                  <td valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td valign="top">&nbsp;</td>
                </tr>
				-->
				
                <tr>
                  <td valign="top"><p><strong><span class="text036">Aged Creditors Report</span></strong></p></td>
</tr>
                <tr>
                  <td valign="top"><p>This report is a listing of invoices that are yet to be paid.  It also shows how long the invoices have remained unpaid (<a onClick="glossary_show(9, 'Finance')" class="link036" style="cursor:hand;">aged creditor</a>).</p></td>
                </tr>
                <tr>
                  <td height="10"></td>
                </tr>
                <tr>
                  <td style="vertical-align:middle;"><img src="images/pdf.gif" alt="Adobe Acrobat Document" width="16" height="16" border="0" style="vertical-align:middle;">&nbsp;<a href="#" class="link416CA0" style="text-decoration:underline" onclick="show_payable_aging_detail();"><strong>View report</strong></a></td>
                </tr>
                <tr>
                  <td valign="top">&nbsp;</td>
                </tr>
                <tr>
                  <td valign="top">&nbsp;</td>
                </tr>

              </table></td>
              <td width="378" valign="top" background="images/financial_reports/chart.jpg" bgcolor="#F5F7FB" style="border-left:1px solid #eaeaea;background-repeat:no-repeat;">
			    <table width="348" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td>&nbsp;</td>
                    <td align="right">&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td align="right">&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td align="right">&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td align="right">&nbsp;</td>
                  </tr>
                  <tr>
                    <td width="250">These documents can be viewed using Adobe Reader. Click to download. </td>
                    <td width="98" align="right" style="vertical-align:middle;"><a href="<?=$UTILS_ADOBE_URL?>" target="_blank"><img src="images/getacrobat.gif" alt="Get Adobe Reader" width="88" height="31" border="0" style="vertical-align:middle;"></a></td>
                  </tr>
                </table>
			    <p><img src="images/spacer.gif" alt="Helping you to manage your company finances" width="348" height="310"></p>
			  <p>&nbsp;</p></td>
              </tr>
          </table></td>
        </tr>
		<tr><td height="5" bgcolor="#416CA0" style="border:1px solid #003366;"><img src="images/spacer.gif" alt=""></td>
		</tr>
      </table>
    <!-- InstanceEndEditable --></td>
  </tr>
  <tr>
    <td height="25"></td>
  </tr>
  <tr>
    <td><table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="445" height="14"><a href="about_us.php" class="link416CA0">About RMG Living</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="contact_us.php" class="link416CA0">Contact Us</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="privacy.php" class="link416CA0">Legal Information</a></td>
        <td width="315" align="right">Copyright <?=date("Y", time())?> &copy; <a href="http://www.rmgltd.co.uk">Residential Management Group Ltd</a></td>
      </tr>
	  <tr><td colspan="2">&nbsp;</td></tr>
	  <tr><td colspan="2" style="text-align:left;"><p>&nbsp;</p>
	    </td>
	  </tr>
    </table></td>
  </tr> 
  <tr><td>&nbsp;</td></tr>
</table>
</body>
<!-- InstanceEnd --></html>
